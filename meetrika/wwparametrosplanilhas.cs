/*
               File: WWParametrosPlanilhas
        Description:  Par�metros das Planilhas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:51:27.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwparametrosplanilhas : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwparametrosplanilhas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwparametrosplanilhas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         dynParametrosPln_AreaTrabalhoCod = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"PARAMETROSPLN_AREATRABALHOCOD") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLAPARAMETROSPLN_AREATRABALHOCODF02( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_73 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_73_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_73_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV17ParametrosPln_Campo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ParametrosPln_Campo1", AV17ParametrosPln_Campo1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV21ParametrosPln_Campo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ParametrosPln_Campo2", AV21ParametrosPln_Campo2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV25ParametrosPln_Campo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ParametrosPln_Campo3", AV25ParametrosPln_Campo3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV34TFParametrosPln_AreaTrabalhoCod = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFParametrosPln_AreaTrabalhoCod", AV34TFParametrosPln_AreaTrabalhoCod);
               AV53TFParametrosPln_AreaTrabalhoCod_Sel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFParametrosPln_AreaTrabalhoCod_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFParametrosPln_AreaTrabalhoCod_Sel), 6, 0)));
               AV55TFParametrosPln_Aba = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFParametrosPln_Aba", AV55TFParametrosPln_Aba);
               AV56TFParametrosPln_Aba_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFParametrosPln_Aba_Sel", AV56TFParametrosPln_Aba_Sel);
               AV38TFParametrosPln_Campo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFParametrosPln_Campo", AV38TFParametrosPln_Campo);
               AV39TFParametrosPln_Campo_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFParametrosPln_Campo_Sel", AV39TFParametrosPln_Campo_Sel);
               AV42TFParametrosPln_Coluna = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFParametrosPln_Coluna", AV42TFParametrosPln_Coluna);
               AV43TFParametrosPln_Coluna_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFParametrosPln_Coluna_Sel", AV43TFParametrosPln_Coluna_Sel);
               AV46TFParametrosPln_Linha = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFParametrosPln_Linha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFParametrosPln_Linha), 6, 0)));
               AV47TFParametrosPln_Linha_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFParametrosPln_Linha_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFParametrosPln_Linha_To), 6, 0)));
               AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace", AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace);
               AV57ddo_ParametrosPln_AbaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ParametrosPln_AbaTitleControlIdToReplace", AV57ddo_ParametrosPln_AbaTitleControlIdToReplace);
               AV40ddo_ParametrosPln_CampoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_ParametrosPln_CampoTitleControlIdToReplace", AV40ddo_ParametrosPln_CampoTitleControlIdToReplace);
               AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace", AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace);
               AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace", AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace);
               AV81Pgmname = GetNextPar( );
               AV58TFParametrosPln_AreaTrabalhoCod_SelDsc = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFParametrosPln_AreaTrabalhoCod_SelDsc", AV58TFParametrosPln_AreaTrabalhoCod_SelDsc);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A848ParametrosPln_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ParametrosPln_Campo1, AV19DynamicFiltersSelector2, AV21ParametrosPln_Campo2, AV23DynamicFiltersSelector3, AV25ParametrosPln_Campo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFParametrosPln_AreaTrabalhoCod, AV53TFParametrosPln_AreaTrabalhoCod_Sel, AV55TFParametrosPln_Aba, AV56TFParametrosPln_Aba_Sel, AV38TFParametrosPln_Campo, AV39TFParametrosPln_Campo_Sel, AV42TFParametrosPln_Coluna, AV43TFParametrosPln_Coluna_Sel, AV46TFParametrosPln_Linha, AV47TFParametrosPln_Linha_To, AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace, AV57ddo_ParametrosPln_AbaTitleControlIdToReplace, AV40ddo_ParametrosPln_CampoTitleControlIdToReplace, AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace, AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace, AV81Pgmname, AV58TFParametrosPln_AreaTrabalhoCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A848ParametrosPln_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAF02( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTF02( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203118512826");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwparametrosplanilhas.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vPARAMETROSPLN_CAMPO1", AV17ParametrosPln_Campo1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vPARAMETROSPLN_CAMPO2", AV21ParametrosPln_Campo2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vPARAMETROSPLN_CAMPO3", AV25ParametrosPln_Campo3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSPLN_AREATRABALHOCOD", AV34TFParametrosPln_AreaTrabalhoCod);
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSPLN_AREATRABALHOCOD_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53TFParametrosPln_AreaTrabalhoCod_Sel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSPLN_ABA", StringUtil.RTrim( AV55TFParametrosPln_Aba));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSPLN_ABA_SEL", StringUtil.RTrim( AV56TFParametrosPln_Aba_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSPLN_CAMPO", AV38TFParametrosPln_Campo);
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSPLN_CAMPO_SEL", AV39TFParametrosPln_Campo_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSPLN_COLUNA", StringUtil.RTrim( AV42TFParametrosPln_Coluna));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSPLN_COLUNA_SEL", StringUtil.RTrim( AV43TFParametrosPln_Coluna_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSPLN_LINHA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFParametrosPln_Linha), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPARAMETROSPLN_LINHA_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFParametrosPln_Linha_To), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_73", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_73), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV49DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV49DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPARAMETROSPLN_AREATRABALHOCODTITLEFILTERDATA", AV33ParametrosPln_AreaTrabalhoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPARAMETROSPLN_AREATRABALHOCODTITLEFILTERDATA", AV33ParametrosPln_AreaTrabalhoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPARAMETROSPLN_ABATITLEFILTERDATA", AV54ParametrosPln_AbaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPARAMETROSPLN_ABATITLEFILTERDATA", AV54ParametrosPln_AbaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPARAMETROSPLN_CAMPOTITLEFILTERDATA", AV37ParametrosPln_CampoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPARAMETROSPLN_CAMPOTITLEFILTERDATA", AV37ParametrosPln_CampoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPARAMETROSPLN_COLUNATITLEFILTERDATA", AV41ParametrosPln_ColunaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPARAMETROSPLN_COLUNATITLEFILTERDATA", AV41ParametrosPln_ColunaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPARAMETROSPLN_LINHATITLEFILTERDATA", AV45ParametrosPln_LinhaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPARAMETROSPLN_LINHATITLEFILTERDATA", AV45ParametrosPln_LinhaTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV81Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Caption", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Tooltip", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Cls", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Selectedvalue_set", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Selectedtext_set", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Selectedtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_parametrospln_areatrabalhocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_parametrospln_areatrabalhocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Sortedstatus", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Includefilter", StringUtil.BoolToStr( Ddo_parametrospln_areatrabalhocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Filtertype", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_parametrospln_areatrabalhocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_parametrospln_areatrabalhocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Datalisttype", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Datalistproc", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_parametrospln_areatrabalhocod_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Sortasc", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Sortdsc", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Loadingdata", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Cleanfilter", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Noresultsfound", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Caption", StringUtil.RTrim( Ddo_parametrospln_aba_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Tooltip", StringUtil.RTrim( Ddo_parametrospln_aba_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Cls", StringUtil.RTrim( Ddo_parametrospln_aba_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Filteredtext_set", StringUtil.RTrim( Ddo_parametrospln_aba_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Selectedvalue_set", StringUtil.RTrim( Ddo_parametrospln_aba_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Dropdownoptionstype", StringUtil.RTrim( Ddo_parametrospln_aba_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_parametrospln_aba_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Includesortasc", StringUtil.BoolToStr( Ddo_parametrospln_aba_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Includesortdsc", StringUtil.BoolToStr( Ddo_parametrospln_aba_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Sortedstatus", StringUtil.RTrim( Ddo_parametrospln_aba_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Includefilter", StringUtil.BoolToStr( Ddo_parametrospln_aba_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Filtertype", StringUtil.RTrim( Ddo_parametrospln_aba_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Filterisrange", StringUtil.BoolToStr( Ddo_parametrospln_aba_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Includedatalist", StringUtil.BoolToStr( Ddo_parametrospln_aba_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Datalisttype", StringUtil.RTrim( Ddo_parametrospln_aba_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Datalistproc", StringUtil.RTrim( Ddo_parametrospln_aba_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_parametrospln_aba_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Sortasc", StringUtil.RTrim( Ddo_parametrospln_aba_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Sortdsc", StringUtil.RTrim( Ddo_parametrospln_aba_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Loadingdata", StringUtil.RTrim( Ddo_parametrospln_aba_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Cleanfilter", StringUtil.RTrim( Ddo_parametrospln_aba_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Noresultsfound", StringUtil.RTrim( Ddo_parametrospln_aba_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Searchbuttontext", StringUtil.RTrim( Ddo_parametrospln_aba_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Caption", StringUtil.RTrim( Ddo_parametrospln_campo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Tooltip", StringUtil.RTrim( Ddo_parametrospln_campo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Cls", StringUtil.RTrim( Ddo_parametrospln_campo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Filteredtext_set", StringUtil.RTrim( Ddo_parametrospln_campo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Selectedvalue_set", StringUtil.RTrim( Ddo_parametrospln_campo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_parametrospln_campo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_parametrospln_campo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Includesortasc", StringUtil.BoolToStr( Ddo_parametrospln_campo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Includesortdsc", StringUtil.BoolToStr( Ddo_parametrospln_campo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Sortedstatus", StringUtil.RTrim( Ddo_parametrospln_campo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Includefilter", StringUtil.BoolToStr( Ddo_parametrospln_campo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Filtertype", StringUtil.RTrim( Ddo_parametrospln_campo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Filterisrange", StringUtil.BoolToStr( Ddo_parametrospln_campo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Includedatalist", StringUtil.BoolToStr( Ddo_parametrospln_campo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Datalisttype", StringUtil.RTrim( Ddo_parametrospln_campo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Datalistproc", StringUtil.RTrim( Ddo_parametrospln_campo_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_parametrospln_campo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Sortasc", StringUtil.RTrim( Ddo_parametrospln_campo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Sortdsc", StringUtil.RTrim( Ddo_parametrospln_campo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Loadingdata", StringUtil.RTrim( Ddo_parametrospln_campo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Cleanfilter", StringUtil.RTrim( Ddo_parametrospln_campo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Noresultsfound", StringUtil.RTrim( Ddo_parametrospln_campo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Searchbuttontext", StringUtil.RTrim( Ddo_parametrospln_campo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Caption", StringUtil.RTrim( Ddo_parametrospln_coluna_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Tooltip", StringUtil.RTrim( Ddo_parametrospln_coluna_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Cls", StringUtil.RTrim( Ddo_parametrospln_coluna_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Filteredtext_set", StringUtil.RTrim( Ddo_parametrospln_coluna_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Selectedvalue_set", StringUtil.RTrim( Ddo_parametrospln_coluna_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Dropdownoptionstype", StringUtil.RTrim( Ddo_parametrospln_coluna_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_parametrospln_coluna_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Includesortasc", StringUtil.BoolToStr( Ddo_parametrospln_coluna_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Includesortdsc", StringUtil.BoolToStr( Ddo_parametrospln_coluna_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Sortedstatus", StringUtil.RTrim( Ddo_parametrospln_coluna_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Includefilter", StringUtil.BoolToStr( Ddo_parametrospln_coluna_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Filtertype", StringUtil.RTrim( Ddo_parametrospln_coluna_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Filterisrange", StringUtil.BoolToStr( Ddo_parametrospln_coluna_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Includedatalist", StringUtil.BoolToStr( Ddo_parametrospln_coluna_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Datalisttype", StringUtil.RTrim( Ddo_parametrospln_coluna_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Datalistproc", StringUtil.RTrim( Ddo_parametrospln_coluna_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_parametrospln_coluna_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Sortasc", StringUtil.RTrim( Ddo_parametrospln_coluna_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Sortdsc", StringUtil.RTrim( Ddo_parametrospln_coluna_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Loadingdata", StringUtil.RTrim( Ddo_parametrospln_coluna_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Cleanfilter", StringUtil.RTrim( Ddo_parametrospln_coluna_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Noresultsfound", StringUtil.RTrim( Ddo_parametrospln_coluna_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Searchbuttontext", StringUtil.RTrim( Ddo_parametrospln_coluna_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Caption", StringUtil.RTrim( Ddo_parametrospln_linha_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Tooltip", StringUtil.RTrim( Ddo_parametrospln_linha_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Cls", StringUtil.RTrim( Ddo_parametrospln_linha_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Filteredtext_set", StringUtil.RTrim( Ddo_parametrospln_linha_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Filteredtextto_set", StringUtil.RTrim( Ddo_parametrospln_linha_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Dropdownoptionstype", StringUtil.RTrim( Ddo_parametrospln_linha_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_parametrospln_linha_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Includesortasc", StringUtil.BoolToStr( Ddo_parametrospln_linha_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Includesortdsc", StringUtil.BoolToStr( Ddo_parametrospln_linha_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Sortedstatus", StringUtil.RTrim( Ddo_parametrospln_linha_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Includefilter", StringUtil.BoolToStr( Ddo_parametrospln_linha_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Filtertype", StringUtil.RTrim( Ddo_parametrospln_linha_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Filterisrange", StringUtil.BoolToStr( Ddo_parametrospln_linha_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Includedatalist", StringUtil.BoolToStr( Ddo_parametrospln_linha_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Sortasc", StringUtil.RTrim( Ddo_parametrospln_linha_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Sortdsc", StringUtil.RTrim( Ddo_parametrospln_linha_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Cleanfilter", StringUtil.RTrim( Ddo_parametrospln_linha_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Rangefilterfrom", StringUtil.RTrim( Ddo_parametrospln_linha_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Rangefilterto", StringUtil.RTrim( Ddo_parametrospln_linha_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Searchbuttontext", StringUtil.RTrim( Ddo_parametrospln_linha_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Activeeventkey", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Selectedvalue_get", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_AREATRABALHOCOD_Selectedtext_get", StringUtil.RTrim( Ddo_parametrospln_areatrabalhocod_Selectedtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Activeeventkey", StringUtil.RTrim( Ddo_parametrospln_aba_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Filteredtext_get", StringUtil.RTrim( Ddo_parametrospln_aba_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_ABA_Selectedvalue_get", StringUtil.RTrim( Ddo_parametrospln_aba_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Activeeventkey", StringUtil.RTrim( Ddo_parametrospln_campo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Filteredtext_get", StringUtil.RTrim( Ddo_parametrospln_campo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_CAMPO_Selectedvalue_get", StringUtil.RTrim( Ddo_parametrospln_campo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Activeeventkey", StringUtil.RTrim( Ddo_parametrospln_coluna_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Filteredtext_get", StringUtil.RTrim( Ddo_parametrospln_coluna_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_COLUNA_Selectedvalue_get", StringUtil.RTrim( Ddo_parametrospln_coluna_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Activeeventkey", StringUtil.RTrim( Ddo_parametrospln_linha_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Filteredtext_get", StringUtil.RTrim( Ddo_parametrospln_linha_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PARAMETROSPLN_LINHA_Filteredtextto_get", StringUtil.RTrim( Ddo_parametrospln_linha_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEF02( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTF02( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwparametrosplanilhas.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWParametrosPlanilhas" ;
      }

      public override String GetPgmdesc( )
      {
         return " Par�metros das Planilhas" ;
      }

      protected void WBF00( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_F02( true) ;
         }
         else
         {
            wb_table1_2_F02( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_F02e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(86, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(87, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,87);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrospln_areatrabalhocod_Internalname, AV34TFParametrosPln_AreaTrabalhoCod, StringUtil.RTrim( context.localUtil.Format( AV34TFParametrosPln_AreaTrabalhoCod, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrospln_areatrabalhocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrospln_areatrabalhocod_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWParametrosPlanilhas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrospln_areatrabalhocod_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53TFParametrosPln_AreaTrabalhoCod_Sel), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV53TFParametrosPln_AreaTrabalhoCod_Sel), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrospln_areatrabalhocod_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrospln_areatrabalhocod_sel_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWParametrosPlanilhas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrospln_areatrabalhocod_seldsc_Internalname, AV58TFParametrosPln_AreaTrabalhoCod_SelDsc, StringUtil.RTrim( context.localUtil.Format( AV58TFParametrosPln_AreaTrabalhoCod_SelDsc, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrospln_areatrabalhocod_seldsc_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrospln_areatrabalhocod_seldsc_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWParametrosPlanilhas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrospln_aba_Internalname, StringUtil.RTrim( AV55TFParametrosPln_Aba), StringUtil.RTrim( context.localUtil.Format( AV55TFParametrosPln_Aba, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrospln_aba_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrospln_aba_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWParametrosPlanilhas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrospln_aba_sel_Internalname, StringUtil.RTrim( AV56TFParametrosPln_Aba_Sel), StringUtil.RTrim( context.localUtil.Format( AV56TFParametrosPln_Aba_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrospln_aba_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrospln_aba_sel_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWParametrosPlanilhas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrospln_campo_Internalname, AV38TFParametrosPln_Campo, StringUtil.RTrim( context.localUtil.Format( AV38TFParametrosPln_Campo, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrospln_campo_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrospln_campo_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWParametrosPlanilhas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrospln_campo_sel_Internalname, AV39TFParametrosPln_Campo_Sel, StringUtil.RTrim( context.localUtil.Format( AV39TFParametrosPln_Campo_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrospln_campo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrospln_campo_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWParametrosPlanilhas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrospln_coluna_Internalname, StringUtil.RTrim( AV42TFParametrosPln_Coluna), StringUtil.RTrim( context.localUtil.Format( AV42TFParametrosPln_Coluna, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrospln_coluna_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrospln_coluna_Visible, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWParametrosPlanilhas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrospln_coluna_sel_Internalname, StringUtil.RTrim( AV43TFParametrosPln_Coluna_Sel), StringUtil.RTrim( context.localUtil.Format( AV43TFParametrosPln_Coluna_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrospln_coluna_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrospln_coluna_sel_Visible, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWParametrosPlanilhas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrospln_linha_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFParametrosPln_Linha), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46TFParametrosPln_Linha), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrospln_linha_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrospln_linha_Visible, 1, 0, "text", "", 70, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWParametrosPlanilhas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfparametrospln_linha_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFParametrosPln_Linha_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV47TFParametrosPln_Linha_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfparametrospln_linha_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfparametrospln_linha_to_Visible, 1, 0, "text", "", 70, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWParametrosPlanilhas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PARAMETROSPLN_AREATRABALHOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_parametrospln_areatrabalhocodtitlecontrolidtoreplace_Internalname, AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", 0, edtavDdo_parametrospln_areatrabalhocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWParametrosPlanilhas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PARAMETROSPLN_ABAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_parametrospln_abatitlecontrolidtoreplace_Internalname, AV57ddo_ParametrosPln_AbaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", 0, edtavDdo_parametrospln_abatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWParametrosPlanilhas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PARAMETROSPLN_CAMPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_parametrospln_campotitlecontrolidtoreplace_Internalname, AV40ddo_ParametrosPln_CampoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", 0, edtavDdo_parametrospln_campotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWParametrosPlanilhas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PARAMETROSPLN_COLUNAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_parametrospln_colunatitlecontrolidtoreplace_Internalname, AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, edtavDdo_parametrospln_colunatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWParametrosPlanilhas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PARAMETROSPLN_LINHAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_parametrospln_linhatitlecontrolidtoreplace_Internalname, AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavDdo_parametrospln_linhatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWParametrosPlanilhas.htm");
         }
         wbLoad = true;
      }

      protected void STARTF02( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Par�metros das Planilhas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPF00( ) ;
      }

      protected void WSF02( )
      {
         STARTF02( ) ;
         EVTF02( ) ;
      }

      protected void EVTF02( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11F02 */
                              E11F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PARAMETROSPLN_AREATRABALHOCOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12F02 */
                              E12F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PARAMETROSPLN_ABA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13F02 */
                              E13F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PARAMETROSPLN_CAMPO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14F02 */
                              E14F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PARAMETROSPLN_COLUNA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15F02 */
                              E15F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PARAMETROSPLN_LINHA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16F02 */
                              E16F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17F02 */
                              E17F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18F02 */
                              E18F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19F02 */
                              E19F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20F02 */
                              E20F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21F02 */
                              E21F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22F02 */
                              E22F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23F02 */
                              E23F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24F02 */
                              E24F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25F02 */
                              E25F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26F02 */
                              E26F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27F02 */
                              E27F02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_73_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
                              SubsflControlProps_732( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV79Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV80Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A848ParametrosPln_Codigo = (int)(context.localUtil.CToN( cgiGet( edtParametrosPln_Codigo_Internalname), ",", "."));
                              dynParametrosPln_AreaTrabalhoCod.Name = dynParametrosPln_AreaTrabalhoCod_Internalname;
                              dynParametrosPln_AreaTrabalhoCod.CurrentValue = cgiGet( dynParametrosPln_AreaTrabalhoCod_Internalname);
                              A847ParametrosPln_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynParametrosPln_AreaTrabalhoCod_Internalname), "."));
                              n847ParametrosPln_AreaTrabalhoCod = false;
                              A2015ParametrosPln_Aba = cgiGet( edtParametrosPln_Aba_Internalname);
                              A849ParametrosPln_Campo = StringUtil.Upper( cgiGet( edtParametrosPln_Campo_Internalname));
                              A850ParametrosPln_Coluna = StringUtil.Upper( cgiGet( edtParametrosPln_Coluna_Internalname));
                              A851ParametrosPln_Linha = (int)(context.localUtil.CToN( cgiGet( edtParametrosPln_Linha_Internalname), ",", "."));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28F02 */
                                    E28F02 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29F02 */
                                    E29F02 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E30F02 */
                                    E30F02 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Parametrospln_campo1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPARAMETROSPLN_CAMPO1"), AV17ParametrosPln_Campo1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Parametrospln_campo2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPARAMETROSPLN_CAMPO2"), AV21ParametrosPln_Campo2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Parametrospln_campo3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPARAMETROSPLN_CAMPO3"), AV25ParametrosPln_Campo3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrospln_areatrabalhocod Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_AREATRABALHOCOD"), AV34TFParametrosPln_AreaTrabalhoCod) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrospln_areatrabalhocod_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSPLN_AREATRABALHOCOD_SEL"), ",", ".") != Convert.ToDecimal( AV53TFParametrosPln_AreaTrabalhoCod_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrospln_aba Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_ABA"), AV55TFParametrosPln_Aba) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrospln_aba_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_ABA_SEL"), AV56TFParametrosPln_Aba_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrospln_campo Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_CAMPO"), AV38TFParametrosPln_Campo) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrospln_campo_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_CAMPO_SEL"), AV39TFParametrosPln_Campo_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrospln_coluna Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_COLUNA"), AV42TFParametrosPln_Coluna) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrospln_coluna_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_COLUNA_SEL"), AV43TFParametrosPln_Coluna_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrospln_linha Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSPLN_LINHA"), ",", ".") != Convert.ToDecimal( AV46TFParametrosPln_Linha )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfparametrospln_linha_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSPLN_LINHA_TO"), ",", ".") != Convert.ToDecimal( AV47TFParametrosPln_Linha_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEF02( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAF02( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("PARAMETROSPLN_CAMPO", "Campo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("PARAMETROSPLN_CAMPO", "Campo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("PARAMETROSPLN_CAMPO", "Campo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            GXCCtl = "PARAMETROSPLN_AREATRABALHOCOD_" + sGXsfl_73_idx;
            dynParametrosPln_AreaTrabalhoCod.Name = GXCCtl;
            dynParametrosPln_AreaTrabalhoCod.WebTags = "";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAPARAMETROSPLN_AREATRABALHOCODF02( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPARAMETROSPLN_AREATRABALHOCOD_dataF02( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPARAMETROSPLN_AREATRABALHOCOD_htmlF02( )
      {
         int gxdynajaxvalue ;
         GXDLAPARAMETROSPLN_AREATRABALHOCOD_dataF02( ) ;
         gxdynajaxindex = 1;
         dynParametrosPln_AreaTrabalhoCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynParametrosPln_AreaTrabalhoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAPARAMETROSPLN_AREATRABALHOCOD_dataF02( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Todas");
         /* Using cursor H00F02 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00F02_A847ParametrosPln_AreaTrabalhoCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00F02_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_732( ) ;
         while ( nGXsfl_73_idx <= nRC_GXsfl_73 )
         {
            sendrow_732( ) ;
            nGXsfl_73_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_732( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV17ParametrosPln_Campo1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV21ParametrosPln_Campo2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV25ParametrosPln_Campo3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV34TFParametrosPln_AreaTrabalhoCod ,
                                       int AV53TFParametrosPln_AreaTrabalhoCod_Sel ,
                                       String AV55TFParametrosPln_Aba ,
                                       String AV56TFParametrosPln_Aba_Sel ,
                                       String AV38TFParametrosPln_Campo ,
                                       String AV39TFParametrosPln_Campo_Sel ,
                                       String AV42TFParametrosPln_Coluna ,
                                       String AV43TFParametrosPln_Coluna_Sel ,
                                       int AV46TFParametrosPln_Linha ,
                                       int AV47TFParametrosPln_Linha_To ,
                                       String AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace ,
                                       String AV57ddo_ParametrosPln_AbaTitleControlIdToReplace ,
                                       String AV40ddo_ParametrosPln_CampoTitleControlIdToReplace ,
                                       String AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace ,
                                       String AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace ,
                                       String AV81Pgmname ,
                                       String AV58TFParametrosPln_AreaTrabalhoCod_SelDsc ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A848ParametrosPln_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFF02( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSPLN_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A848ParametrosPln_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PARAMETROSPLN_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A848ParametrosPln_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSPLN_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A847ParametrosPln_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PARAMETROSPLN_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSPLN_ABA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A2015ParametrosPln_Aba, ""))));
         GxWebStd.gx_hidden_field( context, "PARAMETROSPLN_ABA", StringUtil.RTrim( A2015ParametrosPln_Aba));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSPLN_CAMPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A849ParametrosPln_Campo, "@!"))));
         GxWebStd.gx_hidden_field( context, "PARAMETROSPLN_CAMPO", A849ParametrosPln_Campo);
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSPLN_COLUNA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A850ParametrosPln_Coluna, "@!"))));
         GxWebStd.gx_hidden_field( context, "PARAMETROSPLN_COLUNA", StringUtil.RTrim( A850ParametrosPln_Coluna));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSPLN_LINHA", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A851ParametrosPln_Linha), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PARAMETROSPLN_LINHA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A851ParametrosPln_Linha), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFF02( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV81Pgmname = "WWParametrosPlanilhas";
         context.Gx_err = 0;
      }

      protected void RFF02( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 73;
         /* Execute user event: E29F02 */
         E29F02 ();
         nGXsfl_73_idx = 1;
         sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
         SubsflControlProps_732( ) ;
         nGXsfl_73_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_732( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ,
                                                 AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 ,
                                                 AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ,
                                                 AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ,
                                                 AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 ,
                                                 AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ,
                                                 AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ,
                                                 AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 ,
                                                 AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ,
                                                 AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ,
                                                 AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ,
                                                 AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba ,
                                                 AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ,
                                                 AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo ,
                                                 AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ,
                                                 AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ,
                                                 AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha ,
                                                 AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ,
                                                 A849ParametrosPln_Campo ,
                                                 A6AreaTrabalho_Descricao ,
                                                 A847ParametrosPln_AreaTrabalhoCod ,
                                                 A2015ParametrosPln_Aba ,
                                                 A850ParametrosPln_Coluna ,
                                                 A851ParametrosPln_Linha ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 = StringUtil.Concat( StringUtil.RTrim( AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1), "%", "");
            lV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 = StringUtil.Concat( StringUtil.RTrim( AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2), "%", "");
            lV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 = StringUtil.Concat( StringUtil.RTrim( AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3), "%", "");
            lV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = StringUtil.Concat( StringUtil.RTrim( AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod), "%", "");
            lV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba = StringUtil.PadR( StringUtil.RTrim( AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba), 30, "%");
            lV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo = StringUtil.Concat( StringUtil.RTrim( AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo), "%", "");
            lV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = StringUtil.PadR( StringUtil.RTrim( AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna), 2, "%");
            /* Using cursor H00F03 */
            pr_default.execute(1, new Object[] {lV62WWParametrosPlanilhasDS_2_Parametrospln_campo1, lV65WWParametrosPlanilhasDS_5_Parametrospln_campo2, lV68WWParametrosPlanilhasDS_8_Parametrospln_campo3, lV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod, AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel, lV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba, AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel, lV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo, AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel, lV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna, AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel, AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha, AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_73_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A6AreaTrabalho_Descricao = H00F03_A6AreaTrabalho_Descricao[0];
               n6AreaTrabalho_Descricao = H00F03_n6AreaTrabalho_Descricao[0];
               A851ParametrosPln_Linha = H00F03_A851ParametrosPln_Linha[0];
               A850ParametrosPln_Coluna = H00F03_A850ParametrosPln_Coluna[0];
               A849ParametrosPln_Campo = H00F03_A849ParametrosPln_Campo[0];
               A2015ParametrosPln_Aba = H00F03_A2015ParametrosPln_Aba[0];
               A847ParametrosPln_AreaTrabalhoCod = H00F03_A847ParametrosPln_AreaTrabalhoCod[0];
               n847ParametrosPln_AreaTrabalhoCod = H00F03_n847ParametrosPln_AreaTrabalhoCod[0];
               A848ParametrosPln_Codigo = H00F03_A848ParametrosPln_Codigo[0];
               A6AreaTrabalho_Descricao = H00F03_A6AreaTrabalho_Descricao[0];
               n6AreaTrabalho_Descricao = H00F03_n6AreaTrabalho_Descricao[0];
               /* Execute user event: E30F02 */
               E30F02 ();
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 73;
            WBF00( ) ;
         }
         nGXsfl_73_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 = AV17ParametrosPln_Campo1;
         AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 = AV21ParametrosPln_Campo2;
         AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 = AV25ParametrosPln_Campo3;
         AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = AV34TFParametrosPln_AreaTrabalhoCod;
         AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel = AV53TFParametrosPln_AreaTrabalhoCod_Sel;
         AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba = AV55TFParametrosPln_Aba;
         AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel = AV56TFParametrosPln_Aba_Sel;
         AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo = AV38TFParametrosPln_Campo;
         AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel = AV39TFParametrosPln_Campo_Sel;
         AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = AV42TFParametrosPln_Coluna;
         AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel = AV43TFParametrosPln_Coluna_Sel;
         AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha = AV46TFParametrosPln_Linha;
         AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to = AV47TFParametrosPln_Linha_To;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ,
                                              AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 ,
                                              AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ,
                                              AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ,
                                              AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 ,
                                              AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ,
                                              AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ,
                                              AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 ,
                                              AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ,
                                              AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ,
                                              AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ,
                                              AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba ,
                                              AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ,
                                              AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo ,
                                              AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ,
                                              AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ,
                                              AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha ,
                                              AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ,
                                              A849ParametrosPln_Campo ,
                                              A6AreaTrabalho_Descricao ,
                                              A847ParametrosPln_AreaTrabalhoCod ,
                                              A2015ParametrosPln_Aba ,
                                              A850ParametrosPln_Coluna ,
                                              A851ParametrosPln_Linha ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 = StringUtil.Concat( StringUtil.RTrim( AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1), "%", "");
         lV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 = StringUtil.Concat( StringUtil.RTrim( AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2), "%", "");
         lV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 = StringUtil.Concat( StringUtil.RTrim( AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3), "%", "");
         lV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = StringUtil.Concat( StringUtil.RTrim( AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod), "%", "");
         lV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba = StringUtil.PadR( StringUtil.RTrim( AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba), 30, "%");
         lV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo = StringUtil.Concat( StringUtil.RTrim( AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo), "%", "");
         lV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = StringUtil.PadR( StringUtil.RTrim( AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna), 2, "%");
         /* Using cursor H00F04 */
         pr_default.execute(2, new Object[] {lV62WWParametrosPlanilhasDS_2_Parametrospln_campo1, lV65WWParametrosPlanilhasDS_5_Parametrospln_campo2, lV68WWParametrosPlanilhasDS_8_Parametrospln_campo3, lV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod, AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel, lV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba, AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel, lV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo, AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel, lV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna, AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel, AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha, AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to});
         GRID_nRecordCount = H00F04_AGRID_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 = AV17ParametrosPln_Campo1;
         AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 = AV21ParametrosPln_Campo2;
         AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 = AV25ParametrosPln_Campo3;
         AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = AV34TFParametrosPln_AreaTrabalhoCod;
         AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel = AV53TFParametrosPln_AreaTrabalhoCod_Sel;
         AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba = AV55TFParametrosPln_Aba;
         AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel = AV56TFParametrosPln_Aba_Sel;
         AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo = AV38TFParametrosPln_Campo;
         AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel = AV39TFParametrosPln_Campo_Sel;
         AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = AV42TFParametrosPln_Coluna;
         AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel = AV43TFParametrosPln_Coluna_Sel;
         AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha = AV46TFParametrosPln_Linha;
         AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to = AV47TFParametrosPln_Linha_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ParametrosPln_Campo1, AV19DynamicFiltersSelector2, AV21ParametrosPln_Campo2, AV23DynamicFiltersSelector3, AV25ParametrosPln_Campo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFParametrosPln_AreaTrabalhoCod, AV53TFParametrosPln_AreaTrabalhoCod_Sel, AV55TFParametrosPln_Aba, AV56TFParametrosPln_Aba_Sel, AV38TFParametrosPln_Campo, AV39TFParametrosPln_Campo_Sel, AV42TFParametrosPln_Coluna, AV43TFParametrosPln_Coluna_Sel, AV46TFParametrosPln_Linha, AV47TFParametrosPln_Linha_To, AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace, AV57ddo_ParametrosPln_AbaTitleControlIdToReplace, AV40ddo_ParametrosPln_CampoTitleControlIdToReplace, AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace, AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace, AV81Pgmname, AV58TFParametrosPln_AreaTrabalhoCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A848ParametrosPln_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 = AV17ParametrosPln_Campo1;
         AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 = AV21ParametrosPln_Campo2;
         AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 = AV25ParametrosPln_Campo3;
         AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = AV34TFParametrosPln_AreaTrabalhoCod;
         AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel = AV53TFParametrosPln_AreaTrabalhoCod_Sel;
         AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba = AV55TFParametrosPln_Aba;
         AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel = AV56TFParametrosPln_Aba_Sel;
         AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo = AV38TFParametrosPln_Campo;
         AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel = AV39TFParametrosPln_Campo_Sel;
         AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = AV42TFParametrosPln_Coluna;
         AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel = AV43TFParametrosPln_Coluna_Sel;
         AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha = AV46TFParametrosPln_Linha;
         AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to = AV47TFParametrosPln_Linha_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ParametrosPln_Campo1, AV19DynamicFiltersSelector2, AV21ParametrosPln_Campo2, AV23DynamicFiltersSelector3, AV25ParametrosPln_Campo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFParametrosPln_AreaTrabalhoCod, AV53TFParametrosPln_AreaTrabalhoCod_Sel, AV55TFParametrosPln_Aba, AV56TFParametrosPln_Aba_Sel, AV38TFParametrosPln_Campo, AV39TFParametrosPln_Campo_Sel, AV42TFParametrosPln_Coluna, AV43TFParametrosPln_Coluna_Sel, AV46TFParametrosPln_Linha, AV47TFParametrosPln_Linha_To, AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace, AV57ddo_ParametrosPln_AbaTitleControlIdToReplace, AV40ddo_ParametrosPln_CampoTitleControlIdToReplace, AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace, AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace, AV81Pgmname, AV58TFParametrosPln_AreaTrabalhoCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A848ParametrosPln_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 = AV17ParametrosPln_Campo1;
         AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 = AV21ParametrosPln_Campo2;
         AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 = AV25ParametrosPln_Campo3;
         AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = AV34TFParametrosPln_AreaTrabalhoCod;
         AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel = AV53TFParametrosPln_AreaTrabalhoCod_Sel;
         AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba = AV55TFParametrosPln_Aba;
         AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel = AV56TFParametrosPln_Aba_Sel;
         AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo = AV38TFParametrosPln_Campo;
         AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel = AV39TFParametrosPln_Campo_Sel;
         AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = AV42TFParametrosPln_Coluna;
         AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel = AV43TFParametrosPln_Coluna_Sel;
         AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha = AV46TFParametrosPln_Linha;
         AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to = AV47TFParametrosPln_Linha_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ParametrosPln_Campo1, AV19DynamicFiltersSelector2, AV21ParametrosPln_Campo2, AV23DynamicFiltersSelector3, AV25ParametrosPln_Campo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFParametrosPln_AreaTrabalhoCod, AV53TFParametrosPln_AreaTrabalhoCod_Sel, AV55TFParametrosPln_Aba, AV56TFParametrosPln_Aba_Sel, AV38TFParametrosPln_Campo, AV39TFParametrosPln_Campo_Sel, AV42TFParametrosPln_Coluna, AV43TFParametrosPln_Coluna_Sel, AV46TFParametrosPln_Linha, AV47TFParametrosPln_Linha_To, AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace, AV57ddo_ParametrosPln_AbaTitleControlIdToReplace, AV40ddo_ParametrosPln_CampoTitleControlIdToReplace, AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace, AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace, AV81Pgmname, AV58TFParametrosPln_AreaTrabalhoCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A848ParametrosPln_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 = AV17ParametrosPln_Campo1;
         AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 = AV21ParametrosPln_Campo2;
         AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 = AV25ParametrosPln_Campo3;
         AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = AV34TFParametrosPln_AreaTrabalhoCod;
         AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel = AV53TFParametrosPln_AreaTrabalhoCod_Sel;
         AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba = AV55TFParametrosPln_Aba;
         AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel = AV56TFParametrosPln_Aba_Sel;
         AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo = AV38TFParametrosPln_Campo;
         AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel = AV39TFParametrosPln_Campo_Sel;
         AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = AV42TFParametrosPln_Coluna;
         AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel = AV43TFParametrosPln_Coluna_Sel;
         AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha = AV46TFParametrosPln_Linha;
         AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to = AV47TFParametrosPln_Linha_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ParametrosPln_Campo1, AV19DynamicFiltersSelector2, AV21ParametrosPln_Campo2, AV23DynamicFiltersSelector3, AV25ParametrosPln_Campo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFParametrosPln_AreaTrabalhoCod, AV53TFParametrosPln_AreaTrabalhoCod_Sel, AV55TFParametrosPln_Aba, AV56TFParametrosPln_Aba_Sel, AV38TFParametrosPln_Campo, AV39TFParametrosPln_Campo_Sel, AV42TFParametrosPln_Coluna, AV43TFParametrosPln_Coluna_Sel, AV46TFParametrosPln_Linha, AV47TFParametrosPln_Linha_To, AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace, AV57ddo_ParametrosPln_AbaTitleControlIdToReplace, AV40ddo_ParametrosPln_CampoTitleControlIdToReplace, AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace, AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace, AV81Pgmname, AV58TFParametrosPln_AreaTrabalhoCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A848ParametrosPln_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 = AV17ParametrosPln_Campo1;
         AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 = AV21ParametrosPln_Campo2;
         AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 = AV25ParametrosPln_Campo3;
         AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = AV34TFParametrosPln_AreaTrabalhoCod;
         AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel = AV53TFParametrosPln_AreaTrabalhoCod_Sel;
         AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba = AV55TFParametrosPln_Aba;
         AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel = AV56TFParametrosPln_Aba_Sel;
         AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo = AV38TFParametrosPln_Campo;
         AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel = AV39TFParametrosPln_Campo_Sel;
         AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = AV42TFParametrosPln_Coluna;
         AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel = AV43TFParametrosPln_Coluna_Sel;
         AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha = AV46TFParametrosPln_Linha;
         AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to = AV47TFParametrosPln_Linha_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ParametrosPln_Campo1, AV19DynamicFiltersSelector2, AV21ParametrosPln_Campo2, AV23DynamicFiltersSelector3, AV25ParametrosPln_Campo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFParametrosPln_AreaTrabalhoCod, AV53TFParametrosPln_AreaTrabalhoCod_Sel, AV55TFParametrosPln_Aba, AV56TFParametrosPln_Aba_Sel, AV38TFParametrosPln_Campo, AV39TFParametrosPln_Campo_Sel, AV42TFParametrosPln_Coluna, AV43TFParametrosPln_Coluna_Sel, AV46TFParametrosPln_Linha, AV47TFParametrosPln_Linha_To, AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace, AV57ddo_ParametrosPln_AbaTitleControlIdToReplace, AV40ddo_ParametrosPln_CampoTitleControlIdToReplace, AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace, AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace, AV81Pgmname, AV58TFParametrosPln_AreaTrabalhoCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A848ParametrosPln_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPF00( )
      {
         /* Before Start, stand alone formulas. */
         AV81Pgmname = "WWParametrosPlanilhas";
         context.Gx_err = 0;
         GXAPARAMETROSPLN_AREATRABALHOCOD_htmlF02( ) ;
         /* Using cursor H00F05 */
         pr_default.execute(3, new Object[] {n847ParametrosPln_AreaTrabalhoCod, A847ParametrosPln_AreaTrabalhoCod});
         A6AreaTrabalho_Descricao = H00F05_A6AreaTrabalho_Descricao[0];
         n6AreaTrabalho_Descricao = H00F05_n6AreaTrabalho_Descricao[0];
         pr_default.close(3);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E28F02 */
         E28F02 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV49DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vPARAMETROSPLN_AREATRABALHOCODTITLEFILTERDATA"), AV33ParametrosPln_AreaTrabalhoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPARAMETROSPLN_ABATITLEFILTERDATA"), AV54ParametrosPln_AbaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPARAMETROSPLN_CAMPOTITLEFILTERDATA"), AV37ParametrosPln_CampoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPARAMETROSPLN_COLUNATITLEFILTERDATA"), AV41ParametrosPln_ColunaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPARAMETROSPLN_LINHATITLEFILTERDATA"), AV45ParametrosPln_LinhaTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV17ParametrosPln_Campo1 = StringUtil.Upper( cgiGet( edtavParametrospln_campo1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ParametrosPln_Campo1", AV17ParametrosPln_Campo1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            AV21ParametrosPln_Campo2 = StringUtil.Upper( cgiGet( edtavParametrospln_campo2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ParametrosPln_Campo2", AV21ParametrosPln_Campo2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            AV25ParametrosPln_Campo3 = StringUtil.Upper( cgiGet( edtavParametrospln_campo3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ParametrosPln_Campo3", AV25ParametrosPln_Campo3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV34TFParametrosPln_AreaTrabalhoCod = StringUtil.Upper( cgiGet( edtavTfparametrospln_areatrabalhocod_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFParametrosPln_AreaTrabalhoCod", AV34TFParametrosPln_AreaTrabalhoCod);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfparametrospln_areatrabalhocod_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfparametrospln_areatrabalhocod_sel_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPARAMETROSPLN_AREATRABALHOCOD_SEL");
               GX_FocusControl = edtavTfparametrospln_areatrabalhocod_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53TFParametrosPln_AreaTrabalhoCod_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFParametrosPln_AreaTrabalhoCod_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFParametrosPln_AreaTrabalhoCod_Sel), 6, 0)));
            }
            else
            {
               AV53TFParametrosPln_AreaTrabalhoCod_Sel = (int)(context.localUtil.CToN( cgiGet( edtavTfparametrospln_areatrabalhocod_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFParametrosPln_AreaTrabalhoCod_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFParametrosPln_AreaTrabalhoCod_Sel), 6, 0)));
            }
            AV58TFParametrosPln_AreaTrabalhoCod_SelDsc = cgiGet( edtavTfparametrospln_areatrabalhocod_seldsc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFParametrosPln_AreaTrabalhoCod_SelDsc", AV58TFParametrosPln_AreaTrabalhoCod_SelDsc);
            AV55TFParametrosPln_Aba = cgiGet( edtavTfparametrospln_aba_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFParametrosPln_Aba", AV55TFParametrosPln_Aba);
            AV56TFParametrosPln_Aba_Sel = cgiGet( edtavTfparametrospln_aba_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFParametrosPln_Aba_Sel", AV56TFParametrosPln_Aba_Sel);
            AV38TFParametrosPln_Campo = StringUtil.Upper( cgiGet( edtavTfparametrospln_campo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFParametrosPln_Campo", AV38TFParametrosPln_Campo);
            AV39TFParametrosPln_Campo_Sel = StringUtil.Upper( cgiGet( edtavTfparametrospln_campo_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFParametrosPln_Campo_Sel", AV39TFParametrosPln_Campo_Sel);
            AV42TFParametrosPln_Coluna = StringUtil.Upper( cgiGet( edtavTfparametrospln_coluna_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFParametrosPln_Coluna", AV42TFParametrosPln_Coluna);
            AV43TFParametrosPln_Coluna_Sel = StringUtil.Upper( cgiGet( edtavTfparametrospln_coluna_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFParametrosPln_Coluna_Sel", AV43TFParametrosPln_Coluna_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfparametrospln_linha_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfparametrospln_linha_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPARAMETROSPLN_LINHA");
               GX_FocusControl = edtavTfparametrospln_linha_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFParametrosPln_Linha = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFParametrosPln_Linha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFParametrosPln_Linha), 6, 0)));
            }
            else
            {
               AV46TFParametrosPln_Linha = (int)(context.localUtil.CToN( cgiGet( edtavTfparametrospln_linha_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFParametrosPln_Linha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFParametrosPln_Linha), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfparametrospln_linha_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfparametrospln_linha_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPARAMETROSPLN_LINHA_TO");
               GX_FocusControl = edtavTfparametrospln_linha_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFParametrosPln_Linha_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFParametrosPln_Linha_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFParametrosPln_Linha_To), 6, 0)));
            }
            else
            {
               AV47TFParametrosPln_Linha_To = (int)(context.localUtil.CToN( cgiGet( edtavTfparametrospln_linha_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFParametrosPln_Linha_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFParametrosPln_Linha_To), 6, 0)));
            }
            AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace = cgiGet( edtavDdo_parametrospln_areatrabalhocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace", AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace);
            AV57ddo_ParametrosPln_AbaTitleControlIdToReplace = cgiGet( edtavDdo_parametrospln_abatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ParametrosPln_AbaTitleControlIdToReplace", AV57ddo_ParametrosPln_AbaTitleControlIdToReplace);
            AV40ddo_ParametrosPln_CampoTitleControlIdToReplace = cgiGet( edtavDdo_parametrospln_campotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_ParametrosPln_CampoTitleControlIdToReplace", AV40ddo_ParametrosPln_CampoTitleControlIdToReplace);
            AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace = cgiGet( edtavDdo_parametrospln_colunatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace", AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace);
            AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace = cgiGet( edtavDdo_parametrospln_linhatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace", AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_73 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_73"), ",", "."));
            AV51GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV52GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_parametrospln_areatrabalhocod_Caption = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Caption");
            Ddo_parametrospln_areatrabalhocod_Tooltip = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Tooltip");
            Ddo_parametrospln_areatrabalhocod_Cls = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Cls");
            Ddo_parametrospln_areatrabalhocod_Filteredtext_set = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Filteredtext_set");
            Ddo_parametrospln_areatrabalhocod_Selectedvalue_set = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Selectedvalue_set");
            Ddo_parametrospln_areatrabalhocod_Selectedtext_set = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Selectedtext_set");
            Ddo_parametrospln_areatrabalhocod_Dropdownoptionstype = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Dropdownoptionstype");
            Ddo_parametrospln_areatrabalhocod_Titlecontrolidtoreplace = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Titlecontrolidtoreplace");
            Ddo_parametrospln_areatrabalhocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Includesortasc"));
            Ddo_parametrospln_areatrabalhocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Includesortdsc"));
            Ddo_parametrospln_areatrabalhocod_Sortedstatus = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Sortedstatus");
            Ddo_parametrospln_areatrabalhocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Includefilter"));
            Ddo_parametrospln_areatrabalhocod_Filtertype = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Filtertype");
            Ddo_parametrospln_areatrabalhocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Filterisrange"));
            Ddo_parametrospln_areatrabalhocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Includedatalist"));
            Ddo_parametrospln_areatrabalhocod_Datalisttype = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Datalisttype");
            Ddo_parametrospln_areatrabalhocod_Datalistproc = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Datalistproc");
            Ddo_parametrospln_areatrabalhocod_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_parametrospln_areatrabalhocod_Sortasc = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Sortasc");
            Ddo_parametrospln_areatrabalhocod_Sortdsc = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Sortdsc");
            Ddo_parametrospln_areatrabalhocod_Loadingdata = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Loadingdata");
            Ddo_parametrospln_areatrabalhocod_Cleanfilter = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Cleanfilter");
            Ddo_parametrospln_areatrabalhocod_Noresultsfound = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Noresultsfound");
            Ddo_parametrospln_areatrabalhocod_Searchbuttontext = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Searchbuttontext");
            Ddo_parametrospln_aba_Caption = cgiGet( "DDO_PARAMETROSPLN_ABA_Caption");
            Ddo_parametrospln_aba_Tooltip = cgiGet( "DDO_PARAMETROSPLN_ABA_Tooltip");
            Ddo_parametrospln_aba_Cls = cgiGet( "DDO_PARAMETROSPLN_ABA_Cls");
            Ddo_parametrospln_aba_Filteredtext_set = cgiGet( "DDO_PARAMETROSPLN_ABA_Filteredtext_set");
            Ddo_parametrospln_aba_Selectedvalue_set = cgiGet( "DDO_PARAMETROSPLN_ABA_Selectedvalue_set");
            Ddo_parametrospln_aba_Dropdownoptionstype = cgiGet( "DDO_PARAMETROSPLN_ABA_Dropdownoptionstype");
            Ddo_parametrospln_aba_Titlecontrolidtoreplace = cgiGet( "DDO_PARAMETROSPLN_ABA_Titlecontrolidtoreplace");
            Ddo_parametrospln_aba_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_ABA_Includesortasc"));
            Ddo_parametrospln_aba_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_ABA_Includesortdsc"));
            Ddo_parametrospln_aba_Sortedstatus = cgiGet( "DDO_PARAMETROSPLN_ABA_Sortedstatus");
            Ddo_parametrospln_aba_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_ABA_Includefilter"));
            Ddo_parametrospln_aba_Filtertype = cgiGet( "DDO_PARAMETROSPLN_ABA_Filtertype");
            Ddo_parametrospln_aba_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_ABA_Filterisrange"));
            Ddo_parametrospln_aba_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_ABA_Includedatalist"));
            Ddo_parametrospln_aba_Datalisttype = cgiGet( "DDO_PARAMETROSPLN_ABA_Datalisttype");
            Ddo_parametrospln_aba_Datalistproc = cgiGet( "DDO_PARAMETROSPLN_ABA_Datalistproc");
            Ddo_parametrospln_aba_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PARAMETROSPLN_ABA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_parametrospln_aba_Sortasc = cgiGet( "DDO_PARAMETROSPLN_ABA_Sortasc");
            Ddo_parametrospln_aba_Sortdsc = cgiGet( "DDO_PARAMETROSPLN_ABA_Sortdsc");
            Ddo_parametrospln_aba_Loadingdata = cgiGet( "DDO_PARAMETROSPLN_ABA_Loadingdata");
            Ddo_parametrospln_aba_Cleanfilter = cgiGet( "DDO_PARAMETROSPLN_ABA_Cleanfilter");
            Ddo_parametrospln_aba_Noresultsfound = cgiGet( "DDO_PARAMETROSPLN_ABA_Noresultsfound");
            Ddo_parametrospln_aba_Searchbuttontext = cgiGet( "DDO_PARAMETROSPLN_ABA_Searchbuttontext");
            Ddo_parametrospln_campo_Caption = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Caption");
            Ddo_parametrospln_campo_Tooltip = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Tooltip");
            Ddo_parametrospln_campo_Cls = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Cls");
            Ddo_parametrospln_campo_Filteredtext_set = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Filteredtext_set");
            Ddo_parametrospln_campo_Selectedvalue_set = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Selectedvalue_set");
            Ddo_parametrospln_campo_Dropdownoptionstype = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Dropdownoptionstype");
            Ddo_parametrospln_campo_Titlecontrolidtoreplace = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Titlecontrolidtoreplace");
            Ddo_parametrospln_campo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_CAMPO_Includesortasc"));
            Ddo_parametrospln_campo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_CAMPO_Includesortdsc"));
            Ddo_parametrospln_campo_Sortedstatus = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Sortedstatus");
            Ddo_parametrospln_campo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_CAMPO_Includefilter"));
            Ddo_parametrospln_campo_Filtertype = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Filtertype");
            Ddo_parametrospln_campo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_CAMPO_Filterisrange"));
            Ddo_parametrospln_campo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_CAMPO_Includedatalist"));
            Ddo_parametrospln_campo_Datalisttype = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Datalisttype");
            Ddo_parametrospln_campo_Datalistproc = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Datalistproc");
            Ddo_parametrospln_campo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PARAMETROSPLN_CAMPO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_parametrospln_campo_Sortasc = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Sortasc");
            Ddo_parametrospln_campo_Sortdsc = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Sortdsc");
            Ddo_parametrospln_campo_Loadingdata = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Loadingdata");
            Ddo_parametrospln_campo_Cleanfilter = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Cleanfilter");
            Ddo_parametrospln_campo_Noresultsfound = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Noresultsfound");
            Ddo_parametrospln_campo_Searchbuttontext = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Searchbuttontext");
            Ddo_parametrospln_coluna_Caption = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Caption");
            Ddo_parametrospln_coluna_Tooltip = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Tooltip");
            Ddo_parametrospln_coluna_Cls = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Cls");
            Ddo_parametrospln_coluna_Filteredtext_set = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Filteredtext_set");
            Ddo_parametrospln_coluna_Selectedvalue_set = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Selectedvalue_set");
            Ddo_parametrospln_coluna_Dropdownoptionstype = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Dropdownoptionstype");
            Ddo_parametrospln_coluna_Titlecontrolidtoreplace = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Titlecontrolidtoreplace");
            Ddo_parametrospln_coluna_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_COLUNA_Includesortasc"));
            Ddo_parametrospln_coluna_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_COLUNA_Includesortdsc"));
            Ddo_parametrospln_coluna_Sortedstatus = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Sortedstatus");
            Ddo_parametrospln_coluna_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_COLUNA_Includefilter"));
            Ddo_parametrospln_coluna_Filtertype = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Filtertype");
            Ddo_parametrospln_coluna_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_COLUNA_Filterisrange"));
            Ddo_parametrospln_coluna_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_COLUNA_Includedatalist"));
            Ddo_parametrospln_coluna_Datalisttype = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Datalisttype");
            Ddo_parametrospln_coluna_Datalistproc = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Datalistproc");
            Ddo_parametrospln_coluna_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PARAMETROSPLN_COLUNA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_parametrospln_coluna_Sortasc = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Sortasc");
            Ddo_parametrospln_coluna_Sortdsc = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Sortdsc");
            Ddo_parametrospln_coluna_Loadingdata = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Loadingdata");
            Ddo_parametrospln_coluna_Cleanfilter = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Cleanfilter");
            Ddo_parametrospln_coluna_Noresultsfound = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Noresultsfound");
            Ddo_parametrospln_coluna_Searchbuttontext = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Searchbuttontext");
            Ddo_parametrospln_linha_Caption = cgiGet( "DDO_PARAMETROSPLN_LINHA_Caption");
            Ddo_parametrospln_linha_Tooltip = cgiGet( "DDO_PARAMETROSPLN_LINHA_Tooltip");
            Ddo_parametrospln_linha_Cls = cgiGet( "DDO_PARAMETROSPLN_LINHA_Cls");
            Ddo_parametrospln_linha_Filteredtext_set = cgiGet( "DDO_PARAMETROSPLN_LINHA_Filteredtext_set");
            Ddo_parametrospln_linha_Filteredtextto_set = cgiGet( "DDO_PARAMETROSPLN_LINHA_Filteredtextto_set");
            Ddo_parametrospln_linha_Dropdownoptionstype = cgiGet( "DDO_PARAMETROSPLN_LINHA_Dropdownoptionstype");
            Ddo_parametrospln_linha_Titlecontrolidtoreplace = cgiGet( "DDO_PARAMETROSPLN_LINHA_Titlecontrolidtoreplace");
            Ddo_parametrospln_linha_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_LINHA_Includesortasc"));
            Ddo_parametrospln_linha_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_LINHA_Includesortdsc"));
            Ddo_parametrospln_linha_Sortedstatus = cgiGet( "DDO_PARAMETROSPLN_LINHA_Sortedstatus");
            Ddo_parametrospln_linha_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_LINHA_Includefilter"));
            Ddo_parametrospln_linha_Filtertype = cgiGet( "DDO_PARAMETROSPLN_LINHA_Filtertype");
            Ddo_parametrospln_linha_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_LINHA_Filterisrange"));
            Ddo_parametrospln_linha_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PARAMETROSPLN_LINHA_Includedatalist"));
            Ddo_parametrospln_linha_Sortasc = cgiGet( "DDO_PARAMETROSPLN_LINHA_Sortasc");
            Ddo_parametrospln_linha_Sortdsc = cgiGet( "DDO_PARAMETROSPLN_LINHA_Sortdsc");
            Ddo_parametrospln_linha_Cleanfilter = cgiGet( "DDO_PARAMETROSPLN_LINHA_Cleanfilter");
            Ddo_parametrospln_linha_Rangefilterfrom = cgiGet( "DDO_PARAMETROSPLN_LINHA_Rangefilterfrom");
            Ddo_parametrospln_linha_Rangefilterto = cgiGet( "DDO_PARAMETROSPLN_LINHA_Rangefilterto");
            Ddo_parametrospln_linha_Searchbuttontext = cgiGet( "DDO_PARAMETROSPLN_LINHA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_parametrospln_areatrabalhocod_Activeeventkey = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Activeeventkey");
            Ddo_parametrospln_areatrabalhocod_Filteredtext_get = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Filteredtext_get");
            Ddo_parametrospln_areatrabalhocod_Selectedvalue_get = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Selectedvalue_get");
            Ddo_parametrospln_areatrabalhocod_Selectedtext_get = cgiGet( "DDO_PARAMETROSPLN_AREATRABALHOCOD_Selectedtext_get");
            Ddo_parametrospln_aba_Activeeventkey = cgiGet( "DDO_PARAMETROSPLN_ABA_Activeeventkey");
            Ddo_parametrospln_aba_Filteredtext_get = cgiGet( "DDO_PARAMETROSPLN_ABA_Filteredtext_get");
            Ddo_parametrospln_aba_Selectedvalue_get = cgiGet( "DDO_PARAMETROSPLN_ABA_Selectedvalue_get");
            Ddo_parametrospln_campo_Activeeventkey = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Activeeventkey");
            Ddo_parametrospln_campo_Filteredtext_get = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Filteredtext_get");
            Ddo_parametrospln_campo_Selectedvalue_get = cgiGet( "DDO_PARAMETROSPLN_CAMPO_Selectedvalue_get");
            Ddo_parametrospln_coluna_Activeeventkey = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Activeeventkey");
            Ddo_parametrospln_coluna_Filteredtext_get = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Filteredtext_get");
            Ddo_parametrospln_coluna_Selectedvalue_get = cgiGet( "DDO_PARAMETROSPLN_COLUNA_Selectedvalue_get");
            Ddo_parametrospln_linha_Activeeventkey = cgiGet( "DDO_PARAMETROSPLN_LINHA_Activeeventkey");
            Ddo_parametrospln_linha_Filteredtext_get = cgiGet( "DDO_PARAMETROSPLN_LINHA_Filteredtext_get");
            Ddo_parametrospln_linha_Filteredtextto_get = cgiGet( "DDO_PARAMETROSPLN_LINHA_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPARAMETROSPLN_CAMPO1"), AV17ParametrosPln_Campo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPARAMETROSPLN_CAMPO2"), AV21ParametrosPln_Campo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPARAMETROSPLN_CAMPO3"), AV25ParametrosPln_Campo3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_AREATRABALHOCOD"), AV34TFParametrosPln_AreaTrabalhoCod) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSPLN_AREATRABALHOCOD_SEL"), ",", ".") != Convert.ToDecimal( AV53TFParametrosPln_AreaTrabalhoCod_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_ABA"), AV55TFParametrosPln_Aba) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_ABA_SEL"), AV56TFParametrosPln_Aba_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_CAMPO"), AV38TFParametrosPln_Campo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_CAMPO_SEL"), AV39TFParametrosPln_Campo_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_COLUNA"), AV42TFParametrosPln_Coluna) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPARAMETROSPLN_COLUNA_SEL"), AV43TFParametrosPln_Coluna_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSPLN_LINHA"), ",", ".") != Convert.ToDecimal( AV46TFParametrosPln_Linha )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPARAMETROSPLN_LINHA_TO"), ",", ".") != Convert.ToDecimal( AV47TFParametrosPln_Linha_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E28F02 */
         E28F02 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28F02( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "PARAMETROSPLN_CAMPO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "PARAMETROSPLN_CAMPO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "PARAMETROSPLN_CAMPO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfparametrospln_areatrabalhocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrospln_areatrabalhocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrospln_areatrabalhocod_Visible), 5, 0)));
         edtavTfparametrospln_areatrabalhocod_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrospln_areatrabalhocod_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrospln_areatrabalhocod_sel_Visible), 5, 0)));
         edtavTfparametrospln_areatrabalhocod_seldsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrospln_areatrabalhocod_seldsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrospln_areatrabalhocod_seldsc_Visible), 5, 0)));
         edtavTfparametrospln_aba_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrospln_aba_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrospln_aba_Visible), 5, 0)));
         edtavTfparametrospln_aba_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrospln_aba_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrospln_aba_sel_Visible), 5, 0)));
         edtavTfparametrospln_campo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrospln_campo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrospln_campo_Visible), 5, 0)));
         edtavTfparametrospln_campo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrospln_campo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrospln_campo_sel_Visible), 5, 0)));
         edtavTfparametrospln_coluna_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrospln_coluna_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrospln_coluna_Visible), 5, 0)));
         edtavTfparametrospln_coluna_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrospln_coluna_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrospln_coluna_sel_Visible), 5, 0)));
         edtavTfparametrospln_linha_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrospln_linha_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrospln_linha_Visible), 5, 0)));
         edtavTfparametrospln_linha_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfparametrospln_linha_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfparametrospln_linha_to_Visible), 5, 0)));
         Ddo_parametrospln_areatrabalhocod_Titlecontrolidtoreplace = subGrid_Internalname+"_ParametrosPln_AreaTrabalhoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_areatrabalhocod_Internalname, "TitleControlIdToReplace", Ddo_parametrospln_areatrabalhocod_Titlecontrolidtoreplace);
         AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace = Ddo_parametrospln_areatrabalhocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace", AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace);
         edtavDdo_parametrospln_areatrabalhocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_parametrospln_areatrabalhocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_parametrospln_areatrabalhocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_parametrospln_aba_Titlecontrolidtoreplace = subGrid_Internalname+"_ParametrosPln_Aba";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_aba_Internalname, "TitleControlIdToReplace", Ddo_parametrospln_aba_Titlecontrolidtoreplace);
         AV57ddo_ParametrosPln_AbaTitleControlIdToReplace = Ddo_parametrospln_aba_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ParametrosPln_AbaTitleControlIdToReplace", AV57ddo_ParametrosPln_AbaTitleControlIdToReplace);
         edtavDdo_parametrospln_abatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_parametrospln_abatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_parametrospln_abatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_parametrospln_campo_Titlecontrolidtoreplace = subGrid_Internalname+"_ParametrosPln_Campo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_campo_Internalname, "TitleControlIdToReplace", Ddo_parametrospln_campo_Titlecontrolidtoreplace);
         AV40ddo_ParametrosPln_CampoTitleControlIdToReplace = Ddo_parametrospln_campo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_ParametrosPln_CampoTitleControlIdToReplace", AV40ddo_ParametrosPln_CampoTitleControlIdToReplace);
         edtavDdo_parametrospln_campotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_parametrospln_campotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_parametrospln_campotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_parametrospln_coluna_Titlecontrolidtoreplace = subGrid_Internalname+"_ParametrosPln_Coluna";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_coluna_Internalname, "TitleControlIdToReplace", Ddo_parametrospln_coluna_Titlecontrolidtoreplace);
         AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace = Ddo_parametrospln_coluna_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace", AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace);
         edtavDdo_parametrospln_colunatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_parametrospln_colunatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_parametrospln_colunatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_parametrospln_linha_Titlecontrolidtoreplace = subGrid_Internalname+"_ParametrosPln_Linha";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_linha_Internalname, "TitleControlIdToReplace", Ddo_parametrospln_linha_Titlecontrolidtoreplace);
         AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace = Ddo_parametrospln_linha_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace", AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace);
         edtavDdo_parametrospln_linhatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_parametrospln_linhatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_parametrospln_linhatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Par�metros das Planilhas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "�rea de Trabalho", 0);
         cmbavOrderedby.addItem("2", "Aba", 0);
         cmbavOrderedby.addItem("3", "Campo", 0);
         cmbavOrderedby.addItem("4", "Coluna", 0);
         cmbavOrderedby.addItem("5", "Linha", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV49DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV49DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E29F02( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV33ParametrosPln_AreaTrabalhoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54ParametrosPln_AbaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37ParametrosPln_CampoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41ParametrosPln_ColunaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45ParametrosPln_LinhaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         dynParametrosPln_AreaTrabalhoCod_Titleformat = 2;
         dynParametrosPln_AreaTrabalhoCod.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "�rea de Trabalho", AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynParametrosPln_AreaTrabalhoCod_Internalname, "Title", dynParametrosPln_AreaTrabalhoCod.Title.Text);
         edtParametrosPln_Aba_Titleformat = 2;
         edtParametrosPln_Aba_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Aba", AV57ddo_ParametrosPln_AbaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosPln_Aba_Internalname, "Title", edtParametrosPln_Aba_Title);
         edtParametrosPln_Campo_Titleformat = 2;
         edtParametrosPln_Campo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Campo", AV40ddo_ParametrosPln_CampoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosPln_Campo_Internalname, "Title", edtParametrosPln_Campo_Title);
         edtParametrosPln_Coluna_Titleformat = 2;
         edtParametrosPln_Coluna_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Coluna", AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosPln_Coluna_Internalname, "Title", edtParametrosPln_Coluna_Title);
         edtParametrosPln_Linha_Titleformat = 2;
         edtParametrosPln_Linha_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Linha", AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosPln_Linha_Internalname, "Title", edtParametrosPln_Linha_Title);
         AV51GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51GridCurrentPage), 10, 0)));
         AV52GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52GridPageCount), 10, 0)));
         AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 = AV17ParametrosPln_Campo1;
         AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 = AV21ParametrosPln_Campo2;
         AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 = AV25ParametrosPln_Campo3;
         AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = AV34TFParametrosPln_AreaTrabalhoCod;
         AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel = AV53TFParametrosPln_AreaTrabalhoCod_Sel;
         AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba = AV55TFParametrosPln_Aba;
         AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel = AV56TFParametrosPln_Aba_Sel;
         AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo = AV38TFParametrosPln_Campo;
         AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel = AV39TFParametrosPln_Campo_Sel;
         AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = AV42TFParametrosPln_Coluna;
         AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel = AV43TFParametrosPln_Coluna_Sel;
         AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha = AV46TFParametrosPln_Linha;
         AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to = AV47TFParametrosPln_Linha_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33ParametrosPln_AreaTrabalhoCodTitleFilterData", AV33ParametrosPln_AreaTrabalhoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54ParametrosPln_AbaTitleFilterData", AV54ParametrosPln_AbaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37ParametrosPln_CampoTitleFilterData", AV37ParametrosPln_CampoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV41ParametrosPln_ColunaTitleFilterData", AV41ParametrosPln_ColunaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45ParametrosPln_LinhaTitleFilterData", AV45ParametrosPln_LinhaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11F02( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV50PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV50PageToGo) ;
         }
      }

      protected void E12F02( )
      {
         /* Ddo_parametrospln_areatrabalhocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_parametrospln_areatrabalhocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrospln_areatrabalhocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_areatrabalhocod_Internalname, "SortedStatus", Ddo_parametrospln_areatrabalhocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrospln_areatrabalhocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrospln_areatrabalhocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_areatrabalhocod_Internalname, "SortedStatus", Ddo_parametrospln_areatrabalhocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrospln_areatrabalhocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFParametrosPln_AreaTrabalhoCod = Ddo_parametrospln_areatrabalhocod_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFParametrosPln_AreaTrabalhoCod", AV34TFParametrosPln_AreaTrabalhoCod);
            AV53TFParametrosPln_AreaTrabalhoCod_Sel = (int)(NumberUtil.Val( Ddo_parametrospln_areatrabalhocod_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFParametrosPln_AreaTrabalhoCod_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFParametrosPln_AreaTrabalhoCod_Sel), 6, 0)));
            AV58TFParametrosPln_AreaTrabalhoCod_SelDsc = Ddo_parametrospln_areatrabalhocod_Selectedtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFParametrosPln_AreaTrabalhoCod_SelDsc", AV58TFParametrosPln_AreaTrabalhoCod_SelDsc);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13F02( )
      {
         /* Ddo_parametrospln_aba_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_parametrospln_aba_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrospln_aba_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_aba_Internalname, "SortedStatus", Ddo_parametrospln_aba_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrospln_aba_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrospln_aba_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_aba_Internalname, "SortedStatus", Ddo_parametrospln_aba_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrospln_aba_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFParametrosPln_Aba = Ddo_parametrospln_aba_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFParametrosPln_Aba", AV55TFParametrosPln_Aba);
            AV56TFParametrosPln_Aba_Sel = Ddo_parametrospln_aba_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFParametrosPln_Aba_Sel", AV56TFParametrosPln_Aba_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14F02( )
      {
         /* Ddo_parametrospln_campo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_parametrospln_campo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrospln_campo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_campo_Internalname, "SortedStatus", Ddo_parametrospln_campo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrospln_campo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrospln_campo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_campo_Internalname, "SortedStatus", Ddo_parametrospln_campo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrospln_campo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFParametrosPln_Campo = Ddo_parametrospln_campo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFParametrosPln_Campo", AV38TFParametrosPln_Campo);
            AV39TFParametrosPln_Campo_Sel = Ddo_parametrospln_campo_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFParametrosPln_Campo_Sel", AV39TFParametrosPln_Campo_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15F02( )
      {
         /* Ddo_parametrospln_coluna_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_parametrospln_coluna_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrospln_coluna_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_coluna_Internalname, "SortedStatus", Ddo_parametrospln_coluna_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrospln_coluna_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrospln_coluna_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_coluna_Internalname, "SortedStatus", Ddo_parametrospln_coluna_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrospln_coluna_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV42TFParametrosPln_Coluna = Ddo_parametrospln_coluna_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFParametrosPln_Coluna", AV42TFParametrosPln_Coluna);
            AV43TFParametrosPln_Coluna_Sel = Ddo_parametrospln_coluna_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFParametrosPln_Coluna_Sel", AV43TFParametrosPln_Coluna_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16F02( )
      {
         /* Ddo_parametrospln_linha_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_parametrospln_linha_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrospln_linha_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_linha_Internalname, "SortedStatus", Ddo_parametrospln_linha_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrospln_linha_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_parametrospln_linha_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_linha_Internalname, "SortedStatus", Ddo_parametrospln_linha_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_parametrospln_linha_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFParametrosPln_Linha = (int)(NumberUtil.Val( Ddo_parametrospln_linha_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFParametrosPln_Linha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFParametrosPln_Linha), 6, 0)));
            AV47TFParametrosPln_Linha_To = (int)(NumberUtil.Val( Ddo_parametrospln_linha_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFParametrosPln_Linha_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFParametrosPln_Linha_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E30F02( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV79Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("parametrosplanilhas.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A848ParametrosPln_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV80Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("parametrosplanilhas.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A848ParametrosPln_Codigo);
         edtParametrosPln_Campo_Link = formatLink("viewparametrosplanilhas.aspx") + "?" + UrlEncode("" +A848ParametrosPln_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 73;
         }
         sendrow_732( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(73, GridRow);
         }
      }

      protected void E17F02( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E23F02( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E18F02( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ParametrosPln_Campo1, AV19DynamicFiltersSelector2, AV21ParametrosPln_Campo2, AV23DynamicFiltersSelector3, AV25ParametrosPln_Campo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFParametrosPln_AreaTrabalhoCod, AV53TFParametrosPln_AreaTrabalhoCod_Sel, AV55TFParametrosPln_Aba, AV56TFParametrosPln_Aba_Sel, AV38TFParametrosPln_Campo, AV39TFParametrosPln_Campo_Sel, AV42TFParametrosPln_Coluna, AV43TFParametrosPln_Coluna_Sel, AV46TFParametrosPln_Linha, AV47TFParametrosPln_Linha_To, AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace, AV57ddo_ParametrosPln_AbaTitleControlIdToReplace, AV40ddo_ParametrosPln_CampoTitleControlIdToReplace, AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace, AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace, AV81Pgmname, AV58TFParametrosPln_AreaTrabalhoCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A848ParametrosPln_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E24F02( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25F02( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E19F02( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ParametrosPln_Campo1, AV19DynamicFiltersSelector2, AV21ParametrosPln_Campo2, AV23DynamicFiltersSelector3, AV25ParametrosPln_Campo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFParametrosPln_AreaTrabalhoCod, AV53TFParametrosPln_AreaTrabalhoCod_Sel, AV55TFParametrosPln_Aba, AV56TFParametrosPln_Aba_Sel, AV38TFParametrosPln_Campo, AV39TFParametrosPln_Campo_Sel, AV42TFParametrosPln_Coluna, AV43TFParametrosPln_Coluna_Sel, AV46TFParametrosPln_Linha, AV47TFParametrosPln_Linha_To, AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace, AV57ddo_ParametrosPln_AbaTitleControlIdToReplace, AV40ddo_ParametrosPln_CampoTitleControlIdToReplace, AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace, AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace, AV81Pgmname, AV58TFParametrosPln_AreaTrabalhoCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A848ParametrosPln_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E26F02( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20F02( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ParametrosPln_Campo1, AV19DynamicFiltersSelector2, AV21ParametrosPln_Campo2, AV23DynamicFiltersSelector3, AV25ParametrosPln_Campo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFParametrosPln_AreaTrabalhoCod, AV53TFParametrosPln_AreaTrabalhoCod_Sel, AV55TFParametrosPln_Aba, AV56TFParametrosPln_Aba_Sel, AV38TFParametrosPln_Campo, AV39TFParametrosPln_Campo_Sel, AV42TFParametrosPln_Coluna, AV43TFParametrosPln_Coluna_Sel, AV46TFParametrosPln_Linha, AV47TFParametrosPln_Linha_To, AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace, AV57ddo_ParametrosPln_AbaTitleControlIdToReplace, AV40ddo_ParametrosPln_CampoTitleControlIdToReplace, AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace, AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace, AV81Pgmname, AV58TFParametrosPln_AreaTrabalhoCod_SelDsc, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A848ParametrosPln_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E27F02( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21F02( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E22F02( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("parametrosplanilhas.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_parametrospln_areatrabalhocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_areatrabalhocod_Internalname, "SortedStatus", Ddo_parametrospln_areatrabalhocod_Sortedstatus);
         Ddo_parametrospln_aba_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_aba_Internalname, "SortedStatus", Ddo_parametrospln_aba_Sortedstatus);
         Ddo_parametrospln_campo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_campo_Internalname, "SortedStatus", Ddo_parametrospln_campo_Sortedstatus);
         Ddo_parametrospln_coluna_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_coluna_Internalname, "SortedStatus", Ddo_parametrospln_coluna_Sortedstatus);
         Ddo_parametrospln_linha_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_linha_Internalname, "SortedStatus", Ddo_parametrospln_linha_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_parametrospln_areatrabalhocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_areatrabalhocod_Internalname, "SortedStatus", Ddo_parametrospln_areatrabalhocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_parametrospln_aba_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_aba_Internalname, "SortedStatus", Ddo_parametrospln_aba_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_parametrospln_campo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_campo_Internalname, "SortedStatus", Ddo_parametrospln_campo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_parametrospln_coluna_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_coluna_Internalname, "SortedStatus", Ddo_parametrospln_coluna_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_parametrospln_linha_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_linha_Internalname, "SortedStatus", Ddo_parametrospln_linha_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavParametrospln_campo1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavParametrospln_campo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavParametrospln_campo1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PARAMETROSPLN_CAMPO") == 0 )
         {
            edtavParametrospln_campo1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavParametrospln_campo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavParametrospln_campo1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavParametrospln_campo2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavParametrospln_campo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavParametrospln_campo2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PARAMETROSPLN_CAMPO") == 0 )
         {
            edtavParametrospln_campo2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavParametrospln_campo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavParametrospln_campo2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavParametrospln_campo3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavParametrospln_campo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavParametrospln_campo3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PARAMETROSPLN_CAMPO") == 0 )
         {
            edtavParametrospln_campo3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavParametrospln_campo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavParametrospln_campo3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "PARAMETROSPLN_CAMPO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV21ParametrosPln_Campo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ParametrosPln_Campo2", AV21ParametrosPln_Campo2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "PARAMETROSPLN_CAMPO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV25ParametrosPln_Campo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ParametrosPln_Campo3", AV25ParametrosPln_Campo3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV34TFParametrosPln_AreaTrabalhoCod = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFParametrosPln_AreaTrabalhoCod", AV34TFParametrosPln_AreaTrabalhoCod);
         Ddo_parametrospln_areatrabalhocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_areatrabalhocod_Internalname, "FilteredText_set", Ddo_parametrospln_areatrabalhocod_Filteredtext_set);
         AV53TFParametrosPln_AreaTrabalhoCod_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFParametrosPln_AreaTrabalhoCod_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFParametrosPln_AreaTrabalhoCod_Sel), 6, 0)));
         Ddo_parametrospln_areatrabalhocod_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_areatrabalhocod_Internalname, "SelectedValue_set", Ddo_parametrospln_areatrabalhocod_Selectedvalue_set);
         AV55TFParametrosPln_Aba = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFParametrosPln_Aba", AV55TFParametrosPln_Aba);
         Ddo_parametrospln_aba_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_aba_Internalname, "FilteredText_set", Ddo_parametrospln_aba_Filteredtext_set);
         AV56TFParametrosPln_Aba_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFParametrosPln_Aba_Sel", AV56TFParametrosPln_Aba_Sel);
         Ddo_parametrospln_aba_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_aba_Internalname, "SelectedValue_set", Ddo_parametrospln_aba_Selectedvalue_set);
         AV38TFParametrosPln_Campo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFParametrosPln_Campo", AV38TFParametrosPln_Campo);
         Ddo_parametrospln_campo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_campo_Internalname, "FilteredText_set", Ddo_parametrospln_campo_Filteredtext_set);
         AV39TFParametrosPln_Campo_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFParametrosPln_Campo_Sel", AV39TFParametrosPln_Campo_Sel);
         Ddo_parametrospln_campo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_campo_Internalname, "SelectedValue_set", Ddo_parametrospln_campo_Selectedvalue_set);
         AV42TFParametrosPln_Coluna = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFParametrosPln_Coluna", AV42TFParametrosPln_Coluna);
         Ddo_parametrospln_coluna_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_coluna_Internalname, "FilteredText_set", Ddo_parametrospln_coluna_Filteredtext_set);
         AV43TFParametrosPln_Coluna_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFParametrosPln_Coluna_Sel", AV43TFParametrosPln_Coluna_Sel);
         Ddo_parametrospln_coluna_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_coluna_Internalname, "SelectedValue_set", Ddo_parametrospln_coluna_Selectedvalue_set);
         AV46TFParametrosPln_Linha = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFParametrosPln_Linha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFParametrosPln_Linha), 6, 0)));
         Ddo_parametrospln_linha_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_linha_Internalname, "FilteredText_set", Ddo_parametrospln_linha_Filteredtext_set);
         AV47TFParametrosPln_Linha_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFParametrosPln_Linha_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFParametrosPln_Linha_To), 6, 0)));
         Ddo_parametrospln_linha_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_linha_Internalname, "FilteredTextTo_set", Ddo_parametrospln_linha_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "PARAMETROSPLN_CAMPO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17ParametrosPln_Campo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ParametrosPln_Campo1", AV17ParametrosPln_Campo1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV81Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV81Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV81Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV82GXV1 = 1;
         while ( AV82GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV82GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_AREATRABALHOCOD") == 0 )
            {
               AV34TFParametrosPln_AreaTrabalhoCod = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFParametrosPln_AreaTrabalhoCod", AV34TFParametrosPln_AreaTrabalhoCod);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFParametrosPln_AreaTrabalhoCod)) )
               {
                  Ddo_parametrospln_areatrabalhocod_Filteredtext_set = AV34TFParametrosPln_AreaTrabalhoCod;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_areatrabalhocod_Internalname, "FilteredText_set", Ddo_parametrospln_areatrabalhocod_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_AREATRABALHOCOD_SEL") == 0 )
            {
               AV53TFParametrosPln_AreaTrabalhoCod_Sel = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFParametrosPln_AreaTrabalhoCod_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFParametrosPln_AreaTrabalhoCod_Sel), 6, 0)));
               if ( ! (0==AV53TFParametrosPln_AreaTrabalhoCod_Sel) )
               {
                  Ddo_parametrospln_areatrabalhocod_Selectedvalue_set = StringUtil.Str( (decimal)(AV53TFParametrosPln_AreaTrabalhoCod_Sel), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_areatrabalhocod_Internalname, "SelectedValue_set", Ddo_parametrospln_areatrabalhocod_Selectedvalue_set);
                  AV58TFParametrosPln_AreaTrabalhoCod_SelDsc = AV11GridStateFilterValue.gxTpr_Valueto;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFParametrosPln_AreaTrabalhoCod_SelDsc", AV58TFParametrosPln_AreaTrabalhoCod_SelDsc);
                  Ddo_parametrospln_areatrabalhocod_Selectedtext_set = AV58TFParametrosPln_AreaTrabalhoCod_SelDsc;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_areatrabalhocod_Internalname, "SelectedText_set", Ddo_parametrospln_areatrabalhocod_Selectedtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_ABA") == 0 )
            {
               AV55TFParametrosPln_Aba = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFParametrosPln_Aba", AV55TFParametrosPln_Aba);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFParametrosPln_Aba)) )
               {
                  Ddo_parametrospln_aba_Filteredtext_set = AV55TFParametrosPln_Aba;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_aba_Internalname, "FilteredText_set", Ddo_parametrospln_aba_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_ABA_SEL") == 0 )
            {
               AV56TFParametrosPln_Aba_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFParametrosPln_Aba_Sel", AV56TFParametrosPln_Aba_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFParametrosPln_Aba_Sel)) )
               {
                  Ddo_parametrospln_aba_Selectedvalue_set = AV56TFParametrosPln_Aba_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_aba_Internalname, "SelectedValue_set", Ddo_parametrospln_aba_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_CAMPO") == 0 )
            {
               AV38TFParametrosPln_Campo = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFParametrosPln_Campo", AV38TFParametrosPln_Campo);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFParametrosPln_Campo)) )
               {
                  Ddo_parametrospln_campo_Filteredtext_set = AV38TFParametrosPln_Campo;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_campo_Internalname, "FilteredText_set", Ddo_parametrospln_campo_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_CAMPO_SEL") == 0 )
            {
               AV39TFParametrosPln_Campo_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFParametrosPln_Campo_Sel", AV39TFParametrosPln_Campo_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFParametrosPln_Campo_Sel)) )
               {
                  Ddo_parametrospln_campo_Selectedvalue_set = AV39TFParametrosPln_Campo_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_campo_Internalname, "SelectedValue_set", Ddo_parametrospln_campo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_COLUNA") == 0 )
            {
               AV42TFParametrosPln_Coluna = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFParametrosPln_Coluna", AV42TFParametrosPln_Coluna);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFParametrosPln_Coluna)) )
               {
                  Ddo_parametrospln_coluna_Filteredtext_set = AV42TFParametrosPln_Coluna;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_coluna_Internalname, "FilteredText_set", Ddo_parametrospln_coluna_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_COLUNA_SEL") == 0 )
            {
               AV43TFParametrosPln_Coluna_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFParametrosPln_Coluna_Sel", AV43TFParametrosPln_Coluna_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFParametrosPln_Coluna_Sel)) )
               {
                  Ddo_parametrospln_coluna_Selectedvalue_set = AV43TFParametrosPln_Coluna_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_coluna_Internalname, "SelectedValue_set", Ddo_parametrospln_coluna_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_LINHA") == 0 )
            {
               AV46TFParametrosPln_Linha = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFParametrosPln_Linha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFParametrosPln_Linha), 6, 0)));
               AV47TFParametrosPln_Linha_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFParametrosPln_Linha_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFParametrosPln_Linha_To), 6, 0)));
               if ( ! (0==AV46TFParametrosPln_Linha) )
               {
                  Ddo_parametrospln_linha_Filteredtext_set = StringUtil.Str( (decimal)(AV46TFParametrosPln_Linha), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_linha_Internalname, "FilteredText_set", Ddo_parametrospln_linha_Filteredtext_set);
               }
               if ( ! (0==AV47TFParametrosPln_Linha_To) )
               {
                  Ddo_parametrospln_linha_Filteredtextto_set = StringUtil.Str( (decimal)(AV47TFParametrosPln_Linha_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_parametrospln_linha_Internalname, "FilteredTextTo_set", Ddo_parametrospln_linha_Filteredtextto_set);
               }
            }
            AV82GXV1 = (int)(AV82GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PARAMETROSPLN_CAMPO") == 0 )
            {
               AV17ParametrosPln_Campo1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ParametrosPln_Campo1", AV17ParametrosPln_Campo1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PARAMETROSPLN_CAMPO") == 0 )
               {
                  AV21ParametrosPln_Campo2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ParametrosPln_Campo2", AV21ParametrosPln_Campo2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PARAMETROSPLN_CAMPO") == 0 )
                  {
                     AV25ParametrosPln_Campo3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ParametrosPln_Campo3", AV25ParametrosPln_Campo3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV81Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFParametrosPln_AreaTrabalhoCod)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSPLN_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = AV34TFParametrosPln_AreaTrabalhoCod;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV53TFParametrosPln_AreaTrabalhoCod_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSPLN_AREATRABALHOCOD_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV53TFParametrosPln_AreaTrabalhoCod_Sel), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = AV58TFParametrosPln_AreaTrabalhoCod_SelDsc;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFParametrosPln_Aba)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSPLN_ABA";
            AV11GridStateFilterValue.gxTpr_Value = AV55TFParametrosPln_Aba;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFParametrosPln_Aba_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSPLN_ABA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV56TFParametrosPln_Aba_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFParametrosPln_Campo)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSPLN_CAMPO";
            AV11GridStateFilterValue.gxTpr_Value = AV38TFParametrosPln_Campo;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFParametrosPln_Campo_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSPLN_CAMPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFParametrosPln_Campo_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFParametrosPln_Coluna)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSPLN_COLUNA";
            AV11GridStateFilterValue.gxTpr_Value = AV42TFParametrosPln_Coluna;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFParametrosPln_Coluna_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSPLN_COLUNA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV43TFParametrosPln_Coluna_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV46TFParametrosPln_Linha) && (0==AV47TFParametrosPln_Linha_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPARAMETROSPLN_LINHA";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV46TFParametrosPln_Linha), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV47TFParametrosPln_Linha_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV81Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PARAMETROSPLN_CAMPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ParametrosPln_Campo1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17ParametrosPln_Campo1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PARAMETROSPLN_CAMPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ParametrosPln_Campo2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21ParametrosPln_Campo2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PARAMETROSPLN_CAMPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ParametrosPln_Campo3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ParametrosPln_Campo3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV81Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ParametrosPlanilhas";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_F02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_F02( true) ;
         }
         else
         {
            wb_table2_8_F02( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_F02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_67_F02( true) ;
         }
         else
         {
            wb_table3_67_F02( false) ;
         }
         return  ;
      }

      protected void wb_table3_67_F02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_F02e( true) ;
         }
         else
         {
            wb_table1_2_F02e( false) ;
         }
      }

      protected void wb_table3_67_F02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_70_F02( true) ;
         }
         else
         {
            wb_table4_70_F02( false) ;
         }
         return  ;
      }

      protected void wb_table4_70_F02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_67_F02e( true) ;
         }
         else
         {
            wb_table3_67_F02e( false) ;
         }
      }

      protected void wb_table4_70_F02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"73\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( dynParametrosPln_AreaTrabalhoCod_Titleformat == 0 )
               {
                  context.SendWebValue( dynParametrosPln_AreaTrabalhoCod.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( dynParametrosPln_AreaTrabalhoCod.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtParametrosPln_Aba_Titleformat == 0 )
               {
                  context.SendWebValue( edtParametrosPln_Aba_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtParametrosPln_Aba_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtParametrosPln_Campo_Titleformat == 0 )
               {
                  context.SendWebValue( edtParametrosPln_Campo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtParametrosPln_Campo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(43), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtParametrosPln_Coluna_Titleformat == 0 )
               {
                  context.SendWebValue( edtParametrosPln_Coluna_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtParametrosPln_Coluna_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtParametrosPln_Linha_Titleformat == 0 )
               {
                  context.SendWebValue( edtParametrosPln_Linha_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtParametrosPln_Linha_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A848ParametrosPln_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( dynParametrosPln_AreaTrabalhoCod.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynParametrosPln_AreaTrabalhoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2015ParametrosPln_Aba));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtParametrosPln_Aba_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtParametrosPln_Aba_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A849ParametrosPln_Campo);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtParametrosPln_Campo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtParametrosPln_Campo_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtParametrosPln_Campo_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A850ParametrosPln_Coluna));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtParametrosPln_Coluna_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtParametrosPln_Coluna_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A851ParametrosPln_Linha), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtParametrosPln_Linha_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtParametrosPln_Linha_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 73 )
         {
            wbEnd = 0;
            nRC_GXsfl_73 = (short)(nGXsfl_73_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_70_F02e( true) ;
         }
         else
         {
            wb_table4_70_F02e( false) ;
         }
      }

      protected void wb_table2_8_F02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblParametrosplanilhastitle_Internalname, "Par�metros das Planilhas", "", "", lblParametrosplanilhastitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_F02( true) ;
         }
         else
         {
            wb_table5_13_F02( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_F02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWParametrosPlanilhas.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_F02( true) ;
         }
         else
         {
            wb_table6_23_F02( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_F02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_F02e( true) ;
         }
         else
         {
            wb_table2_8_F02e( false) ;
         }
      }

      protected void wb_table6_23_F02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_F02( true) ;
         }
         else
         {
            wb_table7_28_F02( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_F02e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_F02e( true) ;
         }
         else
         {
            wb_table6_23_F02e( false) ;
         }
      }

      protected void wb_table7_28_F02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWParametrosPlanilhas.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavParametrospln_campo1_Internalname, AV17ParametrosPln_Campo1, StringUtil.RTrim( context.localUtil.Format( AV17ParametrosPln_Campo1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavParametrospln_campo1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavParametrospln_campo1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWParametrosPlanilhas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_WWParametrosPlanilhas.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavParametrospln_campo2_Internalname, AV21ParametrosPln_Campo2, StringUtil.RTrim( context.localUtil.Format( AV21ParametrosPln_Campo2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavParametrospln_campo2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavParametrospln_campo2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWParametrosPlanilhas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWParametrosPlanilhas.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavParametrospln_campo3_Internalname, AV25ParametrosPln_Campo3, StringUtil.RTrim( context.localUtil.Format( AV25ParametrosPln_Campo3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavParametrospln_campo3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavParametrospln_campo3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_F02e( true) ;
         }
         else
         {
            wb_table7_28_F02e( false) ;
         }
      }

      protected void wb_table5_13_F02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_F02e( true) ;
         }
         else
         {
            wb_table5_13_F02e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAF02( ) ;
         WSF02( ) ;
         WEF02( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203118513485");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwparametrosplanilhas.js", "?20203118513485");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_732( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_73_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_73_idx;
         edtParametrosPln_Codigo_Internalname = "PARAMETROSPLN_CODIGO_"+sGXsfl_73_idx;
         dynParametrosPln_AreaTrabalhoCod_Internalname = "PARAMETROSPLN_AREATRABALHOCOD_"+sGXsfl_73_idx;
         edtParametrosPln_Aba_Internalname = "PARAMETROSPLN_ABA_"+sGXsfl_73_idx;
         edtParametrosPln_Campo_Internalname = "PARAMETROSPLN_CAMPO_"+sGXsfl_73_idx;
         edtParametrosPln_Coluna_Internalname = "PARAMETROSPLN_COLUNA_"+sGXsfl_73_idx;
         edtParametrosPln_Linha_Internalname = "PARAMETROSPLN_LINHA_"+sGXsfl_73_idx;
      }

      protected void SubsflControlProps_fel_732( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_73_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_73_fel_idx;
         edtParametrosPln_Codigo_Internalname = "PARAMETROSPLN_CODIGO_"+sGXsfl_73_fel_idx;
         dynParametrosPln_AreaTrabalhoCod_Internalname = "PARAMETROSPLN_AREATRABALHOCOD_"+sGXsfl_73_fel_idx;
         edtParametrosPln_Aba_Internalname = "PARAMETROSPLN_ABA_"+sGXsfl_73_fel_idx;
         edtParametrosPln_Campo_Internalname = "PARAMETROSPLN_CAMPO_"+sGXsfl_73_fel_idx;
         edtParametrosPln_Coluna_Internalname = "PARAMETROSPLN_COLUNA_"+sGXsfl_73_fel_idx;
         edtParametrosPln_Linha_Internalname = "PARAMETROSPLN_LINHA_"+sGXsfl_73_fel_idx;
      }

      protected void sendrow_732( )
      {
         SubsflControlProps_732( ) ;
         WBF00( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_73_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_73_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_73_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV79Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV79Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV80Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV80Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtParametrosPln_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A848ParametrosPln_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A848ParametrosPln_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtParametrosPln_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            GXAPARAMETROSPLN_AREATRABALHOCOD_htmlF02( ) ;
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_73_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "PARAMETROSPLN_AREATRABALHOCOD_" + sGXsfl_73_idx;
               dynParametrosPln_AreaTrabalhoCod.Name = GXCCtl;
               dynParametrosPln_AreaTrabalhoCod.WebTags = "";
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynParametrosPln_AreaTrabalhoCod,(String)dynParametrosPln_AreaTrabalhoCod_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)),(short)1,(String)dynParametrosPln_AreaTrabalhoCod_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            dynParametrosPln_AreaTrabalhoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynParametrosPln_AreaTrabalhoCod_Internalname, "Values", (String)(dynParametrosPln_AreaTrabalhoCod.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtParametrosPln_Aba_Internalname,StringUtil.RTrim( A2015ParametrosPln_Aba),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtParametrosPln_Aba_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtParametrosPln_Campo_Internalname,(String)A849ParametrosPln_Campo,StringUtil.RTrim( context.localUtil.Format( A849ParametrosPln_Campo, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtParametrosPln_Campo_Link,(String)"",(String)"",(String)"",(String)edtParametrosPln_Campo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtParametrosPln_Coluna_Internalname,StringUtil.RTrim( A850ParametrosPln_Coluna),StringUtil.RTrim( context.localUtil.Format( A850ParametrosPln_Coluna, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtParametrosPln_Coluna_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)43,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtParametrosPln_Linha_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A851ParametrosPln_Linha), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A851ParametrosPln_Linha), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtParametrosPln_Linha_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSPLN_CODIGO"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, context.localUtil.Format( (decimal)(A848ParametrosPln_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSPLN_AREATRABALHOCOD"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, context.localUtil.Format( (decimal)(A847ParametrosPln_AreaTrabalhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSPLN_ABA"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, StringUtil.RTrim( context.localUtil.Format( A2015ParametrosPln_Aba, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSPLN_CAMPO"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, StringUtil.RTrim( context.localUtil.Format( A849ParametrosPln_Campo, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSPLN_COLUNA"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, StringUtil.RTrim( context.localUtil.Format( A850ParametrosPln_Coluna, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSPLN_LINHA"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, context.localUtil.Format( (decimal)(A851ParametrosPln_Linha), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_73_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_732( ) ;
         }
         /* End function sendrow_732 */
      }

      protected void init_default_properties( )
      {
         lblParametrosplanilhastitle_Internalname = "PARAMETROSPLANILHASTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavParametrospln_campo1_Internalname = "vPARAMETROSPLN_CAMPO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavParametrospln_campo2_Internalname = "vPARAMETROSPLN_CAMPO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavParametrospln_campo3_Internalname = "vPARAMETROSPLN_CAMPO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtParametrosPln_Codigo_Internalname = "PARAMETROSPLN_CODIGO";
         dynParametrosPln_AreaTrabalhoCod_Internalname = "PARAMETROSPLN_AREATRABALHOCOD";
         edtParametrosPln_Aba_Internalname = "PARAMETROSPLN_ABA";
         edtParametrosPln_Campo_Internalname = "PARAMETROSPLN_CAMPO";
         edtParametrosPln_Coluna_Internalname = "PARAMETROSPLN_COLUNA";
         edtParametrosPln_Linha_Internalname = "PARAMETROSPLN_LINHA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfparametrospln_areatrabalhocod_Internalname = "vTFPARAMETROSPLN_AREATRABALHOCOD";
         edtavTfparametrospln_areatrabalhocod_sel_Internalname = "vTFPARAMETROSPLN_AREATRABALHOCOD_SEL";
         edtavTfparametrospln_areatrabalhocod_seldsc_Internalname = "vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC";
         edtavTfparametrospln_aba_Internalname = "vTFPARAMETROSPLN_ABA";
         edtavTfparametrospln_aba_sel_Internalname = "vTFPARAMETROSPLN_ABA_SEL";
         edtavTfparametrospln_campo_Internalname = "vTFPARAMETROSPLN_CAMPO";
         edtavTfparametrospln_campo_sel_Internalname = "vTFPARAMETROSPLN_CAMPO_SEL";
         edtavTfparametrospln_coluna_Internalname = "vTFPARAMETROSPLN_COLUNA";
         edtavTfparametrospln_coluna_sel_Internalname = "vTFPARAMETROSPLN_COLUNA_SEL";
         edtavTfparametrospln_linha_Internalname = "vTFPARAMETROSPLN_LINHA";
         edtavTfparametrospln_linha_to_Internalname = "vTFPARAMETROSPLN_LINHA_TO";
         Ddo_parametrospln_areatrabalhocod_Internalname = "DDO_PARAMETROSPLN_AREATRABALHOCOD";
         edtavDdo_parametrospln_areatrabalhocodtitlecontrolidtoreplace_Internalname = "vDDO_PARAMETROSPLN_AREATRABALHOCODTITLECONTROLIDTOREPLACE";
         Ddo_parametrospln_aba_Internalname = "DDO_PARAMETROSPLN_ABA";
         edtavDdo_parametrospln_abatitlecontrolidtoreplace_Internalname = "vDDO_PARAMETROSPLN_ABATITLECONTROLIDTOREPLACE";
         Ddo_parametrospln_campo_Internalname = "DDO_PARAMETROSPLN_CAMPO";
         edtavDdo_parametrospln_campotitlecontrolidtoreplace_Internalname = "vDDO_PARAMETROSPLN_CAMPOTITLECONTROLIDTOREPLACE";
         Ddo_parametrospln_coluna_Internalname = "DDO_PARAMETROSPLN_COLUNA";
         edtavDdo_parametrospln_colunatitlecontrolidtoreplace_Internalname = "vDDO_PARAMETROSPLN_COLUNATITLECONTROLIDTOREPLACE";
         Ddo_parametrospln_linha_Internalname = "DDO_PARAMETROSPLN_LINHA";
         edtavDdo_parametrospln_linhatitlecontrolidtoreplace_Internalname = "vDDO_PARAMETROSPLN_LINHATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtParametrosPln_Linha_Jsonclick = "";
         edtParametrosPln_Coluna_Jsonclick = "";
         edtParametrosPln_Campo_Jsonclick = "";
         edtParametrosPln_Aba_Jsonclick = "";
         dynParametrosPln_AreaTrabalhoCod_Jsonclick = "";
         edtParametrosPln_Codigo_Jsonclick = "";
         edtavParametrospln_campo3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavParametrospln_campo2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavParametrospln_campo1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtParametrosPln_Campo_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtParametrosPln_Linha_Titleformat = 0;
         edtParametrosPln_Coluna_Titleformat = 0;
         edtParametrosPln_Campo_Titleformat = 0;
         edtParametrosPln_Aba_Titleformat = 0;
         dynParametrosPln_AreaTrabalhoCod_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavParametrospln_campo3_Visible = 1;
         edtavParametrospln_campo2_Visible = 1;
         edtavParametrospln_campo1_Visible = 1;
         edtParametrosPln_Linha_Title = "Linha";
         edtParametrosPln_Coluna_Title = "Coluna";
         edtParametrosPln_Campo_Title = "Campo";
         edtParametrosPln_Aba_Title = "Aba";
         dynParametrosPln_AreaTrabalhoCod.Title.Text = "�rea de Trabalho";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_parametrospln_linhatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_parametrospln_colunatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_parametrospln_campotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_parametrospln_abatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_parametrospln_areatrabalhocodtitlecontrolidtoreplace_Visible = 1;
         edtavTfparametrospln_linha_to_Jsonclick = "";
         edtavTfparametrospln_linha_to_Visible = 1;
         edtavTfparametrospln_linha_Jsonclick = "";
         edtavTfparametrospln_linha_Visible = 1;
         edtavTfparametrospln_coluna_sel_Jsonclick = "";
         edtavTfparametrospln_coluna_sel_Visible = 1;
         edtavTfparametrospln_coluna_Jsonclick = "";
         edtavTfparametrospln_coluna_Visible = 1;
         edtavTfparametrospln_campo_sel_Jsonclick = "";
         edtavTfparametrospln_campo_sel_Visible = 1;
         edtavTfparametrospln_campo_Jsonclick = "";
         edtavTfparametrospln_campo_Visible = 1;
         edtavTfparametrospln_aba_sel_Jsonclick = "";
         edtavTfparametrospln_aba_sel_Visible = 1;
         edtavTfparametrospln_aba_Jsonclick = "";
         edtavTfparametrospln_aba_Visible = 1;
         edtavTfparametrospln_areatrabalhocod_seldsc_Jsonclick = "";
         edtavTfparametrospln_areatrabalhocod_seldsc_Visible = 1;
         edtavTfparametrospln_areatrabalhocod_sel_Jsonclick = "";
         edtavTfparametrospln_areatrabalhocod_sel_Visible = 1;
         edtavTfparametrospln_areatrabalhocod_Jsonclick = "";
         edtavTfparametrospln_areatrabalhocod_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_parametrospln_linha_Searchbuttontext = "Pesquisar";
         Ddo_parametrospln_linha_Rangefilterto = "At�";
         Ddo_parametrospln_linha_Rangefilterfrom = "Desde";
         Ddo_parametrospln_linha_Cleanfilter = "Limpar pesquisa";
         Ddo_parametrospln_linha_Sortdsc = "Ordenar de Z � A";
         Ddo_parametrospln_linha_Sortasc = "Ordenar de A � Z";
         Ddo_parametrospln_linha_Includedatalist = Convert.ToBoolean( 0);
         Ddo_parametrospln_linha_Filterisrange = Convert.ToBoolean( -1);
         Ddo_parametrospln_linha_Filtertype = "Numeric";
         Ddo_parametrospln_linha_Includefilter = Convert.ToBoolean( -1);
         Ddo_parametrospln_linha_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_parametrospln_linha_Includesortasc = Convert.ToBoolean( -1);
         Ddo_parametrospln_linha_Titlecontrolidtoreplace = "";
         Ddo_parametrospln_linha_Dropdownoptionstype = "GridTitleSettings";
         Ddo_parametrospln_linha_Cls = "ColumnSettings";
         Ddo_parametrospln_linha_Tooltip = "Op��es";
         Ddo_parametrospln_linha_Caption = "";
         Ddo_parametrospln_coluna_Searchbuttontext = "Pesquisar";
         Ddo_parametrospln_coluna_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_parametrospln_coluna_Cleanfilter = "Limpar pesquisa";
         Ddo_parametrospln_coluna_Loadingdata = "Carregando dados...";
         Ddo_parametrospln_coluna_Sortdsc = "Ordenar de Z � A";
         Ddo_parametrospln_coluna_Sortasc = "Ordenar de A � Z";
         Ddo_parametrospln_coluna_Datalistupdateminimumcharacters = 0;
         Ddo_parametrospln_coluna_Datalistproc = "GetWWParametrosPlanilhasFilterData";
         Ddo_parametrospln_coluna_Datalisttype = "Dynamic";
         Ddo_parametrospln_coluna_Includedatalist = Convert.ToBoolean( -1);
         Ddo_parametrospln_coluna_Filterisrange = Convert.ToBoolean( 0);
         Ddo_parametrospln_coluna_Filtertype = "Character";
         Ddo_parametrospln_coluna_Includefilter = Convert.ToBoolean( -1);
         Ddo_parametrospln_coluna_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_parametrospln_coluna_Includesortasc = Convert.ToBoolean( -1);
         Ddo_parametrospln_coluna_Titlecontrolidtoreplace = "";
         Ddo_parametrospln_coluna_Dropdownoptionstype = "GridTitleSettings";
         Ddo_parametrospln_coluna_Cls = "ColumnSettings";
         Ddo_parametrospln_coluna_Tooltip = "Op��es";
         Ddo_parametrospln_coluna_Caption = "";
         Ddo_parametrospln_campo_Searchbuttontext = "Pesquisar";
         Ddo_parametrospln_campo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_parametrospln_campo_Cleanfilter = "Limpar pesquisa";
         Ddo_parametrospln_campo_Loadingdata = "Carregando dados...";
         Ddo_parametrospln_campo_Sortdsc = "Ordenar de Z � A";
         Ddo_parametrospln_campo_Sortasc = "Ordenar de A � Z";
         Ddo_parametrospln_campo_Datalistupdateminimumcharacters = 0;
         Ddo_parametrospln_campo_Datalistproc = "GetWWParametrosPlanilhasFilterData";
         Ddo_parametrospln_campo_Datalisttype = "Dynamic";
         Ddo_parametrospln_campo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_parametrospln_campo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_parametrospln_campo_Filtertype = "Character";
         Ddo_parametrospln_campo_Includefilter = Convert.ToBoolean( -1);
         Ddo_parametrospln_campo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_parametrospln_campo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_parametrospln_campo_Titlecontrolidtoreplace = "";
         Ddo_parametrospln_campo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_parametrospln_campo_Cls = "ColumnSettings";
         Ddo_parametrospln_campo_Tooltip = "Op��es";
         Ddo_parametrospln_campo_Caption = "";
         Ddo_parametrospln_aba_Searchbuttontext = "Pesquisar";
         Ddo_parametrospln_aba_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_parametrospln_aba_Cleanfilter = "Limpar pesquisa";
         Ddo_parametrospln_aba_Loadingdata = "Carregando dados...";
         Ddo_parametrospln_aba_Sortdsc = "Ordenar de Z � A";
         Ddo_parametrospln_aba_Sortasc = "Ordenar de A � Z";
         Ddo_parametrospln_aba_Datalistupdateminimumcharacters = 0;
         Ddo_parametrospln_aba_Datalistproc = "GetWWParametrosPlanilhasFilterData";
         Ddo_parametrospln_aba_Datalisttype = "Dynamic";
         Ddo_parametrospln_aba_Includedatalist = Convert.ToBoolean( -1);
         Ddo_parametrospln_aba_Filterisrange = Convert.ToBoolean( 0);
         Ddo_parametrospln_aba_Filtertype = "Character";
         Ddo_parametrospln_aba_Includefilter = Convert.ToBoolean( -1);
         Ddo_parametrospln_aba_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_parametrospln_aba_Includesortasc = Convert.ToBoolean( -1);
         Ddo_parametrospln_aba_Titlecontrolidtoreplace = "";
         Ddo_parametrospln_aba_Dropdownoptionstype = "GridTitleSettings";
         Ddo_parametrospln_aba_Cls = "ColumnSettings";
         Ddo_parametrospln_aba_Tooltip = "Op��es";
         Ddo_parametrospln_aba_Caption = "";
         Ddo_parametrospln_areatrabalhocod_Searchbuttontext = "Pesquisar";
         Ddo_parametrospln_areatrabalhocod_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_parametrospln_areatrabalhocod_Cleanfilter = "Limpar pesquisa";
         Ddo_parametrospln_areatrabalhocod_Loadingdata = "Carregando dados...";
         Ddo_parametrospln_areatrabalhocod_Sortdsc = "Ordenar de Z � A";
         Ddo_parametrospln_areatrabalhocod_Sortasc = "Ordenar de A � Z";
         Ddo_parametrospln_areatrabalhocod_Datalistupdateminimumcharacters = 0;
         Ddo_parametrospln_areatrabalhocod_Datalistproc = "GetWWParametrosPlanilhasFilterData";
         Ddo_parametrospln_areatrabalhocod_Datalisttype = "Dynamic";
         Ddo_parametrospln_areatrabalhocod_Includedatalist = Convert.ToBoolean( -1);
         Ddo_parametrospln_areatrabalhocod_Filterisrange = Convert.ToBoolean( 0);
         Ddo_parametrospln_areatrabalhocod_Filtertype = "Character";
         Ddo_parametrospln_areatrabalhocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_parametrospln_areatrabalhocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_parametrospln_areatrabalhocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_parametrospln_areatrabalhocod_Titlecontrolidtoreplace = "";
         Ddo_parametrospln_areatrabalhocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_parametrospln_areatrabalhocod_Cls = "ColumnSettings";
         Ddo_parametrospln_areatrabalhocod_Tooltip = "Op��es";
         Ddo_parametrospln_areatrabalhocod_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Par�metros das Planilhas";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ParametrosPln_AbaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_ABATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ParametrosPln_CampoTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_CAMPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_COLUNATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_LINHATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'AV81Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV58TFParametrosPln_AreaTrabalhoCod_SelDsc',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV33ParametrosPln_AreaTrabalhoCodTitleFilterData',fld:'vPARAMETROSPLN_AREATRABALHOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV54ParametrosPln_AbaTitleFilterData',fld:'vPARAMETROSPLN_ABATITLEFILTERDATA',pic:'',nv:null},{av:'AV37ParametrosPln_CampoTitleFilterData',fld:'vPARAMETROSPLN_CAMPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV41ParametrosPln_ColunaTitleFilterData',fld:'vPARAMETROSPLN_COLUNATITLEFILTERDATA',pic:'',nv:null},{av:'AV45ParametrosPln_LinhaTitleFilterData',fld:'vPARAMETROSPLN_LINHATITLEFILTERDATA',pic:'',nv:null},{av:'dynParametrosPln_AreaTrabalhoCod'},{av:'edtParametrosPln_Aba_Titleformat',ctrl:'PARAMETROSPLN_ABA',prop:'Titleformat'},{av:'edtParametrosPln_Aba_Title',ctrl:'PARAMETROSPLN_ABA',prop:'Title'},{av:'edtParametrosPln_Campo_Titleformat',ctrl:'PARAMETROSPLN_CAMPO',prop:'Titleformat'},{av:'edtParametrosPln_Campo_Title',ctrl:'PARAMETROSPLN_CAMPO',prop:'Title'},{av:'edtParametrosPln_Coluna_Titleformat',ctrl:'PARAMETROSPLN_COLUNA',prop:'Titleformat'},{av:'edtParametrosPln_Coluna_Title',ctrl:'PARAMETROSPLN_COLUNA',prop:'Title'},{av:'edtParametrosPln_Linha_Titleformat',ctrl:'PARAMETROSPLN_LINHA',prop:'Titleformat'},{av:'edtParametrosPln_Linha_Title',ctrl:'PARAMETROSPLN_LINHA',prop:'Title'},{av:'AV51GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV52GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11F02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ParametrosPln_AbaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_ABATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ParametrosPln_CampoTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_CAMPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_COLUNATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_LINHATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV58TFParametrosPln_AreaTrabalhoCod_SelDsc',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_PARAMETROSPLN_AREATRABALHOCOD.ONOPTIONCLICKED","{handler:'E12F02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ParametrosPln_AbaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_ABATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ParametrosPln_CampoTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_CAMPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_COLUNATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_LINHATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV58TFParametrosPln_AreaTrabalhoCod_SelDsc',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_parametrospln_areatrabalhocod_Activeeventkey',ctrl:'DDO_PARAMETROSPLN_AREATRABALHOCOD',prop:'ActiveEventKey'},{av:'Ddo_parametrospln_areatrabalhocod_Filteredtext_get',ctrl:'DDO_PARAMETROSPLN_AREATRABALHOCOD',prop:'FilteredText_get'},{av:'Ddo_parametrospln_areatrabalhocod_Selectedvalue_get',ctrl:'DDO_PARAMETROSPLN_AREATRABALHOCOD',prop:'SelectedValue_get'},{av:'Ddo_parametrospln_areatrabalhocod_Selectedtext_get',ctrl:'DDO_PARAMETROSPLN_AREATRABALHOCOD',prop:'SelectedText_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_parametrospln_areatrabalhocod_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_AREATRABALHOCOD',prop:'SortedStatus'},{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV58TFParametrosPln_AreaTrabalhoCod_SelDsc',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC',pic:'',nv:''},{av:'Ddo_parametrospln_aba_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_ABA',prop:'SortedStatus'},{av:'Ddo_parametrospln_campo_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_CAMPO',prop:'SortedStatus'},{av:'Ddo_parametrospln_coluna_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_COLUNA',prop:'SortedStatus'},{av:'Ddo_parametrospln_linha_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_LINHA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PARAMETROSPLN_ABA.ONOPTIONCLICKED","{handler:'E13F02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ParametrosPln_AbaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_ABATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ParametrosPln_CampoTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_CAMPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_COLUNATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_LINHATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV58TFParametrosPln_AreaTrabalhoCod_SelDsc',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_parametrospln_aba_Activeeventkey',ctrl:'DDO_PARAMETROSPLN_ABA',prop:'ActiveEventKey'},{av:'Ddo_parametrospln_aba_Filteredtext_get',ctrl:'DDO_PARAMETROSPLN_ABA',prop:'FilteredText_get'},{av:'Ddo_parametrospln_aba_Selectedvalue_get',ctrl:'DDO_PARAMETROSPLN_ABA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_parametrospln_aba_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_ABA',prop:'SortedStatus'},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'Ddo_parametrospln_areatrabalhocod_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_parametrospln_campo_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_CAMPO',prop:'SortedStatus'},{av:'Ddo_parametrospln_coluna_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_COLUNA',prop:'SortedStatus'},{av:'Ddo_parametrospln_linha_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_LINHA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PARAMETROSPLN_CAMPO.ONOPTIONCLICKED","{handler:'E14F02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ParametrosPln_AbaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_ABATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ParametrosPln_CampoTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_CAMPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_COLUNATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_LINHATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV58TFParametrosPln_AreaTrabalhoCod_SelDsc',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_parametrospln_campo_Activeeventkey',ctrl:'DDO_PARAMETROSPLN_CAMPO',prop:'ActiveEventKey'},{av:'Ddo_parametrospln_campo_Filteredtext_get',ctrl:'DDO_PARAMETROSPLN_CAMPO',prop:'FilteredText_get'},{av:'Ddo_parametrospln_campo_Selectedvalue_get',ctrl:'DDO_PARAMETROSPLN_CAMPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_parametrospln_campo_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_CAMPO',prop:'SortedStatus'},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'Ddo_parametrospln_areatrabalhocod_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_parametrospln_aba_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_ABA',prop:'SortedStatus'},{av:'Ddo_parametrospln_coluna_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_COLUNA',prop:'SortedStatus'},{av:'Ddo_parametrospln_linha_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_LINHA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PARAMETROSPLN_COLUNA.ONOPTIONCLICKED","{handler:'E15F02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ParametrosPln_AbaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_ABATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ParametrosPln_CampoTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_CAMPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_COLUNATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_LINHATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV58TFParametrosPln_AreaTrabalhoCod_SelDsc',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_parametrospln_coluna_Activeeventkey',ctrl:'DDO_PARAMETROSPLN_COLUNA',prop:'ActiveEventKey'},{av:'Ddo_parametrospln_coluna_Filteredtext_get',ctrl:'DDO_PARAMETROSPLN_COLUNA',prop:'FilteredText_get'},{av:'Ddo_parametrospln_coluna_Selectedvalue_get',ctrl:'DDO_PARAMETROSPLN_COLUNA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_parametrospln_coluna_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_COLUNA',prop:'SortedStatus'},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'Ddo_parametrospln_areatrabalhocod_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_parametrospln_aba_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_ABA',prop:'SortedStatus'},{av:'Ddo_parametrospln_campo_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_CAMPO',prop:'SortedStatus'},{av:'Ddo_parametrospln_linha_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_LINHA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PARAMETROSPLN_LINHA.ONOPTIONCLICKED","{handler:'E16F02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ParametrosPln_AbaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_ABATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ParametrosPln_CampoTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_CAMPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_COLUNATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_LINHATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV58TFParametrosPln_AreaTrabalhoCod_SelDsc',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_parametrospln_linha_Activeeventkey',ctrl:'DDO_PARAMETROSPLN_LINHA',prop:'ActiveEventKey'},{av:'Ddo_parametrospln_linha_Filteredtext_get',ctrl:'DDO_PARAMETROSPLN_LINHA',prop:'FilteredText_get'},{av:'Ddo_parametrospln_linha_Filteredtextto_get',ctrl:'DDO_PARAMETROSPLN_LINHA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_parametrospln_linha_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_LINHA',prop:'SortedStatus'},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_parametrospln_areatrabalhocod_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_parametrospln_aba_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_ABA',prop:'SortedStatus'},{av:'Ddo_parametrospln_campo_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_CAMPO',prop:'SortedStatus'},{av:'Ddo_parametrospln_coluna_Sortedstatus',ctrl:'DDO_PARAMETROSPLN_COLUNA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E30F02',iparms:[{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtParametrosPln_Campo_Link',ctrl:'PARAMETROSPLN_CAMPO',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E17F02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ParametrosPln_AbaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_ABATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ParametrosPln_CampoTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_CAMPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_COLUNATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_LINHATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV58TFParametrosPln_AreaTrabalhoCod_SelDsc',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E23F02',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E18F02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ParametrosPln_AbaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_ABATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ParametrosPln_CampoTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_CAMPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_COLUNATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_LINHATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV58TFParametrosPln_AreaTrabalhoCod_SelDsc',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavParametrospln_campo2_Visible',ctrl:'vPARAMETROSPLN_CAMPO2',prop:'Visible'},{av:'edtavParametrospln_campo3_Visible',ctrl:'vPARAMETROSPLN_CAMPO3',prop:'Visible'},{av:'edtavParametrospln_campo1_Visible',ctrl:'vPARAMETROSPLN_CAMPO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E24F02',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavParametrospln_campo1_Visible',ctrl:'vPARAMETROSPLN_CAMPO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E25F02',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E19F02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ParametrosPln_AbaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_ABATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ParametrosPln_CampoTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_CAMPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_COLUNATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_LINHATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV58TFParametrosPln_AreaTrabalhoCod_SelDsc',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavParametrospln_campo2_Visible',ctrl:'vPARAMETROSPLN_CAMPO2',prop:'Visible'},{av:'edtavParametrospln_campo3_Visible',ctrl:'vPARAMETROSPLN_CAMPO3',prop:'Visible'},{av:'edtavParametrospln_campo1_Visible',ctrl:'vPARAMETROSPLN_CAMPO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E26F02',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavParametrospln_campo2_Visible',ctrl:'vPARAMETROSPLN_CAMPO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E20F02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ParametrosPln_AbaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_ABATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ParametrosPln_CampoTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_CAMPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_COLUNATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_LINHATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV58TFParametrosPln_AreaTrabalhoCod_SelDsc',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavParametrospln_campo2_Visible',ctrl:'vPARAMETROSPLN_CAMPO2',prop:'Visible'},{av:'edtavParametrospln_campo3_Visible',ctrl:'vPARAMETROSPLN_CAMPO3',prop:'Visible'},{av:'edtavParametrospln_campo1_Visible',ctrl:'vPARAMETROSPLN_CAMPO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E27F02',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavParametrospln_campo3_Visible',ctrl:'vPARAMETROSPLN_CAMPO3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21F02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ParametrosPln_AbaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_ABATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ParametrosPln_CampoTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_CAMPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_COLUNATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace',fld:'vDDO_PARAMETROSPLN_LINHATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV58TFParametrosPln_AreaTrabalhoCod_SelDsc',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SELDSC',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV34TFParametrosPln_AreaTrabalhoCod',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD',pic:'@!',nv:''},{av:'Ddo_parametrospln_areatrabalhocod_Filteredtext_set',ctrl:'DDO_PARAMETROSPLN_AREATRABALHOCOD',prop:'FilteredText_set'},{av:'AV53TFParametrosPln_AreaTrabalhoCod_Sel',fld:'vTFPARAMETROSPLN_AREATRABALHOCOD_SEL',pic:'ZZZZZ9',nv:0},{av:'Ddo_parametrospln_areatrabalhocod_Selectedvalue_set',ctrl:'DDO_PARAMETROSPLN_AREATRABALHOCOD',prop:'SelectedValue_set'},{av:'AV55TFParametrosPln_Aba',fld:'vTFPARAMETROSPLN_ABA',pic:'',nv:''},{av:'Ddo_parametrospln_aba_Filteredtext_set',ctrl:'DDO_PARAMETROSPLN_ABA',prop:'FilteredText_set'},{av:'AV56TFParametrosPln_Aba_Sel',fld:'vTFPARAMETROSPLN_ABA_SEL',pic:'',nv:''},{av:'Ddo_parametrospln_aba_Selectedvalue_set',ctrl:'DDO_PARAMETROSPLN_ABA',prop:'SelectedValue_set'},{av:'AV38TFParametrosPln_Campo',fld:'vTFPARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'Ddo_parametrospln_campo_Filteredtext_set',ctrl:'DDO_PARAMETROSPLN_CAMPO',prop:'FilteredText_set'},{av:'AV39TFParametrosPln_Campo_Sel',fld:'vTFPARAMETROSPLN_CAMPO_SEL',pic:'@!',nv:''},{av:'Ddo_parametrospln_campo_Selectedvalue_set',ctrl:'DDO_PARAMETROSPLN_CAMPO',prop:'SelectedValue_set'},{av:'AV42TFParametrosPln_Coluna',fld:'vTFPARAMETROSPLN_COLUNA',pic:'@!',nv:''},{av:'Ddo_parametrospln_coluna_Filteredtext_set',ctrl:'DDO_PARAMETROSPLN_COLUNA',prop:'FilteredText_set'},{av:'AV43TFParametrosPln_Coluna_Sel',fld:'vTFPARAMETROSPLN_COLUNA_SEL',pic:'@!',nv:''},{av:'Ddo_parametrospln_coluna_Selectedvalue_set',ctrl:'DDO_PARAMETROSPLN_COLUNA',prop:'SelectedValue_set'},{av:'AV46TFParametrosPln_Linha',fld:'vTFPARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'Ddo_parametrospln_linha_Filteredtext_set',ctrl:'DDO_PARAMETROSPLN_LINHA',prop:'FilteredText_set'},{av:'AV47TFParametrosPln_Linha_To',fld:'vTFPARAMETROSPLN_LINHA_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_parametrospln_linha_Filteredtextto_set',ctrl:'DDO_PARAMETROSPLN_LINHA',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ParametrosPln_Campo1',fld:'vPARAMETROSPLN_CAMPO1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavParametrospln_campo1_Visible',ctrl:'vPARAMETROSPLN_CAMPO1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ParametrosPln_Campo2',fld:'vPARAMETROSPLN_CAMPO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ParametrosPln_Campo3',fld:'vPARAMETROSPLN_CAMPO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavParametrospln_campo2_Visible',ctrl:'vPARAMETROSPLN_CAMPO2',prop:'Visible'},{av:'edtavParametrospln_campo3_Visible',ctrl:'vPARAMETROSPLN_CAMPO3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E22F02',iparms:[{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_parametrospln_areatrabalhocod_Activeeventkey = "";
         Ddo_parametrospln_areatrabalhocod_Filteredtext_get = "";
         Ddo_parametrospln_areatrabalhocod_Selectedvalue_get = "";
         Ddo_parametrospln_areatrabalhocod_Selectedtext_get = "";
         Ddo_parametrospln_aba_Activeeventkey = "";
         Ddo_parametrospln_aba_Filteredtext_get = "";
         Ddo_parametrospln_aba_Selectedvalue_get = "";
         Ddo_parametrospln_campo_Activeeventkey = "";
         Ddo_parametrospln_campo_Filteredtext_get = "";
         Ddo_parametrospln_campo_Selectedvalue_get = "";
         Ddo_parametrospln_coluna_Activeeventkey = "";
         Ddo_parametrospln_coluna_Filteredtext_get = "";
         Ddo_parametrospln_coluna_Selectedvalue_get = "";
         Ddo_parametrospln_linha_Activeeventkey = "";
         Ddo_parametrospln_linha_Filteredtext_get = "";
         Ddo_parametrospln_linha_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ParametrosPln_Campo1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21ParametrosPln_Campo2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25ParametrosPln_Campo3 = "";
         AV34TFParametrosPln_AreaTrabalhoCod = "";
         AV55TFParametrosPln_Aba = "";
         AV56TFParametrosPln_Aba_Sel = "";
         AV38TFParametrosPln_Campo = "";
         AV39TFParametrosPln_Campo_Sel = "";
         AV42TFParametrosPln_Coluna = "";
         AV43TFParametrosPln_Coluna_Sel = "";
         AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace = "";
         AV57ddo_ParametrosPln_AbaTitleControlIdToReplace = "";
         AV40ddo_ParametrosPln_CampoTitleControlIdToReplace = "";
         AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace = "";
         AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace = "";
         AV81Pgmname = "";
         AV58TFParametrosPln_AreaTrabalhoCod_SelDsc = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV49DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV33ParametrosPln_AreaTrabalhoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54ParametrosPln_AbaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37ParametrosPln_CampoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41ParametrosPln_ColunaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45ParametrosPln_LinhaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_parametrospln_areatrabalhocod_Filteredtext_set = "";
         Ddo_parametrospln_areatrabalhocod_Selectedvalue_set = "";
         Ddo_parametrospln_areatrabalhocod_Selectedtext_set = "";
         Ddo_parametrospln_areatrabalhocod_Sortedstatus = "";
         Ddo_parametrospln_aba_Filteredtext_set = "";
         Ddo_parametrospln_aba_Selectedvalue_set = "";
         Ddo_parametrospln_aba_Sortedstatus = "";
         Ddo_parametrospln_campo_Filteredtext_set = "";
         Ddo_parametrospln_campo_Selectedvalue_set = "";
         Ddo_parametrospln_campo_Sortedstatus = "";
         Ddo_parametrospln_coluna_Filteredtext_set = "";
         Ddo_parametrospln_coluna_Selectedvalue_set = "";
         Ddo_parametrospln_coluna_Sortedstatus = "";
         Ddo_parametrospln_linha_Filteredtext_set = "";
         Ddo_parametrospln_linha_Filteredtextto_set = "";
         Ddo_parametrospln_linha_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV79Update_GXI = "";
         AV29Delete = "";
         AV80Delete_GXI = "";
         A2015ParametrosPln_Aba = "";
         A849ParametrosPln_Campo = "";
         A850ParametrosPln_Coluna = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00F02_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         H00F02_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         H00F02_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00F02_n6AreaTrabalho_Descricao = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         lV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 = "";
         lV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 = "";
         lV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 = "";
         lV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = "";
         lV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba = "";
         lV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo = "";
         lV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = "";
         AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 = "";
         AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 = "";
         AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 = "";
         AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 = "";
         AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 = "";
         AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 = "";
         AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = "";
         AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel = "";
         AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba = "";
         AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel = "";
         AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo = "";
         AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel = "";
         AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = "";
         A6AreaTrabalho_Descricao = "";
         H00F03_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00F03_n6AreaTrabalho_Descricao = new bool[] {false} ;
         H00F03_A851ParametrosPln_Linha = new int[1] ;
         H00F03_A850ParametrosPln_Coluna = new String[] {""} ;
         H00F03_A849ParametrosPln_Campo = new String[] {""} ;
         H00F03_A2015ParametrosPln_Aba = new String[] {""} ;
         H00F03_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         H00F03_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         H00F03_A848ParametrosPln_Codigo = new int[1] ;
         H00F04_AGRID_nRecordCount = new long[1] ;
         H00F05_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00F05_n6AreaTrabalho_Descricao = new bool[] {false} ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblParametrosplanilhastitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwparametrosplanilhas__default(),
            new Object[][] {
                new Object[] {
               H00F02_A847ParametrosPln_AreaTrabalhoCod, H00F02_A6AreaTrabalho_Descricao, H00F02_n6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00F03_A6AreaTrabalho_Descricao, H00F03_n6AreaTrabalho_Descricao, H00F03_A851ParametrosPln_Linha, H00F03_A850ParametrosPln_Coluna, H00F03_A849ParametrosPln_Campo, H00F03_A2015ParametrosPln_Aba, H00F03_A847ParametrosPln_AreaTrabalhoCod, H00F03_n847ParametrosPln_AreaTrabalhoCod, H00F03_A848ParametrosPln_Codigo
               }
               , new Object[] {
               H00F04_AGRID_nRecordCount
               }
               , new Object[] {
               H00F05_A6AreaTrabalho_Descricao, H00F05_n6AreaTrabalho_Descricao
               }
            }
         );
         AV81Pgmname = "WWParametrosPlanilhas";
         /* GeneXus formulas. */
         AV81Pgmname = "WWParametrosPlanilhas";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_73 ;
      private short nGXsfl_73_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_73_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short dynParametrosPln_AreaTrabalhoCod_Titleformat ;
      private short edtParametrosPln_Aba_Titleformat ;
      private short edtParametrosPln_Campo_Titleformat ;
      private short edtParametrosPln_Coluna_Titleformat ;
      private short edtParametrosPln_Linha_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV53TFParametrosPln_AreaTrabalhoCod_Sel ;
      private int AV46TFParametrosPln_Linha ;
      private int AV47TFParametrosPln_Linha_To ;
      private int A848ParametrosPln_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_parametrospln_areatrabalhocod_Datalistupdateminimumcharacters ;
      private int Ddo_parametrospln_aba_Datalistupdateminimumcharacters ;
      private int Ddo_parametrospln_campo_Datalistupdateminimumcharacters ;
      private int Ddo_parametrospln_coluna_Datalistupdateminimumcharacters ;
      private int edtavTfparametrospln_areatrabalhocod_Visible ;
      private int edtavTfparametrospln_areatrabalhocod_sel_Visible ;
      private int edtavTfparametrospln_areatrabalhocod_seldsc_Visible ;
      private int edtavTfparametrospln_aba_Visible ;
      private int edtavTfparametrospln_aba_sel_Visible ;
      private int edtavTfparametrospln_campo_Visible ;
      private int edtavTfparametrospln_campo_sel_Visible ;
      private int edtavTfparametrospln_coluna_Visible ;
      private int edtavTfparametrospln_coluna_sel_Visible ;
      private int edtavTfparametrospln_linha_Visible ;
      private int edtavTfparametrospln_linha_to_Visible ;
      private int edtavDdo_parametrospln_areatrabalhocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_parametrospln_abatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_parametrospln_campotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_parametrospln_colunatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_parametrospln_linhatitlecontrolidtoreplace_Visible ;
      private int A847ParametrosPln_AreaTrabalhoCod ;
      private int A851ParametrosPln_Linha ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ;
      private int AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha ;
      private int AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV50PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavParametrospln_campo1_Visible ;
      private int edtavParametrospln_campo2_Visible ;
      private int edtavParametrospln_campo3_Visible ;
      private int AV82GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV51GridCurrentPage ;
      private long AV52GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_parametrospln_areatrabalhocod_Activeeventkey ;
      private String Ddo_parametrospln_areatrabalhocod_Filteredtext_get ;
      private String Ddo_parametrospln_areatrabalhocod_Selectedvalue_get ;
      private String Ddo_parametrospln_areatrabalhocod_Selectedtext_get ;
      private String Ddo_parametrospln_aba_Activeeventkey ;
      private String Ddo_parametrospln_aba_Filteredtext_get ;
      private String Ddo_parametrospln_aba_Selectedvalue_get ;
      private String Ddo_parametrospln_campo_Activeeventkey ;
      private String Ddo_parametrospln_campo_Filteredtext_get ;
      private String Ddo_parametrospln_campo_Selectedvalue_get ;
      private String Ddo_parametrospln_coluna_Activeeventkey ;
      private String Ddo_parametrospln_coluna_Filteredtext_get ;
      private String Ddo_parametrospln_coluna_Selectedvalue_get ;
      private String Ddo_parametrospln_linha_Activeeventkey ;
      private String Ddo_parametrospln_linha_Filteredtext_get ;
      private String Ddo_parametrospln_linha_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_73_idx="0001" ;
      private String AV55TFParametrosPln_Aba ;
      private String AV56TFParametrosPln_Aba_Sel ;
      private String AV42TFParametrosPln_Coluna ;
      private String AV43TFParametrosPln_Coluna_Sel ;
      private String AV81Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_parametrospln_areatrabalhocod_Caption ;
      private String Ddo_parametrospln_areatrabalhocod_Tooltip ;
      private String Ddo_parametrospln_areatrabalhocod_Cls ;
      private String Ddo_parametrospln_areatrabalhocod_Filteredtext_set ;
      private String Ddo_parametrospln_areatrabalhocod_Selectedvalue_set ;
      private String Ddo_parametrospln_areatrabalhocod_Selectedtext_set ;
      private String Ddo_parametrospln_areatrabalhocod_Dropdownoptionstype ;
      private String Ddo_parametrospln_areatrabalhocod_Titlecontrolidtoreplace ;
      private String Ddo_parametrospln_areatrabalhocod_Sortedstatus ;
      private String Ddo_parametrospln_areatrabalhocod_Filtertype ;
      private String Ddo_parametrospln_areatrabalhocod_Datalisttype ;
      private String Ddo_parametrospln_areatrabalhocod_Datalistproc ;
      private String Ddo_parametrospln_areatrabalhocod_Sortasc ;
      private String Ddo_parametrospln_areatrabalhocod_Sortdsc ;
      private String Ddo_parametrospln_areatrabalhocod_Loadingdata ;
      private String Ddo_parametrospln_areatrabalhocod_Cleanfilter ;
      private String Ddo_parametrospln_areatrabalhocod_Noresultsfound ;
      private String Ddo_parametrospln_areatrabalhocod_Searchbuttontext ;
      private String Ddo_parametrospln_aba_Caption ;
      private String Ddo_parametrospln_aba_Tooltip ;
      private String Ddo_parametrospln_aba_Cls ;
      private String Ddo_parametrospln_aba_Filteredtext_set ;
      private String Ddo_parametrospln_aba_Selectedvalue_set ;
      private String Ddo_parametrospln_aba_Dropdownoptionstype ;
      private String Ddo_parametrospln_aba_Titlecontrolidtoreplace ;
      private String Ddo_parametrospln_aba_Sortedstatus ;
      private String Ddo_parametrospln_aba_Filtertype ;
      private String Ddo_parametrospln_aba_Datalisttype ;
      private String Ddo_parametrospln_aba_Datalistproc ;
      private String Ddo_parametrospln_aba_Sortasc ;
      private String Ddo_parametrospln_aba_Sortdsc ;
      private String Ddo_parametrospln_aba_Loadingdata ;
      private String Ddo_parametrospln_aba_Cleanfilter ;
      private String Ddo_parametrospln_aba_Noresultsfound ;
      private String Ddo_parametrospln_aba_Searchbuttontext ;
      private String Ddo_parametrospln_campo_Caption ;
      private String Ddo_parametrospln_campo_Tooltip ;
      private String Ddo_parametrospln_campo_Cls ;
      private String Ddo_parametrospln_campo_Filteredtext_set ;
      private String Ddo_parametrospln_campo_Selectedvalue_set ;
      private String Ddo_parametrospln_campo_Dropdownoptionstype ;
      private String Ddo_parametrospln_campo_Titlecontrolidtoreplace ;
      private String Ddo_parametrospln_campo_Sortedstatus ;
      private String Ddo_parametrospln_campo_Filtertype ;
      private String Ddo_parametrospln_campo_Datalisttype ;
      private String Ddo_parametrospln_campo_Datalistproc ;
      private String Ddo_parametrospln_campo_Sortasc ;
      private String Ddo_parametrospln_campo_Sortdsc ;
      private String Ddo_parametrospln_campo_Loadingdata ;
      private String Ddo_parametrospln_campo_Cleanfilter ;
      private String Ddo_parametrospln_campo_Noresultsfound ;
      private String Ddo_parametrospln_campo_Searchbuttontext ;
      private String Ddo_parametrospln_coluna_Caption ;
      private String Ddo_parametrospln_coluna_Tooltip ;
      private String Ddo_parametrospln_coluna_Cls ;
      private String Ddo_parametrospln_coluna_Filteredtext_set ;
      private String Ddo_parametrospln_coluna_Selectedvalue_set ;
      private String Ddo_parametrospln_coluna_Dropdownoptionstype ;
      private String Ddo_parametrospln_coluna_Titlecontrolidtoreplace ;
      private String Ddo_parametrospln_coluna_Sortedstatus ;
      private String Ddo_parametrospln_coluna_Filtertype ;
      private String Ddo_parametrospln_coluna_Datalisttype ;
      private String Ddo_parametrospln_coluna_Datalistproc ;
      private String Ddo_parametrospln_coluna_Sortasc ;
      private String Ddo_parametrospln_coluna_Sortdsc ;
      private String Ddo_parametrospln_coluna_Loadingdata ;
      private String Ddo_parametrospln_coluna_Cleanfilter ;
      private String Ddo_parametrospln_coluna_Noresultsfound ;
      private String Ddo_parametrospln_coluna_Searchbuttontext ;
      private String Ddo_parametrospln_linha_Caption ;
      private String Ddo_parametrospln_linha_Tooltip ;
      private String Ddo_parametrospln_linha_Cls ;
      private String Ddo_parametrospln_linha_Filteredtext_set ;
      private String Ddo_parametrospln_linha_Filteredtextto_set ;
      private String Ddo_parametrospln_linha_Dropdownoptionstype ;
      private String Ddo_parametrospln_linha_Titlecontrolidtoreplace ;
      private String Ddo_parametrospln_linha_Sortedstatus ;
      private String Ddo_parametrospln_linha_Filtertype ;
      private String Ddo_parametrospln_linha_Sortasc ;
      private String Ddo_parametrospln_linha_Sortdsc ;
      private String Ddo_parametrospln_linha_Cleanfilter ;
      private String Ddo_parametrospln_linha_Rangefilterfrom ;
      private String Ddo_parametrospln_linha_Rangefilterto ;
      private String Ddo_parametrospln_linha_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfparametrospln_areatrabalhocod_Internalname ;
      private String edtavTfparametrospln_areatrabalhocod_Jsonclick ;
      private String edtavTfparametrospln_areatrabalhocod_sel_Internalname ;
      private String edtavTfparametrospln_areatrabalhocod_sel_Jsonclick ;
      private String edtavTfparametrospln_areatrabalhocod_seldsc_Internalname ;
      private String edtavTfparametrospln_areatrabalhocod_seldsc_Jsonclick ;
      private String edtavTfparametrospln_aba_Internalname ;
      private String edtavTfparametrospln_aba_Jsonclick ;
      private String edtavTfparametrospln_aba_sel_Internalname ;
      private String edtavTfparametrospln_aba_sel_Jsonclick ;
      private String edtavTfparametrospln_campo_Internalname ;
      private String edtavTfparametrospln_campo_Jsonclick ;
      private String edtavTfparametrospln_campo_sel_Internalname ;
      private String edtavTfparametrospln_campo_sel_Jsonclick ;
      private String edtavTfparametrospln_coluna_Internalname ;
      private String edtavTfparametrospln_coluna_Jsonclick ;
      private String edtavTfparametrospln_coluna_sel_Internalname ;
      private String edtavTfparametrospln_coluna_sel_Jsonclick ;
      private String edtavTfparametrospln_linha_Internalname ;
      private String edtavTfparametrospln_linha_Jsonclick ;
      private String edtavTfparametrospln_linha_to_Internalname ;
      private String edtavTfparametrospln_linha_to_Jsonclick ;
      private String edtavDdo_parametrospln_areatrabalhocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_parametrospln_abatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_parametrospln_campotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_parametrospln_colunatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_parametrospln_linhatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtParametrosPln_Codigo_Internalname ;
      private String dynParametrosPln_AreaTrabalhoCod_Internalname ;
      private String A2015ParametrosPln_Aba ;
      private String edtParametrosPln_Aba_Internalname ;
      private String edtParametrosPln_Campo_Internalname ;
      private String A850ParametrosPln_Coluna ;
      private String edtParametrosPln_Coluna_Internalname ;
      private String edtParametrosPln_Linha_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba ;
      private String lV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ;
      private String AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ;
      private String AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba ;
      private String AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ;
      private String AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavParametrospln_campo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavParametrospln_campo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavParametrospln_campo3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_parametrospln_areatrabalhocod_Internalname ;
      private String Ddo_parametrospln_aba_Internalname ;
      private String Ddo_parametrospln_campo_Internalname ;
      private String Ddo_parametrospln_coluna_Internalname ;
      private String Ddo_parametrospln_linha_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtParametrosPln_Aba_Title ;
      private String edtParametrosPln_Campo_Title ;
      private String edtParametrosPln_Coluna_Title ;
      private String edtParametrosPln_Linha_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtParametrosPln_Campo_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblParametrosplanilhastitle_Internalname ;
      private String lblParametrosplanilhastitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavParametrospln_campo1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavParametrospln_campo2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavParametrospln_campo3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_73_fel_idx="0001" ;
      private String ROClassString ;
      private String edtParametrosPln_Codigo_Jsonclick ;
      private String dynParametrosPln_AreaTrabalhoCod_Jsonclick ;
      private String edtParametrosPln_Aba_Jsonclick ;
      private String edtParametrosPln_Campo_Jsonclick ;
      private String edtParametrosPln_Coluna_Jsonclick ;
      private String edtParametrosPln_Linha_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_parametrospln_areatrabalhocod_Includesortasc ;
      private bool Ddo_parametrospln_areatrabalhocod_Includesortdsc ;
      private bool Ddo_parametrospln_areatrabalhocod_Includefilter ;
      private bool Ddo_parametrospln_areatrabalhocod_Filterisrange ;
      private bool Ddo_parametrospln_areatrabalhocod_Includedatalist ;
      private bool Ddo_parametrospln_aba_Includesortasc ;
      private bool Ddo_parametrospln_aba_Includesortdsc ;
      private bool Ddo_parametrospln_aba_Includefilter ;
      private bool Ddo_parametrospln_aba_Filterisrange ;
      private bool Ddo_parametrospln_aba_Includedatalist ;
      private bool Ddo_parametrospln_campo_Includesortasc ;
      private bool Ddo_parametrospln_campo_Includesortdsc ;
      private bool Ddo_parametrospln_campo_Includefilter ;
      private bool Ddo_parametrospln_campo_Filterisrange ;
      private bool Ddo_parametrospln_campo_Includedatalist ;
      private bool Ddo_parametrospln_coluna_Includesortasc ;
      private bool Ddo_parametrospln_coluna_Includesortdsc ;
      private bool Ddo_parametrospln_coluna_Includefilter ;
      private bool Ddo_parametrospln_coluna_Filterisrange ;
      private bool Ddo_parametrospln_coluna_Includedatalist ;
      private bool Ddo_parametrospln_linha_Includesortasc ;
      private bool Ddo_parametrospln_linha_Includesortdsc ;
      private bool Ddo_parametrospln_linha_Includefilter ;
      private bool Ddo_parametrospln_linha_Filterisrange ;
      private bool Ddo_parametrospln_linha_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n847ParametrosPln_AreaTrabalhoCod ;
      private bool AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ;
      private bool AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ;
      private bool n6AreaTrabalho_Descricao ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17ParametrosPln_Campo1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV21ParametrosPln_Campo2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV25ParametrosPln_Campo3 ;
      private String AV34TFParametrosPln_AreaTrabalhoCod ;
      private String AV38TFParametrosPln_Campo ;
      private String AV39TFParametrosPln_Campo_Sel ;
      private String AV36ddo_ParametrosPln_AreaTrabalhoCodTitleControlIdToReplace ;
      private String AV57ddo_ParametrosPln_AbaTitleControlIdToReplace ;
      private String AV40ddo_ParametrosPln_CampoTitleControlIdToReplace ;
      private String AV44ddo_ParametrosPln_ColunaTitleControlIdToReplace ;
      private String AV48ddo_ParametrosPln_LinhaTitleControlIdToReplace ;
      private String AV58TFParametrosPln_AreaTrabalhoCod_SelDsc ;
      private String AV79Update_GXI ;
      private String AV80Delete_GXI ;
      private String A849ParametrosPln_Campo ;
      private String lV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 ;
      private String lV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 ;
      private String lV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 ;
      private String lV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ;
      private String lV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo ;
      private String AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ;
      private String AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 ;
      private String AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ;
      private String AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 ;
      private String AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ;
      private String AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 ;
      private String AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ;
      private String AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ;
      private String AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo ;
      private String A6AreaTrabalho_Descricao ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox dynParametrosPln_AreaTrabalhoCod ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00F02_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] H00F02_n847ParametrosPln_AreaTrabalhoCod ;
      private String[] H00F02_A6AreaTrabalho_Descricao ;
      private bool[] H00F02_n6AreaTrabalho_Descricao ;
      private String[] H00F03_A6AreaTrabalho_Descricao ;
      private bool[] H00F03_n6AreaTrabalho_Descricao ;
      private int[] H00F03_A851ParametrosPln_Linha ;
      private String[] H00F03_A850ParametrosPln_Coluna ;
      private String[] H00F03_A849ParametrosPln_Campo ;
      private String[] H00F03_A2015ParametrosPln_Aba ;
      private int[] H00F03_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] H00F03_n847ParametrosPln_AreaTrabalhoCod ;
      private int[] H00F03_A848ParametrosPln_Codigo ;
      private long[] H00F04_AGRID_nRecordCount ;
      private String[] H00F05_A6AreaTrabalho_Descricao ;
      private bool[] H00F05_n6AreaTrabalho_Descricao ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33ParametrosPln_AreaTrabalhoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54ParametrosPln_AbaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37ParametrosPln_CampoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV41ParametrosPln_ColunaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45ParametrosPln_LinhaTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV49DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwparametrosplanilhas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00F03( IGxContext context ,
                                             String AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ,
                                             String AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 ,
                                             bool AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ,
                                             String AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ,
                                             String AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 ,
                                             bool AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ,
                                             String AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ,
                                             String AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 ,
                                             int AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ,
                                             String AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ,
                                             String AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ,
                                             String AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba ,
                                             String AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ,
                                             String AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo ,
                                             String AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ,
                                             String AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ,
                                             int AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha ,
                                             int AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ,
                                             String A849ParametrosPln_Campo ,
                                             String A6AreaTrabalho_Descricao ,
                                             int A847ParametrosPln_AreaTrabalhoCod ,
                                             String A2015ParametrosPln_Aba ,
                                             String A850ParametrosPln_Coluna ,
                                             int A851ParametrosPln_Linha ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [18] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[AreaTrabalho_Descricao], T1.[ParametrosPln_Linha], T1.[ParametrosPln_Coluna], T1.[ParametrosPln_Campo], T1.[ParametrosPln_Aba], T1.[ParametrosPln_AreaTrabalhoCod] AS ParametrosPln_AreaTrabalhoCod, T1.[ParametrosPln_Codigo]";
         sFromString = " FROM ([ParametrosPlanilhas] T1 WITH (NOLOCK) LEFT JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[ParametrosPln_AreaTrabalhoCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 + '%')";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 + '%')";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( (0==AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (0==AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_AreaTrabalhoCod] = @AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_AreaTrabalhoCod] = @AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Aba] like @lV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Aba] like @lV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Aba] = @AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Aba] = @AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like @lV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like @lV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] = @AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] = @AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Coluna] like @lV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Coluna] like @lV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Coluna] = @AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Coluna] = @AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Linha] >= @AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Linha] >= @AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Linha] <= @AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Linha] <= @AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ParametrosPln_AreaTrabalhoCod]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ParametrosPln_AreaTrabalhoCod] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ParametrosPln_Aba]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ParametrosPln_Aba] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ParametrosPln_Campo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ParametrosPln_Campo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ParametrosPln_Coluna]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ParametrosPln_Coluna] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ParametrosPln_Linha]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ParametrosPln_Linha] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ParametrosPln_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00F04( IGxContext context ,
                                             String AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ,
                                             String AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 ,
                                             bool AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ,
                                             String AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ,
                                             String AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 ,
                                             bool AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ,
                                             String AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ,
                                             String AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 ,
                                             int AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ,
                                             String AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ,
                                             String AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ,
                                             String AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba ,
                                             String AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ,
                                             String AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo ,
                                             String AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ,
                                             String AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ,
                                             int AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha ,
                                             int AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ,
                                             String A849ParametrosPln_Campo ,
                                             String A6AreaTrabalho_Descricao ,
                                             int A847ParametrosPln_AreaTrabalhoCod ,
                                             String A2015ParametrosPln_Aba ,
                                             String A850ParametrosPln_Coluna ,
                                             int A851ParametrosPln_Linha ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [13] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ParametrosPlanilhas] T1 WITH (NOLOCK) LEFT JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[ParametrosPln_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV61WWParametrosPlanilhasDS_1_Dynamicfiltersselector1, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWParametrosPlanilhasDS_2_Parametrospln_campo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV62WWParametrosPlanilhasDS_2_Parametrospln_campo1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( AV63WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWParametrosPlanilhasDS_4_Dynamicfiltersselector2, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWParametrosPlanilhasDS_5_Parametrospln_campo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV65WWParametrosPlanilhasDS_5_Parametrospln_campo2 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV66WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV67WWParametrosPlanilhasDS_7_Dynamicfiltersselector3, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWParametrosPlanilhasDS_8_Parametrospln_campo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV68WWParametrosPlanilhasDS_8_Parametrospln_campo3 + '%')";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( (0==AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (0==AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_AreaTrabalhoCod] = @AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_AreaTrabalhoCod] = @AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Aba] like @lV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Aba] like @lV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Aba] = @AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Aba] = @AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like @lV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like @lV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] = @AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] = @AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Coluna] like @lV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Coluna] like @lV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Coluna] = @AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Coluna] = @AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Linha] >= @AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Linha] >= @AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Linha] <= @AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Linha] <= @AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00F03(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] );
               case 2 :
                     return conditional_H00F04(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00F02 ;
          prmH00F02 = new Object[] {
          } ;
          Object[] prmH00F05 ;
          prmH00F05 = new Object[] {
          new Object[] {"@ParametrosPln_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00F03 ;
          prmH00F03 = new Object[] {
          new Object[] {"@lV62WWParametrosPlanilhasDS_2_Parametrospln_campo1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV65WWParametrosPlanilhasDS_5_Parametrospln_campo2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV68WWParametrosPlanilhasDS_8_Parametrospln_campo3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba",SqlDbType.Char,30,0} ,
          new Object[] {"@AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel",SqlDbType.Char,30,0} ,
          new Object[] {"@lV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna",SqlDbType.Char,2,0} ,
          new Object[] {"@AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha",SqlDbType.Int,6,0} ,
          new Object[] {"@AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00F04 ;
          prmH00F04 = new Object[] {
          new Object[] {"@lV62WWParametrosPlanilhasDS_2_Parametrospln_campo1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV65WWParametrosPlanilhasDS_5_Parametrospln_campo2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV68WWParametrosPlanilhasDS_8_Parametrospln_campo3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV69WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV70WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV71WWParametrosPlanilhasDS_11_Tfparametrospln_aba",SqlDbType.Char,30,0} ,
          new Object[] {"@AV72WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel",SqlDbType.Char,30,0} ,
          new Object[] {"@lV73WWParametrosPlanilhasDS_13_Tfparametrospln_campo",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV74WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV75WWParametrosPlanilhasDS_15_Tfparametrospln_coluna",SqlDbType.Char,2,0} ,
          new Object[] {"@AV76WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV77WWParametrosPlanilhasDS_17_Tfparametrospln_linha",SqlDbType.Int,6,0} ,
          new Object[] {"@AV78WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00F02", "SELECT [AreaTrabalho_Codigo] AS ParametrosPln_AreaTrabalhoCod, [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F02,0,0,true,false )
             ,new CursorDef("H00F03", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F03,11,0,true,false )
             ,new CursorDef("H00F04", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F04,1,0,true,false )
             ,new CursorDef("H00F05", "SELECT [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ParametrosPln_AreaTrabalhoCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F05,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 30) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
