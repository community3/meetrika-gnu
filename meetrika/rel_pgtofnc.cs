/*
               File: REL_PgtoFnc
        Description: Pagamento Funcion�rio
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:15:18.43
       Program type: HTTP procedure
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_pgtofnc : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV12Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV13ContagemResultado_LiqLogCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV66Liquidadas = (bool)(BooleanUtil.Val(GetNextPar( )));
                  AV77Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV50ContagemResultado_ContadorFM = (int)(NumberUtil.Val( GetNextPar( ), "."));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public rel_pgtofnc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public rel_pgtofnc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_ContagemResultado_LiqLogCod ,
                           bool aP2_Liquidadas ,
                           int aP3_Contrato_Codigo ,
                           int aP4_ContagemResultado_ContadorFM )
      {
         this.AV12Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV13ContagemResultado_LiqLogCod = aP1_ContagemResultado_LiqLogCod;
         this.AV66Liquidadas = aP2_Liquidadas;
         this.AV77Contrato_Codigo = aP3_Contrato_Codigo;
         this.AV50ContagemResultado_ContadorFM = aP4_ContagemResultado_ContadorFM;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_ContagemResultado_LiqLogCod ,
                                 bool aP2_Liquidadas ,
                                 int aP3_Contrato_Codigo ,
                                 int aP4_ContagemResultado_ContadorFM )
      {
         rel_pgtofnc objrel_pgtofnc;
         objrel_pgtofnc = new rel_pgtofnc();
         objrel_pgtofnc.AV12Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objrel_pgtofnc.AV13ContagemResultado_LiqLogCod = aP1_ContagemResultado_LiqLogCod;
         objrel_pgtofnc.AV66Liquidadas = aP2_Liquidadas;
         objrel_pgtofnc.AV77Contrato_Codigo = aP3_Contrato_Codigo;
         objrel_pgtofnc.AV50ContagemResultado_ContadorFM = aP4_ContagemResultado_ContadorFM;
         objrel_pgtofnc.context.SetSubmitInitialConfig(context);
         objrel_pgtofnc.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_pgtofnc);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_pgtofnc)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 1, 15840, 12240, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
            if ( AV66Liquidadas )
            {
               AV67Situacao = "Liquidadas";
            }
            else
            {
               AV67Situacao = "Para liquidar";
            }
            AV68Codigos.FromXml(AV69WebSession.Get("SdtCodigos"), "Collection");
            AV65Hoje = DateTimeUtil.ServerNow( context, "DEFAULT");
            /* Execute user subroutine: 'PRINTFILTERS' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTDATA' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'PRINTFOOTER' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            AV51Quantidade = 0;
            AV52PFTotalB = 0;
            AV53GlsTotal = 0;
            AV54TotalBruto = 0;
            AV55TotalLiquido = 0;
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H960( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTFILTERS' Routine */
         if ( (0==AV50ContagemResultado_ContadorFM) )
         {
            AV25Funcionario = "Todos";
         }
         else
         {
            /* Execute user subroutine: 'NOMEFUNCIONARIO' */
            S121 ();
            if (returnInSub) return;
         }
         if ( (0==AV12Contratada_AreaTrabalhoCod) )
         {
            AV49AreaTrabalho = "Todas";
         }
         else
         {
            /* Using cursor P00962 */
            pr_default.execute(0, new Object[] {AV12Contratada_AreaTrabalhoCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A5AreaTrabalho_Codigo = P00962_A5AreaTrabalho_Codigo[0];
               A6AreaTrabalho_Descricao = P00962_A6AreaTrabalho_Descricao[0];
               AV49AreaTrabalho = A6AreaTrabalho_Descricao;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
         }
         if ( (0==AV77Contrato_Codigo) )
         {
            AV74Contrato = "Todos";
         }
         else
         {
            /* Using cursor P00963 */
            pr_default.execute(1, new Object[] {AV77Contrato_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A74Contrato_Codigo = P00963_A74Contrato_Codigo[0];
               A77Contrato_Numero = P00963_A77Contrato_Numero[0];
               AV74Contrato = A77Contrato_Numero;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
      }

      protected void S131( )
      {
         /* 'PRINTDATA' Routine */
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV68Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor P00965 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            A52Contratada_AreaTrabalhoCod = P00965_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00965_n52Contratada_AreaTrabalhoCod[0];
            A601ContagemResultado_Servico = P00965_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00965_n601ContagemResultado_Servico[0];
            A456ContagemResultado_Codigo = P00965_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = P00965_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00965_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P00965_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00965_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = P00965_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00965_n1553ContagemResultado_CntSrvCod[0];
            A1051ContagemResultado_GlsValor = P00965_A1051ContagemResultado_GlsValor[0];
            n1051ContagemResultado_GlsValor = P00965_n1051ContagemResultado_GlsValor[0];
            A801ContagemResultado_ServicoSigla = P00965_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00965_n801ContagemResultado_ServicoSigla[0];
            A493ContagemResultado_DemandaFM = P00965_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00965_n493ContagemResultado_DemandaFM[0];
            A1046ContagemResultado_Agrupador = P00965_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00965_n1046ContagemResultado_Agrupador[0];
            A457ContagemResultado_Demanda = P00965_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00965_n457ContagemResultado_Demanda[0];
            A53Contratada_AreaTrabalhoDes = P00965_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P00965_n53Contratada_AreaTrabalhoDes[0];
            A584ContagemResultado_ContadorFM = P00965_A584ContagemResultado_ContadorFM[0];
            A1480ContagemResultado_CstUntUltima = P00965_A1480ContagemResultado_CstUntUltima[0];
            A682ContagemResultado_PFBFMUltima = P00965_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P00965_A684ContagemResultado_PFBFSUltima[0];
            A566ContagemResultado_DataUltCnt = P00965_A566ContagemResultado_DataUltCnt[0];
            A584ContagemResultado_ContadorFM = P00965_A584ContagemResultado_ContadorFM[0];
            A1480ContagemResultado_CstUntUltima = P00965_A1480ContagemResultado_CstUntUltima[0];
            A682ContagemResultado_PFBFMUltima = P00965_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P00965_A684ContagemResultado_PFBFSUltima[0];
            A566ContagemResultado_DataUltCnt = P00965_A566ContagemResultado_DataUltCnt[0];
            A52Contratada_AreaTrabalhoCod = P00965_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00965_n52Contratada_AreaTrabalhoCod[0];
            A53Contratada_AreaTrabalhoDes = P00965_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P00965_n53Contratada_AreaTrabalhoDes[0];
            A601ContagemResultado_Servico = P00965_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00965_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00965_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00965_n801ContagemResultado_ServicoSigla[0];
            AV16ContagemResultado_StatusDmnDescription = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
            AV56ContratadaCod = A490ContagemResultado_ContratadaCod;
            AV57ContadorFM = A584ContagemResultado_ContadorFM;
            if ( ( A1480ContagemResultado_CstUntUltima > Convert.ToDecimal( 0 )) )
            {
               AV60CstUntNrm = A1480ContagemResultado_CstUntUltima;
            }
            else
            {
               new prc_cstuntprdnrm(context ).execute( ref  A1553ContagemResultado_CntSrvCod, ref  A584ContagemResultado_ContadorFM, ref  AV60CstUntNrm, out  AV59CalculoPFinal) ;
            }
            if ( StringUtil.StrCmp(AV59CalculoPFinal, "BM") == 0 )
            {
               AV58PFB = A682ContagemResultado_PFBFMUltima;
            }
            else if ( StringUtil.StrCmp(AV59CalculoPFinal, "MB") == 0 )
            {
               if ( ( A684ContagemResultado_PFBFSUltima > Convert.ToDecimal( 0 )) )
               {
                  if ( A684ContagemResultado_PFBFSUltima < A682ContagemResultado_PFBFMUltima )
                  {
                     AV58PFB = A684ContagemResultado_PFBFSUltima;
                  }
                  else
                  {
                     AV58PFB = A682ContagemResultado_PFBFMUltima;
                  }
               }
               else
               {
                  AV58PFB = A682ContagemResultado_PFBFMUltima;
               }
            }
            else
            {
               AV58PFB = A682ContagemResultado_PFBFMUltima;
            }
            AV61ValorB = (decimal)(AV60CstUntNrm*AV58PFB);
            AV61ValorB = NumberUtil.Round( AV61ValorB, 2);
            AV64ValorL = AV61ValorB;
            AV51Quantidade = (short)(AV51Quantidade+1);
            AV52PFTotalB = (decimal)(AV52PFTotalB+AV58PFB);
            AV53GlsTotal = (decimal)(AV53GlsTotal+A1051ContagemResultado_GlsValor);
            AV54TotalBruto = (decimal)(AV54TotalBruto+AV61ValorB);
            AV72Codigo = A456ContagemResultado_Codigo;
            AV57ContadorFM = A584ContagemResultado_ContadorFM;
            /* Execute user subroutine: 'PAGAMENTOSREALIZADOS' */
            S146 ();
            if ( returnInSub )
            {
               pr_default.close(2);
               returnInSub = true;
               if (true) return;
            }
            AV55TotalLiquido = (decimal)(AV55TotalLiquido+AV64ValorL);
            AV54TotalBruto = NumberUtil.Round( AV54TotalBruto, 2);
            AV53GlsTotal = NumberUtil.Round( AV53GlsTotal, 2);
            AV55TotalLiquido = NumberUtil.Round( AV55TotalLiquido, 2);
            /* Execute user subroutine: 'BEFOREPRINTLINE' */
            S156 ();
            if ( returnInSub )
            {
               pr_default.close(2);
               returnInSub = true;
               if (true) return;
            }
            H960( false, 18) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 7, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A1046ContagemResultado_Agrupador, "@!")), 122, Gx_line+2, 178, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")), 586, Gx_line+2, 642, Gx_line+16, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A53Contratada_AreaTrabalhoDes, "@!")), 5, Gx_line+2, 117, Gx_line+17, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")), 183, Gx_line+2, 295, Gx_line+17, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, "")), 300, Gx_line+2, 412, Gx_line+17, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( A566ContagemResultado_DataUltCnt, "99/99/99"), 417, Gx_line+2, 473, Gx_line+17, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV16ContagemResultado_StatusDmnDescription, "")), 478, Gx_line+2, 590, Gx_line+17, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A1051ContagemResultado_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.999")), 710, Gx_line+2, 766, Gx_line+17, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV58PFB, "ZZ,ZZZ,ZZ9.999")), 648, Gx_line+2, 706, Gx_line+17, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV64ValorL, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 768, Gx_line+2, 833, Gx_line+17, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV73Asterisco, "")), 833, Gx_line+0, 845, Gx_line+15, 1+256, 0, 0, 1) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+18);
            /* Execute user subroutine: 'AFTERPRINTLINE' */
            S166 ();
            if ( returnInSub )
            {
               pr_default.close(2);
               returnInSub = true;
               if (true) return;
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
         H960( false, 67) ;
         getPrinter().GxDrawRect(0, Gx_line+7, 843, Gx_line+57, 1, 0, 0, 0, 1, 245, 245, 245, 0, 0, 0, 0, 0, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("", 283, Gx_line+33, 383, Gx_line+50, 0+256, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV51Quantidade), "ZZ9")), 133, Gx_line+33, 156, Gx_line+51, 2+256, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV52PFTotalB, "ZZ,ZZZ,ZZ9.999")), 242, Gx_line+33, 314, Gx_line+51, 2, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV54TotalBruto, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 392, Gx_line+33, 489, Gx_line+51, 2, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV53GlsTotal, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 575, Gx_line+33, 628, Gx_line+51, 2, 0, 0, 0) ;
         getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV55TotalLiquido, "ZZZ,ZZZ,ZZZ,ZZ9.99")), 708, Gx_line+33, 805, Gx_line+51, 2, 0, 0, 0) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText("Qtde:", 92, Gx_line+33, 132, Gx_line+51, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Total Und:", 167, Gx_line+33, 243, Gx_line+51, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Bruto R$:", 325, Gx_line+33, 392, Gx_line+51, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Glosas R$:", 492, Gx_line+33, 572, Gx_line+51, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("Liquido R$:", 633, Gx_line+33, 715, Gx_line+51, 0+256, 0, 0, 0) ;
         getPrinter().GxDrawText("T O T A I S:", 17, Gx_line+17, 100, Gx_line+35, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+67);
      }

      protected void S156( )
      {
         /* 'BEFOREPRINTLINE' Routine */
      }

      protected void S166( )
      {
         /* 'AFTERPRINTLINE' Routine */
      }

      protected void S171( )
      {
         /* 'PRINTFOOTER' Routine */
      }

      protected void S121( )
      {
         /* 'NOMEFUNCIONARIO' Routine */
         /* Using cursor P00966 */
         pr_default.execute(3, new Object[] {AV50ContagemResultado_ContadorFM});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A57Usuario_PessoaCod = P00966_A57Usuario_PessoaCod[0];
            A1Usuario_Codigo = P00966_A1Usuario_Codigo[0];
            A58Usuario_PessoaNom = P00966_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00966_n58Usuario_PessoaNom[0];
            A58Usuario_PessoaNom = P00966_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00966_n58Usuario_PessoaNom[0];
            AV25Funcionario = A58Usuario_PessoaNom;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
      }

      protected void S146( )
      {
         /* 'PAGAMENTOSREALIZADOS' Routine */
         AV73Asterisco = "";
         /* Using cursor P00967 */
         pr_default.execute(4, new Object[] {AV72Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A1371ContagemResultadoLiqLogOS_OSCod = P00967_A1371ContagemResultadoLiqLogOS_OSCod[0];
            A1372ContagemResultadoLiqLogOS_UserCod = P00967_A1372ContagemResultadoLiqLogOS_UserCod[0];
            A1375ContagemResultadoLiqLogOS_Valor = P00967_A1375ContagemResultadoLiqLogOS_Valor[0];
            A1033ContagemResultadoLiqLog_Codigo = P00967_A1033ContagemResultadoLiqLog_Codigo[0];
            A1370ContagemResultadoLiqLogOS_Codigo = P00967_A1370ContagemResultadoLiqLogOS_Codigo[0];
            if ( A1372ContagemResultadoLiqLogOS_UserCod == AV57ContadorFM )
            {
               AV64ValorL = (decimal)(AV64ValorL-A1375ContagemResultadoLiqLogOS_Valor);
               AV73Asterisco = "*";
            }
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void H960( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxDrawLine(0, Gx_line+3, 842, Gx_line+3, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV65Hoje, "99/99/99 99:99"), 383, Gx_line+11, 463, Gx_line+26, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(AV9WWPContext.gxTpr_Username, 0, Gx_line+11, 340, Gx_line+26, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 733, Gx_line+11, 772, Gx_line+26, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 792, Gx_line+11, 841, Gx_line+25, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 775, Gx_line+11, 789, Gx_line+25, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("P�gina", 700, Gx_line+11, 735, Gx_line+25, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+34);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("-", 217, Gx_line+95, 225, Gx_line+109, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Funcion�rio", 0, Gx_line+61, 160, Gx_line+75, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("�rea de Trabalho", 0, Gx_line+78, 160, Gx_line+92, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV20FilterContagemResultado_DataUltCntDescription, "")), 0, Gx_line+95, 160, Gx_line+110, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Contrato", 0, Gx_line+111, 160, Gx_line+125, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV25Funcionario, "@!")), 150, Gx_line+61, 768, Gx_line+76, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV49AreaTrabalho, "@!")), 150, Gx_line+78, 768, Gx_line+93, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV63ContagemResultado_DataUltCntTo, "99/99/99"), 233, Gx_line+95, 291, Gx_line+110, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV21ContagemResultado_DataUltCnt, "99/99/99"), 150, Gx_line+95, 208, Gx_line+110, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV74Contrato, "")), 150, Gx_line+111, 768, Gx_line+126, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV67Situacao, "")), 349, Gx_line+33, 496, Gx_line+53, 1+256, 0, 0, 1) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 18, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Extrato de Funcion�rios", 5, Gx_line+5, 841, Gx_line+35, 1, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+128);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda)) )
               {
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("N� OS", 5, Gx_line+2, 165, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV46ContagemResultado_Demanda, "@!")), 158, Gx_line+0, 776, Gx_line+15, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+17);
               }
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27ContagemResultado_Agrupador)) )
               {
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Agrupador", 5, Gx_line+2, 165, Gx_line+16, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, true, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27ContagemResultado_Agrupador, "@!")), 158, Gx_line+0, 776, Gx_line+15, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+17);
               }
               getPrinter().GxDrawLine(5, Gx_line+30, 117, Gx_line+30, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(122, Gx_line+30, 178, Gx_line+30, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(183, Gx_line+30, 295, Gx_line+30, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(300, Gx_line+30, 412, Gx_line+30, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(417, Gx_line+30, 473, Gx_line+30, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(478, Gx_line+30, 580, Gx_line+30, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(597, Gx_line+30, 643, Gx_line+30, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(721, Gx_line+30, 767, Gx_line+30, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(660, Gx_line+30, 706, Gx_line+30, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(779, Gx_line+30, 832, Gx_line+30, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("�rea", 5, Gx_line+15, 117, Gx_line+29, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Agrupador", 122, Gx_line+15, 178, Gx_line+29, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Demanda", 183, Gx_line+15, 295, Gx_line+29, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("OS FM", 300, Gx_line+15, 412, Gx_line+29, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Contagem", 417, Gx_line+15, 473, Gx_line+29, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Status", 478, Gx_line+15, 590, Gx_line+29, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Servi�o", 597, Gx_line+15, 643, Gx_line+29, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("R$ Glosa", 721, Gx_line+15, 767, Gx_line+29, 2, 0, 0, 0) ;
               getPrinter().GxDrawText("Und", 659, Gx_line+15, 705, Gx_line+29, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("R$ Liq.", 778, Gx_line+15, 824, Gx_line+29, 2, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+34);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, true, 56, 14, 70, 118,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 18, 22, 35, 35, 56, 42, 12, 21, 21, 25, 37, 18, 21, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 18, 18, 37, 37, 37, 35, 64, 42, 42, 45, 45, 42, 38, 49, 45, 18, 32, 42, 35, 53, 45, 49, 42, 49, 45, 42, 38, 45, 42, 61, 42, 42, 38, 18, 18, 18, 30, 35, 21, 35, 35, 32, 35, 35, 18, 35, 35, 14, 14, 32, 14, 52, 35, 35, 35, 35, 21, 32, 18, 35, 32, 45, 32, 32, 29, 21, 16, 21, 37, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 18, 21, 35, 35, 34, 35, 16, 35, 21, 46, 23, 35, 37, 21, 46, 35, 25, 35, 21, 20, 21, 35, 34, 21, 21, 20, 23, 35, 53, 53, 53, 38, 42, 42, 42, 42, 42, 42, 63, 45, 42, 42, 42, 42, 18, 18, 18, 18, 45, 45, 49, 49, 49, 49, 49, 37, 49, 45, 45, 45, 45, 42, 42, 38, 35, 35, 35, 35, 35, 35, 56, 32, 35, 35, 35, 35, 18, 18, 18, 18, 35, 35, 35, 35, 35, 35, 35, 35, 38, 35, 35, 35, 35, 32, 35, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV67Situacao = "";
         AV68Codigos = new GxSimpleCollection();
         AV69WebSession = context.GetSession();
         AV65Hoje = (DateTime)(DateTime.MinValue);
         AV25Funcionario = "";
         AV49AreaTrabalho = "";
         scmdbuf = "";
         P00962_A5AreaTrabalho_Codigo = new int[1] ;
         P00962_A6AreaTrabalho_Descricao = new String[] {""} ;
         A6AreaTrabalho_Descricao = "";
         AV74Contrato = "";
         P00963_A74Contrato_Codigo = new int[1] ;
         P00963_A77Contrato_Numero = new String[] {""} ;
         A77Contrato_Numero = "";
         P00965_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00965_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00965_A601ContagemResultado_Servico = new int[1] ;
         P00965_n601ContagemResultado_Servico = new bool[] {false} ;
         P00965_A456ContagemResultado_Codigo = new int[1] ;
         P00965_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00965_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00965_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00965_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00965_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00965_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00965_A1051ContagemResultado_GlsValor = new decimal[1] ;
         P00965_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         P00965_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00965_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00965_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00965_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00965_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00965_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00965_A457ContagemResultado_Demanda = new String[] {""} ;
         P00965_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00965_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         P00965_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         P00965_A584ContagemResultado_ContadorFM = new int[1] ;
         P00965_A1480ContagemResultado_CstUntUltima = new decimal[1] ;
         P00965_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00965_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00965_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         A484ContagemResultado_StatusDmn = "";
         A801ContagemResultado_ServicoSigla = "";
         A493ContagemResultado_DemandaFM = "";
         A1046ContagemResultado_Agrupador = "";
         A457ContagemResultado_Demanda = "";
         A53Contratada_AreaTrabalhoDes = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         AV16ContagemResultado_StatusDmnDescription = "";
         AV59CalculoPFinal = "";
         AV73Asterisco = "";
         P00966_A57Usuario_PessoaCod = new int[1] ;
         P00966_A1Usuario_Codigo = new int[1] ;
         P00966_A58Usuario_PessoaNom = new String[] {""} ;
         P00966_n58Usuario_PessoaNom = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         P00967_A1371ContagemResultadoLiqLogOS_OSCod = new int[1] ;
         P00967_A1372ContagemResultadoLiqLogOS_UserCod = new int[1] ;
         P00967_A1375ContagemResultadoLiqLogOS_Valor = new decimal[1] ;
         P00967_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         P00967_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         AV20FilterContagemResultado_DataUltCntDescription = "";
         AV63ContagemResultado_DataUltCntTo = DateTime.MinValue;
         AV21ContagemResultado_DataUltCnt = DateTime.MinValue;
         AV46ContagemResultado_Demanda = "";
         AV27ContagemResultado_Agrupador = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.rel_pgtofnc__default(),
            new Object[][] {
                new Object[] {
               P00962_A5AreaTrabalho_Codigo, P00962_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00963_A74Contrato_Codigo, P00963_A77Contrato_Numero
               }
               , new Object[] {
               P00965_A52Contratada_AreaTrabalhoCod, P00965_n52Contratada_AreaTrabalhoCod, P00965_A601ContagemResultado_Servico, P00965_n601ContagemResultado_Servico, P00965_A456ContagemResultado_Codigo, P00965_A484ContagemResultado_StatusDmn, P00965_n484ContagemResultado_StatusDmn, P00965_A490ContagemResultado_ContratadaCod, P00965_n490ContagemResultado_ContratadaCod, P00965_A1553ContagemResultado_CntSrvCod,
               P00965_n1553ContagemResultado_CntSrvCod, P00965_A1051ContagemResultado_GlsValor, P00965_n1051ContagemResultado_GlsValor, P00965_A801ContagemResultado_ServicoSigla, P00965_n801ContagemResultado_ServicoSigla, P00965_A493ContagemResultado_DemandaFM, P00965_n493ContagemResultado_DemandaFM, P00965_A1046ContagemResultado_Agrupador, P00965_n1046ContagemResultado_Agrupador, P00965_A457ContagemResultado_Demanda,
               P00965_n457ContagemResultado_Demanda, P00965_A53Contratada_AreaTrabalhoDes, P00965_n53Contratada_AreaTrabalhoDes, P00965_A584ContagemResultado_ContadorFM, P00965_A1480ContagemResultado_CstUntUltima, P00965_A682ContagemResultado_PFBFMUltima, P00965_A684ContagemResultado_PFBFSUltima, P00965_A566ContagemResultado_DataUltCnt
               }
               , new Object[] {
               P00966_A57Usuario_PessoaCod, P00966_A1Usuario_Codigo, P00966_A58Usuario_PessoaNom, P00966_n58Usuario_PessoaNom
               }
               , new Object[] {
               P00967_A1371ContagemResultadoLiqLogOS_OSCod, P00967_A1372ContagemResultadoLiqLogOS_UserCod, P00967_A1375ContagemResultadoLiqLogOS_Valor, P00967_A1033ContagemResultadoLiqLog_Codigo, P00967_A1370ContagemResultadoLiqLogOS_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV51Quantidade ;
      private int AV12Contratada_AreaTrabalhoCod ;
      private int AV13ContagemResultado_LiqLogCod ;
      private int AV77Contrato_Codigo ;
      private int AV50ContagemResultado_ContadorFM ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A5AreaTrabalho_Codigo ;
      private int A74Contrato_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A601ContagemResultado_Servico ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A584ContagemResultado_ContadorFM ;
      private int AV56ContratadaCod ;
      private int AV57ContadorFM ;
      private int AV72Codigo ;
      private int Gx_OldLine ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private int A1371ContagemResultadoLiqLogOS_OSCod ;
      private int A1372ContagemResultadoLiqLogOS_UserCod ;
      private int A1033ContagemResultadoLiqLog_Codigo ;
      private int A1370ContagemResultadoLiqLogOS_Codigo ;
      private decimal AV52PFTotalB ;
      private decimal AV53GlsTotal ;
      private decimal AV54TotalBruto ;
      private decimal AV55TotalLiquido ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal A1480ContagemResultado_CstUntUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal AV60CstUntNrm ;
      private decimal AV58PFB ;
      private decimal AV61ValorB ;
      private decimal AV64ValorL ;
      private decimal A1375ContagemResultadoLiqLogOS_Valor ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV67Situacao ;
      private String AV25Funcionario ;
      private String AV49AreaTrabalho ;
      private String scmdbuf ;
      private String AV74Contrato ;
      private String A77Contrato_Numero ;
      private String A484ContagemResultado_StatusDmn ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A1046ContagemResultado_Agrupador ;
      private String AV59CalculoPFinal ;
      private String AV73Asterisco ;
      private String A58Usuario_PessoaNom ;
      private String AV27ContagemResultado_Agrupador ;
      private DateTime AV65Hoje ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime AV63ContagemResultado_DataUltCntTo ;
      private DateTime AV21ContagemResultado_DataUltCnt ;
      private bool entryPointCalled ;
      private bool AV66Liquidadas ;
      private bool returnInSub ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n457ContagemResultado_Demanda ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n58Usuario_PessoaNom ;
      private String A6AreaTrabalho_Descricao ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String AV16ContagemResultado_StatusDmnDescription ;
      private String AV20FilterContagemResultado_DataUltCntDescription ;
      private String AV46ContagemResultado_Demanda ;
      private IGxSession AV69WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00962_A5AreaTrabalho_Codigo ;
      private String[] P00962_A6AreaTrabalho_Descricao ;
      private int[] P00963_A74Contrato_Codigo ;
      private String[] P00963_A77Contrato_Numero ;
      private int[] P00965_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00965_n52Contratada_AreaTrabalhoCod ;
      private int[] P00965_A601ContagemResultado_Servico ;
      private bool[] P00965_n601ContagemResultado_Servico ;
      private int[] P00965_A456ContagemResultado_Codigo ;
      private String[] P00965_A484ContagemResultado_StatusDmn ;
      private bool[] P00965_n484ContagemResultado_StatusDmn ;
      private int[] P00965_A490ContagemResultado_ContratadaCod ;
      private bool[] P00965_n490ContagemResultado_ContratadaCod ;
      private int[] P00965_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00965_n1553ContagemResultado_CntSrvCod ;
      private decimal[] P00965_A1051ContagemResultado_GlsValor ;
      private bool[] P00965_n1051ContagemResultado_GlsValor ;
      private String[] P00965_A801ContagemResultado_ServicoSigla ;
      private bool[] P00965_n801ContagemResultado_ServicoSigla ;
      private String[] P00965_A493ContagemResultado_DemandaFM ;
      private bool[] P00965_n493ContagemResultado_DemandaFM ;
      private String[] P00965_A1046ContagemResultado_Agrupador ;
      private bool[] P00965_n1046ContagemResultado_Agrupador ;
      private String[] P00965_A457ContagemResultado_Demanda ;
      private bool[] P00965_n457ContagemResultado_Demanda ;
      private String[] P00965_A53Contratada_AreaTrabalhoDes ;
      private bool[] P00965_n53Contratada_AreaTrabalhoDes ;
      private int[] P00965_A584ContagemResultado_ContadorFM ;
      private decimal[] P00965_A1480ContagemResultado_CstUntUltima ;
      private decimal[] P00965_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00965_A684ContagemResultado_PFBFSUltima ;
      private DateTime[] P00965_A566ContagemResultado_DataUltCnt ;
      private int[] P00966_A57Usuario_PessoaCod ;
      private int[] P00966_A1Usuario_Codigo ;
      private String[] P00966_A58Usuario_PessoaNom ;
      private bool[] P00966_n58Usuario_PessoaNom ;
      private int[] P00967_A1371ContagemResultadoLiqLogOS_OSCod ;
      private int[] P00967_A1372ContagemResultadoLiqLogOS_UserCod ;
      private decimal[] P00967_A1375ContagemResultadoLiqLogOS_Valor ;
      private int[] P00967_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] P00967_A1370ContagemResultadoLiqLogOS_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV68Codigos ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class rel_pgtofnc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00965( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV68Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object1 ;
         GXv_Object1 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T5.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_GlsValor], T6.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_Demanda], T4.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, COALESCE( T2.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T2.[ContagemResultado_CstUntUltima], 0) AS ContagemResultado_CstUntUltima, COALESCE( T2.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T2.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T2.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt FROM ((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo], MIN([ContagemResultado_CstUntPrd]) AS ContagemResultado_CstUntUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo]";
         scmdbuf = scmdbuf + " = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T5.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV68Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T4.[AreaTrabalho_Descricao], T1.[ContagemResultado_Demanda]";
         GXv_Object1[0] = scmdbuf;
         return GXv_Object1 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_P00965(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00962 ;
          prmP00962 = new Object[] {
          new Object[] {"@AV12Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00963 ;
          prmP00963 = new Object[] {
          new Object[] {"@AV77Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00966 ;
          prmP00966 = new Object[] {
          new Object[] {"@AV50ContagemResultado_ContadorFM",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00967 ;
          prmP00967 = new Object[] {
          new Object[] {"@AV72Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00965 ;
          prmP00965 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00962", "SELECT TOP 1 [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV12Contratada_AreaTrabalhoCod ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00962,1,0,false,true )
             ,new CursorDef("P00963", "SELECT TOP 1 [Contrato_Codigo], [Contrato_Numero] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV77Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00963,1,0,false,true )
             ,new CursorDef("P00965", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00965,100,0,true,false )
             ,new CursorDef("P00966", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV50ContagemResultado_ContadorFM ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00966,1,0,false,true )
             ,new CursorDef("P00967", "SELECT [ContagemResultadoLiqLogOS_OSCod], [ContagemResultadoLiqLogOS_UserCod], [ContagemResultadoLiqLogOS_Valor], [ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_Codigo] FROM [ContagemResultadoLiqLogOS] WITH (NOLOCK) WHERE [ContagemResultadoLiqLogOS_OSCod] = @AV72Codigo ORDER BY [ContagemResultadoLiqLogOS_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00967,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((decimal[]) buf[24])[0] = rslt.getDecimal(14) ;
                ((decimal[]) buf[25])[0] = rslt.getDecimal(15) ;
                ((decimal[]) buf[26])[0] = rslt.getDecimal(16) ;
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(17) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
