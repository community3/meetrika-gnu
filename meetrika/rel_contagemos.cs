/*
               File: REL_ContagemOS
        Description: Stub for REL_ContagemOS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:14:14.68
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_contagemos : GXProcedure
   {
      public rel_contagemos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public rel_contagemos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           short aP1_ContagemResultado_StatusCnt ,
                           String aP2_GridStateXML ,
                           int aP3_ContagemResultado_LoteAceite )
      {
         this.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV3ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         this.AV4GridStateXML = aP2_GridStateXML;
         this.AV5ContagemResultado_LoteAceite = aP3_ContagemResultado_LoteAceite;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 short aP1_ContagemResultado_StatusCnt ,
                                 String aP2_GridStateXML ,
                                 int aP3_ContagemResultado_LoteAceite )
      {
         rel_contagemos objrel_contagemos;
         objrel_contagemos = new rel_contagemos();
         objrel_contagemos.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objrel_contagemos.AV3ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         objrel_contagemos.AV4GridStateXML = aP2_GridStateXML;
         objrel_contagemos.AV5ContagemResultado_LoteAceite = aP3_ContagemResultado_LoteAceite;
         objrel_contagemos.context.SetSubmitInitialConfig(context);
         objrel_contagemos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_contagemos);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_contagemos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2Contratada_AreaTrabalhoCod,(short)AV3ContagemResultado_StatusCnt,(String)AV4GridStateXML,(int)AV5ContagemResultado_LoteAceite} ;
         ClassLoader.Execute("arel_contagemos","GeneXus.Programs.arel_contagemos", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 4 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV3ContagemResultado_StatusCnt ;
      private int AV2Contratada_AreaTrabalhoCod ;
      private int AV5ContagemResultado_LoteAceite ;
      private String AV4GridStateXML ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
