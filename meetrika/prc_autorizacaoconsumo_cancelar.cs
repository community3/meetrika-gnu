/*
               File: Prc_AutorizacaoConsumo_Cancelar
        Description: Prc_Autorizacao Consumo_Cancelar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:3.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_autorizacaoconsumo_cancelar : GXProcedure
   {
      public prc_autorizacaoconsumo_cancelar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_autorizacaoconsumo_cancelar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AutorizacaoConsumo_Codigo )
      {
         this.AV8AutorizacaoConsumo_Codigo = aP0_AutorizacaoConsumo_Codigo;
         initialize();
         executePrivate();
         aP0_AutorizacaoConsumo_Codigo=this.AV8AutorizacaoConsumo_Codigo;
      }

      public int executeUdp( )
      {
         this.AV8AutorizacaoConsumo_Codigo = aP0_AutorizacaoConsumo_Codigo;
         initialize();
         executePrivate();
         aP0_AutorizacaoConsumo_Codigo=this.AV8AutorizacaoConsumo_Codigo;
         return AV8AutorizacaoConsumo_Codigo ;
      }

      public void executeSubmit( ref int aP0_AutorizacaoConsumo_Codigo )
      {
         prc_autorizacaoconsumo_cancelar objprc_autorizacaoconsumo_cancelar;
         objprc_autorizacaoconsumo_cancelar = new prc_autorizacaoconsumo_cancelar();
         objprc_autorizacaoconsumo_cancelar.AV8AutorizacaoConsumo_Codigo = aP0_AutorizacaoConsumo_Codigo;
         objprc_autorizacaoconsumo_cancelar.context.SetSubmitInitialConfig(context);
         objprc_autorizacaoconsumo_cancelar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_autorizacaoconsumo_cancelar);
         aP0_AutorizacaoConsumo_Codigo=this.AV8AutorizacaoConsumo_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_autorizacaoconsumo_cancelar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8AutorizacaoConsumo_Codigo ;
      private int aP0_AutorizacaoConsumo_Codigo ;
   }

}
