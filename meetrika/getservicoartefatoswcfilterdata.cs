/*
               File: GetServicoArtefatosWCFilterData
        Description: Get Servico Artefatos WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:41.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getservicoartefatoswcfilterdata : GXProcedure
   {
      public getservicoartefatoswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getservicoartefatoswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getservicoartefatoswcfilterdata objgetservicoartefatoswcfilterdata;
         objgetservicoartefatoswcfilterdata = new getservicoartefatoswcfilterdata();
         objgetservicoartefatoswcfilterdata.AV14DDOName = aP0_DDOName;
         objgetservicoartefatoswcfilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetservicoartefatoswcfilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetservicoartefatoswcfilterdata.AV18OptionsJson = "" ;
         objgetservicoartefatoswcfilterdata.AV21OptionsDescJson = "" ;
         objgetservicoartefatoswcfilterdata.AV23OptionIndexesJson = "" ;
         objgetservicoartefatoswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetservicoartefatoswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetservicoartefatoswcfilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getservicoartefatoswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_SERVICOARTEFATO_ARTEFATODSC") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICOARTEFATO_ARTEFATODSCOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("ServicoArtefatosWCGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ServicoArtefatosWCGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("ServicoArtefatosWCGridState"), "");
         }
         AV33GXV1 = 1;
         while ( AV33GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV33GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFSERVICOARTEFATO_ARTEFATODSC") == 0 )
            {
               AV10TFServicoArtefato_ArtefatoDsc = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFSERVICOARTEFATO_ARTEFATODSC_SEL") == 0 )
            {
               AV11TFServicoArtefato_ArtefatoDsc_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "PARM_&SERVICO_CODIGO") == 0 )
            {
               AV30Servico_Codigo = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            AV33GXV1 = (int)(AV33GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADSERVICOARTEFATO_ARTEFATODSCOPTIONS' Routine */
         AV10TFServicoArtefato_ArtefatoDsc = AV12SearchTxt;
         AV11TFServicoArtefato_ArtefatoDsc_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFServicoArtefato_ArtefatoDsc_Sel ,
                                              AV10TFServicoArtefato_ArtefatoDsc ,
                                              A1767ServicoArtefato_ArtefatoDsc ,
                                              AV30Servico_Codigo ,
                                              A155Servico_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFServicoArtefato_ArtefatoDsc = StringUtil.Concat( StringUtil.RTrim( AV10TFServicoArtefato_ArtefatoDsc), "%", "");
         /* Using cursor P00JR2 */
         pr_default.execute(0, new Object[] {AV30Servico_Codigo, lV10TFServicoArtefato_ArtefatoDsc, AV11TFServicoArtefato_ArtefatoDsc_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJR2 = false;
            A1766ServicoArtefato_ArtefatoCod = P00JR2_A1766ServicoArtefato_ArtefatoCod[0];
            A155Servico_Codigo = P00JR2_A155Servico_Codigo[0];
            A1767ServicoArtefato_ArtefatoDsc = P00JR2_A1767ServicoArtefato_ArtefatoDsc[0];
            n1767ServicoArtefato_ArtefatoDsc = P00JR2_n1767ServicoArtefato_ArtefatoDsc[0];
            A1767ServicoArtefato_ArtefatoDsc = P00JR2_A1767ServicoArtefato_ArtefatoDsc[0];
            n1767ServicoArtefato_ArtefatoDsc = P00JR2_n1767ServicoArtefato_ArtefatoDsc[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00JR2_A155Servico_Codigo[0] == A155Servico_Codigo ) && ( P00JR2_A1766ServicoArtefato_ArtefatoCod[0] == A1766ServicoArtefato_ArtefatoCod ) )
            {
               BRKJR2 = false;
               AV24count = (long)(AV24count+1);
               BRKJR2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1767ServicoArtefato_ArtefatoDsc)) )
            {
               AV16Option = A1767ServicoArtefato_ArtefatoDsc;
               AV15InsertIndex = 1;
               while ( ( AV15InsertIndex <= AV17Options.Count ) && ( StringUtil.StrCmp(((String)AV17Options.Item(AV15InsertIndex)), AV16Option) < 0 ) )
               {
                  AV15InsertIndex = (int)(AV15InsertIndex+1);
               }
               AV17Options.Add(AV16Option, AV15InsertIndex);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), AV15InsertIndex);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJR2 )
            {
               BRKJR2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFServicoArtefato_ArtefatoDsc = "";
         AV11TFServicoArtefato_ArtefatoDsc_Sel = "";
         scmdbuf = "";
         lV10TFServicoArtefato_ArtefatoDsc = "";
         A1767ServicoArtefato_ArtefatoDsc = "";
         P00JR2_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         P00JR2_A155Servico_Codigo = new int[1] ;
         P00JR2_A1767ServicoArtefato_ArtefatoDsc = new String[] {""} ;
         P00JR2_n1767ServicoArtefato_ArtefatoDsc = new bool[] {false} ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getservicoartefatoswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00JR2_A1766ServicoArtefato_ArtefatoCod, P00JR2_A155Servico_Codigo, P00JR2_A1767ServicoArtefato_ArtefatoDsc, P00JR2_n1767ServicoArtefato_ArtefatoDsc
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV33GXV1 ;
      private int AV30Servico_Codigo ;
      private int A155Servico_Codigo ;
      private int A1766ServicoArtefato_ArtefatoCod ;
      private int AV15InsertIndex ;
      private long AV24count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool BRKJR2 ;
      private bool n1767ServicoArtefato_ArtefatoDsc ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV10TFServicoArtefato_ArtefatoDsc ;
      private String AV11TFServicoArtefato_ArtefatoDsc_Sel ;
      private String lV10TFServicoArtefato_ArtefatoDsc ;
      private String A1767ServicoArtefato_ArtefatoDsc ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00JR2_A1766ServicoArtefato_ArtefatoCod ;
      private int[] P00JR2_A155Servico_Codigo ;
      private String[] P00JR2_A1767ServicoArtefato_ArtefatoDsc ;
      private bool[] P00JR2_n1767ServicoArtefato_ArtefatoDsc ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
   }

   public class getservicoartefatoswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00JR2( IGxContext context ,
                                             String AV11TFServicoArtefato_ArtefatoDsc_Sel ,
                                             String AV10TFServicoArtefato_ArtefatoDsc ,
                                             String A1767ServicoArtefato_ArtefatoDsc ,
                                             int AV30Servico_Codigo ,
                                             int A155Servico_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [3] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod, T1.[Servico_Codigo], T2.[Artefatos_Descricao] AS ServicoArtefato_ArtefatoDsc FROM ([ServicoArtefatos] T1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = T1.[ServicoArtefato_ArtefatoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Servico_Codigo] = @AV30Servico_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServicoArtefato_ArtefatoDsc_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServicoArtefato_ArtefatoDsc)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Artefatos_Descricao] like @lV10TFServicoArtefato_ArtefatoDsc)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServicoArtefato_ArtefatoDsc_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Artefatos_Descricao] = @AV11TFServicoArtefato_ArtefatoDsc_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Servico_Codigo], T1.[ServicoArtefato_ArtefatoCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00JR2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00JR2 ;
          prmP00JR2 = new Object[] {
          new Object[] {"@AV30Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFServicoArtefato_ArtefatoDsc",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV11TFServicoArtefato_ArtefatoDsc_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00JR2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JR2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getservicoartefatoswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getservicoartefatoswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getservicoartefatoswcfilterdata") )
          {
             return  ;
          }
          getservicoartefatoswcfilterdata worker = new getservicoartefatoswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
