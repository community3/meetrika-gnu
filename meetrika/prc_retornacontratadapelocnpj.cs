/*
               File: PRC_RetornaContratadaPeloCNPJ
        Description: Retorna Contratada Pelo CNPJ
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:42.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_retornacontratadapelocnpj : GXProcedure
   {
      public prc_retornacontratadapelocnpj( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_retornacontratadapelocnpj( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Contratada_CNPJ ,
                           out String aP1_Contratada_RazaoSocial ,
                           out String aP2_Contratada_BancoNome ,
                           out String aP3_Contratada_BancoNro ,
                           out String aP4_Contratada_AgenciaNome ,
                           out String aP5_Contratada_AgenciaNro ,
                           out String aP6_Contratada_ContaCorrente ,
                           out String aP7_Contratada_Sigla ,
                           out String aP8_Pessoa_IE ,
                           out int aP9_Contratada_PessoaCod )
      {
         this.AV8Contratada_CNPJ = aP0_Contratada_CNPJ;
         this.AV10Contratada_RazaoSocial = "" ;
         this.AV13Contratada_BancoNome = "" ;
         this.AV14Contratada_BancoNro = "" ;
         this.AV15Contratada_AgenciaNome = "" ;
         this.AV16Contratada_AgenciaNro = "" ;
         this.AV12Contratada_ContaCorrente = "" ;
         this.AV17Contratada_Sigla = "" ;
         this.AV18Pessoa_IE = "" ;
         this.AV11Contratada_PessoaCod = 0 ;
         initialize();
         executePrivate();
         aP1_Contratada_RazaoSocial=this.AV10Contratada_RazaoSocial;
         aP2_Contratada_BancoNome=this.AV13Contratada_BancoNome;
         aP3_Contratada_BancoNro=this.AV14Contratada_BancoNro;
         aP4_Contratada_AgenciaNome=this.AV15Contratada_AgenciaNome;
         aP5_Contratada_AgenciaNro=this.AV16Contratada_AgenciaNro;
         aP6_Contratada_ContaCorrente=this.AV12Contratada_ContaCorrente;
         aP7_Contratada_Sigla=this.AV17Contratada_Sigla;
         aP8_Pessoa_IE=this.AV18Pessoa_IE;
         aP9_Contratada_PessoaCod=this.AV11Contratada_PessoaCod;
      }

      public int executeUdp( String aP0_Contratada_CNPJ ,
                             out String aP1_Contratada_RazaoSocial ,
                             out String aP2_Contratada_BancoNome ,
                             out String aP3_Contratada_BancoNro ,
                             out String aP4_Contratada_AgenciaNome ,
                             out String aP5_Contratada_AgenciaNro ,
                             out String aP6_Contratada_ContaCorrente ,
                             out String aP7_Contratada_Sigla ,
                             out String aP8_Pessoa_IE )
      {
         this.AV8Contratada_CNPJ = aP0_Contratada_CNPJ;
         this.AV10Contratada_RazaoSocial = "" ;
         this.AV13Contratada_BancoNome = "" ;
         this.AV14Contratada_BancoNro = "" ;
         this.AV15Contratada_AgenciaNome = "" ;
         this.AV16Contratada_AgenciaNro = "" ;
         this.AV12Contratada_ContaCorrente = "" ;
         this.AV17Contratada_Sigla = "" ;
         this.AV18Pessoa_IE = "" ;
         this.AV11Contratada_PessoaCod = 0 ;
         initialize();
         executePrivate();
         aP1_Contratada_RazaoSocial=this.AV10Contratada_RazaoSocial;
         aP2_Contratada_BancoNome=this.AV13Contratada_BancoNome;
         aP3_Contratada_BancoNro=this.AV14Contratada_BancoNro;
         aP4_Contratada_AgenciaNome=this.AV15Contratada_AgenciaNome;
         aP5_Contratada_AgenciaNro=this.AV16Contratada_AgenciaNro;
         aP6_Contratada_ContaCorrente=this.AV12Contratada_ContaCorrente;
         aP7_Contratada_Sigla=this.AV17Contratada_Sigla;
         aP8_Pessoa_IE=this.AV18Pessoa_IE;
         aP9_Contratada_PessoaCod=this.AV11Contratada_PessoaCod;
         return AV11Contratada_PessoaCod ;
      }

      public void executeSubmit( String aP0_Contratada_CNPJ ,
                                 out String aP1_Contratada_RazaoSocial ,
                                 out String aP2_Contratada_BancoNome ,
                                 out String aP3_Contratada_BancoNro ,
                                 out String aP4_Contratada_AgenciaNome ,
                                 out String aP5_Contratada_AgenciaNro ,
                                 out String aP6_Contratada_ContaCorrente ,
                                 out String aP7_Contratada_Sigla ,
                                 out String aP8_Pessoa_IE ,
                                 out int aP9_Contratada_PessoaCod )
      {
         prc_retornacontratadapelocnpj objprc_retornacontratadapelocnpj;
         objprc_retornacontratadapelocnpj = new prc_retornacontratadapelocnpj();
         objprc_retornacontratadapelocnpj.AV8Contratada_CNPJ = aP0_Contratada_CNPJ;
         objprc_retornacontratadapelocnpj.AV10Contratada_RazaoSocial = "" ;
         objprc_retornacontratadapelocnpj.AV13Contratada_BancoNome = "" ;
         objprc_retornacontratadapelocnpj.AV14Contratada_BancoNro = "" ;
         objprc_retornacontratadapelocnpj.AV15Contratada_AgenciaNome = "" ;
         objprc_retornacontratadapelocnpj.AV16Contratada_AgenciaNro = "" ;
         objprc_retornacontratadapelocnpj.AV12Contratada_ContaCorrente = "" ;
         objprc_retornacontratadapelocnpj.AV17Contratada_Sigla = "" ;
         objprc_retornacontratadapelocnpj.AV18Pessoa_IE = "" ;
         objprc_retornacontratadapelocnpj.AV11Contratada_PessoaCod = 0 ;
         objprc_retornacontratadapelocnpj.context.SetSubmitInitialConfig(context);
         objprc_retornacontratadapelocnpj.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_retornacontratadapelocnpj);
         aP1_Contratada_RazaoSocial=this.AV10Contratada_RazaoSocial;
         aP2_Contratada_BancoNome=this.AV13Contratada_BancoNome;
         aP3_Contratada_BancoNro=this.AV14Contratada_BancoNro;
         aP4_Contratada_AgenciaNome=this.AV15Contratada_AgenciaNome;
         aP5_Contratada_AgenciaNro=this.AV16Contratada_AgenciaNro;
         aP6_Contratada_ContaCorrente=this.AV12Contratada_ContaCorrente;
         aP7_Contratada_Sigla=this.AV17Contratada_Sigla;
         aP8_Pessoa_IE=this.AV18Pessoa_IE;
         aP9_Contratada_PessoaCod=this.AV11Contratada_PessoaCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_retornacontratadapelocnpj)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11Contratada_PessoaCod = 0;
         AV10Contratada_RazaoSocial = "";
         AV13Contratada_BancoNome = "";
         AV14Contratada_BancoNro = "";
         AV15Contratada_AgenciaNome = "";
         AV16Contratada_AgenciaNro = "";
         AV12Contratada_ContaCorrente = "";
         /* Using cursor P001S2 */
         pr_default.execute(0, new Object[] {AV8Contratada_CNPJ});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A37Pessoa_Docto = P001S2_A37Pessoa_Docto[0];
            A34Pessoa_Codigo = P001S2_A34Pessoa_Codigo[0];
            A35Pessoa_Nome = P001S2_A35Pessoa_Nome[0];
            A518Pessoa_IE = P001S2_A518Pessoa_IE[0];
            n518Pessoa_IE = P001S2_n518Pessoa_IE[0];
            AV11Contratada_PessoaCod = A34Pessoa_Codigo;
            AV10Contratada_RazaoSocial = A35Pessoa_Nome;
            AV18Pessoa_IE = A518Pessoa_IE;
            /* Using cursor P001S3 */
            pr_default.execute(1, new Object[] {AV11Contratada_PessoaCod});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A40Contratada_PessoaCod = P001S3_A40Contratada_PessoaCod[0];
               A342Contratada_BancoNome = P001S3_A342Contratada_BancoNome[0];
               n342Contratada_BancoNome = P001S3_n342Contratada_BancoNome[0];
               A343Contratada_BancoNro = P001S3_A343Contratada_BancoNro[0];
               n343Contratada_BancoNro = P001S3_n343Contratada_BancoNro[0];
               A344Contratada_AgenciaNome = P001S3_A344Contratada_AgenciaNome[0];
               n344Contratada_AgenciaNome = P001S3_n344Contratada_AgenciaNome[0];
               A345Contratada_AgenciaNro = P001S3_A345Contratada_AgenciaNro[0];
               n345Contratada_AgenciaNro = P001S3_n345Contratada_AgenciaNro[0];
               A51Contratada_ContaCorrente = P001S3_A51Contratada_ContaCorrente[0];
               n51Contratada_ContaCorrente = P001S3_n51Contratada_ContaCorrente[0];
               A438Contratada_Sigla = P001S3_A438Contratada_Sigla[0];
               A39Contratada_Codigo = P001S3_A39Contratada_Codigo[0];
               AV13Contratada_BancoNome = A342Contratada_BancoNome;
               AV14Contratada_BancoNro = A343Contratada_BancoNro;
               AV15Contratada_AgenciaNome = A344Contratada_AgenciaNome;
               AV16Contratada_AgenciaNro = A345Contratada_AgenciaNro;
               AV12Contratada_ContaCorrente = A51Contratada_ContaCorrente;
               AV17Contratada_Sigla = A438Contratada_Sigla;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001S2_A37Pessoa_Docto = new String[] {""} ;
         P001S2_A34Pessoa_Codigo = new int[1] ;
         P001S2_A35Pessoa_Nome = new String[] {""} ;
         P001S2_A518Pessoa_IE = new String[] {""} ;
         P001S2_n518Pessoa_IE = new bool[] {false} ;
         A37Pessoa_Docto = "";
         A35Pessoa_Nome = "";
         A518Pessoa_IE = "";
         P001S3_A40Contratada_PessoaCod = new int[1] ;
         P001S3_A342Contratada_BancoNome = new String[] {""} ;
         P001S3_n342Contratada_BancoNome = new bool[] {false} ;
         P001S3_A343Contratada_BancoNro = new String[] {""} ;
         P001S3_n343Contratada_BancoNro = new bool[] {false} ;
         P001S3_A344Contratada_AgenciaNome = new String[] {""} ;
         P001S3_n344Contratada_AgenciaNome = new bool[] {false} ;
         P001S3_A345Contratada_AgenciaNro = new String[] {""} ;
         P001S3_n345Contratada_AgenciaNro = new bool[] {false} ;
         P001S3_A51Contratada_ContaCorrente = new String[] {""} ;
         P001S3_n51Contratada_ContaCorrente = new bool[] {false} ;
         P001S3_A438Contratada_Sigla = new String[] {""} ;
         P001S3_A39Contratada_Codigo = new int[1] ;
         A342Contratada_BancoNome = "";
         A343Contratada_BancoNro = "";
         A344Contratada_AgenciaNome = "";
         A345Contratada_AgenciaNro = "";
         A51Contratada_ContaCorrente = "";
         A438Contratada_Sigla = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_retornacontratadapelocnpj__default(),
            new Object[][] {
                new Object[] {
               P001S2_A37Pessoa_Docto, P001S2_A34Pessoa_Codigo, P001S2_A35Pessoa_Nome, P001S2_A518Pessoa_IE, P001S2_n518Pessoa_IE
               }
               , new Object[] {
               P001S3_A40Contratada_PessoaCod, P001S3_A342Contratada_BancoNome, P001S3_n342Contratada_BancoNome, P001S3_A343Contratada_BancoNro, P001S3_n343Contratada_BancoNro, P001S3_A344Contratada_AgenciaNome, P001S3_n344Contratada_AgenciaNome, P001S3_A345Contratada_AgenciaNro, P001S3_n345Contratada_AgenciaNro, P001S3_A51Contratada_ContaCorrente,
               P001S3_n51Contratada_ContaCorrente, P001S3_A438Contratada_Sigla, P001S3_A39Contratada_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV11Contratada_PessoaCod ;
      private int A34Pessoa_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A39Contratada_Codigo ;
      private String AV8Contratada_CNPJ ;
      private String AV10Contratada_RazaoSocial ;
      private String AV13Contratada_BancoNome ;
      private String AV14Contratada_BancoNro ;
      private String AV15Contratada_AgenciaNome ;
      private String AV16Contratada_AgenciaNro ;
      private String AV12Contratada_ContaCorrente ;
      private String scmdbuf ;
      private String A35Pessoa_Nome ;
      private String A518Pessoa_IE ;
      private String AV18Pessoa_IE ;
      private String A342Contratada_BancoNome ;
      private String A343Contratada_BancoNro ;
      private String A344Contratada_AgenciaNome ;
      private String A345Contratada_AgenciaNro ;
      private String A51Contratada_ContaCorrente ;
      private String A438Contratada_Sigla ;
      private String AV17Contratada_Sigla ;
      private bool n518Pessoa_IE ;
      private bool n342Contratada_BancoNome ;
      private bool n343Contratada_BancoNro ;
      private bool n344Contratada_AgenciaNome ;
      private bool n345Contratada_AgenciaNro ;
      private bool n51Contratada_ContaCorrente ;
      private String A37Pessoa_Docto ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P001S2_A37Pessoa_Docto ;
      private int[] P001S2_A34Pessoa_Codigo ;
      private String[] P001S2_A35Pessoa_Nome ;
      private String[] P001S2_A518Pessoa_IE ;
      private bool[] P001S2_n518Pessoa_IE ;
      private int[] P001S3_A40Contratada_PessoaCod ;
      private String[] P001S3_A342Contratada_BancoNome ;
      private bool[] P001S3_n342Contratada_BancoNome ;
      private String[] P001S3_A343Contratada_BancoNro ;
      private bool[] P001S3_n343Contratada_BancoNro ;
      private String[] P001S3_A344Contratada_AgenciaNome ;
      private bool[] P001S3_n344Contratada_AgenciaNome ;
      private String[] P001S3_A345Contratada_AgenciaNro ;
      private bool[] P001S3_n345Contratada_AgenciaNro ;
      private String[] P001S3_A51Contratada_ContaCorrente ;
      private bool[] P001S3_n51Contratada_ContaCorrente ;
      private String[] P001S3_A438Contratada_Sigla ;
      private int[] P001S3_A39Contratada_Codigo ;
      private String aP1_Contratada_RazaoSocial ;
      private String aP2_Contratada_BancoNome ;
      private String aP3_Contratada_BancoNro ;
      private String aP4_Contratada_AgenciaNome ;
      private String aP5_Contratada_AgenciaNro ;
      private String aP6_Contratada_ContaCorrente ;
      private String aP7_Contratada_Sigla ;
      private String aP8_Pessoa_IE ;
      private int aP9_Contratada_PessoaCod ;
   }

   public class prc_retornacontratadapelocnpj__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001S2 ;
          prmP001S2 = new Object[] {
          new Object[] {"@AV8Contratada_CNPJ",SqlDbType.Char,14,0}
          } ;
          Object[] prmP001S3 ;
          prmP001S3 = new Object[] {
          new Object[] {"@AV11Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001S2", "SELECT [Pessoa_Docto], [Pessoa_Codigo], [Pessoa_Nome], [Pessoa_IE] FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Docto] = @AV8Contratada_CNPJ ORDER BY [Pessoa_Docto] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001S2,1,0,true,true )
             ,new CursorDef("P001S3", "SELECT TOP 1 [Contratada_PessoaCod], [Contratada_BancoNome], [Contratada_BancoNro], [Contratada_AgenciaNome], [Contratada_AgenciaNro], [Contratada_ContaCorrente], [Contratada_Sigla], [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_PessoaCod] = @AV11Contratada_PessoaCod ORDER BY [Contratada_PessoaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001S3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 6) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
