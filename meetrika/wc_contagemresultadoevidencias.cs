/*
               File: WC_ContagemResultadoEvidencias
        Description: Contagem Resultado Evidencias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:4.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_contagemresultadoevidencias : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_contagemresultadoevidencias( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_contagemresultadoevidencias( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynavCtltipodocumento_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CTLTIPODOCUMENTO_CODIGO") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVCTLTIPODOCUMENTO_CODIGOC22( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_11 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_11_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_11_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV5SDT_ContagemResultadoEvidencias);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV5SDT_ContagemResultadoEvidencias, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAC22( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavCtlcontagemresultadoevidencia_codigo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlcontagemresultadoevidencia_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcontagemresultadoevidencia_codigo_Enabled), 5, 0)));
               edtavCtlcontagemresultadoevidencia_nomearq_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlcontagemresultadoevidencia_nomearq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcontagemresultadoevidencia_nomearq_Enabled), 5, 0)));
               dynavCtltipodocumento_codigo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavCtltipodocumento_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCtltipodocumento_codigo.Enabled), 5, 0)));
               edtavCtlcontagemresultadoevidencia_data_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlcontagemresultadoevidencia_data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcontagemresultadoevidencia_data_Enabled), 5, 0)));
               GXVCTLTIPODOCUMENTO_CODIGO_htmlC22( ) ;
               WSC22( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Resultado Evidencias") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031177451");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_contagemresultadoevidencias.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"Sdt_contagemresultadoevidencias", AV5SDT_ContagemResultadoEvidencias);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"Sdt_contagemresultadoevidencias", AV5SDT_ContagemResultadoEvidencias);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_11", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_11), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSDT_CONTAGEMRESULTADOEVIDENCIAS", AV5SDT_ContagemResultadoEvidencias);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSDT_CONTAGEMRESULTADOEVIDENCIAS", AV5SDT_ContagemResultadoEvidencias);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV18WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV18WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormC22( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_contagemresultadoevidencias.js", "?202031177454");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_ContagemResultadoEvidencias" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Evidencias" ;
      }

      protected void WBC20( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_contagemresultadoevidencias.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_C22( true) ;
         }
         else
         {
            wb_table1_2_C22( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_C22e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTC22( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado Evidencias", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPC20( ) ;
            }
         }
      }

      protected void WSC22( )
      {
         STARTC22( ) ;
         EVTC22( ) ;
      }

      protected void EVTC22( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11C22 */
                                    E11C22 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOARTEFATOS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12C22 */
                                    E12C22 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavCtlcontagemresultadoevidencia_codigo_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC20( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VAPAGAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VAPAGAR.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC20( ) ;
                              }
                              nGXsfl_11_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
                              SubsflControlProps_112( ) ;
                              AV21GXV1 = (short)(nGXsfl_11_idx+GRID_nFirstRecordOnPage);
                              if ( ( AV5SDT_ContagemResultadoEvidencias.Count >= AV21GXV1 ) && ( AV21GXV1 > 0 ) )
                              {
                                 AV5SDT_ContagemResultadoEvidencias.CurrentItem = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV21GXV1));
                                 AV11Apagar = cgiGet( edtavApagar_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavApagar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV11Apagar)) ? AV26Apagar_GXI : context.convertURL( context.PathToRelativeUrl( AV11Apagar))));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCtlcontagemresultadoevidencia_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13C22 */
                                          E13C22 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCtlcontagemresultadoevidencia_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14C22 */
                                          E14C22 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCtlcontagemresultadoevidencia_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15C22 */
                                          E15C22 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VAPAGAR.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCtlcontagemresultadoevidencia_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16C22 */
                                          E16C22 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCtlcontagemresultadoevidencia_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPC20( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCtlcontagemresultadoevidencia_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEC22( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormC22( ) ;
            }
         }
      }

      protected void PAC22( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "CTLTIPODOCUMENTO_CODIGO_" + sGXsfl_11_idx;
            dynavCtltipodocumento_codigo.Name = GXCCtl;
            dynavCtltipodocumento_codigo.WebTags = "";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVCTLTIPODOCUMENTO_CODIGOC22( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVCTLTIPODOCUMENTO_CODIGO_dataC22( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVCTLTIPODOCUMENTO_CODIGO_htmlC22( )
      {
         int gxdynajaxvalue ;
         GXDLVCTLTIPODOCUMENTO_CODIGO_dataC22( ) ;
         gxdynajaxindex = 1;
         dynavCtltipodocumento_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavCtltipodocumento_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVCTLTIPODOCUMENTO_CODIGO_dataC22( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00C22 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00C22_A645TipoDocumento_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00C22_A646TipoDocumento_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_112( ) ;
         while ( nGXsfl_11_idx <= nRC_GXsfl_11 )
         {
            sendrow_112( ) ;
            nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       IGxCollection AV5SDT_ContagemResultadoEvidencias ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFC22( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFC22( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlcontagemresultadoevidencia_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlcontagemresultadoevidencia_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcontagemresultadoevidencia_codigo_Enabled), 5, 0)));
         edtavCtlcontagemresultadoevidencia_nomearq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlcontagemresultadoevidencia_nomearq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcontagemresultadoevidencia_nomearq_Enabled), 5, 0)));
         dynavCtltipodocumento_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavCtltipodocumento_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCtltipodocumento_codigo.Enabled), 5, 0)));
         edtavCtlcontagemresultadoevidencia_data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlcontagemresultadoevidencia_data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcontagemresultadoevidencia_data_Enabled), 5, 0)));
      }

      protected void RFC22( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 11;
         /* Execute user event: E14C22 */
         E14C22 ();
         nGXsfl_11_idx = 1;
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
         nGXsfl_11_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_112( ) ;
            /* Execute user event: E15C22 */
            E15C22 ();
            if ( ( GRID_nCurrentRecord > 0 ) && ( GRID_nGridOutOfScope == 0 ) && ( nGXsfl_11_idx == 1 ) )
            {
               GRID_nCurrentRecord = 0;
               GRID_nGridOutOfScope = 1;
               subgrid_firstpage( ) ;
               /* Execute user event: E15C22 */
               E15C22 ();
            }
            wbEnd = 11;
            WBC20( ) ;
         }
         nGXsfl_11_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return AV5SDT_ContagemResultadoEvidencias.Count ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV5SDT_ContagemResultadoEvidencias, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV5SDT_ContagemResultadoEvidencias, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV5SDT_ContagemResultadoEvidencias, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV5SDT_ContagemResultadoEvidencias, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV5SDT_ContagemResultadoEvidencias, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPC20( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtlcontagemresultadoevidencia_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlcontagemresultadoevidencia_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcontagemresultadoevidencia_codigo_Enabled), 5, 0)));
         edtavCtlcontagemresultadoevidencia_nomearq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlcontagemresultadoevidencia_nomearq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcontagemresultadoevidencia_nomearq_Enabled), 5, 0)));
         dynavCtltipodocumento_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavCtltipodocumento_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavCtltipodocumento_codigo.Enabled), 5, 0)));
         edtavCtlcontagemresultadoevidencia_data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlcontagemresultadoevidencia_data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlcontagemresultadoevidencia_data_Enabled), 5, 0)));
         GXVCTLTIPODOCUMENTO_CODIGO_htmlC22( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13C22 */
         E13C22 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"Sdt_contagemresultadoevidencias"), AV5SDT_ContagemResultadoEvidencias);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSDT_CONTAGEMRESULTADOEVIDENCIAS"), AV5SDT_ContagemResultadoEvidencias);
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_11 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_11"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            nRC_GXsfl_11 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_11"), ",", "."));
            nGXsfl_11_fel_idx = 0;
            while ( nGXsfl_11_fel_idx < nRC_GXsfl_11 )
            {
               nGXsfl_11_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_fel_idx+1));
               sGXsfl_11_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_112( ) ;
               AV21GXV1 = (short)(nGXsfl_11_fel_idx+GRID_nFirstRecordOnPage);
               if ( ( AV5SDT_ContagemResultadoEvidencias.Count >= AV21GXV1 ) && ( AV21GXV1 > 0 ) )
               {
                  AV5SDT_ContagemResultadoEvidencias.CurrentItem = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV21GXV1));
                  AV11Apagar = cgiGet( edtavApagar_Internalname);
               }
            }
            if ( nGXsfl_11_fel_idx == 0 )
            {
               nGXsfl_11_idx = 1;
               sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
               SubsflControlProps_112( ) ;
            }
            nGXsfl_11_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13C22 */
         E13C22 ();
         if (returnInSub) return;
      }

      protected void E13C22( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV18WWPContext) ;
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV15ContagemResultado_Codigo = (int)(NumberUtil.Val( AV6Websession.Get("Codigo"), "."));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ContagemResultado_Codigo), 6, 0)));
         /* Execute user subroutine: 'ARTEFATOS' */
         S112 ();
         if (returnInSub) return;
      }

      protected void E14C22( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV5SDT_ContagemResultadoEvidencias.FromXml(AV6Websession.Get("ArquivosEvd"), "");
         gx_BV11 = true;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV5SDT_ContagemResultadoEvidencias", AV5SDT_ContagemResultadoEvidencias);
      }

      private void E15C22( )
      {
         /* Load Routine */
         AV21GXV1 = 1;
         while ( AV21GXV1 <= AV5SDT_ContagemResultadoEvidencias.Count )
         {
            AV5SDT_ContagemResultadoEvidencias.CurrentItem = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV21GXV1));
            AV11Apagar = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavApagar_Internalname, AV11Apagar);
            AV26Apagar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            if ( ((SdtSDT_ContagemResultadoEvidencias_Arquivo)(AV5SDT_ContagemResultadoEvidencias.CurrentItem)).gxTpr_Contagemresultadoevidencia_codigo < 0 )
            {
               edtavApagar_Visible = 0;
               edtavCtlcontagemresultadoevidencia_nomearq_Fontstrikethru = 1;
               dynavCtltipodocumento_codigo.FontStrikethru = 1;
               edtavCtlcontagemresultadoevidencia_data_Fontstrikethru = 1;
            }
            else
            {
               edtavApagar_Visible = 1;
               edtavCtlcontagemresultadoevidencia_nomearq_Fontstrikethru = 0;
               dynavCtltipodocumento_codigo.FontStrikethru = 0;
               edtavCtlcontagemresultadoevidencia_data_Fontstrikethru = 0;
            }
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 11;
            }
            if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
            {
               sendrow_112( ) ;
               GRID_nEOF = 0;
               GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
               if ( GRID_nCurrentRecord + 1 >= subGrid_Recordcount( ) )
               {
                  GRID_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
               }
            }
            GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_11_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(11, GridRow);
            }
            AV21GXV1 = (short)(AV21GXV1+1);
         }
      }

      protected void E16C22( )
      {
         AV21GXV1 = (short)(nGXsfl_11_idx+GRID_nFirstRecordOnPage);
         if ( AV5SDT_ContagemResultadoEvidencias.Count >= AV21GXV1 )
         {
            AV5SDT_ContagemResultadoEvidencias.CurrentItem = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV21GXV1));
         }
         /* Apagar_Click Routine */
         AV13i = (short)(AV5SDT_ContagemResultadoEvidencias.IndexOf(((SdtSDT_ContagemResultadoEvidencias_Arquivo)(AV5SDT_ContagemResultadoEvidencias.CurrentItem))));
         if ( (0==((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV13i)).gxTpr_Contagemresultadoevidencia_codigo) )
         {
            AV5SDT_ContagemResultadoEvidencias.RemoveItem(AV13i);
            gx_BV11 = true;
         }
         else
         {
            ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV13i)).gxTpr_Contagemresultadoevidencia_codigo = (int)(((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV13i)).gxTpr_Contagemresultadoevidencia_codigo*-1);
         }
         AV6Websession.Set("ArquivosEvd", AV5SDT_ContagemResultadoEvidencias.ToXml(false, true, "SDT_ContagemResultadoEvidencias", "GxEv3Up14_Meetrika"));
         context.DoAjaxRefreshCmp(sPrefix);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV5SDT_ContagemResultadoEvidencias", AV5SDT_ContagemResultadoEvidencias);
         nGXsfl_11_bak_idx = nGXsfl_11_idx;
         gxgrGrid_refresh( subGrid_Rows, AV5SDT_ContagemResultadoEvidencias, sPrefix) ;
         nGXsfl_11_idx = nGXsfl_11_bak_idx;
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
      }

      protected void E11C22( )
      {
         /* 'DoInsert' Routine */
         AV16Window.Height = 270;
         AV16Window.Width = 660;
         AV16Window.Url = formatLink("wp_selarquivo.aspx") ;
         AV16Window.SetReturnParms(new Object[] {});
         context.NewWindow(AV16Window);
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S112( )
      {
         /* 'ARTEFATOS' Routine */
         AV27GXLvl63 = 0;
         /* Using cursor H00C23 */
         pr_default.execute(1, new Object[] {AV15ContagemResultado_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1772ContagemResultadoArtefato_OSCod = H00C23_A1772ContagemResultadoArtefato_OSCod[0];
            AV27GXLvl63 = 1;
            imgArtefatos_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgArtefatos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgArtefatos_Visible), 5, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV27GXLvl63 == 0 )
         {
            imgArtefatos_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgArtefatos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgArtefatos_Visible), 5, 0)));
         }
      }

      protected void E12C22( )
      {
         /* 'DoArtefatos' Routine */
         context.PopUp(formatLink("wp_linkartefatos.aspx") + "?" + UrlEncode("" +AV15ContagemResultado_Codigo) + "," + UrlEncode("" +AV18WWPContext.gxTpr_Userid), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void wb_table1_2_C22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:20px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbanexos_Internalname, "(Tamanho dos arquivos m�ximo esperado 30MB cada)", "", "", lblTbanexos_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WC_ContagemResultadoEvidencias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_C22( true) ;
         }
         else
         {
            wb_table2_8_C22( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_C22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_C22e( true) ;
         }
         else
         {
            wb_table1_2_C22e( false) ;
         }
      }

      protected void wb_table2_8_C22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 30, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"11\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavApagar_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contagem Resultado Evidencia_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo Documento") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Upload") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV11Apagar));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavApagar_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlcontagemresultadoevidencia_codigo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Fontstrikethru", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlcontagemresultadoevidencia_nomearq_Fontstrikethru), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlcontagemresultadoevidencia_nomearq_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Fontstrikethru", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynavCtltipodocumento_codigo.FontStrikethru), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynavCtltipodocumento_codigo.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Fontstrikethru", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlcontagemresultadoevidencia_data_Fontstrikethru), 1, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlcontagemresultadoevidencia_data_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 11 )
         {
            wbEnd = 0;
            nRC_GXsfl_11 = (short)(nGXsfl_11_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               AV21GXV1 = nGXsfl_11_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top")+"\">") ;
            wb_table3_18_C22( true) ;
         }
         else
         {
            wb_table3_18_C22( false) ;
         }
         return  ;
      }

      protected void wb_table3_18_C22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_C22e( true) ;
         }
         else
         {
            wb_table2_8_C22e( false) ;
         }
      }

      protected void wb_table3_18_C22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "", 0, "center", "", 1, 10, sStyleString, "none", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "35d9ce8e-0cd2-4075-b379-43e1e9bf02ad", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Anexar arquivo / link", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WC_ContagemResultadoEvidencias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgArtefatos_Internalname, context.GetImagePath( "b2566579-dad1-42f5-8fd2-1897d2dc25a5", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgArtefatos_Visible, 1, "", "Anexar artefatos", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgArtefatos_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOARTEFATOS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WC_ContagemResultadoEvidencias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_18_C22e( true) ;
         }
         else
         {
            wb_table3_18_C22e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAC22( ) ;
         WSC22( ) ;
         WEC22( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAC22( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_contagemresultadoevidencias");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAC22( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
         }
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAC22( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSC22( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSC22( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEC22( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031177510");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("wc_contagemresultadoevidencias.js", "?202031177510");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_112( )
      {
         edtavApagar_Internalname = sPrefix+"vAPAGAR_"+sGXsfl_11_idx;
         edtavCtlcontagemresultadoevidencia_codigo_Internalname = sPrefix+"CTLCONTAGEMRESULTADOEVIDENCIA_CODIGO_"+sGXsfl_11_idx;
         edtavCtlcontagemresultadoevidencia_nomearq_Internalname = sPrefix+"CTLCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_"+sGXsfl_11_idx;
         dynavCtltipodocumento_codigo_Internalname = sPrefix+"CTLTIPODOCUMENTO_CODIGO_"+sGXsfl_11_idx;
         edtavCtlcontagemresultadoevidencia_data_Internalname = sPrefix+"CTLCONTAGEMRESULTADOEVIDENCIA_DATA_"+sGXsfl_11_idx;
      }

      protected void SubsflControlProps_fel_112( )
      {
         edtavApagar_Internalname = sPrefix+"vAPAGAR_"+sGXsfl_11_fel_idx;
         edtavCtlcontagemresultadoevidencia_codigo_Internalname = sPrefix+"CTLCONTAGEMRESULTADOEVIDENCIA_CODIGO_"+sGXsfl_11_fel_idx;
         edtavCtlcontagemresultadoevidencia_nomearq_Internalname = sPrefix+"CTLCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_"+sGXsfl_11_fel_idx;
         dynavCtltipodocumento_codigo_Internalname = sPrefix+"CTLTIPODOCUMENTO_CODIGO_"+sGXsfl_11_fel_idx;
         edtavCtlcontagemresultadoevidencia_data_Internalname = sPrefix+"CTLCONTAGEMRESULTADOEVIDENCIA_DATA_"+sGXsfl_11_fel_idx;
      }

      protected void sendrow_112( )
      {
         SubsflControlProps_112( ) ;
         WBC20( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_11_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_11_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_11_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavApagar_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavApagar_Enabled!=0)&&(edtavApagar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 12,'"+sPrefix+"',false,'',11)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV11Apagar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV11Apagar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV26Apagar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV11Apagar)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavApagar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV11Apagar)) ? AV26Apagar_GXI : context.PathToRelativeUrl( AV11Apagar)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavApagar_Visible,(short)1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavApagar_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVAPAGAR.CLICK."+sGXsfl_11_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV11Apagar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlcontagemresultadoevidencia_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV21GXV1)).gxTpr_Contagemresultadoevidencia_codigo), 6, 0, ",", "")),((edtavCtlcontagemresultadoevidencia_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV21GXV1)).gxTpr_Contagemresultadoevidencia_codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV21GXV1)).gxTpr_Contagemresultadoevidencia_codigo), "ZZZZZ9")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlcontagemresultadoevidencia_codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCtlcontagemresultadoevidencia_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlcontagemresultadoevidencia_nomearq_Internalname,StringUtil.RTrim( ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV21GXV1)).gxTpr_Contagemresultadoevidencia_nomearq),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlcontagemresultadoevidencia_nomearq_Jsonclick,(short)0,(String)"Attribute",(String)((edtavCtlcontagemresultadoevidencia_nomearq_Fontstrikethru==1) ? "text-decoration:line-through;" : ""),(String)ROClassString,(String)"",(short)-1,(int)edtavCtlcontagemresultadoevidencia_nomearq_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GXVCTLTIPODOCUMENTO_CODIGO_htmlC22( ) ;
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_11_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CTLTIPODOCUMENTO_CODIGO_" + sGXsfl_11_idx;
               dynavCtltipodocumento_codigo.Name = GXCCtl;
               dynavCtltipodocumento_codigo.WebTags = "";
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynavCtltipodocumento_codigo,(String)dynavCtltipodocumento_codigo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV21GXV1)).gxTpr_Tipodocumento_codigo), 6, 0)),(short)1,(String)dynavCtltipodocumento_codigo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,dynavCtltipodocumento_codigo.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",((dynavCtltipodocumento_codigo.FontStrikethru==1) ? "text-decoration:line-through;" : ""),(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
            dynavCtltipodocumento_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV21GXV1)).gxTpr_Tipodocumento_codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavCtltipodocumento_codigo_Internalname, "Values", (String)(dynavCtltipodocumento_codigo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlcontagemresultadoevidencia_data_Internalname,context.localUtil.TToC( ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV21GXV1)).gxTpr_Contagemresultadoevidencia_data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV5SDT_ContagemResultadoEvidencias.Item(AV21GXV1)).gxTpr_Contagemresultadoevidencia_data, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlcontagemresultadoevidencia_data_Jsonclick,(short)0,(String)"Attribute",(String)((edtavCtlcontagemresultadoevidencia_data_Fontstrikethru==1) ? "text-decoration:line-through;" : ""),(String)ROClassString,(String)"",(short)-1,(int)edtavCtlcontagemresultadoevidencia_data_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GridContainer.AddRow(GridRow);
            nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         /* End function sendrow_112 */
      }

      protected void init_default_properties( )
      {
         lblTbanexos_Internalname = sPrefix+"TBANEXOS";
         edtavApagar_Internalname = sPrefix+"vAPAGAR";
         edtavCtlcontagemresultadoevidencia_codigo_Internalname = sPrefix+"CTLCONTAGEMRESULTADOEVIDENCIA_CODIGO";
         edtavCtlcontagemresultadoevidencia_nomearq_Internalname = sPrefix+"CTLCONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
         dynavCtltipodocumento_codigo_Internalname = sPrefix+"CTLTIPODOCUMENTO_CODIGO";
         edtavCtlcontagemresultadoevidencia_data_Internalname = sPrefix+"CTLCONTAGEMRESULTADOEVIDENCIA_DATA";
         imgInsert_Internalname = sPrefix+"INSERT";
         imgArtefatos_Internalname = sPrefix+"ARTEFATOS";
         tblTable2_Internalname = sPrefix+"TABLE2";
         tblTablemain_Internalname = sPrefix+"TABLEMAIN";
         tblTable1_Internalname = sPrefix+"TABLE1";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavCtlcontagemresultadoevidencia_data_Jsonclick = "";
         dynavCtltipodocumento_codigo_Jsonclick = "";
         edtavCtlcontagemresultadoevidencia_nomearq_Jsonclick = "";
         edtavCtlcontagemresultadoevidencia_codigo_Jsonclick = "";
         edtavApagar_Jsonclick = "";
         edtavApagar_Enabled = 1;
         imgArtefatos_Visible = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavCtlcontagemresultadoevidencia_data_Enabled = 0;
         edtavCtlcontagemresultadoevidencia_data_Fontstrikethru = 0;
         dynavCtltipodocumento_codigo.Enabled = 0;
         dynavCtltipodocumento_codigo.FontStrikethru = 0;
         edtavCtlcontagemresultadoevidencia_nomearq_Enabled = 0;
         edtavCtlcontagemresultadoevidencia_nomearq_Fontstrikethru = 0;
         edtavCtlcontagemresultadoevidencia_codigo_Enabled = 0;
         edtavApagar_Visible = -1;
         subGrid_Class = "WorkWith";
         subGrid_Backcolorstyle = 3;
         edtavCtlcontagemresultadoevidencia_data_Enabled = -1;
         dynavCtltipodocumento_codigo.Enabled = -1;
         edtavCtlcontagemresultadoevidencia_nomearq_Enabled = -1;
         edtavCtlcontagemresultadoevidencia_codigo_Enabled = -1;
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null},{av:'sPrefix',nv:''}],oparms:[{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null}]}");
         setEventMetadata("VAPAGAR.CLICK","{handler:'E16C22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null},{av:'sPrefix',nv:''}],oparms:[{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null}]}");
         setEventMetadata("'DOINSERT'","{handler:'E11C22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'DOARTEFATOS'","{handler:'E12C22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null},{av:'sPrefix',nv:''},{av:'AV15ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null}],oparms:[{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null}],oparms:[{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null}],oparms:[{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null}],oparms:[{av:'AV5SDT_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',grid:11,pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV5SDT_ContagemResultadoEvidencias = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_Meetrika", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV18WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV11Apagar = "";
         AV26Apagar_GXI = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00C22_A645TipoDocumento_Codigo = new int[1] ;
         H00C22_A646TipoDocumento_Nome = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         AV6Websession = context.GetSession();
         GridRow = new GXWebRow();
         AV16Window = new GXWindow();
         H00C23_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         H00C23_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         sStyleString = "";
         lblTbanexos_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         TempTags = "";
         ClassString = "";
         StyleString = "";
         imgInsert_Jsonclick = "";
         imgArtefatos_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_contagemresultadoevidencias__default(),
            new Object[][] {
                new Object[] {
               H00C22_A645TipoDocumento_Codigo, H00C22_A646TipoDocumento_Nome
               }
               , new Object[] {
               H00C23_A1769ContagemResultadoArtefato_Codigo, H00C23_A1772ContagemResultadoArtefato_OSCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlcontagemresultadoevidencia_codigo_Enabled = 0;
         edtavCtlcontagemresultadoevidencia_nomearq_Enabled = 0;
         dynavCtltipodocumento_codigo.Enabled = 0;
         edtavCtlcontagemresultadoevidencia_data_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_11 ;
      private short nGXsfl_11_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short AV21GXV1 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_11_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short nGXsfl_11_fel_idx=1 ;
      private short edtavCtlcontagemresultadoevidencia_nomearq_Fontstrikethru ;
      private short edtavCtlcontagemresultadoevidencia_data_Fontstrikethru ;
      private short AV13i ;
      private short nGXsfl_11_bak_idx=1 ;
      private short AV27GXLvl63 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int edtavCtlcontagemresultadoevidencia_codigo_Enabled ;
      private int edtavCtlcontagemresultadoevidencia_nomearq_Enabled ;
      private int edtavCtlcontagemresultadoevidencia_data_Enabled ;
      private int AV15ContagemResultado_Codigo ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GRID_nGridOutOfScope ;
      private int edtavApagar_Visible ;
      private int A1772ContagemResultadoArtefato_OSCod ;
      private int imgArtefatos_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavApagar_Enabled ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_11_idx="0001" ;
      private String GXKey ;
      private String edtavCtlcontagemresultadoevidencia_codigo_Internalname ;
      private String edtavCtlcontagemresultadoevidencia_nomearq_Internalname ;
      private String dynavCtltipodocumento_codigo_Internalname ;
      private String edtavCtlcontagemresultadoevidencia_data_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavApagar_Internalname ;
      private String GXCCtl ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String sGXsfl_11_fel_idx="0001" ;
      private String imgArtefatos_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String lblTbanexos_Internalname ;
      private String lblTbanexos_Jsonclick ;
      private String tblTablemain_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTable2_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String imgArtefatos_Jsonclick ;
      private String edtavApagar_Jsonclick ;
      private String ROClassString ;
      private String edtavCtlcontagemresultadoevidencia_codigo_Jsonclick ;
      private String edtavCtlcontagemresultadoevidencia_nomearq_Jsonclick ;
      private String dynavCtltipodocumento_codigo_Jsonclick ;
      private String edtavCtlcontagemresultadoevidencia_data_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool gx_BV11 ;
      private bool AV11Apagar_IsBlob ;
      private String AV26Apagar_GXI ;
      private String AV11Apagar ;
      private IGxSession AV6Websession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavCtltipodocumento_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00C22_A645TipoDocumento_Codigo ;
      private String[] H00C22_A646TipoDocumento_Nome ;
      private int[] H00C23_A1769ContagemResultadoArtefato_Codigo ;
      private int[] H00C23_A1772ContagemResultadoArtefato_OSCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV5SDT_ContagemResultadoEvidencias ;
      private GXWindow AV16Window ;
      private wwpbaseobjects.SdtWWPContext AV18WWPContext ;
   }

   public class wc_contagemresultadoevidencias__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00C22 ;
          prmH00C22 = new Object[] {
          } ;
          Object[] prmH00C23 ;
          prmH00C23 = new Object[] {
          new Object[] {"@AV15ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00C22", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) ORDER BY [TipoDocumento_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C22,0,0,true,false )
             ,new CursorDef("H00C23", "SELECT TOP 1 [ContagemResultadoArtefato_Codigo], [ContagemResultadoArtefato_OSCod] FROM [ContagemResultadoArtefato] WITH (NOLOCK) WHERE [ContagemResultadoArtefato_OSCod] = @AV15ContagemResultado_Codigo ORDER BY [ContagemResultadoArtefato_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C23,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
