/*
               File: PRC_ContratoVigencia
        Description: PRC_Contrato Vigencia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:43.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contratovigencia : GXProcedure
   {
      public prc_contratovigencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contratovigencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo ,
                           out DateTime aP1_Contrato_DataVigenciaInicio ,
                           out DateTime aP2_Contrato_DataVigenciaTermino )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV9Contrato_DataVigenciaInicio = DateTime.MinValue ;
         this.AV10Contrato_DataVigenciaTermino = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP1_Contrato_DataVigenciaInicio=this.AV9Contrato_DataVigenciaInicio;
         aP2_Contrato_DataVigenciaTermino=this.AV10Contrato_DataVigenciaTermino;
      }

      public DateTime executeUdp( int aP0_Contrato_Codigo ,
                                  out DateTime aP1_Contrato_DataVigenciaInicio )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV9Contrato_DataVigenciaInicio = DateTime.MinValue ;
         this.AV10Contrato_DataVigenciaTermino = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP1_Contrato_DataVigenciaInicio=this.AV9Contrato_DataVigenciaInicio;
         aP2_Contrato_DataVigenciaTermino=this.AV10Contrato_DataVigenciaTermino;
         return AV10Contrato_DataVigenciaTermino ;
      }

      public void executeSubmit( int aP0_Contrato_Codigo ,
                                 out DateTime aP1_Contrato_DataVigenciaInicio ,
                                 out DateTime aP2_Contrato_DataVigenciaTermino )
      {
         prc_contratovigencia objprc_contratovigencia;
         objprc_contratovigencia = new prc_contratovigencia();
         objprc_contratovigencia.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_contratovigencia.AV9Contrato_DataVigenciaInicio = DateTime.MinValue ;
         objprc_contratovigencia.AV10Contrato_DataVigenciaTermino = DateTime.MinValue ;
         objprc_contratovigencia.context.SetSubmitInitialConfig(context);
         objprc_contratovigencia.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contratovigencia);
         aP1_Contrato_DataVigenciaInicio=this.AV9Contrato_DataVigenciaInicio;
         aP2_Contrato_DataVigenciaTermino=this.AV10Contrato_DataVigenciaTermino;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contratovigencia)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00BQ2 */
         pr_default.execute(0, new Object[] {AV8Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P00BQ2_A74Contrato_Codigo[0];
            A1571SaldoContrato_VigenciaInicio = P00BQ2_A1571SaldoContrato_VigenciaInicio[0];
            A1572SaldoContrato_VigenciaFim = P00BQ2_A1572SaldoContrato_VigenciaFim[0];
            A1561SaldoContrato_Codigo = P00BQ2_A1561SaldoContrato_Codigo[0];
            AV9Contrato_DataVigenciaInicio = A1571SaldoContrato_VigenciaInicio;
            AV10Contrato_DataVigenciaTermino = A1572SaldoContrato_VigenciaFim;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00BQ2_A74Contrato_Codigo = new int[1] ;
         P00BQ2_A1571SaldoContrato_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         P00BQ2_A1572SaldoContrato_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         P00BQ2_A1561SaldoContrato_Codigo = new int[1] ;
         A1571SaldoContrato_VigenciaInicio = DateTime.MinValue;
         A1572SaldoContrato_VigenciaFim = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contratovigencia__default(),
            new Object[][] {
                new Object[] {
               P00BQ2_A74Contrato_Codigo, P00BQ2_A1571SaldoContrato_VigenciaInicio, P00BQ2_A1572SaldoContrato_VigenciaFim, P00BQ2_A1561SaldoContrato_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private String scmdbuf ;
      private DateTime AV10Contrato_DataVigenciaTermino ;
      private DateTime A1571SaldoContrato_VigenciaInicio ;
      private DateTime A1572SaldoContrato_VigenciaFim ;
      private DateTime AV9Contrato_DataVigenciaInicio ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00BQ2_A74Contrato_Codigo ;
      private DateTime[] P00BQ2_A1571SaldoContrato_VigenciaInicio ;
      private DateTime[] P00BQ2_A1572SaldoContrato_VigenciaFim ;
      private int[] P00BQ2_A1561SaldoContrato_Codigo ;
      private DateTime aP1_Contrato_DataVigenciaInicio ;
      private DateTime aP2_Contrato_DataVigenciaTermino ;
   }

   public class prc_contratovigencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BQ2 ;
          prmP00BQ2 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BQ2", "SELECT TOP 1 [Contrato_Codigo], [SaldoContrato_VigenciaInicio], [SaldoContrato_VigenciaFim], [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV8Contrato_Codigo ORDER BY [SaldoContrato_VigenciaFim] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BQ2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
