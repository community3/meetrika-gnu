/*
               File: PRC_LiquidarDemandas
        Description: Lquidar Demandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:14:52.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_liquidardemandas : GXProcedure
   {
      public prc_liquidardemandas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_liquidardemandas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Usuario_Codigo )
      {
         this.AV11Usuario_Codigo = aP0_Usuario_Codigo;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.AV11Usuario_Codigo;
      }

      public int executeUdp( )
      {
         this.AV11Usuario_Codigo = aP0_Usuario_Codigo;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.AV11Usuario_Codigo;
         return AV11Usuario_Codigo ;
      }

      public void executeSubmit( ref int aP0_Usuario_Codigo )
      {
         prc_liquidardemandas objprc_liquidardemandas;
         objprc_liquidardemandas = new prc_liquidardemandas();
         objprc_liquidardemandas.AV11Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_liquidardemandas.context.SetSubmitInitialConfig(context);
         objprc_liquidardemandas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_liquidardemandas);
         aP0_Usuario_Codigo=this.AV11Usuario_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_liquidardemandas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8SDT_FiltroConsContadorFM.FromXml(AV9WebSession.Get("FiltroConsultaContador"), "");
         if ( (0==AV8SDT_FiltroConsContadorFM.gxTpr_Mes) )
         {
            AV15Character = "01/01/" + StringUtil.Substring( StringUtil.Trim( StringUtil.Str( (decimal)(AV8SDT_FiltroConsContadorFM.gxTpr_Ano), 4, 0)), 3, 2);
            AV13DataIni = context.localUtil.CToD( AV15Character, 2);
            AV15Character = "31/12/" + StringUtil.Substring( StringUtil.Trim( StringUtil.Str( (decimal)(AV8SDT_FiltroConsContadorFM.gxTpr_Ano), 4, 0)), 3, 2);
            AV14DataFim = context.localUtil.CToD( AV15Character, 2);
         }
         else
         {
            AV15Character = "01/" + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(AV8SDT_FiltroConsContadorFM.gxTpr_Mes), 10, 0)), 2, "0") + "/" + StringUtil.Substring( StringUtil.Trim( StringUtil.Str( (decimal)(AV8SDT_FiltroConsContadorFM.gxTpr_Ano), 4, 0)), 3, 2);
            AV13DataIni = context.localUtil.CToD( AV15Character, 2);
            AV14DataFim = DateTimeUtil.DateEndOfMonth( AV13DataIni);
         }
         /*
            INSERT RECORD ON TABLE ContagemResultadoLiqLog

         */
         A1034ContagemResultadoLiqLog_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         A1368ContagemResultadoLiqLog_Owner = AV11Usuario_Codigo;
         /* Using cursor P006S2 */
         pr_default.execute(0, new Object[] {A1034ContagemResultadoLiqLog_Data, A1368ContagemResultadoLiqLog_Owner});
         A1033ContagemResultadoLiqLog_Codigo = P006S2_A1033ContagemResultadoLiqLog_Codigo[0];
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoLiqLog") ;
         if ( (pr_default.getStatus(0) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         AV12ContagemResultado_LiqLogCod = A1033ContagemResultadoLiqLog_Codigo;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV8SDT_FiltroConsContadorFM.gxTpr_Areatrabalho_codigo ,
                                              AV8SDT_FiltroConsContadorFM.gxTpr_Servico ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A601ContagemResultado_Servico ,
                                              A1043ContagemResultado_LiqLogCod ,
                                              A484ContagemResultado_StatusDmn ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV13DataIni ,
                                              AV14DataFim ,
                                              A584ContagemResultado_ContadorFM ,
                                              AV8SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor P006S4 */
         pr_default.execute(1, new Object[] {AV13DataIni, AV14DataFim, AV8SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod, AV8SDT_FiltroConsContadorFM.gxTpr_Areatrabalho_codigo, AV8SDT_FiltroConsContadorFM.gxTpr_Servico});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P006S4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P006S4_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = P006S4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P006S4_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P006S4_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P006S4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P006S4_n601ContagemResultado_Servico[0];
            A52Contratada_AreaTrabalhoCod = P006S4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P006S4_n52Contratada_AreaTrabalhoCod[0];
            A484ContagemResultado_StatusDmn = P006S4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P006S4_n484ContagemResultado_StatusDmn[0];
            A1043ContagemResultado_LiqLogCod = P006S4_A1043ContagemResultado_LiqLogCod[0];
            n1043ContagemResultado_LiqLogCod = P006S4_n1043ContagemResultado_LiqLogCod[0];
            A566ContagemResultado_DataUltCnt = P006S4_A566ContagemResultado_DataUltCnt[0];
            A584ContagemResultado_ContadorFM = P006S4_A584ContagemResultado_ContadorFM[0];
            A52Contratada_AreaTrabalhoCod = P006S4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P006S4_n52Contratada_AreaTrabalhoCod[0];
            A601ContagemResultado_Servico = P006S4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P006S4_n601ContagemResultado_Servico[0];
            A566ContagemResultado_DataUltCnt = P006S4_A566ContagemResultado_DataUltCnt[0];
            A584ContagemResultado_ContadorFM = P006S4_A584ContagemResultado_ContadorFM[0];
            A1043ContagemResultado_LiqLogCod = AV12ContagemResultado_LiqLogCod;
            n1043ContagemResultado_LiqLogCod = false;
            /*
               INSERT RECORD ON TABLE ContagemResultadoLiqLogOS

            */
            A1371ContagemResultadoLiqLogOS_OSCod = A456ContagemResultado_Codigo;
            A1372ContagemResultadoLiqLogOS_UserCod = AV11Usuario_Codigo;
            A1375ContagemResultadoLiqLogOS_Valor = 0;
            /* Using cursor P006S5 */
            pr_default.execute(2, new Object[] {A1033ContagemResultadoLiqLog_Codigo, A1370ContagemResultadoLiqLogOS_Codigo, A1372ContagemResultadoLiqLogOS_UserCod, A1375ContagemResultadoLiqLogOS_Valor, A1371ContagemResultadoLiqLogOS_OSCod});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoLiqLogOS") ;
            if ( (pr_default.getStatus(2) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            /* Using cursor P006S6 */
            pr_default.execute(3, new Object[] {n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, A456ContagemResultado_Codigo});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_LiquidarDemandas");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8SDT_FiltroConsContadorFM = new SdtSDT_FiltroConsContadorFM(context);
         AV9WebSession = context.GetSession();
         AV15Character = "";
         AV13DataIni = DateTime.MinValue;
         AV14DataFim = DateTime.MinValue;
         A1034ContagemResultadoLiqLog_Data = (DateTime)(DateTime.MinValue);
         P006S2_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         Gx_emsg = "";
         scmdbuf = "";
         A484ContagemResultado_StatusDmn = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P006S4_A490ContagemResultado_ContratadaCod = new int[1] ;
         P006S4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P006S4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P006S4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P006S4_A456ContagemResultado_Codigo = new int[1] ;
         P006S4_A601ContagemResultado_Servico = new int[1] ;
         P006S4_n601ContagemResultado_Servico = new bool[] {false} ;
         P006S4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P006S4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P006S4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P006S4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P006S4_A1043ContagemResultado_LiqLogCod = new int[1] ;
         P006S4_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         P006S4_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P006S4_A584ContagemResultado_ContadorFM = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_liquidardemandas__default(),
            new Object[][] {
                new Object[] {
               P006S2_A1033ContagemResultadoLiqLog_Codigo
               }
               , new Object[] {
               P006S4_A490ContagemResultado_ContratadaCod, P006S4_n490ContagemResultado_ContratadaCod, P006S4_A1553ContagemResultado_CntSrvCod, P006S4_n1553ContagemResultado_CntSrvCod, P006S4_A456ContagemResultado_Codigo, P006S4_A601ContagemResultado_Servico, P006S4_n601ContagemResultado_Servico, P006S4_A52Contratada_AreaTrabalhoCod, P006S4_n52Contratada_AreaTrabalhoCod, P006S4_A484ContagemResultado_StatusDmn,
               P006S4_n484ContagemResultado_StatusDmn, P006S4_A1043ContagemResultado_LiqLogCod, P006S4_n1043ContagemResultado_LiqLogCod, P006S4_A566ContagemResultado_DataUltCnt, P006S4_A584ContagemResultado_ContadorFM
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV11Usuario_Codigo ;
      private int GX_INS125 ;
      private int A1368ContagemResultadoLiqLog_Owner ;
      private int A1033ContagemResultadoLiqLog_Codigo ;
      private int AV12ContagemResultado_LiqLogCod ;
      private int AV8SDT_FiltroConsContadorFM_gxTpr_Areatrabalho_codigo ;
      private int AV8SDT_FiltroConsContadorFM_gxTpr_Servico ;
      private int AV8SDT_FiltroConsContadorFM_gxTpr_Contagemresultado_contadorfmcod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A601ContagemResultado_Servico ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A584ContagemResultado_ContadorFM ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int GX_INS164 ;
      private int A1371ContagemResultadoLiqLogOS_OSCod ;
      private int A1372ContagemResultadoLiqLogOS_UserCod ;
      private int A1370ContagemResultadoLiqLogOS_Codigo ;
      private decimal A1375ContagemResultadoLiqLogOS_Valor ;
      private String AV15Character ;
      private String Gx_emsg ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private DateTime A1034ContagemResultadoLiqLog_Data ;
      private DateTime AV13DataIni ;
      private DateTime AV14DataFim ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private IGxSession AV9WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Usuario_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P006S2_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] P006S4_A490ContagemResultado_ContratadaCod ;
      private bool[] P006S4_n490ContagemResultado_ContratadaCod ;
      private int[] P006S4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P006S4_n1553ContagemResultado_CntSrvCod ;
      private int[] P006S4_A456ContagemResultado_Codigo ;
      private int[] P006S4_A601ContagemResultado_Servico ;
      private bool[] P006S4_n601ContagemResultado_Servico ;
      private int[] P006S4_A52Contratada_AreaTrabalhoCod ;
      private bool[] P006S4_n52Contratada_AreaTrabalhoCod ;
      private String[] P006S4_A484ContagemResultado_StatusDmn ;
      private bool[] P006S4_n484ContagemResultado_StatusDmn ;
      private int[] P006S4_A1043ContagemResultado_LiqLogCod ;
      private bool[] P006S4_n1043ContagemResultado_LiqLogCod ;
      private DateTime[] P006S4_A566ContagemResultado_DataUltCnt ;
      private int[] P006S4_A584ContagemResultado_ContadorFM ;
      private SdtSDT_FiltroConsContadorFM AV8SDT_FiltroConsContadorFM ;
   }

   public class prc_liquidardemandas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P006S4( IGxContext context ,
                                             int AV8SDT_FiltroConsContadorFM_gxTpr_Areatrabalho_codigo ,
                                             int AV8SDT_FiltroConsContadorFM_gxTpr_Servico ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int A601ContagemResultado_Servico ,
                                             int A1043ContagemResultado_LiqLogCod ,
                                             String A484ContagemResultado_StatusDmn ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV13DataIni ,
                                             DateTime AV14DataFim ,
                                             int A584ContagemResultado_ContadorFM ,
                                             int AV8SDT_FiltroConsContadorFM_gxTpr_Contagemresultado_contadorfmcod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [5] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T3.[Servico_Codigo] AS ContagemResultado_Servico, T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_LiqLogCod], COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T4.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_LiqLogCod] IS NULL or (T1.[ContagemResultado_LiqLogCod] = convert(int, 0)))";
         scmdbuf = scmdbuf + " and (( T1.[ContagemResultado_StatusDmn] = 'O' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L'))";
         scmdbuf = scmdbuf + " and (COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV13DataIni)";
         scmdbuf = scmdbuf + " and (COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV14DataFim)";
         scmdbuf = scmdbuf + " and (COALESCE( T4.[ContagemResultado_ContadorFM], 0) = @AV8SDT_F_1Contagemresultado_c)";
         if ( ! (0==AV8SDT_FiltroConsContadorFM_gxTpr_Areatrabalho_codigo) )
         {
            sWhereString = sWhereString + " and (T2.[Contratada_AreaTrabalhoCod] = @AV8SDT_F_2Areatrabalho_codigo)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV8SDT_FiltroConsContadorFM_gxTpr_Servico) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV8SDT_F_3Servico)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P006S4(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006S2 ;
          prmP006S2 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoLiqLog_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006S5 ;
          prmP006S5 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultadoLiqLogOS_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006S6 ;
          prmP006S6 = new Object[] {
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006S4 ;
          prmP006S4 = new Object[] {
          new Object[] {"@AV13DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV14DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV8SDT_F_1Contagemresultado_c",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8SDT_F_2Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8SDT_F_3Servico",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006S2", "INSERT INTO [ContagemResultadoLiqLog]([ContagemResultadoLiqLog_Data], [ContagemResultadoLiqLog_Owner], [ContagemResultadoLiqLog_Observacao]) VALUES(@ContagemResultadoLiqLog_Data, @ContagemResultadoLiqLog_Owner, ''); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP006S2)
             ,new CursorDef("P006S4", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006S4,1,0,true,false )
             ,new CursorDef("P006S5", "INSERT INTO [ContagemResultadoLiqLogOS]([ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_Codigo], [ContagemResultadoLiqLogOS_UserCod], [ContagemResultadoLiqLogOS_Valor], [ContagemResultadoLiqLogOS_OSCod]) VALUES(@ContagemResultadoLiqLog_Codigo, @ContagemResultadoLiqLogOS_Codigo, @ContagemResultadoLiqLogOS_UserCod, @ContagemResultadoLiqLogOS_Valor, @ContagemResultadoLiqLogOS_OSCod)", GxErrorMask.GX_NOMASK,prmP006S5)
             ,new CursorDef("P006S6", "UPDATE [ContagemResultado] SET [ContagemResultado_LiqLogCod]=@ContagemResultado_LiqLogCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006S6)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(8) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
