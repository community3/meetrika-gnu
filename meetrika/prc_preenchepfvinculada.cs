/*
               File: PRC_PreenchePFVinculada
        Description: Preenche PF da Vinculada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:10.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_preenchepfvinculada : GXProcedure
   {
      public prc_preenchepfvinculada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_preenchepfvinculada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           decimal aP1_PFB ,
                           decimal aP2_PFL )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8PFB = aP1_PFB;
         this.AV9PFL = aP2_PFL;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 decimal aP1_PFB ,
                                 decimal aP2_PFL )
      {
         prc_preenchepfvinculada objprc_preenchepfvinculada;
         objprc_preenchepfvinculada = new prc_preenchepfvinculada();
         objprc_preenchepfvinculada.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_preenchepfvinculada.AV8PFB = aP1_PFB;
         objprc_preenchepfvinculada.AV9PFL = aP2_PFL;
         objprc_preenchepfvinculada.context.SetSubmitInitialConfig(context);
         objprc_preenchepfvinculada.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_preenchepfvinculada);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_preenchepfvinculada)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00SV2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P00SV2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00SV2_n490ContagemResultado_ContratadaCod[0];
            A517ContagemResultado_Ultima = P00SV2_A517ContagemResultado_Ultima[0];
            A1326ContagemResultado_ContratadaTipoFab = P00SV2_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00SV2_n1326ContagemResultado_ContratadaTipoFab[0];
            A458ContagemResultado_PFBFS = P00SV2_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = P00SV2_n458ContagemResultado_PFBFS[0];
            A459ContagemResultado_PFLFS = P00SV2_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = P00SV2_n459ContagemResultado_PFLFS[0];
            A460ContagemResultado_PFBFM = P00SV2_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = P00SV2_n460ContagemResultado_PFBFM[0];
            A461ContagemResultado_PFLFM = P00SV2_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = P00SV2_n461ContagemResultado_PFLFM[0];
            A473ContagemResultado_DataCnt = P00SV2_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = P00SV2_A511ContagemResultado_HoraCnt[0];
            A490ContagemResultado_ContratadaCod = P00SV2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00SV2_n490ContagemResultado_ContratadaCod[0];
            A1326ContagemResultado_ContratadaTipoFab = P00SV2_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00SV2_n1326ContagemResultado_ContratadaTipoFab[0];
            if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 )
            {
               A458ContagemResultado_PFBFS = AV8PFB;
               n458ContagemResultado_PFBFS = false;
               A459ContagemResultado_PFLFS = AV9PFL;
               n459ContagemResultado_PFLFS = false;
            }
            else
            {
               A460ContagemResultado_PFBFM = AV8PFB;
               n460ContagemResultado_PFBFM = false;
               A461ContagemResultado_PFLFM = AV9PFL;
               n461ContagemResultado_PFLFM = false;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00SV3 */
            pr_default.execute(1, new Object[] {n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if (true) break;
            /* Using cursor P00SV4 */
            pr_default.execute(2, new Object[] {n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00SV2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00SV2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00SV2_A456ContagemResultado_Codigo = new int[1] ;
         P00SV2_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00SV2_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00SV2_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00SV2_A458ContagemResultado_PFBFS = new decimal[1] ;
         P00SV2_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P00SV2_A459ContagemResultado_PFLFS = new decimal[1] ;
         P00SV2_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P00SV2_A460ContagemResultado_PFBFM = new decimal[1] ;
         P00SV2_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P00SV2_A461ContagemResultado_PFLFM = new decimal[1] ;
         P00SV2_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P00SV2_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00SV2_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A1326ContagemResultado_ContratadaTipoFab = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_preenchepfvinculada__default(),
            new Object[][] {
                new Object[] {
               P00SV2_A490ContagemResultado_ContratadaCod, P00SV2_n490ContagemResultado_ContratadaCod, P00SV2_A456ContagemResultado_Codigo, P00SV2_A517ContagemResultado_Ultima, P00SV2_A1326ContagemResultado_ContratadaTipoFab, P00SV2_n1326ContagemResultado_ContratadaTipoFab, P00SV2_A458ContagemResultado_PFBFS, P00SV2_n458ContagemResultado_PFBFS, P00SV2_A459ContagemResultado_PFLFS, P00SV2_n459ContagemResultado_PFLFS,
               P00SV2_A460ContagemResultado_PFBFM, P00SV2_n460ContagemResultado_PFBFM, P00SV2_A461ContagemResultado_PFLFM, P00SV2_n461ContagemResultado_PFLFM, P00SV2_A473ContagemResultado_DataCnt, P00SV2_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private decimal AV8PFB ;
      private decimal AV9PFL ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private String scmdbuf ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool A517ContagemResultado_Ultima ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00SV2_A490ContagemResultado_ContratadaCod ;
      private bool[] P00SV2_n490ContagemResultado_ContratadaCod ;
      private int[] P00SV2_A456ContagemResultado_Codigo ;
      private bool[] P00SV2_A517ContagemResultado_Ultima ;
      private String[] P00SV2_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00SV2_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] P00SV2_A458ContagemResultado_PFBFS ;
      private bool[] P00SV2_n458ContagemResultado_PFBFS ;
      private decimal[] P00SV2_A459ContagemResultado_PFLFS ;
      private bool[] P00SV2_n459ContagemResultado_PFLFS ;
      private decimal[] P00SV2_A460ContagemResultado_PFBFM ;
      private bool[] P00SV2_n460ContagemResultado_PFBFM ;
      private decimal[] P00SV2_A461ContagemResultado_PFLFM ;
      private bool[] P00SV2_n461ContagemResultado_PFLFM ;
      private DateTime[] P00SV2_A473ContagemResultado_DataCnt ;
      private String[] P00SV2_A511ContagemResultado_HoraCnt ;
   }

   public class prc_preenchepfvinculada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SV2 ;
          prmP00SV2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00SV3 ;
          prmP00SV3 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP00SV4 ;
          prmP00SV4 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SV2", "SELECT TOP 1 T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Ultima], T3.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFLFM], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM (([ContagemResultadoContagens] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) WHERE (T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND (T1.[ContagemResultado_Ultima] = 1) ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SV2,1,0,true,true )
             ,new CursorDef("P00SV3", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_PFBFM]=@ContagemResultado_PFBFM, [ContagemResultado_PFLFM]=@ContagemResultado_PFLFM  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00SV3)
             ,new CursorDef("P00SV4", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_PFBFM]=@ContagemResultado_PFBFM, [ContagemResultado_PFLFM]=@ContagemResultado_PFLFM  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00SV4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(9) ;
                ((String[]) buf[15])[0] = rslt.getString(10, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                stmt.SetParameter(6, (DateTime)parms[9]);
                stmt.SetParameter(7, (String)parms[10]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                stmt.SetParameter(6, (DateTime)parms[9]);
                stmt.SetParameter(7, (String)parms[10]);
                return;
       }
    }

 }

}
