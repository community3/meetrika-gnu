/*
               File: type_SdtGAMRoleFilter
        Description: GAMRoleFilter
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 21:58:12.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMRoleFilter : GxUserType, IGxExternalObject
   {
      public SdtGAMRoleFilter( )
      {
         initialize();
      }

      public SdtGAMRoleFilter( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMRoleFilter_externalReference == null )
         {
            GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
         }
         returntostring = "";
         returntostring = (String)(GAMRoleFilter_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Guid
      {
         get {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            return GAMRoleFilter_externalReference.GUID ;
         }

         set {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            GAMRoleFilter_externalReference.GUID = value;
         }

      }

      public String gxTpr_Externalid
      {
         get {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            return GAMRoleFilter_externalReference.ExternalId ;
         }

         set {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            GAMRoleFilter_externalReference.ExternalId = value;
         }

      }

      public int gxTpr_Securitypolicyid
      {
         get {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            return GAMRoleFilter_externalReference.SecurityPolicyId ;
         }

         set {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            GAMRoleFilter_externalReference.SecurityPolicyId = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            return GAMRoleFilter_externalReference.Name ;
         }

         set {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            GAMRoleFilter_externalReference.Name = value;
         }

      }

      public String gxTpr_Description
      {
         get {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            return GAMRoleFilter_externalReference.Description ;
         }

         set {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            GAMRoleFilter_externalReference.Description = value;
         }

      }

      public long gxTpr_Id
      {
         get {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            return GAMRoleFilter_externalReference.Id ;
         }

         set {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            GAMRoleFilter_externalReference.Id = value;
         }

      }

      public SdtGAMDescription gxTpr_Descriptions
      {
         get {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            SdtGAMDescription intValue ;
            intValue = new SdtGAMDescription(context);
            Artech.Security.GAMDescription externalParm0 ;
            externalParm0 = GAMRoleFilter_externalReference.Descriptions;
            intValue.ExternalInstance = externalParm0;
            return intValue ;
         }

         set {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            SdtGAMDescription intValue ;
            Artech.Security.GAMDescription externalParm1 ;
            intValue = value;
            externalParm1 = (Artech.Security.GAMDescription)(intValue.ExternalInstance);
            GAMRoleFilter_externalReference.Descriptions = externalParm1;
         }

      }

      public SdtGAMProperty gxTpr_Properties
      {
         get {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            SdtGAMProperty intValue ;
            intValue = new SdtGAMProperty(context);
            Artech.Security.GAMProperty externalParm2 ;
            externalParm2 = GAMRoleFilter_externalReference.Properties;
            intValue.ExternalInstance = externalParm2;
            return intValue ;
         }

         set {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            SdtGAMProperty intValue ;
            Artech.Security.GAMProperty externalParm3 ;
            intValue = value;
            externalParm3 = (Artech.Security.GAMProperty)(intValue.ExternalInstance);
            GAMRoleFilter_externalReference.Properties = externalParm3;
         }

      }

      public bool gxTpr_Loaddescriptions
      {
         get {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            return GAMRoleFilter_externalReference.LoadDescriptions ;
         }

         set {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            GAMRoleFilter_externalReference.LoadDescriptions = value;
         }

      }

      public bool gxTpr_Loadproperties
      {
         get {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            return GAMRoleFilter_externalReference.LoadProperties ;
         }

         set {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            GAMRoleFilter_externalReference.LoadProperties = value;
         }

      }

      public int gxTpr_Start
      {
         get {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            return GAMRoleFilter_externalReference.Start ;
         }

         set {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            GAMRoleFilter_externalReference.Start = value;
         }

      }

      public int gxTpr_Limit
      {
         get {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            return GAMRoleFilter_externalReference.Limit ;
         }

         set {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            GAMRoleFilter_externalReference.Limit = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMRoleFilter_externalReference == null )
            {
               GAMRoleFilter_externalReference = new Artech.Security.GAMRoleFilter(context);
            }
            return GAMRoleFilter_externalReference ;
         }

         set {
            GAMRoleFilter_externalReference = (Artech.Security.GAMRoleFilter)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMRoleFilter GAMRoleFilter_externalReference=null ;
   }

}
