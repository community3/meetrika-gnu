/*
               File: PRC_NewTabelaDC
        Description: New Tabela DC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:43.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_newtabeladc : GXProcedure
   {
      public prc_newtabeladc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_newtabeladc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoDados_Codigo ,
                           int aP1_Tabela_SistemaCod )
      {
         this.AV12FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.AV9Tabela_SistemaCod = aP1_Tabela_SistemaCod;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_FuncaoDados_Codigo ,
                                 int aP1_Tabela_SistemaCod )
      {
         prc_newtabeladc objprc_newtabeladc;
         objprc_newtabeladc = new prc_newtabeladc();
         objprc_newtabeladc.AV12FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         objprc_newtabeladc.AV9Tabela_SistemaCod = aP1_Tabela_SistemaCod;
         objprc_newtabeladc.context.SetSubmitInitialConfig(context);
         objprc_newtabeladc.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_newtabeladc);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_newtabeladc)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Tabela_Nome = AV10WebSession.Get("FiltroRecebido");
         AV15GXLvl4 = 0;
         /* Using cursor P002A2 */
         pr_default.execute(0, new Object[] {AV8Tabela_Nome, AV9Tabela_SistemaCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A173Tabela_Nome = P002A2_A173Tabela_Nome[0];
            A190Tabela_SistemaCod = P002A2_A190Tabela_SistemaCod[0];
            A172Tabela_Codigo = P002A2_A172Tabela_Codigo[0];
            AV15GXLvl4 = 1;
            AV11Tabela_Codigo = A172Tabela_Codigo;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV15GXLvl4 == 0 )
         {
            /*
               INSERT RECORD ON TABLE Tabela

            */
            A173Tabela_Nome = AV8Tabela_Nome;
            A190Tabela_SistemaCod = AV9Tabela_SistemaCod;
            A188Tabela_ModuloCod = 0;
            n188Tabela_ModuloCod = false;
            n188Tabela_ModuloCod = true;
            A181Tabela_PaiCod = 0;
            n181Tabela_PaiCod = false;
            n181Tabela_PaiCod = true;
            A746Tabela_MelhoraCod = 0;
            n746Tabela_MelhoraCod = false;
            n746Tabela_MelhoraCod = true;
            A174Tabela_Ativo = true;
            /* Using cursor P002A3 */
            pr_default.execute(1, new Object[] {A173Tabela_Nome, A174Tabela_Ativo, n181Tabela_PaiCod, A181Tabela_PaiCod, n188Tabela_ModuloCod, A188Tabela_ModuloCod, A190Tabela_SistemaCod, n746Tabela_MelhoraCod, A746Tabela_MelhoraCod});
            A172Tabela_Codigo = P002A3_A172Tabela_Codigo[0];
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("Tabela") ;
            if ( (pr_default.getStatus(1) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            AV11Tabela_Codigo = A172Tabela_Codigo;
         }
         AV16GXLvl21 = 0;
         /* Using cursor P002A4 */
         pr_default.execute(2, new Object[] {AV12FuncaoDados_Codigo, AV11Tabela_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A172Tabela_Codigo = P002A4_A172Tabela_Codigo[0];
            A368FuncaoDados_Codigo = P002A4_A368FuncaoDados_Codigo[0];
            AV16GXLvl21 = 1;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         if ( AV16GXLvl21 == 0 )
         {
            /*
               INSERT RECORD ON TABLE FuncaoDadosTabela

            */
            A368FuncaoDados_Codigo = AV12FuncaoDados_Codigo;
            A172Tabela_Codigo = AV11Tabela_Codigo;
            /* Using cursor P002A5 */
            pr_default.execute(3, new Object[] {A368FuncaoDados_Codigo, A172Tabela_Codigo});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("FuncaoDadosTabela") ;
            if ( (pr_default.getStatus(3) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
         }
         new prc_newatributo(context ).execute(  AV11Tabela_Codigo,  AV9Tabela_SistemaCod) ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_NewTabelaDC");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8Tabela_Nome = "";
         AV10WebSession = context.GetSession();
         scmdbuf = "";
         P002A2_A173Tabela_Nome = new String[] {""} ;
         P002A2_A190Tabela_SistemaCod = new int[1] ;
         P002A2_A172Tabela_Codigo = new int[1] ;
         A173Tabela_Nome = "";
         P002A3_A172Tabela_Codigo = new int[1] ;
         Gx_emsg = "";
         P002A4_A172Tabela_Codigo = new int[1] ;
         P002A4_A368FuncaoDados_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_newtabeladc__default(),
            new Object[][] {
                new Object[] {
               P002A2_A173Tabela_Nome, P002A2_A190Tabela_SistemaCod, P002A2_A172Tabela_Codigo
               }
               , new Object[] {
               P002A3_A172Tabela_Codigo
               }
               , new Object[] {
               P002A4_A172Tabela_Codigo, P002A4_A368FuncaoDados_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV15GXLvl4 ;
      private short AV16GXLvl21 ;
      private int AV12FuncaoDados_Codigo ;
      private int AV9Tabela_SistemaCod ;
      private int A190Tabela_SistemaCod ;
      private int A172Tabela_Codigo ;
      private int AV11Tabela_Codigo ;
      private int GX_INS39 ;
      private int A188Tabela_ModuloCod ;
      private int A181Tabela_PaiCod ;
      private int A746Tabela_MelhoraCod ;
      private int A368FuncaoDados_Codigo ;
      private int GX_INS60 ;
      private String AV8Tabela_Nome ;
      private String scmdbuf ;
      private String A173Tabela_Nome ;
      private String Gx_emsg ;
      private bool n188Tabela_ModuloCod ;
      private bool n181Tabela_PaiCod ;
      private bool n746Tabela_MelhoraCod ;
      private bool A174Tabela_Ativo ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P002A2_A173Tabela_Nome ;
      private int[] P002A2_A190Tabela_SistemaCod ;
      private int[] P002A2_A172Tabela_Codigo ;
      private int[] P002A3_A172Tabela_Codigo ;
      private int[] P002A4_A172Tabela_Codigo ;
      private int[] P002A4_A368FuncaoDados_Codigo ;
   }

   public class prc_newtabeladc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002A2 ;
          prmP002A2 = new Object[] {
          new Object[] {"@AV8Tabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV9Tabela_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002A3 ;
          prmP002A3 = new Object[] {
          new Object[] {"@Tabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Tabela_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Tabela_PaiCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002A4 ;
          prmP002A4 = new Object[] {
          new Object[] {"@AV12FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002A5 ;
          prmP002A5 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002A2", "SELECT TOP 1 [Tabela_Nome], [Tabela_SistemaCod], [Tabela_Codigo] FROM [Tabela] WITH (NOLOCK) WHERE ([Tabela_Nome] = @AV8Tabela_Nome) AND ([Tabela_SistemaCod] = @AV9Tabela_SistemaCod) ORDER BY [Tabela_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002A2,1,0,false,true )
             ,new CursorDef("P002A3", "INSERT INTO [Tabela]([Tabela_Nome], [Tabela_Ativo], [Tabela_PaiCod], [Tabela_ModuloCod], [Tabela_SistemaCod], [Tabela_MelhoraCod], [Tabela_Descricao]) VALUES(@Tabela_Nome, @Tabela_Ativo, @Tabela_PaiCod, @Tabela_ModuloCod, @Tabela_SistemaCod, @Tabela_MelhoraCod, ''); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP002A3)
             ,new CursorDef("P002A4", "SELECT [Tabela_Codigo], [FuncaoDados_Codigo] FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV12FuncaoDados_Codigo and [Tabela_Codigo] = @AV11Tabela_Codigo ORDER BY [FuncaoDados_Codigo], [Tabela_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002A4,1,0,false,true )
             ,new CursorDef("P002A5", "INSERT INTO [FuncaoDadosTabela]([FuncaoDados_Codigo], [Tabela_Codigo]) VALUES(@FuncaoDados_Codigo, @Tabela_Codigo)", GxErrorMask.GX_NOMASK,prmP002A5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                stmt.SetParameter(5, (int)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[8]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
