/*
               File: WC_RequisitosTecnicos
        Description: Requisitos Tecnicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:6:24.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_requisitostecnicos : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_requisitostecnicos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_requisitostecnicos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Requisito_ReqCod )
      {
         this.A1999Requisito_ReqCod = aP0_Requisito_ReqCod;
         executePrivate();
         aP0_Requisito_ReqCod=this.A1999Requisito_ReqCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynRequisito_TipoReqCod = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1999Requisito_ReqCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1999Requisito_ReqCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1999Requisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1999Requisito_ReqCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1999Requisito_ReqCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"REQUISITO_TIPOREQCOD") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLAREQUISITO_TIPOREQCODQA2( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_7 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_7_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_7_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  edtRequisito_Codigo_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtRequisito_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRequisito_Codigo_Visible), 5, 0)));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  A1999Requisito_ReqCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1999Requisito_ReqCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1999Requisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1999Requisito_ReqCod), 6, 0)));
                  edtRequisito_Codigo_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtRequisito_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRequisito_Codigo_Visible), 5, 0)));
                  A2004ContagemResultadoRequisito_ReqCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2004ContagemResultadoRequisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0)));
                  A2003ContagemResultadoRequisito_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2003ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0)));
                  AV6Qtde = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6Qtde", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Qtde), 4, 0)));
                  AV11Pontos = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV11Pontos", StringUtil.LTrim( StringUtil.Str( AV11Pontos, 14, 5)));
                  A1932Requisito_Pontuacao = NumberUtil.Val( GetNextPar( ), ".");
                  n1932Requisito_Pontuacao = false;
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( A1999Requisito_ReqCod, A2004ContagemResultadoRequisito_ReqCod, A2003ContagemResultadoRequisito_OSCod, AV6Qtde, AV11Pontos, A1932Requisito_Pontuacao, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAQA2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               GXAREQUISITO_TIPOREQCOD_htmlQA2( ) ;
               WSQA2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Requisitos Tecnicos") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311762473");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "white-space: nowrap;";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_requisitostecnicos.aspx") + "?" + UrlEncode("" +A1999Requisito_ReqCod)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_7", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_7), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1999Requisito_ReqCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1999Requisito_ReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"REQUISITO_REQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1999Requisito_ReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOREQUISITO_REQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2004ContagemResultadoRequisito_ReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOREQUISITO_OSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vQTDE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6Qtde), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPONTOS", StringUtil.LTrim( StringUtil.NToC( AV11Pontos, 14, 5, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV9WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV9WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Width", StringUtil.RTrim( Dvelop_gxbootstrap_panel_Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Height", StringUtil.RTrim( Dvelop_gxbootstrap_panel_Height));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Title", StringUtil.RTrim( Dvelop_gxbootstrap_panel_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Collapsible", StringUtil.BoolToStr( Dvelop_gxbootstrap_panel_Collapsible));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Collapsed", StringUtil.BoolToStr( Dvelop_gxbootstrap_panel_Collapsed));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Autowidth", StringUtil.BoolToStr( Dvelop_gxbootstrap_panel_Autowidth));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Showcollapseicon", StringUtil.BoolToStr( Dvelop_gxbootstrap_panel_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL1_Title", StringUtil.RTrim( Confirmpanel1_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL1_Confirmationtext", StringUtil.RTrim( Confirmpanel1_Confirmationtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL1_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel1_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL1_Nobuttoncaption", StringUtil.RTrim( Confirmpanel1_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL1_Cancelbuttoncaption", StringUtil.RTrim( Confirmpanel1_Cancelbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL1_Yesbuttonposition", StringUtil.RTrim( Confirmpanel1_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"REQUISITO_CODIGO_Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRequisito_Codigo_Visible), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL1_Result", StringUtil.RTrim( Confirmpanel1_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormQA2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_requisitostecnicos.js", "?2020311762493");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_RequisitosTecnicos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Requisitos Tecnicos" ;
      }

      protected void WBQA0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_requisitostecnicos.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_QA2( true) ;
         }
         else
         {
            wb_table1_2_QA2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_QA2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONFIRMPANEL1Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"CONFIRMPANEL1Container"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
         }
         wbLoad = true;
      }

      protected void STARTQA2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Requisitos Tecnicos", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPQA0( ) ;
            }
         }
      }

      protected void WSQA2( )
      {
         STARTQA2( ) ;
         EVTQA2( ) ;
      }

      protected void EVTQA2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL1.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11QA2 */
                                    E11QA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "VUPDREQTEC.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "VUPDREQTEC.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQA0( ) ;
                              }
                              nGXsfl_7_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_7_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_7_idx), 4, 0)), 4, "0");
                              SubsflControlProps_72( ) ;
                              A1919Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( edtRequisito_Codigo_Internalname), ",", "."));
                              AV5UpdReqTec = cgiGet( edtavUpdreqtec_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdreqtec_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV5UpdReqTec)) ? AV16Updreqtec_GXI : context.convertURL( context.PathToRelativeUrl( AV5UpdReqTec))));
                              AV10DltReqTec = cgiGet( edtavDltreqtec_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDltreqtec_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV10DltReqTec)) ? AV17Dltreqtec_GXI : context.convertURL( context.PathToRelativeUrl( AV10DltReqTec))));
                              A1931Requisito_Ordem = (short)(context.localUtil.CToN( cgiGet( edtRequisito_Ordem_Internalname), ",", "."));
                              n1931Requisito_Ordem = false;
                              A1927Requisito_Titulo = cgiGet( edtRequisito_Titulo_Internalname);
                              n1927Requisito_Titulo = false;
                              dynRequisito_TipoReqCod.Name = dynRequisito_TipoReqCod_Internalname;
                              dynRequisito_TipoReqCod.CurrentValue = cgiGet( dynRequisito_TipoReqCod_Internalname);
                              A2049Requisito_TipoReqCod = (int)(NumberUtil.Val( cgiGet( dynRequisito_TipoReqCod_Internalname), "."));
                              n2049Requisito_TipoReqCod = false;
                              A1932Requisito_Pontuacao = context.localUtil.CToN( cgiGet( edtRequisito_Pontuacao_Internalname), ",", ".");
                              n1932Requisito_Pontuacao = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E12QA2 */
                                          E12QA2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E13QA2 */
                                          E13QA2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E14QA2 */
                                          E14QA2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VUPDREQTEC.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E15QA2 */
                                          E15QA2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPQA0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEQA2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormQA2( ) ;
            }
         }
      }

      protected void PAQA2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "REQUISITO_TIPOREQCOD_" + sGXsfl_7_idx;
            dynRequisito_TipoReqCod.Name = GXCCtl;
            dynRequisito_TipoReqCod.WebTags = "";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAREQUISITO_TIPOREQCODQA2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAREQUISITO_TIPOREQCOD_dataQA2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAREQUISITO_TIPOREQCOD_htmlQA2( )
      {
         int gxdynajaxvalue ;
         GXDLAREQUISITO_TIPOREQCOD_dataQA2( ) ;
         gxdynajaxindex = 1;
         dynRequisito_TipoReqCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynRequisito_TipoReqCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAREQUISITO_TIPOREQCOD_dataQA2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00QA2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00QA2_A2049Requisito_TipoReqCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00QA2_A2042TipoRequisito_Identificador[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_72( ) ;
         while ( nGXsfl_7_idx <= nRC_GXsfl_7 )
         {
            sendrow_72( ) ;
            nGXsfl_7_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_7_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_7_idx+1));
            sGXsfl_7_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_7_idx), 4, 0)), 4, "0");
            SubsflControlProps_72( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int A1999Requisito_ReqCod ,
                                       int A2004ContagemResultadoRequisito_ReqCod ,
                                       int A2003ContagemResultadoRequisito_OSCod ,
                                       short AV6Qtde ,
                                       decimal AV11Pontos ,
                                       decimal A1932Requisito_Pontuacao ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFQA2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"REQUISITO_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1931Requisito_Ordem), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_TITULO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1927Requisito_Titulo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"REQUISITO_TITULO", A1927Requisito_Titulo);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_TIPOREQCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2049Requisito_TipoReqCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"REQUISITO_TIPOREQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2049Requisito_TipoReqCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_PONTUACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1932Requisito_Pontuacao, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"REQUISITO_PONTUACAO", StringUtil.LTrim( StringUtil.NToC( A1932Requisito_Pontuacao, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFQA2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFQA2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 7;
         /* Execute user event: E13QA2 */
         E13QA2 ();
         nGXsfl_7_idx = 1;
         sGXsfl_7_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_7_idx), 4, 0)), 4, "0");
         SubsflControlProps_72( ) ;
         nGXsfl_7_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_72( ) ;
            /* Using cursor H00QA3 */
            pr_default.execute(1, new Object[] {n1999Requisito_ReqCod, A1999Requisito_ReqCod});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1932Requisito_Pontuacao = H00QA3_A1932Requisito_Pontuacao[0];
               n1932Requisito_Pontuacao = H00QA3_n1932Requisito_Pontuacao[0];
               A2049Requisito_TipoReqCod = H00QA3_A2049Requisito_TipoReqCod[0];
               n2049Requisito_TipoReqCod = H00QA3_n2049Requisito_TipoReqCod[0];
               A1927Requisito_Titulo = H00QA3_A1927Requisito_Titulo[0];
               n1927Requisito_Titulo = H00QA3_n1927Requisito_Titulo[0];
               A1931Requisito_Ordem = H00QA3_A1931Requisito_Ordem[0];
               n1931Requisito_Ordem = H00QA3_n1931Requisito_Ordem[0];
               A1919Requisito_Codigo = H00QA3_A1919Requisito_Codigo[0];
               /* Execute user event: E14QA2 */
               E14QA2 ();
               pr_default.readNext(1);
            }
            pr_default.close(1);
            wbEnd = 7;
            WBQA0( ) ;
         }
         nGXsfl_7_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPQA0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         GXAREQUISITO_TIPOREQCOD_htmlQA2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12QA2 */
         E12QA2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_7 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_7"), ",", "."));
            wcpOA1999Requisito_ReqCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1999Requisito_ReqCod"), ",", "."));
            Dvelop_gxbootstrap_panel_Width = cgiGet( sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Width");
            Dvelop_gxbootstrap_panel_Height = cgiGet( sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Height");
            Dvelop_gxbootstrap_panel_Title = cgiGet( sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Title");
            Dvelop_gxbootstrap_panel_Collapsible = StringUtil.StrToBool( cgiGet( sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Collapsible"));
            Dvelop_gxbootstrap_panel_Collapsed = StringUtil.StrToBool( cgiGet( sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Collapsed"));
            Dvelop_gxbootstrap_panel_Autowidth = StringUtil.StrToBool( cgiGet( sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Autowidth"));
            Dvelop_gxbootstrap_panel_Showcollapseicon = StringUtil.StrToBool( cgiGet( sPrefix+"DVELOP_GXBOOTSTRAP_PANEL_Showcollapseicon"));
            Confirmpanel1_Title = cgiGet( sPrefix+"CONFIRMPANEL1_Title");
            Confirmpanel1_Confirmationtext = cgiGet( sPrefix+"CONFIRMPANEL1_Confirmationtext");
            Confirmpanel1_Yesbuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL1_Yesbuttoncaption");
            Confirmpanel1_Nobuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL1_Nobuttoncaption");
            Confirmpanel1_Cancelbuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL1_Cancelbuttoncaption");
            Confirmpanel1_Yesbuttonposition = cgiGet( sPrefix+"CONFIRMPANEL1_Yesbuttonposition");
            Confirmpanel1_Result = cgiGet( sPrefix+"CONFIRMPANEL1_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12QA2 */
         E12QA2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12QA2( )
      {
         /* Start Routine */
         edtRequisito_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtRequisito_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRequisito_Codigo_Visible), 5, 0)));
      }

      protected void E13QA2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV6Qtde = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6Qtde", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Qtde), 4, 0)));
         AV11Pontos = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV11Pontos", StringUtil.LTrim( StringUtil.Str( AV11Pontos, 14, 5)));
         /* Execute user subroutine: 'OSCOD' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV9WWPContext", AV9WWPContext);
      }

      private void E14QA2( )
      {
         /* Grid_Load Routine */
         AV5UpdReqTec = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdreqtec_Internalname, AV5UpdReqTec);
         AV16Updreqtec_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         AV10DltReqTec = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDltreqtec_Internalname, AV10DltReqTec);
         AV17Dltreqtec_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         AV6Qtde = (short)(AV6Qtde+1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6Qtde", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Qtde), 4, 0)));
         AV11Pontos = (decimal)(AV11Pontos+A1932Requisito_Pontuacao);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV11Pontos", StringUtil.LTrim( StringUtil.Str( AV11Pontos, 14, 5)));
         edtRequisito_Ordem_Horizontalalignment = "center";
         Dvelop_gxbootstrap_panel_Showcollapseicon = true;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvelop_gxbootstrap_panel_Internalname, "ShowCollapseIcon", StringUtil.BoolToStr( Dvelop_gxbootstrap_panel_Showcollapseicon));
         Dvelop_gxbootstrap_panel_Title = "Requisitos associados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV6Qtde), 4, 0))+") - ["+StringUtil.Str( AV11Pontos, 14, 5)+"]";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Dvelop_gxbootstrap_panel_Internalname, "Title", Dvelop_gxbootstrap_panel_Title);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 7;
         }
         sendrow_72( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_7_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(7, GridRow);
         }
      }

      protected void E15QA2( )
      {
         /* Updreqtec_Click Routine */
         AV8window.Height = (int)(AV9WWPContext.gxTpr_Screen_height*0.63m);
         AV8window.Width = (int)(AV9WWPContext.gxTpr_Screen_width*0.60m);
         AV8window.Autoresize = 0;
         AV8window.Url = formatLink("wp_novorequisito.aspx") + "?" + UrlEncode("" +A1919Requisito_Codigo) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV12OSCod);
         AV8window.SetReturnParms(new Object[] {});
         context.NewWindow(AV8window);
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E11QA2( )
      {
         /* Confirmpanel1_Close Routine */
         if ( StringUtil.StrCmp(Confirmpanel1_Result, "Yes") == 0 )
         {
            new prc_dltrequisito(context ).execute( ref  AV12OSCod, ref  A1919Requisito_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OSCod), 6, 0)));
            gxgrGrid_refresh( A1999Requisito_ReqCod, A2004ContagemResultadoRequisito_ReqCod, A2003ContagemResultadoRequisito_OSCod, AV6Qtde, AV11Pontos, A1932Requisito_Pontuacao, sPrefix) ;
         }
      }

      protected void S112( )
      {
         /* 'OSCOD' Routine */
         /* Using cursor H00QA4 */
         pr_default.execute(2, new Object[] {n1999Requisito_ReqCod, A1999Requisito_ReqCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            AV13ReqCod = A1999Requisito_ReqCod;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ReqCod), 6, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(2);
         }
         pr_default.close(2);
         /* Using cursor H00QA5 */
         pr_default.execute(3, new Object[] {AV13ReqCod});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A2004ContagemResultadoRequisito_ReqCod = H00QA5_A2004ContagemResultadoRequisito_ReqCod[0];
            A2003ContagemResultadoRequisito_OSCod = H00QA5_A2003ContagemResultadoRequisito_OSCod[0];
            AV12OSCod = A2003ContagemResultadoRequisito_OSCod;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OSCod), 6, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void wb_table1_2_QA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVELOP_GXBOOTSTRAP_PANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"DVELOP_GXBOOTSTRAP_PANELContainer"+"Body"+"\" style=\"display:none;\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"7\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(124), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtRequisito_Codigo_Visible==0) ? "display:none;" : "")+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "C�digo do Requisito") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+edtRequisito_Ordem_Horizontalalignment+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Ordem") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(100), 4, 0))+"%"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "T�tulo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Pontua��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1919Requisito_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRequisito_Codigo_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV5UpdReqTec));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV10DltReqTec));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1931Requisito_Ordem), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Horizontalalignment", StringUtil.RTrim( edtRequisito_Ordem_Horizontalalignment));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1927Requisito_Titulo);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2049Requisito_TipoReqCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1932Requisito_Pontuacao, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 7 )
         {
            wbEnd = 0;
            nRC_GXsfl_7 = (short)(nGXsfl_7_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_QA2e( true) ;
         }
         else
         {
            wb_table1_2_QA2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1999Requisito_ReqCod = Convert.ToInt32(getParm(obj,0));
         n1999Requisito_ReqCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1999Requisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1999Requisito_ReqCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAQA2( ) ;
         WSQA2( ) ;
         WEQA2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1999Requisito_ReqCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAQA2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_requisitostecnicos");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAQA2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1999Requisito_ReqCod = Convert.ToInt32(getParm(obj,2));
            n1999Requisito_ReqCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1999Requisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1999Requisito_ReqCod), 6, 0)));
         }
         wcpOA1999Requisito_ReqCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1999Requisito_ReqCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1999Requisito_ReqCod != wcpOA1999Requisito_ReqCod ) ) )
         {
            setjustcreated();
         }
         wcpOA1999Requisito_ReqCod = A1999Requisito_ReqCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1999Requisito_ReqCod = cgiGet( sPrefix+"A1999Requisito_ReqCod_CTRL");
         if ( StringUtil.Len( sCtrlA1999Requisito_ReqCod) > 0 )
         {
            A1999Requisito_ReqCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA1999Requisito_ReqCod), ",", "."));
            n1999Requisito_ReqCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1999Requisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1999Requisito_ReqCod), 6, 0)));
         }
         else
         {
            A1999Requisito_ReqCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1999Requisito_ReqCod_PARM"), ",", "."));
            n1999Requisito_ReqCod = false;
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAQA2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSQA2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSQA2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1999Requisito_ReqCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1999Requisito_ReqCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1999Requisito_ReqCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1999Requisito_ReqCod_CTRL", StringUtil.RTrim( sCtrlA1999Requisito_ReqCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEQA2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311762573");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("wc_requisitostecnicos.js", "?2020311762573");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_72( )
      {
         edtRequisito_Codigo_Internalname = sPrefix+"REQUISITO_CODIGO_"+sGXsfl_7_idx;
         edtavUpdreqtec_Internalname = sPrefix+"vUPDREQTEC_"+sGXsfl_7_idx;
         edtavDltreqtec_Internalname = sPrefix+"vDLTREQTEC_"+sGXsfl_7_idx;
         edtRequisito_Ordem_Internalname = sPrefix+"REQUISITO_ORDEM_"+sGXsfl_7_idx;
         edtRequisito_Titulo_Internalname = sPrefix+"REQUISITO_TITULO_"+sGXsfl_7_idx;
         dynRequisito_TipoReqCod_Internalname = sPrefix+"REQUISITO_TIPOREQCOD_"+sGXsfl_7_idx;
         edtRequisito_Pontuacao_Internalname = sPrefix+"REQUISITO_PONTUACAO_"+sGXsfl_7_idx;
      }

      protected void SubsflControlProps_fel_72( )
      {
         edtRequisito_Codigo_Internalname = sPrefix+"REQUISITO_CODIGO_"+sGXsfl_7_fel_idx;
         edtavUpdreqtec_Internalname = sPrefix+"vUPDREQTEC_"+sGXsfl_7_fel_idx;
         edtavDltreqtec_Internalname = sPrefix+"vDLTREQTEC_"+sGXsfl_7_fel_idx;
         edtRequisito_Ordem_Internalname = sPrefix+"REQUISITO_ORDEM_"+sGXsfl_7_fel_idx;
         edtRequisito_Titulo_Internalname = sPrefix+"REQUISITO_TITULO_"+sGXsfl_7_fel_idx;
         dynRequisito_TipoReqCod_Internalname = sPrefix+"REQUISITO_TIPOREQCOD_"+sGXsfl_7_fel_idx;
         edtRequisito_Pontuacao_Internalname = sPrefix+"REQUISITO_PONTUACAO_"+sGXsfl_7_fel_idx;
      }

      protected void sendrow_72( )
      {
         SubsflControlProps_72( ) ;
         WBQA0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0x0);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_7_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_7_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((edtRequisito_Codigo_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1919Requisito_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1919Requisito_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_Codigo_Jsonclick,(short)0,(String)"Attribute",(String)"color:#000000;",(String)ROClassString,(String)"",(int)edtRequisito_Codigo_Visible,(short)0,(short)0,(String)"text",(String)"",(short)124,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)7,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavUpdreqtec_Enabled!=0)&&(edtavUpdreqtec_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 9,'"+sPrefix+"',false,'',7)\"" : " ");
         ClassString = "Image";
         StyleString = "color:#000000;";
         AV5UpdReqTec_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV5UpdReqTec))&&String.IsNullOrEmpty(StringUtil.RTrim( AV16Updreqtec_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV5UpdReqTec)));
         GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdreqtec_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV5UpdReqTec)) ? AV16Updreqtec_GXI : context.PathToRelativeUrl( AV5UpdReqTec)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)"",(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavUpdreqtec_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVUPDREQTEC.CLICK."+sGXsfl_7_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV5UpdReqTec_IsBlob,(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavDltreqtec_Enabled!=0)&&(edtavDltreqtec_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 10,'"+sPrefix+"',false,'',7)\"" : " ");
         ClassString = "Image";
         StyleString = "color:#000000;";
         AV10DltReqTec_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV10DltReqTec))&&String.IsNullOrEmpty(StringUtil.RTrim( AV17Dltreqtec_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV10DltReqTec)));
         GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDltreqtec_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV10DltReqTec)) ? AV17Dltreqtec_GXI : context.PathToRelativeUrl( AV10DltReqTec)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)"",(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavDltreqtec_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e16qa2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV10DltReqTec_IsBlob,(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+edtRequisito_Ordem_Horizontalalignment+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Ordem_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1931Requisito_Ordem), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_Ordem_Jsonclick,(short)0,(String)"Attribute",(String)"color:#000000;",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)7,(short)1,(short)-1,(short)0,(bool)true,(String)"Ordem",(String)edtRequisito_Ordem_Horizontalalignment,(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Titulo_Internalname,(String)A1927Requisito_Titulo,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_Titulo_Jsonclick,(short)0,(String)"Attribute",(String)"color:#000000;",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)100,(String)"%",(short)17,(String)"px",(short)250,(short)0,(short)0,(short)7,(short)1,(short)-1,(short)-1,(bool)true,(String)"ReqNome",(String)"left",(bool)true});
         GXAREQUISITO_TIPOREQCOD_htmlQA2( ) ;
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         if ( ( nGXsfl_7_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "REQUISITO_TIPOREQCOD_" + sGXsfl_7_idx;
            dynRequisito_TipoReqCod.Name = GXCCtl;
            dynRequisito_TipoReqCod.WebTags = "";
         }
         /* ComboBox */
         GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynRequisito_TipoReqCod,(String)dynRequisito_TipoReqCod_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0)),(short)1,(String)dynRequisito_TipoReqCod_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"color:#000000;",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
         dynRequisito_TipoReqCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2049Requisito_TipoReqCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynRequisito_TipoReqCod_Internalname, "Values", (String)(dynRequisito_TipoReqCod.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Pontuacao_Internalname,StringUtil.LTrim( StringUtil.NToC( A1932Requisito_Pontuacao, 14, 5, ",", "")),context.localUtil.Format( A1932Requisito_Pontuacao, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_Pontuacao_Jsonclick,(short)0,(String)"Attribute",(String)"color:#000000;",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)7,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_ORDEM"+"_"+sGXsfl_7_idx, GetSecureSignedToken( sPrefix+sGXsfl_7_idx, context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_TITULO"+"_"+sGXsfl_7_idx, GetSecureSignedToken( sPrefix+sGXsfl_7_idx, StringUtil.RTrim( context.localUtil.Format( A1927Requisito_Titulo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_TIPOREQCOD"+"_"+sGXsfl_7_idx, GetSecureSignedToken( sPrefix+sGXsfl_7_idx, context.localUtil.Format( (decimal)(A2049Requisito_TipoReqCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REQUISITO_PONTUACAO"+"_"+sGXsfl_7_idx, GetSecureSignedToken( sPrefix+sGXsfl_7_idx, context.localUtil.Format( A1932Requisito_Pontuacao, "ZZ,ZZZ,ZZ9.999")));
         GridContainer.AddRow(GridRow);
         nGXsfl_7_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_7_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_7_idx+1));
         sGXsfl_7_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_7_idx), 4, 0)), 4, "0");
         SubsflControlProps_72( ) ;
         /* End function sendrow_72 */
      }

      protected void init_default_properties( )
      {
         edtRequisito_Codigo_Internalname = sPrefix+"REQUISITO_CODIGO";
         edtavUpdreqtec_Internalname = sPrefix+"vUPDREQTEC";
         edtavDltreqtec_Internalname = sPrefix+"vDLTREQTEC";
         edtRequisito_Ordem_Internalname = sPrefix+"REQUISITO_ORDEM";
         edtRequisito_Titulo_Internalname = sPrefix+"REQUISITO_TITULO";
         dynRequisito_TipoReqCod_Internalname = sPrefix+"REQUISITO_TIPOREQCOD";
         edtRequisito_Pontuacao_Internalname = sPrefix+"REQUISITO_PONTUACAO";
         Dvelop_gxbootstrap_panel_Internalname = sPrefix+"DVELOP_GXBOOTSTRAP_PANEL";
         tblTable3_Internalname = sPrefix+"TABLE3";
         Confirmpanel1_Internalname = sPrefix+"CONFIRMPANEL1";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtRequisito_Pontuacao_Jsonclick = "";
         dynRequisito_TipoReqCod_Jsonclick = "";
         edtRequisito_Titulo_Jsonclick = "";
         edtRequisito_Ordem_Jsonclick = "";
         edtavDltreqtec_Jsonclick = "";
         edtavDltreqtec_Visible = -1;
         edtavDltreqtec_Enabled = 1;
         edtavUpdreqtec_Jsonclick = "";
         edtavUpdreqtec_Visible = -1;
         edtavUpdreqtec_Enabled = 1;
         edtRequisito_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtRequisito_Ordem_Horizontalalignment = "right";
         subGrid_Class = "WorkWithBorder WorkWith";
         subGrid_Titleforecolor = (int)(0x000000);
         subGrid_Backcolorstyle = 0;
         Confirmpanel1_Yesbuttonposition = "left";
         Confirmpanel1_Cancelbuttoncaption = "Cancel";
         Confirmpanel1_Nobuttoncaption = "N�o";
         Confirmpanel1_Yesbuttoncaption = "Sim";
         Confirmpanel1_Confirmationtext = "Confirma eliminar este requisito t�cnico?";
         Confirmpanel1_Title = "Confirma��o";
         Dvelop_gxbootstrap_panel_Showcollapseicon = Convert.ToBoolean( -1);
         Dvelop_gxbootstrap_panel_Autowidth = Convert.ToBoolean( -1);
         Dvelop_gxbootstrap_panel_Collapsed = Convert.ToBoolean( 0);
         Dvelop_gxbootstrap_panel_Collapsible = Convert.ToBoolean( 0);
         Dvelop_gxbootstrap_panel_Title = "Requisitos relacionados (0)";
         Dvelop_gxbootstrap_panel_Height = "100%";
         Dvelop_gxbootstrap_panel_Width = "100%";
         edtRequisito_Codigo_Visible = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'edtRequisito_Codigo_Visible',ctrl:'REQUISITO_CODIGO',prop:'Visible'},{av:'AV6Qtde',fld:'vQTDE',pic:'ZZZ9',nv:0},{av:'AV11Pontos',fld:'vPONTOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'sPrefix',nv:''},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A2003ContagemResultadoRequisito_OSCod',fld:'CONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV9WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV6Qtde',fld:'vQTDE',pic:'ZZZ9',nv:0},{av:'AV11Pontos',fld:'vPONTOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV13ReqCod',fld:'vREQCOD',pic:'ZZZZZ9',nv:0},{av:'AV12OSCod',fld:'vOSCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E14QA2',iparms:[{av:'AV6Qtde',fld:'vQTDE',pic:'ZZZ9',nv:0},{av:'AV11Pontos',fld:'vPONTOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0}],oparms:[{av:'AV5UpdReqTec',fld:'vUPDREQTEC',pic:'',nv:''},{av:'AV10DltReqTec',fld:'vDLTREQTEC',pic:'',nv:''},{av:'AV6Qtde',fld:'vQTDE',pic:'ZZZ9',nv:0},{av:'AV11Pontos',fld:'vPONTOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'edtRequisito_Ordem_Horizontalalignment',ctrl:'REQUISITO_ORDEM',prop:'Horizontalalignment'},{av:'Dvelop_gxbootstrap_panel_Showcollapseicon',ctrl:'DVELOP_GXBOOTSTRAP_PANEL',prop:'ShowCollapseIcon'},{av:'Dvelop_gxbootstrap_panel_Title',ctrl:'DVELOP_GXBOOTSTRAP_PANEL',prop:'Title'}]}");
         setEventMetadata("VUPDREQTEC.CLICK","{handler:'E15QA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'edtRequisito_Codigo_Visible',ctrl:'REQUISITO_CODIGO',prop:'Visible'},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A2003ContagemResultadoRequisito_OSCod',fld:'CONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Qtde',fld:'vQTDE',pic:'ZZZ9',nv:0},{av:'AV11Pontos',fld:'vPONTOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'sPrefix',nv:''},{av:'AV9WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12OSCod',fld:'vOSCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("VDLTREQTEC.CLICK","{handler:'E16QA2',iparms:[{av:'A1931Requisito_Ordem',fld:'REQUISITO_ORDEM',pic:'ZZ9',hsh:true,nv:0}],oparms:[{av:'Confirmpanel1_Confirmationtext',ctrl:'CONFIRMPANEL1',prop:'ConfirmationText'}]}");
         setEventMetadata("CONFIRMPANEL1.CLOSE","{handler:'E11QA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'edtRequisito_Codigo_Visible',ctrl:'REQUISITO_CODIGO',prop:'Visible'},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A2003ContagemResultadoRequisito_OSCod',fld:'CONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Qtde',fld:'vQTDE',pic:'ZZZ9',nv:0},{av:'AV11Pontos',fld:'vPONTOS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'sPrefix',nv:''},{av:'Confirmpanel1_Result',ctrl:'CONFIRMPANEL1',prop:'Result'},{av:'AV12OSCod',fld:'vOSCOD',pic:'ZZZZZ9',nv:0},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12OSCod',fld:'vOSCOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Confirmpanel1_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV5UpdReqTec = "";
         AV16Updreqtec_GXI = "";
         AV10DltReqTec = "";
         AV17Dltreqtec_GXI = "";
         A1927Requisito_Titulo = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00QA2_A2049Requisito_TipoReqCod = new int[1] ;
         H00QA2_n2049Requisito_TipoReqCod = new bool[] {false} ;
         H00QA2_A2042TipoRequisito_Identificador = new String[] {""} ;
         H00QA2_n2042TipoRequisito_Identificador = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         H00QA3_A1999Requisito_ReqCod = new int[1] ;
         H00QA3_n1999Requisito_ReqCod = new bool[] {false} ;
         H00QA3_A1932Requisito_Pontuacao = new decimal[1] ;
         H00QA3_n1932Requisito_Pontuacao = new bool[] {false} ;
         H00QA3_A2049Requisito_TipoReqCod = new int[1] ;
         H00QA3_n2049Requisito_TipoReqCod = new bool[] {false} ;
         H00QA3_A1927Requisito_Titulo = new String[] {""} ;
         H00QA3_n1927Requisito_Titulo = new bool[] {false} ;
         H00QA3_A1931Requisito_Ordem = new short[1] ;
         H00QA3_n1931Requisito_Ordem = new bool[] {false} ;
         H00QA3_A1919Requisito_Codigo = new int[1] ;
         GridRow = new GXWebRow();
         AV8window = new GXWindow();
         H00QA4_A1919Requisito_Codigo = new int[1] ;
         H00QA4_A1999Requisito_ReqCod = new int[1] ;
         H00QA4_n1999Requisito_ReqCod = new bool[] {false} ;
         H00QA5_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         H00QA5_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         H00QA5_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1999Requisito_ReqCod = "";
         ROClassString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_requisitostecnicos__default(),
            new Object[][] {
                new Object[] {
               H00QA2_A2049Requisito_TipoReqCod, H00QA2_A2042TipoRequisito_Identificador, H00QA2_n2042TipoRequisito_Identificador
               }
               , new Object[] {
               H00QA3_A1999Requisito_ReqCod, H00QA3_n1999Requisito_ReqCod, H00QA3_A1932Requisito_Pontuacao, H00QA3_n1932Requisito_Pontuacao, H00QA3_A2049Requisito_TipoReqCod, H00QA3_n2049Requisito_TipoReqCod, H00QA3_A1927Requisito_Titulo, H00QA3_n1927Requisito_Titulo, H00QA3_A1931Requisito_Ordem, H00QA3_n1931Requisito_Ordem,
               H00QA3_A1919Requisito_Codigo
               }
               , new Object[] {
               H00QA4_A1919Requisito_Codigo, H00QA4_A1999Requisito_ReqCod, H00QA4_n1999Requisito_ReqCod
               }
               , new Object[] {
               H00QA5_A2005ContagemResultadoRequisito_Codigo, H00QA5_A2004ContagemResultadoRequisito_ReqCod, H00QA5_A2003ContagemResultadoRequisito_OSCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_7 ;
      private short nGXsfl_7_idx=1 ;
      private short AV6Qtde ;
      private short initialized ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A1931Requisito_Ordem ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_7_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short GRID_nEOF ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int A1999Requisito_ReqCod ;
      private int wcpOA1999Requisito_ReqCod ;
      private int edtRequisito_Codigo_Visible ;
      private int A2004ContagemResultadoRequisito_ReqCod ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int AV12OSCod ;
      private int A1919Requisito_Codigo ;
      private int A2049Requisito_TipoReqCod ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int subGrid_Titleforecolor ;
      private int AV13ReqCod ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavUpdreqtec_Enabled ;
      private int edtavUpdreqtec_Visible ;
      private int edtavDltreqtec_Enabled ;
      private int edtavDltreqtec_Visible ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private decimal AV11Pontos ;
      private decimal A1932Requisito_Pontuacao ;
      private String Confirmpanel1_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_7_idx="0001" ;
      private String edtRequisito_Codigo_Internalname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvelop_gxbootstrap_panel_Width ;
      private String Dvelop_gxbootstrap_panel_Height ;
      private String Dvelop_gxbootstrap_panel_Title ;
      private String Confirmpanel1_Title ;
      private String Confirmpanel1_Confirmationtext ;
      private String Confirmpanel1_Yesbuttoncaption ;
      private String Confirmpanel1_Nobuttoncaption ;
      private String Confirmpanel1_Cancelbuttoncaption ;
      private String Confirmpanel1_Yesbuttonposition ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdreqtec_Internalname ;
      private String edtavDltreqtec_Internalname ;
      private String edtRequisito_Ordem_Internalname ;
      private String edtRequisito_Titulo_Internalname ;
      private String dynRequisito_TipoReqCod_Internalname ;
      private String edtRequisito_Pontuacao_Internalname ;
      private String GXCCtl ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtRequisito_Ordem_Horizontalalignment ;
      private String Dvelop_gxbootstrap_panel_Internalname ;
      private String sStyleString ;
      private String tblTable3_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlA1999Requisito_ReqCod ;
      private String sGXsfl_7_fel_idx="0001" ;
      private String ROClassString ;
      private String edtRequisito_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String edtavUpdreqtec_Jsonclick ;
      private String edtavDltreqtec_Jsonclick ;
      private String edtRequisito_Ordem_Jsonclick ;
      private String edtRequisito_Titulo_Jsonclick ;
      private String dynRequisito_TipoReqCod_Jsonclick ;
      private String edtRequisito_Pontuacao_Jsonclick ;
      private String Confirmpanel1_Internalname ;
      private bool entryPointCalled ;
      private bool n1999Requisito_ReqCod ;
      private bool n1932Requisito_Pontuacao ;
      private bool toggleJsOutput ;
      private bool Dvelop_gxbootstrap_panel_Collapsible ;
      private bool Dvelop_gxbootstrap_panel_Collapsed ;
      private bool Dvelop_gxbootstrap_panel_Autowidth ;
      private bool Dvelop_gxbootstrap_panel_Showcollapseicon ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1931Requisito_Ordem ;
      private bool n1927Requisito_Titulo ;
      private bool n2049Requisito_TipoReqCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV5UpdReqTec_IsBlob ;
      private bool AV10DltReqTec_IsBlob ;
      private String AV16Updreqtec_GXI ;
      private String AV17Dltreqtec_GXI ;
      private String A1927Requisito_Titulo ;
      private String AV5UpdReqTec ;
      private String AV10DltReqTec ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Requisito_ReqCod ;
      private GXCombobox dynRequisito_TipoReqCod ;
      private IDataStoreProvider pr_default ;
      private int[] H00QA2_A2049Requisito_TipoReqCod ;
      private bool[] H00QA2_n2049Requisito_TipoReqCod ;
      private String[] H00QA2_A2042TipoRequisito_Identificador ;
      private bool[] H00QA2_n2042TipoRequisito_Identificador ;
      private int[] H00QA3_A1999Requisito_ReqCod ;
      private bool[] H00QA3_n1999Requisito_ReqCod ;
      private decimal[] H00QA3_A1932Requisito_Pontuacao ;
      private bool[] H00QA3_n1932Requisito_Pontuacao ;
      private int[] H00QA3_A2049Requisito_TipoReqCod ;
      private bool[] H00QA3_n2049Requisito_TipoReqCod ;
      private String[] H00QA3_A1927Requisito_Titulo ;
      private bool[] H00QA3_n1927Requisito_Titulo ;
      private short[] H00QA3_A1931Requisito_Ordem ;
      private bool[] H00QA3_n1931Requisito_Ordem ;
      private int[] H00QA3_A1919Requisito_Codigo ;
      private int[] H00QA4_A1919Requisito_Codigo ;
      private int[] H00QA4_A1999Requisito_ReqCod ;
      private bool[] H00QA4_n1999Requisito_ReqCod ;
      private int[] H00QA5_A2005ContagemResultadoRequisito_Codigo ;
      private int[] H00QA5_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] H00QA5_A2003ContagemResultadoRequisito_OSCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWindow AV8window ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class wc_requisitostecnicos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00QA2 ;
          prmH00QA2 = new Object[] {
          } ;
          Object[] prmH00QA3 ;
          prmH00QA3 = new Object[] {
          new Object[] {"@Requisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QA4 ;
          prmH00QA4 = new Object[] {
          new Object[] {"@Requisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QA5 ;
          prmH00QA5 = new Object[] {
          new Object[] {"@AV13ReqCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00QA2", "SELECT [TipoRequisito_Codigo] AS Requisito_TipoReqCod, [TipoRequisito_Identificador] FROM [TipoRequisito] WITH (NOLOCK) ORDER BY [TipoRequisito_Identificador] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QA2,0,0,true,false )
             ,new CursorDef("H00QA3", "SELECT [Requisito_ReqCod], [Requisito_Pontuacao], [Requisito_TipoReqCod] AS Requisito_TipoReqCod, [Requisito_Titulo], [Requisito_Ordem], [Requisito_Codigo] FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_ReqCod] = @Requisito_ReqCod ORDER BY [Requisito_ReqCod], [Requisito_Ordem] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QA3,11,0,true,false )
             ,new CursorDef("H00QA4", "SELECT TOP 1 [Requisito_Codigo], [Requisito_ReqCod] FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_ReqCod] = @Requisito_ReqCod ORDER BY [Requisito_ReqCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QA4,1,0,false,true )
             ,new CursorDef("H00QA5", "SELECT TOP 1 [ContagemResultadoRequisito_Codigo], [ContagemResultadoRequisito_ReqCod], [ContagemResultadoRequisito_OSCod] FROM [ContagemResultadoRequisito] WITH (NOLOCK) WHERE [ContagemResultadoRequisito_ReqCod] = @AV13ReqCod ORDER BY [ContagemResultadoRequisito_ReqCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QA5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
