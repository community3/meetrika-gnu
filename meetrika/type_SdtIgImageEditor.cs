/*
               File: type_SdtIgImageEditor
        Description: IgImageEditor
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:56.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtIgImageEditor : GxUserType, IGxExternalObject
   {
      public SdtIgImageEditor( )
      {
         initialize();
      }

      public SdtIgImageEditor( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void open( String gxTp_path )
      {
         if ( IgImageEditor_externalReference == null )
         {
            IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
         }
         IgImageEditor_externalReference.Open(gxTp_path);
         return  ;
      }

      public void close( )
      {
         if ( IgImageEditor_externalReference == null )
         {
            IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
         }
         IgImageEditor_externalReference.Close();
         return  ;
      }

      public void resize( int gxTp_height ,
                          int gxTp_width ,
                          String gxTp_outputFile )
      {
         if ( IgImageEditor_externalReference == null )
         {
            IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
         }
         IgImageEditor_externalReference.Resize(gxTp_height, gxTp_width, gxTp_outputFile);
         return  ;
      }

      public void crop( int gxTp_x ,
                        int gxTp_y ,
                        int gxTp_height ,
                        int gxTp_width ,
                        String gxTp_outputFile )
      {
         if ( IgImageEditor_externalReference == null )
         {
            IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
         }
         IgImageEditor_externalReference.Crop(gxTp_x, gxTp_y, gxTp_height, gxTp_width, gxTp_outputFile);
         return  ;
      }

      public void rotateflip( int gxTp_angle ,
                              bool gxTp_flipX ,
                              bool gxTp_flipY ,
                              String gxTp_outputFile )
      {
         if ( IgImageEditor_externalReference == null )
         {
            IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
         }
         IgImageEditor_externalReference.RotateFlip(gxTp_angle, gxTp_flipX, gxTp_flipY, gxTp_outputFile);
         return  ;
      }

      public void roundedcorner( int gxTp_radius ,
                                 String gxTp_backgroundHexColor ,
                                 String gxTp_outputFile )
      {
         if ( IgImageEditor_externalReference == null )
         {
            IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
         }
         IgImageEditor_externalReference.RoundedCorner(gxTp_radius, gxTp_backgroundHexColor, gxTp_outputFile);
         return  ;
      }

      public void format( String gxTp_format ,
                          long gxTp_quality ,
                          String gxTp_outputFile )
      {
         if ( IgImageEditor_externalReference == null )
         {
            IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
         }
         IgImageEditor_externalReference.Format(gxTp_format, gxTp_quality, gxTp_outputFile);
         return  ;
      }

      public void merge( int gxTp_height ,
                         int gxTp_width ,
                         String gxTp_backgroundHexColor ,
                         String gxTp_images ,
                         String gxTp_outputFile )
      {
         if ( IgImageEditor_externalReference == null )
         {
            IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
         }
         IgImageEditor_externalReference.Merge(gxTp_height, gxTp_width, gxTp_backgroundHexColor, gxTp_images, gxTp_outputFile);
         return  ;
      }

      public bool gxTpr_Isopen
      {
         get {
            if ( IgImageEditor_externalReference == null )
            {
               IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
            }
            return IgImageEditor_externalReference.isOpen ;
         }

      }

      public int gxTpr_Getheight
      {
         get {
            if ( IgImageEditor_externalReference == null )
            {
               IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
            }
            return IgImageEditor_externalReference.GetHeight ;
         }

      }

      public int gxTpr_Getwidth
      {
         get {
            if ( IgImageEditor_externalReference == null )
            {
               IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
            }
            return IgImageEditor_externalReference.GetWidth ;
         }

      }

      public String gxTpr_Getformat
      {
         get {
            if ( IgImageEditor_externalReference == null )
            {
               IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
            }
            return IgImageEditor_externalReference.GetFormat ;
         }

      }

      public String gxTpr_Getcolor
      {
         get {
            if ( IgImageEditor_externalReference == null )
            {
               IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
            }
            return IgImageEditor_externalReference.GetColor ;
         }

      }

      public long gxTpr_Getsize
      {
         get {
            if ( IgImageEditor_externalReference == null )
            {
               IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
            }
            return IgImageEditor_externalReference.GetSize ;
         }

      }

      public long gxTpr_Quality
      {
         get {
            if ( IgImageEditor_externalReference == null )
            {
               IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
            }
            return IgImageEditor_externalReference.Quality ;
         }

         set {
            if ( IgImageEditor_externalReference == null )
            {
               IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
            }
            IgImageEditor_externalReference.Quality = value;
         }

      }

      public String gxTpr_Lasterrordescription
      {
         get {
            if ( IgImageEditor_externalReference == null )
            {
               IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
            }
            return IgImageEditor_externalReference.LastErrorDescription ;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( IgImageEditor_externalReference == null )
            {
               IgImageEditor_externalReference = new IgImageEditor.IgImageEditor();
            }
            return IgImageEditor_externalReference ;
         }

         set {
            IgImageEditor_externalReference = (IgImageEditor.IgImageEditor)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected IgImageEditor.IgImageEditor IgImageEditor_externalReference=null ;
   }

}
