/*
               File: ContratoSistemas_BC
        Description: Sistemas atendidos pelo Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:40.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratosistemas_bc : GXHttpHandler, IGxSilentTrn
   {
      public contratosistemas_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratosistemas_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow49189( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey49189( ) ;
         standaloneModal( ) ;
         AddRow49189( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11492 */
            E11492 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1725ContratoSistemas_CntCod = A1725ContratoSistemas_CntCod;
               Z1726ContratoSistemas_SistemaCod = A1726ContratoSistemas_SistemaCod;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_490( )
      {
         BeforeValidate49189( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls49189( ) ;
            }
            else
            {
               CheckExtendedTable49189( ) ;
               if ( AnyError == 0 )
               {
                  ZM49189( 2) ;
                  ZM49189( 3) ;
               }
               CloseExtendedTableCursors49189( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12492( )
      {
         /* Start Routine */
      }

      protected void E11492( )
      {
         /* After Trn Routine */
      }

      protected void ZM49189( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -1 )
         {
            Z1725ContratoSistemas_CntCod = A1725ContratoSistemas_CntCod;
            Z1726ContratoSistemas_SistemaCod = A1726ContratoSistemas_SistemaCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load49189( )
      {
         /* Using cursor BC00496 */
         pr_default.execute(4, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound189 = 1;
            ZM49189( -1) ;
         }
         pr_default.close(4);
         OnLoadActions49189( ) ;
      }

      protected void OnLoadActions49189( )
      {
      }

      protected void CheckExtendedTable49189( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00494 */
         pr_default.execute(2, new Object[] {A1725ContratoSistemas_CntCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Sistemas_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_CNTCOD");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC00495 */
         pr_default.execute(3, new Object[] {A1726ContratoSistemas_SistemaCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_SISTEMACOD");
            AnyError = 1;
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors49189( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey49189( )
      {
         /* Using cursor BC00497 */
         pr_default.execute(5, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound189 = 1;
         }
         else
         {
            RcdFound189 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00493 */
         pr_default.execute(1, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM49189( 1) ;
            RcdFound189 = 1;
            A1725ContratoSistemas_CntCod = BC00493_A1725ContratoSistemas_CntCod[0];
            A1726ContratoSistemas_SistemaCod = BC00493_A1726ContratoSistemas_SistemaCod[0];
            Z1725ContratoSistemas_CntCod = A1725ContratoSistemas_CntCod;
            Z1726ContratoSistemas_SistemaCod = A1726ContratoSistemas_SistemaCod;
            sMode189 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load49189( ) ;
            if ( AnyError == 1 )
            {
               RcdFound189 = 0;
               InitializeNonKey49189( ) ;
            }
            Gx_mode = sMode189;
         }
         else
         {
            RcdFound189 = 0;
            InitializeNonKey49189( ) ;
            sMode189 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode189;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey49189( ) ;
         if ( RcdFound189 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_490( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency49189( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00492 */
            pr_default.execute(0, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoSistemas"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoSistemas"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert49189( )
      {
         BeforeValidate49189( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable49189( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM49189( 0) ;
            CheckOptimisticConcurrency49189( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm49189( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert49189( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00498 */
                     pr_default.execute(6, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoSistemas") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load49189( ) ;
            }
            EndLevel49189( ) ;
         }
         CloseExtendedTableCursors49189( ) ;
      }

      protected void Update49189( )
      {
         BeforeValidate49189( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable49189( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency49189( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm49189( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate49189( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ContratoSistemas] */
                     DeferredUpdate49189( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel49189( ) ;
         }
         CloseExtendedTableCursors49189( ) ;
      }

      protected void DeferredUpdate49189( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate49189( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency49189( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls49189( ) ;
            AfterConfirm49189( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete49189( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC00499 */
                  pr_default.execute(7, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoSistemas") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode189 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel49189( ) ;
         Gx_mode = sMode189;
      }

      protected void OnDeleteControls49189( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel49189( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete49189( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart49189( )
      {
         /* Scan By routine */
         /* Using cursor BC004910 */
         pr_default.execute(8, new Object[] {A1725ContratoSistemas_CntCod, A1726ContratoSistemas_SistemaCod});
         RcdFound189 = 0;
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound189 = 1;
            A1725ContratoSistemas_CntCod = BC004910_A1725ContratoSistemas_CntCod[0];
            A1726ContratoSistemas_SistemaCod = BC004910_A1726ContratoSistemas_SistemaCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext49189( )
      {
         /* Scan next routine */
         pr_default.readNext(8);
         RcdFound189 = 0;
         ScanKeyLoad49189( ) ;
      }

      protected void ScanKeyLoad49189( )
      {
         sMode189 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound189 = 1;
            A1725ContratoSistemas_CntCod = BC004910_A1725ContratoSistemas_CntCod[0];
            A1726ContratoSistemas_SistemaCod = BC004910_A1726ContratoSistemas_SistemaCod[0];
         }
         Gx_mode = sMode189;
      }

      protected void ScanKeyEnd49189( )
      {
         pr_default.close(8);
      }

      protected void AfterConfirm49189( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert49189( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate49189( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete49189( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete49189( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate49189( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes49189( )
      {
      }

      protected void AddRow49189( )
      {
         VarsToRow189( bcContratoSistemas) ;
      }

      protected void ReadRow49189( )
      {
         RowToVars189( bcContratoSistemas, 1) ;
      }

      protected void InitializeNonKey49189( )
      {
      }

      protected void InitAll49189( )
      {
         A1725ContratoSistemas_CntCod = 0;
         A1726ContratoSistemas_SistemaCod = 0;
         InitializeNonKey49189( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow189( SdtContratoSistemas obj189 )
      {
         obj189.gxTpr_Mode = Gx_mode;
         obj189.gxTpr_Contratosistemas_cntcod = A1725ContratoSistemas_CntCod;
         obj189.gxTpr_Contratosistemas_sistemacod = A1726ContratoSistemas_SistemaCod;
         obj189.gxTpr_Contratosistemas_cntcod_Z = Z1725ContratoSistemas_CntCod;
         obj189.gxTpr_Contratosistemas_sistemacod_Z = Z1726ContratoSistemas_SistemaCod;
         obj189.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow189( SdtContratoSistemas obj189 )
      {
         obj189.gxTpr_Contratosistemas_cntcod = A1725ContratoSistemas_CntCod;
         obj189.gxTpr_Contratosistemas_sistemacod = A1726ContratoSistemas_SistemaCod;
         return  ;
      }

      public void RowToVars189( SdtContratoSistemas obj189 ,
                                int forceLoad )
      {
         Gx_mode = obj189.gxTpr_Mode;
         A1725ContratoSistemas_CntCod = obj189.gxTpr_Contratosistemas_cntcod;
         A1726ContratoSistemas_SistemaCod = obj189.gxTpr_Contratosistemas_sistemacod;
         Z1725ContratoSistemas_CntCod = obj189.gxTpr_Contratosistemas_cntcod_Z;
         Z1726ContratoSistemas_SistemaCod = obj189.gxTpr_Contratosistemas_sistemacod_Z;
         Gx_mode = obj189.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1725ContratoSistemas_CntCod = (int)getParm(obj,0);
         A1726ContratoSistemas_SistemaCod = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey49189( ) ;
         ScanKeyStart49189( ) ;
         if ( RcdFound189 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004911 */
            pr_default.execute(9, new Object[] {A1725ContratoSistemas_CntCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Sistemas_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_CNTCOD");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor BC004912 */
            pr_default.execute(10, new Object[] {A1726ContratoSistemas_SistemaCod});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_SISTEMACOD");
               AnyError = 1;
            }
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z1725ContratoSistemas_CntCod = A1725ContratoSistemas_CntCod;
            Z1726ContratoSistemas_SistemaCod = A1726ContratoSistemas_SistemaCod;
         }
         ZM49189( -1) ;
         OnLoadActions49189( ) ;
         AddRow49189( ) ;
         ScanKeyEnd49189( ) ;
         if ( RcdFound189 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars189( bcContratoSistemas, 0) ;
         ScanKeyStart49189( ) ;
         if ( RcdFound189 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004911 */
            pr_default.execute(9, new Object[] {A1725ContratoSistemas_CntCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Sistemas_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_CNTCOD");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor BC004912 */
            pr_default.execute(10, new Object[] {A1726ContratoSistemas_SistemaCod});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Sistemas_Sistema'.", "ForeignKeyNotFound", 1, "CONTRATOSISTEMAS_SISTEMACOD");
               AnyError = 1;
            }
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z1725ContratoSistemas_CntCod = A1725ContratoSistemas_CntCod;
            Z1726ContratoSistemas_SistemaCod = A1726ContratoSistemas_SistemaCod;
         }
         ZM49189( -1) ;
         OnLoadActions49189( ) ;
         AddRow49189( ) ;
         ScanKeyEnd49189( ) ;
         if ( RcdFound189 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars189( bcContratoSistemas, 0) ;
         nKeyPressed = 1;
         GetKey49189( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert49189( ) ;
         }
         else
         {
            if ( RcdFound189 == 1 )
            {
               if ( ( A1725ContratoSistemas_CntCod != Z1725ContratoSistemas_CntCod ) || ( A1726ContratoSistemas_SistemaCod != Z1726ContratoSistemas_SistemaCod ) )
               {
                  A1725ContratoSistemas_CntCod = Z1725ContratoSistemas_CntCod;
                  A1726ContratoSistemas_SistemaCod = Z1726ContratoSistemas_SistemaCod;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update49189( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A1725ContratoSistemas_CntCod != Z1725ContratoSistemas_CntCod ) || ( A1726ContratoSistemas_SistemaCod != Z1726ContratoSistemas_SistemaCod ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert49189( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert49189( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow189( bcContratoSistemas) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars189( bcContratoSistemas, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey49189( ) ;
         if ( RcdFound189 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A1725ContratoSistemas_CntCod != Z1725ContratoSistemas_CntCod ) || ( A1726ContratoSistemas_SistemaCod != Z1726ContratoSistemas_SistemaCod ) )
            {
               A1725ContratoSistemas_CntCod = Z1725ContratoSistemas_CntCod;
               A1726ContratoSistemas_SistemaCod = Z1726ContratoSistemas_SistemaCod;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A1725ContratoSistemas_CntCod != Z1725ContratoSistemas_CntCod ) || ( A1726ContratoSistemas_SistemaCod != Z1726ContratoSistemas_SistemaCod ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
         context.RollbackDataStores( "ContratoSistemas_BC");
         VarsToRow189( bcContratoSistemas) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContratoSistemas.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContratoSistemas.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContratoSistemas )
         {
            bcContratoSistemas = (SdtContratoSistemas)(sdt);
            if ( StringUtil.StrCmp(bcContratoSistemas.gxTpr_Mode, "") == 0 )
            {
               bcContratoSistemas.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow189( bcContratoSistemas) ;
            }
            else
            {
               RowToVars189( bcContratoSistemas, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContratoSistemas.gxTpr_Mode, "") == 0 )
            {
               bcContratoSistemas.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars189( bcContratoSistemas, 1) ;
         return  ;
      }

      public SdtContratoSistemas ContratoSistemas_BC
      {
         get {
            return bcContratoSistemas ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         BC00496_A1725ContratoSistemas_CntCod = new int[1] ;
         BC00496_A1726ContratoSistemas_SistemaCod = new int[1] ;
         BC00494_A1725ContratoSistemas_CntCod = new int[1] ;
         BC00495_A1726ContratoSistemas_SistemaCod = new int[1] ;
         BC00497_A1725ContratoSistemas_CntCod = new int[1] ;
         BC00497_A1726ContratoSistemas_SistemaCod = new int[1] ;
         BC00493_A1725ContratoSistemas_CntCod = new int[1] ;
         BC00493_A1726ContratoSistemas_SistemaCod = new int[1] ;
         sMode189 = "";
         BC00492_A1725ContratoSistemas_CntCod = new int[1] ;
         BC00492_A1726ContratoSistemas_SistemaCod = new int[1] ;
         BC004910_A1725ContratoSistemas_CntCod = new int[1] ;
         BC004910_A1726ContratoSistemas_SistemaCod = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC004911_A1725ContratoSistemas_CntCod = new int[1] ;
         BC004912_A1726ContratoSistemas_SistemaCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratosistemas_bc__default(),
            new Object[][] {
                new Object[] {
               BC00492_A1725ContratoSistemas_CntCod, BC00492_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               BC00493_A1725ContratoSistemas_CntCod, BC00493_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               BC00494_A1725ContratoSistemas_CntCod
               }
               , new Object[] {
               BC00495_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               BC00496_A1725ContratoSistemas_CntCod, BC00496_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               BC00497_A1725ContratoSistemas_CntCod, BC00497_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004910_A1725ContratoSistemas_CntCod, BC004910_A1726ContratoSistemas_SistemaCod
               }
               , new Object[] {
               BC004911_A1725ContratoSistemas_CntCod
               }
               , new Object[] {
               BC004912_A1726ContratoSistemas_SistemaCod
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12492 */
         E12492 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound189 ;
      private int trnEnded ;
      private int Z1725ContratoSistemas_CntCod ;
      private int A1725ContratoSistemas_CntCod ;
      private int Z1726ContratoSistemas_SistemaCod ;
      private int A1726ContratoSistemas_SistemaCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode189 ;
      private SdtContratoSistemas bcContratoSistemas ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC00496_A1725ContratoSistemas_CntCod ;
      private int[] BC00496_A1726ContratoSistemas_SistemaCod ;
      private int[] BC00494_A1725ContratoSistemas_CntCod ;
      private int[] BC00495_A1726ContratoSistemas_SistemaCod ;
      private int[] BC00497_A1725ContratoSistemas_CntCod ;
      private int[] BC00497_A1726ContratoSistemas_SistemaCod ;
      private int[] BC00493_A1725ContratoSistemas_CntCod ;
      private int[] BC00493_A1726ContratoSistemas_SistemaCod ;
      private int[] BC00492_A1725ContratoSistemas_CntCod ;
      private int[] BC00492_A1726ContratoSistemas_SistemaCod ;
      private int[] BC004910_A1725ContratoSistemas_CntCod ;
      private int[] BC004910_A1726ContratoSistemas_SistemaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC004911_A1725ContratoSistemas_CntCod ;
      private int[] BC004912_A1726ContratoSistemas_SistemaCod ;
   }

   public class contratosistemas_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00496 ;
          prmBC00496 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00494 ;
          prmBC00494 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00495 ;
          prmBC00495 = new Object[] {
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00497 ;
          prmBC00497 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00493 ;
          prmBC00493 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00492 ;
          prmBC00492 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00498 ;
          prmBC00498 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00499 ;
          prmBC00499 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004910 ;
          prmBC004910 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004911 ;
          prmBC004911 = new Object[] {
          new Object[] {"@ContratoSistemas_CntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004912 ;
          prmBC004912 = new Object[] {
          new Object[] {"@ContratoSistemas_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00492", "SELECT [ContratoSistemas_CntCod] AS ContratoSistemas_CntCod, [ContratoSistemas_SistemaCod] AS ContratoSistemas_SistemaCod FROM [ContratoSistemas] WITH (UPDLOCK) WHERE [ContratoSistemas_CntCod] = @ContratoSistemas_CntCod AND [ContratoSistemas_SistemaCod] = @ContratoSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00492,1,0,true,false )
             ,new CursorDef("BC00493", "SELECT [ContratoSistemas_CntCod] AS ContratoSistemas_CntCod, [ContratoSistemas_SistemaCod] AS ContratoSistemas_SistemaCod FROM [ContratoSistemas] WITH (NOLOCK) WHERE [ContratoSistemas_CntCod] = @ContratoSistemas_CntCod AND [ContratoSistemas_SistemaCod] = @ContratoSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00493,1,0,true,false )
             ,new CursorDef("BC00494", "SELECT [Contrato_Codigo] AS ContratoSistemas_CntCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoSistemas_CntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00494,1,0,true,false )
             ,new CursorDef("BC00495", "SELECT [Sistema_Codigo] AS ContratoSistemas_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContratoSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00495,1,0,true,false )
             ,new CursorDef("BC00496", "SELECT TM1.[ContratoSistemas_CntCod] AS ContratoSistemas_CntCod, TM1.[ContratoSistemas_SistemaCod] AS ContratoSistemas_SistemaCod FROM [ContratoSistemas] TM1 WITH (NOLOCK) WHERE TM1.[ContratoSistemas_CntCod] = @ContratoSistemas_CntCod and TM1.[ContratoSistemas_SistemaCod] = @ContratoSistemas_SistemaCod ORDER BY TM1.[ContratoSistemas_CntCod], TM1.[ContratoSistemas_SistemaCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00496,100,0,true,false )
             ,new CursorDef("BC00497", "SELECT [ContratoSistemas_CntCod] AS ContratoSistemas_CntCod, [ContratoSistemas_SistemaCod] AS ContratoSistemas_SistemaCod FROM [ContratoSistemas] WITH (NOLOCK) WHERE [ContratoSistemas_CntCod] = @ContratoSistemas_CntCod AND [ContratoSistemas_SistemaCod] = @ContratoSistemas_SistemaCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00497,1,0,true,false )
             ,new CursorDef("BC00498", "INSERT INTO [ContratoSistemas]([ContratoSistemas_CntCod], [ContratoSistemas_SistemaCod]) VALUES(@ContratoSistemas_CntCod, @ContratoSistemas_SistemaCod)", GxErrorMask.GX_NOMASK,prmBC00498)
             ,new CursorDef("BC00499", "DELETE FROM [ContratoSistemas]  WHERE [ContratoSistemas_CntCod] = @ContratoSistemas_CntCod AND [ContratoSistemas_SistemaCod] = @ContratoSistemas_SistemaCod", GxErrorMask.GX_NOMASK,prmBC00499)
             ,new CursorDef("BC004910", "SELECT TM1.[ContratoSistemas_CntCod] AS ContratoSistemas_CntCod, TM1.[ContratoSistemas_SistemaCod] AS ContratoSistemas_SistemaCod FROM [ContratoSistemas] TM1 WITH (NOLOCK) WHERE TM1.[ContratoSistemas_CntCod] = @ContratoSistemas_CntCod and TM1.[ContratoSistemas_SistemaCod] = @ContratoSistemas_SistemaCod ORDER BY TM1.[ContratoSistemas_CntCod], TM1.[ContratoSistemas_SistemaCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004910,100,0,true,false )
             ,new CursorDef("BC004911", "SELECT [Contrato_Codigo] AS ContratoSistemas_CntCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoSistemas_CntCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004911,1,0,true,false )
             ,new CursorDef("BC004912", "SELECT [Sistema_Codigo] AS ContratoSistemas_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContratoSistemas_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004912,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
