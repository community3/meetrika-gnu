/*
               File: ContratanteUsuario
        Description: Contratante Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:15:52.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratanteusuario : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A63ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A63ContratanteUsuario_ContratanteCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_9") == 0 )
         {
            A63ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_9( A63ContratanteUsuario_ContratanteCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A340ContratanteUsuario_ContratantePesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n340ContratanteUsuario_ContratantePesCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A340ContratanteUsuario_ContratantePesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A340ContratanteUsuario_ContratantePesCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A340ContratanteUsuario_ContratantePesCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A60ContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A60ContratanteUsuario_UsuarioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A61ContratanteUsuario_UsuarioPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n61ContratanteUsuario_UsuarioPessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61ContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A61ContratanteUsuario_UsuarioPessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratanteUsuario_ContratanteCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTEUSUARIO_CONTRATANTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratanteUsuario_ContratanteCod), "ZZZZZ9")));
               AV8ContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratanteUsuario_UsuarioCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTEUSUARIO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContratanteUsuario_UsuarioCod), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contratante Usuario", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratanteusuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratanteusuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratanteUsuario_ContratanteCod ,
                           int aP2_ContratanteUsuario_UsuarioCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratanteUsuario_ContratanteCod = aP1_ContratanteUsuario_ContratanteCod;
         this.AV8ContratanteUsuario_UsuarioCod = aP2_ContratanteUsuario_UsuarioCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0D15( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0D15e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratanteUsuario_ContratanteCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A63ContratanteUsuario_ContratanteCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratanteUsuario_ContratanteCod_Jsonclick, 0, "Attribute", "", "", "", edtContratanteUsuario_ContratanteCod_Visible, edtContratanteUsuario_ContratanteCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratanteUsuario.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratanteUsuario_ContratanteFan_Internalname, StringUtil.RTrim( A65ContratanteUsuario_ContratanteFan), StringUtil.RTrim( context.localUtil.Format( A65ContratanteUsuario_ContratanteFan, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratanteUsuario_ContratanteFan_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtContratanteUsuario_ContratanteFan_Visible, edtContratanteUsuario_ContratanteFan_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratanteUsuario_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A60ContratanteUsuario_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratanteUsuario_UsuarioCod_Jsonclick, 0, "Attribute", "", "", "", edtContratanteUsuario_UsuarioCod_Visible, edtContratanteUsuario_UsuarioCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratanteUsuario.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratanteUsuario_UsuarioPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0, ",", "")), ((edtContratanteUsuario_UsuarioPessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratanteUsuario_UsuarioPessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContratanteUsuario_UsuarioPessoaCod_Visible, edtContratanteUsuario_UsuarioPessoaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratanteUsuario.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0D15( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0D15( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0D15e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_36_0D15( true) ;
         }
         return  ;
      }

      protected void wb_table3_36_0D15e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0D15e( true) ;
         }
         else
         {
            wb_table1_2_0D15e( false) ;
         }
      }

      protected void wb_table3_36_0D15( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_36_0D15e( true) ;
         }
         else
         {
            wb_table3_36_0D15e( false) ;
         }
      }

      protected void wb_table2_5_0D15( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_12_0D15( true) ;
         }
         return  ;
      }

      protected void wb_table4_12_0D15e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0D15e( true) ;
         }
         else
         {
            wb_table2_5_0D15e( false) ;
         }
      }

      protected void wb_table4_12_0D15( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table5_17_0D15( true) ;
         }
         return  ;
      }

      protected void wb_table5_17_0D15e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_12_0D15e( true) ;
         }
         else
         {
            wb_table4_12_0D15e( false) ;
         }
      }

      protected void wb_table5_17_0D15( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_nome_Internalname, "Usu�rio", "", "", lblTextblockusuario_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuario_Nome_Internalname, StringUtil.RTrim( A2Usuario_Nome), StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuario_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtUsuario_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratanteusuario_usuariopessoanom_Internalname, "Pessoa", "", "", lblTextblockcontratanteusuario_usuariopessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratanteUsuario_UsuarioPessoaNom_Internalname, StringUtil.RTrim( A62ContratanteUsuario_UsuarioPessoaNom), StringUtil.RTrim( context.localUtil.Format( A62ContratanteUsuario_UsuarioPessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratanteUsuario_UsuarioPessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratanteUsuario_UsuarioPessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratanteusuario_contratanteraz_Internalname, "Contratante", "", "", lblTextblockcontratanteusuario_contratanteraz_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratanteUsuario_ContratanteRaz_Internalname, StringUtil.RTrim( A64ContratanteUsuario_ContratanteRaz), StringUtil.RTrim( context.localUtil.Format( A64ContratanteUsuario_ContratanteRaz, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratanteUsuario_ContratanteRaz_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratanteUsuario_ContratanteRaz_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_17_0D15e( true) ;
         }
         else
         {
            wb_table5_17_0D15e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110D2 */
         E110D2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A2Usuario_Nome = StringUtil.Upper( cgiGet( edtUsuario_Nome_Internalname));
               n2Usuario_Nome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
               A62ContratanteUsuario_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtContratanteUsuario_UsuarioPessoaNom_Internalname));
               n62ContratanteUsuario_UsuarioPessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62ContratanteUsuario_UsuarioPessoaNom", A62ContratanteUsuario_UsuarioPessoaNom);
               A64ContratanteUsuario_ContratanteRaz = StringUtil.Upper( cgiGet( edtContratanteUsuario_ContratanteRaz_Internalname));
               n64ContratanteUsuario_ContratanteRaz = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A64ContratanteUsuario_ContratanteRaz", A64ContratanteUsuario_ContratanteRaz);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratanteUsuario_ContratanteCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratanteUsuario_ContratanteCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A63ContratanteUsuario_ContratanteCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
               }
               else
               {
                  A63ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( edtContratanteUsuario_ContratanteCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
               }
               A65ContratanteUsuario_ContratanteFan = StringUtil.Upper( cgiGet( edtContratanteUsuario_ContratanteFan_Internalname));
               n65ContratanteUsuario_ContratanteFan = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A65ContratanteUsuario_ContratanteFan", A65ContratanteUsuario_ContratanteFan);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratanteUsuario_UsuarioCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratanteUsuario_UsuarioCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATANTEUSUARIO_USUARIOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratanteUsuario_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A60ContratanteUsuario_UsuarioCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
               }
               else
               {
                  A60ContratanteUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContratanteUsuario_UsuarioCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
               }
               A61ContratanteUsuario_UsuarioPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratanteUsuario_UsuarioPessoaCod_Internalname), ",", "."));
               n61ContratanteUsuario_UsuarioPessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61ContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0)));
               /* Read saved values. */
               Z63ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( "Z63ContratanteUsuario_ContratanteCod"), ",", "."));
               Z60ContratanteUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "Z60ContratanteUsuario_UsuarioCod"), ",", "."));
               Z1728ContratanteUsuario_EhFiscal = StringUtil.StrToBool( cgiGet( "Z1728ContratanteUsuario_EhFiscal"));
               n1728ContratanteUsuario_EhFiscal = ((false==A1728ContratanteUsuario_EhFiscal) ? true : false);
               A1728ContratanteUsuario_EhFiscal = StringUtil.StrToBool( cgiGet( "Z1728ContratanteUsuario_EhFiscal"));
               n1728ContratanteUsuario_EhFiscal = false;
               n1728ContratanteUsuario_EhFiscal = ((false==A1728ContratanteUsuario_EhFiscal) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7ContratanteUsuario_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( "vCONTRATANTEUSUARIO_CONTRATANTECOD"), ",", "."));
               AV8ContratanteUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "vCONTRATANTEUSUARIO_USUARIOCOD"), ",", "."));
               A1728ContratanteUsuario_EhFiscal = StringUtil.StrToBool( cgiGet( "CONTRATANTEUSUARIO_EHFISCAL"));
               n1728ContratanteUsuario_EhFiscal = ((false==A1728ContratanteUsuario_EhFiscal) ? true : false);
               A1020ContratanteUsuario_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATANTEUSUARIO_AREATRABALHOCOD"), ",", "."));
               n1020ContratanteUsuario_AreaTrabalhoCod = false;
               A340ContratanteUsuario_ContratantePesCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATANTEUSUARIO_CONTRATANTEPESCOD"), ",", "."));
               A1019ContratanteUsuario_UsuarioEhContratante = StringUtil.StrToBool( cgiGet( "CONTRATANTEUSUARIO_USUARIOEHCONTRATANTE"));
               n1019ContratanteUsuario_UsuarioEhContratante = false;
               A341Usuario_UserGamGuid = cgiGet( "USUARIO_USERGAMGUID");
               n341Usuario_UserGamGuid = false;
               A492ContratanteUsuario_UsuarioPessoaDoc = cgiGet( "CONTRATANTEUSUARIO_USUARIOPESSOADOC");
               n492ContratanteUsuario_UsuarioPessoaDoc = false;
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratanteUsuario";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1728ContratanteUsuario_EhFiscal);
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A63ContratanteUsuario_ContratanteCod != Z63ContratanteUsuario_ContratanteCod ) || ( A60ContratanteUsuario_UsuarioCod != Z60ContratanteUsuario_UsuarioCod ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratanteusuario:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratanteusuario:[SecurityCheckFailed value for]"+"ContratanteUsuario_EhFiscal:"+StringUtil.BoolToStr( A1728ContratanteUsuario_EhFiscal));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A63ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
                  A60ContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode15 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode15;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound15 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0D0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
                        AnyError = 1;
                        GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110D2 */
                           E110D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120D2 */
                           E120D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120D2 */
            E120D2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0D15( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0D15( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0D0( )
      {
         BeforeValidate0D15( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0D15( ) ;
            }
            else
            {
               CheckExtendedTable0D15( ) ;
               CloseExtendedTableCursors0D15( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0D0( )
      {
      }

      protected void E110D2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         edtContratanteUsuario_ContratanteCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_ContratanteCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_ContratanteCod_Visible), 5, 0)));
         edtContratanteUsuario_ContratanteFan_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_ContratanteFan_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_ContratanteFan_Visible), 5, 0)));
         edtContratanteUsuario_UsuarioCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_UsuarioCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_UsuarioCod_Visible), 5, 0)));
         edtContratanteUsuario_UsuarioPessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_UsuarioPessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_UsuarioPessoaCod_Visible), 5, 0)));
         AV12AreaTrabalho_Codigo = AV9WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12AreaTrabalho_Codigo), 6, 0)));
      }

      protected void E120D2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            new prc_tirardagestaodecontratos(context ).execute( ref  AV8ContratanteUsuario_UsuarioCod) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContratanteUsuario_UsuarioCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTEUSUARIO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContratanteUsuario_UsuarioCod), "ZZZZZ9")));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV10TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratanteusuario.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0D15( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1728ContratanteUsuario_EhFiscal = T000D3_A1728ContratanteUsuario_EhFiscal[0];
            }
            else
            {
               Z1728ContratanteUsuario_EhFiscal = A1728ContratanteUsuario_EhFiscal;
            }
         }
         if ( GX_JID == -7 )
         {
            Z1728ContratanteUsuario_EhFiscal = A1728ContratanteUsuario_EhFiscal;
            Z63ContratanteUsuario_ContratanteCod = A63ContratanteUsuario_ContratanteCod;
            Z60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
            Z65ContratanteUsuario_ContratanteFan = A65ContratanteUsuario_ContratanteFan;
            Z340ContratanteUsuario_ContratantePesCod = A340ContratanteUsuario_ContratantePesCod;
            Z64ContratanteUsuario_ContratanteRaz = A64ContratanteUsuario_ContratanteRaz;
            Z2Usuario_Nome = A2Usuario_Nome;
            Z1019ContratanteUsuario_UsuarioEhContratante = A1019ContratanteUsuario_UsuarioEhContratante;
            Z341Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            Z61ContratanteUsuario_UsuarioPessoaCod = A61ContratanteUsuario_UsuarioPessoaCod;
            Z62ContratanteUsuario_UsuarioPessoaNom = A62ContratanteUsuario_UsuarioPessoaNom;
            Z492ContratanteUsuario_UsuarioPessoaDoc = A492ContratanteUsuario_UsuarioPessoaDoc;
         }
      }

      protected void standaloneNotModal( )
      {
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratanteUsuario_ContratanteCod) )
         {
            A63ContratanteUsuario_ContratanteCod = AV7ContratanteUsuario_ContratanteCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
         }
         if ( ! (0==AV7ContratanteUsuario_ContratanteCod) )
         {
            edtContratanteUsuario_ContratanteCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_ContratanteCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_ContratanteCod_Enabled), 5, 0)));
         }
         else
         {
            edtContratanteUsuario_ContratanteCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_ContratanteCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_ContratanteCod_Enabled), 5, 0)));
         }
         if ( ! (0==AV7ContratanteUsuario_ContratanteCod) )
         {
            edtContratanteUsuario_ContratanteCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_ContratanteCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_ContratanteCod_Enabled), 5, 0)));
         }
         if ( ! (0==AV8ContratanteUsuario_UsuarioCod) )
         {
            A60ContratanteUsuario_UsuarioCod = AV8ContratanteUsuario_UsuarioCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
         }
         if ( ! (0==AV8ContratanteUsuario_UsuarioCod) )
         {
            edtContratanteUsuario_UsuarioCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_UsuarioCod_Enabled), 5, 0)));
         }
         else
         {
            edtContratanteUsuario_UsuarioCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_UsuarioCod_Enabled), 5, 0)));
         }
         if ( ! (0==AV8ContratanteUsuario_UsuarioCod) )
         {
            edtContratanteUsuario_UsuarioCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_UsuarioCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T000D5 */
            pr_default.execute(2, new Object[] {A63ContratanteUsuario_ContratanteCod});
            if ( (pr_default.getStatus(2) != 101) )
            {
               A1020ContratanteUsuario_AreaTrabalhoCod = T000D5_A1020ContratanteUsuario_AreaTrabalhoCod[0];
               n1020ContratanteUsuario_AreaTrabalhoCod = T000D5_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            }
            else
            {
               A1020ContratanteUsuario_AreaTrabalhoCod = 0;
               n1020ContratanteUsuario_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1020ContratanteUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1020ContratanteUsuario_AreaTrabalhoCod), 6, 0)));
            }
            pr_default.close(2);
            /* Using cursor T000D6 */
            pr_default.execute(3, new Object[] {A63ContratanteUsuario_ContratanteCod});
            A65ContratanteUsuario_ContratanteFan = T000D6_A65ContratanteUsuario_ContratanteFan[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A65ContratanteUsuario_ContratanteFan", A65ContratanteUsuario_ContratanteFan);
            n65ContratanteUsuario_ContratanteFan = T000D6_n65ContratanteUsuario_ContratanteFan[0];
            A340ContratanteUsuario_ContratantePesCod = T000D6_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = T000D6_n340ContratanteUsuario_ContratantePesCod[0];
            pr_default.close(3);
            /* Using cursor T000D8 */
            pr_default.execute(5, new Object[] {n340ContratanteUsuario_ContratantePesCod, A340ContratanteUsuario_ContratantePesCod});
            A64ContratanteUsuario_ContratanteRaz = T000D8_A64ContratanteUsuario_ContratanteRaz[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A64ContratanteUsuario_ContratanteRaz", A64ContratanteUsuario_ContratanteRaz);
            n64ContratanteUsuario_ContratanteRaz = T000D8_n64ContratanteUsuario_ContratanteRaz[0];
            pr_default.close(5);
            /* Using cursor T000D7 */
            pr_default.execute(4, new Object[] {A60ContratanteUsuario_UsuarioCod});
            A2Usuario_Nome = T000D7_A2Usuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
            n2Usuario_Nome = T000D7_n2Usuario_Nome[0];
            A1019ContratanteUsuario_UsuarioEhContratante = T000D7_A1019ContratanteUsuario_UsuarioEhContratante[0];
            n1019ContratanteUsuario_UsuarioEhContratante = T000D7_n1019ContratanteUsuario_UsuarioEhContratante[0];
            A341Usuario_UserGamGuid = T000D7_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = T000D7_n341Usuario_UserGamGuid[0];
            A61ContratanteUsuario_UsuarioPessoaCod = T000D7_A61ContratanteUsuario_UsuarioPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61ContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0)));
            n61ContratanteUsuario_UsuarioPessoaCod = T000D7_n61ContratanteUsuario_UsuarioPessoaCod[0];
            pr_default.close(4);
            /* Using cursor T000D9 */
            pr_default.execute(6, new Object[] {n61ContratanteUsuario_UsuarioPessoaCod, A61ContratanteUsuario_UsuarioPessoaCod});
            A62ContratanteUsuario_UsuarioPessoaNom = T000D9_A62ContratanteUsuario_UsuarioPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62ContratanteUsuario_UsuarioPessoaNom", A62ContratanteUsuario_UsuarioPessoaNom);
            n62ContratanteUsuario_UsuarioPessoaNom = T000D9_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A492ContratanteUsuario_UsuarioPessoaDoc = T000D9_A492ContratanteUsuario_UsuarioPessoaDoc[0];
            n492ContratanteUsuario_UsuarioPessoaDoc = T000D9_n492ContratanteUsuario_UsuarioPessoaDoc[0];
            pr_default.close(6);
         }
      }

      protected void Load0D15( )
      {
         /* Using cursor T000D10 */
         pr_default.execute(7, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound15 = 1;
            A65ContratanteUsuario_ContratanteFan = T000D10_A65ContratanteUsuario_ContratanteFan[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A65ContratanteUsuario_ContratanteFan", A65ContratanteUsuario_ContratanteFan);
            n65ContratanteUsuario_ContratanteFan = T000D10_n65ContratanteUsuario_ContratanteFan[0];
            A64ContratanteUsuario_ContratanteRaz = T000D10_A64ContratanteUsuario_ContratanteRaz[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A64ContratanteUsuario_ContratanteRaz", A64ContratanteUsuario_ContratanteRaz);
            n64ContratanteUsuario_ContratanteRaz = T000D10_n64ContratanteUsuario_ContratanteRaz[0];
            A2Usuario_Nome = T000D10_A2Usuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
            n2Usuario_Nome = T000D10_n2Usuario_Nome[0];
            A62ContratanteUsuario_UsuarioPessoaNom = T000D10_A62ContratanteUsuario_UsuarioPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62ContratanteUsuario_UsuarioPessoaNom", A62ContratanteUsuario_UsuarioPessoaNom);
            n62ContratanteUsuario_UsuarioPessoaNom = T000D10_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A492ContratanteUsuario_UsuarioPessoaDoc = T000D10_A492ContratanteUsuario_UsuarioPessoaDoc[0];
            n492ContratanteUsuario_UsuarioPessoaDoc = T000D10_n492ContratanteUsuario_UsuarioPessoaDoc[0];
            A1019ContratanteUsuario_UsuarioEhContratante = T000D10_A1019ContratanteUsuario_UsuarioEhContratante[0];
            n1019ContratanteUsuario_UsuarioEhContratante = T000D10_n1019ContratanteUsuario_UsuarioEhContratante[0];
            A341Usuario_UserGamGuid = T000D10_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = T000D10_n341Usuario_UserGamGuid[0];
            A1728ContratanteUsuario_EhFiscal = T000D10_A1728ContratanteUsuario_EhFiscal[0];
            n1728ContratanteUsuario_EhFiscal = T000D10_n1728ContratanteUsuario_EhFiscal[0];
            A340ContratanteUsuario_ContratantePesCod = T000D10_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = T000D10_n340ContratanteUsuario_ContratantePesCod[0];
            A61ContratanteUsuario_UsuarioPessoaCod = T000D10_A61ContratanteUsuario_UsuarioPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61ContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0)));
            n61ContratanteUsuario_UsuarioPessoaCod = T000D10_n61ContratanteUsuario_UsuarioPessoaCod[0];
            ZM0D15( -7) ;
         }
         pr_default.close(7);
         OnLoadActions0D15( ) ;
      }

      protected void OnLoadActions0D15( )
      {
         /* Using cursor T000D5 */
         pr_default.execute(2, new Object[] {A63ContratanteUsuario_ContratanteCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1020ContratanteUsuario_AreaTrabalhoCod = T000D5_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = T000D5_n1020ContratanteUsuario_AreaTrabalhoCod[0];
         }
         else
         {
            A1020ContratanteUsuario_AreaTrabalhoCod = 0;
            n1020ContratanteUsuario_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1020ContratanteUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1020ContratanteUsuario_AreaTrabalhoCod), 6, 0)));
         }
         pr_default.close(2);
      }

      protected void CheckExtendedTable0D15( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T000D5 */
         pr_default.execute(2, new Object[] {A63ContratanteUsuario_ContratanteCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1020ContratanteUsuario_AreaTrabalhoCod = T000D5_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = T000D5_n1020ContratanteUsuario_AreaTrabalhoCod[0];
         }
         else
         {
            A1020ContratanteUsuario_AreaTrabalhoCod = 0;
            n1020ContratanteUsuario_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1020ContratanteUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1020ContratanteUsuario_AreaTrabalhoCod), 6, 0)));
         }
         pr_default.close(2);
         /* Using cursor T000D6 */
         pr_default.execute(3, new Object[] {A63ContratanteUsuario_ContratanteCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratante Usuario_Contratante'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
            AnyError = 1;
            GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A65ContratanteUsuario_ContratanteFan = T000D6_A65ContratanteUsuario_ContratanteFan[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A65ContratanteUsuario_ContratanteFan", A65ContratanteUsuario_ContratanteFan);
         n65ContratanteUsuario_ContratanteFan = T000D6_n65ContratanteUsuario_ContratanteFan[0];
         A340ContratanteUsuario_ContratantePesCod = T000D6_A340ContratanteUsuario_ContratantePesCod[0];
         n340ContratanteUsuario_ContratantePesCod = T000D6_n340ContratanteUsuario_ContratantePesCod[0];
         pr_default.close(3);
         /* Using cursor T000D8 */
         pr_default.execute(5, new Object[] {n340ContratanteUsuario_ContratantePesCod, A340ContratanteUsuario_ContratantePesCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A64ContratanteUsuario_ContratanteRaz = T000D8_A64ContratanteUsuario_ContratanteRaz[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A64ContratanteUsuario_ContratanteRaz", A64ContratanteUsuario_ContratanteRaz);
         n64ContratanteUsuario_ContratanteRaz = T000D8_n64ContratanteUsuario_ContratanteRaz[0];
         pr_default.close(5);
         /* Using cursor T000D7 */
         pr_default.execute(4, new Object[] {A60ContratanteUsuario_UsuarioCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratante Usuario_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratanteUsuario_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2Usuario_Nome = T000D7_A2Usuario_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
         n2Usuario_Nome = T000D7_n2Usuario_Nome[0];
         A1019ContratanteUsuario_UsuarioEhContratante = T000D7_A1019ContratanteUsuario_UsuarioEhContratante[0];
         n1019ContratanteUsuario_UsuarioEhContratante = T000D7_n1019ContratanteUsuario_UsuarioEhContratante[0];
         A341Usuario_UserGamGuid = T000D7_A341Usuario_UserGamGuid[0];
         n341Usuario_UserGamGuid = T000D7_n341Usuario_UserGamGuid[0];
         A61ContratanteUsuario_UsuarioPessoaCod = T000D7_A61ContratanteUsuario_UsuarioPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61ContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0)));
         n61ContratanteUsuario_UsuarioPessoaCod = T000D7_n61ContratanteUsuario_UsuarioPessoaCod[0];
         pr_default.close(4);
         /* Using cursor T000D9 */
         pr_default.execute(6, new Object[] {n61ContratanteUsuario_UsuarioPessoaCod, A61ContratanteUsuario_UsuarioPessoaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A62ContratanteUsuario_UsuarioPessoaNom = T000D9_A62ContratanteUsuario_UsuarioPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62ContratanteUsuario_UsuarioPessoaNom", A62ContratanteUsuario_UsuarioPessoaNom);
         n62ContratanteUsuario_UsuarioPessoaNom = T000D9_n62ContratanteUsuario_UsuarioPessoaNom[0];
         A492ContratanteUsuario_UsuarioPessoaDoc = T000D9_A492ContratanteUsuario_UsuarioPessoaDoc[0];
         n492ContratanteUsuario_UsuarioPessoaDoc = T000D9_n492ContratanteUsuario_UsuarioPessoaDoc[0];
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors0D15( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(5);
         pr_default.close(4);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_8( int A63ContratanteUsuario_ContratanteCod )
      {
         /* Using cursor T000D12 */
         pr_default.execute(8, new Object[] {A63ContratanteUsuario_ContratanteCod});
         if ( (pr_default.getStatus(8) != 101) )
         {
            A1020ContratanteUsuario_AreaTrabalhoCod = T000D12_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = T000D12_n1020ContratanteUsuario_AreaTrabalhoCod[0];
         }
         else
         {
            A1020ContratanteUsuario_AreaTrabalhoCod = 0;
            n1020ContratanteUsuario_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1020ContratanteUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1020ContratanteUsuario_AreaTrabalhoCod), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1020ContratanteUsuario_AreaTrabalhoCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_9( int A63ContratanteUsuario_ContratanteCod )
      {
         /* Using cursor T000D13 */
         pr_default.execute(9, new Object[] {A63ContratanteUsuario_ContratanteCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratante Usuario_Contratante'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
            AnyError = 1;
            GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A65ContratanteUsuario_ContratanteFan = T000D13_A65ContratanteUsuario_ContratanteFan[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A65ContratanteUsuario_ContratanteFan", A65ContratanteUsuario_ContratanteFan);
         n65ContratanteUsuario_ContratanteFan = T000D13_n65ContratanteUsuario_ContratanteFan[0];
         A340ContratanteUsuario_ContratantePesCod = T000D13_A340ContratanteUsuario_ContratantePesCod[0];
         n340ContratanteUsuario_ContratantePesCod = T000D13_n340ContratanteUsuario_ContratantePesCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A65ContratanteUsuario_ContratanteFan))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A340ContratanteUsuario_ContratantePesCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_11( int A340ContratanteUsuario_ContratantePesCod )
      {
         /* Using cursor T000D14 */
         pr_default.execute(10, new Object[] {n340ContratanteUsuario_ContratantePesCod, A340ContratanteUsuario_ContratantePesCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A64ContratanteUsuario_ContratanteRaz = T000D14_A64ContratanteUsuario_ContratanteRaz[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A64ContratanteUsuario_ContratanteRaz", A64ContratanteUsuario_ContratanteRaz);
         n64ContratanteUsuario_ContratanteRaz = T000D14_n64ContratanteUsuario_ContratanteRaz[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A64ContratanteUsuario_ContratanteRaz))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_10( int A60ContratanteUsuario_UsuarioCod )
      {
         /* Using cursor T000D15 */
         pr_default.execute(11, new Object[] {A60ContratanteUsuario_UsuarioCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratante Usuario_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratanteUsuario_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2Usuario_Nome = T000D15_A2Usuario_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
         n2Usuario_Nome = T000D15_n2Usuario_Nome[0];
         A1019ContratanteUsuario_UsuarioEhContratante = T000D15_A1019ContratanteUsuario_UsuarioEhContratante[0];
         n1019ContratanteUsuario_UsuarioEhContratante = T000D15_n1019ContratanteUsuario_UsuarioEhContratante[0];
         A341Usuario_UserGamGuid = T000D15_A341Usuario_UserGamGuid[0];
         n341Usuario_UserGamGuid = T000D15_n341Usuario_UserGamGuid[0];
         A61ContratanteUsuario_UsuarioPessoaCod = T000D15_A61ContratanteUsuario_UsuarioPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61ContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0)));
         n61ContratanteUsuario_UsuarioPessoaCod = T000D15_n61ContratanteUsuario_UsuarioPessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2Usuario_Nome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A1019ContratanteUsuario_UsuarioEhContratante))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A341Usuario_UserGamGuid))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void gxLoad_12( int A61ContratanteUsuario_UsuarioPessoaCod )
      {
         /* Using cursor T000D16 */
         pr_default.execute(12, new Object[] {n61ContratanteUsuario_UsuarioPessoaCod, A61ContratanteUsuario_UsuarioPessoaCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A62ContratanteUsuario_UsuarioPessoaNom = T000D16_A62ContratanteUsuario_UsuarioPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62ContratanteUsuario_UsuarioPessoaNom", A62ContratanteUsuario_UsuarioPessoaNom);
         n62ContratanteUsuario_UsuarioPessoaNom = T000D16_n62ContratanteUsuario_UsuarioPessoaNom[0];
         A492ContratanteUsuario_UsuarioPessoaDoc = T000D16_A492ContratanteUsuario_UsuarioPessoaDoc[0];
         n492ContratanteUsuario_UsuarioPessoaDoc = T000D16_n492ContratanteUsuario_UsuarioPessoaDoc[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A62ContratanteUsuario_UsuarioPessoaNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( A492ContratanteUsuario_UsuarioPessoaDoc)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void GetKey0D15( )
      {
         /* Using cursor T000D17 */
         pr_default.execute(13, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound15 = 1;
         }
         else
         {
            RcdFound15 = 0;
         }
         pr_default.close(13);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000D3 */
         pr_default.execute(1, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0D15( 7) ;
            RcdFound15 = 1;
            A1728ContratanteUsuario_EhFiscal = T000D3_A1728ContratanteUsuario_EhFiscal[0];
            n1728ContratanteUsuario_EhFiscal = T000D3_n1728ContratanteUsuario_EhFiscal[0];
            A63ContratanteUsuario_ContratanteCod = T000D3_A63ContratanteUsuario_ContratanteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
            A60ContratanteUsuario_UsuarioCod = T000D3_A60ContratanteUsuario_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
            Z63ContratanteUsuario_ContratanteCod = A63ContratanteUsuario_ContratanteCod;
            Z60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
            sMode15 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0D15( ) ;
            if ( AnyError == 1 )
            {
               RcdFound15 = 0;
               InitializeNonKey0D15( ) ;
            }
            Gx_mode = sMode15;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound15 = 0;
            InitializeNonKey0D15( ) ;
            sMode15 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode15;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0D15( ) ;
         if ( RcdFound15 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound15 = 0;
         /* Using cursor T000D18 */
         pr_default.execute(14, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
         if ( (pr_default.getStatus(14) != 101) )
         {
            while ( (pr_default.getStatus(14) != 101) && ( ( T000D18_A63ContratanteUsuario_ContratanteCod[0] < A63ContratanteUsuario_ContratanteCod ) || ( T000D18_A63ContratanteUsuario_ContratanteCod[0] == A63ContratanteUsuario_ContratanteCod ) && ( T000D18_A60ContratanteUsuario_UsuarioCod[0] < A60ContratanteUsuario_UsuarioCod ) ) )
            {
               pr_default.readNext(14);
            }
            if ( (pr_default.getStatus(14) != 101) && ( ( T000D18_A63ContratanteUsuario_ContratanteCod[0] > A63ContratanteUsuario_ContratanteCod ) || ( T000D18_A63ContratanteUsuario_ContratanteCod[0] == A63ContratanteUsuario_ContratanteCod ) && ( T000D18_A60ContratanteUsuario_UsuarioCod[0] > A60ContratanteUsuario_UsuarioCod ) ) )
            {
               A63ContratanteUsuario_ContratanteCod = T000D18_A63ContratanteUsuario_ContratanteCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
               A60ContratanteUsuario_UsuarioCod = T000D18_A60ContratanteUsuario_UsuarioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
               RcdFound15 = 1;
            }
         }
         pr_default.close(14);
      }

      protected void move_previous( )
      {
         RcdFound15 = 0;
         /* Using cursor T000D19 */
         pr_default.execute(15, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
         if ( (pr_default.getStatus(15) != 101) )
         {
            while ( (pr_default.getStatus(15) != 101) && ( ( T000D19_A63ContratanteUsuario_ContratanteCod[0] > A63ContratanteUsuario_ContratanteCod ) || ( T000D19_A63ContratanteUsuario_ContratanteCod[0] == A63ContratanteUsuario_ContratanteCod ) && ( T000D19_A60ContratanteUsuario_UsuarioCod[0] > A60ContratanteUsuario_UsuarioCod ) ) )
            {
               pr_default.readNext(15);
            }
            if ( (pr_default.getStatus(15) != 101) && ( ( T000D19_A63ContratanteUsuario_ContratanteCod[0] < A63ContratanteUsuario_ContratanteCod ) || ( T000D19_A63ContratanteUsuario_ContratanteCod[0] == A63ContratanteUsuario_ContratanteCod ) && ( T000D19_A60ContratanteUsuario_UsuarioCod[0] < A60ContratanteUsuario_UsuarioCod ) ) )
            {
               A63ContratanteUsuario_ContratanteCod = T000D19_A63ContratanteUsuario_ContratanteCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
               A60ContratanteUsuario_UsuarioCod = T000D19_A60ContratanteUsuario_UsuarioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
               RcdFound15 = 1;
            }
         }
         pr_default.close(15);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0D15( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0D15( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound15 == 1 )
            {
               if ( ( A63ContratanteUsuario_ContratanteCod != Z63ContratanteUsuario_ContratanteCod ) || ( A60ContratanteUsuario_UsuarioCod != Z60ContratanteUsuario_UsuarioCod ) )
               {
                  A63ContratanteUsuario_ContratanteCod = Z63ContratanteUsuario_ContratanteCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
                  A60ContratanteUsuario_UsuarioCod = Z60ContratanteUsuario_UsuarioCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0D15( ) ;
                  GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A63ContratanteUsuario_ContratanteCod != Z63ContratanteUsuario_ContratanteCod ) || ( A60ContratanteUsuario_UsuarioCod != Z60ContratanteUsuario_UsuarioCod ) )
               {
                  /* Insert record */
                  GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0D15( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
                     AnyError = 1;
                     GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0D15( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A63ContratanteUsuario_ContratanteCod != Z63ContratanteUsuario_ContratanteCod ) || ( A60ContratanteUsuario_UsuarioCod != Z60ContratanteUsuario_UsuarioCod ) )
         {
            A63ContratanteUsuario_ContratanteCod = Z63ContratanteUsuario_ContratanteCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
            A60ContratanteUsuario_UsuarioCod = Z60ContratanteUsuario_UsuarioCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
            AnyError = 1;
            GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0D15( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000D2 */
            pr_default.execute(0, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratanteUsuario"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1728ContratanteUsuario_EhFiscal != T000D2_A1728ContratanteUsuario_EhFiscal[0] ) )
            {
               if ( Z1728ContratanteUsuario_EhFiscal != T000D2_A1728ContratanteUsuario_EhFiscal[0] )
               {
                  GXUtil.WriteLog("contratanteusuario:[seudo value changed for attri]"+"ContratanteUsuario_EhFiscal");
                  GXUtil.WriteLogRaw("Old: ",Z1728ContratanteUsuario_EhFiscal);
                  GXUtil.WriteLogRaw("Current: ",T000D2_A1728ContratanteUsuario_EhFiscal[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratanteUsuario"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0D15( )
      {
         BeforeValidate0D15( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0D15( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0D15( 0) ;
            CheckOptimisticConcurrency0D15( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0D15( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0D15( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000D20 */
                     pr_default.execute(16, new Object[] {n1728ContratanteUsuario_EhFiscal, A1728ContratanteUsuario_EhFiscal, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratanteUsuario") ;
                     if ( (pr_default.getStatus(16) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0D0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0D15( ) ;
            }
            EndLevel0D15( ) ;
         }
         CloseExtendedTableCursors0D15( ) ;
      }

      protected void Update0D15( )
      {
         BeforeValidate0D15( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0D15( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0D15( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0D15( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0D15( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000D21 */
                     pr_default.execute(17, new Object[] {n1728ContratanteUsuario_EhFiscal, A1728ContratanteUsuario_EhFiscal, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
                     pr_default.close(17);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratanteUsuario") ;
                     if ( (pr_default.getStatus(17) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratanteUsuario"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0D15( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0D15( ) ;
         }
         CloseExtendedTableCursors0D15( ) ;
      }

      protected void DeferredUpdate0D15( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0D15( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0D15( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0D15( ) ;
            AfterConfirm0D15( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0D15( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000D22 */
                  pr_default.execute(18, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
                  pr_default.close(18);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratanteUsuario") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode15 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0D15( ) ;
         Gx_mode = sMode15;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0D15( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000D24 */
            pr_default.execute(19, new Object[] {A63ContratanteUsuario_ContratanteCod});
            if ( (pr_default.getStatus(19) != 101) )
            {
               A1020ContratanteUsuario_AreaTrabalhoCod = T000D24_A1020ContratanteUsuario_AreaTrabalhoCod[0];
               n1020ContratanteUsuario_AreaTrabalhoCod = T000D24_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            }
            else
            {
               A1020ContratanteUsuario_AreaTrabalhoCod = 0;
               n1020ContratanteUsuario_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1020ContratanteUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1020ContratanteUsuario_AreaTrabalhoCod), 6, 0)));
            }
            pr_default.close(19);
            /* Using cursor T000D25 */
            pr_default.execute(20, new Object[] {A63ContratanteUsuario_ContratanteCod});
            A65ContratanteUsuario_ContratanteFan = T000D25_A65ContratanteUsuario_ContratanteFan[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A65ContratanteUsuario_ContratanteFan", A65ContratanteUsuario_ContratanteFan);
            n65ContratanteUsuario_ContratanteFan = T000D25_n65ContratanteUsuario_ContratanteFan[0];
            A340ContratanteUsuario_ContratantePesCod = T000D25_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = T000D25_n340ContratanteUsuario_ContratantePesCod[0];
            pr_default.close(20);
            /* Using cursor T000D26 */
            pr_default.execute(21, new Object[] {n340ContratanteUsuario_ContratantePesCod, A340ContratanteUsuario_ContratantePesCod});
            A64ContratanteUsuario_ContratanteRaz = T000D26_A64ContratanteUsuario_ContratanteRaz[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A64ContratanteUsuario_ContratanteRaz", A64ContratanteUsuario_ContratanteRaz);
            n64ContratanteUsuario_ContratanteRaz = T000D26_n64ContratanteUsuario_ContratanteRaz[0];
            pr_default.close(21);
            /* Using cursor T000D27 */
            pr_default.execute(22, new Object[] {A60ContratanteUsuario_UsuarioCod});
            A2Usuario_Nome = T000D27_A2Usuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
            n2Usuario_Nome = T000D27_n2Usuario_Nome[0];
            A1019ContratanteUsuario_UsuarioEhContratante = T000D27_A1019ContratanteUsuario_UsuarioEhContratante[0];
            n1019ContratanteUsuario_UsuarioEhContratante = T000D27_n1019ContratanteUsuario_UsuarioEhContratante[0];
            A341Usuario_UserGamGuid = T000D27_A341Usuario_UserGamGuid[0];
            n341Usuario_UserGamGuid = T000D27_n341Usuario_UserGamGuid[0];
            A61ContratanteUsuario_UsuarioPessoaCod = T000D27_A61ContratanteUsuario_UsuarioPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61ContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0)));
            n61ContratanteUsuario_UsuarioPessoaCod = T000D27_n61ContratanteUsuario_UsuarioPessoaCod[0];
            pr_default.close(22);
            /* Using cursor T000D28 */
            pr_default.execute(23, new Object[] {n61ContratanteUsuario_UsuarioPessoaCod, A61ContratanteUsuario_UsuarioPessoaCod});
            A62ContratanteUsuario_UsuarioPessoaNom = T000D28_A62ContratanteUsuario_UsuarioPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62ContratanteUsuario_UsuarioPessoaNom", A62ContratanteUsuario_UsuarioPessoaNom);
            n62ContratanteUsuario_UsuarioPessoaNom = T000D28_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A492ContratanteUsuario_UsuarioPessoaDoc = T000D28_A492ContratanteUsuario_UsuarioPessoaDoc[0];
            n492ContratanteUsuario_UsuarioPessoaDoc = T000D28_n492ContratanteUsuario_UsuarioPessoaDoc[0];
            pr_default.close(23);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000D29 */
            pr_default.execute(24, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas do Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor T000D30 */
            pr_default.execute(25, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gpo Obj Ctrl Responsavel"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
            /* Using cursor T000D31 */
            pr_default.execute(26, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico Responsavel"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
         }
      }

      protected void EndLevel0D15( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0D15( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(19);
            pr_default.close(20);
            pr_default.close(22);
            pr_default.close(21);
            pr_default.close(23);
            context.CommitDataStores( "ContratanteUsuario");
            if ( AnyError == 0 )
            {
               ConfirmValues0D0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(19);
            pr_default.close(20);
            pr_default.close(22);
            pr_default.close(21);
            pr_default.close(23);
            context.RollbackDataStores( "ContratanteUsuario");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0D15( )
      {
         /* Scan By routine */
         /* Using cursor T000D32 */
         pr_default.execute(27);
         RcdFound15 = 0;
         if ( (pr_default.getStatus(27) != 101) )
         {
            RcdFound15 = 1;
            A63ContratanteUsuario_ContratanteCod = T000D32_A63ContratanteUsuario_ContratanteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
            A60ContratanteUsuario_UsuarioCod = T000D32_A60ContratanteUsuario_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0D15( )
      {
         /* Scan next routine */
         pr_default.readNext(27);
         RcdFound15 = 0;
         if ( (pr_default.getStatus(27) != 101) )
         {
            RcdFound15 = 1;
            A63ContratanteUsuario_ContratanteCod = T000D32_A63ContratanteUsuario_ContratanteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
            A60ContratanteUsuario_UsuarioCod = T000D32_A60ContratanteUsuario_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
         }
      }

      protected void ScanEnd0D15( )
      {
         pr_default.close(27);
      }

      protected void AfterConfirm0D15( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0D15( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0D15( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0D15( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0D15( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0D15( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0D15( )
      {
         edtUsuario_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuario_Nome_Enabled), 5, 0)));
         edtContratanteUsuario_UsuarioPessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_UsuarioPessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_UsuarioPessoaNom_Enabled), 5, 0)));
         edtContratanteUsuario_ContratanteRaz_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_ContratanteRaz_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_ContratanteRaz_Enabled), 5, 0)));
         edtContratanteUsuario_ContratanteCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_ContratanteCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_ContratanteCod_Enabled), 5, 0)));
         edtContratanteUsuario_ContratanteFan_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_ContratanteFan_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_ContratanteFan_Enabled), 5, 0)));
         edtContratanteUsuario_UsuarioCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_UsuarioCod_Enabled), 5, 0)));
         edtContratanteUsuario_UsuarioPessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratanteUsuario_UsuarioPessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratanteUsuario_UsuarioPessoaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0D0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117155448");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratanteusuario.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratanteUsuario_ContratanteCod) + "," + UrlEncode("" +AV8ContratanteUsuario_UsuarioCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1728ContratanteUsuario_EhFiscal", Z1728ContratanteUsuario_EhFiscal);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV10TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV10TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATANTEUSUARIO_EHFISCAL", A1728ContratanteUsuario_EhFiscal);
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1020ContratanteUsuario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTEPESCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A340ContratanteUsuario_ContratantePesCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOEHCONTRATANTE", A1019ContratanteUsuario_UsuarioEhContratante);
         GxWebStd.gx_hidden_field( context, "USUARIO_USERGAMGUID", StringUtil.RTrim( A341Usuario_UserGamGuid));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOPESSOADOC", A492ContratanteUsuario_UsuarioPessoaDoc);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTEUSUARIO_CONTRATANTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratanteUsuario_ContratanteCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTEUSUARIO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContratanteUsuario_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratanteUsuario";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1728ContratanteUsuario_EhFiscal);
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratanteusuario:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratanteusuario:[SendSecurityCheck value for]"+"ContratanteUsuario_EhFiscal:"+StringUtil.BoolToStr( A1728ContratanteUsuario_EhFiscal));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratanteusuario.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratanteUsuario_ContratanteCod) + "," + UrlEncode("" +AV8ContratanteUsuario_UsuarioCod) ;
      }

      public override String GetPgmname( )
      {
         return "ContratanteUsuario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contratante Usuario" ;
      }

      protected void InitializeNonKey0D15( )
      {
         A1020ContratanteUsuario_AreaTrabalhoCod = 0;
         n1020ContratanteUsuario_AreaTrabalhoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1020ContratanteUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1020ContratanteUsuario_AreaTrabalhoCod), 6, 0)));
         A340ContratanteUsuario_ContratantePesCod = 0;
         n340ContratanteUsuario_ContratantePesCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A340ContratanteUsuario_ContratantePesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A340ContratanteUsuario_ContratantePesCod), 6, 0)));
         A65ContratanteUsuario_ContratanteFan = "";
         n65ContratanteUsuario_ContratanteFan = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A65ContratanteUsuario_ContratanteFan", A65ContratanteUsuario_ContratanteFan);
         A64ContratanteUsuario_ContratanteRaz = "";
         n64ContratanteUsuario_ContratanteRaz = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A64ContratanteUsuario_ContratanteRaz", A64ContratanteUsuario_ContratanteRaz);
         A2Usuario_Nome = "";
         n2Usuario_Nome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Usuario_Nome", A2Usuario_Nome);
         A61ContratanteUsuario_UsuarioPessoaCod = 0;
         n61ContratanteUsuario_UsuarioPessoaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61ContratanteUsuario_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0)));
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         n62ContratanteUsuario_UsuarioPessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62ContratanteUsuario_UsuarioPessoaNom", A62ContratanteUsuario_UsuarioPessoaNom);
         A492ContratanteUsuario_UsuarioPessoaDoc = "";
         n492ContratanteUsuario_UsuarioPessoaDoc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A492ContratanteUsuario_UsuarioPessoaDoc", A492ContratanteUsuario_UsuarioPessoaDoc);
         A1019ContratanteUsuario_UsuarioEhContratante = false;
         n1019ContratanteUsuario_UsuarioEhContratante = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1019ContratanteUsuario_UsuarioEhContratante", A1019ContratanteUsuario_UsuarioEhContratante);
         A341Usuario_UserGamGuid = "";
         n341Usuario_UserGamGuid = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
         A1728ContratanteUsuario_EhFiscal = false;
         n1728ContratanteUsuario_EhFiscal = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1728ContratanteUsuario_EhFiscal", A1728ContratanteUsuario_EhFiscal);
         Z1728ContratanteUsuario_EhFiscal = false;
      }

      protected void InitAll0D15( )
      {
         A63ContratanteUsuario_ContratanteCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0)));
         A60ContratanteUsuario_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)));
         InitializeNonKey0D15( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117155473");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratanteusuario.js", "?20203117155473");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockusuario_nome_Internalname = "TEXTBLOCKUSUARIO_NOME";
         edtUsuario_Nome_Internalname = "USUARIO_NOME";
         lblTextblockcontratanteusuario_usuariopessoanom_Internalname = "TEXTBLOCKCONTRATANTEUSUARIO_USUARIOPESSOANOM";
         edtContratanteUsuario_UsuarioPessoaNom_Internalname = "CONTRATANTEUSUARIO_USUARIOPESSOANOM";
         lblTextblockcontratanteusuario_contratanteraz_Internalname = "TEXTBLOCKCONTRATANTEUSUARIO_CONTRATANTERAZ";
         edtContratanteUsuario_ContratanteRaz_Internalname = "CONTRATANTEUSUARIO_CONTRATANTERAZ";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratanteUsuario_ContratanteCod_Internalname = "CONTRATANTEUSUARIO_CONTRATANTECOD";
         edtContratanteUsuario_ContratanteFan_Internalname = "CONTRATANTEUSUARIO_CONTRATANTEFAN";
         edtContratanteUsuario_UsuarioCod_Internalname = "CONTRATANTEUSUARIO_USUARIOCOD";
         edtContratanteUsuario_UsuarioPessoaCod_Internalname = "CONTRATANTEUSUARIO_USUARIOPESSOACOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Usu�rio do Contratante";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contratante Usuario";
         edtContratanteUsuario_ContratanteRaz_Jsonclick = "";
         edtContratanteUsuario_ContratanteRaz_Enabled = 0;
         edtContratanteUsuario_UsuarioPessoaNom_Jsonclick = "";
         edtContratanteUsuario_UsuarioPessoaNom_Enabled = 0;
         edtUsuario_Nome_Jsonclick = "";
         edtUsuario_Nome_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContratanteUsuario_UsuarioPessoaCod_Jsonclick = "";
         edtContratanteUsuario_UsuarioPessoaCod_Enabled = 0;
         edtContratanteUsuario_UsuarioPessoaCod_Visible = 1;
         edtContratanteUsuario_UsuarioCod_Jsonclick = "";
         edtContratanteUsuario_UsuarioCod_Enabled = 1;
         edtContratanteUsuario_UsuarioCod_Visible = 1;
         edtContratanteUsuario_ContratanteFan_Jsonclick = "";
         edtContratanteUsuario_ContratanteFan_Enabled = 0;
         edtContratanteUsuario_ContratanteFan_Visible = 1;
         edtContratanteUsuario_ContratanteCod_Jsonclick = "";
         edtContratanteUsuario_ContratanteCod_Enabled = 1;
         edtContratanteUsuario_ContratanteCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Contratanteusuario_contratantecod( int GX_Parm1 ,
                                                           int GX_Parm2 ,
                                                           int GX_Parm3 ,
                                                           String GX_Parm4 ,
                                                           String GX_Parm5 )
      {
         A63ContratanteUsuario_ContratanteCod = GX_Parm1;
         A340ContratanteUsuario_ContratantePesCod = GX_Parm2;
         n340ContratanteUsuario_ContratantePesCod = false;
         A1020ContratanteUsuario_AreaTrabalhoCod = GX_Parm3;
         n1020ContratanteUsuario_AreaTrabalhoCod = false;
         A65ContratanteUsuario_ContratanteFan = GX_Parm4;
         n65ContratanteUsuario_ContratanteFan = false;
         A64ContratanteUsuario_ContratanteRaz = GX_Parm5;
         n64ContratanteUsuario_ContratanteRaz = false;
         /* Using cursor T000D24 */
         pr_default.execute(19, new Object[] {A63ContratanteUsuario_ContratanteCod});
         if ( (pr_default.getStatus(19) != 101) )
         {
            A1020ContratanteUsuario_AreaTrabalhoCod = T000D24_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = T000D24_n1020ContratanteUsuario_AreaTrabalhoCod[0];
         }
         else
         {
            A1020ContratanteUsuario_AreaTrabalhoCod = 0;
            n1020ContratanteUsuario_AreaTrabalhoCod = false;
         }
         pr_default.close(19);
         /* Using cursor T000D25 */
         pr_default.execute(20, new Object[] {A63ContratanteUsuario_ContratanteCod});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratante Usuario_Contratante'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
            AnyError = 1;
            GX_FocusControl = edtContratanteUsuario_ContratanteCod_Internalname;
         }
         A65ContratanteUsuario_ContratanteFan = T000D25_A65ContratanteUsuario_ContratanteFan[0];
         n65ContratanteUsuario_ContratanteFan = T000D25_n65ContratanteUsuario_ContratanteFan[0];
         A340ContratanteUsuario_ContratantePesCod = T000D25_A340ContratanteUsuario_ContratantePesCod[0];
         n340ContratanteUsuario_ContratantePesCod = T000D25_n340ContratanteUsuario_ContratantePesCod[0];
         pr_default.close(20);
         /* Using cursor T000D26 */
         pr_default.execute(21, new Object[] {n340ContratanteUsuario_ContratantePesCod, A340ContratanteUsuario_ContratantePesCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A64ContratanteUsuario_ContratanteRaz = T000D26_A64ContratanteUsuario_ContratanteRaz[0];
         n64ContratanteUsuario_ContratanteRaz = T000D26_n64ContratanteUsuario_ContratanteRaz[0];
         pr_default.close(21);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1020ContratanteUsuario_AreaTrabalhoCod = 0;
            n1020ContratanteUsuario_AreaTrabalhoCod = false;
            A65ContratanteUsuario_ContratanteFan = "";
            n65ContratanteUsuario_ContratanteFan = false;
            A340ContratanteUsuario_ContratantePesCod = 0;
            n340ContratanteUsuario_ContratantePesCod = false;
            A64ContratanteUsuario_ContratanteRaz = "";
            n64ContratanteUsuario_ContratanteRaz = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1020ContratanteUsuario_AreaTrabalhoCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A65ContratanteUsuario_ContratanteFan));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A340ContratanteUsuario_ContratantePesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A64ContratanteUsuario_ContratanteRaz));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratanteusuario_usuariocod( int GX_Parm1 ,
                                                       int GX_Parm2 ,
                                                       String GX_Parm3 ,
                                                       bool GX_Parm4 ,
                                                       String GX_Parm5 ,
                                                       String GX_Parm6 ,
                                                       String GX_Parm7 )
      {
         A60ContratanteUsuario_UsuarioCod = GX_Parm1;
         A61ContratanteUsuario_UsuarioPessoaCod = GX_Parm2;
         n61ContratanteUsuario_UsuarioPessoaCod = false;
         A2Usuario_Nome = GX_Parm3;
         n2Usuario_Nome = false;
         A1019ContratanteUsuario_UsuarioEhContratante = GX_Parm4;
         n1019ContratanteUsuario_UsuarioEhContratante = false;
         A341Usuario_UserGamGuid = GX_Parm5;
         n341Usuario_UserGamGuid = false;
         A62ContratanteUsuario_UsuarioPessoaNom = GX_Parm6;
         n62ContratanteUsuario_UsuarioPessoaNom = false;
         A492ContratanteUsuario_UsuarioPessoaDoc = GX_Parm7;
         n492ContratanteUsuario_UsuarioPessoaDoc = false;
         /* Using cursor T000D27 */
         pr_default.execute(22, new Object[] {A60ContratanteUsuario_UsuarioCod});
         if ( (pr_default.getStatus(22) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratante Usuario_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratanteUsuario_UsuarioCod_Internalname;
         }
         A2Usuario_Nome = T000D27_A2Usuario_Nome[0];
         n2Usuario_Nome = T000D27_n2Usuario_Nome[0];
         A1019ContratanteUsuario_UsuarioEhContratante = T000D27_A1019ContratanteUsuario_UsuarioEhContratante[0];
         n1019ContratanteUsuario_UsuarioEhContratante = T000D27_n1019ContratanteUsuario_UsuarioEhContratante[0];
         A341Usuario_UserGamGuid = T000D27_A341Usuario_UserGamGuid[0];
         n341Usuario_UserGamGuid = T000D27_n341Usuario_UserGamGuid[0];
         A61ContratanteUsuario_UsuarioPessoaCod = T000D27_A61ContratanteUsuario_UsuarioPessoaCod[0];
         n61ContratanteUsuario_UsuarioPessoaCod = T000D27_n61ContratanteUsuario_UsuarioPessoaCod[0];
         pr_default.close(22);
         /* Using cursor T000D28 */
         pr_default.execute(23, new Object[] {n61ContratanteUsuario_UsuarioPessoaCod, A61ContratanteUsuario_UsuarioPessoaCod});
         if ( (pr_default.getStatus(23) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A62ContratanteUsuario_UsuarioPessoaNom = T000D28_A62ContratanteUsuario_UsuarioPessoaNom[0];
         n62ContratanteUsuario_UsuarioPessoaNom = T000D28_n62ContratanteUsuario_UsuarioPessoaNom[0];
         A492ContratanteUsuario_UsuarioPessoaDoc = T000D28_A492ContratanteUsuario_UsuarioPessoaDoc[0];
         n492ContratanteUsuario_UsuarioPessoaDoc = T000D28_n492ContratanteUsuario_UsuarioPessoaDoc[0];
         pr_default.close(23);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A2Usuario_Nome = "";
            n2Usuario_Nome = false;
            A1019ContratanteUsuario_UsuarioEhContratante = false;
            n1019ContratanteUsuario_UsuarioEhContratante = false;
            A341Usuario_UserGamGuid = "";
            n341Usuario_UserGamGuid = false;
            A61ContratanteUsuario_UsuarioPessoaCod = 0;
            n61ContratanteUsuario_UsuarioPessoaCod = false;
            A62ContratanteUsuario_UsuarioPessoaNom = "";
            n62ContratanteUsuario_UsuarioPessoaNom = false;
            A492ContratanteUsuario_UsuarioPessoaDoc = "";
            n492ContratanteUsuario_UsuarioPessoaDoc = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A2Usuario_Nome));
         isValidOutput.Add(A1019ContratanteUsuario_UsuarioEhContratante);
         isValidOutput.Add(StringUtil.RTrim( A341Usuario_UserGamGuid));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A61ContratanteUsuario_UsuarioPessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A62ContratanteUsuario_UsuarioPessoaNom));
         isValidOutput.Add(A492ContratanteUsuario_UsuarioPessoaDoc);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratanteUsuario_ContratanteCod',fld:'vCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8ContratanteUsuario_UsuarioCod',fld:'vCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120D2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV8ContratanteUsuario_UsuarioCod',fld:'vCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV10TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'AV8ContratanteUsuario_UsuarioCod',fld:'vCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(20);
         pr_default.close(22);
         pr_default.close(21);
         pr_default.close(23);
         pr_default.close(19);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         A65ContratanteUsuario_ContratanteFan = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockusuario_nome_Jsonclick = "";
         A2Usuario_Nome = "";
         lblTextblockcontratanteusuario_usuariopessoanom_Jsonclick = "";
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         lblTextblockcontratanteusuario_contratanteraz_Jsonclick = "";
         A64ContratanteUsuario_ContratanteRaz = "";
         A341Usuario_UserGamGuid = "";
         A492ContratanteUsuario_UsuarioPessoaDoc = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode15 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         Z65ContratanteUsuario_ContratanteFan = "";
         Z64ContratanteUsuario_ContratanteRaz = "";
         Z2Usuario_Nome = "";
         Z341Usuario_UserGamGuid = "";
         Z62ContratanteUsuario_UsuarioPessoaNom = "";
         Z492ContratanteUsuario_UsuarioPessoaDoc = "";
         T000D5_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         T000D5_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         T000D6_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         T000D6_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         T000D6_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         T000D6_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         T000D8_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         T000D8_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         T000D7_A2Usuario_Nome = new String[] {""} ;
         T000D7_n2Usuario_Nome = new bool[] {false} ;
         T000D7_A1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         T000D7_n1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         T000D7_A341Usuario_UserGamGuid = new String[] {""} ;
         T000D7_n341Usuario_UserGamGuid = new bool[] {false} ;
         T000D7_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         T000D7_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T000D9_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         T000D9_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T000D9_A492ContratanteUsuario_UsuarioPessoaDoc = new String[] {""} ;
         T000D9_n492ContratanteUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         T000D10_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         T000D10_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         T000D10_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         T000D10_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         T000D10_A2Usuario_Nome = new String[] {""} ;
         T000D10_n2Usuario_Nome = new bool[] {false} ;
         T000D10_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         T000D10_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T000D10_A492ContratanteUsuario_UsuarioPessoaDoc = new String[] {""} ;
         T000D10_n492ContratanteUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         T000D10_A1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         T000D10_n1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         T000D10_A341Usuario_UserGamGuid = new String[] {""} ;
         T000D10_n341Usuario_UserGamGuid = new bool[] {false} ;
         T000D10_A1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         T000D10_n1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         T000D10_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T000D10_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         T000D10_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         T000D10_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         T000D10_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         T000D10_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T000D12_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         T000D12_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         T000D13_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         T000D13_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         T000D13_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         T000D13_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         T000D14_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         T000D14_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         T000D15_A2Usuario_Nome = new String[] {""} ;
         T000D15_n2Usuario_Nome = new bool[] {false} ;
         T000D15_A1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         T000D15_n1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         T000D15_A341Usuario_UserGamGuid = new String[] {""} ;
         T000D15_n341Usuario_UserGamGuid = new bool[] {false} ;
         T000D15_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         T000D15_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T000D16_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         T000D16_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T000D16_A492ContratanteUsuario_UsuarioPessoaDoc = new String[] {""} ;
         T000D16_n492ContratanteUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         T000D17_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T000D17_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         T000D3_A1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         T000D3_n1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         T000D3_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T000D3_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         T000D18_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T000D18_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         T000D19_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T000D19_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         T000D2_A1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         T000D2_n1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         T000D2_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T000D2_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         T000D24_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         T000D24_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         T000D25_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         T000D25_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         T000D25_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         T000D25_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         T000D26_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         T000D26_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         T000D27_A2Usuario_Nome = new String[] {""} ;
         T000D27_n2Usuario_Nome = new bool[] {false} ;
         T000D27_A1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         T000D27_n1019ContratanteUsuario_UsuarioEhContratante = new bool[] {false} ;
         T000D27_A341Usuario_UserGamGuid = new String[] {""} ;
         T000D27_n341Usuario_UserGamGuid = new bool[] {false} ;
         T000D27_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         T000D27_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T000D28_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         T000D28_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T000D28_A492ContratanteUsuario_UsuarioPessoaDoc = new String[] {""} ;
         T000D28_n492ContratanteUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         T000D29_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T000D29_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         T000D29_A127Sistema_Codigo = new int[1] ;
         T000D30_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         T000D31_A1548ServicoResponsavel_Codigo = new int[1] ;
         T000D32_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T000D32_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratanteusuario__default(),
            new Object[][] {
                new Object[] {
               T000D2_A1728ContratanteUsuario_EhFiscal, T000D2_n1728ContratanteUsuario_EhFiscal, T000D2_A63ContratanteUsuario_ContratanteCod, T000D2_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               T000D3_A1728ContratanteUsuario_EhFiscal, T000D3_n1728ContratanteUsuario_EhFiscal, T000D3_A63ContratanteUsuario_ContratanteCod, T000D3_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               T000D5_A1020ContratanteUsuario_AreaTrabalhoCod, T000D5_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               T000D6_A65ContratanteUsuario_ContratanteFan, T000D6_n65ContratanteUsuario_ContratanteFan, T000D6_A340ContratanteUsuario_ContratantePesCod, T000D6_n340ContratanteUsuario_ContratantePesCod
               }
               , new Object[] {
               T000D7_A2Usuario_Nome, T000D7_n2Usuario_Nome, T000D7_A1019ContratanteUsuario_UsuarioEhContratante, T000D7_n1019ContratanteUsuario_UsuarioEhContratante, T000D7_A341Usuario_UserGamGuid, T000D7_n341Usuario_UserGamGuid, T000D7_A61ContratanteUsuario_UsuarioPessoaCod, T000D7_n61ContratanteUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               T000D8_A64ContratanteUsuario_ContratanteRaz, T000D8_n64ContratanteUsuario_ContratanteRaz
               }
               , new Object[] {
               T000D9_A62ContratanteUsuario_UsuarioPessoaNom, T000D9_n62ContratanteUsuario_UsuarioPessoaNom, T000D9_A492ContratanteUsuario_UsuarioPessoaDoc, T000D9_n492ContratanteUsuario_UsuarioPessoaDoc
               }
               , new Object[] {
               T000D10_A65ContratanteUsuario_ContratanteFan, T000D10_n65ContratanteUsuario_ContratanteFan, T000D10_A64ContratanteUsuario_ContratanteRaz, T000D10_n64ContratanteUsuario_ContratanteRaz, T000D10_A2Usuario_Nome, T000D10_n2Usuario_Nome, T000D10_A62ContratanteUsuario_UsuarioPessoaNom, T000D10_n62ContratanteUsuario_UsuarioPessoaNom, T000D10_A492ContratanteUsuario_UsuarioPessoaDoc, T000D10_n492ContratanteUsuario_UsuarioPessoaDoc,
               T000D10_A1019ContratanteUsuario_UsuarioEhContratante, T000D10_n1019ContratanteUsuario_UsuarioEhContratante, T000D10_A341Usuario_UserGamGuid, T000D10_n341Usuario_UserGamGuid, T000D10_A1728ContratanteUsuario_EhFiscal, T000D10_n1728ContratanteUsuario_EhFiscal, T000D10_A63ContratanteUsuario_ContratanteCod, T000D10_A60ContratanteUsuario_UsuarioCod, T000D10_A340ContratanteUsuario_ContratantePesCod, T000D10_n340ContratanteUsuario_ContratantePesCod,
               T000D10_A61ContratanteUsuario_UsuarioPessoaCod, T000D10_n61ContratanteUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               T000D12_A1020ContratanteUsuario_AreaTrabalhoCod, T000D12_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               T000D13_A65ContratanteUsuario_ContratanteFan, T000D13_n65ContratanteUsuario_ContratanteFan, T000D13_A340ContratanteUsuario_ContratantePesCod, T000D13_n340ContratanteUsuario_ContratantePesCod
               }
               , new Object[] {
               T000D14_A64ContratanteUsuario_ContratanteRaz, T000D14_n64ContratanteUsuario_ContratanteRaz
               }
               , new Object[] {
               T000D15_A2Usuario_Nome, T000D15_n2Usuario_Nome, T000D15_A1019ContratanteUsuario_UsuarioEhContratante, T000D15_n1019ContratanteUsuario_UsuarioEhContratante, T000D15_A341Usuario_UserGamGuid, T000D15_n341Usuario_UserGamGuid, T000D15_A61ContratanteUsuario_UsuarioPessoaCod, T000D15_n61ContratanteUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               T000D16_A62ContratanteUsuario_UsuarioPessoaNom, T000D16_n62ContratanteUsuario_UsuarioPessoaNom, T000D16_A492ContratanteUsuario_UsuarioPessoaDoc, T000D16_n492ContratanteUsuario_UsuarioPessoaDoc
               }
               , new Object[] {
               T000D17_A63ContratanteUsuario_ContratanteCod, T000D17_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               T000D18_A63ContratanteUsuario_ContratanteCod, T000D18_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               T000D19_A63ContratanteUsuario_ContratanteCod, T000D19_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000D24_A1020ContratanteUsuario_AreaTrabalhoCod, T000D24_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               T000D25_A65ContratanteUsuario_ContratanteFan, T000D25_n65ContratanteUsuario_ContratanteFan, T000D25_A340ContratanteUsuario_ContratantePesCod, T000D25_n340ContratanteUsuario_ContratantePesCod
               }
               , new Object[] {
               T000D26_A64ContratanteUsuario_ContratanteRaz, T000D26_n64ContratanteUsuario_ContratanteRaz
               }
               , new Object[] {
               T000D27_A2Usuario_Nome, T000D27_n2Usuario_Nome, T000D27_A1019ContratanteUsuario_UsuarioEhContratante, T000D27_n1019ContratanteUsuario_UsuarioEhContratante, T000D27_A341Usuario_UserGamGuid, T000D27_n341Usuario_UserGamGuid, T000D27_A61ContratanteUsuario_UsuarioPessoaCod, T000D27_n61ContratanteUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               T000D28_A62ContratanteUsuario_UsuarioPessoaNom, T000D28_n62ContratanteUsuario_UsuarioPessoaNom, T000D28_A492ContratanteUsuario_UsuarioPessoaDoc, T000D28_n492ContratanteUsuario_UsuarioPessoaDoc
               }
               , new Object[] {
               T000D29_A63ContratanteUsuario_ContratanteCod, T000D29_A60ContratanteUsuario_UsuarioCod, T000D29_A127Sistema_Codigo
               }
               , new Object[] {
               T000D30_A1837GpoObjCtrlResponsavel_Codigo
               }
               , new Object[] {
               T000D31_A1548ServicoResponsavel_Codigo
               }
               , new Object[] {
               T000D32_A63ContratanteUsuario_ContratanteCod, T000D32_A60ContratanteUsuario_UsuarioCod
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound15 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ContratanteUsuario_ContratanteCod ;
      private int wcpOAV8ContratanteUsuario_UsuarioCod ;
      private int Z63ContratanteUsuario_ContratanteCod ;
      private int Z60ContratanteUsuario_UsuarioCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A340ContratanteUsuario_ContratantePesCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int AV7ContratanteUsuario_ContratanteCod ;
      private int AV8ContratanteUsuario_UsuarioCod ;
      private int trnEnded ;
      private int edtContratanteUsuario_ContratanteCod_Visible ;
      private int edtContratanteUsuario_ContratanteCod_Enabled ;
      private int edtContratanteUsuario_ContratanteFan_Visible ;
      private int edtContratanteUsuario_ContratanteFan_Enabled ;
      private int edtContratanteUsuario_UsuarioCod_Visible ;
      private int edtContratanteUsuario_UsuarioCod_Enabled ;
      private int edtContratanteUsuario_UsuarioPessoaCod_Enabled ;
      private int edtContratanteUsuario_UsuarioPessoaCod_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtUsuario_Nome_Enabled ;
      private int edtContratanteUsuario_UsuarioPessoaNom_Enabled ;
      private int edtContratanteUsuario_ContratanteRaz_Enabled ;
      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private int AV12AreaTrabalho_Codigo ;
      private int Z340ContratanteUsuario_ContratantePesCod ;
      private int Z61ContratanteUsuario_UsuarioPessoaCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratanteUsuario_ContratanteCod_Internalname ;
      private String TempTags ;
      private String edtContratanteUsuario_ContratanteCod_Jsonclick ;
      private String edtContratanteUsuario_ContratanteFan_Internalname ;
      private String A65ContratanteUsuario_ContratanteFan ;
      private String edtContratanteUsuario_ContratanteFan_Jsonclick ;
      private String edtContratanteUsuario_UsuarioCod_Internalname ;
      private String edtContratanteUsuario_UsuarioCod_Jsonclick ;
      private String edtContratanteUsuario_UsuarioPessoaCod_Internalname ;
      private String edtContratanteUsuario_UsuarioPessoaCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockusuario_nome_Internalname ;
      private String lblTextblockusuario_nome_Jsonclick ;
      private String edtUsuario_Nome_Internalname ;
      private String A2Usuario_Nome ;
      private String edtUsuario_Nome_Jsonclick ;
      private String lblTextblockcontratanteusuario_usuariopessoanom_Internalname ;
      private String lblTextblockcontratanteusuario_usuariopessoanom_Jsonclick ;
      private String edtContratanteUsuario_UsuarioPessoaNom_Internalname ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String edtContratanteUsuario_UsuarioPessoaNom_Jsonclick ;
      private String lblTextblockcontratanteusuario_contratanteraz_Internalname ;
      private String lblTextblockcontratanteusuario_contratanteraz_Jsonclick ;
      private String edtContratanteUsuario_ContratanteRaz_Internalname ;
      private String A64ContratanteUsuario_ContratanteRaz ;
      private String edtContratanteUsuario_ContratanteRaz_Jsonclick ;
      private String A341Usuario_UserGamGuid ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode15 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z65ContratanteUsuario_ContratanteFan ;
      private String Z64ContratanteUsuario_ContratanteRaz ;
      private String Z2Usuario_Nome ;
      private String Z341Usuario_UserGamGuid ;
      private String Z62ContratanteUsuario_UsuarioPessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z1728ContratanteUsuario_EhFiscal ;
      private bool entryPointCalled ;
      private bool n340ContratanteUsuario_ContratantePesCod ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n2Usuario_Nome ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool n64ContratanteUsuario_ContratanteRaz ;
      private bool n65ContratanteUsuario_ContratanteFan ;
      private bool n1728ContratanteUsuario_EhFiscal ;
      private bool A1728ContratanteUsuario_EhFiscal ;
      private bool n1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool A1019ContratanteUsuario_UsuarioEhContratante ;
      private bool n1019ContratanteUsuario_UsuarioEhContratante ;
      private bool n341Usuario_UserGamGuid ;
      private bool n492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Z1019ContratanteUsuario_UsuarioEhContratante ;
      private String A492ContratanteUsuario_UsuarioPessoaDoc ;
      private String Z492ContratanteUsuario_UsuarioPessoaDoc ;
      private IGxSession AV11WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T000D5_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] T000D5_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private String[] T000D6_A65ContratanteUsuario_ContratanteFan ;
      private bool[] T000D6_n65ContratanteUsuario_ContratanteFan ;
      private int[] T000D6_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] T000D6_n340ContratanteUsuario_ContratantePesCod ;
      private String[] T000D8_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] T000D8_n64ContratanteUsuario_ContratanteRaz ;
      private String[] T000D7_A2Usuario_Nome ;
      private bool[] T000D7_n2Usuario_Nome ;
      private bool[] T000D7_A1019ContratanteUsuario_UsuarioEhContratante ;
      private bool[] T000D7_n1019ContratanteUsuario_UsuarioEhContratante ;
      private String[] T000D7_A341Usuario_UserGamGuid ;
      private bool[] T000D7_n341Usuario_UserGamGuid ;
      private int[] T000D7_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] T000D7_n61ContratanteUsuario_UsuarioPessoaCod ;
      private String[] T000D9_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] T000D9_n62ContratanteUsuario_UsuarioPessoaNom ;
      private String[] T000D9_A492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool[] T000D9_n492ContratanteUsuario_UsuarioPessoaDoc ;
      private String[] T000D10_A65ContratanteUsuario_ContratanteFan ;
      private bool[] T000D10_n65ContratanteUsuario_ContratanteFan ;
      private String[] T000D10_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] T000D10_n64ContratanteUsuario_ContratanteRaz ;
      private String[] T000D10_A2Usuario_Nome ;
      private bool[] T000D10_n2Usuario_Nome ;
      private String[] T000D10_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] T000D10_n62ContratanteUsuario_UsuarioPessoaNom ;
      private String[] T000D10_A492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool[] T000D10_n492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool[] T000D10_A1019ContratanteUsuario_UsuarioEhContratante ;
      private bool[] T000D10_n1019ContratanteUsuario_UsuarioEhContratante ;
      private String[] T000D10_A341Usuario_UserGamGuid ;
      private bool[] T000D10_n341Usuario_UserGamGuid ;
      private bool[] T000D10_A1728ContratanteUsuario_EhFiscal ;
      private bool[] T000D10_n1728ContratanteUsuario_EhFiscal ;
      private int[] T000D10_A63ContratanteUsuario_ContratanteCod ;
      private int[] T000D10_A60ContratanteUsuario_UsuarioCod ;
      private int[] T000D10_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] T000D10_n340ContratanteUsuario_ContratantePesCod ;
      private int[] T000D10_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] T000D10_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] T000D12_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] T000D12_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private String[] T000D13_A65ContratanteUsuario_ContratanteFan ;
      private bool[] T000D13_n65ContratanteUsuario_ContratanteFan ;
      private int[] T000D13_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] T000D13_n340ContratanteUsuario_ContratantePesCod ;
      private String[] T000D14_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] T000D14_n64ContratanteUsuario_ContratanteRaz ;
      private String[] T000D15_A2Usuario_Nome ;
      private bool[] T000D15_n2Usuario_Nome ;
      private bool[] T000D15_A1019ContratanteUsuario_UsuarioEhContratante ;
      private bool[] T000D15_n1019ContratanteUsuario_UsuarioEhContratante ;
      private String[] T000D15_A341Usuario_UserGamGuid ;
      private bool[] T000D15_n341Usuario_UserGamGuid ;
      private int[] T000D15_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] T000D15_n61ContratanteUsuario_UsuarioPessoaCod ;
      private String[] T000D16_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] T000D16_n62ContratanteUsuario_UsuarioPessoaNom ;
      private String[] T000D16_A492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool[] T000D16_n492ContratanteUsuario_UsuarioPessoaDoc ;
      private int[] T000D17_A63ContratanteUsuario_ContratanteCod ;
      private int[] T000D17_A60ContratanteUsuario_UsuarioCod ;
      private bool[] T000D3_A1728ContratanteUsuario_EhFiscal ;
      private bool[] T000D3_n1728ContratanteUsuario_EhFiscal ;
      private int[] T000D3_A63ContratanteUsuario_ContratanteCod ;
      private int[] T000D3_A60ContratanteUsuario_UsuarioCod ;
      private int[] T000D18_A63ContratanteUsuario_ContratanteCod ;
      private int[] T000D18_A60ContratanteUsuario_UsuarioCod ;
      private int[] T000D19_A63ContratanteUsuario_ContratanteCod ;
      private int[] T000D19_A60ContratanteUsuario_UsuarioCod ;
      private bool[] T000D2_A1728ContratanteUsuario_EhFiscal ;
      private bool[] T000D2_n1728ContratanteUsuario_EhFiscal ;
      private int[] T000D2_A63ContratanteUsuario_ContratanteCod ;
      private int[] T000D2_A60ContratanteUsuario_UsuarioCod ;
      private int[] T000D24_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] T000D24_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private String[] T000D25_A65ContratanteUsuario_ContratanteFan ;
      private bool[] T000D25_n65ContratanteUsuario_ContratanteFan ;
      private int[] T000D25_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] T000D25_n340ContratanteUsuario_ContratantePesCod ;
      private String[] T000D26_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] T000D26_n64ContratanteUsuario_ContratanteRaz ;
      private String[] T000D27_A2Usuario_Nome ;
      private bool[] T000D27_n2Usuario_Nome ;
      private bool[] T000D27_A1019ContratanteUsuario_UsuarioEhContratante ;
      private bool[] T000D27_n1019ContratanteUsuario_UsuarioEhContratante ;
      private String[] T000D27_A341Usuario_UserGamGuid ;
      private bool[] T000D27_n341Usuario_UserGamGuid ;
      private int[] T000D27_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] T000D27_n61ContratanteUsuario_UsuarioPessoaCod ;
      private String[] T000D28_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] T000D28_n62ContratanteUsuario_UsuarioPessoaNom ;
      private String[] T000D28_A492ContratanteUsuario_UsuarioPessoaDoc ;
      private bool[] T000D28_n492ContratanteUsuario_UsuarioPessoaDoc ;
      private int[] T000D29_A63ContratanteUsuario_ContratanteCod ;
      private int[] T000D29_A60ContratanteUsuario_UsuarioCod ;
      private int[] T000D29_A127Sistema_Codigo ;
      private int[] T000D30_A1837GpoObjCtrlResponsavel_Codigo ;
      private int[] T000D31_A1548ServicoResponsavel_Codigo ;
      private int[] T000D32_A63ContratanteUsuario_ContratanteCod ;
      private int[] T000D32_A60ContratanteUsuario_UsuarioCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
   }

   public class contratanteusuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000D10 ;
          prmT000D10 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D5 ;
          prmT000D5 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D6 ;
          prmT000D6 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D8 ;
          prmT000D8 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratantePesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D7 ;
          prmT000D7 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D9 ;
          prmT000D9 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D12 ;
          prmT000D12 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D13 ;
          prmT000D13 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D14 ;
          prmT000D14 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratantePesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D15 ;
          prmT000D15 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D16 ;
          prmT000D16 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D17 ;
          prmT000D17 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D3 ;
          prmT000D3 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D18 ;
          prmT000D18 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D19 ;
          prmT000D19 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D2 ;
          prmT000D2 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D20 ;
          prmT000D20 = new Object[] {
          new Object[] {"@ContratanteUsuario_EhFiscal",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D21 ;
          prmT000D21 = new Object[] {
          new Object[] {"@ContratanteUsuario_EhFiscal",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D22 ;
          prmT000D22 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D29 ;
          prmT000D29 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D30 ;
          prmT000D30 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D31 ;
          prmT000D31 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D32 ;
          prmT000D32 = new Object[] {
          } ;
          Object[] prmT000D24 ;
          prmT000D24 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D25 ;
          prmT000D25 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D26 ;
          prmT000D26 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratantePesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D27 ;
          prmT000D27 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000D28 ;
          prmT000D28 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000D2", "SELECT [ContratanteUsuario_EhFiscal], [ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [ContratanteUsuario] WITH (UPDLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D2,1,0,true,false )
             ,new CursorDef("T000D3", "SELECT [ContratanteUsuario_EhFiscal], [ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D3,1,0,true,false )
             ,new CursorDef("T000D5", "SELECT COALESCE( T1.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ContratanteUsuario_ContratanteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D5,1,0,true,false )
             ,new CursorDef("T000D6", "SELECT [Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, [Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ContratanteUsuario_ContratanteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D6,1,0,true,false )
             ,new CursorDef("T000D7", "SELECT [Usuario_Nome], [Usuario_EhContratante] AS ContratanteUsuario_UsuarioEhContratante, [Usuario_UserGamGuid], [Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D7,1,0,true,false )
             ,new CursorDef("T000D8", "SELECT [Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_ContratantePesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D8,1,0,true,false )
             ,new CursorDef("T000D9", "SELECT [Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, [Pessoa_Docto] AS ContratanteUsuario_UsuarioPessoaDoc FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_UsuarioPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D9,1,0,true,false )
             ,new CursorDef("T000D10", "SELECT T2.[Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, T3.[Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz, T4.[Usuario_Nome], T5.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, T5.[Pessoa_Docto] AS ContratanteUsuario_UsuarioPessoaDoc, T4.[Usuario_EhContratante] AS ContratanteUsuario_UsuarioEhContratante, T4.[Usuario_UserGamGuid], TM1.[ContratanteUsuario_EhFiscal], TM1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, TM1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T2.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T4.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod FROM (((([ContratanteUsuario] TM1 WITH (NOLOCK) INNER JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = TM1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = TM1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE TM1.[ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod and TM1.[ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod ORDER BY TM1.[ContratanteUsuario_ContratanteCod], TM1.[ContratanteUsuario_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000D10,100,0,true,false )
             ,new CursorDef("T000D12", "SELECT COALESCE( T1.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ContratanteUsuario_ContratanteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D12,1,0,true,false )
             ,new CursorDef("T000D13", "SELECT [Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, [Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ContratanteUsuario_ContratanteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D13,1,0,true,false )
             ,new CursorDef("T000D14", "SELECT [Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_ContratantePesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D14,1,0,true,false )
             ,new CursorDef("T000D15", "SELECT [Usuario_Nome], [Usuario_EhContratante] AS ContratanteUsuario_UsuarioEhContratante, [Usuario_UserGamGuid], [Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D15,1,0,true,false )
             ,new CursorDef("T000D16", "SELECT [Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, [Pessoa_Docto] AS ContratanteUsuario_UsuarioPessoaDoc FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_UsuarioPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D16,1,0,true,false )
             ,new CursorDef("T000D17", "SELECT [ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000D17,1,0,true,false )
             ,new CursorDef("T000D18", "SELECT TOP 1 [ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE ( [ContratanteUsuario_ContratanteCod] > @ContratanteUsuario_ContratanteCod or [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod and [ContratanteUsuario_UsuarioCod] > @ContratanteUsuario_UsuarioCod) ORDER BY [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000D18,1,0,true,true )
             ,new CursorDef("T000D19", "SELECT TOP 1 [ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE ( [ContratanteUsuario_ContratanteCod] < @ContratanteUsuario_ContratanteCod or [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod and [ContratanteUsuario_UsuarioCod] < @ContratanteUsuario_UsuarioCod) ORDER BY [ContratanteUsuario_ContratanteCod] DESC, [ContratanteUsuario_UsuarioCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000D19,1,0,true,true )
             ,new CursorDef("T000D20", "INSERT INTO [ContratanteUsuario]([ContratanteUsuario_EhFiscal], [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod]) VALUES(@ContratanteUsuario_EhFiscal, @ContratanteUsuario_ContratanteCod, @ContratanteUsuario_UsuarioCod)", GxErrorMask.GX_NOMASK,prmT000D20)
             ,new CursorDef("T000D21", "UPDATE [ContratanteUsuario] SET [ContratanteUsuario_EhFiscal]=@ContratanteUsuario_EhFiscal  WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod", GxErrorMask.GX_NOMASK,prmT000D21)
             ,new CursorDef("T000D22", "DELETE FROM [ContratanteUsuario]  WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod", GxErrorMask.GX_NOMASK,prmT000D22)
             ,new CursorDef("T000D24", "SELECT COALESCE( T1.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ContratanteUsuario_ContratanteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D24,1,0,true,false )
             ,new CursorDef("T000D25", "SELECT [Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, [Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ContratanteUsuario_ContratanteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D25,1,0,true,false )
             ,new CursorDef("T000D26", "SELECT [Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_ContratantePesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D26,1,0,true,false )
             ,new CursorDef("T000D27", "SELECT [Usuario_Nome], [Usuario_EhContratante] AS ContratanteUsuario_UsuarioEhContratante, [Usuario_UserGamGuid], [Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D27,1,0,true,false )
             ,new CursorDef("T000D28", "SELECT [Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, [Pessoa_Docto] AS ContratanteUsuario_UsuarioPessoaDoc FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratanteUsuario_UsuarioPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D28,1,0,true,false )
             ,new CursorDef("T000D29", "SELECT TOP 1 [ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, [Sistema_Codigo] FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D29,1,0,true,true )
             ,new CursorDef("T000D30", "SELECT TOP 1 [GpoObjCtrlResponsavel_Codigo] FROM [GpoObjCtrlResponsavel] WITH (NOLOCK) WHERE [GpoObjCtrlResponsavel_CteCteCod] = @ContratanteUsuario_ContratanteCod AND [GpoObjCtrlResponsavel_CteUsrCod] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D30,1,0,true,true )
             ,new CursorDef("T000D31", "SELECT TOP 1 [ServicoResponsavel_Codigo] FROM [ServicoResponsavel] WITH (NOLOCK) WHERE [ServicoResponsavel_CteCteCod] = @ContratanteUsuario_ContratanteCod AND [ServicoResponsavel_CteUsrCod] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D31,1,0,true,true )
             ,new CursorDef("T000D32", "SELECT [ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [ContratanteUsuario] WITH (NOLOCK) ORDER BY [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000D32,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 40) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((bool[]) buf[10])[0] = rslt.getBool(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 40) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((bool[]) buf[14])[0] = rslt.getBool(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 40) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 21 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 40) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 23 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
