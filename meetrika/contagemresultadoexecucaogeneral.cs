/*
               File: ContagemResultadoExecucaoGeneral
        Description: Contagem Resultado Execucao General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:5.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoexecucaogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemresultadoexecucaogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemresultadoexecucaogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultadoExecucao_Codigo )
      {
         this.A1405ContagemResultadoExecucao_Codigo = aP0_ContagemResultadoExecucao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1405ContagemResultadoExecucao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1405ContagemResultadoExecucao_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAKK2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ContagemResultadoExecucaoGeneral";
               context.Gx_err = 0;
               WSKK2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Resultado Execucao General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311728587");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadoexecucaogeneral.aspx") + "?" + UrlEncode("" +A1405ContagemResultadoExecucao_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1405ContagemResultadoExecucao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOEXECUCAO_OSCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1404ContagemResultadoExecucao_OSCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOEXECUCAO_INICIO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1406ContagemResultadoExecucao_Inicio, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOEXECUCAO_PREVISTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1408ContagemResultadoExecucao_Prevista, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOEXECUCAO_FIM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1407ContagemResultadoExecucao_Fim, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOEXECUCAO_DIAS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1410ContagemResultadoExecucao_Dias), "ZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormKK2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemresultadoexecucaogeneral.js", "?2020311728589");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoExecucaoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Execucao General" ;
      }

      protected void WBKK0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemresultadoexecucaogeneral.aspx");
            }
            wb_table1_2_KK2( true) ;
         }
         else
         {
            wb_table1_2_KK2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_KK2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultadoExecucao_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoExecucaoGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTKK2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado Execucao General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPKK0( ) ;
            }
         }
      }

      protected void WSKK2( )
      {
         STARTKK2( ) ;
         EVTKK2( ) ;
      }

      protected void EVTKK2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11KK2 */
                                    E11KK2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12KK2 */
                                    E12KK2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13KK2 */
                                    E13KK2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14KK2 */
                                    E14KK2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKK0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKK2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormKK2( ) ;
            }
         }
      }

      protected void PAKK2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKK2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ContagemResultadoExecucaoGeneral";
         context.Gx_err = 0;
      }

      protected void RFKK2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00KK2 */
            pr_default.execute(0, new Object[] {A1405ContagemResultadoExecucao_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1410ContagemResultadoExecucao_Dias = H00KK2_A1410ContagemResultadoExecucao_Dias[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1410ContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEXECUCAO_DIAS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1410ContagemResultadoExecucao_Dias), "ZZZ9")));
               n1410ContagemResultadoExecucao_Dias = H00KK2_n1410ContagemResultadoExecucao_Dias[0];
               A1407ContagemResultadoExecucao_Fim = H00KK2_A1407ContagemResultadoExecucao_Fim[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1407ContagemResultadoExecucao_Fim", context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEXECUCAO_FIM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1407ContagemResultadoExecucao_Fim, "99/99/99 99:99")));
               n1407ContagemResultadoExecucao_Fim = H00KK2_n1407ContagemResultadoExecucao_Fim[0];
               A1411ContagemResultadoExecucao_PrazoDias = H00KK2_A1411ContagemResultadoExecucao_PrazoDias[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1411ContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), "ZZZ9")));
               n1411ContagemResultadoExecucao_PrazoDias = H00KK2_n1411ContagemResultadoExecucao_PrazoDias[0];
               A1408ContagemResultadoExecucao_Prevista = H00KK2_A1408ContagemResultadoExecucao_Prevista[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1408ContagemResultadoExecucao_Prevista", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEXECUCAO_PREVISTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1408ContagemResultadoExecucao_Prevista, "99/99/99 99:99")));
               n1408ContagemResultadoExecucao_Prevista = H00KK2_n1408ContagemResultadoExecucao_Prevista[0];
               A1406ContagemResultadoExecucao_Inicio = H00KK2_A1406ContagemResultadoExecucao_Inicio[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEXECUCAO_INICIO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1406ContagemResultadoExecucao_Inicio, "99/99/99 99:99")));
               A1404ContagemResultadoExecucao_OSCod = H00KK2_A1404ContagemResultadoExecucao_OSCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1404ContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEXECUCAO_OSCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1404ContagemResultadoExecucao_OSCod), "ZZZZZ9")));
               /* Execute user event: E12KK2 */
               E12KK2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBKK0( ) ;
         }
      }

      protected void STRUPKK0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ContagemResultadoExecucaoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11KK2 */
         E11KK2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1404ContagemResultadoExecucao_OSCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_OSCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1404ContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEXECUCAO_OSCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1404ContagemResultadoExecucao_OSCod), "ZZZZZ9")));
            A1406ContagemResultadoExecucao_Inicio = context.localUtil.CToT( cgiGet( edtContagemResultadoExecucao_Inicio_Internalname), 0);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1406ContagemResultadoExecucao_Inicio", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEXECUCAO_INICIO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1406ContagemResultadoExecucao_Inicio, "99/99/99 99:99")));
            A1408ContagemResultadoExecucao_Prevista = context.localUtil.CToT( cgiGet( edtContagemResultadoExecucao_Prevista_Internalname), 0);
            n1408ContagemResultadoExecucao_Prevista = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1408ContagemResultadoExecucao_Prevista", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEXECUCAO_PREVISTA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1408ContagemResultadoExecucao_Prevista, "99/99/99 99:99")));
            A1411ContagemResultadoExecucao_PrazoDias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_PrazoDias_Internalname), ",", "."));
            n1411ContagemResultadoExecucao_PrazoDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1411ContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), "ZZZ9")));
            A1407ContagemResultadoExecucao_Fim = context.localUtil.CToT( cgiGet( edtContagemResultadoExecucao_Fim_Internalname), 0);
            n1407ContagemResultadoExecucao_Fim = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1407ContagemResultadoExecucao_Fim", context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEXECUCAO_FIM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1407ContagemResultadoExecucao_Fim, "99/99/99 99:99")));
            A1410ContagemResultadoExecucao_Dias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_Dias_Internalname), ",", "."));
            n1410ContagemResultadoExecucao_Dias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1410ContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOEXECUCAO_DIAS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1410ContagemResultadoExecucao_Dias), "ZZZ9")));
            /* Read saved values. */
            wcpOA1405ContagemResultadoExecucao_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1405ContagemResultadoExecucao_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11KK2 */
         E11KK2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11KK2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12KK2( )
      {
         /* Load Routine */
         edtContagemResultadoExecucao_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoExecucao_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoExecucao_Codigo_Visible), 5, 0)));
      }

      protected void E13KK2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contagemresultadoexecucao.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1405ContagemResultadoExecucao_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14KK2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contagemresultadoexecucao.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1405ContagemResultadoExecucao_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemResultadoExecucao";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContagemResultadoExecucao_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContagemResultadoExecucao_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_KK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_KK2( true) ;
         }
         else
         {
            wb_table2_8_KK2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_KK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_KK2( true) ;
         }
         else
         {
            wb_table3_41_KK2( false) ;
         }
         return  ;
      }

      protected void wb_table3_41_KK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_KK2e( true) ;
         }
         else
         {
            wb_table1_2_KK2e( false) ;
         }
      }

      protected void wb_table3_41_KK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_KK2e( true) ;
         }
         else
         {
            wb_table3_41_KK2e( false) ;
         }
      }

      protected void wb_table2_8_KK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoexecucao_oscod_Internalname, "Demada", "", "", lblTextblockcontagemresultadoexecucao_oscod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_OSCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1404ContagemResultadoExecucao_OSCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_OSCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoexecucao_inicio_Internalname, "Inicio", "", "", lblTextblockcontagemresultadoexecucao_inicio_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoExecucao_Inicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_Inicio_Internalname, context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1406ContagemResultadoExecucao_Inicio, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_Inicio_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoExecucaoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoExecucao_Inicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoexecucao_prevista_Internalname, "Previsto", "", "", lblTextblockcontagemresultadoexecucao_prevista_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoExecucao_Prevista_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_Prevista_Internalname, context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1408ContagemResultadoExecucao_Prevista, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_Prevista_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoExecucaoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoExecucao_Prevista_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoexecucao_prazodias_Internalname, "Dias", "", "", lblTextblockcontagemresultadoexecucao_prazodias_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_PrazoDias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_PrazoDias_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoexecucao_fim_Internalname, "Fim", "", "", lblTextblockcontagemresultadoexecucao_fim_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoExecucao_Fim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_Fim_Internalname, context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1407ContagemResultadoExecucao_Fim, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_Fim_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoExecucaoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoExecucao_Fim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoexecucao_dias_Internalname, "Usado", "", "", lblTextblockcontagemresultadoexecucao_dias_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoExecucao_Dias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1410ContagemResultadoExecucao_Dias), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoExecucao_Dias_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoExecucaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_KK2e( true) ;
         }
         else
         {
            wb_table2_8_KK2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1405ContagemResultadoExecucao_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKK2( ) ;
         WSKK2( ) ;
         WEKK2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1405ContagemResultadoExecucao_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAKK2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemresultadoexecucaogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAKK2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1405ContagemResultadoExecucao_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
         }
         wcpOA1405ContagemResultadoExecucao_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1405ContagemResultadoExecucao_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1405ContagemResultadoExecucao_Codigo != wcpOA1405ContagemResultadoExecucao_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1405ContagemResultadoExecucao_Codigo = A1405ContagemResultadoExecucao_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1405ContagemResultadoExecucao_Codigo = cgiGet( sPrefix+"A1405ContagemResultadoExecucao_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1405ContagemResultadoExecucao_Codigo) > 0 )
         {
            A1405ContagemResultadoExecucao_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1405ContagemResultadoExecucao_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1405ContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0)));
         }
         else
         {
            A1405ContagemResultadoExecucao_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1405ContagemResultadoExecucao_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAKK2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSKK2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSKK2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1405ContagemResultadoExecucao_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1405ContagemResultadoExecucao_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1405ContagemResultadoExecucao_Codigo_CTRL", StringUtil.RTrim( sCtrlA1405ContagemResultadoExecucao_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEKK2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311728618");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contagemresultadoexecucaogeneral.js", "?2020311728618");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultadoexecucao_oscod_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADOEXECUCAO_OSCOD";
         edtContagemResultadoExecucao_OSCod_Internalname = sPrefix+"CONTAGEMRESULTADOEXECUCAO_OSCOD";
         lblTextblockcontagemresultadoexecucao_inicio_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADOEXECUCAO_INICIO";
         edtContagemResultadoExecucao_Inicio_Internalname = sPrefix+"CONTAGEMRESULTADOEXECUCAO_INICIO";
         lblTextblockcontagemresultadoexecucao_prevista_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADOEXECUCAO_PREVISTA";
         edtContagemResultadoExecucao_Prevista_Internalname = sPrefix+"CONTAGEMRESULTADOEXECUCAO_PREVISTA";
         lblTextblockcontagemresultadoexecucao_prazodias_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADOEXECUCAO_PRAZODIAS";
         edtContagemResultadoExecucao_PrazoDias_Internalname = sPrefix+"CONTAGEMRESULTADOEXECUCAO_PRAZODIAS";
         lblTextblockcontagemresultadoexecucao_fim_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADOEXECUCAO_FIM";
         edtContagemResultadoExecucao_Fim_Internalname = sPrefix+"CONTAGEMRESULTADOEXECUCAO_FIM";
         lblTextblockcontagemresultadoexecucao_dias_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADOEXECUCAO_DIAS";
         edtContagemResultadoExecucao_Dias_Internalname = sPrefix+"CONTAGEMRESULTADOEXECUCAO_DIAS";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContagemResultadoExecucao_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADOEXECUCAO_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContagemResultadoExecucao_Dias_Jsonclick = "";
         edtContagemResultadoExecucao_Fim_Jsonclick = "";
         edtContagemResultadoExecucao_PrazoDias_Jsonclick = "";
         edtContagemResultadoExecucao_Prevista_Jsonclick = "";
         edtContagemResultadoExecucao_Inicio_Jsonclick = "";
         edtContagemResultadoExecucao_OSCod_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtContagemResultadoExecucao_Codigo_Jsonclick = "";
         edtContagemResultadoExecucao_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13KK2',iparms:[{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14KK2',iparms:[{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         A1408ContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00KK2_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         H00KK2_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         H00KK2_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         H00KK2_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         H00KK2_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         H00KK2_A1411ContagemResultadoExecucao_PrazoDias = new short[1] ;
         H00KK2_n1411ContagemResultadoExecucao_PrazoDias = new bool[] {false} ;
         H00KK2_A1408ContagemResultadoExecucao_Prevista = new DateTime[] {DateTime.MinValue} ;
         H00KK2_n1408ContagemResultadoExecucao_Prevista = new bool[] {false} ;
         H00KK2_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         H00KK2_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontagemresultadoexecucao_oscod_Jsonclick = "";
         lblTextblockcontagemresultadoexecucao_inicio_Jsonclick = "";
         lblTextblockcontagemresultadoexecucao_prevista_Jsonclick = "";
         lblTextblockcontagemresultadoexecucao_prazodias_Jsonclick = "";
         lblTextblockcontagemresultadoexecucao_fim_Jsonclick = "";
         lblTextblockcontagemresultadoexecucao_dias_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1405ContagemResultadoExecucao_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoexecucaogeneral__default(),
            new Object[][] {
                new Object[] {
               H00KK2_A1405ContagemResultadoExecucao_Codigo, H00KK2_A1410ContagemResultadoExecucao_Dias, H00KK2_n1410ContagemResultadoExecucao_Dias, H00KK2_A1407ContagemResultadoExecucao_Fim, H00KK2_n1407ContagemResultadoExecucao_Fim, H00KK2_A1411ContagemResultadoExecucao_PrazoDias, H00KK2_n1411ContagemResultadoExecucao_PrazoDias, H00KK2_A1408ContagemResultadoExecucao_Prevista, H00KK2_n1408ContagemResultadoExecucao_Prevista, H00KK2_A1406ContagemResultadoExecucao_Inicio,
               H00KK2_A1404ContagemResultadoExecucao_OSCod
               }
            }
         );
         AV14Pgmname = "ContagemResultadoExecucaoGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ContagemResultadoExecucaoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A1411ContagemResultadoExecucao_PrazoDias ;
      private short A1410ContagemResultadoExecucao_Dias ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private int wcpOA1405ContagemResultadoExecucao_Codigo ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int edtContagemResultadoExecucao_Codigo_Visible ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7ContagemResultadoExecucao_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtContagemResultadoExecucao_Codigo_Internalname ;
      private String edtContagemResultadoExecucao_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtContagemResultadoExecucao_OSCod_Internalname ;
      private String edtContagemResultadoExecucao_Inicio_Internalname ;
      private String edtContagemResultadoExecucao_Prevista_Internalname ;
      private String edtContagemResultadoExecucao_PrazoDias_Internalname ;
      private String edtContagemResultadoExecucao_Fim_Internalname ;
      private String edtContagemResultadoExecucao_Dias_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_oscod_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_oscod_Jsonclick ;
      private String edtContagemResultadoExecucao_OSCod_Jsonclick ;
      private String lblTextblockcontagemresultadoexecucao_inicio_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_inicio_Jsonclick ;
      private String edtContagemResultadoExecucao_Inicio_Jsonclick ;
      private String lblTextblockcontagemresultadoexecucao_prevista_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_prevista_Jsonclick ;
      private String edtContagemResultadoExecucao_Prevista_Jsonclick ;
      private String lblTextblockcontagemresultadoexecucao_prazodias_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_prazodias_Jsonclick ;
      private String edtContagemResultadoExecucao_PrazoDias_Jsonclick ;
      private String lblTextblockcontagemresultadoexecucao_fim_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_fim_Jsonclick ;
      private String edtContagemResultadoExecucao_Fim_Jsonclick ;
      private String lblTextblockcontagemresultadoexecucao_dias_Internalname ;
      private String lblTextblockcontagemresultadoexecucao_dias_Jsonclick ;
      private String edtContagemResultadoExecucao_Dias_Jsonclick ;
      private String sCtrlA1405ContagemResultadoExecucao_Codigo ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private DateTime A1408ContagemResultadoExecucao_Prevista ;
      private DateTime A1407ContagemResultadoExecucao_Fim ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1410ContagemResultadoExecucao_Dias ;
      private bool n1407ContagemResultadoExecucao_Fim ;
      private bool n1411ContagemResultadoExecucao_PrazoDias ;
      private bool n1408ContagemResultadoExecucao_Prevista ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00KK2_A1405ContagemResultadoExecucao_Codigo ;
      private short[] H00KK2_A1410ContagemResultadoExecucao_Dias ;
      private bool[] H00KK2_n1410ContagemResultadoExecucao_Dias ;
      private DateTime[] H00KK2_A1407ContagemResultadoExecucao_Fim ;
      private bool[] H00KK2_n1407ContagemResultadoExecucao_Fim ;
      private short[] H00KK2_A1411ContagemResultadoExecucao_PrazoDias ;
      private bool[] H00KK2_n1411ContagemResultadoExecucao_PrazoDias ;
      private DateTime[] H00KK2_A1408ContagemResultadoExecucao_Prevista ;
      private bool[] H00KK2_n1408ContagemResultadoExecucao_Prevista ;
      private DateTime[] H00KK2_A1406ContagemResultadoExecucao_Inicio ;
      private int[] H00KK2_A1404ContagemResultadoExecucao_OSCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contagemresultadoexecucaogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KK2 ;
          prmH00KK2 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KK2", "SELECT [ContagemResultadoExecucao_Codigo], [ContagemResultadoExecucao_Dias], [ContagemResultadoExecucao_Fim], [ContagemResultadoExecucao_PrazoDias], [ContagemResultadoExecucao_Prevista], [ContagemResultadoExecucao_Inicio], [ContagemResultadoExecucao_OSCod] FROM [ContagemResultadoExecucao] WITH (NOLOCK) WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo ORDER BY [ContagemResultadoExecucao_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KK2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
