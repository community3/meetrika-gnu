/*
               File: GetPromptContratoFilterData
        Description: Get Prompt Contrato Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/11/2019 13:27:51.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratofilterdata : GXProcedure
   {
      public getpromptcontratofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV25DDOName = aP0_DDOName;
         this.AV23SearchTxt = aP1_SearchTxt;
         this.AV24SearchTxtTo = aP2_SearchTxtTo;
         this.AV29OptionsJson = "" ;
         this.AV32OptionsDescJson = "" ;
         this.AV34OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV29OptionsJson;
         aP4_OptionsDescJson=this.AV32OptionsDescJson;
         aP5_OptionIndexesJson=this.AV34OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV25DDOName = aP0_DDOName;
         this.AV23SearchTxt = aP1_SearchTxt;
         this.AV24SearchTxtTo = aP2_SearchTxtTo;
         this.AV29OptionsJson = "" ;
         this.AV32OptionsDescJson = "" ;
         this.AV34OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV29OptionsJson;
         aP4_OptionsDescJson=this.AV32OptionsDescJson;
         aP5_OptionIndexesJson=this.AV34OptionIndexesJson;
         return AV34OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratofilterdata objgetpromptcontratofilterdata;
         objgetpromptcontratofilterdata = new getpromptcontratofilterdata();
         objgetpromptcontratofilterdata.AV25DDOName = aP0_DDOName;
         objgetpromptcontratofilterdata.AV23SearchTxt = aP1_SearchTxt;
         objgetpromptcontratofilterdata.AV24SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratofilterdata.AV29OptionsJson = "" ;
         objgetpromptcontratofilterdata.AV32OptionsDescJson = "" ;
         objgetpromptcontratofilterdata.AV34OptionIndexesJson = "" ;
         objgetpromptcontratofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratofilterdata);
         aP3_OptionsJson=this.AV29OptionsJson;
         aP4_OptionsDescJson=this.AV32OptionsDescJson;
         aP5_OptionIndexesJson=this.AV34OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV28Options = (IGxCollection)(new GxSimpleCollection());
         AV31OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV33OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV25DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV25DDOName), "DDO_CONTRATO_NUMEROATA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROATAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV25DDOName), "DDO_CONTRATADA_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOANOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV25DDOName), "DDO_CONTRATADA_PESSOACNPJ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOACNPJOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV29OptionsJson = AV28Options.ToJSonString(false);
         AV32OptionsDescJson = AV31OptionsDesc.ToJSonString(false);
         AV34OptionIndexesJson = AV33OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV36Session.Get("PromptContratoGridState"), "") == 0 )
         {
            AV38GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoGridState"), "");
         }
         else
         {
            AV38GridState.FromXml(AV36Session.Get("PromptContratoGridState"), "");
         }
         AV66GXV1 = 1;
         while ( AV66GXV1 <= AV38GridState.gxTpr_Filtervalues.Count )
         {
            AV39GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV38GridState.gxTpr_Filtervalues.Item(AV66GXV1));
            if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV39GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV39GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMEROATA") == 0 )
            {
               AV12TFContrato_NumeroAta = AV39GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMEROATA_SEL") == 0 )
            {
               AV13TFContrato_NumeroAta_Sel = AV39GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFCONTRATO_ANO") == 0 )
            {
               AV14TFContrato_Ano = (short)(NumberUtil.Val( AV39GridStateFilterValue.gxTpr_Value, "."));
               AV15TFContrato_Ano_To = (short)(NumberUtil.Val( AV39GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV16TFContratada_PessoaNom = AV39GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV17TFContratada_PessoaNom_Sel = AV39GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ") == 0 )
            {
               AV18TFContratada_PessoaCNPJ = AV39GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ_SEL") == 0 )
            {
               AV19TFContratada_PessoaCNPJ_Sel = AV39GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFCONTRATO_VALOR") == 0 )
            {
               AV20TFContrato_Valor = NumberUtil.Val( AV39GridStateFilterValue.gxTpr_Value, ".");
               AV21TFContrato_Valor_To = NumberUtil.Val( AV39GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV39GridStateFilterValue.gxTpr_Name, "TFCONTRATO_ATIVO_SEL") == 0 )
            {
               AV22TFContrato_Ativo_Sel = (short)(NumberUtil.Val( AV39GridStateFilterValue.gxTpr_Value, "."));
            }
            AV66GXV1 = (int)(AV66GXV1+1);
         }
         if ( AV38GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV40GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV38GridState.gxTpr_Dynamicfilters.Item(1));
            AV41DynamicFiltersSelector1 = AV40GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 )
            {
               AV43Contrato_Numero1 = AV40GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 )
            {
               AV42DynamicFiltersOperator1 = AV40GridStateDynamicFilter.gxTpr_Operator;
               AV44Contrato_NumeroAta1 = AV40GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
            {
               AV42DynamicFiltersOperator1 = AV40GridStateDynamicFilter.gxTpr_Operator;
               AV45Contratada_PessoaNom1 = AV40GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_ANO") == 0 )
            {
               AV46Contrato_Ano1 = (short)(NumberUtil.Val( AV40GridStateDynamicFilter.gxTpr_Value, "."));
               AV47Contrato_Ano_To1 = (short)(NumberUtil.Val( AV40GridStateDynamicFilter.gxTpr_Valueto, "."));
            }
            if ( AV38GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV48DynamicFiltersEnabled2 = true;
               AV40GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV38GridState.gxTpr_Dynamicfilters.Item(2));
               AV49DynamicFiltersSelector2 = AV40GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 )
               {
                  AV51Contrato_Numero2 = AV40GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 )
               {
                  AV50DynamicFiltersOperator2 = AV40GridStateDynamicFilter.gxTpr_Operator;
                  AV52Contrato_NumeroAta2 = AV40GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
               {
                  AV50DynamicFiltersOperator2 = AV40GridStateDynamicFilter.gxTpr_Operator;
                  AV53Contratada_PessoaNom2 = AV40GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_ANO") == 0 )
               {
                  AV54Contrato_Ano2 = (short)(NumberUtil.Val( AV40GridStateDynamicFilter.gxTpr_Value, "."));
                  AV55Contrato_Ano_To2 = (short)(NumberUtil.Val( AV40GridStateDynamicFilter.gxTpr_Valueto, "."));
               }
               if ( AV38GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV56DynamicFiltersEnabled3 = true;
                  AV40GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV38GridState.gxTpr_Dynamicfilters.Item(3));
                  AV57DynamicFiltersSelector3 = AV40GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 )
                  {
                     AV59Contrato_Numero3 = AV40GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 )
                  {
                     AV58DynamicFiltersOperator3 = AV40GridStateDynamicFilter.gxTpr_Operator;
                     AV60Contrato_NumeroAta3 = AV40GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 )
                  {
                     AV58DynamicFiltersOperator3 = AV40GridStateDynamicFilter.gxTpr_Operator;
                     AV61Contratada_PessoaNom3 = AV40GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_ANO") == 0 )
                  {
                     AV62Contrato_Ano3 = (short)(NumberUtil.Val( AV40GridStateDynamicFilter.gxTpr_Value, "."));
                     AV63Contrato_Ano_To3 = (short)(NumberUtil.Val( AV40GridStateDynamicFilter.gxTpr_Valueto, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV23SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV41DynamicFiltersSelector1 ,
                                              AV43Contrato_Numero1 ,
                                              AV42DynamicFiltersOperator1 ,
                                              AV44Contrato_NumeroAta1 ,
                                              AV45Contratada_PessoaNom1 ,
                                              AV46Contrato_Ano1 ,
                                              AV47Contrato_Ano_To1 ,
                                              AV48DynamicFiltersEnabled2 ,
                                              AV49DynamicFiltersSelector2 ,
                                              AV51Contrato_Numero2 ,
                                              AV50DynamicFiltersOperator2 ,
                                              AV52Contrato_NumeroAta2 ,
                                              AV53Contratada_PessoaNom2 ,
                                              AV54Contrato_Ano2 ,
                                              AV55Contrato_Ano_To2 ,
                                              AV56DynamicFiltersEnabled3 ,
                                              AV57DynamicFiltersSelector3 ,
                                              AV59Contrato_Numero3 ,
                                              AV58DynamicFiltersOperator3 ,
                                              AV60Contrato_NumeroAta3 ,
                                              AV61Contratada_PessoaNom3 ,
                                              AV62Contrato_Ano3 ,
                                              AV63Contrato_Ano_To3 ,
                                              AV11TFContrato_Numero_Sel ,
                                              AV10TFContrato_Numero ,
                                              AV13TFContrato_NumeroAta_Sel ,
                                              AV12TFContrato_NumeroAta ,
                                              AV14TFContrato_Ano ,
                                              AV15TFContrato_Ano_To ,
                                              AV17TFContratada_PessoaNom_Sel ,
                                              AV16TFContratada_PessoaNom ,
                                              AV19TFContratada_PessoaCNPJ_Sel ,
                                              AV18TFContratada_PessoaCNPJ ,
                                              AV20TFContrato_Valor ,
                                              AV21TFContrato_Valor_To ,
                                              AV22TFContrato_Ativo_Sel ,
                                              A77Contrato_Numero ,
                                              A78Contrato_NumeroAta ,
                                              A41Contratada_PessoaNom ,
                                              A79Contrato_Ano ,
                                              A42Contratada_PessoaCNPJ ,
                                              A89Contrato_Valor ,
                                              A92Contrato_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN
                                              }
         });
         lV43Contrato_Numero1 = StringUtil.PadR( StringUtil.RTrim( AV43Contrato_Numero1), 20, "%");
         lV44Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta1), 10, "%");
         lV44Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta1), 10, "%");
         lV45Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV45Contratada_PessoaNom1), 100, "%");
         lV45Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV45Contratada_PessoaNom1), 100, "%");
         lV51Contrato_Numero2 = StringUtil.PadR( StringUtil.RTrim( AV51Contrato_Numero2), 20, "%");
         lV52Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV52Contrato_NumeroAta2), 10, "%");
         lV52Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV52Contrato_NumeroAta2), 10, "%");
         lV53Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV53Contratada_PessoaNom2), 100, "%");
         lV53Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV53Contratada_PessoaNom2), 100, "%");
         lV59Contrato_Numero3 = StringUtil.PadR( StringUtil.RTrim( AV59Contrato_Numero3), 20, "%");
         lV60Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV60Contrato_NumeroAta3), 10, "%");
         lV60Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV60Contrato_NumeroAta3), 10, "%");
         lV61Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV61Contratada_PessoaNom3), 100, "%");
         lV61Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV61Contratada_PessoaNom3), 100, "%");
         lV10TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFContrato_Numero), 20, "%");
         lV12TFContrato_NumeroAta = StringUtil.PadR( StringUtil.RTrim( AV12TFContrato_NumeroAta), 10, "%");
         lV16TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV16TFContratada_PessoaNom), 100, "%");
         lV18TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV18TFContratada_PessoaCNPJ), "%", "");
         /* Using cursor P00UB2 */
         pr_default.execute(0, new Object[] {lV43Contrato_Numero1, lV44Contrato_NumeroAta1, lV44Contrato_NumeroAta1, lV45Contratada_PessoaNom1, lV45Contratada_PessoaNom1, AV46Contrato_Ano1, AV47Contrato_Ano_To1, lV51Contrato_Numero2, lV52Contrato_NumeroAta2, lV52Contrato_NumeroAta2, lV53Contratada_PessoaNom2, lV53Contratada_PessoaNom2, AV54Contrato_Ano2, AV55Contrato_Ano_To2, lV59Contrato_Numero3, lV60Contrato_NumeroAta3, lV60Contrato_NumeroAta3, lV61Contratada_PessoaNom3, lV61Contratada_PessoaNom3, AV62Contrato_Ano3, AV63Contrato_Ano_To3, lV10TFContrato_Numero, AV11TFContrato_Numero_Sel, lV12TFContrato_NumeroAta, AV13TFContrato_NumeroAta_Sel, AV14TFContrato_Ano, AV15TFContrato_Ano_To, lV16TFContratada_PessoaNom, AV17TFContratada_PessoaNom_Sel, lV18TFContratada_PessoaCNPJ, AV19TFContratada_PessoaCNPJ_Sel, AV20TFContrato_Valor, AV21TFContrato_Valor_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUB2 = false;
            A39Contratada_Codigo = P00UB2_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00UB2_A40Contratada_PessoaCod[0];
            A77Contrato_Numero = P00UB2_A77Contrato_Numero[0];
            A92Contrato_Ativo = P00UB2_A92Contrato_Ativo[0];
            A89Contrato_Valor = P00UB2_A89Contrato_Valor[0];
            A42Contratada_PessoaCNPJ = P00UB2_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UB2_n42Contratada_PessoaCNPJ[0];
            A79Contrato_Ano = P00UB2_A79Contrato_Ano[0];
            A41Contratada_PessoaNom = P00UB2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UB2_n41Contratada_PessoaNom[0];
            A78Contrato_NumeroAta = P00UB2_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00UB2_n78Contrato_NumeroAta[0];
            A74Contrato_Codigo = P00UB2_A74Contrato_Codigo[0];
            A40Contratada_PessoaCod = P00UB2_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00UB2_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UB2_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00UB2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UB2_n41Contratada_PessoaNom[0];
            AV35count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00UB2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKUB2 = false;
               A74Contrato_Codigo = P00UB2_A74Contrato_Codigo[0];
               AV35count = (long)(AV35count+1);
               BRKUB2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV27Option = A77Contrato_Numero;
               AV28Options.Add(AV27Option, 0);
               AV33OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV35count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV28Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUB2 )
            {
               BRKUB2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATO_NUMEROATAOPTIONS' Routine */
         AV12TFContrato_NumeroAta = AV23SearchTxt;
         AV13TFContrato_NumeroAta_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV41DynamicFiltersSelector1 ,
                                              AV43Contrato_Numero1 ,
                                              AV42DynamicFiltersOperator1 ,
                                              AV44Contrato_NumeroAta1 ,
                                              AV45Contratada_PessoaNom1 ,
                                              AV46Contrato_Ano1 ,
                                              AV47Contrato_Ano_To1 ,
                                              AV48DynamicFiltersEnabled2 ,
                                              AV49DynamicFiltersSelector2 ,
                                              AV51Contrato_Numero2 ,
                                              AV50DynamicFiltersOperator2 ,
                                              AV52Contrato_NumeroAta2 ,
                                              AV53Contratada_PessoaNom2 ,
                                              AV54Contrato_Ano2 ,
                                              AV55Contrato_Ano_To2 ,
                                              AV56DynamicFiltersEnabled3 ,
                                              AV57DynamicFiltersSelector3 ,
                                              AV59Contrato_Numero3 ,
                                              AV58DynamicFiltersOperator3 ,
                                              AV60Contrato_NumeroAta3 ,
                                              AV61Contratada_PessoaNom3 ,
                                              AV62Contrato_Ano3 ,
                                              AV63Contrato_Ano_To3 ,
                                              AV11TFContrato_Numero_Sel ,
                                              AV10TFContrato_Numero ,
                                              AV13TFContrato_NumeroAta_Sel ,
                                              AV12TFContrato_NumeroAta ,
                                              AV14TFContrato_Ano ,
                                              AV15TFContrato_Ano_To ,
                                              AV17TFContratada_PessoaNom_Sel ,
                                              AV16TFContratada_PessoaNom ,
                                              AV19TFContratada_PessoaCNPJ_Sel ,
                                              AV18TFContratada_PessoaCNPJ ,
                                              AV20TFContrato_Valor ,
                                              AV21TFContrato_Valor_To ,
                                              AV22TFContrato_Ativo_Sel ,
                                              A77Contrato_Numero ,
                                              A78Contrato_NumeroAta ,
                                              A41Contratada_PessoaNom ,
                                              A79Contrato_Ano ,
                                              A42Contratada_PessoaCNPJ ,
                                              A89Contrato_Valor ,
                                              A92Contrato_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN
                                              }
         });
         lV43Contrato_Numero1 = StringUtil.PadR( StringUtil.RTrim( AV43Contrato_Numero1), 20, "%");
         lV44Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta1), 10, "%");
         lV44Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta1), 10, "%");
         lV45Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV45Contratada_PessoaNom1), 100, "%");
         lV45Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV45Contratada_PessoaNom1), 100, "%");
         lV51Contrato_Numero2 = StringUtil.PadR( StringUtil.RTrim( AV51Contrato_Numero2), 20, "%");
         lV52Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV52Contrato_NumeroAta2), 10, "%");
         lV52Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV52Contrato_NumeroAta2), 10, "%");
         lV53Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV53Contratada_PessoaNom2), 100, "%");
         lV53Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV53Contratada_PessoaNom2), 100, "%");
         lV59Contrato_Numero3 = StringUtil.PadR( StringUtil.RTrim( AV59Contrato_Numero3), 20, "%");
         lV60Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV60Contrato_NumeroAta3), 10, "%");
         lV60Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV60Contrato_NumeroAta3), 10, "%");
         lV61Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV61Contratada_PessoaNom3), 100, "%");
         lV61Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV61Contratada_PessoaNom3), 100, "%");
         lV10TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFContrato_Numero), 20, "%");
         lV12TFContrato_NumeroAta = StringUtil.PadR( StringUtil.RTrim( AV12TFContrato_NumeroAta), 10, "%");
         lV16TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV16TFContratada_PessoaNom), 100, "%");
         lV18TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV18TFContratada_PessoaCNPJ), "%", "");
         /* Using cursor P00UB3 */
         pr_default.execute(1, new Object[] {lV43Contrato_Numero1, lV44Contrato_NumeroAta1, lV44Contrato_NumeroAta1, lV45Contratada_PessoaNom1, lV45Contratada_PessoaNom1, AV46Contrato_Ano1, AV47Contrato_Ano_To1, lV51Contrato_Numero2, lV52Contrato_NumeroAta2, lV52Contrato_NumeroAta2, lV53Contratada_PessoaNom2, lV53Contratada_PessoaNom2, AV54Contrato_Ano2, AV55Contrato_Ano_To2, lV59Contrato_Numero3, lV60Contrato_NumeroAta3, lV60Contrato_NumeroAta3, lV61Contratada_PessoaNom3, lV61Contratada_PessoaNom3, AV62Contrato_Ano3, AV63Contrato_Ano_To3, lV10TFContrato_Numero, AV11TFContrato_Numero_Sel, lV12TFContrato_NumeroAta, AV13TFContrato_NumeroAta_Sel, AV14TFContrato_Ano, AV15TFContrato_Ano_To, lV16TFContratada_PessoaNom, AV17TFContratada_PessoaNom_Sel, lV18TFContratada_PessoaCNPJ, AV19TFContratada_PessoaCNPJ_Sel, AV20TFContrato_Valor, AV21TFContrato_Valor_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUB4 = false;
            A39Contratada_Codigo = P00UB3_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00UB3_A40Contratada_PessoaCod[0];
            A78Contrato_NumeroAta = P00UB3_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00UB3_n78Contrato_NumeroAta[0];
            A92Contrato_Ativo = P00UB3_A92Contrato_Ativo[0];
            A89Contrato_Valor = P00UB3_A89Contrato_Valor[0];
            A42Contratada_PessoaCNPJ = P00UB3_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UB3_n42Contratada_PessoaCNPJ[0];
            A79Contrato_Ano = P00UB3_A79Contrato_Ano[0];
            A41Contratada_PessoaNom = P00UB3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UB3_n41Contratada_PessoaNom[0];
            A77Contrato_Numero = P00UB3_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00UB3_A74Contrato_Codigo[0];
            A40Contratada_PessoaCod = P00UB3_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00UB3_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UB3_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00UB3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UB3_n41Contratada_PessoaNom[0];
            AV35count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00UB3_A78Contrato_NumeroAta[0], A78Contrato_NumeroAta) == 0 ) )
            {
               BRKUB4 = false;
               A74Contrato_Codigo = P00UB3_A74Contrato_Codigo[0];
               AV35count = (long)(AV35count+1);
               BRKUB4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A78Contrato_NumeroAta)) )
            {
               AV27Option = A78Contrato_NumeroAta;
               AV28Options.Add(AV27Option, 0);
               AV33OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV35count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV28Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUB4 )
            {
               BRKUB4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATADA_PESSOANOMOPTIONS' Routine */
         AV16TFContratada_PessoaNom = AV23SearchTxt;
         AV17TFContratada_PessoaNom_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV41DynamicFiltersSelector1 ,
                                              AV43Contrato_Numero1 ,
                                              AV42DynamicFiltersOperator1 ,
                                              AV44Contrato_NumeroAta1 ,
                                              AV45Contratada_PessoaNom1 ,
                                              AV46Contrato_Ano1 ,
                                              AV47Contrato_Ano_To1 ,
                                              AV48DynamicFiltersEnabled2 ,
                                              AV49DynamicFiltersSelector2 ,
                                              AV51Contrato_Numero2 ,
                                              AV50DynamicFiltersOperator2 ,
                                              AV52Contrato_NumeroAta2 ,
                                              AV53Contratada_PessoaNom2 ,
                                              AV54Contrato_Ano2 ,
                                              AV55Contrato_Ano_To2 ,
                                              AV56DynamicFiltersEnabled3 ,
                                              AV57DynamicFiltersSelector3 ,
                                              AV59Contrato_Numero3 ,
                                              AV58DynamicFiltersOperator3 ,
                                              AV60Contrato_NumeroAta3 ,
                                              AV61Contratada_PessoaNom3 ,
                                              AV62Contrato_Ano3 ,
                                              AV63Contrato_Ano_To3 ,
                                              AV11TFContrato_Numero_Sel ,
                                              AV10TFContrato_Numero ,
                                              AV13TFContrato_NumeroAta_Sel ,
                                              AV12TFContrato_NumeroAta ,
                                              AV14TFContrato_Ano ,
                                              AV15TFContrato_Ano_To ,
                                              AV17TFContratada_PessoaNom_Sel ,
                                              AV16TFContratada_PessoaNom ,
                                              AV19TFContratada_PessoaCNPJ_Sel ,
                                              AV18TFContratada_PessoaCNPJ ,
                                              AV20TFContrato_Valor ,
                                              AV21TFContrato_Valor_To ,
                                              AV22TFContrato_Ativo_Sel ,
                                              A77Contrato_Numero ,
                                              A78Contrato_NumeroAta ,
                                              A41Contratada_PessoaNom ,
                                              A79Contrato_Ano ,
                                              A42Contratada_PessoaCNPJ ,
                                              A89Contrato_Valor ,
                                              A92Contrato_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN
                                              }
         });
         lV43Contrato_Numero1 = StringUtil.PadR( StringUtil.RTrim( AV43Contrato_Numero1), 20, "%");
         lV44Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta1), 10, "%");
         lV44Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta1), 10, "%");
         lV45Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV45Contratada_PessoaNom1), 100, "%");
         lV45Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV45Contratada_PessoaNom1), 100, "%");
         lV51Contrato_Numero2 = StringUtil.PadR( StringUtil.RTrim( AV51Contrato_Numero2), 20, "%");
         lV52Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV52Contrato_NumeroAta2), 10, "%");
         lV52Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV52Contrato_NumeroAta2), 10, "%");
         lV53Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV53Contratada_PessoaNom2), 100, "%");
         lV53Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV53Contratada_PessoaNom2), 100, "%");
         lV59Contrato_Numero3 = StringUtil.PadR( StringUtil.RTrim( AV59Contrato_Numero3), 20, "%");
         lV60Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV60Contrato_NumeroAta3), 10, "%");
         lV60Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV60Contrato_NumeroAta3), 10, "%");
         lV61Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV61Contratada_PessoaNom3), 100, "%");
         lV61Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV61Contratada_PessoaNom3), 100, "%");
         lV10TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFContrato_Numero), 20, "%");
         lV12TFContrato_NumeroAta = StringUtil.PadR( StringUtil.RTrim( AV12TFContrato_NumeroAta), 10, "%");
         lV16TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV16TFContratada_PessoaNom), 100, "%");
         lV18TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV18TFContratada_PessoaCNPJ), "%", "");
         /* Using cursor P00UB4 */
         pr_default.execute(2, new Object[] {lV43Contrato_Numero1, lV44Contrato_NumeroAta1, lV44Contrato_NumeroAta1, lV45Contratada_PessoaNom1, lV45Contratada_PessoaNom1, AV46Contrato_Ano1, AV47Contrato_Ano_To1, lV51Contrato_Numero2, lV52Contrato_NumeroAta2, lV52Contrato_NumeroAta2, lV53Contratada_PessoaNom2, lV53Contratada_PessoaNom2, AV54Contrato_Ano2, AV55Contrato_Ano_To2, lV59Contrato_Numero3, lV60Contrato_NumeroAta3, lV60Contrato_NumeroAta3, lV61Contratada_PessoaNom3, lV61Contratada_PessoaNom3, AV62Contrato_Ano3, AV63Contrato_Ano_To3, lV10TFContrato_Numero, AV11TFContrato_Numero_Sel, lV12TFContrato_NumeroAta, AV13TFContrato_NumeroAta_Sel, AV14TFContrato_Ano, AV15TFContrato_Ano_To, lV16TFContratada_PessoaNom, AV17TFContratada_PessoaNom_Sel, lV18TFContratada_PessoaCNPJ, AV19TFContratada_PessoaCNPJ_Sel, AV20TFContrato_Valor, AV21TFContrato_Valor_To});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKUB6 = false;
            A39Contratada_Codigo = P00UB4_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00UB4_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00UB4_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UB4_n41Contratada_PessoaNom[0];
            A92Contrato_Ativo = P00UB4_A92Contrato_Ativo[0];
            A89Contrato_Valor = P00UB4_A89Contrato_Valor[0];
            A42Contratada_PessoaCNPJ = P00UB4_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UB4_n42Contratada_PessoaCNPJ[0];
            A79Contrato_Ano = P00UB4_A79Contrato_Ano[0];
            A78Contrato_NumeroAta = P00UB4_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00UB4_n78Contrato_NumeroAta[0];
            A77Contrato_Numero = P00UB4_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00UB4_A74Contrato_Codigo[0];
            A40Contratada_PessoaCod = P00UB4_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00UB4_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UB4_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00UB4_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UB4_n42Contratada_PessoaCNPJ[0];
            AV35count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00UB4_A41Contratada_PessoaNom[0], A41Contratada_PessoaNom) == 0 ) )
            {
               BRKUB6 = false;
               A39Contratada_Codigo = P00UB4_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00UB4_A40Contratada_PessoaCod[0];
               A74Contrato_Codigo = P00UB4_A74Contrato_Codigo[0];
               A40Contratada_PessoaCod = P00UB4_A40Contratada_PessoaCod[0];
               AV35count = (long)(AV35count+1);
               BRKUB6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A41Contratada_PessoaNom)) )
            {
               AV27Option = A41Contratada_PessoaNom;
               AV28Options.Add(AV27Option, 0);
               AV33OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV35count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV28Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUB6 )
            {
               BRKUB6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATADA_PESSOACNPJOPTIONS' Routine */
         AV18TFContratada_PessoaCNPJ = AV23SearchTxt;
         AV19TFContratada_PessoaCNPJ_Sel = "";
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV41DynamicFiltersSelector1 ,
                                              AV43Contrato_Numero1 ,
                                              AV42DynamicFiltersOperator1 ,
                                              AV44Contrato_NumeroAta1 ,
                                              AV45Contratada_PessoaNom1 ,
                                              AV46Contrato_Ano1 ,
                                              AV47Contrato_Ano_To1 ,
                                              AV48DynamicFiltersEnabled2 ,
                                              AV49DynamicFiltersSelector2 ,
                                              AV51Contrato_Numero2 ,
                                              AV50DynamicFiltersOperator2 ,
                                              AV52Contrato_NumeroAta2 ,
                                              AV53Contratada_PessoaNom2 ,
                                              AV54Contrato_Ano2 ,
                                              AV55Contrato_Ano_To2 ,
                                              AV56DynamicFiltersEnabled3 ,
                                              AV57DynamicFiltersSelector3 ,
                                              AV59Contrato_Numero3 ,
                                              AV58DynamicFiltersOperator3 ,
                                              AV60Contrato_NumeroAta3 ,
                                              AV61Contratada_PessoaNom3 ,
                                              AV62Contrato_Ano3 ,
                                              AV63Contrato_Ano_To3 ,
                                              AV11TFContrato_Numero_Sel ,
                                              AV10TFContrato_Numero ,
                                              AV13TFContrato_NumeroAta_Sel ,
                                              AV12TFContrato_NumeroAta ,
                                              AV14TFContrato_Ano ,
                                              AV15TFContrato_Ano_To ,
                                              AV17TFContratada_PessoaNom_Sel ,
                                              AV16TFContratada_PessoaNom ,
                                              AV19TFContratada_PessoaCNPJ_Sel ,
                                              AV18TFContratada_PessoaCNPJ ,
                                              AV20TFContrato_Valor ,
                                              AV21TFContrato_Valor_To ,
                                              AV22TFContrato_Ativo_Sel ,
                                              A77Contrato_Numero ,
                                              A78Contrato_NumeroAta ,
                                              A41Contratada_PessoaNom ,
                                              A79Contrato_Ano ,
                                              A42Contratada_PessoaCNPJ ,
                                              A89Contrato_Valor ,
                                              A92Contrato_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN
                                              }
         });
         lV43Contrato_Numero1 = StringUtil.PadR( StringUtil.RTrim( AV43Contrato_Numero1), 20, "%");
         lV44Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta1), 10, "%");
         lV44Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta1), 10, "%");
         lV45Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV45Contratada_PessoaNom1), 100, "%");
         lV45Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV45Contratada_PessoaNom1), 100, "%");
         lV51Contrato_Numero2 = StringUtil.PadR( StringUtil.RTrim( AV51Contrato_Numero2), 20, "%");
         lV52Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV52Contrato_NumeroAta2), 10, "%");
         lV52Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV52Contrato_NumeroAta2), 10, "%");
         lV53Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV53Contratada_PessoaNom2), 100, "%");
         lV53Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV53Contratada_PessoaNom2), 100, "%");
         lV59Contrato_Numero3 = StringUtil.PadR( StringUtil.RTrim( AV59Contrato_Numero3), 20, "%");
         lV60Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV60Contrato_NumeroAta3), 10, "%");
         lV60Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV60Contrato_NumeroAta3), 10, "%");
         lV61Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV61Contratada_PessoaNom3), 100, "%");
         lV61Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV61Contratada_PessoaNom3), 100, "%");
         lV10TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFContrato_Numero), 20, "%");
         lV12TFContrato_NumeroAta = StringUtil.PadR( StringUtil.RTrim( AV12TFContrato_NumeroAta), 10, "%");
         lV16TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV16TFContratada_PessoaNom), 100, "%");
         lV18TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV18TFContratada_PessoaCNPJ), "%", "");
         /* Using cursor P00UB5 */
         pr_default.execute(3, new Object[] {lV43Contrato_Numero1, lV44Contrato_NumeroAta1, lV44Contrato_NumeroAta1, lV45Contratada_PessoaNom1, lV45Contratada_PessoaNom1, AV46Contrato_Ano1, AV47Contrato_Ano_To1, lV51Contrato_Numero2, lV52Contrato_NumeroAta2, lV52Contrato_NumeroAta2, lV53Contratada_PessoaNom2, lV53Contratada_PessoaNom2, AV54Contrato_Ano2, AV55Contrato_Ano_To2, lV59Contrato_Numero3, lV60Contrato_NumeroAta3, lV60Contrato_NumeroAta3, lV61Contratada_PessoaNom3, lV61Contratada_PessoaNom3, AV62Contrato_Ano3, AV63Contrato_Ano_To3, lV10TFContrato_Numero, AV11TFContrato_Numero_Sel, lV12TFContrato_NumeroAta, AV13TFContrato_NumeroAta_Sel, AV14TFContrato_Ano, AV15TFContrato_Ano_To, lV16TFContratada_PessoaNom, AV17TFContratada_PessoaNom_Sel, lV18TFContratada_PessoaCNPJ, AV19TFContratada_PessoaCNPJ_Sel, AV20TFContrato_Valor, AV21TFContrato_Valor_To});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKUB8 = false;
            A39Contratada_Codigo = P00UB5_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00UB5_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00UB5_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UB5_n42Contratada_PessoaCNPJ[0];
            A92Contrato_Ativo = P00UB5_A92Contrato_Ativo[0];
            A89Contrato_Valor = P00UB5_A89Contrato_Valor[0];
            A79Contrato_Ano = P00UB5_A79Contrato_Ano[0];
            A41Contratada_PessoaNom = P00UB5_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UB5_n41Contratada_PessoaNom[0];
            A78Contrato_NumeroAta = P00UB5_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00UB5_n78Contrato_NumeroAta[0];
            A77Contrato_Numero = P00UB5_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00UB5_A74Contrato_Codigo[0];
            A40Contratada_PessoaCod = P00UB5_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00UB5_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UB5_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00UB5_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UB5_n41Contratada_PessoaNom[0];
            AV35count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00UB5_A42Contratada_PessoaCNPJ[0], A42Contratada_PessoaCNPJ) == 0 ) )
            {
               BRKUB8 = false;
               A39Contratada_Codigo = P00UB5_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00UB5_A40Contratada_PessoaCod[0];
               A74Contrato_Codigo = P00UB5_A74Contrato_Codigo[0];
               A40Contratada_PessoaCod = P00UB5_A40Contratada_PessoaCod[0];
               AV35count = (long)(AV35count+1);
               BRKUB8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A42Contratada_PessoaCNPJ)) )
            {
               AV27Option = A42Contratada_PessoaCNPJ;
               AV28Options.Add(AV27Option, 0);
               AV33OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV35count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV28Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUB8 )
            {
               BRKUB8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV28Options = new GxSimpleCollection();
         AV31OptionsDesc = new GxSimpleCollection();
         AV33OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV36Session = context.GetSession();
         AV38GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV39GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContrato_NumeroAta = "";
         AV13TFContrato_NumeroAta_Sel = "";
         AV16TFContratada_PessoaNom = "";
         AV17TFContratada_PessoaNom_Sel = "";
         AV18TFContratada_PessoaCNPJ = "";
         AV19TFContratada_PessoaCNPJ_Sel = "";
         AV40GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV41DynamicFiltersSelector1 = "";
         AV43Contrato_Numero1 = "";
         AV44Contrato_NumeroAta1 = "";
         AV45Contratada_PessoaNom1 = "";
         AV49DynamicFiltersSelector2 = "";
         AV51Contrato_Numero2 = "";
         AV52Contrato_NumeroAta2 = "";
         AV53Contratada_PessoaNom2 = "";
         AV57DynamicFiltersSelector3 = "";
         AV59Contrato_Numero3 = "";
         AV60Contrato_NumeroAta3 = "";
         AV61Contratada_PessoaNom3 = "";
         scmdbuf = "";
         lV43Contrato_Numero1 = "";
         lV44Contrato_NumeroAta1 = "";
         lV45Contratada_PessoaNom1 = "";
         lV51Contrato_Numero2 = "";
         lV52Contrato_NumeroAta2 = "";
         lV53Contratada_PessoaNom2 = "";
         lV59Contrato_Numero3 = "";
         lV60Contrato_NumeroAta3 = "";
         lV61Contratada_PessoaNom3 = "";
         lV10TFContrato_Numero = "";
         lV12TFContrato_NumeroAta = "";
         lV16TFContratada_PessoaNom = "";
         lV18TFContratada_PessoaCNPJ = "";
         A77Contrato_Numero = "";
         A78Contrato_NumeroAta = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         P00UB2_A39Contratada_Codigo = new int[1] ;
         P00UB2_A40Contratada_PessoaCod = new int[1] ;
         P00UB2_A77Contrato_Numero = new String[] {""} ;
         P00UB2_A92Contrato_Ativo = new bool[] {false} ;
         P00UB2_A89Contrato_Valor = new decimal[1] ;
         P00UB2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00UB2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00UB2_A79Contrato_Ano = new short[1] ;
         P00UB2_A41Contratada_PessoaNom = new String[] {""} ;
         P00UB2_n41Contratada_PessoaNom = new bool[] {false} ;
         P00UB2_A78Contrato_NumeroAta = new String[] {""} ;
         P00UB2_n78Contrato_NumeroAta = new bool[] {false} ;
         P00UB2_A74Contrato_Codigo = new int[1] ;
         AV27Option = "";
         P00UB3_A39Contratada_Codigo = new int[1] ;
         P00UB3_A40Contratada_PessoaCod = new int[1] ;
         P00UB3_A78Contrato_NumeroAta = new String[] {""} ;
         P00UB3_n78Contrato_NumeroAta = new bool[] {false} ;
         P00UB3_A92Contrato_Ativo = new bool[] {false} ;
         P00UB3_A89Contrato_Valor = new decimal[1] ;
         P00UB3_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00UB3_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00UB3_A79Contrato_Ano = new short[1] ;
         P00UB3_A41Contratada_PessoaNom = new String[] {""} ;
         P00UB3_n41Contratada_PessoaNom = new bool[] {false} ;
         P00UB3_A77Contrato_Numero = new String[] {""} ;
         P00UB3_A74Contrato_Codigo = new int[1] ;
         P00UB4_A39Contratada_Codigo = new int[1] ;
         P00UB4_A40Contratada_PessoaCod = new int[1] ;
         P00UB4_A41Contratada_PessoaNom = new String[] {""} ;
         P00UB4_n41Contratada_PessoaNom = new bool[] {false} ;
         P00UB4_A92Contrato_Ativo = new bool[] {false} ;
         P00UB4_A89Contrato_Valor = new decimal[1] ;
         P00UB4_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00UB4_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00UB4_A79Contrato_Ano = new short[1] ;
         P00UB4_A78Contrato_NumeroAta = new String[] {""} ;
         P00UB4_n78Contrato_NumeroAta = new bool[] {false} ;
         P00UB4_A77Contrato_Numero = new String[] {""} ;
         P00UB4_A74Contrato_Codigo = new int[1] ;
         P00UB5_A39Contratada_Codigo = new int[1] ;
         P00UB5_A40Contratada_PessoaCod = new int[1] ;
         P00UB5_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00UB5_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00UB5_A92Contrato_Ativo = new bool[] {false} ;
         P00UB5_A89Contrato_Valor = new decimal[1] ;
         P00UB5_A79Contrato_Ano = new short[1] ;
         P00UB5_A41Contratada_PessoaNom = new String[] {""} ;
         P00UB5_n41Contratada_PessoaNom = new bool[] {false} ;
         P00UB5_A78Contrato_NumeroAta = new String[] {""} ;
         P00UB5_n78Contrato_NumeroAta = new bool[] {false} ;
         P00UB5_A77Contrato_Numero = new String[] {""} ;
         P00UB5_A74Contrato_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UB2_A39Contratada_Codigo, P00UB2_A40Contratada_PessoaCod, P00UB2_A77Contrato_Numero, P00UB2_A92Contrato_Ativo, P00UB2_A89Contrato_Valor, P00UB2_A42Contratada_PessoaCNPJ, P00UB2_n42Contratada_PessoaCNPJ, P00UB2_A79Contrato_Ano, P00UB2_A41Contratada_PessoaNom, P00UB2_n41Contratada_PessoaNom,
               P00UB2_A78Contrato_NumeroAta, P00UB2_n78Contrato_NumeroAta, P00UB2_A74Contrato_Codigo
               }
               , new Object[] {
               P00UB3_A39Contratada_Codigo, P00UB3_A40Contratada_PessoaCod, P00UB3_A78Contrato_NumeroAta, P00UB3_n78Contrato_NumeroAta, P00UB3_A92Contrato_Ativo, P00UB3_A89Contrato_Valor, P00UB3_A42Contratada_PessoaCNPJ, P00UB3_n42Contratada_PessoaCNPJ, P00UB3_A79Contrato_Ano, P00UB3_A41Contratada_PessoaNom,
               P00UB3_n41Contratada_PessoaNom, P00UB3_A77Contrato_Numero, P00UB3_A74Contrato_Codigo
               }
               , new Object[] {
               P00UB4_A39Contratada_Codigo, P00UB4_A40Contratada_PessoaCod, P00UB4_A41Contratada_PessoaNom, P00UB4_n41Contratada_PessoaNom, P00UB4_A92Contrato_Ativo, P00UB4_A89Contrato_Valor, P00UB4_A42Contratada_PessoaCNPJ, P00UB4_n42Contratada_PessoaCNPJ, P00UB4_A79Contrato_Ano, P00UB4_A78Contrato_NumeroAta,
               P00UB4_n78Contrato_NumeroAta, P00UB4_A77Contrato_Numero, P00UB4_A74Contrato_Codigo
               }
               , new Object[] {
               P00UB5_A39Contratada_Codigo, P00UB5_A40Contratada_PessoaCod, P00UB5_A42Contratada_PessoaCNPJ, P00UB5_n42Contratada_PessoaCNPJ, P00UB5_A92Contrato_Ativo, P00UB5_A89Contrato_Valor, P00UB5_A79Contrato_Ano, P00UB5_A41Contratada_PessoaNom, P00UB5_n41Contratada_PessoaNom, P00UB5_A78Contrato_NumeroAta,
               P00UB5_n78Contrato_NumeroAta, P00UB5_A77Contrato_Numero, P00UB5_A74Contrato_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14TFContrato_Ano ;
      private short AV15TFContrato_Ano_To ;
      private short AV22TFContrato_Ativo_Sel ;
      private short AV42DynamicFiltersOperator1 ;
      private short AV46Contrato_Ano1 ;
      private short AV47Contrato_Ano_To1 ;
      private short AV50DynamicFiltersOperator2 ;
      private short AV54Contrato_Ano2 ;
      private short AV55Contrato_Ano_To2 ;
      private short AV58DynamicFiltersOperator3 ;
      private short AV62Contrato_Ano3 ;
      private short AV63Contrato_Ano_To3 ;
      private short A79Contrato_Ano ;
      private int AV66GXV1 ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A74Contrato_Codigo ;
      private long AV35count ;
      private decimal AV20TFContrato_Valor ;
      private decimal AV21TFContrato_Valor_To ;
      private decimal A89Contrato_Valor ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String AV12TFContrato_NumeroAta ;
      private String AV13TFContrato_NumeroAta_Sel ;
      private String AV16TFContratada_PessoaNom ;
      private String AV17TFContratada_PessoaNom_Sel ;
      private String AV43Contrato_Numero1 ;
      private String AV44Contrato_NumeroAta1 ;
      private String AV45Contratada_PessoaNom1 ;
      private String AV51Contrato_Numero2 ;
      private String AV52Contrato_NumeroAta2 ;
      private String AV53Contratada_PessoaNom2 ;
      private String AV59Contrato_Numero3 ;
      private String AV60Contrato_NumeroAta3 ;
      private String AV61Contratada_PessoaNom3 ;
      private String scmdbuf ;
      private String lV43Contrato_Numero1 ;
      private String lV44Contrato_NumeroAta1 ;
      private String lV45Contratada_PessoaNom1 ;
      private String lV51Contrato_Numero2 ;
      private String lV52Contrato_NumeroAta2 ;
      private String lV53Contratada_PessoaNom2 ;
      private String lV59Contrato_Numero3 ;
      private String lV60Contrato_NumeroAta3 ;
      private String lV61Contratada_PessoaNom3 ;
      private String lV10TFContrato_Numero ;
      private String lV12TFContrato_NumeroAta ;
      private String lV16TFContratada_PessoaNom ;
      private String A77Contrato_Numero ;
      private String A78Contrato_NumeroAta ;
      private String A41Contratada_PessoaNom ;
      private bool returnInSub ;
      private bool AV48DynamicFiltersEnabled2 ;
      private bool AV56DynamicFiltersEnabled3 ;
      private bool A92Contrato_Ativo ;
      private bool BRKUB2 ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n41Contratada_PessoaNom ;
      private bool n78Contrato_NumeroAta ;
      private bool BRKUB4 ;
      private bool BRKUB6 ;
      private bool BRKUB8 ;
      private String AV34OptionIndexesJson ;
      private String AV29OptionsJson ;
      private String AV32OptionsDescJson ;
      private String AV25DDOName ;
      private String AV23SearchTxt ;
      private String AV24SearchTxtTo ;
      private String AV18TFContratada_PessoaCNPJ ;
      private String AV19TFContratada_PessoaCNPJ_Sel ;
      private String AV41DynamicFiltersSelector1 ;
      private String AV49DynamicFiltersSelector2 ;
      private String AV57DynamicFiltersSelector3 ;
      private String lV18TFContratada_PessoaCNPJ ;
      private String A42Contratada_PessoaCNPJ ;
      private String AV27Option ;
      private IGxSession AV36Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00UB2_A39Contratada_Codigo ;
      private int[] P00UB2_A40Contratada_PessoaCod ;
      private String[] P00UB2_A77Contrato_Numero ;
      private bool[] P00UB2_A92Contrato_Ativo ;
      private decimal[] P00UB2_A89Contrato_Valor ;
      private String[] P00UB2_A42Contratada_PessoaCNPJ ;
      private bool[] P00UB2_n42Contratada_PessoaCNPJ ;
      private short[] P00UB2_A79Contrato_Ano ;
      private String[] P00UB2_A41Contratada_PessoaNom ;
      private bool[] P00UB2_n41Contratada_PessoaNom ;
      private String[] P00UB2_A78Contrato_NumeroAta ;
      private bool[] P00UB2_n78Contrato_NumeroAta ;
      private int[] P00UB2_A74Contrato_Codigo ;
      private int[] P00UB3_A39Contratada_Codigo ;
      private int[] P00UB3_A40Contratada_PessoaCod ;
      private String[] P00UB3_A78Contrato_NumeroAta ;
      private bool[] P00UB3_n78Contrato_NumeroAta ;
      private bool[] P00UB3_A92Contrato_Ativo ;
      private decimal[] P00UB3_A89Contrato_Valor ;
      private String[] P00UB3_A42Contratada_PessoaCNPJ ;
      private bool[] P00UB3_n42Contratada_PessoaCNPJ ;
      private short[] P00UB3_A79Contrato_Ano ;
      private String[] P00UB3_A41Contratada_PessoaNom ;
      private bool[] P00UB3_n41Contratada_PessoaNom ;
      private String[] P00UB3_A77Contrato_Numero ;
      private int[] P00UB3_A74Contrato_Codigo ;
      private int[] P00UB4_A39Contratada_Codigo ;
      private int[] P00UB4_A40Contratada_PessoaCod ;
      private String[] P00UB4_A41Contratada_PessoaNom ;
      private bool[] P00UB4_n41Contratada_PessoaNom ;
      private bool[] P00UB4_A92Contrato_Ativo ;
      private decimal[] P00UB4_A89Contrato_Valor ;
      private String[] P00UB4_A42Contratada_PessoaCNPJ ;
      private bool[] P00UB4_n42Contratada_PessoaCNPJ ;
      private short[] P00UB4_A79Contrato_Ano ;
      private String[] P00UB4_A78Contrato_NumeroAta ;
      private bool[] P00UB4_n78Contrato_NumeroAta ;
      private String[] P00UB4_A77Contrato_Numero ;
      private int[] P00UB4_A74Contrato_Codigo ;
      private int[] P00UB5_A39Contratada_Codigo ;
      private int[] P00UB5_A40Contratada_PessoaCod ;
      private String[] P00UB5_A42Contratada_PessoaCNPJ ;
      private bool[] P00UB5_n42Contratada_PessoaCNPJ ;
      private bool[] P00UB5_A92Contrato_Ativo ;
      private decimal[] P00UB5_A89Contrato_Valor ;
      private short[] P00UB5_A79Contrato_Ano ;
      private String[] P00UB5_A41Contratada_PessoaNom ;
      private bool[] P00UB5_n41Contratada_PessoaNom ;
      private String[] P00UB5_A78Contrato_NumeroAta ;
      private bool[] P00UB5_n78Contrato_NumeroAta ;
      private String[] P00UB5_A77Contrato_Numero ;
      private int[] P00UB5_A74Contrato_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV33OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV38GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV39GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV40GridStateDynamicFilter ;
   }

   public class getpromptcontratofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UB2( IGxContext context ,
                                             String AV41DynamicFiltersSelector1 ,
                                             String AV43Contrato_Numero1 ,
                                             short AV42DynamicFiltersOperator1 ,
                                             String AV44Contrato_NumeroAta1 ,
                                             String AV45Contratada_PessoaNom1 ,
                                             short AV46Contrato_Ano1 ,
                                             short AV47Contrato_Ano_To1 ,
                                             bool AV48DynamicFiltersEnabled2 ,
                                             String AV49DynamicFiltersSelector2 ,
                                             String AV51Contrato_Numero2 ,
                                             short AV50DynamicFiltersOperator2 ,
                                             String AV52Contrato_NumeroAta2 ,
                                             String AV53Contratada_PessoaNom2 ,
                                             short AV54Contrato_Ano2 ,
                                             short AV55Contrato_Ano_To2 ,
                                             bool AV56DynamicFiltersEnabled3 ,
                                             String AV57DynamicFiltersSelector3 ,
                                             String AV59Contrato_Numero3 ,
                                             short AV58DynamicFiltersOperator3 ,
                                             String AV60Contrato_NumeroAta3 ,
                                             String AV61Contratada_PessoaNom3 ,
                                             short AV62Contrato_Ano3 ,
                                             short AV63Contrato_Ano_To3 ,
                                             String AV11TFContrato_Numero_Sel ,
                                             String AV10TFContrato_Numero ,
                                             String AV13TFContrato_NumeroAta_Sel ,
                                             String AV12TFContrato_NumeroAta ,
                                             short AV14TFContrato_Ano ,
                                             short AV15TFContrato_Ano_To ,
                                             String AV17TFContratada_PessoaNom_Sel ,
                                             String AV16TFContratada_PessoaNom ,
                                             String AV19TFContratada_PessoaCNPJ_Sel ,
                                             String AV18TFContratada_PessoaCNPJ ,
                                             decimal AV20TFContrato_Valor ,
                                             decimal AV21TFContrato_Valor_To ,
                                             short AV22TFContrato_Ativo_Sel ,
                                             String A77Contrato_Numero ,
                                             String A78Contrato_NumeroAta ,
                                             String A41Contratada_PessoaNom ,
                                             short A79Contrato_Ano ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A89Contrato_Valor ,
                                             bool A92Contrato_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [33] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratada_Codigo], T2.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_Numero], T1.[Contrato_Ativo], T1.[Contrato_Valor], T3.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contrato_Ano], T3.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_NumeroAta], T1.[Contrato_Codigo] FROM (([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Contrato_Numero1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV43Contrato_Numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV43Contrato_Numero1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( AV42DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV44Contrato_NumeroAta1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV44Contrato_NumeroAta1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( AV42DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV42DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Contratada_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV45Contratada_PessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV45Contratada_PessoaNom1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV42DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Contratada_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV45Contratada_PessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV45Contratada_PessoaNom1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV46Contrato_Ano1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV46Contrato_Ano1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV46Contrato_Ano1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV47Contrato_Ano_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV47Contrato_Ano_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV47Contrato_Ano_To1)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Contrato_Numero2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV51Contrato_Numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV51Contrato_Numero2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( AV50DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Contrato_NumeroAta2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV52Contrato_NumeroAta2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV52Contrato_NumeroAta2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( AV50DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Contrato_NumeroAta2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV52Contrato_NumeroAta2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV52Contrato_NumeroAta2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV50DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53Contratada_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV53Contratada_PessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV53Contratada_PessoaNom2)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV50DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53Contratada_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV53Contratada_PessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV53Contratada_PessoaNom2)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV54Contrato_Ano2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV54Contrato_Ano2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV54Contrato_Ano2)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV55Contrato_Ano_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV55Contrato_Ano_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV55Contrato_Ano_To2)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Contrato_Numero3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV59Contrato_Numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV59Contrato_Numero3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Contrato_NumeroAta3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV60Contrato_NumeroAta3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV60Contrato_NumeroAta3)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Contrato_NumeroAta3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV60Contrato_NumeroAta3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV60Contrato_NumeroAta3)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61Contratada_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV61Contratada_PessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV61Contratada_PessoaNom3)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61Contratada_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV61Contratada_PessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV61Contratada_PessoaNom3)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV62Contrato_Ano3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV62Contrato_Ano3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV62Contrato_Ano3)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV63Contrato_Ano_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV63Contrato_Ano_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV63Contrato_Ano_To3)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContrato_NumeroAta_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContrato_NumeroAta)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV12TFContrato_NumeroAta)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV12TFContrato_NumeroAta)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContrato_NumeroAta_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] = @AV13TFContrato_NumeroAta_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] = @AV13TFContrato_NumeroAta_Sel)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! (0==AV14TFContrato_Ano) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV14TFContrato_Ano)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV14TFContrato_Ano)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( ! (0==AV15TFContrato_Ano_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV15TFContrato_Ano_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV15TFContrato_Ano_To)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV16TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV16TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV17TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV17TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like @lV18TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Docto] like @lV18TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Docto] = @AV19TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Docto] = @AV19TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV20TFContrato_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Valor] >= @AV20TFContrato_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Valor] >= @AV20TFContrato_Valor)";
            }
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFContrato_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Valor] <= @AV21TFContrato_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Valor] <= @AV21TFContrato_Valor_To)";
            }
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( AV22TFContrato_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ativo] = 1)";
            }
         }
         if ( AV22TFContrato_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UB3( IGxContext context ,
                                             String AV41DynamicFiltersSelector1 ,
                                             String AV43Contrato_Numero1 ,
                                             short AV42DynamicFiltersOperator1 ,
                                             String AV44Contrato_NumeroAta1 ,
                                             String AV45Contratada_PessoaNom1 ,
                                             short AV46Contrato_Ano1 ,
                                             short AV47Contrato_Ano_To1 ,
                                             bool AV48DynamicFiltersEnabled2 ,
                                             String AV49DynamicFiltersSelector2 ,
                                             String AV51Contrato_Numero2 ,
                                             short AV50DynamicFiltersOperator2 ,
                                             String AV52Contrato_NumeroAta2 ,
                                             String AV53Contratada_PessoaNom2 ,
                                             short AV54Contrato_Ano2 ,
                                             short AV55Contrato_Ano_To2 ,
                                             bool AV56DynamicFiltersEnabled3 ,
                                             String AV57DynamicFiltersSelector3 ,
                                             String AV59Contrato_Numero3 ,
                                             short AV58DynamicFiltersOperator3 ,
                                             String AV60Contrato_NumeroAta3 ,
                                             String AV61Contratada_PessoaNom3 ,
                                             short AV62Contrato_Ano3 ,
                                             short AV63Contrato_Ano_To3 ,
                                             String AV11TFContrato_Numero_Sel ,
                                             String AV10TFContrato_Numero ,
                                             String AV13TFContrato_NumeroAta_Sel ,
                                             String AV12TFContrato_NumeroAta ,
                                             short AV14TFContrato_Ano ,
                                             short AV15TFContrato_Ano_To ,
                                             String AV17TFContratada_PessoaNom_Sel ,
                                             String AV16TFContratada_PessoaNom ,
                                             String AV19TFContratada_PessoaCNPJ_Sel ,
                                             String AV18TFContratada_PessoaCNPJ ,
                                             decimal AV20TFContrato_Valor ,
                                             decimal AV21TFContrato_Valor_To ,
                                             short AV22TFContrato_Ativo_Sel ,
                                             String A77Contrato_Numero ,
                                             String A78Contrato_NumeroAta ,
                                             String A41Contratada_PessoaNom ,
                                             short A79Contrato_Ano ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A89Contrato_Valor ,
                                             bool A92Contrato_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [33] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratada_Codigo], T2.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_NumeroAta], T1.[Contrato_Ativo], T1.[Contrato_Valor], T3.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contrato_Ano], T3.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_Numero], T1.[Contrato_Codigo] FROM (([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Contrato_Numero1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV43Contrato_Numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV43Contrato_Numero1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( AV42DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV44Contrato_NumeroAta1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV44Contrato_NumeroAta1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( AV42DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV42DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Contratada_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV45Contratada_PessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV45Contratada_PessoaNom1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV42DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Contratada_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV45Contratada_PessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV45Contratada_PessoaNom1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV46Contrato_Ano1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV46Contrato_Ano1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV46Contrato_Ano1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV47Contrato_Ano_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV47Contrato_Ano_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV47Contrato_Ano_To1)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Contrato_Numero2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV51Contrato_Numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV51Contrato_Numero2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( AV50DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Contrato_NumeroAta2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV52Contrato_NumeroAta2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV52Contrato_NumeroAta2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( AV50DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Contrato_NumeroAta2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV52Contrato_NumeroAta2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV52Contrato_NumeroAta2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV50DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53Contratada_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV53Contratada_PessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV53Contratada_PessoaNom2)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV50DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53Contratada_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV53Contratada_PessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV53Contratada_PessoaNom2)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV54Contrato_Ano2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV54Contrato_Ano2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV54Contrato_Ano2)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV55Contrato_Ano_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV55Contrato_Ano_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV55Contrato_Ano_To2)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Contrato_Numero3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV59Contrato_Numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV59Contrato_Numero3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Contrato_NumeroAta3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV60Contrato_NumeroAta3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV60Contrato_NumeroAta3)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Contrato_NumeroAta3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV60Contrato_NumeroAta3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV60Contrato_NumeroAta3)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61Contratada_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV61Contratada_PessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV61Contratada_PessoaNom3)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61Contratada_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV61Contratada_PessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV61Contratada_PessoaNom3)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV62Contrato_Ano3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV62Contrato_Ano3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV62Contrato_Ano3)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV63Contrato_Ano_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV63Contrato_Ano_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV63Contrato_Ano_To3)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContrato_NumeroAta_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContrato_NumeroAta)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV12TFContrato_NumeroAta)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV12TFContrato_NumeroAta)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContrato_NumeroAta_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] = @AV13TFContrato_NumeroAta_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] = @AV13TFContrato_NumeroAta_Sel)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! (0==AV14TFContrato_Ano) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV14TFContrato_Ano)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV14TFContrato_Ano)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! (0==AV15TFContrato_Ano_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV15TFContrato_Ano_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV15TFContrato_Ano_To)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV16TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV16TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV17TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV17TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like @lV18TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Docto] like @lV18TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Docto] = @AV19TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Docto] = @AV19TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV20TFContrato_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Valor] >= @AV20TFContrato_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Valor] >= @AV20TFContrato_Valor)";
            }
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFContrato_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Valor] <= @AV21TFContrato_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Valor] <= @AV21TFContrato_Valor_To)";
            }
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( AV22TFContrato_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ativo] = 1)";
            }
         }
         if ( AV22TFContrato_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_NumeroAta]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00UB4( IGxContext context ,
                                             String AV41DynamicFiltersSelector1 ,
                                             String AV43Contrato_Numero1 ,
                                             short AV42DynamicFiltersOperator1 ,
                                             String AV44Contrato_NumeroAta1 ,
                                             String AV45Contratada_PessoaNom1 ,
                                             short AV46Contrato_Ano1 ,
                                             short AV47Contrato_Ano_To1 ,
                                             bool AV48DynamicFiltersEnabled2 ,
                                             String AV49DynamicFiltersSelector2 ,
                                             String AV51Contrato_Numero2 ,
                                             short AV50DynamicFiltersOperator2 ,
                                             String AV52Contrato_NumeroAta2 ,
                                             String AV53Contratada_PessoaNom2 ,
                                             short AV54Contrato_Ano2 ,
                                             short AV55Contrato_Ano_To2 ,
                                             bool AV56DynamicFiltersEnabled3 ,
                                             String AV57DynamicFiltersSelector3 ,
                                             String AV59Contrato_Numero3 ,
                                             short AV58DynamicFiltersOperator3 ,
                                             String AV60Contrato_NumeroAta3 ,
                                             String AV61Contratada_PessoaNom3 ,
                                             short AV62Contrato_Ano3 ,
                                             short AV63Contrato_Ano_To3 ,
                                             String AV11TFContrato_Numero_Sel ,
                                             String AV10TFContrato_Numero ,
                                             String AV13TFContrato_NumeroAta_Sel ,
                                             String AV12TFContrato_NumeroAta ,
                                             short AV14TFContrato_Ano ,
                                             short AV15TFContrato_Ano_To ,
                                             String AV17TFContratada_PessoaNom_Sel ,
                                             String AV16TFContratada_PessoaNom ,
                                             String AV19TFContratada_PessoaCNPJ_Sel ,
                                             String AV18TFContratada_PessoaCNPJ ,
                                             decimal AV20TFContrato_Valor ,
                                             decimal AV21TFContrato_Valor_To ,
                                             short AV22TFContrato_Ativo_Sel ,
                                             String A77Contrato_Numero ,
                                             String A78Contrato_NumeroAta ,
                                             String A41Contratada_PessoaNom ,
                                             short A79Contrato_Ano ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A89Contrato_Valor ,
                                             bool A92Contrato_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [33] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratada_Codigo], T2.[Contratada_PessoaCod] AS Contratada_PessoaCod, T3.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_Ativo], T1.[Contrato_Valor], T3.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contrato_Ano], T1.[Contrato_NumeroAta], T1.[Contrato_Numero], T1.[Contrato_Codigo] FROM (([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Contrato_Numero1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV43Contrato_Numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV43Contrato_Numero1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( AV42DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV44Contrato_NumeroAta1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV44Contrato_NumeroAta1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( AV42DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV42DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Contratada_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV45Contratada_PessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV45Contratada_PessoaNom1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV42DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Contratada_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV45Contratada_PessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV45Contratada_PessoaNom1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV46Contrato_Ano1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV46Contrato_Ano1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV46Contrato_Ano1)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV47Contrato_Ano_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV47Contrato_Ano_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV47Contrato_Ano_To1)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Contrato_Numero2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV51Contrato_Numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV51Contrato_Numero2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( AV50DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Contrato_NumeroAta2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV52Contrato_NumeroAta2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV52Contrato_NumeroAta2)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( AV50DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Contrato_NumeroAta2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV52Contrato_NumeroAta2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV52Contrato_NumeroAta2)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV50DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53Contratada_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV53Contratada_PessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV53Contratada_PessoaNom2)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV50DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53Contratada_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV53Contratada_PessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV53Contratada_PessoaNom2)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV54Contrato_Ano2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV54Contrato_Ano2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV54Contrato_Ano2)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV55Contrato_Ano_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV55Contrato_Ano_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV55Contrato_Ano_To2)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Contrato_Numero3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV59Contrato_Numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV59Contrato_Numero3)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Contrato_NumeroAta3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV60Contrato_NumeroAta3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV60Contrato_NumeroAta3)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Contrato_NumeroAta3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV60Contrato_NumeroAta3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV60Contrato_NumeroAta3)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61Contratada_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV61Contratada_PessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV61Contratada_PessoaNom3)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61Contratada_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV61Contratada_PessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV61Contratada_PessoaNom3)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV62Contrato_Ano3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV62Contrato_Ano3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV62Contrato_Ano3)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV63Contrato_Ano_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV63Contrato_Ano_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV63Contrato_Ano_To3)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContrato_NumeroAta_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContrato_NumeroAta)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV12TFContrato_NumeroAta)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV12TFContrato_NumeroAta)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContrato_NumeroAta_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] = @AV13TFContrato_NumeroAta_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] = @AV13TFContrato_NumeroAta_Sel)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! (0==AV14TFContrato_Ano) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV14TFContrato_Ano)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV14TFContrato_Ano)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( ! (0==AV15TFContrato_Ano_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV15TFContrato_Ano_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV15TFContrato_Ano_To)";
            }
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV16TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV16TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV17TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV17TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like @lV18TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Docto] like @lV18TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Docto] = @AV19TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Docto] = @AV19TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV20TFContrato_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Valor] >= @AV20TFContrato_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Valor] >= @AV20TFContrato_Valor)";
            }
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFContrato_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Valor] <= @AV21TFContrato_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Valor] <= @AV21TFContrato_Valor_To)";
            }
         }
         else
         {
            GXv_int5[32] = 1;
         }
         if ( AV22TFContrato_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ativo] = 1)";
            }
         }
         if ( AV22TFContrato_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00UB5( IGxContext context ,
                                             String AV41DynamicFiltersSelector1 ,
                                             String AV43Contrato_Numero1 ,
                                             short AV42DynamicFiltersOperator1 ,
                                             String AV44Contrato_NumeroAta1 ,
                                             String AV45Contratada_PessoaNom1 ,
                                             short AV46Contrato_Ano1 ,
                                             short AV47Contrato_Ano_To1 ,
                                             bool AV48DynamicFiltersEnabled2 ,
                                             String AV49DynamicFiltersSelector2 ,
                                             String AV51Contrato_Numero2 ,
                                             short AV50DynamicFiltersOperator2 ,
                                             String AV52Contrato_NumeroAta2 ,
                                             String AV53Contratada_PessoaNom2 ,
                                             short AV54Contrato_Ano2 ,
                                             short AV55Contrato_Ano_To2 ,
                                             bool AV56DynamicFiltersEnabled3 ,
                                             String AV57DynamicFiltersSelector3 ,
                                             String AV59Contrato_Numero3 ,
                                             short AV58DynamicFiltersOperator3 ,
                                             String AV60Contrato_NumeroAta3 ,
                                             String AV61Contratada_PessoaNom3 ,
                                             short AV62Contrato_Ano3 ,
                                             short AV63Contrato_Ano_To3 ,
                                             String AV11TFContrato_Numero_Sel ,
                                             String AV10TFContrato_Numero ,
                                             String AV13TFContrato_NumeroAta_Sel ,
                                             String AV12TFContrato_NumeroAta ,
                                             short AV14TFContrato_Ano ,
                                             short AV15TFContrato_Ano_To ,
                                             String AV17TFContratada_PessoaNom_Sel ,
                                             String AV16TFContratada_PessoaNom ,
                                             String AV19TFContratada_PessoaCNPJ_Sel ,
                                             String AV18TFContratada_PessoaCNPJ ,
                                             decimal AV20TFContrato_Valor ,
                                             decimal AV21TFContrato_Valor_To ,
                                             short AV22TFContrato_Ativo_Sel ,
                                             String A77Contrato_Numero ,
                                             String A78Contrato_NumeroAta ,
                                             String A41Contratada_PessoaNom ,
                                             short A79Contrato_Ano ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A89Contrato_Valor ,
                                             bool A92Contrato_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [33] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratada_Codigo], T2.[Contratada_PessoaCod] AS Contratada_PessoaCod, T3.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contrato_Ativo], T1.[Contrato_Valor], T1.[Contrato_Ano], T3.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_NumeroAta], T1.[Contrato_Numero], T1.[Contrato_Codigo] FROM (([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Contrato_Numero1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV43Contrato_Numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV43Contrato_Numero1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( AV42DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV44Contrato_NumeroAta1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV44Contrato_NumeroAta1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( AV42DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta1)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV42DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Contratada_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV45Contratada_PessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV45Contratada_PessoaNom1)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV42DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Contratada_PessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV45Contratada_PessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV45Contratada_PessoaNom1)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV46Contrato_Ano1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV46Contrato_Ano1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV46Contrato_Ano1)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV41DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV47Contrato_Ano_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV47Contrato_Ano_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV47Contrato_Ano_To1)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Contrato_Numero2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV51Contrato_Numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV51Contrato_Numero2)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( AV50DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Contrato_NumeroAta2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV52Contrato_NumeroAta2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV52Contrato_NumeroAta2)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( AV50DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Contrato_NumeroAta2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV52Contrato_NumeroAta2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV52Contrato_NumeroAta2)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV50DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53Contratada_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV53Contratada_PessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV53Contratada_PessoaNom2)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV50DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53Contratada_PessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV53Contratada_PessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV53Contratada_PessoaNom2)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV54Contrato_Ano2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV54Contrato_Ano2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV54Contrato_Ano2)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV55Contrato_Ano_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV55Contrato_Ano_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV55Contrato_Ano_To2)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Contrato_Numero3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV59Contrato_Numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV59Contrato_Numero3)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Contrato_NumeroAta3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV60Contrato_NumeroAta3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV60Contrato_NumeroAta3)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Contrato_NumeroAta3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV60Contrato_NumeroAta3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like '%' + @lV60Contrato_NumeroAta3)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61Contratada_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV61Contratada_PessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV61Contratada_PessoaNom3)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61Contratada_PessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV61Contratada_PessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV61Contratada_PessoaNom3)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV62Contrato_Ano3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV62Contrato_Ano3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV62Contrato_Ano3)";
            }
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV63Contrato_Ano_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV63Contrato_Ano_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV63Contrato_Ano_To3)";
            }
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContrato_NumeroAta_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContrato_NumeroAta)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like @lV12TFContrato_NumeroAta)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] like @lV12TFContrato_NumeroAta)";
            }
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContrato_NumeroAta_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] = @AV13TFContrato_NumeroAta_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_NumeroAta] = @AV13TFContrato_NumeroAta_Sel)";
            }
         }
         else
         {
            GXv_int7[24] = 1;
         }
         if ( ! (0==AV14TFContrato_Ano) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV14TFContrato_Ano)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] >= @AV14TFContrato_Ano)";
            }
         }
         else
         {
            GXv_int7[25] = 1;
         }
         if ( ! (0==AV15TFContrato_Ano_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV15TFContrato_Ano_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ano] <= @AV15TFContrato_Ano_To)";
            }
         }
         else
         {
            GXv_int7[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV16TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV16TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int7[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV17TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV17TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int7[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like @lV18TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Docto] like @lV18TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int7[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Docto] = @AV19TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Docto] = @AV19TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int7[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV20TFContrato_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Valor] >= @AV20TFContrato_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Valor] >= @AV20TFContrato_Valor)";
            }
         }
         else
         {
            GXv_int7[31] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFContrato_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Valor] <= @AV21TFContrato_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Valor] <= @AV21TFContrato_Valor_To)";
            }
         }
         else
         {
            GXv_int7[32] = 1;
         }
         if ( AV22TFContrato_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ativo] = 1)";
            }
         }
         if ( AV22TFContrato_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Docto]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UB2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (short)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (short)dynConstraints[39] , (String)dynConstraints[40] , (decimal)dynConstraints[41] , (bool)dynConstraints[42] );
               case 1 :
                     return conditional_P00UB3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (short)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (short)dynConstraints[39] , (String)dynConstraints[40] , (decimal)dynConstraints[41] , (bool)dynConstraints[42] );
               case 2 :
                     return conditional_P00UB4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (short)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (short)dynConstraints[39] , (String)dynConstraints[40] , (decimal)dynConstraints[41] , (bool)dynConstraints[42] );
               case 3 :
                     return conditional_P00UB5(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (short)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (short)dynConstraints[39] , (String)dynConstraints[40] , (decimal)dynConstraints[41] , (bool)dynConstraints[42] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UB2 ;
          prmP00UB2 = new Object[] {
          new Object[] {"@lV43Contrato_Numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV44Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV44Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV45Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV45Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV46Contrato_Ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV47Contrato_Ano_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV51Contrato_Numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV52Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV52Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV53Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV53Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV54Contrato_Ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV55Contrato_Ano_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV59Contrato_Numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV60Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV60Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV61Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV61Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62Contrato_Ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV63Contrato_Ano_To3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV10TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFContrato_NumeroAta",SqlDbType.Char,10,0} ,
          new Object[] {"@AV13TFContrato_NumeroAta_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV14TFContrato_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV15TFContrato_Ano_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV16TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV17TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV19TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV20TFContrato_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV21TFContrato_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00UB3 ;
          prmP00UB3 = new Object[] {
          new Object[] {"@lV43Contrato_Numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV44Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV44Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV45Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV45Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV46Contrato_Ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV47Contrato_Ano_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV51Contrato_Numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV52Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV52Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV53Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV53Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV54Contrato_Ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV55Contrato_Ano_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV59Contrato_Numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV60Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV60Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV61Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV61Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62Contrato_Ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV63Contrato_Ano_To3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV10TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFContrato_NumeroAta",SqlDbType.Char,10,0} ,
          new Object[] {"@AV13TFContrato_NumeroAta_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV14TFContrato_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV15TFContrato_Ano_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV16TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV17TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV19TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV20TFContrato_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV21TFContrato_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00UB4 ;
          prmP00UB4 = new Object[] {
          new Object[] {"@lV43Contrato_Numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV44Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV44Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV45Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV45Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV46Contrato_Ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV47Contrato_Ano_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV51Contrato_Numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV52Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV52Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV53Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV53Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV54Contrato_Ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV55Contrato_Ano_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV59Contrato_Numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV60Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV60Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV61Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV61Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62Contrato_Ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV63Contrato_Ano_To3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV10TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFContrato_NumeroAta",SqlDbType.Char,10,0} ,
          new Object[] {"@AV13TFContrato_NumeroAta_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV14TFContrato_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV15TFContrato_Ano_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV16TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV17TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV19TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV20TFContrato_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV21TFContrato_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00UB5 ;
          prmP00UB5 = new Object[] {
          new Object[] {"@lV43Contrato_Numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV44Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV44Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV45Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV45Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV46Contrato_Ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV47Contrato_Ano_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV51Contrato_Numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV52Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV52Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV53Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV53Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV54Contrato_Ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV55Contrato_Ano_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV59Contrato_Numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV60Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV60Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV61Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV61Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62Contrato_Ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV63Contrato_Ano_To3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV10TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFContrato_NumeroAta",SqlDbType.Char,10,0} ,
          new Object[] {"@AV13TFContrato_NumeroAta_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV14TFContrato_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV15TFContrato_Ano_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV16TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV17TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV19TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV20TFContrato_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV21TFContrato_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UB2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UB2,100,0,true,false )
             ,new CursorDef("P00UB3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UB3,100,0,true,false )
             ,new CursorDef("P00UB4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UB4,100,0,true,false )
             ,new CursorDef("P00UB5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UB5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((short[]) buf[7])[0] = rslt.getShort(7) ;
                ((String[]) buf[8])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((String[]) buf[10])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((short[]) buf[8])[0] = rslt.getShort(7) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getString(9, 20) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((short[]) buf[8])[0] = rslt.getShort(7) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getString(9, 20) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getString(9, 20) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[46]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[52]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[53]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[64]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[65]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[46]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[52]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[53]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[64]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[65]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[46]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[52]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[53]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[64]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[65]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[46]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[52]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[53]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[64]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[65]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratofilterdata") )
          {
             return  ;
          }
          getpromptcontratofilterdata worker = new getpromptcontratofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
