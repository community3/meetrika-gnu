/*
               File: GetWWRequisitoFilterData
        Description: Get WWRequisito Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:17:31.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwrequisitofilterdata : GXProcedure
   {
      public getwwrequisitofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwrequisitofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV43DDOName = aP0_DDOName;
         this.AV41SearchTxt = aP1_SearchTxt;
         this.AV42SearchTxtTo = aP2_SearchTxtTo;
         this.AV47OptionsJson = "" ;
         this.AV50OptionsDescJson = "" ;
         this.AV52OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV47OptionsJson;
         aP4_OptionsDescJson=this.AV50OptionsDescJson;
         aP5_OptionIndexesJson=this.AV52OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV43DDOName = aP0_DDOName;
         this.AV41SearchTxt = aP1_SearchTxt;
         this.AV42SearchTxtTo = aP2_SearchTxtTo;
         this.AV47OptionsJson = "" ;
         this.AV50OptionsDescJson = "" ;
         this.AV52OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV47OptionsJson;
         aP4_OptionsDescJson=this.AV50OptionsDescJson;
         aP5_OptionIndexesJson=this.AV52OptionIndexesJson;
         return AV52OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwrequisitofilterdata objgetwwrequisitofilterdata;
         objgetwwrequisitofilterdata = new getwwrequisitofilterdata();
         objgetwwrequisitofilterdata.AV43DDOName = aP0_DDOName;
         objgetwwrequisitofilterdata.AV41SearchTxt = aP1_SearchTxt;
         objgetwwrequisitofilterdata.AV42SearchTxtTo = aP2_SearchTxtTo;
         objgetwwrequisitofilterdata.AV47OptionsJson = "" ;
         objgetwwrequisitofilterdata.AV50OptionsDescJson = "" ;
         objgetwwrequisitofilterdata.AV52OptionIndexesJson = "" ;
         objgetwwrequisitofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwrequisitofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwrequisitofilterdata);
         aP3_OptionsJson=this.AV47OptionsJson;
         aP4_OptionsDescJson=this.AV50OptionsDescJson;
         aP5_OptionIndexesJson=this.AV52OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwrequisitofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV46Options = (IGxCollection)(new GxSimpleCollection());
         AV49OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV51OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV43DDOName), "DDO_REQUISITO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADREQUISITO_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV43DDOName), "DDO_PROPOSTA_OBJETIVO") == 0 )
         {
            /* Execute user subroutine: 'LOADPROPOSTA_OBJETIVOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV43DDOName), "DDO_REQUISITO_AGRUPADOR") == 0 )
         {
            /* Execute user subroutine: 'LOADREQUISITO_AGRUPADOROPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV43DDOName), "DDO_REQUISITO_TITULO") == 0 )
         {
            /* Execute user subroutine: 'LOADREQUISITO_TITULOOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV43DDOName), "DDO_REQUISITO_RESTRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADREQUISITO_RESTRICAOOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV47OptionsJson = AV46Options.ToJSonString(false);
         AV50OptionsDescJson = AV49OptionsDesc.ToJSonString(false);
         AV52OptionIndexesJson = AV51OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV54Session.Get("WWRequisitoGridState"), "") == 0 )
         {
            AV56GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWRequisitoGridState"), "");
         }
         else
         {
            AV56GridState.FromXml(AV54Session.Get("WWRequisitoGridState"), "");
         }
         AV74GXV1 = 1;
         while ( AV74GXV1 <= AV56GridState.gxTpr_Filtervalues.Count )
         {
            AV57GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV56GridState.gxTpr_Filtervalues.Item(AV74GXV1));
            if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_CODIGO") == 0 )
            {
               AV10TFRequisito_Codigo = (int)(NumberUtil.Val( AV57GridStateFilterValue.gxTpr_Value, "."));
               AV11TFRequisito_Codigo_To = (int)(NumberUtil.Val( AV57GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_DESCRICAO") == 0 )
            {
               AV12TFRequisito_Descricao = AV57GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_DESCRICAO_SEL") == 0 )
            {
               AV13TFRequisito_Descricao_Sel = AV57GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFPROPOSTA_CODIGO") == 0 )
            {
               AV14TFProposta_Codigo = (int)(NumberUtil.Val( AV57GridStateFilterValue.gxTpr_Value, "."));
               AV15TFProposta_Codigo_To = (int)(NumberUtil.Val( AV57GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFPROPOSTA_OBJETIVO") == 0 )
            {
               AV16TFProposta_Objetivo = AV57GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFPROPOSTA_OBJETIVO_SEL") == 0 )
            {
               AV17TFProposta_Objetivo_Sel = AV57GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_TIPOREQCOD") == 0 )
            {
               AV70TFRequisito_TipoReqCod = (int)(NumberUtil.Val( AV57GridStateFilterValue.gxTpr_Value, "."));
               AV71TFRequisito_TipoReqCod_To = (int)(NumberUtil.Val( AV57GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_AGRUPADOR") == 0 )
            {
               AV22TFRequisito_Agrupador = AV57GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_AGRUPADOR_SEL") == 0 )
            {
               AV23TFRequisito_Agrupador_Sel = AV57GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_TITULO") == 0 )
            {
               AV24TFRequisito_Titulo = AV57GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_TITULO_SEL") == 0 )
            {
               AV25TFRequisito_Titulo_Sel = AV57GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_RESTRICAO") == 0 )
            {
               AV28TFRequisito_Restricao = AV57GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_RESTRICAO_SEL") == 0 )
            {
               AV29TFRequisito_Restricao_Sel = AV57GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_ORDEM") == 0 )
            {
               AV32TFRequisito_Ordem = (short)(NumberUtil.Val( AV57GridStateFilterValue.gxTpr_Value, "."));
               AV33TFRequisito_Ordem_To = (short)(NumberUtil.Val( AV57GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_PONTUACAO") == 0 )
            {
               AV34TFRequisito_Pontuacao = NumberUtil.Val( AV57GridStateFilterValue.gxTpr_Value, ".");
               AV35TFRequisito_Pontuacao_To = NumberUtil.Val( AV57GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_DATAHOMOLOGACAO") == 0 )
            {
               AV36TFRequisito_DataHomologacao = context.localUtil.CToD( AV57GridStateFilterValue.gxTpr_Value, 2);
               AV37TFRequisito_DataHomologacao_To = context.localUtil.CToD( AV57GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_STATUS_SEL") == 0 )
            {
               AV38TFRequisito_Status_SelsJson = AV57GridStateFilterValue.gxTpr_Value;
               AV39TFRequisito_Status_Sels.FromJSonString(AV38TFRequisito_Status_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV57GridStateFilterValue.gxTpr_Name, "TFREQUISITO_ATIVO_SEL") == 0 )
            {
               AV40TFRequisito_Ativo_Sel = (short)(NumberUtil.Val( AV57GridStateFilterValue.gxTpr_Value, "."));
            }
            AV74GXV1 = (int)(AV74GXV1+1);
         }
         if ( AV56GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV58GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV56GridState.gxTpr_Dynamicfilters.Item(1));
            AV59DynamicFiltersSelector1 = AV58GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV59DynamicFiltersSelector1, "REQUISITO_DESCRICAO") == 0 )
            {
               AV60DynamicFiltersOperator1 = AV58GridStateDynamicFilter.gxTpr_Operator;
               AV61Requisito_Descricao1 = AV58GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV56GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV62DynamicFiltersEnabled2 = true;
               AV58GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV56GridState.gxTpr_Dynamicfilters.Item(2));
               AV63DynamicFiltersSelector2 = AV58GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV63DynamicFiltersSelector2, "REQUISITO_DESCRICAO") == 0 )
               {
                  AV64DynamicFiltersOperator2 = AV58GridStateDynamicFilter.gxTpr_Operator;
                  AV65Requisito_Descricao2 = AV58GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV56GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV66DynamicFiltersEnabled3 = true;
                  AV58GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV56GridState.gxTpr_Dynamicfilters.Item(3));
                  AV67DynamicFiltersSelector3 = AV58GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV67DynamicFiltersSelector3, "REQUISITO_DESCRICAO") == 0 )
                  {
                     AV68DynamicFiltersOperator3 = AV58GridStateDynamicFilter.gxTpr_Operator;
                     AV69Requisito_Descricao3 = AV58GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADREQUISITO_DESCRICAOOPTIONS' Routine */
         AV12TFRequisito_Descricao = AV41SearchTxt;
         AV13TFRequisito_Descricao_Sel = "";
         AV76WWRequisitoDS_1_Dynamicfiltersselector1 = AV59DynamicFiltersSelector1;
         AV77WWRequisitoDS_2_Dynamicfiltersoperator1 = AV60DynamicFiltersOperator1;
         AV78WWRequisitoDS_3_Requisito_descricao1 = AV61Requisito_Descricao1;
         AV79WWRequisitoDS_4_Dynamicfiltersenabled2 = AV62DynamicFiltersEnabled2;
         AV80WWRequisitoDS_5_Dynamicfiltersselector2 = AV63DynamicFiltersSelector2;
         AV81WWRequisitoDS_6_Dynamicfiltersoperator2 = AV64DynamicFiltersOperator2;
         AV82WWRequisitoDS_7_Requisito_descricao2 = AV65Requisito_Descricao2;
         AV83WWRequisitoDS_8_Dynamicfiltersenabled3 = AV66DynamicFiltersEnabled3;
         AV84WWRequisitoDS_9_Dynamicfiltersselector3 = AV67DynamicFiltersSelector3;
         AV85WWRequisitoDS_10_Dynamicfiltersoperator3 = AV68DynamicFiltersOperator3;
         AV86WWRequisitoDS_11_Requisito_descricao3 = AV69Requisito_Descricao3;
         AV87WWRequisitoDS_12_Tfrequisito_codigo = AV10TFRequisito_Codigo;
         AV88WWRequisitoDS_13_Tfrequisito_codigo_to = AV11TFRequisito_Codigo_To;
         AV89WWRequisitoDS_14_Tfrequisito_descricao = AV12TFRequisito_Descricao;
         AV90WWRequisitoDS_15_Tfrequisito_descricao_sel = AV13TFRequisito_Descricao_Sel;
         AV91WWRequisitoDS_16_Tfproposta_codigo = AV14TFProposta_Codigo;
         AV92WWRequisitoDS_17_Tfproposta_codigo_to = AV15TFProposta_Codigo_To;
         AV93WWRequisitoDS_18_Tfproposta_objetivo = AV16TFProposta_Objetivo;
         AV94WWRequisitoDS_19_Tfproposta_objetivo_sel = AV17TFProposta_Objetivo_Sel;
         AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod = AV70TFRequisito_TipoReqCod;
         AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to = AV71TFRequisito_TipoReqCod_To;
         AV97WWRequisitoDS_22_Tfrequisito_agrupador = AV22TFRequisito_Agrupador;
         AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel = AV23TFRequisito_Agrupador_Sel;
         AV99WWRequisitoDS_24_Tfrequisito_titulo = AV24TFRequisito_Titulo;
         AV100WWRequisitoDS_25_Tfrequisito_titulo_sel = AV25TFRequisito_Titulo_Sel;
         AV101WWRequisitoDS_26_Tfrequisito_restricao = AV28TFRequisito_Restricao;
         AV102WWRequisitoDS_27_Tfrequisito_restricao_sel = AV29TFRequisito_Restricao_Sel;
         AV103WWRequisitoDS_28_Tfrequisito_ordem = AV32TFRequisito_Ordem;
         AV104WWRequisitoDS_29_Tfrequisito_ordem_to = AV33TFRequisito_Ordem_To;
         AV105WWRequisitoDS_30_Tfrequisito_pontuacao = AV34TFRequisito_Pontuacao;
         AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to = AV35TFRequisito_Pontuacao_To;
         AV107WWRequisitoDS_32_Tfrequisito_datahomologacao = AV36TFRequisito_DataHomologacao;
         AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to = AV37TFRequisito_DataHomologacao_To;
         AV109WWRequisitoDS_34_Tfrequisito_status_sels = AV39TFRequisito_Status_Sels;
         AV110WWRequisitoDS_35_Tfrequisito_ativo_sel = AV40TFRequisito_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1934Requisito_Status ,
                                              AV109WWRequisitoDS_34_Tfrequisito_status_sels ,
                                              AV76WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                              AV77WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                              AV78WWRequisitoDS_3_Requisito_descricao1 ,
                                              AV79WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                              AV80WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                              AV81WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                              AV82WWRequisitoDS_7_Requisito_descricao2 ,
                                              AV83WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                              AV84WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                              AV85WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                              AV86WWRequisitoDS_11_Requisito_descricao3 ,
                                              AV87WWRequisitoDS_12_Tfrequisito_codigo ,
                                              AV88WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                              AV90WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                              AV89WWRequisitoDS_14_Tfrequisito_descricao ,
                                              AV91WWRequisitoDS_16_Tfproposta_codigo ,
                                              AV92WWRequisitoDS_17_Tfproposta_codigo_to ,
                                              AV94WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                              AV93WWRequisitoDS_18_Tfproposta_objetivo ,
                                              AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                              AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                              AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                              AV97WWRequisitoDS_22_Tfrequisito_agrupador ,
                                              AV100WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                              AV99WWRequisitoDS_24_Tfrequisito_titulo ,
                                              AV102WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                              AV101WWRequisitoDS_26_Tfrequisito_restricao ,
                                              AV103WWRequisitoDS_28_Tfrequisito_ordem ,
                                              AV104WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                              AV105WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                              AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                              AV107WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                              AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                              AV109WWRequisitoDS_34_Tfrequisito_status_sels.Count ,
                                              AV110WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                              A1923Requisito_Descricao ,
                                              A1919Requisito_Codigo ,
                                              A1685Proposta_Codigo ,
                                              A1690Proposta_Objetivo ,
                                              A2049Requisito_TipoReqCod ,
                                              A1926Requisito_Agrupador ,
                                              A1927Requisito_Titulo ,
                                              A1929Requisito_Restricao ,
                                              A1931Requisito_Ordem ,
                                              A1932Requisito_Pontuacao ,
                                              A1933Requisito_DataHomologacao ,
                                              A1935Requisito_Ativo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV78WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1), "%", "");
         lV78WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1), "%", "");
         lV82WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2), "%", "");
         lV82WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2), "%", "");
         lV86WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3), "%", "");
         lV86WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3), "%", "");
         lV89WWRequisitoDS_14_Tfrequisito_descricao = StringUtil.Concat( StringUtil.RTrim( AV89WWRequisitoDS_14_Tfrequisito_descricao), "%", "");
         lV93WWRequisitoDS_18_Tfproposta_objetivo = StringUtil.Concat( StringUtil.RTrim( AV93WWRequisitoDS_18_Tfproposta_objetivo), "%", "");
         lV97WWRequisitoDS_22_Tfrequisito_agrupador = StringUtil.Concat( StringUtil.RTrim( AV97WWRequisitoDS_22_Tfrequisito_agrupador), "%", "");
         lV99WWRequisitoDS_24_Tfrequisito_titulo = StringUtil.Concat( StringUtil.RTrim( AV99WWRequisitoDS_24_Tfrequisito_titulo), "%", "");
         lV101WWRequisitoDS_26_Tfrequisito_restricao = StringUtil.Concat( StringUtil.RTrim( AV101WWRequisitoDS_26_Tfrequisito_restricao), "%", "");
         /* Using cursor P00UV2 */
         pr_default.execute(0, new Object[] {lV78WWRequisitoDS_3_Requisito_descricao1, lV78WWRequisitoDS_3_Requisito_descricao1, lV82WWRequisitoDS_7_Requisito_descricao2, lV82WWRequisitoDS_7_Requisito_descricao2, lV86WWRequisitoDS_11_Requisito_descricao3, lV86WWRequisitoDS_11_Requisito_descricao3, AV87WWRequisitoDS_12_Tfrequisito_codigo, AV88WWRequisitoDS_13_Tfrequisito_codigo_to, lV89WWRequisitoDS_14_Tfrequisito_descricao, AV90WWRequisitoDS_15_Tfrequisito_descricao_sel, AV91WWRequisitoDS_16_Tfproposta_codigo, AV92WWRequisitoDS_17_Tfproposta_codigo_to, lV93WWRequisitoDS_18_Tfproposta_objetivo, AV94WWRequisitoDS_19_Tfproposta_objetivo_sel, AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod, AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to, lV97WWRequisitoDS_22_Tfrequisito_agrupador, AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel, lV99WWRequisitoDS_24_Tfrequisito_titulo, AV100WWRequisitoDS_25_Tfrequisito_titulo_sel, lV101WWRequisitoDS_26_Tfrequisito_restricao, AV102WWRequisitoDS_27_Tfrequisito_restricao_sel, AV103WWRequisitoDS_28_Tfrequisito_ordem, AV104WWRequisitoDS_29_Tfrequisito_ordem_to, AV105WWRequisitoDS_30_Tfrequisito_pontuacao, AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to, AV107WWRequisitoDS_32_Tfrequisito_datahomologacao, AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUV2 = false;
            A1923Requisito_Descricao = P00UV2_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = P00UV2_n1923Requisito_Descricao[0];
            A1935Requisito_Ativo = P00UV2_A1935Requisito_Ativo[0];
            A1934Requisito_Status = P00UV2_A1934Requisito_Status[0];
            A1933Requisito_DataHomologacao = P00UV2_A1933Requisito_DataHomologacao[0];
            n1933Requisito_DataHomologacao = P00UV2_n1933Requisito_DataHomologacao[0];
            A1932Requisito_Pontuacao = P00UV2_A1932Requisito_Pontuacao[0];
            n1932Requisito_Pontuacao = P00UV2_n1932Requisito_Pontuacao[0];
            A1931Requisito_Ordem = P00UV2_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = P00UV2_n1931Requisito_Ordem[0];
            A1929Requisito_Restricao = P00UV2_A1929Requisito_Restricao[0];
            n1929Requisito_Restricao = P00UV2_n1929Requisito_Restricao[0];
            A1927Requisito_Titulo = P00UV2_A1927Requisito_Titulo[0];
            n1927Requisito_Titulo = P00UV2_n1927Requisito_Titulo[0];
            A1926Requisito_Agrupador = P00UV2_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = P00UV2_n1926Requisito_Agrupador[0];
            A2049Requisito_TipoReqCod = P00UV2_A2049Requisito_TipoReqCod[0];
            n2049Requisito_TipoReqCod = P00UV2_n2049Requisito_TipoReqCod[0];
            A1690Proposta_Objetivo = P00UV2_A1690Proposta_Objetivo[0];
            A1685Proposta_Codigo = P00UV2_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = P00UV2_n1685Proposta_Codigo[0];
            A1919Requisito_Codigo = P00UV2_A1919Requisito_Codigo[0];
            A1690Proposta_Objetivo = P00UV2_A1690Proposta_Objetivo[0];
            AV53count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00UV2_A1923Requisito_Descricao[0], A1923Requisito_Descricao) == 0 ) )
            {
               BRKUV2 = false;
               A1919Requisito_Codigo = P00UV2_A1919Requisito_Codigo[0];
               AV53count = (long)(AV53count+1);
               BRKUV2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1923Requisito_Descricao)) )
            {
               AV45Option = A1923Requisito_Descricao;
               AV46Options.Add(AV45Option, 0);
               AV51OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV53count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV46Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUV2 )
            {
               BRKUV2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADPROPOSTA_OBJETIVOOPTIONS' Routine */
         AV16TFProposta_Objetivo = AV41SearchTxt;
         AV17TFProposta_Objetivo_Sel = "";
         AV76WWRequisitoDS_1_Dynamicfiltersselector1 = AV59DynamicFiltersSelector1;
         AV77WWRequisitoDS_2_Dynamicfiltersoperator1 = AV60DynamicFiltersOperator1;
         AV78WWRequisitoDS_3_Requisito_descricao1 = AV61Requisito_Descricao1;
         AV79WWRequisitoDS_4_Dynamicfiltersenabled2 = AV62DynamicFiltersEnabled2;
         AV80WWRequisitoDS_5_Dynamicfiltersselector2 = AV63DynamicFiltersSelector2;
         AV81WWRequisitoDS_6_Dynamicfiltersoperator2 = AV64DynamicFiltersOperator2;
         AV82WWRequisitoDS_7_Requisito_descricao2 = AV65Requisito_Descricao2;
         AV83WWRequisitoDS_8_Dynamicfiltersenabled3 = AV66DynamicFiltersEnabled3;
         AV84WWRequisitoDS_9_Dynamicfiltersselector3 = AV67DynamicFiltersSelector3;
         AV85WWRequisitoDS_10_Dynamicfiltersoperator3 = AV68DynamicFiltersOperator3;
         AV86WWRequisitoDS_11_Requisito_descricao3 = AV69Requisito_Descricao3;
         AV87WWRequisitoDS_12_Tfrequisito_codigo = AV10TFRequisito_Codigo;
         AV88WWRequisitoDS_13_Tfrequisito_codigo_to = AV11TFRequisito_Codigo_To;
         AV89WWRequisitoDS_14_Tfrequisito_descricao = AV12TFRequisito_Descricao;
         AV90WWRequisitoDS_15_Tfrequisito_descricao_sel = AV13TFRequisito_Descricao_Sel;
         AV91WWRequisitoDS_16_Tfproposta_codigo = AV14TFProposta_Codigo;
         AV92WWRequisitoDS_17_Tfproposta_codigo_to = AV15TFProposta_Codigo_To;
         AV93WWRequisitoDS_18_Tfproposta_objetivo = AV16TFProposta_Objetivo;
         AV94WWRequisitoDS_19_Tfproposta_objetivo_sel = AV17TFProposta_Objetivo_Sel;
         AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod = AV70TFRequisito_TipoReqCod;
         AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to = AV71TFRequisito_TipoReqCod_To;
         AV97WWRequisitoDS_22_Tfrequisito_agrupador = AV22TFRequisito_Agrupador;
         AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel = AV23TFRequisito_Agrupador_Sel;
         AV99WWRequisitoDS_24_Tfrequisito_titulo = AV24TFRequisito_Titulo;
         AV100WWRequisitoDS_25_Tfrequisito_titulo_sel = AV25TFRequisito_Titulo_Sel;
         AV101WWRequisitoDS_26_Tfrequisito_restricao = AV28TFRequisito_Restricao;
         AV102WWRequisitoDS_27_Tfrequisito_restricao_sel = AV29TFRequisito_Restricao_Sel;
         AV103WWRequisitoDS_28_Tfrequisito_ordem = AV32TFRequisito_Ordem;
         AV104WWRequisitoDS_29_Tfrequisito_ordem_to = AV33TFRequisito_Ordem_To;
         AV105WWRequisitoDS_30_Tfrequisito_pontuacao = AV34TFRequisito_Pontuacao;
         AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to = AV35TFRequisito_Pontuacao_To;
         AV107WWRequisitoDS_32_Tfrequisito_datahomologacao = AV36TFRequisito_DataHomologacao;
         AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to = AV37TFRequisito_DataHomologacao_To;
         AV109WWRequisitoDS_34_Tfrequisito_status_sels = AV39TFRequisito_Status_Sels;
         AV110WWRequisitoDS_35_Tfrequisito_ativo_sel = AV40TFRequisito_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1934Requisito_Status ,
                                              AV109WWRequisitoDS_34_Tfrequisito_status_sels ,
                                              AV76WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                              AV77WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                              AV78WWRequisitoDS_3_Requisito_descricao1 ,
                                              AV79WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                              AV80WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                              AV81WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                              AV82WWRequisitoDS_7_Requisito_descricao2 ,
                                              AV83WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                              AV84WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                              AV85WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                              AV86WWRequisitoDS_11_Requisito_descricao3 ,
                                              AV87WWRequisitoDS_12_Tfrequisito_codigo ,
                                              AV88WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                              AV90WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                              AV89WWRequisitoDS_14_Tfrequisito_descricao ,
                                              AV91WWRequisitoDS_16_Tfproposta_codigo ,
                                              AV92WWRequisitoDS_17_Tfproposta_codigo_to ,
                                              AV94WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                              AV93WWRequisitoDS_18_Tfproposta_objetivo ,
                                              AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                              AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                              AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                              AV97WWRequisitoDS_22_Tfrequisito_agrupador ,
                                              AV100WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                              AV99WWRequisitoDS_24_Tfrequisito_titulo ,
                                              AV102WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                              AV101WWRequisitoDS_26_Tfrequisito_restricao ,
                                              AV103WWRequisitoDS_28_Tfrequisito_ordem ,
                                              AV104WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                              AV105WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                              AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                              AV107WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                              AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                              AV109WWRequisitoDS_34_Tfrequisito_status_sels.Count ,
                                              AV110WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                              A1923Requisito_Descricao ,
                                              A1919Requisito_Codigo ,
                                              A1685Proposta_Codigo ,
                                              A1690Proposta_Objetivo ,
                                              A2049Requisito_TipoReqCod ,
                                              A1926Requisito_Agrupador ,
                                              A1927Requisito_Titulo ,
                                              A1929Requisito_Restricao ,
                                              A1931Requisito_Ordem ,
                                              A1932Requisito_Pontuacao ,
                                              A1933Requisito_DataHomologacao ,
                                              A1935Requisito_Ativo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV78WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1), "%", "");
         lV78WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1), "%", "");
         lV82WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2), "%", "");
         lV82WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2), "%", "");
         lV86WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3), "%", "");
         lV86WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3), "%", "");
         lV89WWRequisitoDS_14_Tfrequisito_descricao = StringUtil.Concat( StringUtil.RTrim( AV89WWRequisitoDS_14_Tfrequisito_descricao), "%", "");
         lV93WWRequisitoDS_18_Tfproposta_objetivo = StringUtil.Concat( StringUtil.RTrim( AV93WWRequisitoDS_18_Tfproposta_objetivo), "%", "");
         lV97WWRequisitoDS_22_Tfrequisito_agrupador = StringUtil.Concat( StringUtil.RTrim( AV97WWRequisitoDS_22_Tfrequisito_agrupador), "%", "");
         lV99WWRequisitoDS_24_Tfrequisito_titulo = StringUtil.Concat( StringUtil.RTrim( AV99WWRequisitoDS_24_Tfrequisito_titulo), "%", "");
         lV101WWRequisitoDS_26_Tfrequisito_restricao = StringUtil.Concat( StringUtil.RTrim( AV101WWRequisitoDS_26_Tfrequisito_restricao), "%", "");
         /* Using cursor P00UV3 */
         pr_default.execute(1, new Object[] {lV78WWRequisitoDS_3_Requisito_descricao1, lV78WWRequisitoDS_3_Requisito_descricao1, lV82WWRequisitoDS_7_Requisito_descricao2, lV82WWRequisitoDS_7_Requisito_descricao2, lV86WWRequisitoDS_11_Requisito_descricao3, lV86WWRequisitoDS_11_Requisito_descricao3, AV87WWRequisitoDS_12_Tfrequisito_codigo, AV88WWRequisitoDS_13_Tfrequisito_codigo_to, lV89WWRequisitoDS_14_Tfrequisito_descricao, AV90WWRequisitoDS_15_Tfrequisito_descricao_sel, AV91WWRequisitoDS_16_Tfproposta_codigo, AV92WWRequisitoDS_17_Tfproposta_codigo_to, lV93WWRequisitoDS_18_Tfproposta_objetivo, AV94WWRequisitoDS_19_Tfproposta_objetivo_sel, AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod, AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to, lV97WWRequisitoDS_22_Tfrequisito_agrupador, AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel, lV99WWRequisitoDS_24_Tfrequisito_titulo, AV100WWRequisitoDS_25_Tfrequisito_titulo_sel, lV101WWRequisitoDS_26_Tfrequisito_restricao, AV102WWRequisitoDS_27_Tfrequisito_restricao_sel, AV103WWRequisitoDS_28_Tfrequisito_ordem, AV104WWRequisitoDS_29_Tfrequisito_ordem_to, AV105WWRequisitoDS_30_Tfrequisito_pontuacao, AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to, AV107WWRequisitoDS_32_Tfrequisito_datahomologacao, AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUV4 = false;
            A1690Proposta_Objetivo = P00UV3_A1690Proposta_Objetivo[0];
            A1935Requisito_Ativo = P00UV3_A1935Requisito_Ativo[0];
            A1934Requisito_Status = P00UV3_A1934Requisito_Status[0];
            A1933Requisito_DataHomologacao = P00UV3_A1933Requisito_DataHomologacao[0];
            n1933Requisito_DataHomologacao = P00UV3_n1933Requisito_DataHomologacao[0];
            A1932Requisito_Pontuacao = P00UV3_A1932Requisito_Pontuacao[0];
            n1932Requisito_Pontuacao = P00UV3_n1932Requisito_Pontuacao[0];
            A1931Requisito_Ordem = P00UV3_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = P00UV3_n1931Requisito_Ordem[0];
            A1929Requisito_Restricao = P00UV3_A1929Requisito_Restricao[0];
            n1929Requisito_Restricao = P00UV3_n1929Requisito_Restricao[0];
            A1927Requisito_Titulo = P00UV3_A1927Requisito_Titulo[0];
            n1927Requisito_Titulo = P00UV3_n1927Requisito_Titulo[0];
            A1926Requisito_Agrupador = P00UV3_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = P00UV3_n1926Requisito_Agrupador[0];
            A2049Requisito_TipoReqCod = P00UV3_A2049Requisito_TipoReqCod[0];
            n2049Requisito_TipoReqCod = P00UV3_n2049Requisito_TipoReqCod[0];
            A1685Proposta_Codigo = P00UV3_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = P00UV3_n1685Proposta_Codigo[0];
            A1919Requisito_Codigo = P00UV3_A1919Requisito_Codigo[0];
            A1923Requisito_Descricao = P00UV3_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = P00UV3_n1923Requisito_Descricao[0];
            A1690Proposta_Objetivo = P00UV3_A1690Proposta_Objetivo[0];
            AV53count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00UV3_A1690Proposta_Objetivo[0], A1690Proposta_Objetivo) == 0 ) )
            {
               BRKUV4 = false;
               A1685Proposta_Codigo = P00UV3_A1685Proposta_Codigo[0];
               n1685Proposta_Codigo = P00UV3_n1685Proposta_Codigo[0];
               A1919Requisito_Codigo = P00UV3_A1919Requisito_Codigo[0];
               AV53count = (long)(AV53count+1);
               BRKUV4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1690Proposta_Objetivo)) )
            {
               AV45Option = A1690Proposta_Objetivo;
               AV46Options.Add(AV45Option, 0);
               AV51OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV53count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV46Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUV4 )
            {
               BRKUV4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADREQUISITO_AGRUPADOROPTIONS' Routine */
         AV22TFRequisito_Agrupador = AV41SearchTxt;
         AV23TFRequisito_Agrupador_Sel = "";
         AV76WWRequisitoDS_1_Dynamicfiltersselector1 = AV59DynamicFiltersSelector1;
         AV77WWRequisitoDS_2_Dynamicfiltersoperator1 = AV60DynamicFiltersOperator1;
         AV78WWRequisitoDS_3_Requisito_descricao1 = AV61Requisito_Descricao1;
         AV79WWRequisitoDS_4_Dynamicfiltersenabled2 = AV62DynamicFiltersEnabled2;
         AV80WWRequisitoDS_5_Dynamicfiltersselector2 = AV63DynamicFiltersSelector2;
         AV81WWRequisitoDS_6_Dynamicfiltersoperator2 = AV64DynamicFiltersOperator2;
         AV82WWRequisitoDS_7_Requisito_descricao2 = AV65Requisito_Descricao2;
         AV83WWRequisitoDS_8_Dynamicfiltersenabled3 = AV66DynamicFiltersEnabled3;
         AV84WWRequisitoDS_9_Dynamicfiltersselector3 = AV67DynamicFiltersSelector3;
         AV85WWRequisitoDS_10_Dynamicfiltersoperator3 = AV68DynamicFiltersOperator3;
         AV86WWRequisitoDS_11_Requisito_descricao3 = AV69Requisito_Descricao3;
         AV87WWRequisitoDS_12_Tfrequisito_codigo = AV10TFRequisito_Codigo;
         AV88WWRequisitoDS_13_Tfrequisito_codigo_to = AV11TFRequisito_Codigo_To;
         AV89WWRequisitoDS_14_Tfrequisito_descricao = AV12TFRequisito_Descricao;
         AV90WWRequisitoDS_15_Tfrequisito_descricao_sel = AV13TFRequisito_Descricao_Sel;
         AV91WWRequisitoDS_16_Tfproposta_codigo = AV14TFProposta_Codigo;
         AV92WWRequisitoDS_17_Tfproposta_codigo_to = AV15TFProposta_Codigo_To;
         AV93WWRequisitoDS_18_Tfproposta_objetivo = AV16TFProposta_Objetivo;
         AV94WWRequisitoDS_19_Tfproposta_objetivo_sel = AV17TFProposta_Objetivo_Sel;
         AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod = AV70TFRequisito_TipoReqCod;
         AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to = AV71TFRequisito_TipoReqCod_To;
         AV97WWRequisitoDS_22_Tfrequisito_agrupador = AV22TFRequisito_Agrupador;
         AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel = AV23TFRequisito_Agrupador_Sel;
         AV99WWRequisitoDS_24_Tfrequisito_titulo = AV24TFRequisito_Titulo;
         AV100WWRequisitoDS_25_Tfrequisito_titulo_sel = AV25TFRequisito_Titulo_Sel;
         AV101WWRequisitoDS_26_Tfrequisito_restricao = AV28TFRequisito_Restricao;
         AV102WWRequisitoDS_27_Tfrequisito_restricao_sel = AV29TFRequisito_Restricao_Sel;
         AV103WWRequisitoDS_28_Tfrequisito_ordem = AV32TFRequisito_Ordem;
         AV104WWRequisitoDS_29_Tfrequisito_ordem_to = AV33TFRequisito_Ordem_To;
         AV105WWRequisitoDS_30_Tfrequisito_pontuacao = AV34TFRequisito_Pontuacao;
         AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to = AV35TFRequisito_Pontuacao_To;
         AV107WWRequisitoDS_32_Tfrequisito_datahomologacao = AV36TFRequisito_DataHomologacao;
         AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to = AV37TFRequisito_DataHomologacao_To;
         AV109WWRequisitoDS_34_Tfrequisito_status_sels = AV39TFRequisito_Status_Sels;
         AV110WWRequisitoDS_35_Tfrequisito_ativo_sel = AV40TFRequisito_Ativo_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A1934Requisito_Status ,
                                              AV109WWRequisitoDS_34_Tfrequisito_status_sels ,
                                              AV76WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                              AV77WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                              AV78WWRequisitoDS_3_Requisito_descricao1 ,
                                              AV79WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                              AV80WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                              AV81WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                              AV82WWRequisitoDS_7_Requisito_descricao2 ,
                                              AV83WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                              AV84WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                              AV85WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                              AV86WWRequisitoDS_11_Requisito_descricao3 ,
                                              AV87WWRequisitoDS_12_Tfrequisito_codigo ,
                                              AV88WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                              AV90WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                              AV89WWRequisitoDS_14_Tfrequisito_descricao ,
                                              AV91WWRequisitoDS_16_Tfproposta_codigo ,
                                              AV92WWRequisitoDS_17_Tfproposta_codigo_to ,
                                              AV94WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                              AV93WWRequisitoDS_18_Tfproposta_objetivo ,
                                              AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                              AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                              AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                              AV97WWRequisitoDS_22_Tfrequisito_agrupador ,
                                              AV100WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                              AV99WWRequisitoDS_24_Tfrequisito_titulo ,
                                              AV102WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                              AV101WWRequisitoDS_26_Tfrequisito_restricao ,
                                              AV103WWRequisitoDS_28_Tfrequisito_ordem ,
                                              AV104WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                              AV105WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                              AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                              AV107WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                              AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                              AV109WWRequisitoDS_34_Tfrequisito_status_sels.Count ,
                                              AV110WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                              A1923Requisito_Descricao ,
                                              A1919Requisito_Codigo ,
                                              A1685Proposta_Codigo ,
                                              A1690Proposta_Objetivo ,
                                              A2049Requisito_TipoReqCod ,
                                              A1926Requisito_Agrupador ,
                                              A1927Requisito_Titulo ,
                                              A1929Requisito_Restricao ,
                                              A1931Requisito_Ordem ,
                                              A1932Requisito_Pontuacao ,
                                              A1933Requisito_DataHomologacao ,
                                              A1935Requisito_Ativo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV78WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1), "%", "");
         lV78WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1), "%", "");
         lV82WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2), "%", "");
         lV82WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2), "%", "");
         lV86WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3), "%", "");
         lV86WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3), "%", "");
         lV89WWRequisitoDS_14_Tfrequisito_descricao = StringUtil.Concat( StringUtil.RTrim( AV89WWRequisitoDS_14_Tfrequisito_descricao), "%", "");
         lV93WWRequisitoDS_18_Tfproposta_objetivo = StringUtil.Concat( StringUtil.RTrim( AV93WWRequisitoDS_18_Tfproposta_objetivo), "%", "");
         lV97WWRequisitoDS_22_Tfrequisito_agrupador = StringUtil.Concat( StringUtil.RTrim( AV97WWRequisitoDS_22_Tfrequisito_agrupador), "%", "");
         lV99WWRequisitoDS_24_Tfrequisito_titulo = StringUtil.Concat( StringUtil.RTrim( AV99WWRequisitoDS_24_Tfrequisito_titulo), "%", "");
         lV101WWRequisitoDS_26_Tfrequisito_restricao = StringUtil.Concat( StringUtil.RTrim( AV101WWRequisitoDS_26_Tfrequisito_restricao), "%", "");
         /* Using cursor P00UV4 */
         pr_default.execute(2, new Object[] {lV78WWRequisitoDS_3_Requisito_descricao1, lV78WWRequisitoDS_3_Requisito_descricao1, lV82WWRequisitoDS_7_Requisito_descricao2, lV82WWRequisitoDS_7_Requisito_descricao2, lV86WWRequisitoDS_11_Requisito_descricao3, lV86WWRequisitoDS_11_Requisito_descricao3, AV87WWRequisitoDS_12_Tfrequisito_codigo, AV88WWRequisitoDS_13_Tfrequisito_codigo_to, lV89WWRequisitoDS_14_Tfrequisito_descricao, AV90WWRequisitoDS_15_Tfrequisito_descricao_sel, AV91WWRequisitoDS_16_Tfproposta_codigo, AV92WWRequisitoDS_17_Tfproposta_codigo_to, lV93WWRequisitoDS_18_Tfproposta_objetivo, AV94WWRequisitoDS_19_Tfproposta_objetivo_sel, AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod, AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to, lV97WWRequisitoDS_22_Tfrequisito_agrupador, AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel, lV99WWRequisitoDS_24_Tfrequisito_titulo, AV100WWRequisitoDS_25_Tfrequisito_titulo_sel, lV101WWRequisitoDS_26_Tfrequisito_restricao, AV102WWRequisitoDS_27_Tfrequisito_restricao_sel, AV103WWRequisitoDS_28_Tfrequisito_ordem, AV104WWRequisitoDS_29_Tfrequisito_ordem_to, AV105WWRequisitoDS_30_Tfrequisito_pontuacao, AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to, AV107WWRequisitoDS_32_Tfrequisito_datahomologacao, AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKUV6 = false;
            A1926Requisito_Agrupador = P00UV4_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = P00UV4_n1926Requisito_Agrupador[0];
            A1935Requisito_Ativo = P00UV4_A1935Requisito_Ativo[0];
            A1934Requisito_Status = P00UV4_A1934Requisito_Status[0];
            A1933Requisito_DataHomologacao = P00UV4_A1933Requisito_DataHomologacao[0];
            n1933Requisito_DataHomologacao = P00UV4_n1933Requisito_DataHomologacao[0];
            A1932Requisito_Pontuacao = P00UV4_A1932Requisito_Pontuacao[0];
            n1932Requisito_Pontuacao = P00UV4_n1932Requisito_Pontuacao[0];
            A1931Requisito_Ordem = P00UV4_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = P00UV4_n1931Requisito_Ordem[0];
            A1929Requisito_Restricao = P00UV4_A1929Requisito_Restricao[0];
            n1929Requisito_Restricao = P00UV4_n1929Requisito_Restricao[0];
            A1927Requisito_Titulo = P00UV4_A1927Requisito_Titulo[0];
            n1927Requisito_Titulo = P00UV4_n1927Requisito_Titulo[0];
            A2049Requisito_TipoReqCod = P00UV4_A2049Requisito_TipoReqCod[0];
            n2049Requisito_TipoReqCod = P00UV4_n2049Requisito_TipoReqCod[0];
            A1690Proposta_Objetivo = P00UV4_A1690Proposta_Objetivo[0];
            A1685Proposta_Codigo = P00UV4_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = P00UV4_n1685Proposta_Codigo[0];
            A1919Requisito_Codigo = P00UV4_A1919Requisito_Codigo[0];
            A1923Requisito_Descricao = P00UV4_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = P00UV4_n1923Requisito_Descricao[0];
            A1690Proposta_Objetivo = P00UV4_A1690Proposta_Objetivo[0];
            AV53count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00UV4_A1926Requisito_Agrupador[0], A1926Requisito_Agrupador) == 0 ) )
            {
               BRKUV6 = false;
               A1919Requisito_Codigo = P00UV4_A1919Requisito_Codigo[0];
               AV53count = (long)(AV53count+1);
               BRKUV6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1926Requisito_Agrupador)) )
            {
               AV45Option = A1926Requisito_Agrupador;
               AV46Options.Add(AV45Option, 0);
               AV51OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV53count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV46Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUV6 )
            {
               BRKUV6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADREQUISITO_TITULOOPTIONS' Routine */
         AV24TFRequisito_Titulo = AV41SearchTxt;
         AV25TFRequisito_Titulo_Sel = "";
         AV76WWRequisitoDS_1_Dynamicfiltersselector1 = AV59DynamicFiltersSelector1;
         AV77WWRequisitoDS_2_Dynamicfiltersoperator1 = AV60DynamicFiltersOperator1;
         AV78WWRequisitoDS_3_Requisito_descricao1 = AV61Requisito_Descricao1;
         AV79WWRequisitoDS_4_Dynamicfiltersenabled2 = AV62DynamicFiltersEnabled2;
         AV80WWRequisitoDS_5_Dynamicfiltersselector2 = AV63DynamicFiltersSelector2;
         AV81WWRequisitoDS_6_Dynamicfiltersoperator2 = AV64DynamicFiltersOperator2;
         AV82WWRequisitoDS_7_Requisito_descricao2 = AV65Requisito_Descricao2;
         AV83WWRequisitoDS_8_Dynamicfiltersenabled3 = AV66DynamicFiltersEnabled3;
         AV84WWRequisitoDS_9_Dynamicfiltersselector3 = AV67DynamicFiltersSelector3;
         AV85WWRequisitoDS_10_Dynamicfiltersoperator3 = AV68DynamicFiltersOperator3;
         AV86WWRequisitoDS_11_Requisito_descricao3 = AV69Requisito_Descricao3;
         AV87WWRequisitoDS_12_Tfrequisito_codigo = AV10TFRequisito_Codigo;
         AV88WWRequisitoDS_13_Tfrequisito_codigo_to = AV11TFRequisito_Codigo_To;
         AV89WWRequisitoDS_14_Tfrequisito_descricao = AV12TFRequisito_Descricao;
         AV90WWRequisitoDS_15_Tfrequisito_descricao_sel = AV13TFRequisito_Descricao_Sel;
         AV91WWRequisitoDS_16_Tfproposta_codigo = AV14TFProposta_Codigo;
         AV92WWRequisitoDS_17_Tfproposta_codigo_to = AV15TFProposta_Codigo_To;
         AV93WWRequisitoDS_18_Tfproposta_objetivo = AV16TFProposta_Objetivo;
         AV94WWRequisitoDS_19_Tfproposta_objetivo_sel = AV17TFProposta_Objetivo_Sel;
         AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod = AV70TFRequisito_TipoReqCod;
         AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to = AV71TFRequisito_TipoReqCod_To;
         AV97WWRequisitoDS_22_Tfrequisito_agrupador = AV22TFRequisito_Agrupador;
         AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel = AV23TFRequisito_Agrupador_Sel;
         AV99WWRequisitoDS_24_Tfrequisito_titulo = AV24TFRequisito_Titulo;
         AV100WWRequisitoDS_25_Tfrequisito_titulo_sel = AV25TFRequisito_Titulo_Sel;
         AV101WWRequisitoDS_26_Tfrequisito_restricao = AV28TFRequisito_Restricao;
         AV102WWRequisitoDS_27_Tfrequisito_restricao_sel = AV29TFRequisito_Restricao_Sel;
         AV103WWRequisitoDS_28_Tfrequisito_ordem = AV32TFRequisito_Ordem;
         AV104WWRequisitoDS_29_Tfrequisito_ordem_to = AV33TFRequisito_Ordem_To;
         AV105WWRequisitoDS_30_Tfrequisito_pontuacao = AV34TFRequisito_Pontuacao;
         AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to = AV35TFRequisito_Pontuacao_To;
         AV107WWRequisitoDS_32_Tfrequisito_datahomologacao = AV36TFRequisito_DataHomologacao;
         AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to = AV37TFRequisito_DataHomologacao_To;
         AV109WWRequisitoDS_34_Tfrequisito_status_sels = AV39TFRequisito_Status_Sels;
         AV110WWRequisitoDS_35_Tfrequisito_ativo_sel = AV40TFRequisito_Ativo_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A1934Requisito_Status ,
                                              AV109WWRequisitoDS_34_Tfrequisito_status_sels ,
                                              AV76WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                              AV77WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                              AV78WWRequisitoDS_3_Requisito_descricao1 ,
                                              AV79WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                              AV80WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                              AV81WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                              AV82WWRequisitoDS_7_Requisito_descricao2 ,
                                              AV83WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                              AV84WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                              AV85WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                              AV86WWRequisitoDS_11_Requisito_descricao3 ,
                                              AV87WWRequisitoDS_12_Tfrequisito_codigo ,
                                              AV88WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                              AV90WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                              AV89WWRequisitoDS_14_Tfrequisito_descricao ,
                                              AV91WWRequisitoDS_16_Tfproposta_codigo ,
                                              AV92WWRequisitoDS_17_Tfproposta_codigo_to ,
                                              AV94WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                              AV93WWRequisitoDS_18_Tfproposta_objetivo ,
                                              AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                              AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                              AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                              AV97WWRequisitoDS_22_Tfrequisito_agrupador ,
                                              AV100WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                              AV99WWRequisitoDS_24_Tfrequisito_titulo ,
                                              AV102WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                              AV101WWRequisitoDS_26_Tfrequisito_restricao ,
                                              AV103WWRequisitoDS_28_Tfrequisito_ordem ,
                                              AV104WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                              AV105WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                              AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                              AV107WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                              AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                              AV109WWRequisitoDS_34_Tfrequisito_status_sels.Count ,
                                              AV110WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                              A1923Requisito_Descricao ,
                                              A1919Requisito_Codigo ,
                                              A1685Proposta_Codigo ,
                                              A1690Proposta_Objetivo ,
                                              A2049Requisito_TipoReqCod ,
                                              A1926Requisito_Agrupador ,
                                              A1927Requisito_Titulo ,
                                              A1929Requisito_Restricao ,
                                              A1931Requisito_Ordem ,
                                              A1932Requisito_Pontuacao ,
                                              A1933Requisito_DataHomologacao ,
                                              A1935Requisito_Ativo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV78WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1), "%", "");
         lV78WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1), "%", "");
         lV82WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2), "%", "");
         lV82WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2), "%", "");
         lV86WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3), "%", "");
         lV86WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3), "%", "");
         lV89WWRequisitoDS_14_Tfrequisito_descricao = StringUtil.Concat( StringUtil.RTrim( AV89WWRequisitoDS_14_Tfrequisito_descricao), "%", "");
         lV93WWRequisitoDS_18_Tfproposta_objetivo = StringUtil.Concat( StringUtil.RTrim( AV93WWRequisitoDS_18_Tfproposta_objetivo), "%", "");
         lV97WWRequisitoDS_22_Tfrequisito_agrupador = StringUtil.Concat( StringUtil.RTrim( AV97WWRequisitoDS_22_Tfrequisito_agrupador), "%", "");
         lV99WWRequisitoDS_24_Tfrequisito_titulo = StringUtil.Concat( StringUtil.RTrim( AV99WWRequisitoDS_24_Tfrequisito_titulo), "%", "");
         lV101WWRequisitoDS_26_Tfrequisito_restricao = StringUtil.Concat( StringUtil.RTrim( AV101WWRequisitoDS_26_Tfrequisito_restricao), "%", "");
         /* Using cursor P00UV5 */
         pr_default.execute(3, new Object[] {lV78WWRequisitoDS_3_Requisito_descricao1, lV78WWRequisitoDS_3_Requisito_descricao1, lV82WWRequisitoDS_7_Requisito_descricao2, lV82WWRequisitoDS_7_Requisito_descricao2, lV86WWRequisitoDS_11_Requisito_descricao3, lV86WWRequisitoDS_11_Requisito_descricao3, AV87WWRequisitoDS_12_Tfrequisito_codigo, AV88WWRequisitoDS_13_Tfrequisito_codigo_to, lV89WWRequisitoDS_14_Tfrequisito_descricao, AV90WWRequisitoDS_15_Tfrequisito_descricao_sel, AV91WWRequisitoDS_16_Tfproposta_codigo, AV92WWRequisitoDS_17_Tfproposta_codigo_to, lV93WWRequisitoDS_18_Tfproposta_objetivo, AV94WWRequisitoDS_19_Tfproposta_objetivo_sel, AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod, AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to, lV97WWRequisitoDS_22_Tfrequisito_agrupador, AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel, lV99WWRequisitoDS_24_Tfrequisito_titulo, AV100WWRequisitoDS_25_Tfrequisito_titulo_sel, lV101WWRequisitoDS_26_Tfrequisito_restricao, AV102WWRequisitoDS_27_Tfrequisito_restricao_sel, AV103WWRequisitoDS_28_Tfrequisito_ordem, AV104WWRequisitoDS_29_Tfrequisito_ordem_to, AV105WWRequisitoDS_30_Tfrequisito_pontuacao, AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to, AV107WWRequisitoDS_32_Tfrequisito_datahomologacao, AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKUV8 = false;
            A1927Requisito_Titulo = P00UV5_A1927Requisito_Titulo[0];
            n1927Requisito_Titulo = P00UV5_n1927Requisito_Titulo[0];
            A1935Requisito_Ativo = P00UV5_A1935Requisito_Ativo[0];
            A1934Requisito_Status = P00UV5_A1934Requisito_Status[0];
            A1933Requisito_DataHomologacao = P00UV5_A1933Requisito_DataHomologacao[0];
            n1933Requisito_DataHomologacao = P00UV5_n1933Requisito_DataHomologacao[0];
            A1932Requisito_Pontuacao = P00UV5_A1932Requisito_Pontuacao[0];
            n1932Requisito_Pontuacao = P00UV5_n1932Requisito_Pontuacao[0];
            A1931Requisito_Ordem = P00UV5_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = P00UV5_n1931Requisito_Ordem[0];
            A1929Requisito_Restricao = P00UV5_A1929Requisito_Restricao[0];
            n1929Requisito_Restricao = P00UV5_n1929Requisito_Restricao[0];
            A1926Requisito_Agrupador = P00UV5_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = P00UV5_n1926Requisito_Agrupador[0];
            A2049Requisito_TipoReqCod = P00UV5_A2049Requisito_TipoReqCod[0];
            n2049Requisito_TipoReqCod = P00UV5_n2049Requisito_TipoReqCod[0];
            A1690Proposta_Objetivo = P00UV5_A1690Proposta_Objetivo[0];
            A1685Proposta_Codigo = P00UV5_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = P00UV5_n1685Proposta_Codigo[0];
            A1919Requisito_Codigo = P00UV5_A1919Requisito_Codigo[0];
            A1923Requisito_Descricao = P00UV5_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = P00UV5_n1923Requisito_Descricao[0];
            A1690Proposta_Objetivo = P00UV5_A1690Proposta_Objetivo[0];
            AV53count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00UV5_A1927Requisito_Titulo[0], A1927Requisito_Titulo) == 0 ) )
            {
               BRKUV8 = false;
               A1919Requisito_Codigo = P00UV5_A1919Requisito_Codigo[0];
               AV53count = (long)(AV53count+1);
               BRKUV8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1927Requisito_Titulo)) )
            {
               AV45Option = A1927Requisito_Titulo;
               AV46Options.Add(AV45Option, 0);
               AV51OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV53count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV46Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUV8 )
            {
               BRKUV8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADREQUISITO_RESTRICAOOPTIONS' Routine */
         AV28TFRequisito_Restricao = AV41SearchTxt;
         AV29TFRequisito_Restricao_Sel = "";
         AV76WWRequisitoDS_1_Dynamicfiltersselector1 = AV59DynamicFiltersSelector1;
         AV77WWRequisitoDS_2_Dynamicfiltersoperator1 = AV60DynamicFiltersOperator1;
         AV78WWRequisitoDS_3_Requisito_descricao1 = AV61Requisito_Descricao1;
         AV79WWRequisitoDS_4_Dynamicfiltersenabled2 = AV62DynamicFiltersEnabled2;
         AV80WWRequisitoDS_5_Dynamicfiltersselector2 = AV63DynamicFiltersSelector2;
         AV81WWRequisitoDS_6_Dynamicfiltersoperator2 = AV64DynamicFiltersOperator2;
         AV82WWRequisitoDS_7_Requisito_descricao2 = AV65Requisito_Descricao2;
         AV83WWRequisitoDS_8_Dynamicfiltersenabled3 = AV66DynamicFiltersEnabled3;
         AV84WWRequisitoDS_9_Dynamicfiltersselector3 = AV67DynamicFiltersSelector3;
         AV85WWRequisitoDS_10_Dynamicfiltersoperator3 = AV68DynamicFiltersOperator3;
         AV86WWRequisitoDS_11_Requisito_descricao3 = AV69Requisito_Descricao3;
         AV87WWRequisitoDS_12_Tfrequisito_codigo = AV10TFRequisito_Codigo;
         AV88WWRequisitoDS_13_Tfrequisito_codigo_to = AV11TFRequisito_Codigo_To;
         AV89WWRequisitoDS_14_Tfrequisito_descricao = AV12TFRequisito_Descricao;
         AV90WWRequisitoDS_15_Tfrequisito_descricao_sel = AV13TFRequisito_Descricao_Sel;
         AV91WWRequisitoDS_16_Tfproposta_codigo = AV14TFProposta_Codigo;
         AV92WWRequisitoDS_17_Tfproposta_codigo_to = AV15TFProposta_Codigo_To;
         AV93WWRequisitoDS_18_Tfproposta_objetivo = AV16TFProposta_Objetivo;
         AV94WWRequisitoDS_19_Tfproposta_objetivo_sel = AV17TFProposta_Objetivo_Sel;
         AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod = AV70TFRequisito_TipoReqCod;
         AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to = AV71TFRequisito_TipoReqCod_To;
         AV97WWRequisitoDS_22_Tfrequisito_agrupador = AV22TFRequisito_Agrupador;
         AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel = AV23TFRequisito_Agrupador_Sel;
         AV99WWRequisitoDS_24_Tfrequisito_titulo = AV24TFRequisito_Titulo;
         AV100WWRequisitoDS_25_Tfrequisito_titulo_sel = AV25TFRequisito_Titulo_Sel;
         AV101WWRequisitoDS_26_Tfrequisito_restricao = AV28TFRequisito_Restricao;
         AV102WWRequisitoDS_27_Tfrequisito_restricao_sel = AV29TFRequisito_Restricao_Sel;
         AV103WWRequisitoDS_28_Tfrequisito_ordem = AV32TFRequisito_Ordem;
         AV104WWRequisitoDS_29_Tfrequisito_ordem_to = AV33TFRequisito_Ordem_To;
         AV105WWRequisitoDS_30_Tfrequisito_pontuacao = AV34TFRequisito_Pontuacao;
         AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to = AV35TFRequisito_Pontuacao_To;
         AV107WWRequisitoDS_32_Tfrequisito_datahomologacao = AV36TFRequisito_DataHomologacao;
         AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to = AV37TFRequisito_DataHomologacao_To;
         AV109WWRequisitoDS_34_Tfrequisito_status_sels = AV39TFRequisito_Status_Sels;
         AV110WWRequisitoDS_35_Tfrequisito_ativo_sel = AV40TFRequisito_Ativo_Sel;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A1934Requisito_Status ,
                                              AV109WWRequisitoDS_34_Tfrequisito_status_sels ,
                                              AV76WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                              AV77WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                              AV78WWRequisitoDS_3_Requisito_descricao1 ,
                                              AV79WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                              AV80WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                              AV81WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                              AV82WWRequisitoDS_7_Requisito_descricao2 ,
                                              AV83WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                              AV84WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                              AV85WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                              AV86WWRequisitoDS_11_Requisito_descricao3 ,
                                              AV87WWRequisitoDS_12_Tfrequisito_codigo ,
                                              AV88WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                              AV90WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                              AV89WWRequisitoDS_14_Tfrequisito_descricao ,
                                              AV91WWRequisitoDS_16_Tfproposta_codigo ,
                                              AV92WWRequisitoDS_17_Tfproposta_codigo_to ,
                                              AV94WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                              AV93WWRequisitoDS_18_Tfproposta_objetivo ,
                                              AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                              AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                              AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                              AV97WWRequisitoDS_22_Tfrequisito_agrupador ,
                                              AV100WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                              AV99WWRequisitoDS_24_Tfrequisito_titulo ,
                                              AV102WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                              AV101WWRequisitoDS_26_Tfrequisito_restricao ,
                                              AV103WWRequisitoDS_28_Tfrequisito_ordem ,
                                              AV104WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                              AV105WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                              AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                              AV107WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                              AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                              AV109WWRequisitoDS_34_Tfrequisito_status_sels.Count ,
                                              AV110WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                              A1923Requisito_Descricao ,
                                              A1919Requisito_Codigo ,
                                              A1685Proposta_Codigo ,
                                              A1690Proposta_Objetivo ,
                                              A2049Requisito_TipoReqCod ,
                                              A1926Requisito_Agrupador ,
                                              A1927Requisito_Titulo ,
                                              A1929Requisito_Restricao ,
                                              A1931Requisito_Ordem ,
                                              A1932Requisito_Pontuacao ,
                                              A1933Requisito_DataHomologacao ,
                                              A1935Requisito_Ativo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV78WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1), "%", "");
         lV78WWRequisitoDS_3_Requisito_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1), "%", "");
         lV82WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2), "%", "");
         lV82WWRequisitoDS_7_Requisito_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2), "%", "");
         lV86WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3), "%", "");
         lV86WWRequisitoDS_11_Requisito_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3), "%", "");
         lV89WWRequisitoDS_14_Tfrequisito_descricao = StringUtil.Concat( StringUtil.RTrim( AV89WWRequisitoDS_14_Tfrequisito_descricao), "%", "");
         lV93WWRequisitoDS_18_Tfproposta_objetivo = StringUtil.Concat( StringUtil.RTrim( AV93WWRequisitoDS_18_Tfproposta_objetivo), "%", "");
         lV97WWRequisitoDS_22_Tfrequisito_agrupador = StringUtil.Concat( StringUtil.RTrim( AV97WWRequisitoDS_22_Tfrequisito_agrupador), "%", "");
         lV99WWRequisitoDS_24_Tfrequisito_titulo = StringUtil.Concat( StringUtil.RTrim( AV99WWRequisitoDS_24_Tfrequisito_titulo), "%", "");
         lV101WWRequisitoDS_26_Tfrequisito_restricao = StringUtil.Concat( StringUtil.RTrim( AV101WWRequisitoDS_26_Tfrequisito_restricao), "%", "");
         /* Using cursor P00UV6 */
         pr_default.execute(4, new Object[] {lV78WWRequisitoDS_3_Requisito_descricao1, lV78WWRequisitoDS_3_Requisito_descricao1, lV82WWRequisitoDS_7_Requisito_descricao2, lV82WWRequisitoDS_7_Requisito_descricao2, lV86WWRequisitoDS_11_Requisito_descricao3, lV86WWRequisitoDS_11_Requisito_descricao3, AV87WWRequisitoDS_12_Tfrequisito_codigo, AV88WWRequisitoDS_13_Tfrequisito_codigo_to, lV89WWRequisitoDS_14_Tfrequisito_descricao, AV90WWRequisitoDS_15_Tfrequisito_descricao_sel, AV91WWRequisitoDS_16_Tfproposta_codigo, AV92WWRequisitoDS_17_Tfproposta_codigo_to, lV93WWRequisitoDS_18_Tfproposta_objetivo, AV94WWRequisitoDS_19_Tfproposta_objetivo_sel, AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod, AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to, lV97WWRequisitoDS_22_Tfrequisito_agrupador, AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel, lV99WWRequisitoDS_24_Tfrequisito_titulo, AV100WWRequisitoDS_25_Tfrequisito_titulo_sel, lV101WWRequisitoDS_26_Tfrequisito_restricao, AV102WWRequisitoDS_27_Tfrequisito_restricao_sel, AV103WWRequisitoDS_28_Tfrequisito_ordem, AV104WWRequisitoDS_29_Tfrequisito_ordem_to, AV105WWRequisitoDS_30_Tfrequisito_pontuacao, AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to, AV107WWRequisitoDS_32_Tfrequisito_datahomologacao, AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKUV10 = false;
            A1929Requisito_Restricao = P00UV6_A1929Requisito_Restricao[0];
            n1929Requisito_Restricao = P00UV6_n1929Requisito_Restricao[0];
            A1935Requisito_Ativo = P00UV6_A1935Requisito_Ativo[0];
            A1934Requisito_Status = P00UV6_A1934Requisito_Status[0];
            A1933Requisito_DataHomologacao = P00UV6_A1933Requisito_DataHomologacao[0];
            n1933Requisito_DataHomologacao = P00UV6_n1933Requisito_DataHomologacao[0];
            A1932Requisito_Pontuacao = P00UV6_A1932Requisito_Pontuacao[0];
            n1932Requisito_Pontuacao = P00UV6_n1932Requisito_Pontuacao[0];
            A1931Requisito_Ordem = P00UV6_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = P00UV6_n1931Requisito_Ordem[0];
            A1927Requisito_Titulo = P00UV6_A1927Requisito_Titulo[0];
            n1927Requisito_Titulo = P00UV6_n1927Requisito_Titulo[0];
            A1926Requisito_Agrupador = P00UV6_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = P00UV6_n1926Requisito_Agrupador[0];
            A2049Requisito_TipoReqCod = P00UV6_A2049Requisito_TipoReqCod[0];
            n2049Requisito_TipoReqCod = P00UV6_n2049Requisito_TipoReqCod[0];
            A1690Proposta_Objetivo = P00UV6_A1690Proposta_Objetivo[0];
            A1685Proposta_Codigo = P00UV6_A1685Proposta_Codigo[0];
            n1685Proposta_Codigo = P00UV6_n1685Proposta_Codigo[0];
            A1919Requisito_Codigo = P00UV6_A1919Requisito_Codigo[0];
            A1923Requisito_Descricao = P00UV6_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = P00UV6_n1923Requisito_Descricao[0];
            A1690Proposta_Objetivo = P00UV6_A1690Proposta_Objetivo[0];
            AV53count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00UV6_A1929Requisito_Restricao[0], A1929Requisito_Restricao) == 0 ) )
            {
               BRKUV10 = false;
               A1919Requisito_Codigo = P00UV6_A1919Requisito_Codigo[0];
               AV53count = (long)(AV53count+1);
               BRKUV10 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1929Requisito_Restricao)) )
            {
               AV45Option = A1929Requisito_Restricao;
               AV46Options.Add(AV45Option, 0);
               AV51OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV53count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV46Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUV10 )
            {
               BRKUV10 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV46Options = new GxSimpleCollection();
         AV49OptionsDesc = new GxSimpleCollection();
         AV51OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV54Session = context.GetSession();
         AV56GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV57GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFRequisito_Descricao = "";
         AV13TFRequisito_Descricao_Sel = "";
         AV16TFProposta_Objetivo = "";
         AV17TFProposta_Objetivo_Sel = "";
         AV22TFRequisito_Agrupador = "";
         AV23TFRequisito_Agrupador_Sel = "";
         AV24TFRequisito_Titulo = "";
         AV25TFRequisito_Titulo_Sel = "";
         AV28TFRequisito_Restricao = "";
         AV29TFRequisito_Restricao_Sel = "";
         AV36TFRequisito_DataHomologacao = DateTime.MinValue;
         AV37TFRequisito_DataHomologacao_To = DateTime.MinValue;
         AV38TFRequisito_Status_SelsJson = "";
         AV39TFRequisito_Status_Sels = new GxSimpleCollection();
         AV58GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV59DynamicFiltersSelector1 = "";
         AV61Requisito_Descricao1 = "";
         AV63DynamicFiltersSelector2 = "";
         AV65Requisito_Descricao2 = "";
         AV67DynamicFiltersSelector3 = "";
         AV69Requisito_Descricao3 = "";
         AV76WWRequisitoDS_1_Dynamicfiltersselector1 = "";
         AV78WWRequisitoDS_3_Requisito_descricao1 = "";
         AV80WWRequisitoDS_5_Dynamicfiltersselector2 = "";
         AV82WWRequisitoDS_7_Requisito_descricao2 = "";
         AV84WWRequisitoDS_9_Dynamicfiltersselector3 = "";
         AV86WWRequisitoDS_11_Requisito_descricao3 = "";
         AV89WWRequisitoDS_14_Tfrequisito_descricao = "";
         AV90WWRequisitoDS_15_Tfrequisito_descricao_sel = "";
         AV93WWRequisitoDS_18_Tfproposta_objetivo = "";
         AV94WWRequisitoDS_19_Tfproposta_objetivo_sel = "";
         AV97WWRequisitoDS_22_Tfrequisito_agrupador = "";
         AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel = "";
         AV99WWRequisitoDS_24_Tfrequisito_titulo = "";
         AV100WWRequisitoDS_25_Tfrequisito_titulo_sel = "";
         AV101WWRequisitoDS_26_Tfrequisito_restricao = "";
         AV102WWRequisitoDS_27_Tfrequisito_restricao_sel = "";
         AV107WWRequisitoDS_32_Tfrequisito_datahomologacao = DateTime.MinValue;
         AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to = DateTime.MinValue;
         AV109WWRequisitoDS_34_Tfrequisito_status_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV78WWRequisitoDS_3_Requisito_descricao1 = "";
         lV82WWRequisitoDS_7_Requisito_descricao2 = "";
         lV86WWRequisitoDS_11_Requisito_descricao3 = "";
         lV89WWRequisitoDS_14_Tfrequisito_descricao = "";
         lV93WWRequisitoDS_18_Tfproposta_objetivo = "";
         lV97WWRequisitoDS_22_Tfrequisito_agrupador = "";
         lV99WWRequisitoDS_24_Tfrequisito_titulo = "";
         lV101WWRequisitoDS_26_Tfrequisito_restricao = "";
         A1923Requisito_Descricao = "";
         A1690Proposta_Objetivo = "";
         A1926Requisito_Agrupador = "";
         A1927Requisito_Titulo = "";
         A1929Requisito_Restricao = "";
         A1933Requisito_DataHomologacao = DateTime.MinValue;
         P00UV2_A1923Requisito_Descricao = new String[] {""} ;
         P00UV2_n1923Requisito_Descricao = new bool[] {false} ;
         P00UV2_A1935Requisito_Ativo = new bool[] {false} ;
         P00UV2_A1934Requisito_Status = new short[1] ;
         P00UV2_A1933Requisito_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00UV2_n1933Requisito_DataHomologacao = new bool[] {false} ;
         P00UV2_A1932Requisito_Pontuacao = new decimal[1] ;
         P00UV2_n1932Requisito_Pontuacao = new bool[] {false} ;
         P00UV2_A1931Requisito_Ordem = new short[1] ;
         P00UV2_n1931Requisito_Ordem = new bool[] {false} ;
         P00UV2_A1929Requisito_Restricao = new String[] {""} ;
         P00UV2_n1929Requisito_Restricao = new bool[] {false} ;
         P00UV2_A1927Requisito_Titulo = new String[] {""} ;
         P00UV2_n1927Requisito_Titulo = new bool[] {false} ;
         P00UV2_A1926Requisito_Agrupador = new String[] {""} ;
         P00UV2_n1926Requisito_Agrupador = new bool[] {false} ;
         P00UV2_A2049Requisito_TipoReqCod = new int[1] ;
         P00UV2_n2049Requisito_TipoReqCod = new bool[] {false} ;
         P00UV2_A1690Proposta_Objetivo = new String[] {""} ;
         P00UV2_A1685Proposta_Codigo = new int[1] ;
         P00UV2_n1685Proposta_Codigo = new bool[] {false} ;
         P00UV2_A1919Requisito_Codigo = new int[1] ;
         AV45Option = "";
         P00UV3_A1690Proposta_Objetivo = new String[] {""} ;
         P00UV3_A1935Requisito_Ativo = new bool[] {false} ;
         P00UV3_A1934Requisito_Status = new short[1] ;
         P00UV3_A1933Requisito_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00UV3_n1933Requisito_DataHomologacao = new bool[] {false} ;
         P00UV3_A1932Requisito_Pontuacao = new decimal[1] ;
         P00UV3_n1932Requisito_Pontuacao = new bool[] {false} ;
         P00UV3_A1931Requisito_Ordem = new short[1] ;
         P00UV3_n1931Requisito_Ordem = new bool[] {false} ;
         P00UV3_A1929Requisito_Restricao = new String[] {""} ;
         P00UV3_n1929Requisito_Restricao = new bool[] {false} ;
         P00UV3_A1927Requisito_Titulo = new String[] {""} ;
         P00UV3_n1927Requisito_Titulo = new bool[] {false} ;
         P00UV3_A1926Requisito_Agrupador = new String[] {""} ;
         P00UV3_n1926Requisito_Agrupador = new bool[] {false} ;
         P00UV3_A2049Requisito_TipoReqCod = new int[1] ;
         P00UV3_n2049Requisito_TipoReqCod = new bool[] {false} ;
         P00UV3_A1685Proposta_Codigo = new int[1] ;
         P00UV3_n1685Proposta_Codigo = new bool[] {false} ;
         P00UV3_A1919Requisito_Codigo = new int[1] ;
         P00UV3_A1923Requisito_Descricao = new String[] {""} ;
         P00UV3_n1923Requisito_Descricao = new bool[] {false} ;
         P00UV4_A1926Requisito_Agrupador = new String[] {""} ;
         P00UV4_n1926Requisito_Agrupador = new bool[] {false} ;
         P00UV4_A1935Requisito_Ativo = new bool[] {false} ;
         P00UV4_A1934Requisito_Status = new short[1] ;
         P00UV4_A1933Requisito_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00UV4_n1933Requisito_DataHomologacao = new bool[] {false} ;
         P00UV4_A1932Requisito_Pontuacao = new decimal[1] ;
         P00UV4_n1932Requisito_Pontuacao = new bool[] {false} ;
         P00UV4_A1931Requisito_Ordem = new short[1] ;
         P00UV4_n1931Requisito_Ordem = new bool[] {false} ;
         P00UV4_A1929Requisito_Restricao = new String[] {""} ;
         P00UV4_n1929Requisito_Restricao = new bool[] {false} ;
         P00UV4_A1927Requisito_Titulo = new String[] {""} ;
         P00UV4_n1927Requisito_Titulo = new bool[] {false} ;
         P00UV4_A2049Requisito_TipoReqCod = new int[1] ;
         P00UV4_n2049Requisito_TipoReqCod = new bool[] {false} ;
         P00UV4_A1690Proposta_Objetivo = new String[] {""} ;
         P00UV4_A1685Proposta_Codigo = new int[1] ;
         P00UV4_n1685Proposta_Codigo = new bool[] {false} ;
         P00UV4_A1919Requisito_Codigo = new int[1] ;
         P00UV4_A1923Requisito_Descricao = new String[] {""} ;
         P00UV4_n1923Requisito_Descricao = new bool[] {false} ;
         P00UV5_A1927Requisito_Titulo = new String[] {""} ;
         P00UV5_n1927Requisito_Titulo = new bool[] {false} ;
         P00UV5_A1935Requisito_Ativo = new bool[] {false} ;
         P00UV5_A1934Requisito_Status = new short[1] ;
         P00UV5_A1933Requisito_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00UV5_n1933Requisito_DataHomologacao = new bool[] {false} ;
         P00UV5_A1932Requisito_Pontuacao = new decimal[1] ;
         P00UV5_n1932Requisito_Pontuacao = new bool[] {false} ;
         P00UV5_A1931Requisito_Ordem = new short[1] ;
         P00UV5_n1931Requisito_Ordem = new bool[] {false} ;
         P00UV5_A1929Requisito_Restricao = new String[] {""} ;
         P00UV5_n1929Requisito_Restricao = new bool[] {false} ;
         P00UV5_A1926Requisito_Agrupador = new String[] {""} ;
         P00UV5_n1926Requisito_Agrupador = new bool[] {false} ;
         P00UV5_A2049Requisito_TipoReqCod = new int[1] ;
         P00UV5_n2049Requisito_TipoReqCod = new bool[] {false} ;
         P00UV5_A1690Proposta_Objetivo = new String[] {""} ;
         P00UV5_A1685Proposta_Codigo = new int[1] ;
         P00UV5_n1685Proposta_Codigo = new bool[] {false} ;
         P00UV5_A1919Requisito_Codigo = new int[1] ;
         P00UV5_A1923Requisito_Descricao = new String[] {""} ;
         P00UV5_n1923Requisito_Descricao = new bool[] {false} ;
         P00UV6_A1929Requisito_Restricao = new String[] {""} ;
         P00UV6_n1929Requisito_Restricao = new bool[] {false} ;
         P00UV6_A1935Requisito_Ativo = new bool[] {false} ;
         P00UV6_A1934Requisito_Status = new short[1] ;
         P00UV6_A1933Requisito_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00UV6_n1933Requisito_DataHomologacao = new bool[] {false} ;
         P00UV6_A1932Requisito_Pontuacao = new decimal[1] ;
         P00UV6_n1932Requisito_Pontuacao = new bool[] {false} ;
         P00UV6_A1931Requisito_Ordem = new short[1] ;
         P00UV6_n1931Requisito_Ordem = new bool[] {false} ;
         P00UV6_A1927Requisito_Titulo = new String[] {""} ;
         P00UV6_n1927Requisito_Titulo = new bool[] {false} ;
         P00UV6_A1926Requisito_Agrupador = new String[] {""} ;
         P00UV6_n1926Requisito_Agrupador = new bool[] {false} ;
         P00UV6_A2049Requisito_TipoReqCod = new int[1] ;
         P00UV6_n2049Requisito_TipoReqCod = new bool[] {false} ;
         P00UV6_A1690Proposta_Objetivo = new String[] {""} ;
         P00UV6_A1685Proposta_Codigo = new int[1] ;
         P00UV6_n1685Proposta_Codigo = new bool[] {false} ;
         P00UV6_A1919Requisito_Codigo = new int[1] ;
         P00UV6_A1923Requisito_Descricao = new String[] {""} ;
         P00UV6_n1923Requisito_Descricao = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwrequisitofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UV2_A1923Requisito_Descricao, P00UV2_n1923Requisito_Descricao, P00UV2_A1935Requisito_Ativo, P00UV2_A1934Requisito_Status, P00UV2_A1933Requisito_DataHomologacao, P00UV2_n1933Requisito_DataHomologacao, P00UV2_A1932Requisito_Pontuacao, P00UV2_n1932Requisito_Pontuacao, P00UV2_A1931Requisito_Ordem, P00UV2_n1931Requisito_Ordem,
               P00UV2_A1929Requisito_Restricao, P00UV2_n1929Requisito_Restricao, P00UV2_A1927Requisito_Titulo, P00UV2_n1927Requisito_Titulo, P00UV2_A1926Requisito_Agrupador, P00UV2_n1926Requisito_Agrupador, P00UV2_A2049Requisito_TipoReqCod, P00UV2_n2049Requisito_TipoReqCod, P00UV2_A1690Proposta_Objetivo, P00UV2_A1685Proposta_Codigo,
               P00UV2_n1685Proposta_Codigo, P00UV2_A1919Requisito_Codigo
               }
               , new Object[] {
               P00UV3_A1690Proposta_Objetivo, P00UV3_A1935Requisito_Ativo, P00UV3_A1934Requisito_Status, P00UV3_A1933Requisito_DataHomologacao, P00UV3_n1933Requisito_DataHomologacao, P00UV3_A1932Requisito_Pontuacao, P00UV3_n1932Requisito_Pontuacao, P00UV3_A1931Requisito_Ordem, P00UV3_n1931Requisito_Ordem, P00UV3_A1929Requisito_Restricao,
               P00UV3_n1929Requisito_Restricao, P00UV3_A1927Requisito_Titulo, P00UV3_n1927Requisito_Titulo, P00UV3_A1926Requisito_Agrupador, P00UV3_n1926Requisito_Agrupador, P00UV3_A2049Requisito_TipoReqCod, P00UV3_n2049Requisito_TipoReqCod, P00UV3_A1685Proposta_Codigo, P00UV3_n1685Proposta_Codigo, P00UV3_A1919Requisito_Codigo,
               P00UV3_A1923Requisito_Descricao, P00UV3_n1923Requisito_Descricao
               }
               , new Object[] {
               P00UV4_A1926Requisito_Agrupador, P00UV4_n1926Requisito_Agrupador, P00UV4_A1935Requisito_Ativo, P00UV4_A1934Requisito_Status, P00UV4_A1933Requisito_DataHomologacao, P00UV4_n1933Requisito_DataHomologacao, P00UV4_A1932Requisito_Pontuacao, P00UV4_n1932Requisito_Pontuacao, P00UV4_A1931Requisito_Ordem, P00UV4_n1931Requisito_Ordem,
               P00UV4_A1929Requisito_Restricao, P00UV4_n1929Requisito_Restricao, P00UV4_A1927Requisito_Titulo, P00UV4_n1927Requisito_Titulo, P00UV4_A2049Requisito_TipoReqCod, P00UV4_n2049Requisito_TipoReqCod, P00UV4_A1690Proposta_Objetivo, P00UV4_A1685Proposta_Codigo, P00UV4_n1685Proposta_Codigo, P00UV4_A1919Requisito_Codigo,
               P00UV4_A1923Requisito_Descricao, P00UV4_n1923Requisito_Descricao
               }
               , new Object[] {
               P00UV5_A1927Requisito_Titulo, P00UV5_n1927Requisito_Titulo, P00UV5_A1935Requisito_Ativo, P00UV5_A1934Requisito_Status, P00UV5_A1933Requisito_DataHomologacao, P00UV5_n1933Requisito_DataHomologacao, P00UV5_A1932Requisito_Pontuacao, P00UV5_n1932Requisito_Pontuacao, P00UV5_A1931Requisito_Ordem, P00UV5_n1931Requisito_Ordem,
               P00UV5_A1929Requisito_Restricao, P00UV5_n1929Requisito_Restricao, P00UV5_A1926Requisito_Agrupador, P00UV5_n1926Requisito_Agrupador, P00UV5_A2049Requisito_TipoReqCod, P00UV5_n2049Requisito_TipoReqCod, P00UV5_A1690Proposta_Objetivo, P00UV5_A1685Proposta_Codigo, P00UV5_n1685Proposta_Codigo, P00UV5_A1919Requisito_Codigo,
               P00UV5_A1923Requisito_Descricao, P00UV5_n1923Requisito_Descricao
               }
               , new Object[] {
               P00UV6_A1929Requisito_Restricao, P00UV6_n1929Requisito_Restricao, P00UV6_A1935Requisito_Ativo, P00UV6_A1934Requisito_Status, P00UV6_A1933Requisito_DataHomologacao, P00UV6_n1933Requisito_DataHomologacao, P00UV6_A1932Requisito_Pontuacao, P00UV6_n1932Requisito_Pontuacao, P00UV6_A1931Requisito_Ordem, P00UV6_n1931Requisito_Ordem,
               P00UV6_A1927Requisito_Titulo, P00UV6_n1927Requisito_Titulo, P00UV6_A1926Requisito_Agrupador, P00UV6_n1926Requisito_Agrupador, P00UV6_A2049Requisito_TipoReqCod, P00UV6_n2049Requisito_TipoReqCod, P00UV6_A1690Proposta_Objetivo, P00UV6_A1685Proposta_Codigo, P00UV6_n1685Proposta_Codigo, P00UV6_A1919Requisito_Codigo,
               P00UV6_A1923Requisito_Descricao, P00UV6_n1923Requisito_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV32TFRequisito_Ordem ;
      private short AV33TFRequisito_Ordem_To ;
      private short AV40TFRequisito_Ativo_Sel ;
      private short AV60DynamicFiltersOperator1 ;
      private short AV64DynamicFiltersOperator2 ;
      private short AV68DynamicFiltersOperator3 ;
      private short AV77WWRequisitoDS_2_Dynamicfiltersoperator1 ;
      private short AV81WWRequisitoDS_6_Dynamicfiltersoperator2 ;
      private short AV85WWRequisitoDS_10_Dynamicfiltersoperator3 ;
      private short AV103WWRequisitoDS_28_Tfrequisito_ordem ;
      private short AV104WWRequisitoDS_29_Tfrequisito_ordem_to ;
      private short AV110WWRequisitoDS_35_Tfrequisito_ativo_sel ;
      private short A1934Requisito_Status ;
      private short A1931Requisito_Ordem ;
      private int AV74GXV1 ;
      private int AV10TFRequisito_Codigo ;
      private int AV11TFRequisito_Codigo_To ;
      private int AV14TFProposta_Codigo ;
      private int AV15TFProposta_Codigo_To ;
      private int AV70TFRequisito_TipoReqCod ;
      private int AV71TFRequisito_TipoReqCod_To ;
      private int AV87WWRequisitoDS_12_Tfrequisito_codigo ;
      private int AV88WWRequisitoDS_13_Tfrequisito_codigo_to ;
      private int AV91WWRequisitoDS_16_Tfproposta_codigo ;
      private int AV92WWRequisitoDS_17_Tfproposta_codigo_to ;
      private int AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod ;
      private int AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ;
      private int AV109WWRequisitoDS_34_Tfrequisito_status_sels_Count ;
      private int A1919Requisito_Codigo ;
      private int A1685Proposta_Codigo ;
      private int A2049Requisito_TipoReqCod ;
      private long AV53count ;
      private decimal AV34TFRequisito_Pontuacao ;
      private decimal AV35TFRequisito_Pontuacao_To ;
      private decimal AV105WWRequisitoDS_30_Tfrequisito_pontuacao ;
      private decimal AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to ;
      private decimal A1932Requisito_Pontuacao ;
      private String scmdbuf ;
      private DateTime AV36TFRequisito_DataHomologacao ;
      private DateTime AV37TFRequisito_DataHomologacao_To ;
      private DateTime AV107WWRequisitoDS_32_Tfrequisito_datahomologacao ;
      private DateTime AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to ;
      private DateTime A1933Requisito_DataHomologacao ;
      private bool returnInSub ;
      private bool AV62DynamicFiltersEnabled2 ;
      private bool AV66DynamicFiltersEnabled3 ;
      private bool AV79WWRequisitoDS_4_Dynamicfiltersenabled2 ;
      private bool AV83WWRequisitoDS_8_Dynamicfiltersenabled3 ;
      private bool A1935Requisito_Ativo ;
      private bool BRKUV2 ;
      private bool n1923Requisito_Descricao ;
      private bool n1933Requisito_DataHomologacao ;
      private bool n1932Requisito_Pontuacao ;
      private bool n1931Requisito_Ordem ;
      private bool n1929Requisito_Restricao ;
      private bool n1927Requisito_Titulo ;
      private bool n1926Requisito_Agrupador ;
      private bool n2049Requisito_TipoReqCod ;
      private bool n1685Proposta_Codigo ;
      private bool BRKUV4 ;
      private bool BRKUV6 ;
      private bool BRKUV8 ;
      private bool BRKUV10 ;
      private String AV52OptionIndexesJson ;
      private String AV47OptionsJson ;
      private String AV50OptionsDescJson ;
      private String AV38TFRequisito_Status_SelsJson ;
      private String AV61Requisito_Descricao1 ;
      private String AV65Requisito_Descricao2 ;
      private String AV69Requisito_Descricao3 ;
      private String AV78WWRequisitoDS_3_Requisito_descricao1 ;
      private String AV82WWRequisitoDS_7_Requisito_descricao2 ;
      private String AV86WWRequisitoDS_11_Requisito_descricao3 ;
      private String lV78WWRequisitoDS_3_Requisito_descricao1 ;
      private String lV82WWRequisitoDS_7_Requisito_descricao2 ;
      private String lV86WWRequisitoDS_11_Requisito_descricao3 ;
      private String A1923Requisito_Descricao ;
      private String A1690Proposta_Objetivo ;
      private String A1929Requisito_Restricao ;
      private String AV43DDOName ;
      private String AV41SearchTxt ;
      private String AV42SearchTxtTo ;
      private String AV12TFRequisito_Descricao ;
      private String AV13TFRequisito_Descricao_Sel ;
      private String AV16TFProposta_Objetivo ;
      private String AV17TFProposta_Objetivo_Sel ;
      private String AV22TFRequisito_Agrupador ;
      private String AV23TFRequisito_Agrupador_Sel ;
      private String AV24TFRequisito_Titulo ;
      private String AV25TFRequisito_Titulo_Sel ;
      private String AV28TFRequisito_Restricao ;
      private String AV29TFRequisito_Restricao_Sel ;
      private String AV59DynamicFiltersSelector1 ;
      private String AV63DynamicFiltersSelector2 ;
      private String AV67DynamicFiltersSelector3 ;
      private String AV76WWRequisitoDS_1_Dynamicfiltersselector1 ;
      private String AV80WWRequisitoDS_5_Dynamicfiltersselector2 ;
      private String AV84WWRequisitoDS_9_Dynamicfiltersselector3 ;
      private String AV89WWRequisitoDS_14_Tfrequisito_descricao ;
      private String AV90WWRequisitoDS_15_Tfrequisito_descricao_sel ;
      private String AV93WWRequisitoDS_18_Tfproposta_objetivo ;
      private String AV94WWRequisitoDS_19_Tfproposta_objetivo_sel ;
      private String AV97WWRequisitoDS_22_Tfrequisito_agrupador ;
      private String AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel ;
      private String AV99WWRequisitoDS_24_Tfrequisito_titulo ;
      private String AV100WWRequisitoDS_25_Tfrequisito_titulo_sel ;
      private String AV101WWRequisitoDS_26_Tfrequisito_restricao ;
      private String AV102WWRequisitoDS_27_Tfrequisito_restricao_sel ;
      private String lV89WWRequisitoDS_14_Tfrequisito_descricao ;
      private String lV93WWRequisitoDS_18_Tfproposta_objetivo ;
      private String lV97WWRequisitoDS_22_Tfrequisito_agrupador ;
      private String lV99WWRequisitoDS_24_Tfrequisito_titulo ;
      private String lV101WWRequisitoDS_26_Tfrequisito_restricao ;
      private String A1926Requisito_Agrupador ;
      private String A1927Requisito_Titulo ;
      private String AV45Option ;
      private IGxSession AV54Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00UV2_A1923Requisito_Descricao ;
      private bool[] P00UV2_n1923Requisito_Descricao ;
      private bool[] P00UV2_A1935Requisito_Ativo ;
      private short[] P00UV2_A1934Requisito_Status ;
      private DateTime[] P00UV2_A1933Requisito_DataHomologacao ;
      private bool[] P00UV2_n1933Requisito_DataHomologacao ;
      private decimal[] P00UV2_A1932Requisito_Pontuacao ;
      private bool[] P00UV2_n1932Requisito_Pontuacao ;
      private short[] P00UV2_A1931Requisito_Ordem ;
      private bool[] P00UV2_n1931Requisito_Ordem ;
      private String[] P00UV2_A1929Requisito_Restricao ;
      private bool[] P00UV2_n1929Requisito_Restricao ;
      private String[] P00UV2_A1927Requisito_Titulo ;
      private bool[] P00UV2_n1927Requisito_Titulo ;
      private String[] P00UV2_A1926Requisito_Agrupador ;
      private bool[] P00UV2_n1926Requisito_Agrupador ;
      private int[] P00UV2_A2049Requisito_TipoReqCod ;
      private bool[] P00UV2_n2049Requisito_TipoReqCod ;
      private String[] P00UV2_A1690Proposta_Objetivo ;
      private int[] P00UV2_A1685Proposta_Codigo ;
      private bool[] P00UV2_n1685Proposta_Codigo ;
      private int[] P00UV2_A1919Requisito_Codigo ;
      private String[] P00UV3_A1690Proposta_Objetivo ;
      private bool[] P00UV3_A1935Requisito_Ativo ;
      private short[] P00UV3_A1934Requisito_Status ;
      private DateTime[] P00UV3_A1933Requisito_DataHomologacao ;
      private bool[] P00UV3_n1933Requisito_DataHomologacao ;
      private decimal[] P00UV3_A1932Requisito_Pontuacao ;
      private bool[] P00UV3_n1932Requisito_Pontuacao ;
      private short[] P00UV3_A1931Requisito_Ordem ;
      private bool[] P00UV3_n1931Requisito_Ordem ;
      private String[] P00UV3_A1929Requisito_Restricao ;
      private bool[] P00UV3_n1929Requisito_Restricao ;
      private String[] P00UV3_A1927Requisito_Titulo ;
      private bool[] P00UV3_n1927Requisito_Titulo ;
      private String[] P00UV3_A1926Requisito_Agrupador ;
      private bool[] P00UV3_n1926Requisito_Agrupador ;
      private int[] P00UV3_A2049Requisito_TipoReqCod ;
      private bool[] P00UV3_n2049Requisito_TipoReqCod ;
      private int[] P00UV3_A1685Proposta_Codigo ;
      private bool[] P00UV3_n1685Proposta_Codigo ;
      private int[] P00UV3_A1919Requisito_Codigo ;
      private String[] P00UV3_A1923Requisito_Descricao ;
      private bool[] P00UV3_n1923Requisito_Descricao ;
      private String[] P00UV4_A1926Requisito_Agrupador ;
      private bool[] P00UV4_n1926Requisito_Agrupador ;
      private bool[] P00UV4_A1935Requisito_Ativo ;
      private short[] P00UV4_A1934Requisito_Status ;
      private DateTime[] P00UV4_A1933Requisito_DataHomologacao ;
      private bool[] P00UV4_n1933Requisito_DataHomologacao ;
      private decimal[] P00UV4_A1932Requisito_Pontuacao ;
      private bool[] P00UV4_n1932Requisito_Pontuacao ;
      private short[] P00UV4_A1931Requisito_Ordem ;
      private bool[] P00UV4_n1931Requisito_Ordem ;
      private String[] P00UV4_A1929Requisito_Restricao ;
      private bool[] P00UV4_n1929Requisito_Restricao ;
      private String[] P00UV4_A1927Requisito_Titulo ;
      private bool[] P00UV4_n1927Requisito_Titulo ;
      private int[] P00UV4_A2049Requisito_TipoReqCod ;
      private bool[] P00UV4_n2049Requisito_TipoReqCod ;
      private String[] P00UV4_A1690Proposta_Objetivo ;
      private int[] P00UV4_A1685Proposta_Codigo ;
      private bool[] P00UV4_n1685Proposta_Codigo ;
      private int[] P00UV4_A1919Requisito_Codigo ;
      private String[] P00UV4_A1923Requisito_Descricao ;
      private bool[] P00UV4_n1923Requisito_Descricao ;
      private String[] P00UV5_A1927Requisito_Titulo ;
      private bool[] P00UV5_n1927Requisito_Titulo ;
      private bool[] P00UV5_A1935Requisito_Ativo ;
      private short[] P00UV5_A1934Requisito_Status ;
      private DateTime[] P00UV5_A1933Requisito_DataHomologacao ;
      private bool[] P00UV5_n1933Requisito_DataHomologacao ;
      private decimal[] P00UV5_A1932Requisito_Pontuacao ;
      private bool[] P00UV5_n1932Requisito_Pontuacao ;
      private short[] P00UV5_A1931Requisito_Ordem ;
      private bool[] P00UV5_n1931Requisito_Ordem ;
      private String[] P00UV5_A1929Requisito_Restricao ;
      private bool[] P00UV5_n1929Requisito_Restricao ;
      private String[] P00UV5_A1926Requisito_Agrupador ;
      private bool[] P00UV5_n1926Requisito_Agrupador ;
      private int[] P00UV5_A2049Requisito_TipoReqCod ;
      private bool[] P00UV5_n2049Requisito_TipoReqCod ;
      private String[] P00UV5_A1690Proposta_Objetivo ;
      private int[] P00UV5_A1685Proposta_Codigo ;
      private bool[] P00UV5_n1685Proposta_Codigo ;
      private int[] P00UV5_A1919Requisito_Codigo ;
      private String[] P00UV5_A1923Requisito_Descricao ;
      private bool[] P00UV5_n1923Requisito_Descricao ;
      private String[] P00UV6_A1929Requisito_Restricao ;
      private bool[] P00UV6_n1929Requisito_Restricao ;
      private bool[] P00UV6_A1935Requisito_Ativo ;
      private short[] P00UV6_A1934Requisito_Status ;
      private DateTime[] P00UV6_A1933Requisito_DataHomologacao ;
      private bool[] P00UV6_n1933Requisito_DataHomologacao ;
      private decimal[] P00UV6_A1932Requisito_Pontuacao ;
      private bool[] P00UV6_n1932Requisito_Pontuacao ;
      private short[] P00UV6_A1931Requisito_Ordem ;
      private bool[] P00UV6_n1931Requisito_Ordem ;
      private String[] P00UV6_A1927Requisito_Titulo ;
      private bool[] P00UV6_n1927Requisito_Titulo ;
      private String[] P00UV6_A1926Requisito_Agrupador ;
      private bool[] P00UV6_n1926Requisito_Agrupador ;
      private int[] P00UV6_A2049Requisito_TipoReqCod ;
      private bool[] P00UV6_n2049Requisito_TipoReqCod ;
      private String[] P00UV6_A1690Proposta_Objetivo ;
      private int[] P00UV6_A1685Proposta_Codigo ;
      private bool[] P00UV6_n1685Proposta_Codigo ;
      private int[] P00UV6_A1919Requisito_Codigo ;
      private String[] P00UV6_A1923Requisito_Descricao ;
      private bool[] P00UV6_n1923Requisito_Descricao ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV39TFRequisito_Status_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV109WWRequisitoDS_34_Tfrequisito_status_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV46Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV49OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV51OptionIndexes ;
      private wwpbaseobjects.SdtWWPGridState AV56GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV57GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV58GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class getwwrequisitofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UV2( IGxContext context ,
                                             short A1934Requisito_Status ,
                                             IGxCollection AV109WWRequisitoDS_34_Tfrequisito_status_sels ,
                                             String AV76WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                             short AV77WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                             String AV78WWRequisitoDS_3_Requisito_descricao1 ,
                                             bool AV79WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                             String AV80WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                             short AV81WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                             String AV82WWRequisitoDS_7_Requisito_descricao2 ,
                                             bool AV83WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                             String AV84WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                             short AV85WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                             String AV86WWRequisitoDS_11_Requisito_descricao3 ,
                                             int AV87WWRequisitoDS_12_Tfrequisito_codigo ,
                                             int AV88WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                             String AV90WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                             String AV89WWRequisitoDS_14_Tfrequisito_descricao ,
                                             int AV91WWRequisitoDS_16_Tfproposta_codigo ,
                                             int AV92WWRequisitoDS_17_Tfproposta_codigo_to ,
                                             String AV94WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                             String AV93WWRequisitoDS_18_Tfproposta_objetivo ,
                                             int AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                             int AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                             String AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                             String AV97WWRequisitoDS_22_Tfrequisito_agrupador ,
                                             String AV100WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                             String AV99WWRequisitoDS_24_Tfrequisito_titulo ,
                                             String AV102WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                             String AV101WWRequisitoDS_26_Tfrequisito_restricao ,
                                             short AV103WWRequisitoDS_28_Tfrequisito_ordem ,
                                             short AV104WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                             decimal AV105WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                             decimal AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                             DateTime AV107WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                             DateTime AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                             int AV109WWRequisitoDS_34_Tfrequisito_status_sels_Count ,
                                             short AV110WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                             String A1923Requisito_Descricao ,
                                             int A1919Requisito_Codigo ,
                                             int A1685Proposta_Codigo ,
                                             String A1690Proposta_Objetivo ,
                                             int A2049Requisito_TipoReqCod ,
                                             String A1926Requisito_Agrupador ,
                                             String A1927Requisito_Titulo ,
                                             String A1929Requisito_Restricao ,
                                             short A1931Requisito_Ordem ,
                                             decimal A1932Requisito_Pontuacao ,
                                             DateTime A1933Requisito_DataHomologacao ,
                                             bool A1935Requisito_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [28] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Requisito_Descricao], T1.[Requisito_Ativo], T1.[Requisito_Status], T1.[Requisito_DataHomologacao], T1.[Requisito_Pontuacao], T1.[Requisito_Ordem], T1.[Requisito_Restricao], T1.[Requisito_Titulo], T1.[Requisito_Agrupador], T1.[Requisito_TipoReqCod], T2.[Proposta_Objetivo], T1.[Proposta_Codigo], T1.[Requisito_Codigo] FROM ([Requisito] T1 WITH (NOLOCK) LEFT JOIN dbo.[Proposta] T2 WITH (NOLOCK) ON T2.[Proposta_Codigo] = T1.[Proposta_Codigo])";
         if ( ( StringUtil.StrCmp(AV76WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV77WWRequisitoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV77WWRequisitoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV79WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV81WWRequisitoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV79WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV81WWRequisitoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV83WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV85WWRequisitoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV83WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV85WWRequisitoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV87WWRequisitoDS_12_Tfrequisito_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] >= @AV87WWRequisitoDS_12_Tfrequisito_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] >= @AV87WWRequisitoDS_12_Tfrequisito_codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV88WWRequisitoDS_13_Tfrequisito_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] <= @AV88WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] <= @AV88WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWRequisitoDS_14_Tfrequisito_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV89WWRequisitoDS_14_Tfrequisito_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV89WWRequisitoDS_14_Tfrequisito_descricao)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] = @AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] = @AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (0==AV91WWRequisitoDS_16_Tfproposta_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] >= @AV91WWRequisitoDS_16_Tfproposta_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] >= @AV91WWRequisitoDS_16_Tfproposta_codigo)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (0==AV92WWRequisitoDS_17_Tfproposta_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] <= @AV92WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] <= @AV92WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWRequisitoDS_18_Tfproposta_objetivo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] like @lV93WWRequisitoDS_18_Tfproposta_objetivo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] like @lV93WWRequisitoDS_18_Tfproposta_objetivo)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] = @AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] = @AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] >= @AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] >= @AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] <= @AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] <= @AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWRequisitoDS_22_Tfrequisito_agrupador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] like @lV97WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] like @lV97WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] = @AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] = @AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWRequisitoDS_24_Tfrequisito_titulo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] like @lV99WWRequisitoDS_24_Tfrequisito_titulo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] like @lV99WWRequisitoDS_24_Tfrequisito_titulo)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] = @AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] = @AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWRequisitoDS_26_Tfrequisito_restricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] like @lV101WWRequisitoDS_26_Tfrequisito_restricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] like @lV101WWRequisitoDS_26_Tfrequisito_restricao)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] = @AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] = @AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (0==AV103WWRequisitoDS_28_Tfrequisito_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] >= @AV103WWRequisitoDS_28_Tfrequisito_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] >= @AV103WWRequisitoDS_28_Tfrequisito_ordem)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (0==AV104WWRequisitoDS_29_Tfrequisito_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] <= @AV104WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] <= @AV104WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV105WWRequisitoDS_30_Tfrequisito_pontuacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] >= @AV105WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] >= @AV105WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] <= @AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] <= @AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV107WWRequisitoDS_32_Tfrequisito_datahomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] >= @AV107WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] >= @AV107WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] <= @AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] <= @AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( AV109WWRequisitoDS_34_Tfrequisito_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV109WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV109WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
         }
         if ( AV110WWRequisitoDS_35_Tfrequisito_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 1)";
            }
         }
         if ( AV110WWRequisitoDS_35_Tfrequisito_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Requisito_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UV3( IGxContext context ,
                                             short A1934Requisito_Status ,
                                             IGxCollection AV109WWRequisitoDS_34_Tfrequisito_status_sels ,
                                             String AV76WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                             short AV77WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                             String AV78WWRequisitoDS_3_Requisito_descricao1 ,
                                             bool AV79WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                             String AV80WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                             short AV81WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                             String AV82WWRequisitoDS_7_Requisito_descricao2 ,
                                             bool AV83WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                             String AV84WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                             short AV85WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                             String AV86WWRequisitoDS_11_Requisito_descricao3 ,
                                             int AV87WWRequisitoDS_12_Tfrequisito_codigo ,
                                             int AV88WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                             String AV90WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                             String AV89WWRequisitoDS_14_Tfrequisito_descricao ,
                                             int AV91WWRequisitoDS_16_Tfproposta_codigo ,
                                             int AV92WWRequisitoDS_17_Tfproposta_codigo_to ,
                                             String AV94WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                             String AV93WWRequisitoDS_18_Tfproposta_objetivo ,
                                             int AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                             int AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                             String AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                             String AV97WWRequisitoDS_22_Tfrequisito_agrupador ,
                                             String AV100WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                             String AV99WWRequisitoDS_24_Tfrequisito_titulo ,
                                             String AV102WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                             String AV101WWRequisitoDS_26_Tfrequisito_restricao ,
                                             short AV103WWRequisitoDS_28_Tfrequisito_ordem ,
                                             short AV104WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                             decimal AV105WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                             decimal AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                             DateTime AV107WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                             DateTime AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                             int AV109WWRequisitoDS_34_Tfrequisito_status_sels_Count ,
                                             short AV110WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                             String A1923Requisito_Descricao ,
                                             int A1919Requisito_Codigo ,
                                             int A1685Proposta_Codigo ,
                                             String A1690Proposta_Objetivo ,
                                             int A2049Requisito_TipoReqCod ,
                                             String A1926Requisito_Agrupador ,
                                             String A1927Requisito_Titulo ,
                                             String A1929Requisito_Restricao ,
                                             short A1931Requisito_Ordem ,
                                             decimal A1932Requisito_Pontuacao ,
                                             DateTime A1933Requisito_DataHomologacao ,
                                             bool A1935Requisito_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [28] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T2.[Proposta_Objetivo], T1.[Requisito_Ativo], T1.[Requisito_Status], T1.[Requisito_DataHomologacao], T1.[Requisito_Pontuacao], T1.[Requisito_Ordem], T1.[Requisito_Restricao], T1.[Requisito_Titulo], T1.[Requisito_Agrupador], T1.[Requisito_TipoReqCod], T1.[Proposta_Codigo], T1.[Requisito_Codigo], T1.[Requisito_Descricao] FROM ([Requisito] T1 WITH (NOLOCK) LEFT JOIN dbo.[Proposta] T2 WITH (NOLOCK) ON T2.[Proposta_Codigo] = T1.[Proposta_Codigo])";
         if ( ( StringUtil.StrCmp(AV76WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV77WWRequisitoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV77WWRequisitoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV79WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV81WWRequisitoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV79WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV81WWRequisitoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV83WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV85WWRequisitoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV83WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV85WWRequisitoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV87WWRequisitoDS_12_Tfrequisito_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] >= @AV87WWRequisitoDS_12_Tfrequisito_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] >= @AV87WWRequisitoDS_12_Tfrequisito_codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV88WWRequisitoDS_13_Tfrequisito_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] <= @AV88WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] <= @AV88WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWRequisitoDS_14_Tfrequisito_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV89WWRequisitoDS_14_Tfrequisito_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV89WWRequisitoDS_14_Tfrequisito_descricao)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] = @AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] = @AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (0==AV91WWRequisitoDS_16_Tfproposta_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] >= @AV91WWRequisitoDS_16_Tfproposta_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] >= @AV91WWRequisitoDS_16_Tfproposta_codigo)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (0==AV92WWRequisitoDS_17_Tfproposta_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] <= @AV92WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] <= @AV92WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWRequisitoDS_18_Tfproposta_objetivo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] like @lV93WWRequisitoDS_18_Tfproposta_objetivo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] like @lV93WWRequisitoDS_18_Tfproposta_objetivo)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] = @AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] = @AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] >= @AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] >= @AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] <= @AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] <= @AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWRequisitoDS_22_Tfrequisito_agrupador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] like @lV97WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] like @lV97WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] = @AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] = @AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWRequisitoDS_24_Tfrequisito_titulo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] like @lV99WWRequisitoDS_24_Tfrequisito_titulo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] like @lV99WWRequisitoDS_24_Tfrequisito_titulo)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] = @AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] = @AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWRequisitoDS_26_Tfrequisito_restricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] like @lV101WWRequisitoDS_26_Tfrequisito_restricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] like @lV101WWRequisitoDS_26_Tfrequisito_restricao)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] = @AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] = @AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (0==AV103WWRequisitoDS_28_Tfrequisito_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] >= @AV103WWRequisitoDS_28_Tfrequisito_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] >= @AV103WWRequisitoDS_28_Tfrequisito_ordem)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (0==AV104WWRequisitoDS_29_Tfrequisito_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] <= @AV104WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] <= @AV104WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV105WWRequisitoDS_30_Tfrequisito_pontuacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] >= @AV105WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] >= @AV105WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] <= @AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] <= @AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV107WWRequisitoDS_32_Tfrequisito_datahomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] >= @AV107WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] >= @AV107WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] <= @AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] <= @AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( AV109WWRequisitoDS_34_Tfrequisito_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV109WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV109WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
         }
         if ( AV110WWRequisitoDS_35_Tfrequisito_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 1)";
            }
         }
         if ( AV110WWRequisitoDS_35_Tfrequisito_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Proposta_Objetivo]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00UV4( IGxContext context ,
                                             short A1934Requisito_Status ,
                                             IGxCollection AV109WWRequisitoDS_34_Tfrequisito_status_sels ,
                                             String AV76WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                             short AV77WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                             String AV78WWRequisitoDS_3_Requisito_descricao1 ,
                                             bool AV79WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                             String AV80WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                             short AV81WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                             String AV82WWRequisitoDS_7_Requisito_descricao2 ,
                                             bool AV83WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                             String AV84WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                             short AV85WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                             String AV86WWRequisitoDS_11_Requisito_descricao3 ,
                                             int AV87WWRequisitoDS_12_Tfrequisito_codigo ,
                                             int AV88WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                             String AV90WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                             String AV89WWRequisitoDS_14_Tfrequisito_descricao ,
                                             int AV91WWRequisitoDS_16_Tfproposta_codigo ,
                                             int AV92WWRequisitoDS_17_Tfproposta_codigo_to ,
                                             String AV94WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                             String AV93WWRequisitoDS_18_Tfproposta_objetivo ,
                                             int AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                             int AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                             String AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                             String AV97WWRequisitoDS_22_Tfrequisito_agrupador ,
                                             String AV100WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                             String AV99WWRequisitoDS_24_Tfrequisito_titulo ,
                                             String AV102WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                             String AV101WWRequisitoDS_26_Tfrequisito_restricao ,
                                             short AV103WWRequisitoDS_28_Tfrequisito_ordem ,
                                             short AV104WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                             decimal AV105WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                             decimal AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                             DateTime AV107WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                             DateTime AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                             int AV109WWRequisitoDS_34_Tfrequisito_status_sels_Count ,
                                             short AV110WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                             String A1923Requisito_Descricao ,
                                             int A1919Requisito_Codigo ,
                                             int A1685Proposta_Codigo ,
                                             String A1690Proposta_Objetivo ,
                                             int A2049Requisito_TipoReqCod ,
                                             String A1926Requisito_Agrupador ,
                                             String A1927Requisito_Titulo ,
                                             String A1929Requisito_Restricao ,
                                             short A1931Requisito_Ordem ,
                                             decimal A1932Requisito_Pontuacao ,
                                             DateTime A1933Requisito_DataHomologacao ,
                                             bool A1935Requisito_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [28] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Requisito_Agrupador], T1.[Requisito_Ativo], T1.[Requisito_Status], T1.[Requisito_DataHomologacao], T1.[Requisito_Pontuacao], T1.[Requisito_Ordem], T1.[Requisito_Restricao], T1.[Requisito_Titulo], T1.[Requisito_TipoReqCod], T2.[Proposta_Objetivo], T1.[Proposta_Codigo], T1.[Requisito_Codigo], T1.[Requisito_Descricao] FROM ([Requisito] T1 WITH (NOLOCK) LEFT JOIN dbo.[Proposta] T2 WITH (NOLOCK) ON T2.[Proposta_Codigo] = T1.[Proposta_Codigo])";
         if ( ( StringUtil.StrCmp(AV76WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV77WWRequisitoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV77WWRequisitoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV79WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV81WWRequisitoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV79WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV81WWRequisitoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV83WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV85WWRequisitoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV83WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV85WWRequisitoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! (0==AV87WWRequisitoDS_12_Tfrequisito_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] >= @AV87WWRequisitoDS_12_Tfrequisito_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] >= @AV87WWRequisitoDS_12_Tfrequisito_codigo)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! (0==AV88WWRequisitoDS_13_Tfrequisito_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] <= @AV88WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] <= @AV88WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWRequisitoDS_14_Tfrequisito_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV89WWRequisitoDS_14_Tfrequisito_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV89WWRequisitoDS_14_Tfrequisito_descricao)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] = @AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] = @AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! (0==AV91WWRequisitoDS_16_Tfproposta_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] >= @AV91WWRequisitoDS_16_Tfproposta_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] >= @AV91WWRequisitoDS_16_Tfproposta_codigo)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! (0==AV92WWRequisitoDS_17_Tfproposta_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] <= @AV92WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] <= @AV92WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWRequisitoDS_18_Tfproposta_objetivo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] like @lV93WWRequisitoDS_18_Tfproposta_objetivo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] like @lV93WWRequisitoDS_18_Tfproposta_objetivo)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] = @AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] = @AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (0==AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] >= @AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] >= @AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] <= @AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] <= @AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWRequisitoDS_22_Tfrequisito_agrupador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] like @lV97WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] like @lV97WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] = @AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] = @AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWRequisitoDS_24_Tfrequisito_titulo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] like @lV99WWRequisitoDS_24_Tfrequisito_titulo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] like @lV99WWRequisitoDS_24_Tfrequisito_titulo)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] = @AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] = @AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWRequisitoDS_26_Tfrequisito_restricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] like @lV101WWRequisitoDS_26_Tfrequisito_restricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] like @lV101WWRequisitoDS_26_Tfrequisito_restricao)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] = @AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] = @AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! (0==AV103WWRequisitoDS_28_Tfrequisito_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] >= @AV103WWRequisitoDS_28_Tfrequisito_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] >= @AV103WWRequisitoDS_28_Tfrequisito_ordem)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! (0==AV104WWRequisitoDS_29_Tfrequisito_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] <= @AV104WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] <= @AV104WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV105WWRequisitoDS_30_Tfrequisito_pontuacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] >= @AV105WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] >= @AV105WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] <= @AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] <= @AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV107WWRequisitoDS_32_Tfrequisito_datahomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] >= @AV107WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] >= @AV107WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] <= @AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] <= @AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( AV109WWRequisitoDS_34_Tfrequisito_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV109WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV109WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
         }
         if ( AV110WWRequisitoDS_35_Tfrequisito_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 1)";
            }
         }
         if ( AV110WWRequisitoDS_35_Tfrequisito_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Requisito_Agrupador]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00UV5( IGxContext context ,
                                             short A1934Requisito_Status ,
                                             IGxCollection AV109WWRequisitoDS_34_Tfrequisito_status_sels ,
                                             String AV76WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                             short AV77WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                             String AV78WWRequisitoDS_3_Requisito_descricao1 ,
                                             bool AV79WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                             String AV80WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                             short AV81WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                             String AV82WWRequisitoDS_7_Requisito_descricao2 ,
                                             bool AV83WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                             String AV84WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                             short AV85WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                             String AV86WWRequisitoDS_11_Requisito_descricao3 ,
                                             int AV87WWRequisitoDS_12_Tfrequisito_codigo ,
                                             int AV88WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                             String AV90WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                             String AV89WWRequisitoDS_14_Tfrequisito_descricao ,
                                             int AV91WWRequisitoDS_16_Tfproposta_codigo ,
                                             int AV92WWRequisitoDS_17_Tfproposta_codigo_to ,
                                             String AV94WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                             String AV93WWRequisitoDS_18_Tfproposta_objetivo ,
                                             int AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                             int AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                             String AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                             String AV97WWRequisitoDS_22_Tfrequisito_agrupador ,
                                             String AV100WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                             String AV99WWRequisitoDS_24_Tfrequisito_titulo ,
                                             String AV102WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                             String AV101WWRequisitoDS_26_Tfrequisito_restricao ,
                                             short AV103WWRequisitoDS_28_Tfrequisito_ordem ,
                                             short AV104WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                             decimal AV105WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                             decimal AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                             DateTime AV107WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                             DateTime AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                             int AV109WWRequisitoDS_34_Tfrequisito_status_sels_Count ,
                                             short AV110WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                             String A1923Requisito_Descricao ,
                                             int A1919Requisito_Codigo ,
                                             int A1685Proposta_Codigo ,
                                             String A1690Proposta_Objetivo ,
                                             int A2049Requisito_TipoReqCod ,
                                             String A1926Requisito_Agrupador ,
                                             String A1927Requisito_Titulo ,
                                             String A1929Requisito_Restricao ,
                                             short A1931Requisito_Ordem ,
                                             decimal A1932Requisito_Pontuacao ,
                                             DateTime A1933Requisito_DataHomologacao ,
                                             bool A1935Requisito_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [28] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[Requisito_Titulo], T1.[Requisito_Ativo], T1.[Requisito_Status], T1.[Requisito_DataHomologacao], T1.[Requisito_Pontuacao], T1.[Requisito_Ordem], T1.[Requisito_Restricao], T1.[Requisito_Agrupador], T1.[Requisito_TipoReqCod], T2.[Proposta_Objetivo], T1.[Proposta_Codigo], T1.[Requisito_Codigo], T1.[Requisito_Descricao] FROM ([Requisito] T1 WITH (NOLOCK) LEFT JOIN dbo.[Proposta] T2 WITH (NOLOCK) ON T2.[Proposta_Codigo] = T1.[Proposta_Codigo])";
         if ( ( StringUtil.StrCmp(AV76WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV77WWRequisitoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV77WWRequisitoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( AV79WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV81WWRequisitoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( AV79WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV81WWRequisitoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( AV83WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV85WWRequisitoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( AV83WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV85WWRequisitoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( ! (0==AV87WWRequisitoDS_12_Tfrequisito_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] >= @AV87WWRequisitoDS_12_Tfrequisito_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] >= @AV87WWRequisitoDS_12_Tfrequisito_codigo)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( ! (0==AV88WWRequisitoDS_13_Tfrequisito_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] <= @AV88WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] <= @AV88WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWRequisitoDS_14_Tfrequisito_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV89WWRequisitoDS_14_Tfrequisito_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV89WWRequisitoDS_14_Tfrequisito_descricao)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] = @AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] = @AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( ! (0==AV91WWRequisitoDS_16_Tfproposta_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] >= @AV91WWRequisitoDS_16_Tfproposta_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] >= @AV91WWRequisitoDS_16_Tfproposta_codigo)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( ! (0==AV92WWRequisitoDS_17_Tfproposta_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] <= @AV92WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] <= @AV92WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWRequisitoDS_18_Tfproposta_objetivo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] like @lV93WWRequisitoDS_18_Tfproposta_objetivo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] like @lV93WWRequisitoDS_18_Tfproposta_objetivo)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] = @AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] = @AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ! (0==AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] >= @AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] >= @AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( ! (0==AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] <= @AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] <= @AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWRequisitoDS_22_Tfrequisito_agrupador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] like @lV97WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] like @lV97WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] = @AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] = @AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWRequisitoDS_24_Tfrequisito_titulo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] like @lV99WWRequisitoDS_24_Tfrequisito_titulo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] like @lV99WWRequisitoDS_24_Tfrequisito_titulo)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] = @AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] = @AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWRequisitoDS_26_Tfrequisito_restricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] like @lV101WWRequisitoDS_26_Tfrequisito_restricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] like @lV101WWRequisitoDS_26_Tfrequisito_restricao)";
            }
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] = @AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] = @AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( ! (0==AV103WWRequisitoDS_28_Tfrequisito_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] >= @AV103WWRequisitoDS_28_Tfrequisito_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] >= @AV103WWRequisitoDS_28_Tfrequisito_ordem)";
            }
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( ! (0==AV104WWRequisitoDS_29_Tfrequisito_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] <= @AV104WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] <= @AV104WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV105WWRequisitoDS_30_Tfrequisito_pontuacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] >= @AV105WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] >= @AV105WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
         }
         else
         {
            GXv_int7[24] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] <= @AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] <= @AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
         }
         else
         {
            GXv_int7[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV107WWRequisitoDS_32_Tfrequisito_datahomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] >= @AV107WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] >= @AV107WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
         }
         else
         {
            GXv_int7[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] <= @AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] <= @AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
         }
         else
         {
            GXv_int7[27] = 1;
         }
         if ( AV109WWRequisitoDS_34_Tfrequisito_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV109WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV109WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
         }
         if ( AV110WWRequisitoDS_35_Tfrequisito_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 1)";
            }
         }
         if ( AV110WWRequisitoDS_35_Tfrequisito_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Requisito_Titulo]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00UV6( IGxContext context ,
                                             short A1934Requisito_Status ,
                                             IGxCollection AV109WWRequisitoDS_34_Tfrequisito_status_sels ,
                                             String AV76WWRequisitoDS_1_Dynamicfiltersselector1 ,
                                             short AV77WWRequisitoDS_2_Dynamicfiltersoperator1 ,
                                             String AV78WWRequisitoDS_3_Requisito_descricao1 ,
                                             bool AV79WWRequisitoDS_4_Dynamicfiltersenabled2 ,
                                             String AV80WWRequisitoDS_5_Dynamicfiltersselector2 ,
                                             short AV81WWRequisitoDS_6_Dynamicfiltersoperator2 ,
                                             String AV82WWRequisitoDS_7_Requisito_descricao2 ,
                                             bool AV83WWRequisitoDS_8_Dynamicfiltersenabled3 ,
                                             String AV84WWRequisitoDS_9_Dynamicfiltersselector3 ,
                                             short AV85WWRequisitoDS_10_Dynamicfiltersoperator3 ,
                                             String AV86WWRequisitoDS_11_Requisito_descricao3 ,
                                             int AV87WWRequisitoDS_12_Tfrequisito_codigo ,
                                             int AV88WWRequisitoDS_13_Tfrequisito_codigo_to ,
                                             String AV90WWRequisitoDS_15_Tfrequisito_descricao_sel ,
                                             String AV89WWRequisitoDS_14_Tfrequisito_descricao ,
                                             int AV91WWRequisitoDS_16_Tfproposta_codigo ,
                                             int AV92WWRequisitoDS_17_Tfproposta_codigo_to ,
                                             String AV94WWRequisitoDS_19_Tfproposta_objetivo_sel ,
                                             String AV93WWRequisitoDS_18_Tfproposta_objetivo ,
                                             int AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod ,
                                             int AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to ,
                                             String AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel ,
                                             String AV97WWRequisitoDS_22_Tfrequisito_agrupador ,
                                             String AV100WWRequisitoDS_25_Tfrequisito_titulo_sel ,
                                             String AV99WWRequisitoDS_24_Tfrequisito_titulo ,
                                             String AV102WWRequisitoDS_27_Tfrequisito_restricao_sel ,
                                             String AV101WWRequisitoDS_26_Tfrequisito_restricao ,
                                             short AV103WWRequisitoDS_28_Tfrequisito_ordem ,
                                             short AV104WWRequisitoDS_29_Tfrequisito_ordem_to ,
                                             decimal AV105WWRequisitoDS_30_Tfrequisito_pontuacao ,
                                             decimal AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to ,
                                             DateTime AV107WWRequisitoDS_32_Tfrequisito_datahomologacao ,
                                             DateTime AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to ,
                                             int AV109WWRequisitoDS_34_Tfrequisito_status_sels_Count ,
                                             short AV110WWRequisitoDS_35_Tfrequisito_ativo_sel ,
                                             String A1923Requisito_Descricao ,
                                             int A1919Requisito_Codigo ,
                                             int A1685Proposta_Codigo ,
                                             String A1690Proposta_Objetivo ,
                                             int A2049Requisito_TipoReqCod ,
                                             String A1926Requisito_Agrupador ,
                                             String A1927Requisito_Titulo ,
                                             String A1929Requisito_Restricao ,
                                             short A1931Requisito_Ordem ,
                                             decimal A1932Requisito_Pontuacao ,
                                             DateTime A1933Requisito_DataHomologacao ,
                                             bool A1935Requisito_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [28] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[Requisito_Restricao], T1.[Requisito_Ativo], T1.[Requisito_Status], T1.[Requisito_DataHomologacao], T1.[Requisito_Pontuacao], T1.[Requisito_Ordem], T1.[Requisito_Titulo], T1.[Requisito_Agrupador], T1.[Requisito_TipoReqCod], T2.[Proposta_Objetivo], T1.[Proposta_Codigo], T1.[Requisito_Codigo], T1.[Requisito_Descricao] FROM ([Requisito] T1 WITH (NOLOCK) LEFT JOIN dbo.[Proposta] T2 WITH (NOLOCK) ON T2.[Proposta_Codigo] = T1.[Proposta_Codigo])";
         if ( ( StringUtil.StrCmp(AV76WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV77WWRequisitoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int9[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWRequisitoDS_1_Dynamicfiltersselector1, "REQUISITO_DESCRICAO") == 0 ) && ( AV77WWRequisitoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWRequisitoDS_3_Requisito_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV78WWRequisitoDS_3_Requisito_descricao1)";
            }
         }
         else
         {
            GXv_int9[1] = 1;
         }
         if ( AV79WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV81WWRequisitoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int9[2] = 1;
         }
         if ( AV79WWRequisitoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWRequisitoDS_5_Dynamicfiltersselector2, "REQUISITO_DESCRICAO") == 0 ) && ( AV81WWRequisitoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWRequisitoDS_7_Requisito_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV82WWRequisitoDS_7_Requisito_descricao2)";
            }
         }
         else
         {
            GXv_int9[3] = 1;
         }
         if ( AV83WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV85WWRequisitoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int9[4] = 1;
         }
         if ( AV83WWRequisitoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWRequisitoDS_9_Dynamicfiltersselector3, "REQUISITO_DESCRICAO") == 0 ) && ( AV85WWRequisitoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWRequisitoDS_11_Requisito_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like '%' + @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like '%' + @lV86WWRequisitoDS_11_Requisito_descricao3)";
            }
         }
         else
         {
            GXv_int9[5] = 1;
         }
         if ( ! (0==AV87WWRequisitoDS_12_Tfrequisito_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] >= @AV87WWRequisitoDS_12_Tfrequisito_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] >= @AV87WWRequisitoDS_12_Tfrequisito_codigo)";
            }
         }
         else
         {
            GXv_int9[6] = 1;
         }
         if ( ! (0==AV88WWRequisitoDS_13_Tfrequisito_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Codigo] <= @AV88WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Codigo] <= @AV88WWRequisitoDS_13_Tfrequisito_codigo_to)";
            }
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWRequisitoDS_14_Tfrequisito_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] like @lV89WWRequisitoDS_14_Tfrequisito_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] like @lV89WWRequisitoDS_14_Tfrequisito_descricao)";
            }
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Descricao] = @AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Descricao] = @AV90WWRequisitoDS_15_Tfrequisito_descricao_sel)";
            }
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( ! (0==AV91WWRequisitoDS_16_Tfproposta_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] >= @AV91WWRequisitoDS_16_Tfproposta_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] >= @AV91WWRequisitoDS_16_Tfproposta_codigo)";
            }
         }
         else
         {
            GXv_int9[10] = 1;
         }
         if ( ! (0==AV92WWRequisitoDS_17_Tfproposta_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Proposta_Codigo] <= @AV92WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Proposta_Codigo] <= @AV92WWRequisitoDS_17_Tfproposta_codigo_to)";
            }
         }
         else
         {
            GXv_int9[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWRequisitoDS_18_Tfproposta_objetivo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] like @lV93WWRequisitoDS_18_Tfproposta_objetivo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] like @lV93WWRequisitoDS_18_Tfproposta_objetivo)";
            }
         }
         else
         {
            GXv_int9[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Proposta_Objetivo] = @AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Proposta_Objetivo] = @AV94WWRequisitoDS_19_Tfproposta_objetivo_sel)";
            }
         }
         else
         {
            GXv_int9[13] = 1;
         }
         if ( ! (0==AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] >= @AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] >= @AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod)";
            }
         }
         else
         {
            GXv_int9[14] = 1;
         }
         if ( ! (0==AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_TipoReqCod] <= @AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_TipoReqCod] <= @AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to)";
            }
         }
         else
         {
            GXv_int9[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWRequisitoDS_22_Tfrequisito_agrupador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] like @lV97WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] like @lV97WWRequisitoDS_22_Tfrequisito_agrupador)";
            }
         }
         else
         {
            GXv_int9[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Agrupador] = @AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Agrupador] = @AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel)";
            }
         }
         else
         {
            GXv_int9[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWRequisitoDS_24_Tfrequisito_titulo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] like @lV99WWRequisitoDS_24_Tfrequisito_titulo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] like @lV99WWRequisitoDS_24_Tfrequisito_titulo)";
            }
         }
         else
         {
            GXv_int9[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Titulo] = @AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Titulo] = @AV100WWRequisitoDS_25_Tfrequisito_titulo_sel)";
            }
         }
         else
         {
            GXv_int9[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWRequisitoDS_26_Tfrequisito_restricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] like @lV101WWRequisitoDS_26_Tfrequisito_restricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] like @lV101WWRequisitoDS_26_Tfrequisito_restricao)";
            }
         }
         else
         {
            GXv_int9[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Restricao] = @AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Restricao] = @AV102WWRequisitoDS_27_Tfrequisito_restricao_sel)";
            }
         }
         else
         {
            GXv_int9[21] = 1;
         }
         if ( ! (0==AV103WWRequisitoDS_28_Tfrequisito_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] >= @AV103WWRequisitoDS_28_Tfrequisito_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] >= @AV103WWRequisitoDS_28_Tfrequisito_ordem)";
            }
         }
         else
         {
            GXv_int9[22] = 1;
         }
         if ( ! (0==AV104WWRequisitoDS_29_Tfrequisito_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ordem] <= @AV104WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ordem] <= @AV104WWRequisitoDS_29_Tfrequisito_ordem_to)";
            }
         }
         else
         {
            GXv_int9[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV105WWRequisitoDS_30_Tfrequisito_pontuacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] >= @AV105WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] >= @AV105WWRequisitoDS_30_Tfrequisito_pontuacao)";
            }
         }
         else
         {
            GXv_int9[24] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Pontuacao] <= @AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Pontuacao] <= @AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to)";
            }
         }
         else
         {
            GXv_int9[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV107WWRequisitoDS_32_Tfrequisito_datahomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] >= @AV107WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] >= @AV107WWRequisitoDS_32_Tfrequisito_datahomologacao)";
            }
         }
         else
         {
            GXv_int9[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_DataHomologacao] <= @AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_DataHomologacao] <= @AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to)";
            }
         }
         else
         {
            GXv_int9[27] = 1;
         }
         if ( AV109WWRequisitoDS_34_Tfrequisito_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV109WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV109WWRequisitoDS_34_Tfrequisito_status_sels, "T1.[Requisito_Status] IN (", ")") + ")";
            }
         }
         if ( AV110WWRequisitoDS_35_Tfrequisito_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 1)";
            }
         }
         if ( AV110WWRequisitoDS_35_Tfrequisito_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Requisito_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Requisito_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Requisito_Restricao]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UV2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (short)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (int)dynConstraints[35] , (short)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (short)dynConstraints[45] , (decimal)dynConstraints[46] , (DateTime)dynConstraints[47] , (bool)dynConstraints[48] );
               case 1 :
                     return conditional_P00UV3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (short)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (int)dynConstraints[35] , (short)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (short)dynConstraints[45] , (decimal)dynConstraints[46] , (DateTime)dynConstraints[47] , (bool)dynConstraints[48] );
               case 2 :
                     return conditional_P00UV4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (short)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (int)dynConstraints[35] , (short)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (short)dynConstraints[45] , (decimal)dynConstraints[46] , (DateTime)dynConstraints[47] , (bool)dynConstraints[48] );
               case 3 :
                     return conditional_P00UV5(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (short)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (int)dynConstraints[35] , (short)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (short)dynConstraints[45] , (decimal)dynConstraints[46] , (DateTime)dynConstraints[47] , (bool)dynConstraints[48] );
               case 4 :
                     return conditional_P00UV6(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (short)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (int)dynConstraints[35] , (short)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (short)dynConstraints[45] , (decimal)dynConstraints[46] , (DateTime)dynConstraints[47] , (bool)dynConstraints[48] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UV2 ;
          prmP00UV2 = new Object[] {
          new Object[] {"@lV78WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV78WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV82WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV82WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV86WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV86WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV87WWRequisitoDS_12_Tfrequisito_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV88WWRequisitoDS_13_Tfrequisito_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV89WWRequisitoDS_14_Tfrequisito_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV90WWRequisitoDS_15_Tfrequisito_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV91WWRequisitoDS_16_Tfproposta_codigo",SqlDbType.Int,9,0} ,
          new Object[] {"@AV92WWRequisitoDS_17_Tfproposta_codigo_to",SqlDbType.Int,9,0} ,
          new Object[] {"@lV93WWRequisitoDS_18_Tfproposta_objetivo",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV94WWRequisitoDS_19_Tfproposta_objetivo_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV97WWRequisitoDS_22_Tfrequisito_agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel",SqlDbType.VarChar,25,0} ,
          new Object[] {"@lV99WWRequisitoDS_24_Tfrequisito_titulo",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV100WWRequisitoDS_25_Tfrequisito_titulo_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@lV101WWRequisitoDS_26_Tfrequisito_restricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV102WWRequisitoDS_27_Tfrequisito_restricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV103WWRequisitoDS_28_Tfrequisito_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV104WWRequisitoDS_29_Tfrequisito_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV105WWRequisitoDS_30_Tfrequisito_pontuacao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV107WWRequisitoDS_32_Tfrequisito_datahomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP00UV3 ;
          prmP00UV3 = new Object[] {
          new Object[] {"@lV78WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV78WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV82WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV82WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV86WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV86WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV87WWRequisitoDS_12_Tfrequisito_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV88WWRequisitoDS_13_Tfrequisito_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV89WWRequisitoDS_14_Tfrequisito_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV90WWRequisitoDS_15_Tfrequisito_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV91WWRequisitoDS_16_Tfproposta_codigo",SqlDbType.Int,9,0} ,
          new Object[] {"@AV92WWRequisitoDS_17_Tfproposta_codigo_to",SqlDbType.Int,9,0} ,
          new Object[] {"@lV93WWRequisitoDS_18_Tfproposta_objetivo",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV94WWRequisitoDS_19_Tfproposta_objetivo_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV97WWRequisitoDS_22_Tfrequisito_agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel",SqlDbType.VarChar,25,0} ,
          new Object[] {"@lV99WWRequisitoDS_24_Tfrequisito_titulo",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV100WWRequisitoDS_25_Tfrequisito_titulo_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@lV101WWRequisitoDS_26_Tfrequisito_restricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV102WWRequisitoDS_27_Tfrequisito_restricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV103WWRequisitoDS_28_Tfrequisito_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV104WWRequisitoDS_29_Tfrequisito_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV105WWRequisitoDS_30_Tfrequisito_pontuacao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV107WWRequisitoDS_32_Tfrequisito_datahomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP00UV4 ;
          prmP00UV4 = new Object[] {
          new Object[] {"@lV78WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV78WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV82WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV82WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV86WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV86WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV87WWRequisitoDS_12_Tfrequisito_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV88WWRequisitoDS_13_Tfrequisito_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV89WWRequisitoDS_14_Tfrequisito_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV90WWRequisitoDS_15_Tfrequisito_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV91WWRequisitoDS_16_Tfproposta_codigo",SqlDbType.Int,9,0} ,
          new Object[] {"@AV92WWRequisitoDS_17_Tfproposta_codigo_to",SqlDbType.Int,9,0} ,
          new Object[] {"@lV93WWRequisitoDS_18_Tfproposta_objetivo",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV94WWRequisitoDS_19_Tfproposta_objetivo_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV97WWRequisitoDS_22_Tfrequisito_agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel",SqlDbType.VarChar,25,0} ,
          new Object[] {"@lV99WWRequisitoDS_24_Tfrequisito_titulo",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV100WWRequisitoDS_25_Tfrequisito_titulo_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@lV101WWRequisitoDS_26_Tfrequisito_restricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV102WWRequisitoDS_27_Tfrequisito_restricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV103WWRequisitoDS_28_Tfrequisito_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV104WWRequisitoDS_29_Tfrequisito_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV105WWRequisitoDS_30_Tfrequisito_pontuacao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV107WWRequisitoDS_32_Tfrequisito_datahomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP00UV5 ;
          prmP00UV5 = new Object[] {
          new Object[] {"@lV78WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV78WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV82WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV82WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV86WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV86WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV87WWRequisitoDS_12_Tfrequisito_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV88WWRequisitoDS_13_Tfrequisito_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV89WWRequisitoDS_14_Tfrequisito_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV90WWRequisitoDS_15_Tfrequisito_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV91WWRequisitoDS_16_Tfproposta_codigo",SqlDbType.Int,9,0} ,
          new Object[] {"@AV92WWRequisitoDS_17_Tfproposta_codigo_to",SqlDbType.Int,9,0} ,
          new Object[] {"@lV93WWRequisitoDS_18_Tfproposta_objetivo",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV94WWRequisitoDS_19_Tfproposta_objetivo_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV97WWRequisitoDS_22_Tfrequisito_agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel",SqlDbType.VarChar,25,0} ,
          new Object[] {"@lV99WWRequisitoDS_24_Tfrequisito_titulo",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV100WWRequisitoDS_25_Tfrequisito_titulo_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@lV101WWRequisitoDS_26_Tfrequisito_restricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV102WWRequisitoDS_27_Tfrequisito_restricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV103WWRequisitoDS_28_Tfrequisito_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV104WWRequisitoDS_29_Tfrequisito_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV105WWRequisitoDS_30_Tfrequisito_pontuacao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV107WWRequisitoDS_32_Tfrequisito_datahomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP00UV6 ;
          prmP00UV6 = new Object[] {
          new Object[] {"@lV78WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV78WWRequisitoDS_3_Requisito_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV82WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV82WWRequisitoDS_7_Requisito_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV86WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV86WWRequisitoDS_11_Requisito_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV87WWRequisitoDS_12_Tfrequisito_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV88WWRequisitoDS_13_Tfrequisito_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV89WWRequisitoDS_14_Tfrequisito_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV90WWRequisitoDS_15_Tfrequisito_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV91WWRequisitoDS_16_Tfproposta_codigo",SqlDbType.Int,9,0} ,
          new Object[] {"@AV92WWRequisitoDS_17_Tfproposta_codigo_to",SqlDbType.Int,9,0} ,
          new Object[] {"@lV93WWRequisitoDS_18_Tfproposta_objetivo",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV94WWRequisitoDS_19_Tfproposta_objetivo_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV95WWRequisitoDS_20_Tfrequisito_tiporeqcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV96WWRequisitoDS_21_Tfrequisito_tiporeqcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV97WWRequisitoDS_22_Tfrequisito_agrupador",SqlDbType.VarChar,25,0} ,
          new Object[] {"@AV98WWRequisitoDS_23_Tfrequisito_agrupador_sel",SqlDbType.VarChar,25,0} ,
          new Object[] {"@lV99WWRequisitoDS_24_Tfrequisito_titulo",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV100WWRequisitoDS_25_Tfrequisito_titulo_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@lV101WWRequisitoDS_26_Tfrequisito_restricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV102WWRequisitoDS_27_Tfrequisito_restricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV103WWRequisitoDS_28_Tfrequisito_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV104WWRequisitoDS_29_Tfrequisito_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV105WWRequisitoDS_30_Tfrequisito_pontuacao",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV106WWRequisitoDS_31_Tfrequisito_pontuacao_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV107WWRequisitoDS_32_Tfrequisito_datahomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV108WWRequisitoDS_33_Tfrequisito_datahomologacao_to",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UV2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UV2,100,0,true,false )
             ,new CursorDef("P00UV3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UV3,100,0,true,false )
             ,new CursorDef("P00UV4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UV4,100,0,true,false )
             ,new CursorDef("P00UV5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UV5,100,0,true,false )
             ,new CursorDef("P00UV6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UV6,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getLongVarchar(11) ;
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((String[]) buf[20])[0] = rslt.getLongVarchar(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(10) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((String[]) buf[20])[0] = rslt.getLongVarchar(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(10) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((String[]) buf[20])[0] = rslt.getLongVarchar(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(10) ;
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((String[]) buf[20])[0] = rslt.getLongVarchar(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[50]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[51]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[52]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[53]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[55]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[50]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[51]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[52]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[53]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[55]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[50]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[51]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[52]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[53]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[55]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[50]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[51]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[52]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[53]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[55]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[50]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[51]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[52]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[53]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[55]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwrequisitofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwrequisitofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwrequisitofilterdata") )
          {
             return  ;
          }
          getwwrequisitofilterdata worker = new getwwrequisitofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
