/*
               File: GetWWAmbienteTecnologicoFilterData
        Description: Get WWAmbiente Tecnologico Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:59.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwambientetecnologicofilterdata : GXProcedure
   {
      public getwwambientetecnologicofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwambientetecnologicofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
         return AV32OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwambientetecnologicofilterdata objgetwwambientetecnologicofilterdata;
         objgetwwambientetecnologicofilterdata = new getwwambientetecnologicofilterdata();
         objgetwwambientetecnologicofilterdata.AV23DDOName = aP0_DDOName;
         objgetwwambientetecnologicofilterdata.AV21SearchTxt = aP1_SearchTxt;
         objgetwwambientetecnologicofilterdata.AV22SearchTxtTo = aP2_SearchTxtTo;
         objgetwwambientetecnologicofilterdata.AV27OptionsJson = "" ;
         objgetwwambientetecnologicofilterdata.AV30OptionsDescJson = "" ;
         objgetwwambientetecnologicofilterdata.AV32OptionIndexesJson = "" ;
         objgetwwambientetecnologicofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwambientetecnologicofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwambientetecnologicofilterdata);
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwambientetecnologicofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV26Options = (IGxCollection)(new GxSimpleCollection());
         AV29OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV31OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADAMBIENTETECNOLOGICO_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV27OptionsJson = AV26Options.ToJSonString(false);
         AV30OptionsDescJson = AV29OptionsDesc.ToJSonString(false);
         AV32OptionIndexesJson = AV31OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV34Session.Get("WWAmbienteTecnologicoGridState"), "") == 0 )
         {
            AV36GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWAmbienteTecnologicoGridState"), "");
         }
         else
         {
            AV36GridState.FromXml(AV34Session.Get("WWAmbienteTecnologicoGridState"), "");
         }
         AV50GXV1 = 1;
         while ( AV50GXV1 <= AV36GridState.gxTpr_Filtervalues.Count )
         {
            AV37GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV36GridState.gxTpr_Filtervalues.Item(AV50GXV1));
            if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "AMBIENTETECNOLOGICO_AREATRABALHOCOD") == 0 )
            {
               AV39AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV10TFAmbienteTecnologico_Descricao = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO_SEL") == 0 )
            {
               AV11TFAmbienteTecnologico_Descricao_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_TAXAENTREGADSNV") == 0 )
            {
               AV12TFAmbienteTecnologico_TaxaEntregaDsnv = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
               AV13TFAmbienteTecnologico_TaxaEntregaDsnv_To = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR") == 0 )
            {
               AV14TFAmbienteTecnologico_TaxaEntregaMlhr = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
               AV15TFAmbienteTecnologico_TaxaEntregaMlhr_To = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_LOCPF") == 0 )
            {
               AV16TFAmbienteTecnologico_LocPF = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
               AV17TFAmbienteTecnologico_LocPF_To = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_ATIVO_SEL") == 0 )
            {
               AV18TFAmbienteTecnologico_Ativo_Sel = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_FATORAJUSTE") == 0 )
            {
               AV19TFAmbienteTecnologico_FatorAjuste = NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, ".");
               AV20TFAmbienteTecnologico_FatorAjuste_To = NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV50GXV1 = (int)(AV50GXV1+1);
         }
         if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(1));
            AV40DynamicFiltersSelector1 = AV38GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV41AmbienteTecnologico_Descricao1 = AV38GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV42DynamicFiltersEnabled2 = true;
               AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(2));
               AV43DynamicFiltersSelector2 = AV38GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV43DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
               {
                  AV44AmbienteTecnologico_Descricao2 = AV38GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV45DynamicFiltersEnabled3 = true;
                  AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(3));
                  AV46DynamicFiltersSelector3 = AV38GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
                  {
                     AV47AmbienteTecnologico_Descricao3 = AV38GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADAMBIENTETECNOLOGICO_DESCRICAOOPTIONS' Routine */
         AV10TFAmbienteTecnologico_Descricao = AV21SearchTxt;
         AV11TFAmbienteTecnologico_Descricao_Sel = "";
         AV52WWAmbienteTecnologicoDS_1_Ambientetecnologico_areatrabalhocod = AV39AmbienteTecnologico_AreaTrabalhoCod;
         AV53WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV54WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = AV41AmbienteTecnologico_Descricao1;
         AV55WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 = AV42DynamicFiltersEnabled2;
         AV56WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 = AV43DynamicFiltersSelector2;
         AV57WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = AV44AmbienteTecnologico_Descricao2;
         AV58WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 = AV45DynamicFiltersEnabled3;
         AV59WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 = AV46DynamicFiltersSelector3;
         AV60WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = AV47AmbienteTecnologico_Descricao3;
         AV61WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = AV10TFAmbienteTecnologico_Descricao;
         AV62WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel = AV11TFAmbienteTecnologico_Descricao_Sel;
         AV63WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv = AV12TFAmbienteTecnologico_TaxaEntregaDsnv;
         AV64WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to = AV13TFAmbienteTecnologico_TaxaEntregaDsnv_To;
         AV65WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr = AV14TFAmbienteTecnologico_TaxaEntregaMlhr;
         AV66WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to = AV15TFAmbienteTecnologico_TaxaEntregaMlhr_To;
         AV67WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf = AV16TFAmbienteTecnologico_LocPF;
         AV68WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to = AV17TFAmbienteTecnologico_LocPF_To;
         AV69WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel = AV18TFAmbienteTecnologico_Ativo_Sel;
         AV70WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste = AV19TFAmbienteTecnologico_FatorAjuste;
         AV71WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to = AV20TFAmbienteTecnologico_FatorAjuste_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV53WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 ,
                                              AV54WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 ,
                                              AV55WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 ,
                                              AV56WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 ,
                                              AV57WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 ,
                                              AV58WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 ,
                                              AV59WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 ,
                                              AV60WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 ,
                                              AV62WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel ,
                                              AV61WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao ,
                                              AV63WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv ,
                                              AV64WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to ,
                                              AV65WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr ,
                                              AV66WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to ,
                                              AV67WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf ,
                                              AV68WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to ,
                                              AV69WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel ,
                                              AV70WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste ,
                                              AV71WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A976AmbienteTecnologico_TaxaEntregaDsnv ,
                                              A977AmbienteTecnologico_TaxaEntregaMlhr ,
                                              A978AmbienteTecnologico_LocPF ,
                                              A353AmbienteTecnologico_Ativo ,
                                              A1423AmbienteTecnologico_FatorAjuste ,
                                              A728AmbienteTecnologico_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV54WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV54WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1), "%", "");
         lV57WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV57WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2), "%", "");
         lV60WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV60WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3), "%", "");
         lV61WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = StringUtil.Concat( StringUtil.RTrim( AV61WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao), "%", "");
         /* Using cursor P00LV2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV54WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1, lV57WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2, lV60WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3, lV61WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao, AV62WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel, AV63WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv, AV64WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to, AV65WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr, AV66WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to, AV67WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf, AV68WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to, AV70WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste, AV71WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKLV2 = false;
            A728AmbienteTecnologico_AreaTrabalhoCod = P00LV2_A728AmbienteTecnologico_AreaTrabalhoCod[0];
            A352AmbienteTecnologico_Descricao = P00LV2_A352AmbienteTecnologico_Descricao[0];
            A1423AmbienteTecnologico_FatorAjuste = P00LV2_A1423AmbienteTecnologico_FatorAjuste[0];
            n1423AmbienteTecnologico_FatorAjuste = P00LV2_n1423AmbienteTecnologico_FatorAjuste[0];
            A353AmbienteTecnologico_Ativo = P00LV2_A353AmbienteTecnologico_Ativo[0];
            A978AmbienteTecnologico_LocPF = P00LV2_A978AmbienteTecnologico_LocPF[0];
            n978AmbienteTecnologico_LocPF = P00LV2_n978AmbienteTecnologico_LocPF[0];
            A977AmbienteTecnologico_TaxaEntregaMlhr = P00LV2_A977AmbienteTecnologico_TaxaEntregaMlhr[0];
            n977AmbienteTecnologico_TaxaEntregaMlhr = P00LV2_n977AmbienteTecnologico_TaxaEntregaMlhr[0];
            A976AmbienteTecnologico_TaxaEntregaDsnv = P00LV2_A976AmbienteTecnologico_TaxaEntregaDsnv[0];
            n976AmbienteTecnologico_TaxaEntregaDsnv = P00LV2_n976AmbienteTecnologico_TaxaEntregaDsnv[0];
            A351AmbienteTecnologico_Codigo = P00LV2_A351AmbienteTecnologico_Codigo[0];
            AV33count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00LV2_A352AmbienteTecnologico_Descricao[0], A352AmbienteTecnologico_Descricao) == 0 ) )
            {
               BRKLV2 = false;
               A351AmbienteTecnologico_Codigo = P00LV2_A351AmbienteTecnologico_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKLV2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A352AmbienteTecnologico_Descricao)) )
            {
               AV25Option = A352AmbienteTecnologico_Descricao;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLV2 )
            {
               BRKLV2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV26Options = new GxSimpleCollection();
         AV29OptionsDesc = new GxSimpleCollection();
         AV31OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV34Session = context.GetSession();
         AV36GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV37GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFAmbienteTecnologico_Descricao = "";
         AV11TFAmbienteTecnologico_Descricao_Sel = "";
         AV38GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV40DynamicFiltersSelector1 = "";
         AV41AmbienteTecnologico_Descricao1 = "";
         AV43DynamicFiltersSelector2 = "";
         AV44AmbienteTecnologico_Descricao2 = "";
         AV46DynamicFiltersSelector3 = "";
         AV47AmbienteTecnologico_Descricao3 = "";
         AV53WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 = "";
         AV54WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = "";
         AV56WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 = "";
         AV57WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = "";
         AV59WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 = "";
         AV60WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = "";
         AV61WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = "";
         AV62WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel = "";
         scmdbuf = "";
         lV54WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = "";
         lV57WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = "";
         lV60WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = "";
         lV61WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = "";
         A352AmbienteTecnologico_Descricao = "";
         P00LV2_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         P00LV2_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00LV2_A1423AmbienteTecnologico_FatorAjuste = new decimal[1] ;
         P00LV2_n1423AmbienteTecnologico_FatorAjuste = new bool[] {false} ;
         P00LV2_A353AmbienteTecnologico_Ativo = new bool[] {false} ;
         P00LV2_A978AmbienteTecnologico_LocPF = new short[1] ;
         P00LV2_n978AmbienteTecnologico_LocPF = new bool[] {false} ;
         P00LV2_A977AmbienteTecnologico_TaxaEntregaMlhr = new short[1] ;
         P00LV2_n977AmbienteTecnologico_TaxaEntregaMlhr = new bool[] {false} ;
         P00LV2_A976AmbienteTecnologico_TaxaEntregaDsnv = new short[1] ;
         P00LV2_n976AmbienteTecnologico_TaxaEntregaDsnv = new bool[] {false} ;
         P00LV2_A351AmbienteTecnologico_Codigo = new int[1] ;
         AV25Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwambientetecnologicofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00LV2_A728AmbienteTecnologico_AreaTrabalhoCod, P00LV2_A352AmbienteTecnologico_Descricao, P00LV2_A1423AmbienteTecnologico_FatorAjuste, P00LV2_n1423AmbienteTecnologico_FatorAjuste, P00LV2_A353AmbienteTecnologico_Ativo, P00LV2_A978AmbienteTecnologico_LocPF, P00LV2_n978AmbienteTecnologico_LocPF, P00LV2_A977AmbienteTecnologico_TaxaEntregaMlhr, P00LV2_n977AmbienteTecnologico_TaxaEntregaMlhr, P00LV2_A976AmbienteTecnologico_TaxaEntregaDsnv,
               P00LV2_n976AmbienteTecnologico_TaxaEntregaDsnv, P00LV2_A351AmbienteTecnologico_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12TFAmbienteTecnologico_TaxaEntregaDsnv ;
      private short AV13TFAmbienteTecnologico_TaxaEntregaDsnv_To ;
      private short AV14TFAmbienteTecnologico_TaxaEntregaMlhr ;
      private short AV15TFAmbienteTecnologico_TaxaEntregaMlhr_To ;
      private short AV16TFAmbienteTecnologico_LocPF ;
      private short AV17TFAmbienteTecnologico_LocPF_To ;
      private short AV18TFAmbienteTecnologico_Ativo_Sel ;
      private short AV63WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv ;
      private short AV64WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to ;
      private short AV65WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr ;
      private short AV66WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to ;
      private short AV67WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf ;
      private short AV68WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to ;
      private short AV69WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel ;
      private short A976AmbienteTecnologico_TaxaEntregaDsnv ;
      private short A977AmbienteTecnologico_TaxaEntregaMlhr ;
      private short A978AmbienteTecnologico_LocPF ;
      private int AV50GXV1 ;
      private int AV39AmbienteTecnologico_AreaTrabalhoCod ;
      private int AV52WWAmbienteTecnologicoDS_1_Ambientetecnologico_areatrabalhocod ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A728AmbienteTecnologico_AreaTrabalhoCod ;
      private int A351AmbienteTecnologico_Codigo ;
      private long AV33count ;
      private decimal AV19TFAmbienteTecnologico_FatorAjuste ;
      private decimal AV20TFAmbienteTecnologico_FatorAjuste_To ;
      private decimal AV70WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste ;
      private decimal AV71WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to ;
      private decimal A1423AmbienteTecnologico_FatorAjuste ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV42DynamicFiltersEnabled2 ;
      private bool AV45DynamicFiltersEnabled3 ;
      private bool AV55WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 ;
      private bool AV58WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 ;
      private bool A353AmbienteTecnologico_Ativo ;
      private bool BRKLV2 ;
      private bool n1423AmbienteTecnologico_FatorAjuste ;
      private bool n978AmbienteTecnologico_LocPF ;
      private bool n977AmbienteTecnologico_TaxaEntregaMlhr ;
      private bool n976AmbienteTecnologico_TaxaEntregaDsnv ;
      private String AV32OptionIndexesJson ;
      private String AV27OptionsJson ;
      private String AV30OptionsDescJson ;
      private String AV23DDOName ;
      private String AV21SearchTxt ;
      private String AV22SearchTxtTo ;
      private String AV10TFAmbienteTecnologico_Descricao ;
      private String AV11TFAmbienteTecnologico_Descricao_Sel ;
      private String AV40DynamicFiltersSelector1 ;
      private String AV41AmbienteTecnologico_Descricao1 ;
      private String AV43DynamicFiltersSelector2 ;
      private String AV44AmbienteTecnologico_Descricao2 ;
      private String AV46DynamicFiltersSelector3 ;
      private String AV47AmbienteTecnologico_Descricao3 ;
      private String AV53WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 ;
      private String AV54WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 ;
      private String AV56WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 ;
      private String AV57WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 ;
      private String AV59WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 ;
      private String AV60WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 ;
      private String AV61WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao ;
      private String AV62WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel ;
      private String lV54WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 ;
      private String lV57WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 ;
      private String lV60WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 ;
      private String lV61WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao ;
      private String A352AmbienteTecnologico_Descricao ;
      private String AV25Option ;
      private IGxSession AV34Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00LV2_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private String[] P00LV2_A352AmbienteTecnologico_Descricao ;
      private decimal[] P00LV2_A1423AmbienteTecnologico_FatorAjuste ;
      private bool[] P00LV2_n1423AmbienteTecnologico_FatorAjuste ;
      private bool[] P00LV2_A353AmbienteTecnologico_Ativo ;
      private short[] P00LV2_A978AmbienteTecnologico_LocPF ;
      private bool[] P00LV2_n978AmbienteTecnologico_LocPF ;
      private short[] P00LV2_A977AmbienteTecnologico_TaxaEntregaMlhr ;
      private bool[] P00LV2_n977AmbienteTecnologico_TaxaEntregaMlhr ;
      private short[] P00LV2_A976AmbienteTecnologico_TaxaEntregaDsnv ;
      private bool[] P00LV2_n976AmbienteTecnologico_TaxaEntregaDsnv ;
      private int[] P00LV2_A351AmbienteTecnologico_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV36GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV37GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV38GridStateDynamicFilter ;
   }

   public class getwwambientetecnologicofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00LV2( IGxContext context ,
                                             String AV53WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 ,
                                             String AV54WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 ,
                                             bool AV55WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 ,
                                             String AV56WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 ,
                                             String AV57WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 ,
                                             bool AV58WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 ,
                                             String AV59WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 ,
                                             String AV60WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 ,
                                             String AV62WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel ,
                                             String AV61WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao ,
                                             short AV63WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv ,
                                             short AV64WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to ,
                                             short AV65WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr ,
                                             short AV66WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to ,
                                             short AV67WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf ,
                                             short AV68WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to ,
                                             short AV69WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel ,
                                             decimal AV70WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste ,
                                             decimal AV71WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             short A976AmbienteTecnologico_TaxaEntregaDsnv ,
                                             short A977AmbienteTecnologico_TaxaEntregaMlhr ,
                                             short A978AmbienteTecnologico_LocPF ,
                                             bool A353AmbienteTecnologico_Ativo ,
                                             decimal A1423AmbienteTecnologico_FatorAjuste ,
                                             int A728AmbienteTecnologico_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [14] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [AmbienteTecnologico_AreaTrabalhoCod], [AmbienteTecnologico_Descricao], [AmbienteTecnologico_FatorAjuste], [AmbienteTecnologico_Ativo], [AmbienteTecnologico_LocPF], [AmbienteTecnologico_TaxaEntregaMlhr], [AmbienteTecnologico_TaxaEntregaDsnv], [AmbienteTecnologico_Codigo] FROM [AmbienteTecnologico] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([AmbienteTecnologico_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV53WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] like '%' + @lV54WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV55WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] like '%' + @lV57WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV58WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV59WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] like '%' + @lV60WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao)) ) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] like @lV61WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel)) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] = @AV62WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV63WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_TaxaEntregaDsnv] >= @AV63WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV64WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_TaxaEntregaDsnv] <= @AV64WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV65WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_TaxaEntregaMlhr] >= @AV65WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV66WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_TaxaEntregaMlhr] <= @AV66WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (0==AV67WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_LocPF] >= @AV67WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (0==AV68WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_LocPF] <= @AV68WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV69WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Ativo] = 1)";
         }
         if ( AV69WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Ativo] = 0)";
         }
         if ( ! (Convert.ToDecimal(0)==AV70WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_FatorAjuste] >= @AV70WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV71WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_FatorAjuste] <= @AV71WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [AmbienteTecnologico_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00LV2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (decimal)dynConstraints[17] , (decimal)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] , (decimal)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00LV2 ;
          prmP00LV2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV54WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV57WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV60WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV61WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV62WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV63WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV64WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV65WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV66WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV67WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV68WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV70WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste",SqlDbType.Decimal,8,4} ,
          new Object[] {"@AV71WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to",SqlDbType.Decimal,8,4}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00LV2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LV2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwambientetecnologicofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwambientetecnologicofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwambientetecnologicofilterdata") )
          {
             return  ;
          }
          getwwambientetecnologicofilterdata worker = new getwwambientetecnologicofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
