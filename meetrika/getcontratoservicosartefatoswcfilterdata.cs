/*
               File: GetContratoServicosArtefatosWCFilterData
        Description: Get Contrato Servicos Artefatos WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/15/2020 23:16:56.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratoservicosartefatoswcfilterdata : GXProcedure
   {
      public getcontratoservicosartefatoswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratoservicosartefatoswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
         return AV24OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratoservicosartefatoswcfilterdata objgetcontratoservicosartefatoswcfilterdata;
         objgetcontratoservicosartefatoswcfilterdata = new getcontratoservicosartefatoswcfilterdata();
         objgetcontratoservicosartefatoswcfilterdata.AV15DDOName = aP0_DDOName;
         objgetcontratoservicosartefatoswcfilterdata.AV13SearchTxt = aP1_SearchTxt;
         objgetcontratoservicosartefatoswcfilterdata.AV14SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratoservicosartefatoswcfilterdata.AV19OptionsJson = "" ;
         objgetcontratoservicosartefatoswcfilterdata.AV22OptionsDescJson = "" ;
         objgetcontratoservicosartefatoswcfilterdata.AV24OptionIndexesJson = "" ;
         objgetcontratoservicosartefatoswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratoservicosartefatoswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratoservicosartefatoswcfilterdata);
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratoservicosartefatoswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18Options = (IGxCollection)(new GxSimpleCollection());
         AV21OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV23OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV15DDOName), "DDO_ARTEFATOS_CODIGO") == 0 )
         {
            /* Execute user subroutine: 'LOADARTEFATOS_CODIGOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV19OptionsJson = AV18Options.ToJSonString(false);
         AV22OptionsDescJson = AV21OptionsDesc.ToJSonString(false);
         AV24OptionIndexesJson = AV23OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV26Session.Get("ContratoServicosArtefatosWCGridState"), "") == 0 )
         {
            AV28GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoServicosArtefatosWCGridState"), "");
         }
         else
         {
            AV28GridState.FromXml(AV26Session.Get("ContratoServicosArtefatosWCGridState"), "");
         }
         AV34GXV1 = 1;
         while ( AV34GXV1 <= AV28GridState.gxTpr_Filtervalues.Count )
         {
            AV29GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV28GridState.gxTpr_Filtervalues.Item(AV34GXV1));
            if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFARTEFATOS_CODIGO") == 0 )
            {
               AV10TFArtefatos_Codigo = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFARTEFATOS_CODIGO_SEL") == 0 )
            {
               AV11TFArtefatos_Codigo_Sel = (int)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSARTEFATO_OBRIGATORIO_SEL") == 0 )
            {
               AV12TFContratoServicosArtefato_Obrigatorio_Sel = (short)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATOSERVICOS_CODIGO") == 0 )
            {
               AV31ContratoServicos_Codigo = (int)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            AV34GXV1 = (int)(AV34GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADARTEFATOS_CODIGOOPTIONS' Routine */
         AV10TFArtefatos_Codigo = AV13SearchTxt;
         AV11TFArtefatos_Codigo_Sel = 0;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFArtefatos_Codigo_Sel ,
                                              AV10TFArtefatos_Codigo ,
                                              AV12TFContratoServicosArtefato_Obrigatorio_Sel ,
                                              A1751Artefatos_Descricao ,
                                              A1749Artefatos_Codigo ,
                                              A1768ContratoServicosArtefato_Obrigatorio ,
                                              AV31ContratoServicos_Codigo ,
                                              A160ContratoServicos_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFArtefatos_Codigo = StringUtil.Concat( StringUtil.RTrim( AV10TFArtefatos_Codigo), "%", "");
         /* Using cursor P00K12 */
         pr_default.execute(0, new Object[] {AV31ContratoServicos_Codigo, lV10TFArtefatos_Codigo, AV11TFArtefatos_Codigo_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKK12 = false;
            A1749Artefatos_Codigo = P00K12_A1749Artefatos_Codigo[0];
            A160ContratoServicos_Codigo = P00K12_A160ContratoServicos_Codigo[0];
            A1768ContratoServicosArtefato_Obrigatorio = P00K12_A1768ContratoServicosArtefato_Obrigatorio[0];
            n1768ContratoServicosArtefato_Obrigatorio = P00K12_n1768ContratoServicosArtefato_Obrigatorio[0];
            A1751Artefatos_Descricao = P00K12_A1751Artefatos_Descricao[0];
            A1751Artefatos_Descricao = P00K12_A1751Artefatos_Descricao[0];
            AV25count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00K12_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( P00K12_A1749Artefatos_Codigo[0] == A1749Artefatos_Codigo ) )
            {
               BRKK12 = false;
               AV25count = (long)(AV25count+1);
               BRKK12 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1751Artefatos_Descricao)) )
            {
               AV17Option = StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0);
               AV20OptionDesc = A1751Artefatos_Descricao;
               AV16InsertIndex = 1;
               while ( ( AV16InsertIndex <= AV18Options.Count ) && ( StringUtil.StrCmp(((String)AV21OptionsDesc.Item(AV16InsertIndex)), AV20OptionDesc) < 0 ) )
               {
                  AV16InsertIndex = (int)(AV16InsertIndex+1);
               }
               AV18Options.Add(AV17Option, AV16InsertIndex);
               AV21OptionsDesc.Add(AV20OptionDesc, AV16InsertIndex);
               AV23OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV25count), "Z,ZZZ,ZZZ,ZZ9")), AV16InsertIndex);
            }
            if ( AV18Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK12 )
            {
               BRKK12 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18Options = new GxSimpleCollection();
         AV21OptionsDesc = new GxSimpleCollection();
         AV23OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV26Session = context.GetSession();
         AV28GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV29GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFArtefatos_Codigo = "";
         scmdbuf = "";
         lV10TFArtefatos_Codigo = "";
         A1751Artefatos_Descricao = "";
         P00K12_A1749Artefatos_Codigo = new int[1] ;
         P00K12_A160ContratoServicos_Codigo = new int[1] ;
         P00K12_A1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         P00K12_n1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         P00K12_A1751Artefatos_Descricao = new String[] {""} ;
         AV17Option = "";
         AV20OptionDesc = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratoservicosartefatoswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00K12_A1749Artefatos_Codigo, P00K12_A160ContratoServicos_Codigo, P00K12_A1768ContratoServicosArtefato_Obrigatorio, P00K12_n1768ContratoServicosArtefato_Obrigatorio, P00K12_A1751Artefatos_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12TFContratoServicosArtefato_Obrigatorio_Sel ;
      private int AV34GXV1 ;
      private int AV11TFArtefatos_Codigo_Sel ;
      private int AV31ContratoServicos_Codigo ;
      private int A1749Artefatos_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int AV16InsertIndex ;
      private long AV25count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool A1768ContratoServicosArtefato_Obrigatorio ;
      private bool BRKK12 ;
      private bool n1768ContratoServicosArtefato_Obrigatorio ;
      private String AV24OptionIndexesJson ;
      private String AV19OptionsJson ;
      private String AV22OptionsDescJson ;
      private String AV15DDOName ;
      private String AV13SearchTxt ;
      private String AV14SearchTxtTo ;
      private String AV10TFArtefatos_Codigo ;
      private String lV10TFArtefatos_Codigo ;
      private String A1751Artefatos_Descricao ;
      private String AV17Option ;
      private String AV20OptionDesc ;
      private IGxSession AV26Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00K12_A1749Artefatos_Codigo ;
      private int[] P00K12_A160ContratoServicos_Codigo ;
      private bool[] P00K12_A1768ContratoServicosArtefato_Obrigatorio ;
      private bool[] P00K12_n1768ContratoServicosArtefato_Obrigatorio ;
      private String[] P00K12_A1751Artefatos_Descricao ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV18Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV28GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV29GridStateFilterValue ;
   }

   public class getcontratoservicosartefatoswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00K12( IGxContext context ,
                                             int AV11TFArtefatos_Codigo_Sel ,
                                             String AV10TFArtefatos_Codigo ,
                                             short AV12TFContratoServicosArtefato_Obrigatorio_Sel ,
                                             String A1751Artefatos_Descricao ,
                                             int A1749Artefatos_Codigo ,
                                             bool A1768ContratoServicosArtefato_Obrigatorio ,
                                             int AV31ContratoServicos_Codigo ,
                                             int A160ContratoServicos_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [3] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Artefatos_Codigo], T1.[ContratoServicos_Codigo], T1.[ContratoServicosArtefato_Obrigatorio], T2.[Artefatos_Descricao] FROM ([ContratoServicosArtefatos] T1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = T1.[Artefatos_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicos_Codigo] = @AV31ContratoServicos_Codigo)";
         if ( (0==AV11TFArtefatos_Codigo_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFArtefatos_Codigo)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Artefatos_Descricao] like @lV10TFArtefatos_Codigo)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (0==AV11TFArtefatos_Codigo_Sel) )
         {
            sWhereString = sWhereString + " and (T1.[Artefatos_Codigo] = @AV11TFArtefatos_Codigo_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV12TFContratoServicosArtefato_Obrigatorio_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosArtefato_Obrigatorio] = 1)";
         }
         if ( AV12TFContratoServicosArtefato_Obrigatorio_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosArtefato_Obrigatorio] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicos_Codigo], T1.[Artefatos_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00K12(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (String)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00K12 ;
          prmP00K12 = new Object[] {
          new Object[] {"@AV31ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFArtefatos_Codigo",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV11TFArtefatos_Codigo_Sel",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00K12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratoservicosartefatoswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratoservicosartefatoswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratoservicosartefatoswcfilterdata") )
          {
             return  ;
          }
          getcontratoservicosartefatoswcfilterdata worker = new getcontratoservicosartefatoswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
