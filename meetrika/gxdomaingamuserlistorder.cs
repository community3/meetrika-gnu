/*
               File: GAMUserListOrder
        Description: GAMUserListOrder
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:38.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomaingamuserlistorder
   {
      private static Hashtable domain = new Hashtable();
      static gxdomaingamuserlistorder ()
      {
         domain[(short)0] = "None";
         domain[(short)1] = "GUID_ Asc";
         domain[(short)2] = "GUID_ Desc";
         domain[(short)3] = "User Name_ Asc";
         domain[(short)4] = "User Name_ Desc";
         domain[(short)5] = "User Email_ Asc";
         domain[(short)6] = "User Email_ Desc";
         domain[(short)7] = "User First Name_ Asc";
         domain[(short)8] = "User First Name_ Desc";
         domain[(short)9] = "User Last Name_ Asc";
         domain[(short)10] = "User Last Name_ Desc";
         domain[(short)11] = "User External Id_Asc";
         domain[(short)12] = "User External Id_Desc";
      }

      public static string getDescription( IGxContext context ,
                                           short key )
      {
         return (string)domain[key] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (short key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
