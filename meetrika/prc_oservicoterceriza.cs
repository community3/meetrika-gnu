/*
               File: PRC_OServicoTerceriza
        Description: O Servico Terceriza
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:30.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_oservicoterceriza : GXProcedure
   {
      public prc_oservicoterceriza( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_oservicoterceriza( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Servico_Codigo ,
                           out bool aP1_Servico_Terceriza )
      {
         this.A155Servico_Codigo = aP0_Servico_Codigo;
         this.AV8Servico_Terceriza = false ;
         initialize();
         executePrivate();
         aP0_Servico_Codigo=this.A155Servico_Codigo;
         aP1_Servico_Terceriza=this.AV8Servico_Terceriza;
      }

      public bool executeUdp( ref int aP0_Servico_Codigo )
      {
         this.A155Servico_Codigo = aP0_Servico_Codigo;
         this.AV8Servico_Terceriza = false ;
         initialize();
         executePrivate();
         aP0_Servico_Codigo=this.A155Servico_Codigo;
         aP1_Servico_Terceriza=this.AV8Servico_Terceriza;
         return AV8Servico_Terceriza ;
      }

      public void executeSubmit( ref int aP0_Servico_Codigo ,
                                 out bool aP1_Servico_Terceriza )
      {
         prc_oservicoterceriza objprc_oservicoterceriza;
         objprc_oservicoterceriza = new prc_oservicoterceriza();
         objprc_oservicoterceriza.A155Servico_Codigo = aP0_Servico_Codigo;
         objprc_oservicoterceriza.AV8Servico_Terceriza = false ;
         objprc_oservicoterceriza.context.SetSubmitInitialConfig(context);
         objprc_oservicoterceriza.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_oservicoterceriza);
         aP0_Servico_Codigo=this.A155Servico_Codigo;
         aP1_Servico_Terceriza=this.AV8Servico_Terceriza;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_oservicoterceriza)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P006D2 */
         pr_default.execute(0, new Object[] {A155Servico_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A889Servico_Terceriza = P006D2_A889Servico_Terceriza[0];
            n889Servico_Terceriza = P006D2_n889Servico_Terceriza[0];
            AV8Servico_Terceriza = A889Servico_Terceriza;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P006D2_A155Servico_Codigo = new int[1] ;
         P006D2_A889Servico_Terceriza = new bool[] {false} ;
         P006D2_n889Servico_Terceriza = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_oservicoterceriza__default(),
            new Object[][] {
                new Object[] {
               P006D2_A155Servico_Codigo, P006D2_A889Servico_Terceriza, P006D2_n889Servico_Terceriza
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A155Servico_Codigo ;
      private String scmdbuf ;
      private bool AV8Servico_Terceriza ;
      private bool A889Servico_Terceriza ;
      private bool n889Servico_Terceriza ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Servico_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P006D2_A155Servico_Codigo ;
      private bool[] P006D2_A889Servico_Terceriza ;
      private bool[] P006D2_n889Servico_Terceriza ;
      private bool aP1_Servico_Terceriza ;
   }

   public class prc_oservicoterceriza__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006D2 ;
          prmP006D2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006D2", "SELECT TOP 1 [Servico_Codigo], [Servico_Terceriza] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006D2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
