/*
               File: PRC_NumerarOSFMDoLote
        Description: Numerar OSLote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:28.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_numerarosfmdolote : GXProcedure
   {
      public prc_numerarosfmdolote( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_numerarosfmdolote( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_LoteAceiteCod )
      {
         this.A597ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         initialize();
         executePrivate();
         aP0_ContagemResultado_LoteAceiteCod=this.A597ContagemResultado_LoteAceiteCod;
      }

      public int executeUdp( )
      {
         this.A597ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         initialize();
         executePrivate();
         aP0_ContagemResultado_LoteAceiteCod=this.A597ContagemResultado_LoteAceiteCod;
         return A597ContagemResultado_LoteAceiteCod ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_LoteAceiteCod )
      {
         prc_numerarosfmdolote objprc_numerarosfmdolote;
         objprc_numerarosfmdolote = new prc_numerarosfmdolote();
         objprc_numerarosfmdolote.A597ContagemResultado_LoteAceiteCod = aP0_ContagemResultado_LoteAceiteCod;
         objprc_numerarosfmdolote.context.SetSubmitInitialConfig(context);
         objprc_numerarosfmdolote.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_numerarosfmdolote);
         aP0_ContagemResultado_LoteAceiteCod=this.A597ContagemResultado_LoteAceiteCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_numerarosfmdolote)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P005J2 */
         pr_default.execute(0, new Object[] {n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A489ContagemResultado_SistemaCod = P005J2_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P005J2_n489ContagemResultado_SistemaCod[0];
            A493ContagemResultado_DemandaFM = P005J2_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P005J2_n493ContagemResultado_DemandaFM[0];
            A509ContagemrResultado_SistemaSigla = P005J2_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P005J2_n509ContagemrResultado_SistemaSigla[0];
            A515ContagemResultado_SistemaCoord = P005J2_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P005J2_n515ContagemResultado_SistemaCoord[0];
            A456ContagemResultado_Codigo = P005J2_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = P005J2_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P005J2_n509ContagemrResultado_SistemaSigla[0];
            A515ContagemResultado_SistemaCoord = P005J2_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P005J2_n515ContagemResultado_SistemaCoord[0];
            AV9Contratada_OS = (int)(NumberUtil.Val( A493ContagemResultado_DemandaFM, "."));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         /* Using cursor P005J3 */
         pr_default.execute(1, new Object[] {n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRK5J3 = false;
            A489ContagemResultado_SistemaCod = P005J3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P005J3_n489ContagemResultado_SistemaCod[0];
            A509ContagemrResultado_SistemaSigla = P005J3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P005J3_n509ContagemrResultado_SistemaSigla[0];
            A515ContagemResultado_SistemaCoord = P005J3_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P005J3_n515ContagemResultado_SistemaCoord[0];
            A493ContagemResultado_DemandaFM = P005J3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P005J3_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P005J3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P005J3_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P005J3_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = P005J3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P005J3_n509ContagemrResultado_SistemaSigla[0];
            A515ContagemResultado_SistemaCoord = P005J3_A515ContagemResultado_SistemaCoord[0];
            n515ContagemResultado_SistemaCoord = P005J3_n515ContagemResultado_SistemaCoord[0];
            while ( (pr_default.getStatus(1) != 101) && ( P005J3_A597ContagemResultado_LoteAceiteCod[0] == A597ContagemResultado_LoteAceiteCod ) && ( StringUtil.StrCmp(P005J3_A515ContagemResultado_SistemaCoord[0], A515ContagemResultado_SistemaCoord) == 0 ) && ( StringUtil.StrCmp(P005J3_A509ContagemrResultado_SistemaSigla[0], A509ContagemrResultado_SistemaSigla) == 0 ) )
            {
               BRK5J3 = false;
               A489ContagemResultado_SistemaCod = P005J3_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P005J3_n489ContagemResultado_SistemaCod[0];
               A493ContagemResultado_DemandaFM = P005J3_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P005J3_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = P005J3_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P005J3_n457ContagemResultado_Demanda[0];
               A456ContagemResultado_Codigo = P005J3_A456ContagemResultado_Codigo[0];
               A493ContagemResultado_DemandaFM = StringUtil.Trim( StringUtil.Str( (decimal)(AV9Contratada_OS), 8, 0));
               n493ContagemResultado_DemandaFM = false;
               BatchSize = 200;
               pr_default.initializeBatch( 2, BatchSize, this, "Executebatchp005j4");
               /* Using cursor P005J4 */
               pr_default.addRecord(2, new Object[] {n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, A456ContagemResultado_Codigo});
               if ( pr_default.recordCount(2) == pr_default.getBatchSize(2) )
               {
                  Executebatchp005j4( ) ;
               }
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               BRK5J3 = true;
               pr_default.readNext(1);
            }
            if ( pr_default.getBatchSize(2) > 0 )
            {
               Executebatchp005j4( ) ;
            }
            AV9Contratada_OS = (int)(AV9Contratada_OS+1);
            if ( ! BRK5J3 )
            {
               BRK5J3 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
         this.cleanup();
      }

      protected void Executebatchp005j4( )
      {
         /* Using cursor P005J4 */
         pr_default.executeBatch(2);
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_NumerarOSFMDoLote");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(2);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P005J2_A489ContagemResultado_SistemaCod = new int[1] ;
         P005J2_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P005J2_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P005J2_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P005J2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P005J2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P005J2_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P005J2_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P005J2_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P005J2_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P005J2_A456ContagemResultado_Codigo = new int[1] ;
         A493ContagemResultado_DemandaFM = "";
         A509ContagemrResultado_SistemaSigla = "";
         A515ContagemResultado_SistemaCoord = "";
         P005J3_A489ContagemResultado_SistemaCod = new int[1] ;
         P005J3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P005J3_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P005J3_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P005J3_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P005J3_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P005J3_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P005J3_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P005J3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P005J3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P005J3_A457ContagemResultado_Demanda = new String[] {""} ;
         P005J3_n457ContagemResultado_Demanda = new bool[] {false} ;
         P005J3_A456ContagemResultado_Codigo = new int[1] ;
         A457ContagemResultado_Demanda = "";
         P005J4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P005J4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P005J4_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_numerarosfmdolote__default(),
            new Object[][] {
                new Object[] {
               P005J2_A489ContagemResultado_SistemaCod, P005J2_n489ContagemResultado_SistemaCod, P005J2_A597ContagemResultado_LoteAceiteCod, P005J2_n597ContagemResultado_LoteAceiteCod, P005J2_A493ContagemResultado_DemandaFM, P005J2_n493ContagemResultado_DemandaFM, P005J2_A509ContagemrResultado_SistemaSigla, P005J2_n509ContagemrResultado_SistemaSigla, P005J2_A515ContagemResultado_SistemaCoord, P005J2_n515ContagemResultado_SistemaCoord,
               P005J2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P005J3_A489ContagemResultado_SistemaCod, P005J3_n489ContagemResultado_SistemaCod, P005J3_A597ContagemResultado_LoteAceiteCod, P005J3_n597ContagemResultado_LoteAceiteCod, P005J3_A509ContagemrResultado_SistemaSigla, P005J3_n509ContagemrResultado_SistemaSigla, P005J3_A515ContagemResultado_SistemaCoord, P005J3_n515ContagemResultado_SistemaCoord, P005J3_A493ContagemResultado_DemandaFM, P005J3_n493ContagemResultado_DemandaFM,
               P005J3_A457ContagemResultado_Demanda, P005J3_n457ContagemResultado_Demanda, P005J3_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A597ContagemResultado_LoteAceiteCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV9Contratada_OS ;
      private int BatchSize ;
      private String scmdbuf ;
      private String A509ContagemrResultado_SistemaSigla ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool BRK5J3 ;
      private bool n457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A515ContagemResultado_SistemaCoord ;
      private String A457ContagemResultado_Demanda ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_LoteAceiteCod ;
      private IDataStoreProvider pr_default ;
      private int[] P005J2_A489ContagemResultado_SistemaCod ;
      private bool[] P005J2_n489ContagemResultado_SistemaCod ;
      private int[] P005J2_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P005J2_n597ContagemResultado_LoteAceiteCod ;
      private String[] P005J2_A493ContagemResultado_DemandaFM ;
      private bool[] P005J2_n493ContagemResultado_DemandaFM ;
      private String[] P005J2_A509ContagemrResultado_SistemaSigla ;
      private bool[] P005J2_n509ContagemrResultado_SistemaSigla ;
      private String[] P005J2_A515ContagemResultado_SistemaCoord ;
      private bool[] P005J2_n515ContagemResultado_SistemaCoord ;
      private int[] P005J2_A456ContagemResultado_Codigo ;
      private int[] P005J3_A489ContagemResultado_SistemaCod ;
      private bool[] P005J3_n489ContagemResultado_SistemaCod ;
      private int[] P005J3_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P005J3_n597ContagemResultado_LoteAceiteCod ;
      private String[] P005J3_A509ContagemrResultado_SistemaSigla ;
      private bool[] P005J3_n509ContagemrResultado_SistemaSigla ;
      private String[] P005J3_A515ContagemResultado_SistemaCoord ;
      private bool[] P005J3_n515ContagemResultado_SistemaCoord ;
      private String[] P005J3_A493ContagemResultado_DemandaFM ;
      private bool[] P005J3_n493ContagemResultado_DemandaFM ;
      private String[] P005J3_A457ContagemResultado_Demanda ;
      private bool[] P005J3_n457ContagemResultado_Demanda ;
      private int[] P005J3_A456ContagemResultado_Codigo ;
      private String[] P005J4_A493ContagemResultado_DemandaFM ;
      private bool[] P005J4_n493ContagemResultado_DemandaFM ;
      private int[] P005J4_A456ContagemResultado_Codigo ;
   }

   public class prc_numerarosfmdolote__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new BatchUpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005J2 ;
          prmP005J2 = new Object[] {
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005J3 ;
          prmP005J3 = new Object[] {
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005J4 ;
          prmP005J4 = new Object[] {
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005J2", "SELECT TOP 1 T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_DemandaFM], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @ContagemResultado_LoteAceiteCod ORDER BY T1.[ContagemResultado_LoteAceiteCod], T2.[Sistema_Coordenacao], T2.[Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005J2,1,0,false,true )
             ,new CursorDef("P005J3", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_LoteAceiteCod], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @ContagemResultado_LoteAceiteCod ORDER BY T1.[ContagemResultado_LoteAceiteCod], T2.[Sistema_Coordenacao], T2.[Sistema_Sigla], T1.[ContagemResultado_Demanda] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005J3,1,0,true,false )
             ,new CursorDef("P005J4", "UPDATE [ContagemResultado] SET [ContagemResultado_DemandaFM]=@ContagemResultado_DemandaFM  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005J4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 25) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 25) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
