/*
               File: WWLinhaNegocio
        Description: WWLinha Negocio
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:12:24.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwlinhanegocio : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwlinhanegocio( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwlinhanegocio( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_88 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_88_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_88_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17LinhaNegocio_Identificador1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17LinhaNegocio_Identificador1", AV17LinhaNegocio_Identificador1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21LinhaNegocio_Identificador2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21LinhaNegocio_Identificador2", AV21LinhaNegocio_Identificador2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25LinhaNegocio_Identificador3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25LinhaNegocio_Identificador3", AV25LinhaNegocio_Identificador3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV36TFLinhaNegocio_Identificador = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFLinhaNegocio_Identificador", AV36TFLinhaNegocio_Identificador);
               AV37TFLinhaNegocio_Identificador_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFLinhaNegocio_Identificador_Sel", AV37TFLinhaNegocio_Identificador_Sel);
               AV40TFLinhaNegocio_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFLinhaNegocio_Descricao", AV40TFLinhaNegocio_Descricao);
               AV41TFLinhaNegocio_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFLinhaNegocio_Descricao_Sel", AV41TFLinhaNegocio_Descricao_Sel);
               AV44TFLinhaNegocio_GpoObjCtrlCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFLinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFLinhaNegocio_GpoObjCtrlCod), 6, 0)));
               AV45TFLinhaNegocio_GpoObjCtrlCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLinhaNegocio_GpoObjCtrlCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFLinhaNegocio_GpoObjCtrlCod_To), 6, 0)));
               AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace", AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace);
               AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace", AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace);
               AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace", AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV76Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A2036LinhadeNegocio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LinhaNegocio_Identificador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21LinhaNegocio_Identificador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25LinhaNegocio_Identificador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV36TFLinhaNegocio_Identificador, AV37TFLinhaNegocio_Identificador_Sel, AV40TFLinhaNegocio_Descricao, AV41TFLinhaNegocio_Descricao_Sel, AV44TFLinhaNegocio_GpoObjCtrlCod, AV45TFLinhaNegocio_GpoObjCtrlCod_To, AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace, AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace, AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace, AV6WWPContext, AV76Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A2036LinhadeNegocio_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PARK2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTRK2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203119122487");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwlinhanegocio.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLINHANEGOCIO_IDENTIFICADOR1", AV17LinhaNegocio_Identificador1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLINHANEGOCIO_IDENTIFICADOR2", AV21LinhaNegocio_Identificador2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLINHANEGOCIO_IDENTIFICADOR3", AV25LinhaNegocio_Identificador3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLINHANEGOCIO_IDENTIFICADOR", AV36TFLinhaNegocio_Identificador);
         GxWebStd.gx_hidden_field( context, "GXH_vTFLINHANEGOCIO_IDENTIFICADOR_SEL", AV37TFLinhaNegocio_Identificador_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFLINHANEGOCIO_DESCRICAO", AV40TFLinhaNegocio_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFLINHANEGOCIO_DESCRICAO_SEL", AV41TFLinhaNegocio_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFLINHANEGOCIO_GPOOBJCTRLCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFLinhaNegocio_GpoObjCtrlCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFLinhaNegocio_GpoObjCtrlCod_To), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_88", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_88), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV47DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV47DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLINHANEGOCIO_IDENTIFICADORTITLEFILTERDATA", AV35LinhaNegocio_IdentificadorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLINHANEGOCIO_IDENTIFICADORTITLEFILTERDATA", AV35LinhaNegocio_IdentificadorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLINHANEGOCIO_DESCRICAOTITLEFILTERDATA", AV39LinhaNegocio_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLINHANEGOCIO_DESCRICAOTITLEFILTERDATA", AV39LinhaNegocio_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLINHANEGOCIO_GPOOBJCTRLCODTITLEFILTERDATA", AV43LinhaNegocio_GpoObjCtrlCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLINHANEGOCIO_GPOOBJCTRLCODTITLEFILTERDATA", AV43LinhaNegocio_GpoObjCtrlCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV76Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Caption", StringUtil.RTrim( Ddo_linhanegocio_identificador_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Tooltip", StringUtil.RTrim( Ddo_linhanegocio_identificador_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Cls", StringUtil.RTrim( Ddo_linhanegocio_identificador_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Filteredtext_set", StringUtil.RTrim( Ddo_linhanegocio_identificador_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Selectedvalue_set", StringUtil.RTrim( Ddo_linhanegocio_identificador_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_linhanegocio_identificador_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_linhanegocio_identificador_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Includesortasc", StringUtil.BoolToStr( Ddo_linhanegocio_identificador_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Includesortdsc", StringUtil.BoolToStr( Ddo_linhanegocio_identificador_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Sortedstatus", StringUtil.RTrim( Ddo_linhanegocio_identificador_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Includefilter", StringUtil.BoolToStr( Ddo_linhanegocio_identificador_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Filtertype", StringUtil.RTrim( Ddo_linhanegocio_identificador_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Filterisrange", StringUtil.BoolToStr( Ddo_linhanegocio_identificador_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Includedatalist", StringUtil.BoolToStr( Ddo_linhanegocio_identificador_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Datalisttype", StringUtil.RTrim( Ddo_linhanegocio_identificador_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Datalistproc", StringUtil.RTrim( Ddo_linhanegocio_identificador_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_linhanegocio_identificador_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Sortasc", StringUtil.RTrim( Ddo_linhanegocio_identificador_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Sortdsc", StringUtil.RTrim( Ddo_linhanegocio_identificador_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Loadingdata", StringUtil.RTrim( Ddo_linhanegocio_identificador_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Cleanfilter", StringUtil.RTrim( Ddo_linhanegocio_identificador_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Noresultsfound", StringUtil.RTrim( Ddo_linhanegocio_identificador_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Searchbuttontext", StringUtil.RTrim( Ddo_linhanegocio_identificador_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_linhanegocio_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_linhanegocio_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_linhanegocio_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_linhanegocio_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_linhanegocio_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_linhanegocio_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_linhanegocio_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_linhanegocio_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_linhanegocio_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_linhanegocio_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_linhanegocio_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_linhanegocio_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_linhanegocio_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_linhanegocio_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_linhanegocio_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_linhanegocio_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_linhanegocio_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_linhanegocio_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_linhanegocio_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_linhanegocio_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_linhanegocio_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_linhanegocio_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_linhanegocio_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Caption", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Tooltip", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Cls", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Filteredtext_set", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Includesortasc", StringUtil.BoolToStr( Ddo_linhanegocio_gpoobjctrlcod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_linhanegocio_gpoobjctrlcod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Sortedstatus", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Includefilter", StringUtil.BoolToStr( Ddo_linhanegocio_gpoobjctrlcod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Filtertype", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Filterisrange", StringUtil.BoolToStr( Ddo_linhanegocio_gpoobjctrlcod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Includedatalist", StringUtil.BoolToStr( Ddo_linhanegocio_gpoobjctrlcod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Sortasc", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Sortdsc", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Cleanfilter", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Rangefilterto", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Searchbuttontext", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Activeeventkey", StringUtil.RTrim( Ddo_linhanegocio_identificador_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Filteredtext_get", StringUtil.RTrim( Ddo_linhanegocio_identificador_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_IDENTIFICADOR_Selectedvalue_get", StringUtil.RTrim( Ddo_linhanegocio_identificador_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_linhanegocio_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_linhanegocio_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_linhanegocio_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Activeeventkey", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Filteredtext_get", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WERK2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTRK2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwlinhanegocio.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWLinhaNegocio" ;
      }

      public override String GetPgmdesc( )
      {
         return "WWLinha Negocio" ;
      }

      protected void WBRK0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_RK2( true) ;
         }
         else
         {
            wb_table1_2_RK2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_RK2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(100, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(101, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflinhanegocio_identificador_Internalname, AV36TFLinhaNegocio_Identificador, StringUtil.RTrim( context.localUtil.Format( AV36TFLinhaNegocio_Identificador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflinhanegocio_identificador_Jsonclick, 0, "Attribute", "", "", "", edtavTflinhanegocio_identificador_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLinhaNegocio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflinhanegocio_identificador_sel_Internalname, AV37TFLinhaNegocio_Identificador_Sel, StringUtil.RTrim( context.localUtil.Format( AV37TFLinhaNegocio_Identificador_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflinhanegocio_identificador_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTflinhanegocio_identificador_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLinhaNegocio.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTflinhanegocio_descricao_Internalname, AV40TFLinhaNegocio_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", 0, edtavTflinhanegocio_descricao_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_WWLinhaNegocio.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTflinhanegocio_descricao_sel_Internalname, AV41TFLinhaNegocio_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", 0, edtavTflinhanegocio_descricao_sel_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_WWLinhaNegocio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflinhanegocio_gpoobjctrlcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFLinhaNegocio_GpoObjCtrlCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44TFLinhaNegocio_GpoObjCtrlCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflinhanegocio_gpoobjctrlcod_Jsonclick, 0, "Attribute", "", "", "", edtavTflinhanegocio_gpoobjctrlcod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWLinhaNegocio.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflinhanegocio_gpoobjctrlcod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFLinhaNegocio_GpoObjCtrlCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45TFLinhaNegocio_GpoObjCtrlCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflinhanegocio_gpoobjctrlcod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflinhanegocio_gpoobjctrlcod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWLinhaNegocio.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LINHANEGOCIO_IDENTIFICADORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_linhanegocio_identificadortitlecontrolidtoreplace_Internalname, AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"", 0, edtavDdo_linhanegocio_identificadortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWLinhaNegocio.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LINHANEGOCIO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_linhanegocio_descricaotitlecontrolidtoreplace_Internalname, AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"", 0, edtavDdo_linhanegocio_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWLinhaNegocio.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LINHANEGOCIO_GPOOBJCTRLCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_linhanegocio_gpoobjctrlcodtitlecontrolidtoreplace_Internalname, AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", 0, edtavDdo_linhanegocio_gpoobjctrlcodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWLinhaNegocio.htm");
         }
         wbLoad = true;
      }

      protected void STARTRK2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "WWLinha Negocio", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPRK0( ) ;
      }

      protected void WSRK2( )
      {
         STARTRK2( ) ;
         EVTRK2( ) ;
      }

      protected void EVTRK2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11RK2 */
                              E11RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LINHANEGOCIO_IDENTIFICADOR.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12RK2 */
                              E12RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LINHANEGOCIO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13RK2 */
                              E13RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LINHANEGOCIO_GPOOBJCTRLCOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14RK2 */
                              E14RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15RK2 */
                              E15RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16RK2 */
                              E16RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17RK2 */
                              E17RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18RK2 */
                              E18RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19RK2 */
                              E19RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20RK2 */
                              E20RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21RK2 */
                              E21RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22RK2 */
                              E22RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23RK2 */
                              E23RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24RK2 */
                              E24RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25RK2 */
                              E25RK2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_88_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
                              SubsflControlProps_882( ) ;
                              AV51Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV51Update)) ? AV73Update_GXI : context.convertURL( context.PathToRelativeUrl( AV51Update))));
                              AV52Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV52Delete)) ? AV74Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV52Delete))));
                              AV53Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV53Display)) ? AV75Display_GXI : context.convertURL( context.PathToRelativeUrl( AV53Display))));
                              A2036LinhadeNegocio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtLinhadeNegocio_Codigo_Internalname), ",", "."));
                              A2037LinhaNegocio_Identificador = cgiGet( edtLinhaNegocio_Identificador_Internalname);
                              A2038LinhaNegocio_Descricao = cgiGet( edtLinhaNegocio_Descricao_Internalname);
                              n2038LinhaNegocio_Descricao = false;
                              A2035LinhaNegocio_GpoObjCtrlCod = (int)(context.localUtil.CToN( cgiGet( edtLinhaNegocio_GpoObjCtrlCod_Internalname), ",", "."));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26RK2 */
                                    E26RK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27RK2 */
                                    E27RK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28RK2 */
                                    E28RK2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Linhanegocio_identificador1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLINHANEGOCIO_IDENTIFICADOR1"), AV17LinhaNegocio_Identificador1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Linhanegocio_identificador2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLINHANEGOCIO_IDENTIFICADOR2"), AV21LinhaNegocio_Identificador2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Linhanegocio_identificador3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLINHANEGOCIO_IDENTIFICADOR3"), AV25LinhaNegocio_Identificador3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tflinhanegocio_identificador Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLINHANEGOCIO_IDENTIFICADOR"), AV36TFLinhaNegocio_Identificador) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tflinhanegocio_identificador_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLINHANEGOCIO_IDENTIFICADOR_SEL"), AV37TFLinhaNegocio_Identificador_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tflinhanegocio_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLINHANEGOCIO_DESCRICAO"), AV40TFLinhaNegocio_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tflinhanegocio_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLINHANEGOCIO_DESCRICAO_SEL"), AV41TFLinhaNegocio_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tflinhanegocio_gpoobjctrlcod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLINHANEGOCIO_GPOOBJCTRLCOD"), ",", ".") != Convert.ToDecimal( AV44TFLinhaNegocio_GpoObjCtrlCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tflinhanegocio_gpoobjctrlcod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO"), ",", ".") != Convert.ToDecimal( AV45TFLinhaNegocio_GpoObjCtrlCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WERK2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PARK2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("LINHANEGOCIO_IDENTIFICADOR", "Identificador", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("LINHANEGOCIO_IDENTIFICADOR", "Identificador", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("LINHANEGOCIO_IDENTIFICADOR", "Identificador", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_882( ) ;
         while ( nGXsfl_88_idx <= nRC_GXsfl_88 )
         {
            sendrow_882( ) ;
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17LinhaNegocio_Identificador1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21LinhaNegocio_Identificador2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25LinhaNegocio_Identificador3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV36TFLinhaNegocio_Identificador ,
                                       String AV37TFLinhaNegocio_Identificador_Sel ,
                                       String AV40TFLinhaNegocio_Descricao ,
                                       String AV41TFLinhaNegocio_Descricao_Sel ,
                                       int AV44TFLinhaNegocio_GpoObjCtrlCod ,
                                       int AV45TFLinhaNegocio_GpoObjCtrlCod_To ,
                                       String AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace ,
                                       String AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace ,
                                       String AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV76Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A2036LinhadeNegocio_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFRK2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_LINHADENEGOCIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2036LinhadeNegocio_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "LINHADENEGOCIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LINHANEGOCIO_IDENTIFICADOR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A2037LinhaNegocio_Identificador, ""))));
         GxWebStd.gx_hidden_field( context, "LINHANEGOCIO_IDENTIFICADOR", A2037LinhaNegocio_Identificador);
         GxWebStd.gx_hidden_field( context, "gxhash_LINHANEGOCIO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A2038LinhaNegocio_Descricao, ""))));
         GxWebStd.gx_hidden_field( context, "LINHANEGOCIO_DESCRICAO", A2038LinhaNegocio_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_LINHANEGOCIO_GPOOBJCTRLCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "LINHANEGOCIO_GPOOBJCTRLCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFRK2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV76Pgmname = "WWLinhaNegocio";
         context.Gx_err = 0;
      }

      protected void RFRK2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 88;
         /* Execute user event: E27RK2 */
         E27RK2 ();
         nGXsfl_88_idx = 1;
         sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
         SubsflControlProps_882( ) ;
         nGXsfl_88_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_882( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1 ,
                                                 AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 ,
                                                 AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 ,
                                                 AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 ,
                                                 AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2 ,
                                                 AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 ,
                                                 AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 ,
                                                 AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 ,
                                                 AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3 ,
                                                 AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 ,
                                                 AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 ,
                                                 AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel ,
                                                 AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador ,
                                                 AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel ,
                                                 AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao ,
                                                 AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod ,
                                                 AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to ,
                                                 A2037LinhaNegocio_Identificador ,
                                                 A2038LinhaNegocio_Descricao ,
                                                 A2035LinhaNegocio_GpoObjCtrlCod ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 = StringUtil.Concat( StringUtil.RTrim( AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1), "%", "");
            lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 = StringUtil.Concat( StringUtil.RTrim( AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1), "%", "");
            lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 = StringUtil.Concat( StringUtil.RTrim( AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2), "%", "");
            lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 = StringUtil.Concat( StringUtil.RTrim( AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2), "%", "");
            lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 = StringUtil.Concat( StringUtil.RTrim( AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3), "%", "");
            lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 = StringUtil.Concat( StringUtil.RTrim( AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3), "%", "");
            lV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador = StringUtil.Concat( StringUtil.RTrim( AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador), "%", "");
            lV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao = StringUtil.Concat( StringUtil.RTrim( AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao), "%", "");
            /* Using cursor H00RK2 */
            pr_default.execute(0, new Object[] {lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1, lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1, lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2, lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2, lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3, lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3, lV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador, AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel, lV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao, AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel, AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod, AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_88_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2035LinhaNegocio_GpoObjCtrlCod = H00RK2_A2035LinhaNegocio_GpoObjCtrlCod[0];
               A2038LinhaNegocio_Descricao = H00RK2_A2038LinhaNegocio_Descricao[0];
               n2038LinhaNegocio_Descricao = H00RK2_n2038LinhaNegocio_Descricao[0];
               A2037LinhaNegocio_Identificador = H00RK2_A2037LinhaNegocio_Identificador[0];
               A2036LinhadeNegocio_Codigo = H00RK2_A2036LinhadeNegocio_Codigo[0];
               /* Execute user event: E28RK2 */
               E28RK2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 88;
            WBRK0( ) ;
         }
         nGXsfl_88_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 = AV17LinhaNegocio_Identificador1;
         AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 = AV21LinhaNegocio_Identificador2;
         AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 = AV25LinhaNegocio_Identificador3;
         AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador = AV36TFLinhaNegocio_Identificador;
         AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel = AV37TFLinhaNegocio_Identificador_Sel;
         AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao = AV40TFLinhaNegocio_Descricao;
         AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel = AV41TFLinhaNegocio_Descricao_Sel;
         AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod = AV44TFLinhaNegocio_GpoObjCtrlCod;
         AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to = AV45TFLinhaNegocio_GpoObjCtrlCod_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1 ,
                                              AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 ,
                                              AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 ,
                                              AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 ,
                                              AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2 ,
                                              AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 ,
                                              AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 ,
                                              AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 ,
                                              AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3 ,
                                              AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 ,
                                              AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 ,
                                              AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel ,
                                              AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador ,
                                              AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel ,
                                              AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao ,
                                              AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod ,
                                              AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to ,
                                              A2037LinhaNegocio_Identificador ,
                                              A2038LinhaNegocio_Descricao ,
                                              A2035LinhaNegocio_GpoObjCtrlCod ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 = StringUtil.Concat( StringUtil.RTrim( AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1), "%", "");
         lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 = StringUtil.Concat( StringUtil.RTrim( AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1), "%", "");
         lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 = StringUtil.Concat( StringUtil.RTrim( AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2), "%", "");
         lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 = StringUtil.Concat( StringUtil.RTrim( AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2), "%", "");
         lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 = StringUtil.Concat( StringUtil.RTrim( AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3), "%", "");
         lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 = StringUtil.Concat( StringUtil.RTrim( AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3), "%", "");
         lV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador = StringUtil.Concat( StringUtil.RTrim( AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador), "%", "");
         lV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao = StringUtil.Concat( StringUtil.RTrim( AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao), "%", "");
         /* Using cursor H00RK3 */
         pr_default.execute(1, new Object[] {lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1, lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1, lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2, lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2, lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3, lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3, lV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador, AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel, lV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao, AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel, AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod, AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to});
         GRID_nRecordCount = H00RK3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 = AV17LinhaNegocio_Identificador1;
         AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 = AV21LinhaNegocio_Identificador2;
         AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 = AV25LinhaNegocio_Identificador3;
         AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador = AV36TFLinhaNegocio_Identificador;
         AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel = AV37TFLinhaNegocio_Identificador_Sel;
         AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao = AV40TFLinhaNegocio_Descricao;
         AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel = AV41TFLinhaNegocio_Descricao_Sel;
         AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod = AV44TFLinhaNegocio_GpoObjCtrlCod;
         AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to = AV45TFLinhaNegocio_GpoObjCtrlCod_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LinhaNegocio_Identificador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21LinhaNegocio_Identificador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25LinhaNegocio_Identificador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV36TFLinhaNegocio_Identificador, AV37TFLinhaNegocio_Identificador_Sel, AV40TFLinhaNegocio_Descricao, AV41TFLinhaNegocio_Descricao_Sel, AV44TFLinhaNegocio_GpoObjCtrlCod, AV45TFLinhaNegocio_GpoObjCtrlCod_To, AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace, AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace, AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace, AV6WWPContext, AV76Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A2036LinhadeNegocio_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 = AV17LinhaNegocio_Identificador1;
         AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 = AV21LinhaNegocio_Identificador2;
         AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 = AV25LinhaNegocio_Identificador3;
         AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador = AV36TFLinhaNegocio_Identificador;
         AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel = AV37TFLinhaNegocio_Identificador_Sel;
         AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao = AV40TFLinhaNegocio_Descricao;
         AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel = AV41TFLinhaNegocio_Descricao_Sel;
         AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod = AV44TFLinhaNegocio_GpoObjCtrlCod;
         AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to = AV45TFLinhaNegocio_GpoObjCtrlCod_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LinhaNegocio_Identificador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21LinhaNegocio_Identificador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25LinhaNegocio_Identificador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV36TFLinhaNegocio_Identificador, AV37TFLinhaNegocio_Identificador_Sel, AV40TFLinhaNegocio_Descricao, AV41TFLinhaNegocio_Descricao_Sel, AV44TFLinhaNegocio_GpoObjCtrlCod, AV45TFLinhaNegocio_GpoObjCtrlCod_To, AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace, AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace, AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace, AV6WWPContext, AV76Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A2036LinhadeNegocio_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 = AV17LinhaNegocio_Identificador1;
         AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 = AV21LinhaNegocio_Identificador2;
         AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 = AV25LinhaNegocio_Identificador3;
         AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador = AV36TFLinhaNegocio_Identificador;
         AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel = AV37TFLinhaNegocio_Identificador_Sel;
         AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao = AV40TFLinhaNegocio_Descricao;
         AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel = AV41TFLinhaNegocio_Descricao_Sel;
         AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod = AV44TFLinhaNegocio_GpoObjCtrlCod;
         AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to = AV45TFLinhaNegocio_GpoObjCtrlCod_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LinhaNegocio_Identificador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21LinhaNegocio_Identificador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25LinhaNegocio_Identificador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV36TFLinhaNegocio_Identificador, AV37TFLinhaNegocio_Identificador_Sel, AV40TFLinhaNegocio_Descricao, AV41TFLinhaNegocio_Descricao_Sel, AV44TFLinhaNegocio_GpoObjCtrlCod, AV45TFLinhaNegocio_GpoObjCtrlCod_To, AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace, AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace, AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace, AV6WWPContext, AV76Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A2036LinhadeNegocio_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 = AV17LinhaNegocio_Identificador1;
         AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 = AV21LinhaNegocio_Identificador2;
         AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 = AV25LinhaNegocio_Identificador3;
         AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador = AV36TFLinhaNegocio_Identificador;
         AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel = AV37TFLinhaNegocio_Identificador_Sel;
         AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao = AV40TFLinhaNegocio_Descricao;
         AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel = AV41TFLinhaNegocio_Descricao_Sel;
         AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod = AV44TFLinhaNegocio_GpoObjCtrlCod;
         AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to = AV45TFLinhaNegocio_GpoObjCtrlCod_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LinhaNegocio_Identificador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21LinhaNegocio_Identificador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25LinhaNegocio_Identificador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV36TFLinhaNegocio_Identificador, AV37TFLinhaNegocio_Identificador_Sel, AV40TFLinhaNegocio_Descricao, AV41TFLinhaNegocio_Descricao_Sel, AV44TFLinhaNegocio_GpoObjCtrlCod, AV45TFLinhaNegocio_GpoObjCtrlCod_To, AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace, AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace, AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace, AV6WWPContext, AV76Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A2036LinhadeNegocio_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 = AV17LinhaNegocio_Identificador1;
         AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 = AV21LinhaNegocio_Identificador2;
         AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 = AV25LinhaNegocio_Identificador3;
         AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador = AV36TFLinhaNegocio_Identificador;
         AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel = AV37TFLinhaNegocio_Identificador_Sel;
         AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao = AV40TFLinhaNegocio_Descricao;
         AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel = AV41TFLinhaNegocio_Descricao_Sel;
         AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod = AV44TFLinhaNegocio_GpoObjCtrlCod;
         AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to = AV45TFLinhaNegocio_GpoObjCtrlCod_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LinhaNegocio_Identificador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21LinhaNegocio_Identificador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25LinhaNegocio_Identificador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV36TFLinhaNegocio_Identificador, AV37TFLinhaNegocio_Identificador_Sel, AV40TFLinhaNegocio_Descricao, AV41TFLinhaNegocio_Descricao_Sel, AV44TFLinhaNegocio_GpoObjCtrlCod, AV45TFLinhaNegocio_GpoObjCtrlCod_To, AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace, AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace, AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace, AV6WWPContext, AV76Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A2036LinhadeNegocio_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPRK0( )
      {
         /* Before Start, stand alone formulas. */
         AV76Pgmname = "WWLinhaNegocio";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E26RK2 */
         E26RK2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV47DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vLINHANEGOCIO_IDENTIFICADORTITLEFILTERDATA"), AV35LinhaNegocio_IdentificadorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLINHANEGOCIO_DESCRICAOTITLEFILTERDATA"), AV39LinhaNegocio_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLINHANEGOCIO_GPOOBJCTRLCODTITLEFILTERDATA"), AV43LinhaNegocio_GpoObjCtrlCodTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17LinhaNegocio_Identificador1 = cgiGet( edtavLinhanegocio_identificador1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17LinhaNegocio_Identificador1", AV17LinhaNegocio_Identificador1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21LinhaNegocio_Identificador2 = cgiGet( edtavLinhanegocio_identificador2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21LinhaNegocio_Identificador2", AV21LinhaNegocio_Identificador2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25LinhaNegocio_Identificador3 = cgiGet( edtavLinhanegocio_identificador3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25LinhaNegocio_Identificador3", AV25LinhaNegocio_Identificador3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV36TFLinhaNegocio_Identificador = cgiGet( edtavTflinhanegocio_identificador_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFLinhaNegocio_Identificador", AV36TFLinhaNegocio_Identificador);
            AV37TFLinhaNegocio_Identificador_Sel = cgiGet( edtavTflinhanegocio_identificador_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFLinhaNegocio_Identificador_Sel", AV37TFLinhaNegocio_Identificador_Sel);
            AV40TFLinhaNegocio_Descricao = cgiGet( edtavTflinhanegocio_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFLinhaNegocio_Descricao", AV40TFLinhaNegocio_Descricao);
            AV41TFLinhaNegocio_Descricao_Sel = cgiGet( edtavTflinhanegocio_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFLinhaNegocio_Descricao_Sel", AV41TFLinhaNegocio_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflinhanegocio_gpoobjctrlcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflinhanegocio_gpoobjctrlcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLINHANEGOCIO_GPOOBJCTRLCOD");
               GX_FocusControl = edtavTflinhanegocio_gpoobjctrlcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFLinhaNegocio_GpoObjCtrlCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFLinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFLinhaNegocio_GpoObjCtrlCod), 6, 0)));
            }
            else
            {
               AV44TFLinhaNegocio_GpoObjCtrlCod = (int)(context.localUtil.CToN( cgiGet( edtavTflinhanegocio_gpoobjctrlcod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFLinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFLinhaNegocio_GpoObjCtrlCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflinhanegocio_gpoobjctrlcod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflinhanegocio_gpoobjctrlcod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO");
               GX_FocusControl = edtavTflinhanegocio_gpoobjctrlcod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFLinhaNegocio_GpoObjCtrlCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLinhaNegocio_GpoObjCtrlCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFLinhaNegocio_GpoObjCtrlCod_To), 6, 0)));
            }
            else
            {
               AV45TFLinhaNegocio_GpoObjCtrlCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTflinhanegocio_gpoobjctrlcod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLinhaNegocio_GpoObjCtrlCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFLinhaNegocio_GpoObjCtrlCod_To), 6, 0)));
            }
            AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace = cgiGet( edtavDdo_linhanegocio_identificadortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace", AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace);
            AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_linhanegocio_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace", AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace);
            AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace = cgiGet( edtavDdo_linhanegocio_gpoobjctrlcodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace", AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_88 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_88"), ",", "."));
            AV49GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV50GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_linhanegocio_identificador_Caption = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Caption");
            Ddo_linhanegocio_identificador_Tooltip = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Tooltip");
            Ddo_linhanegocio_identificador_Cls = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Cls");
            Ddo_linhanegocio_identificador_Filteredtext_set = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Filteredtext_set");
            Ddo_linhanegocio_identificador_Selectedvalue_set = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Selectedvalue_set");
            Ddo_linhanegocio_identificador_Dropdownoptionstype = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Dropdownoptionstype");
            Ddo_linhanegocio_identificador_Titlecontrolidtoreplace = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Titlecontrolidtoreplace");
            Ddo_linhanegocio_identificador_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Includesortasc"));
            Ddo_linhanegocio_identificador_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Includesortdsc"));
            Ddo_linhanegocio_identificador_Sortedstatus = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Sortedstatus");
            Ddo_linhanegocio_identificador_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Includefilter"));
            Ddo_linhanegocio_identificador_Filtertype = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Filtertype");
            Ddo_linhanegocio_identificador_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Filterisrange"));
            Ddo_linhanegocio_identificador_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Includedatalist"));
            Ddo_linhanegocio_identificador_Datalisttype = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Datalisttype");
            Ddo_linhanegocio_identificador_Datalistproc = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Datalistproc");
            Ddo_linhanegocio_identificador_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_linhanegocio_identificador_Sortasc = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Sortasc");
            Ddo_linhanegocio_identificador_Sortdsc = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Sortdsc");
            Ddo_linhanegocio_identificador_Loadingdata = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Loadingdata");
            Ddo_linhanegocio_identificador_Cleanfilter = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Cleanfilter");
            Ddo_linhanegocio_identificador_Noresultsfound = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Noresultsfound");
            Ddo_linhanegocio_identificador_Searchbuttontext = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Searchbuttontext");
            Ddo_linhanegocio_descricao_Caption = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Caption");
            Ddo_linhanegocio_descricao_Tooltip = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Tooltip");
            Ddo_linhanegocio_descricao_Cls = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Cls");
            Ddo_linhanegocio_descricao_Filteredtext_set = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Filteredtext_set");
            Ddo_linhanegocio_descricao_Selectedvalue_set = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Selectedvalue_set");
            Ddo_linhanegocio_descricao_Dropdownoptionstype = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Dropdownoptionstype");
            Ddo_linhanegocio_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_linhanegocio_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Includesortasc"));
            Ddo_linhanegocio_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Includesortdsc"));
            Ddo_linhanegocio_descricao_Sortedstatus = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Sortedstatus");
            Ddo_linhanegocio_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Includefilter"));
            Ddo_linhanegocio_descricao_Filtertype = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Filtertype");
            Ddo_linhanegocio_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Filterisrange"));
            Ddo_linhanegocio_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Includedatalist"));
            Ddo_linhanegocio_descricao_Datalisttype = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Datalisttype");
            Ddo_linhanegocio_descricao_Datalistproc = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Datalistproc");
            Ddo_linhanegocio_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_linhanegocio_descricao_Sortasc = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Sortasc");
            Ddo_linhanegocio_descricao_Sortdsc = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Sortdsc");
            Ddo_linhanegocio_descricao_Loadingdata = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Loadingdata");
            Ddo_linhanegocio_descricao_Cleanfilter = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Cleanfilter");
            Ddo_linhanegocio_descricao_Noresultsfound = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Noresultsfound");
            Ddo_linhanegocio_descricao_Searchbuttontext = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Searchbuttontext");
            Ddo_linhanegocio_gpoobjctrlcod_Caption = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Caption");
            Ddo_linhanegocio_gpoobjctrlcod_Tooltip = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Tooltip");
            Ddo_linhanegocio_gpoobjctrlcod_Cls = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Cls");
            Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_set = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Filteredtext_set");
            Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_set = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Filteredtextto_set");
            Ddo_linhanegocio_gpoobjctrlcod_Dropdownoptionstype = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Dropdownoptionstype");
            Ddo_linhanegocio_gpoobjctrlcod_Titlecontrolidtoreplace = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Titlecontrolidtoreplace");
            Ddo_linhanegocio_gpoobjctrlcod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Includesortasc"));
            Ddo_linhanegocio_gpoobjctrlcod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Includesortdsc"));
            Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Sortedstatus");
            Ddo_linhanegocio_gpoobjctrlcod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Includefilter"));
            Ddo_linhanegocio_gpoobjctrlcod_Filtertype = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Filtertype");
            Ddo_linhanegocio_gpoobjctrlcod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Filterisrange"));
            Ddo_linhanegocio_gpoobjctrlcod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Includedatalist"));
            Ddo_linhanegocio_gpoobjctrlcod_Sortasc = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Sortasc");
            Ddo_linhanegocio_gpoobjctrlcod_Sortdsc = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Sortdsc");
            Ddo_linhanegocio_gpoobjctrlcod_Cleanfilter = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Cleanfilter");
            Ddo_linhanegocio_gpoobjctrlcod_Rangefilterfrom = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Rangefilterfrom");
            Ddo_linhanegocio_gpoobjctrlcod_Rangefilterto = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Rangefilterto");
            Ddo_linhanegocio_gpoobjctrlcod_Searchbuttontext = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_linhanegocio_identificador_Activeeventkey = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Activeeventkey");
            Ddo_linhanegocio_identificador_Filteredtext_get = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Filteredtext_get");
            Ddo_linhanegocio_identificador_Selectedvalue_get = cgiGet( "DDO_LINHANEGOCIO_IDENTIFICADOR_Selectedvalue_get");
            Ddo_linhanegocio_descricao_Activeeventkey = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Activeeventkey");
            Ddo_linhanegocio_descricao_Filteredtext_get = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Filteredtext_get");
            Ddo_linhanegocio_descricao_Selectedvalue_get = cgiGet( "DDO_LINHANEGOCIO_DESCRICAO_Selectedvalue_get");
            Ddo_linhanegocio_gpoobjctrlcod_Activeeventkey = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Activeeventkey");
            Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_get = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Filteredtext_get");
            Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_get = cgiGet( "DDO_LINHANEGOCIO_GPOOBJCTRLCOD_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLINHANEGOCIO_IDENTIFICADOR1"), AV17LinhaNegocio_Identificador1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLINHANEGOCIO_IDENTIFICADOR2"), AV21LinhaNegocio_Identificador2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLINHANEGOCIO_IDENTIFICADOR3"), AV25LinhaNegocio_Identificador3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLINHANEGOCIO_IDENTIFICADOR"), AV36TFLinhaNegocio_Identificador) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLINHANEGOCIO_IDENTIFICADOR_SEL"), AV37TFLinhaNegocio_Identificador_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLINHANEGOCIO_DESCRICAO"), AV40TFLinhaNegocio_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLINHANEGOCIO_DESCRICAO_SEL"), AV41TFLinhaNegocio_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLINHANEGOCIO_GPOOBJCTRLCOD"), ",", ".") != Convert.ToDecimal( AV44TFLinhaNegocio_GpoObjCtrlCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO"), ",", ".") != Convert.ToDecimal( AV45TFLinhaNegocio_GpoObjCtrlCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E26RK2 */
         E26RK2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26RK2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "LINHANEGOCIO_IDENTIFICADOR";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "LINHANEGOCIO_IDENTIFICADOR";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "LINHANEGOCIO_IDENTIFICADOR";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTflinhanegocio_identificador_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflinhanegocio_identificador_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflinhanegocio_identificador_Visible), 5, 0)));
         edtavTflinhanegocio_identificador_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflinhanegocio_identificador_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflinhanegocio_identificador_sel_Visible), 5, 0)));
         edtavTflinhanegocio_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflinhanegocio_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflinhanegocio_descricao_Visible), 5, 0)));
         edtavTflinhanegocio_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflinhanegocio_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflinhanegocio_descricao_sel_Visible), 5, 0)));
         edtavTflinhanegocio_gpoobjctrlcod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflinhanegocio_gpoobjctrlcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflinhanegocio_gpoobjctrlcod_Visible), 5, 0)));
         edtavTflinhanegocio_gpoobjctrlcod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflinhanegocio_gpoobjctrlcod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflinhanegocio_gpoobjctrlcod_to_Visible), 5, 0)));
         Ddo_linhanegocio_identificador_Titlecontrolidtoreplace = subGrid_Internalname+"_LinhaNegocio_Identificador";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_identificador_Internalname, "TitleControlIdToReplace", Ddo_linhanegocio_identificador_Titlecontrolidtoreplace);
         AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace = Ddo_linhanegocio_identificador_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace", AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace);
         edtavDdo_linhanegocio_identificadortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_linhanegocio_identificadortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_linhanegocio_identificadortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_linhanegocio_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_LinhaNegocio_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_descricao_Internalname, "TitleControlIdToReplace", Ddo_linhanegocio_descricao_Titlecontrolidtoreplace);
         AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace = Ddo_linhanegocio_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace", AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace);
         edtavDdo_linhanegocio_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_linhanegocio_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_linhanegocio_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_linhanegocio_gpoobjctrlcod_Titlecontrolidtoreplace = subGrid_Internalname+"_LinhaNegocio_GpoObjCtrlCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_gpoobjctrlcod_Internalname, "TitleControlIdToReplace", Ddo_linhanegocio_gpoobjctrlcod_Titlecontrolidtoreplace);
         AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace = Ddo_linhanegocio_gpoobjctrlcod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace", AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace);
         edtavDdo_linhanegocio_gpoobjctrlcodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_linhanegocio_gpoobjctrlcodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_linhanegocio_gpoobjctrlcodtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Linhas de Neg�cio";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Identificador", 0);
         cmbavOrderedby.addItem("2", "Descri��o", 0);
         cmbavOrderedby.addItem("3", "Obj. de Ctrl.", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV47DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV47DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E27RK2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV35LinhaNegocio_IdentificadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39LinhaNegocio_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43LinhaNegocio_GpoObjCtrlCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtLinhaNegocio_Identificador_Titleformat = 2;
         edtLinhaNegocio_Identificador_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Identificador", AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLinhaNegocio_Identificador_Internalname, "Title", edtLinhaNegocio_Identificador_Title);
         edtLinhaNegocio_Descricao_Titleformat = 2;
         edtLinhaNegocio_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLinhaNegocio_Descricao_Internalname, "Title", edtLinhaNegocio_Descricao_Title);
         edtLinhaNegocio_GpoObjCtrlCod_Titleformat = 2;
         edtLinhaNegocio_GpoObjCtrlCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Obj. de Ctrl.", AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLinhaNegocio_GpoObjCtrlCod_Internalname, "Title", edtLinhaNegocio_GpoObjCtrlCod_Title);
         AV49GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49GridCurrentPage), 10, 0)));
         AV50GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50GridPageCount), 10, 0)));
         AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 = AV17LinhaNegocio_Identificador1;
         AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 = AV21LinhaNegocio_Identificador2;
         AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 = AV25LinhaNegocio_Identificador3;
         AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador = AV36TFLinhaNegocio_Identificador;
         AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel = AV37TFLinhaNegocio_Identificador_Sel;
         AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao = AV40TFLinhaNegocio_Descricao;
         AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel = AV41TFLinhaNegocio_Descricao_Sel;
         AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod = AV44TFLinhaNegocio_GpoObjCtrlCod;
         AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to = AV45TFLinhaNegocio_GpoObjCtrlCod_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35LinhaNegocio_IdentificadorTitleFilterData", AV35LinhaNegocio_IdentificadorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV39LinhaNegocio_DescricaoTitleFilterData", AV39LinhaNegocio_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV43LinhaNegocio_GpoObjCtrlCodTitleFilterData", AV43LinhaNegocio_GpoObjCtrlCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11RK2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV48PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV48PageToGo) ;
         }
      }

      protected void E12RK2( )
      {
         /* Ddo_linhanegocio_identificador_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_linhanegocio_identificador_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_linhanegocio_identificador_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_identificador_Internalname, "SortedStatus", Ddo_linhanegocio_identificador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_linhanegocio_identificador_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_linhanegocio_identificador_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_identificador_Internalname, "SortedStatus", Ddo_linhanegocio_identificador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_linhanegocio_identificador_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV36TFLinhaNegocio_Identificador = Ddo_linhanegocio_identificador_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFLinhaNegocio_Identificador", AV36TFLinhaNegocio_Identificador);
            AV37TFLinhaNegocio_Identificador_Sel = Ddo_linhanegocio_identificador_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFLinhaNegocio_Identificador_Sel", AV37TFLinhaNegocio_Identificador_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13RK2( )
      {
         /* Ddo_linhanegocio_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_linhanegocio_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_linhanegocio_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_descricao_Internalname, "SortedStatus", Ddo_linhanegocio_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_linhanegocio_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_linhanegocio_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_descricao_Internalname, "SortedStatus", Ddo_linhanegocio_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_linhanegocio_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFLinhaNegocio_Descricao = Ddo_linhanegocio_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFLinhaNegocio_Descricao", AV40TFLinhaNegocio_Descricao);
            AV41TFLinhaNegocio_Descricao_Sel = Ddo_linhanegocio_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFLinhaNegocio_Descricao_Sel", AV41TFLinhaNegocio_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14RK2( )
      {
         /* Ddo_linhanegocio_gpoobjctrlcod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_linhanegocio_gpoobjctrlcod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_gpoobjctrlcod_Internalname, "SortedStatus", Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_linhanegocio_gpoobjctrlcod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_gpoobjctrlcod_Internalname, "SortedStatus", Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_linhanegocio_gpoobjctrlcod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFLinhaNegocio_GpoObjCtrlCod = (int)(NumberUtil.Val( Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFLinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFLinhaNegocio_GpoObjCtrlCod), 6, 0)));
            AV45TFLinhaNegocio_GpoObjCtrlCod_To = (int)(NumberUtil.Val( Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLinhaNegocio_GpoObjCtrlCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFLinhaNegocio_GpoObjCtrlCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E28RK2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("linhanegocio.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A2036LinhadeNegocio_Codigo);
            AV51Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV51Update);
            AV73Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV51Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV51Update);
            AV73Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("linhanegocio.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A2036LinhadeNegocio_Codigo);
            AV52Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV52Delete);
            AV74Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV52Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV52Delete);
            AV74Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewlinhanegocio.aspx") + "?" + UrlEncode("" +A2036LinhadeNegocio_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV53Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV53Display);
            AV75Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV53Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV53Display);
            AV75Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 88;
         }
         sendrow_882( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_88_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(88, GridRow);
         }
      }

      protected void E15RK2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E21RK2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16RK2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LinhaNegocio_Identificador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21LinhaNegocio_Identificador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25LinhaNegocio_Identificador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV36TFLinhaNegocio_Identificador, AV37TFLinhaNegocio_Identificador_Sel, AV40TFLinhaNegocio_Descricao, AV41TFLinhaNegocio_Descricao_Sel, AV44TFLinhaNegocio_GpoObjCtrlCod, AV45TFLinhaNegocio_GpoObjCtrlCod_To, AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace, AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace, AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace, AV6WWPContext, AV76Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A2036LinhadeNegocio_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E22RK2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23RK2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E17RK2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LinhaNegocio_Identificador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21LinhaNegocio_Identificador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25LinhaNegocio_Identificador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV36TFLinhaNegocio_Identificador, AV37TFLinhaNegocio_Identificador_Sel, AV40TFLinhaNegocio_Descricao, AV41TFLinhaNegocio_Descricao_Sel, AV44TFLinhaNegocio_GpoObjCtrlCod, AV45TFLinhaNegocio_GpoObjCtrlCod_To, AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace, AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace, AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace, AV6WWPContext, AV76Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A2036LinhadeNegocio_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E24RK2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18RK2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LinhaNegocio_Identificador1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21LinhaNegocio_Identificador2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25LinhaNegocio_Identificador3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV36TFLinhaNegocio_Identificador, AV37TFLinhaNegocio_Identificador_Sel, AV40TFLinhaNegocio_Descricao, AV41TFLinhaNegocio_Descricao_Sel, AV44TFLinhaNegocio_GpoObjCtrlCod, AV45TFLinhaNegocio_GpoObjCtrlCod_To, AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace, AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace, AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace, AV6WWPContext, AV76Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A2036LinhadeNegocio_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E25RK2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19RK2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E20RK2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("linhanegocio.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S192( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_linhanegocio_identificador_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_identificador_Internalname, "SortedStatus", Ddo_linhanegocio_identificador_Sortedstatus);
         Ddo_linhanegocio_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_descricao_Internalname, "SortedStatus", Ddo_linhanegocio_descricao_Sortedstatus);
         Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_gpoobjctrlcod_Internalname, "SortedStatus", Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_linhanegocio_identificador_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_identificador_Internalname, "SortedStatus", Ddo_linhanegocio_identificador_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_linhanegocio_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_descricao_Internalname, "SortedStatus", Ddo_linhanegocio_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_gpoobjctrlcod_Internalname, "SortedStatus", Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus);
         }
      }

      protected void S172( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( AV6WWPContext.gxTpr_Insert ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavLinhanegocio_identificador1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinhanegocio_identificador1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLinhanegocio_identificador1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LINHANEGOCIO_IDENTIFICADOR") == 0 )
         {
            edtavLinhanegocio_identificador1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinhanegocio_identificador1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLinhanegocio_identificador1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavLinhanegocio_identificador2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinhanegocio_identificador2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLinhanegocio_identificador2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "LINHANEGOCIO_IDENTIFICADOR") == 0 )
         {
            edtavLinhanegocio_identificador2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinhanegocio_identificador2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLinhanegocio_identificador2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavLinhanegocio_identificador3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinhanegocio_identificador3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLinhanegocio_identificador3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "LINHANEGOCIO_IDENTIFICADOR") == 0 )
         {
            edtavLinhanegocio_identificador3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinhanegocio_identificador3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLinhanegocio_identificador3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "LINHANEGOCIO_IDENTIFICADOR";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21LinhaNegocio_Identificador2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21LinhaNegocio_Identificador2", AV21LinhaNegocio_Identificador2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "LINHANEGOCIO_IDENTIFICADOR";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25LinhaNegocio_Identificador3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25LinhaNegocio_Identificador3", AV25LinhaNegocio_Identificador3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'CLEANFILTERS' Routine */
         AV36TFLinhaNegocio_Identificador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFLinhaNegocio_Identificador", AV36TFLinhaNegocio_Identificador);
         Ddo_linhanegocio_identificador_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_identificador_Internalname, "FilteredText_set", Ddo_linhanegocio_identificador_Filteredtext_set);
         AV37TFLinhaNegocio_Identificador_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFLinhaNegocio_Identificador_Sel", AV37TFLinhaNegocio_Identificador_Sel);
         Ddo_linhanegocio_identificador_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_identificador_Internalname, "SelectedValue_set", Ddo_linhanegocio_identificador_Selectedvalue_set);
         AV40TFLinhaNegocio_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFLinhaNegocio_Descricao", AV40TFLinhaNegocio_Descricao);
         Ddo_linhanegocio_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_descricao_Internalname, "FilteredText_set", Ddo_linhanegocio_descricao_Filteredtext_set);
         AV41TFLinhaNegocio_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFLinhaNegocio_Descricao_Sel", AV41TFLinhaNegocio_Descricao_Sel);
         Ddo_linhanegocio_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_descricao_Internalname, "SelectedValue_set", Ddo_linhanegocio_descricao_Selectedvalue_set);
         AV44TFLinhaNegocio_GpoObjCtrlCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFLinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFLinhaNegocio_GpoObjCtrlCod), 6, 0)));
         Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_gpoobjctrlcod_Internalname, "FilteredText_set", Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_set);
         AV45TFLinhaNegocio_GpoObjCtrlCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLinhaNegocio_GpoObjCtrlCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFLinhaNegocio_GpoObjCtrlCod_To), 6, 0)));
         Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_gpoobjctrlcod_Internalname, "FilteredTextTo_set", Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "LINHANEGOCIO_IDENTIFICADOR";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17LinhaNegocio_Identificador1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17LinhaNegocio_Identificador1", AV17LinhaNegocio_Identificador1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV28Session.Get(AV76Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV76Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV28Session.Get(AV76Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S242( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV77GXV1 = 1;
         while ( AV77GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV77GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFLINHANEGOCIO_IDENTIFICADOR") == 0 )
            {
               AV36TFLinhaNegocio_Identificador = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFLinhaNegocio_Identificador", AV36TFLinhaNegocio_Identificador);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFLinhaNegocio_Identificador)) )
               {
                  Ddo_linhanegocio_identificador_Filteredtext_set = AV36TFLinhaNegocio_Identificador;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_identificador_Internalname, "FilteredText_set", Ddo_linhanegocio_identificador_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFLINHANEGOCIO_IDENTIFICADOR_SEL") == 0 )
            {
               AV37TFLinhaNegocio_Identificador_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFLinhaNegocio_Identificador_Sel", AV37TFLinhaNegocio_Identificador_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFLinhaNegocio_Identificador_Sel)) )
               {
                  Ddo_linhanegocio_identificador_Selectedvalue_set = AV37TFLinhaNegocio_Identificador_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_identificador_Internalname, "SelectedValue_set", Ddo_linhanegocio_identificador_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFLINHANEGOCIO_DESCRICAO") == 0 )
            {
               AV40TFLinhaNegocio_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFLinhaNegocio_Descricao", AV40TFLinhaNegocio_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFLinhaNegocio_Descricao)) )
               {
                  Ddo_linhanegocio_descricao_Filteredtext_set = AV40TFLinhaNegocio_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_descricao_Internalname, "FilteredText_set", Ddo_linhanegocio_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFLINHANEGOCIO_DESCRICAO_SEL") == 0 )
            {
               AV41TFLinhaNegocio_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFLinhaNegocio_Descricao_Sel", AV41TFLinhaNegocio_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFLinhaNegocio_Descricao_Sel)) )
               {
                  Ddo_linhanegocio_descricao_Selectedvalue_set = AV41TFLinhaNegocio_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_descricao_Internalname, "SelectedValue_set", Ddo_linhanegocio_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFLINHANEGOCIO_GPOOBJCTRLCOD") == 0 )
            {
               AV44TFLinhaNegocio_GpoObjCtrlCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFLinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFLinhaNegocio_GpoObjCtrlCod), 6, 0)));
               AV45TFLinhaNegocio_GpoObjCtrlCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLinhaNegocio_GpoObjCtrlCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFLinhaNegocio_GpoObjCtrlCod_To), 6, 0)));
               if ( ! (0==AV44TFLinhaNegocio_GpoObjCtrlCod) )
               {
                  Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_set = StringUtil.Str( (decimal)(AV44TFLinhaNegocio_GpoObjCtrlCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_gpoobjctrlcod_Internalname, "FilteredText_set", Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_set);
               }
               if ( ! (0==AV45TFLinhaNegocio_GpoObjCtrlCod_To) )
               {
                  Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_set = StringUtil.Str( (decimal)(AV45TFLinhaNegocio_GpoObjCtrlCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_linhanegocio_gpoobjctrlcod_Internalname, "FilteredTextTo_set", Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_set);
               }
            }
            AV77GXV1 = (int)(AV77GXV1+1);
         }
      }

      protected void S222( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LINHANEGOCIO_IDENTIFICADOR") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17LinhaNegocio_Identificador1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17LinhaNegocio_Identificador1", AV17LinhaNegocio_Identificador1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "LINHANEGOCIO_IDENTIFICADOR") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21LinhaNegocio_Identificador2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21LinhaNegocio_Identificador2", AV21LinhaNegocio_Identificador2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "LINHANEGOCIO_IDENTIFICADOR") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25LinhaNegocio_Identificador3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25LinhaNegocio_Identificador3", AV25LinhaNegocio_Identificador3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S182( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV28Session.Get(AV76Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TFLinhaNegocio_Identificador)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLINHANEGOCIO_IDENTIFICADOR";
            AV11GridStateFilterValue.gxTpr_Value = AV36TFLinhaNegocio_Identificador;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFLinhaNegocio_Identificador_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLINHANEGOCIO_IDENTIFICADOR_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV37TFLinhaNegocio_Identificador_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFLinhaNegocio_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLINHANEGOCIO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV40TFLinhaNegocio_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFLinhaNegocio_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLINHANEGOCIO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV41TFLinhaNegocio_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV44TFLinhaNegocio_GpoObjCtrlCod) && (0==AV45TFLinhaNegocio_GpoObjCtrlCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLINHANEGOCIO_GPOOBJCTRLCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV44TFLinhaNegocio_GpoObjCtrlCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV45TFLinhaNegocio_GpoObjCtrlCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV76Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S202( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17LinhaNegocio_Identificador1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17LinhaNegocio_Identificador1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21LinhaNegocio_Identificador2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21LinhaNegocio_Identificador2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25LinhaNegocio_Identificador3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25LinhaNegocio_Identificador3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV76Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "LinhaNegocio";
         AV28Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_RK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_RK2( true) ;
         }
         else
         {
            wb_table2_8_RK2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_RK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_82_RK2( true) ;
         }
         else
         {
            wb_table3_82_RK2( false) ;
         }
         return  ;
      }

      protected void wb_table3_82_RK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_RK2e( true) ;
         }
         else
         {
            wb_table1_2_RK2e( false) ;
         }
      }

      protected void wb_table3_82_RK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_85_RK2( true) ;
         }
         else
         {
            wb_table4_85_RK2( false) ;
         }
         return  ;
      }

      protected void wb_table4_85_RK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_82_RK2e( true) ;
         }
         else
         {
            wb_table3_82_RK2e( false) ;
         }
      }

      protected void wb_table4_85_RK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"88\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLinhaNegocio_Identificador_Titleformat == 0 )
               {
                  context.SendWebValue( edtLinhaNegocio_Identificador_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLinhaNegocio_Identificador_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLinhaNegocio_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtLinhaNegocio_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLinhaNegocio_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLinhaNegocio_GpoObjCtrlCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtLinhaNegocio_GpoObjCtrlCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLinhaNegocio_GpoObjCtrlCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV51Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV52Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV53Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A2037LinhaNegocio_Identificador);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLinhaNegocio_Identificador_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLinhaNegocio_Identificador_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A2038LinhaNegocio_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLinhaNegocio_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLinhaNegocio_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLinhaNegocio_GpoObjCtrlCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLinhaNegocio_GpoObjCtrlCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 88 )
         {
            wbEnd = 0;
            nRC_GXsfl_88 = (short)(nGXsfl_88_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_85_RK2e( true) ;
         }
         else
         {
            wb_table4_85_RK2e( false) ;
         }
      }

      protected void wb_table2_8_RK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_RK2( true) ;
         }
         else
         {
            wb_table5_11_RK2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_RK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_RK2( true) ;
         }
         else
         {
            wb_table6_23_RK2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_RK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_RK2e( true) ;
         }
         else
         {
            wb_table2_8_RK2e( false) ;
         }
      }

      protected void wb_table6_23_RK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_RK2( true) ;
         }
         else
         {
            wb_table7_28_RK2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_RK2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_RK2e( true) ;
         }
         else
         {
            wb_table6_23_RK2e( false) ;
         }
      }

      protected void wb_table7_28_RK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWLinhaNegocio.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_RK2( true) ;
         }
         else
         {
            wb_table8_37_RK2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_RK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLinhaNegocio.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWLinhaNegocio.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_54_RK2( true) ;
         }
         else
         {
            wb_table9_54_RK2( false) ;
         }
         return  ;
      }

      protected void wb_table9_54_RK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLinhaNegocio.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWLinhaNegocio.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_71_RK2( true) ;
         }
         else
         {
            wb_table10_71_RK2( false) ;
         }
         return  ;
      }

      protected void wb_table10_71_RK2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_RK2e( true) ;
         }
         else
         {
            wb_table7_28_RK2e( false) ;
         }
      }

      protected void wb_table10_71_RK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWLinhaNegocio.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLinhanegocio_identificador3_Internalname, AV25LinhaNegocio_Identificador3, StringUtil.RTrim( context.localUtil.Format( AV25LinhaNegocio_Identificador3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLinhanegocio_identificador3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLinhanegocio_identificador3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_71_RK2e( true) ;
         }
         else
         {
            wb_table10_71_RK2e( false) ;
         }
      }

      protected void wb_table9_54_RK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWLinhaNegocio.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLinhanegocio_identificador2_Internalname, AV21LinhaNegocio_Identificador2, StringUtil.RTrim( context.localUtil.Format( AV21LinhaNegocio_Identificador2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLinhanegocio_identificador2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLinhanegocio_identificador2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_54_RK2e( true) ;
         }
         else
         {
            wb_table9_54_RK2e( false) ;
         }
      }

      protected void wb_table8_37_RK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWLinhaNegocio.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLinhanegocio_identificador1_Internalname, AV17LinhaNegocio_Identificador1, StringUtil.RTrim( context.localUtil.Format( AV17LinhaNegocio_Identificador1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLinhanegocio_identificador1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLinhanegocio_identificador1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_RK2e( true) ;
         }
         else
         {
            wb_table8_37_RK2e( false) ;
         }
      }

      protected void wb_table5_11_RK2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLinhanegociotitle_Internalname, "Linhas de Neg�cio", "", "", lblLinhanegociotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWLinhaNegocio.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWLinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_RK2e( true) ;
         }
         else
         {
            wb_table5_11_RK2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PARK2( ) ;
         WSRK2( ) ;
         WERK2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203119123049");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwlinhanegocio.js", "?20203119123050");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_88_idx;
         edtLinhadeNegocio_Codigo_Internalname = "LINHADENEGOCIO_CODIGO_"+sGXsfl_88_idx;
         edtLinhaNegocio_Identificador_Internalname = "LINHANEGOCIO_IDENTIFICADOR_"+sGXsfl_88_idx;
         edtLinhaNegocio_Descricao_Internalname = "LINHANEGOCIO_DESCRICAO_"+sGXsfl_88_idx;
         edtLinhaNegocio_GpoObjCtrlCod_Internalname = "LINHANEGOCIO_GPOOBJCTRLCOD_"+sGXsfl_88_idx;
      }

      protected void SubsflControlProps_fel_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_88_fel_idx;
         edtLinhadeNegocio_Codigo_Internalname = "LINHADENEGOCIO_CODIGO_"+sGXsfl_88_fel_idx;
         edtLinhaNegocio_Identificador_Internalname = "LINHANEGOCIO_IDENTIFICADOR_"+sGXsfl_88_fel_idx;
         edtLinhaNegocio_Descricao_Internalname = "LINHANEGOCIO_DESCRICAO_"+sGXsfl_88_fel_idx;
         edtLinhaNegocio_GpoObjCtrlCod_Internalname = "LINHANEGOCIO_GPOOBJCTRLCOD_"+sGXsfl_88_fel_idx;
      }

      protected void sendrow_882( )
      {
         SubsflControlProps_882( ) ;
         WBRK0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_88_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_88_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_88_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV51Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV51Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV73Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV51Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV51Update)) ? AV73Update_GXI : context.PathToRelativeUrl( AV51Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV51Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV52Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV52Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV74Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV52Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV52Delete)) ? AV74Delete_GXI : context.PathToRelativeUrl( AV52Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV52Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV53Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV53Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV75Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV53Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV53Display)) ? AV75Display_GXI : context.PathToRelativeUrl( AV53Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV53Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLinhadeNegocio_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2036LinhadeNegocio_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLinhadeNegocio_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLinhaNegocio_Identificador_Internalname,(String)A2037LinhaNegocio_Identificador,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLinhaNegocio_Identificador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLinhaNegocio_Descricao_Internalname,(String)A2038LinhaNegocio_Descricao,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLinhaNegocio_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)250,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLinhaNegocio_GpoObjCtrlCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLinhaNegocio_GpoObjCtrlCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_LINHADENEGOCIO_CODIGO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A2036LinhadeNegocio_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_LINHANEGOCIO_IDENTIFICADOR"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, StringUtil.RTrim( context.localUtil.Format( A2037LinhaNegocio_Identificador, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_LINHANEGOCIO_DESCRICAO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, StringUtil.RTrim( context.localUtil.Format( A2038LinhaNegocio_Descricao, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_LINHANEGOCIO_GPOOBJCTRLCOD"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         /* End function sendrow_882 */
      }

      protected void init_default_properties( )
      {
         lblLinhanegociotitle_Internalname = "LINHANEGOCIOTITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavLinhanegocio_identificador1_Internalname = "vLINHANEGOCIO_IDENTIFICADOR1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavLinhanegocio_identificador2_Internalname = "vLINHANEGOCIO_IDENTIFICADOR2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavLinhanegocio_identificador3_Internalname = "vLINHANEGOCIO_IDENTIFICADOR3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtLinhadeNegocio_Codigo_Internalname = "LINHADENEGOCIO_CODIGO";
         edtLinhaNegocio_Identificador_Internalname = "LINHANEGOCIO_IDENTIFICADOR";
         edtLinhaNegocio_Descricao_Internalname = "LINHANEGOCIO_DESCRICAO";
         edtLinhaNegocio_GpoObjCtrlCod_Internalname = "LINHANEGOCIO_GPOOBJCTRLCOD";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTflinhanegocio_identificador_Internalname = "vTFLINHANEGOCIO_IDENTIFICADOR";
         edtavTflinhanegocio_identificador_sel_Internalname = "vTFLINHANEGOCIO_IDENTIFICADOR_SEL";
         edtavTflinhanegocio_descricao_Internalname = "vTFLINHANEGOCIO_DESCRICAO";
         edtavTflinhanegocio_descricao_sel_Internalname = "vTFLINHANEGOCIO_DESCRICAO_SEL";
         edtavTflinhanegocio_gpoobjctrlcod_Internalname = "vTFLINHANEGOCIO_GPOOBJCTRLCOD";
         edtavTflinhanegocio_gpoobjctrlcod_to_Internalname = "vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO";
         Ddo_linhanegocio_identificador_Internalname = "DDO_LINHANEGOCIO_IDENTIFICADOR";
         edtavDdo_linhanegocio_identificadortitlecontrolidtoreplace_Internalname = "vDDO_LINHANEGOCIO_IDENTIFICADORTITLECONTROLIDTOREPLACE";
         Ddo_linhanegocio_descricao_Internalname = "DDO_LINHANEGOCIO_DESCRICAO";
         edtavDdo_linhanegocio_descricaotitlecontrolidtoreplace_Internalname = "vDDO_LINHANEGOCIO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_linhanegocio_gpoobjctrlcod_Internalname = "DDO_LINHANEGOCIO_GPOOBJCTRLCOD";
         edtavDdo_linhanegocio_gpoobjctrlcodtitlecontrolidtoreplace_Internalname = "vDDO_LINHANEGOCIO_GPOOBJCTRLCODTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtLinhaNegocio_GpoObjCtrlCod_Jsonclick = "";
         edtLinhaNegocio_Descricao_Jsonclick = "";
         edtLinhaNegocio_Identificador_Jsonclick = "";
         edtLinhadeNegocio_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavLinhanegocio_identificador1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavLinhanegocio_identificador2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavLinhanegocio_identificador3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtLinhaNegocio_GpoObjCtrlCod_Titleformat = 0;
         edtLinhaNegocio_Descricao_Titleformat = 0;
         edtLinhaNegocio_Identificador_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavLinhanegocio_identificador3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavLinhanegocio_identificador2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavLinhanegocio_identificador1_Visible = 1;
         edtLinhaNegocio_GpoObjCtrlCod_Title = "Obj. de Ctrl.";
         edtLinhaNegocio_Descricao_Title = "Descri��o";
         edtLinhaNegocio_Identificador_Title = "Identificador";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_linhanegocio_gpoobjctrlcodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_linhanegocio_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_linhanegocio_identificadortitlecontrolidtoreplace_Visible = 1;
         edtavTflinhanegocio_gpoobjctrlcod_to_Jsonclick = "";
         edtavTflinhanegocio_gpoobjctrlcod_to_Visible = 1;
         edtavTflinhanegocio_gpoobjctrlcod_Jsonclick = "";
         edtavTflinhanegocio_gpoobjctrlcod_Visible = 1;
         edtavTflinhanegocio_descricao_sel_Visible = 1;
         edtavTflinhanegocio_descricao_Visible = 1;
         edtavTflinhanegocio_identificador_sel_Jsonclick = "";
         edtavTflinhanegocio_identificador_sel_Visible = 1;
         edtavTflinhanegocio_identificador_Jsonclick = "";
         edtavTflinhanegocio_identificador_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_linhanegocio_gpoobjctrlcod_Searchbuttontext = "Pesquisar";
         Ddo_linhanegocio_gpoobjctrlcod_Rangefilterto = "At�";
         Ddo_linhanegocio_gpoobjctrlcod_Rangefilterfrom = "Desde";
         Ddo_linhanegocio_gpoobjctrlcod_Cleanfilter = "Limpar pesquisa";
         Ddo_linhanegocio_gpoobjctrlcod_Sortdsc = "Ordenar de Z � A";
         Ddo_linhanegocio_gpoobjctrlcod_Sortasc = "Ordenar de A � Z";
         Ddo_linhanegocio_gpoobjctrlcod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_linhanegocio_gpoobjctrlcod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_linhanegocio_gpoobjctrlcod_Filtertype = "Numeric";
         Ddo_linhanegocio_gpoobjctrlcod_Includefilter = Convert.ToBoolean( -1);
         Ddo_linhanegocio_gpoobjctrlcod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_linhanegocio_gpoobjctrlcod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_linhanegocio_gpoobjctrlcod_Titlecontrolidtoreplace = "";
         Ddo_linhanegocio_gpoobjctrlcod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_linhanegocio_gpoobjctrlcod_Cls = "ColumnSettings";
         Ddo_linhanegocio_gpoobjctrlcod_Tooltip = "Op��es";
         Ddo_linhanegocio_gpoobjctrlcod_Caption = "";
         Ddo_linhanegocio_descricao_Searchbuttontext = "Pesquisar";
         Ddo_linhanegocio_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_linhanegocio_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_linhanegocio_descricao_Loadingdata = "Carregando dados...";
         Ddo_linhanegocio_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_linhanegocio_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_linhanegocio_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_linhanegocio_descricao_Datalistproc = "GetWWLinhaNegocioFilterData";
         Ddo_linhanegocio_descricao_Datalisttype = "Dynamic";
         Ddo_linhanegocio_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_linhanegocio_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_linhanegocio_descricao_Filtertype = "Character";
         Ddo_linhanegocio_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_linhanegocio_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_linhanegocio_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_linhanegocio_descricao_Titlecontrolidtoreplace = "";
         Ddo_linhanegocio_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_linhanegocio_descricao_Cls = "ColumnSettings";
         Ddo_linhanegocio_descricao_Tooltip = "Op��es";
         Ddo_linhanegocio_descricao_Caption = "";
         Ddo_linhanegocio_identificador_Searchbuttontext = "Pesquisar";
         Ddo_linhanegocio_identificador_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_linhanegocio_identificador_Cleanfilter = "Limpar pesquisa";
         Ddo_linhanegocio_identificador_Loadingdata = "Carregando dados...";
         Ddo_linhanegocio_identificador_Sortdsc = "Ordenar de Z � A";
         Ddo_linhanegocio_identificador_Sortasc = "Ordenar de A � Z";
         Ddo_linhanegocio_identificador_Datalistupdateminimumcharacters = 0;
         Ddo_linhanegocio_identificador_Datalistproc = "GetWWLinhaNegocioFilterData";
         Ddo_linhanegocio_identificador_Datalisttype = "Dynamic";
         Ddo_linhanegocio_identificador_Includedatalist = Convert.ToBoolean( -1);
         Ddo_linhanegocio_identificador_Filterisrange = Convert.ToBoolean( 0);
         Ddo_linhanegocio_identificador_Filtertype = "Character";
         Ddo_linhanegocio_identificador_Includefilter = Convert.ToBoolean( -1);
         Ddo_linhanegocio_identificador_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_linhanegocio_identificador_Includesortasc = Convert.ToBoolean( -1);
         Ddo_linhanegocio_identificador_Titlecontrolidtoreplace = "";
         Ddo_linhanegocio_identificador_Dropdownoptionstype = "GridTitleSettings";
         Ddo_linhanegocio_identificador_Cls = "ColumnSettings";
         Ddo_linhanegocio_identificador_Tooltip = "Op��es";
         Ddo_linhanegocio_identificador_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "WWLinha Negocio";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A2036LinhadeNegocio_Codigo',fld:'LINHADENEGOCIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_GPOOBJCTRLCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'AV36TFLinhaNegocio_Identificador',fld:'vTFLINHANEGOCIO_IDENTIFICADOR',pic:'',nv:''},{av:'AV37TFLinhaNegocio_Identificador_Sel',fld:'vTFLINHANEGOCIO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFLinhaNegocio_Descricao',fld:'vTFLINHANEGOCIO_DESCRICAO',pic:'',nv:''},{av:'AV41TFLinhaNegocio_Descricao_Sel',fld:'vTFLINHANEGOCIO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV44TFLinhaNegocio_GpoObjCtrlCod',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFLinhaNegocio_GpoObjCtrlCod_To',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV35LinhaNegocio_IdentificadorTitleFilterData',fld:'vLINHANEGOCIO_IDENTIFICADORTITLEFILTERDATA',pic:'',nv:null},{av:'AV39LinhaNegocio_DescricaoTitleFilterData',fld:'vLINHANEGOCIO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV43LinhaNegocio_GpoObjCtrlCodTitleFilterData',fld:'vLINHANEGOCIO_GPOOBJCTRLCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtLinhaNegocio_Identificador_Titleformat',ctrl:'LINHANEGOCIO_IDENTIFICADOR',prop:'Titleformat'},{av:'edtLinhaNegocio_Identificador_Title',ctrl:'LINHANEGOCIO_IDENTIFICADOR',prop:'Title'},{av:'edtLinhaNegocio_Descricao_Titleformat',ctrl:'LINHANEGOCIO_DESCRICAO',prop:'Titleformat'},{av:'edtLinhaNegocio_Descricao_Title',ctrl:'LINHANEGOCIO_DESCRICAO',prop:'Title'},{av:'edtLinhaNegocio_GpoObjCtrlCod_Titleformat',ctrl:'LINHANEGOCIO_GPOOBJCTRLCOD',prop:'Titleformat'},{av:'edtLinhaNegocio_GpoObjCtrlCod_Title',ctrl:'LINHANEGOCIO_GPOOBJCTRLCOD',prop:'Title'},{av:'AV49GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV50GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11RK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFLinhaNegocio_Identificador',fld:'vTFLINHANEGOCIO_IDENTIFICADOR',pic:'',nv:''},{av:'AV37TFLinhaNegocio_Identificador_Sel',fld:'vTFLINHANEGOCIO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFLinhaNegocio_Descricao',fld:'vTFLINHANEGOCIO_DESCRICAO',pic:'',nv:''},{av:'AV41TFLinhaNegocio_Descricao_Sel',fld:'vTFLINHANEGOCIO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV44TFLinhaNegocio_GpoObjCtrlCod',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFLinhaNegocio_GpoObjCtrlCod_To',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_GPOOBJCTRLCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2036LinhadeNegocio_Codigo',fld:'LINHADENEGOCIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_LINHANEGOCIO_IDENTIFICADOR.ONOPTIONCLICKED","{handler:'E12RK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFLinhaNegocio_Identificador',fld:'vTFLINHANEGOCIO_IDENTIFICADOR',pic:'',nv:''},{av:'AV37TFLinhaNegocio_Identificador_Sel',fld:'vTFLINHANEGOCIO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFLinhaNegocio_Descricao',fld:'vTFLINHANEGOCIO_DESCRICAO',pic:'',nv:''},{av:'AV41TFLinhaNegocio_Descricao_Sel',fld:'vTFLINHANEGOCIO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV44TFLinhaNegocio_GpoObjCtrlCod',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFLinhaNegocio_GpoObjCtrlCod_To',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_GPOOBJCTRLCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2036LinhadeNegocio_Codigo',fld:'LINHADENEGOCIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_linhanegocio_identificador_Activeeventkey',ctrl:'DDO_LINHANEGOCIO_IDENTIFICADOR',prop:'ActiveEventKey'},{av:'Ddo_linhanegocio_identificador_Filteredtext_get',ctrl:'DDO_LINHANEGOCIO_IDENTIFICADOR',prop:'FilteredText_get'},{av:'Ddo_linhanegocio_identificador_Selectedvalue_get',ctrl:'DDO_LINHANEGOCIO_IDENTIFICADOR',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_linhanegocio_identificador_Sortedstatus',ctrl:'DDO_LINHANEGOCIO_IDENTIFICADOR',prop:'SortedStatus'},{av:'AV36TFLinhaNegocio_Identificador',fld:'vTFLINHANEGOCIO_IDENTIFICADOR',pic:'',nv:''},{av:'AV37TFLinhaNegocio_Identificador_Sel',fld:'vTFLINHANEGOCIO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'Ddo_linhanegocio_descricao_Sortedstatus',ctrl:'DDO_LINHANEGOCIO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus',ctrl:'DDO_LINHANEGOCIO_GPOOBJCTRLCOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LINHANEGOCIO_DESCRICAO.ONOPTIONCLICKED","{handler:'E13RK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFLinhaNegocio_Identificador',fld:'vTFLINHANEGOCIO_IDENTIFICADOR',pic:'',nv:''},{av:'AV37TFLinhaNegocio_Identificador_Sel',fld:'vTFLINHANEGOCIO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFLinhaNegocio_Descricao',fld:'vTFLINHANEGOCIO_DESCRICAO',pic:'',nv:''},{av:'AV41TFLinhaNegocio_Descricao_Sel',fld:'vTFLINHANEGOCIO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV44TFLinhaNegocio_GpoObjCtrlCod',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFLinhaNegocio_GpoObjCtrlCod_To',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_GPOOBJCTRLCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2036LinhadeNegocio_Codigo',fld:'LINHADENEGOCIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_linhanegocio_descricao_Activeeventkey',ctrl:'DDO_LINHANEGOCIO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_linhanegocio_descricao_Filteredtext_get',ctrl:'DDO_LINHANEGOCIO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_linhanegocio_descricao_Selectedvalue_get',ctrl:'DDO_LINHANEGOCIO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_linhanegocio_descricao_Sortedstatus',ctrl:'DDO_LINHANEGOCIO_DESCRICAO',prop:'SortedStatus'},{av:'AV40TFLinhaNegocio_Descricao',fld:'vTFLINHANEGOCIO_DESCRICAO',pic:'',nv:''},{av:'AV41TFLinhaNegocio_Descricao_Sel',fld:'vTFLINHANEGOCIO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_linhanegocio_identificador_Sortedstatus',ctrl:'DDO_LINHANEGOCIO_IDENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus',ctrl:'DDO_LINHANEGOCIO_GPOOBJCTRLCOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LINHANEGOCIO_GPOOBJCTRLCOD.ONOPTIONCLICKED","{handler:'E14RK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFLinhaNegocio_Identificador',fld:'vTFLINHANEGOCIO_IDENTIFICADOR',pic:'',nv:''},{av:'AV37TFLinhaNegocio_Identificador_Sel',fld:'vTFLINHANEGOCIO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFLinhaNegocio_Descricao',fld:'vTFLINHANEGOCIO_DESCRICAO',pic:'',nv:''},{av:'AV41TFLinhaNegocio_Descricao_Sel',fld:'vTFLINHANEGOCIO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV44TFLinhaNegocio_GpoObjCtrlCod',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFLinhaNegocio_GpoObjCtrlCod_To',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_GPOOBJCTRLCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2036LinhadeNegocio_Codigo',fld:'LINHADENEGOCIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_linhanegocio_gpoobjctrlcod_Activeeventkey',ctrl:'DDO_LINHANEGOCIO_GPOOBJCTRLCOD',prop:'ActiveEventKey'},{av:'Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_get',ctrl:'DDO_LINHANEGOCIO_GPOOBJCTRLCOD',prop:'FilteredText_get'},{av:'Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_get',ctrl:'DDO_LINHANEGOCIO_GPOOBJCTRLCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus',ctrl:'DDO_LINHANEGOCIO_GPOOBJCTRLCOD',prop:'SortedStatus'},{av:'AV44TFLinhaNegocio_GpoObjCtrlCod',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFLinhaNegocio_GpoObjCtrlCod_To',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_linhanegocio_identificador_Sortedstatus',ctrl:'DDO_LINHANEGOCIO_IDENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_linhanegocio_descricao_Sortedstatus',ctrl:'DDO_LINHANEGOCIO_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E28RK2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2036LinhadeNegocio_Codigo',fld:'LINHADENEGOCIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV51Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV52Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV53Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15RK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFLinhaNegocio_Identificador',fld:'vTFLINHANEGOCIO_IDENTIFICADOR',pic:'',nv:''},{av:'AV37TFLinhaNegocio_Identificador_Sel',fld:'vTFLINHANEGOCIO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFLinhaNegocio_Descricao',fld:'vTFLINHANEGOCIO_DESCRICAO',pic:'',nv:''},{av:'AV41TFLinhaNegocio_Descricao_Sel',fld:'vTFLINHANEGOCIO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV44TFLinhaNegocio_GpoObjCtrlCod',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFLinhaNegocio_GpoObjCtrlCod_To',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_GPOOBJCTRLCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2036LinhadeNegocio_Codigo',fld:'LINHADENEGOCIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21RK2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16RK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFLinhaNegocio_Identificador',fld:'vTFLINHANEGOCIO_IDENTIFICADOR',pic:'',nv:''},{av:'AV37TFLinhaNegocio_Identificador_Sel',fld:'vTFLINHANEGOCIO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFLinhaNegocio_Descricao',fld:'vTFLINHANEGOCIO_DESCRICAO',pic:'',nv:''},{av:'AV41TFLinhaNegocio_Descricao_Sel',fld:'vTFLINHANEGOCIO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV44TFLinhaNegocio_GpoObjCtrlCod',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFLinhaNegocio_GpoObjCtrlCod_To',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_GPOOBJCTRLCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2036LinhadeNegocio_Codigo',fld:'LINHADENEGOCIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavLinhanegocio_identificador2_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLinhanegocio_identificador3_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavLinhanegocio_identificador1_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22RK2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavLinhanegocio_identificador1_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E23RK2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17RK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFLinhaNegocio_Identificador',fld:'vTFLINHANEGOCIO_IDENTIFICADOR',pic:'',nv:''},{av:'AV37TFLinhaNegocio_Identificador_Sel',fld:'vTFLINHANEGOCIO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFLinhaNegocio_Descricao',fld:'vTFLINHANEGOCIO_DESCRICAO',pic:'',nv:''},{av:'AV41TFLinhaNegocio_Descricao_Sel',fld:'vTFLINHANEGOCIO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV44TFLinhaNegocio_GpoObjCtrlCod',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFLinhaNegocio_GpoObjCtrlCod_To',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_GPOOBJCTRLCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2036LinhadeNegocio_Codigo',fld:'LINHADENEGOCIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavLinhanegocio_identificador2_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLinhanegocio_identificador3_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavLinhanegocio_identificador1_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E24RK2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavLinhanegocio_identificador2_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E18RK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFLinhaNegocio_Identificador',fld:'vTFLINHANEGOCIO_IDENTIFICADOR',pic:'',nv:''},{av:'AV37TFLinhaNegocio_Identificador_Sel',fld:'vTFLINHANEGOCIO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFLinhaNegocio_Descricao',fld:'vTFLINHANEGOCIO_DESCRICAO',pic:'',nv:''},{av:'AV41TFLinhaNegocio_Descricao_Sel',fld:'vTFLINHANEGOCIO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV44TFLinhaNegocio_GpoObjCtrlCod',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFLinhaNegocio_GpoObjCtrlCod_To',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_GPOOBJCTRLCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2036LinhadeNegocio_Codigo',fld:'LINHADENEGOCIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavLinhanegocio_identificador2_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLinhanegocio_identificador3_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavLinhanegocio_identificador1_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E25RK2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavLinhanegocio_identificador3_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19RK2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFLinhaNegocio_Identificador',fld:'vTFLINHANEGOCIO_IDENTIFICADOR',pic:'',nv:''},{av:'AV37TFLinhaNegocio_Identificador_Sel',fld:'vTFLINHANEGOCIO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'AV40TFLinhaNegocio_Descricao',fld:'vTFLINHANEGOCIO_DESCRICAO',pic:'',nv:''},{av:'AV41TFLinhaNegocio_Descricao_Sel',fld:'vTFLINHANEGOCIO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV44TFLinhaNegocio_GpoObjCtrlCod',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD',pic:'ZZZZZ9',nv:0},{av:'AV45TFLinhaNegocio_GpoObjCtrlCod_To',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_IDENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace',fld:'vDDO_LINHANEGOCIO_GPOOBJCTRLCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV76Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2036LinhadeNegocio_Codigo',fld:'LINHADENEGOCIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV36TFLinhaNegocio_Identificador',fld:'vTFLINHANEGOCIO_IDENTIFICADOR',pic:'',nv:''},{av:'Ddo_linhanegocio_identificador_Filteredtext_set',ctrl:'DDO_LINHANEGOCIO_IDENTIFICADOR',prop:'FilteredText_set'},{av:'AV37TFLinhaNegocio_Identificador_Sel',fld:'vTFLINHANEGOCIO_IDENTIFICADOR_SEL',pic:'',nv:''},{av:'Ddo_linhanegocio_identificador_Selectedvalue_set',ctrl:'DDO_LINHANEGOCIO_IDENTIFICADOR',prop:'SelectedValue_set'},{av:'AV40TFLinhaNegocio_Descricao',fld:'vTFLINHANEGOCIO_DESCRICAO',pic:'',nv:''},{av:'Ddo_linhanegocio_descricao_Filteredtext_set',ctrl:'DDO_LINHANEGOCIO_DESCRICAO',prop:'FilteredText_set'},{av:'AV41TFLinhaNegocio_Descricao_Sel',fld:'vTFLINHANEGOCIO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_linhanegocio_descricao_Selectedvalue_set',ctrl:'DDO_LINHANEGOCIO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV44TFLinhaNegocio_GpoObjCtrlCod',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_set',ctrl:'DDO_LINHANEGOCIO_GPOOBJCTRLCOD',prop:'FilteredText_set'},{av:'AV45TFLinhaNegocio_GpoObjCtrlCod_To',fld:'vTFLINHANEGOCIO_GPOOBJCTRLCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_set',ctrl:'DDO_LINHANEGOCIO_GPOOBJCTRLCOD',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LinhaNegocio_Identificador1',fld:'vLINHANEGOCIO_IDENTIFICADOR1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavLinhanegocio_identificador1_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21LinhaNegocio_Identificador2',fld:'vLINHANEGOCIO_IDENTIFICADOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25LinhaNegocio_Identificador3',fld:'vLINHANEGOCIO_IDENTIFICADOR3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavLinhanegocio_identificador2_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLinhanegocio_identificador3_Visible',ctrl:'vLINHANEGOCIO_IDENTIFICADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E20RK2',iparms:[{av:'A2036LinhadeNegocio_Codigo',fld:'LINHADENEGOCIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_linhanegocio_identificador_Activeeventkey = "";
         Ddo_linhanegocio_identificador_Filteredtext_get = "";
         Ddo_linhanegocio_identificador_Selectedvalue_get = "";
         Ddo_linhanegocio_descricao_Activeeventkey = "";
         Ddo_linhanegocio_descricao_Filteredtext_get = "";
         Ddo_linhanegocio_descricao_Selectedvalue_get = "";
         Ddo_linhanegocio_gpoobjctrlcod_Activeeventkey = "";
         Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_get = "";
         Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17LinhaNegocio_Identificador1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21LinhaNegocio_Identificador2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25LinhaNegocio_Identificador3 = "";
         AV36TFLinhaNegocio_Identificador = "";
         AV37TFLinhaNegocio_Identificador_Sel = "";
         AV40TFLinhaNegocio_Descricao = "";
         AV41TFLinhaNegocio_Descricao_Sel = "";
         AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace = "";
         AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace = "";
         AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV76Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV47DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV35LinhaNegocio_IdentificadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39LinhaNegocio_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43LinhaNegocio_GpoObjCtrlCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_linhanegocio_identificador_Filteredtext_set = "";
         Ddo_linhanegocio_identificador_Selectedvalue_set = "";
         Ddo_linhanegocio_identificador_Sortedstatus = "";
         Ddo_linhanegocio_descricao_Filteredtext_set = "";
         Ddo_linhanegocio_descricao_Selectedvalue_set = "";
         Ddo_linhanegocio_descricao_Sortedstatus = "";
         Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_set = "";
         Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_set = "";
         Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV51Update = "";
         AV73Update_GXI = "";
         AV52Delete = "";
         AV74Delete_GXI = "";
         AV53Display = "";
         AV75Display_GXI = "";
         A2037LinhaNegocio_Identificador = "";
         A2038LinhaNegocio_Descricao = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 = "";
         lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 = "";
         lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 = "";
         lV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador = "";
         lV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao = "";
         AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1 = "";
         AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 = "";
         AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2 = "";
         AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 = "";
         AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3 = "";
         AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 = "";
         AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel = "";
         AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador = "";
         AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel = "";
         AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao = "";
         H00RK2_A2035LinhaNegocio_GpoObjCtrlCod = new int[1] ;
         H00RK2_A2038LinhaNegocio_Descricao = new String[] {""} ;
         H00RK2_n2038LinhaNegocio_Descricao = new bool[] {false} ;
         H00RK2_A2037LinhaNegocio_Identificador = new String[] {""} ;
         H00RK2_A2036LinhadeNegocio_Codigo = new int[1] ;
         H00RK3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV28Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblLinhanegociotitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwlinhanegocio__default(),
            new Object[][] {
                new Object[] {
               H00RK2_A2035LinhaNegocio_GpoObjCtrlCod, H00RK2_A2038LinhaNegocio_Descricao, H00RK2_n2038LinhaNegocio_Descricao, H00RK2_A2037LinhaNegocio_Identificador, H00RK2_A2036LinhadeNegocio_Codigo
               }
               , new Object[] {
               H00RK3_AGRID_nRecordCount
               }
            }
         );
         AV76Pgmname = "WWLinhaNegocio";
         /* GeneXus formulas. */
         AV76Pgmname = "WWLinhaNegocio";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_88 ;
      private short nGXsfl_88_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_88_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 ;
      private short AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 ;
      private short AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 ;
      private short edtLinhaNegocio_Identificador_Titleformat ;
      private short edtLinhaNegocio_Descricao_Titleformat ;
      private short edtLinhaNegocio_GpoObjCtrlCod_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV44TFLinhaNegocio_GpoObjCtrlCod ;
      private int AV45TFLinhaNegocio_GpoObjCtrlCod_To ;
      private int A2036LinhadeNegocio_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_linhanegocio_identificador_Datalistupdateminimumcharacters ;
      private int Ddo_linhanegocio_descricao_Datalistupdateminimumcharacters ;
      private int edtavTflinhanegocio_identificador_Visible ;
      private int edtavTflinhanegocio_identificador_sel_Visible ;
      private int edtavTflinhanegocio_descricao_Visible ;
      private int edtavTflinhanegocio_descricao_sel_Visible ;
      private int edtavTflinhanegocio_gpoobjctrlcod_Visible ;
      private int edtavTflinhanegocio_gpoobjctrlcod_to_Visible ;
      private int edtavDdo_linhanegocio_identificadortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_linhanegocio_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_linhanegocio_gpoobjctrlcodtitlecontrolidtoreplace_Visible ;
      private int A2035LinhaNegocio_GpoObjCtrlCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod ;
      private int AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV48PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavLinhanegocio_identificador1_Visible ;
      private int edtavLinhanegocio_identificador2_Visible ;
      private int edtavLinhanegocio_identificador3_Visible ;
      private int AV77GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV49GridCurrentPage ;
      private long AV50GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_linhanegocio_identificador_Activeeventkey ;
      private String Ddo_linhanegocio_identificador_Filteredtext_get ;
      private String Ddo_linhanegocio_identificador_Selectedvalue_get ;
      private String Ddo_linhanegocio_descricao_Activeeventkey ;
      private String Ddo_linhanegocio_descricao_Filteredtext_get ;
      private String Ddo_linhanegocio_descricao_Selectedvalue_get ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Activeeventkey ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_get ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_88_idx="0001" ;
      private String AV76Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_linhanegocio_identificador_Caption ;
      private String Ddo_linhanegocio_identificador_Tooltip ;
      private String Ddo_linhanegocio_identificador_Cls ;
      private String Ddo_linhanegocio_identificador_Filteredtext_set ;
      private String Ddo_linhanegocio_identificador_Selectedvalue_set ;
      private String Ddo_linhanegocio_identificador_Dropdownoptionstype ;
      private String Ddo_linhanegocio_identificador_Titlecontrolidtoreplace ;
      private String Ddo_linhanegocio_identificador_Sortedstatus ;
      private String Ddo_linhanegocio_identificador_Filtertype ;
      private String Ddo_linhanegocio_identificador_Datalisttype ;
      private String Ddo_linhanegocio_identificador_Datalistproc ;
      private String Ddo_linhanegocio_identificador_Sortasc ;
      private String Ddo_linhanegocio_identificador_Sortdsc ;
      private String Ddo_linhanegocio_identificador_Loadingdata ;
      private String Ddo_linhanegocio_identificador_Cleanfilter ;
      private String Ddo_linhanegocio_identificador_Noresultsfound ;
      private String Ddo_linhanegocio_identificador_Searchbuttontext ;
      private String Ddo_linhanegocio_descricao_Caption ;
      private String Ddo_linhanegocio_descricao_Tooltip ;
      private String Ddo_linhanegocio_descricao_Cls ;
      private String Ddo_linhanegocio_descricao_Filteredtext_set ;
      private String Ddo_linhanegocio_descricao_Selectedvalue_set ;
      private String Ddo_linhanegocio_descricao_Dropdownoptionstype ;
      private String Ddo_linhanegocio_descricao_Titlecontrolidtoreplace ;
      private String Ddo_linhanegocio_descricao_Sortedstatus ;
      private String Ddo_linhanegocio_descricao_Filtertype ;
      private String Ddo_linhanegocio_descricao_Datalisttype ;
      private String Ddo_linhanegocio_descricao_Datalistproc ;
      private String Ddo_linhanegocio_descricao_Sortasc ;
      private String Ddo_linhanegocio_descricao_Sortdsc ;
      private String Ddo_linhanegocio_descricao_Loadingdata ;
      private String Ddo_linhanegocio_descricao_Cleanfilter ;
      private String Ddo_linhanegocio_descricao_Noresultsfound ;
      private String Ddo_linhanegocio_descricao_Searchbuttontext ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Caption ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Tooltip ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Cls ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Filteredtext_set ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Filteredtextto_set ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Dropdownoptionstype ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Titlecontrolidtoreplace ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Sortedstatus ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Filtertype ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Sortasc ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Sortdsc ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Cleanfilter ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Rangefilterfrom ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Rangefilterto ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTflinhanegocio_identificador_Internalname ;
      private String edtavTflinhanegocio_identificador_Jsonclick ;
      private String edtavTflinhanegocio_identificador_sel_Internalname ;
      private String edtavTflinhanegocio_identificador_sel_Jsonclick ;
      private String edtavTflinhanegocio_descricao_Internalname ;
      private String edtavTflinhanegocio_descricao_sel_Internalname ;
      private String edtavTflinhanegocio_gpoobjctrlcod_Internalname ;
      private String edtavTflinhanegocio_gpoobjctrlcod_Jsonclick ;
      private String edtavTflinhanegocio_gpoobjctrlcod_to_Internalname ;
      private String edtavTflinhanegocio_gpoobjctrlcod_to_Jsonclick ;
      private String edtavDdo_linhanegocio_identificadortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_linhanegocio_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_linhanegocio_gpoobjctrlcodtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtLinhadeNegocio_Codigo_Internalname ;
      private String edtLinhaNegocio_Identificador_Internalname ;
      private String edtLinhaNegocio_Descricao_Internalname ;
      private String edtLinhaNegocio_GpoObjCtrlCod_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavLinhanegocio_identificador1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavLinhanegocio_identificador2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavLinhanegocio_identificador3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_linhanegocio_identificador_Internalname ;
      private String Ddo_linhanegocio_descricao_Internalname ;
      private String Ddo_linhanegocio_gpoobjctrlcod_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtLinhaNegocio_Identificador_Title ;
      private String edtLinhaNegocio_Descricao_Title ;
      private String edtLinhaNegocio_GpoObjCtrlCod_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavLinhanegocio_identificador3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavLinhanegocio_identificador2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavLinhanegocio_identificador1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblLinhanegociotitle_Internalname ;
      private String lblLinhanegociotitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_88_fel_idx="0001" ;
      private String ROClassString ;
      private String edtLinhadeNegocio_Codigo_Jsonclick ;
      private String edtLinhaNegocio_Identificador_Jsonclick ;
      private String edtLinhaNegocio_Descricao_Jsonclick ;
      private String edtLinhaNegocio_GpoObjCtrlCod_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_linhanegocio_identificador_Includesortasc ;
      private bool Ddo_linhanegocio_identificador_Includesortdsc ;
      private bool Ddo_linhanegocio_identificador_Includefilter ;
      private bool Ddo_linhanegocio_identificador_Filterisrange ;
      private bool Ddo_linhanegocio_identificador_Includedatalist ;
      private bool Ddo_linhanegocio_descricao_Includesortasc ;
      private bool Ddo_linhanegocio_descricao_Includesortdsc ;
      private bool Ddo_linhanegocio_descricao_Includefilter ;
      private bool Ddo_linhanegocio_descricao_Filterisrange ;
      private bool Ddo_linhanegocio_descricao_Includedatalist ;
      private bool Ddo_linhanegocio_gpoobjctrlcod_Includesortasc ;
      private bool Ddo_linhanegocio_gpoobjctrlcod_Includesortdsc ;
      private bool Ddo_linhanegocio_gpoobjctrlcod_Includefilter ;
      private bool Ddo_linhanegocio_gpoobjctrlcod_Filterisrange ;
      private bool Ddo_linhanegocio_gpoobjctrlcod_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2038LinhaNegocio_Descricao ;
      private bool AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 ;
      private bool AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV51Update_IsBlob ;
      private bool AV52Delete_IsBlob ;
      private bool AV53Display_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17LinhaNegocio_Identificador1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV21LinhaNegocio_Identificador2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV25LinhaNegocio_Identificador3 ;
      private String AV36TFLinhaNegocio_Identificador ;
      private String AV37TFLinhaNegocio_Identificador_Sel ;
      private String AV40TFLinhaNegocio_Descricao ;
      private String AV41TFLinhaNegocio_Descricao_Sel ;
      private String AV38ddo_LinhaNegocio_IdentificadorTitleControlIdToReplace ;
      private String AV42ddo_LinhaNegocio_DescricaoTitleControlIdToReplace ;
      private String AV46ddo_LinhaNegocio_GpoObjCtrlCodTitleControlIdToReplace ;
      private String AV73Update_GXI ;
      private String AV74Delete_GXI ;
      private String AV75Display_GXI ;
      private String A2037LinhaNegocio_Identificador ;
      private String A2038LinhaNegocio_Descricao ;
      private String lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 ;
      private String lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 ;
      private String lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 ;
      private String lV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador ;
      private String lV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao ;
      private String AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1 ;
      private String AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 ;
      private String AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2 ;
      private String AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 ;
      private String AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3 ;
      private String AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 ;
      private String AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel ;
      private String AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador ;
      private String AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel ;
      private String AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao ;
      private String AV51Update ;
      private String AV52Delete ;
      private String AV53Display ;
      private String imgInsert_Bitmap ;
      private IGxSession AV28Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00RK2_A2035LinhaNegocio_GpoObjCtrlCod ;
      private String[] H00RK2_A2038LinhaNegocio_Descricao ;
      private bool[] H00RK2_n2038LinhaNegocio_Descricao ;
      private String[] H00RK2_A2037LinhaNegocio_Identificador ;
      private int[] H00RK2_A2036LinhadeNegocio_Codigo ;
      private long[] H00RK3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV35LinhaNegocio_IdentificadorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39LinhaNegocio_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43LinhaNegocio_GpoObjCtrlCodTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV47DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwlinhanegocio__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00RK2( IGxContext context ,
                                             String AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1 ,
                                             short AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 ,
                                             String AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 ,
                                             bool AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 ,
                                             String AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2 ,
                                             short AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 ,
                                             String AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 ,
                                             bool AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 ,
                                             String AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3 ,
                                             short AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 ,
                                             String AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 ,
                                             String AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel ,
                                             String AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador ,
                                             String AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel ,
                                             String AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao ,
                                             int AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod ,
                                             int AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to ,
                                             String A2037LinhaNegocio_Identificador ,
                                             String A2038LinhaNegocio_Descricao ,
                                             int A2035LinhaNegocio_GpoObjCtrlCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [17] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [LinhaNegocio_GpoObjCtrlCod], [LinhaNegocio_Descricao], [LinhaNegocio_Identificador], [LinhadeNegocio_Codigo]";
         sFromString = " FROM [LinhaNegocio] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like '%' + @lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like '%' + @lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like '%' + @lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like '%' + @lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like '%' + @lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like '%' + @lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] = @AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] = @AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Descricao] like @lV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Descricao] like @lV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Descricao] = @AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Descricao] = @AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (0==AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_GpoObjCtrlCod] >= @AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_GpoObjCtrlCod] >= @AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_GpoObjCtrlCod] <= @AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_GpoObjCtrlCod] <= @AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [LinhaNegocio_Identificador]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [LinhaNegocio_Identificador] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [LinhaNegocio_Descricao]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [LinhaNegocio_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [LinhaNegocio_GpoObjCtrlCod]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [LinhaNegocio_GpoObjCtrlCod] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [LinhadeNegocio_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00RK3( IGxContext context ,
                                             String AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1 ,
                                             short AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 ,
                                             String AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1 ,
                                             bool AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 ,
                                             String AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2 ,
                                             short AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 ,
                                             String AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2 ,
                                             bool AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 ,
                                             String AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3 ,
                                             short AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 ,
                                             String AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3 ,
                                             String AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel ,
                                             String AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador ,
                                             String AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel ,
                                             String AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao ,
                                             int AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod ,
                                             int AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to ,
                                             String A2037LinhaNegocio_Identificador ,
                                             String A2038LinhaNegocio_Descricao ,
                                             int A2035LinhaNegocio_GpoObjCtrlCod ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [12] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [LinhaNegocio] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV56WWLinhaNegocioDS_1_Dynamicfiltersselector1, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV57WWLinhaNegocioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWLinhaNegocioDS_3_Linhanegocio_identificador1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like '%' + @lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like '%' + @lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV59WWLinhaNegocioDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV60WWLinhaNegocioDS_5_Dynamicfiltersselector2, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV61WWLinhaNegocioDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWLinhaNegocioDS_7_Linhanegocio_identificador2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like '%' + @lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like '%' + @lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV63WWLinhaNegocioDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWLinhaNegocioDS_9_Dynamicfiltersselector3, "LINHANEGOCIO_IDENTIFICADOR") == 0 ) && ( AV65WWLinhaNegocioDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWLinhaNegocioDS_11_Linhanegocio_identificador3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like '%' + @lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like '%' + @lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] like @lV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] like @lV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Identificador] = @AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Identificador] = @AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Descricao] like @lV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Descricao] like @lV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_Descricao] = @AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_Descricao] = @AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (0==AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_GpoObjCtrlCod] >= @AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_GpoObjCtrlCod] >= @AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([LinhaNegocio_GpoObjCtrlCod] <= @AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([LinhaNegocio_GpoObjCtrlCod] <= @AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00RK2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
               case 1 :
                     return conditional_H00RK3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00RK2 ;
          prmH00RK2 = new Object[] {
          new Object[] {"@lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00RK3 ;
          prmH00RK3 = new Object[] {
          new Object[] {"@lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV58WWLinhaNegocioDS_3_Linhanegocio_identificador1",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV62WWLinhaNegocioDS_7_Linhanegocio_identificador2",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV66WWLinhaNegocioDS_11_Linhanegocio_identificador3",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV67WWLinhaNegocioDS_12_Tflinhanegocio_identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV68WWLinhaNegocioDS_13_Tflinhanegocio_identificador_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV69WWLinhaNegocioDS_14_Tflinhanegocio_descricao",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV70WWLinhaNegocioDS_15_Tflinhanegocio_descricao_sel",SqlDbType.VarChar,250,0} ,
          new Object[] {"@AV71WWLinhaNegocioDS_16_Tflinhanegocio_gpoobjctrlcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV72WWLinhaNegocioDS_17_Tflinhanegocio_gpoobjctrlcod_to",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00RK2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RK2,11,0,true,false )
             ,new CursorDef("H00RK3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RK3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
       }
    }

 }

}
