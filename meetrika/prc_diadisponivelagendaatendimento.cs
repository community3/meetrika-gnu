/*
               File: PRC_DiaDisponivelAgendaAtendimento
        Description: Dia Disponivel Agenda Atendimento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:55.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_diadisponivelagendaatendimento : GXProcedure
   {
      public prc_diadisponivelagendaatendimento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_diadisponivelagendaatendimento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AgendaAtendimento_CntSrcCod ,
                           DateTime aP1_DiaInicio ,
                           decimal aP2_Unidades ,
                           out DateTime aP3_DiaDisponivel )
      {
         this.A1183AgendaAtendimento_CntSrcCod = aP0_AgendaAtendimento_CntSrcCod;
         this.AV9DiaInicio = aP1_DiaInicio;
         this.AV8Unidades = aP2_Unidades;
         this.AV10DiaDisponivel = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP0_AgendaAtendimento_CntSrcCod=this.A1183AgendaAtendimento_CntSrcCod;
         aP3_DiaDisponivel=this.AV10DiaDisponivel;
      }

      public DateTime executeUdp( ref int aP0_AgendaAtendimento_CntSrcCod ,
                                  DateTime aP1_DiaInicio ,
                                  decimal aP2_Unidades )
      {
         this.A1183AgendaAtendimento_CntSrcCod = aP0_AgendaAtendimento_CntSrcCod;
         this.AV9DiaInicio = aP1_DiaInicio;
         this.AV8Unidades = aP2_Unidades;
         this.AV10DiaDisponivel = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP0_AgendaAtendimento_CntSrcCod=this.A1183AgendaAtendimento_CntSrcCod;
         aP3_DiaDisponivel=this.AV10DiaDisponivel;
         return AV10DiaDisponivel ;
      }

      public void executeSubmit( ref int aP0_AgendaAtendimento_CntSrcCod ,
                                 DateTime aP1_DiaInicio ,
                                 decimal aP2_Unidades ,
                                 out DateTime aP3_DiaDisponivel )
      {
         prc_diadisponivelagendaatendimento objprc_diadisponivelagendaatendimento;
         objprc_diadisponivelagendaatendimento = new prc_diadisponivelagendaatendimento();
         objprc_diadisponivelagendaatendimento.A1183AgendaAtendimento_CntSrcCod = aP0_AgendaAtendimento_CntSrcCod;
         objprc_diadisponivelagendaatendimento.AV9DiaInicio = aP1_DiaInicio;
         objprc_diadisponivelagendaatendimento.AV8Unidades = aP2_Unidades;
         objprc_diadisponivelagendaatendimento.AV10DiaDisponivel = DateTime.MinValue ;
         objprc_diadisponivelagendaatendimento.context.SetSubmitInitialConfig(context);
         objprc_diadisponivelagendaatendimento.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_diadisponivelagendaatendimento);
         aP0_AgendaAtendimento_CntSrcCod=this.A1183AgendaAtendimento_CntSrcCod;
         aP3_DiaDisponivel=this.AV10DiaDisponivel;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_diadisponivelagendaatendimento)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         GXt_dtime1 = AV10DiaDisponivel;
         new prc_adddiasuteis(context ).execute(  AV9DiaInicio,  1,  "C", out  GXt_dtime1) ;
         AV10DiaDisponivel = GXt_dtime1;
         AV11AgendaAtendimento_Data = DateTimeUtil.ResetTime(AV10DiaDisponivel);
         AV20QtdUnd = AV8Unidades;
         GXt_decimal2 = AV12ProdutividadeDia;
         new prc_getprodutividadedia(context ).execute( ref  A1183AgendaAtendimento_CntSrcCod, out  GXt_decimal2) ;
         AV12ProdutividadeDia = GXt_decimal2;
         if ( (Convert.ToDecimal(0)==AV12ProdutividadeDia) )
         {
            this.cleanup();
            if (true) return;
         }
         /* Using cursor P007O2 */
         pr_default.execute(0, new Object[] {A1183AgendaAtendimento_CntSrcCod, AV11AgendaAtendimento_Data});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1184AgendaAtendimento_Data = P007O2_A1184AgendaAtendimento_Data[0];
            A1209AgendaAtendimento_CodDmn = P007O2_A1209AgendaAtendimento_CodDmn[0];
            AV26PrimeiraDataAchada = A1184AgendaAtendimento_Data;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         while ( AV26PrimeiraDataAchada > AV11AgendaAtendimento_Data )
         {
            if ( AV20QtdUnd <= AV12ProdutividadeDia )
            {
               this.cleanup();
               if (true) return;
            }
            else
            {
               AV20QtdUnd = (decimal)(AV20QtdUnd-AV12ProdutividadeDia);
               GXt_dtime1 = AV10DiaDisponivel;
               new prc_adddiasuteis(context ).execute(  AV10DiaDisponivel,  1,  "C", out  GXt_dtime1) ;
               AV10DiaDisponivel = GXt_dtime1;
               AV11AgendaAtendimento_Data = DateTimeUtil.ResetTime(AV10DiaDisponivel);
            }
         }
         if ( ( AV20QtdUnd > Convert.ToDecimal( 0 )) )
         {
            /* Using cursor P007O4 */
            pr_default.execute(1, new Object[] {A1183AgendaAtendimento_CntSrcCod, AV11AgendaAtendimento_Data});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1184AgendaAtendimento_Data = P007O4_A1184AgendaAtendimento_Data[0];
               A40000GXC1 = P007O4_A40000GXC1[0];
               n40000GXC1 = P007O4_n40000GXC1[0];
               A40000GXC1 = P007O4_A40000GXC1[0];
               n40000GXC1 = P007O4_n40000GXC1[0];
               AV10DiaDisponivel = DateTimeUtil.ResetTime( A1184AgendaAtendimento_Data ) ;
               AV23TotalUndDoDia = A40000GXC1;
               AV25Found = true;
               if ( AV23TotalUndDoDia < AV12ProdutividadeDia )
               {
                  if ( AV20QtdUnd <= AV12ProdutividadeDia - AV23TotalUndDoDia )
                  {
                     AV20QtdUnd = 0;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  else
                  {
                     AV20QtdUnd = (decimal)(AV20QtdUnd-(AV12ProdutividadeDia-AV23TotalUndDoDia));
                  }
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
         }
         if ( ( AV20QtdUnd > Convert.ToDecimal( 0 )) )
         {
            AV24d = (decimal)(AV20QtdUnd/ (decimal)(AV12ProdutividadeDia));
            if ( (Convert.ToDecimal( NumberUtil.Int( (long)(AV24d)) ) != AV24d ) )
            {
               AV24d = (decimal)(NumberUtil.Int( (long)(AV24d))+1);
            }
            if ( ( AV20QtdUnd == AV8Unidades ) && ! AV25Found )
            {
               AV24d = (decimal)(AV24d-1);
            }
            if ( ( AV24d > Convert.ToDecimal( 0 )) )
            {
               /* Using cursor P007O5 */
               pr_default.execute(2, new Object[] {A1183AgendaAtendimento_CntSrcCod});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A160ContratoServicos_Codigo = P007O5_A160ContratoServicos_Codigo[0];
                  A1454ContratoServicos_PrazoTpDias = P007O5_A1454ContratoServicos_PrazoTpDias[0];
                  n1454ContratoServicos_PrazoTpDias = P007O5_n1454ContratoServicos_PrazoTpDias[0];
                  AV27TipoDias = A1454ContratoServicos_PrazoTpDias;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               GXt_dtime1 = AV10DiaDisponivel;
               new prc_adddiasuteis(context ).execute(  AV10DiaDisponivel,  (short)(AV24d),  AV27TipoDias, out  GXt_dtime1) ;
               AV10DiaDisponivel = GXt_dtime1;
            }
         }
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV11AgendaAtendimento_Data = DateTime.MinValue;
         scmdbuf = "";
         P007O2_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         P007O2_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         P007O2_A1209AgendaAtendimento_CodDmn = new int[1] ;
         A1184AgendaAtendimento_Data = DateTime.MinValue;
         AV26PrimeiraDataAchada = DateTime.MinValue;
         P007O4_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         P007O4_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         P007O4_A40000GXC1 = new decimal[1] ;
         P007O4_n40000GXC1 = new bool[] {false} ;
         P007O5_A160ContratoServicos_Codigo = new int[1] ;
         P007O5_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P007O5_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         A1454ContratoServicos_PrazoTpDias = "";
         AV27TipoDias = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_diadisponivelagendaatendimento__default(),
            new Object[][] {
                new Object[] {
               P007O2_A1183AgendaAtendimento_CntSrcCod, P007O2_A1184AgendaAtendimento_Data, P007O2_A1209AgendaAtendimento_CodDmn
               }
               , new Object[] {
               P007O4_A1183AgendaAtendimento_CntSrcCod, P007O4_A1184AgendaAtendimento_Data, P007O4_A40000GXC1, P007O4_n40000GXC1
               }
               , new Object[] {
               P007O5_A160ContratoServicos_Codigo, P007O5_A1454ContratoServicos_PrazoTpDias, P007O5_n1454ContratoServicos_PrazoTpDias
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1183AgendaAtendimento_CntSrcCod ;
      private int A1209AgendaAtendimento_CodDmn ;
      private int A160ContratoServicos_Codigo ;
      private decimal AV8Unidades ;
      private decimal AV20QtdUnd ;
      private decimal AV12ProdutividadeDia ;
      private decimal GXt_decimal2 ;
      private decimal A40000GXC1 ;
      private decimal AV23TotalUndDoDia ;
      private decimal AV24d ;
      private String scmdbuf ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String AV27TipoDias ;
      private DateTime AV9DiaInicio ;
      private DateTime AV10DiaDisponivel ;
      private DateTime GXt_dtime1 ;
      private DateTime AV11AgendaAtendimento_Data ;
      private DateTime A1184AgendaAtendimento_Data ;
      private DateTime AV26PrimeiraDataAchada ;
      private bool n40000GXC1 ;
      private bool AV25Found ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AgendaAtendimento_CntSrcCod ;
      private IDataStoreProvider pr_default ;
      private int[] P007O2_A1183AgendaAtendimento_CntSrcCod ;
      private DateTime[] P007O2_A1184AgendaAtendimento_Data ;
      private int[] P007O2_A1209AgendaAtendimento_CodDmn ;
      private int[] P007O4_A1183AgendaAtendimento_CntSrcCod ;
      private DateTime[] P007O4_A1184AgendaAtendimento_Data ;
      private decimal[] P007O4_A40000GXC1 ;
      private bool[] P007O4_n40000GXC1 ;
      private int[] P007O5_A160ContratoServicos_Codigo ;
      private String[] P007O5_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P007O5_n1454ContratoServicos_PrazoTpDias ;
      private DateTime aP3_DiaDisponivel ;
   }

   public class prc_diadisponivelagendaatendimento__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP007O2 ;
          prmP007O2 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11AgendaAtendimento_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP007O4 ;
          prmP007O4 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11AgendaAtendimento_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP007O5 ;
          prmP007O5 = new Object[] {
          new Object[] {"@AgendaAtendimento_CntSrcCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P007O2", "SELECT TOP 1 [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data], [AgendaAtendimento_CodDmn] FROM [AgendaAtendimento] WITH (NOLOCK) WHERE [AgendaAtendimento_CntSrcCod] = @AgendaAtendimento_CntSrcCod and [AgendaAtendimento_Data] >= @AV11AgendaAtendimento_Data ORDER BY [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007O2,1,0,false,true )
             ,new CursorDef("P007O4", "SELECT DISTINCT [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data], [GXC1] FROM ( SELECT TOP(100) PERCENT T1.[AgendaAtendimento_CntSrcCod], T1.[AgendaAtendimento_Data], COALESCE( T2.[GXC1], 0) AS GXC1 FROM ([AgendaAtendimento] T1 WITH (NOLOCK) LEFT JOIN (SELECT SUM([AgendaAtendimento_QtdUnd]) AS GXC1, [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data] FROM [AgendaAtendimento] WITH (NOLOCK) GROUP BY [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data] ) T2 ON T2.[AgendaAtendimento_CntSrcCod] = T1.[AgendaAtendimento_CntSrcCod] AND T2.[AgendaAtendimento_Data] = T1.[AgendaAtendimento_Data]) WHERE T1.[AgendaAtendimento_CntSrcCod] = @AgendaAtendimento_CntSrcCod and T1.[AgendaAtendimento_Data] >= @AV11AgendaAtendimento_Data ORDER BY T1.[AgendaAtendimento_CntSrcCod]) DistinctT ORDER BY [AgendaAtendimento_CntSrcCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007O4,100,0,false,false )
             ,new CursorDef("P007O5", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicos_PrazoTpDias] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AgendaAtendimento_CntSrcCod ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007O5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
