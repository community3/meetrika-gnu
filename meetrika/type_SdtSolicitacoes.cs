/*
               File: type_SdtSolicitacoes
        Description: Solicitacoes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:21:5.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Solicitacoes" )]
   [XmlType(TypeName =  "Solicitacoes" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSolicitacoes : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSolicitacoes( )
      {
         /* Constructor for serialization */
         gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto = "";
         gxTv_SdtSolicitacoes_Solicitacoes_objetivo = "";
         gxTv_SdtSolicitacoes_Solicitacoes_data = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacoes_Solicitacoes_data_ult = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacoes_Solicitacoes_status = "";
         gxTv_SdtSolicitacoes_Mode = "";
         gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto_Z = "";
         gxTv_SdtSolicitacoes_Solicitacoes_data_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacoes_Solicitacoes_status_Z = "";
      }

      public SdtSolicitacoes( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV439Solicitacoes_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV439Solicitacoes_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Solicitacoes_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Solicitacoes");
         metadata.Set("BT", "Solicitacoes");
         metadata.Set("PK", "[ \"Solicitacoes_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Solicitacoes_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Contratada_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Servico_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Sistema_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"Solicitacoes_Usuario-Usuario_Codigo\" ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"Solicitacoes_Usuario_Ult-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicitacoes_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicitacoes_novo_projeto_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicitacoes_usuario_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicitacoes_data_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicitacoes_usuario_ult_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicitacoes_data_ult_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicitacoes_status_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Solicitacoes_objetivo_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSolicitacoes deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSolicitacoes)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSolicitacoes obj ;
         obj = this;
         obj.gxTpr_Solicitacoes_codigo = deserialized.gxTpr_Solicitacoes_codigo;
         obj.gxTpr_Contratada_codigo = deserialized.gxTpr_Contratada_codigo;
         obj.gxTpr_Sistema_codigo = deserialized.gxTpr_Sistema_codigo;
         obj.gxTpr_Servico_codigo = deserialized.gxTpr_Servico_codigo;
         obj.gxTpr_Solicitacoes_novo_projeto = deserialized.gxTpr_Solicitacoes_novo_projeto;
         obj.gxTpr_Solicitacoes_objetivo = deserialized.gxTpr_Solicitacoes_objetivo;
         obj.gxTpr_Solicitacoes_usuario = deserialized.gxTpr_Solicitacoes_usuario;
         obj.gxTpr_Solicitacoes_data = deserialized.gxTpr_Solicitacoes_data;
         obj.gxTpr_Solicitacoes_usuario_ult = deserialized.gxTpr_Solicitacoes_usuario_ult;
         obj.gxTpr_Solicitacoes_data_ult = deserialized.gxTpr_Solicitacoes_data_ult;
         obj.gxTpr_Solicitacoes_status = deserialized.gxTpr_Solicitacoes_status;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Solicitacoes_codigo_Z = deserialized.gxTpr_Solicitacoes_codigo_Z;
         obj.gxTpr_Contratada_codigo_Z = deserialized.gxTpr_Contratada_codigo_Z;
         obj.gxTpr_Sistema_codigo_Z = deserialized.gxTpr_Sistema_codigo_Z;
         obj.gxTpr_Servico_codigo_Z = deserialized.gxTpr_Servico_codigo_Z;
         obj.gxTpr_Solicitacoes_novo_projeto_Z = deserialized.gxTpr_Solicitacoes_novo_projeto_Z;
         obj.gxTpr_Solicitacoes_usuario_Z = deserialized.gxTpr_Solicitacoes_usuario_Z;
         obj.gxTpr_Solicitacoes_data_Z = deserialized.gxTpr_Solicitacoes_data_Z;
         obj.gxTpr_Solicitacoes_usuario_ult_Z = deserialized.gxTpr_Solicitacoes_usuario_ult_Z;
         obj.gxTpr_Solicitacoes_data_ult_Z = deserialized.gxTpr_Solicitacoes_data_ult_Z;
         obj.gxTpr_Solicitacoes_status_Z = deserialized.gxTpr_Solicitacoes_status_Z;
         obj.gxTpr_Solicitacoes_objetivo_N = deserialized.gxTpr_Solicitacoes_objetivo_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Codigo") )
               {
                  gxTv_SdtSolicitacoes_Solicitacoes_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Codigo") )
               {
                  gxTv_SdtSolicitacoes_Contratada_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Codigo") )
               {
                  gxTv_SdtSolicitacoes_Sistema_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Codigo") )
               {
                  gxTv_SdtSolicitacoes_Servico_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Novo_Projeto") )
               {
                  gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Objetivo") )
               {
                  gxTv_SdtSolicitacoes_Solicitacoes_objetivo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Usuario") )
               {
                  gxTv_SdtSolicitacoes_Solicitacoes_usuario = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Data") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacoes_Solicitacoes_data = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSolicitacoes_Solicitacoes_data = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Usuario_Ult") )
               {
                  gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Data_Ult") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacoes_Solicitacoes_data_ult = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSolicitacoes_Solicitacoes_data_ult = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Status") )
               {
                  gxTv_SdtSolicitacoes_Solicitacoes_status = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtSolicitacoes_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtSolicitacoes_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Codigo_Z") )
               {
                  gxTv_SdtSolicitacoes_Solicitacoes_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Codigo_Z") )
               {
                  gxTv_SdtSolicitacoes_Contratada_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Codigo_Z") )
               {
                  gxTv_SdtSolicitacoes_Sistema_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Codigo_Z") )
               {
                  gxTv_SdtSolicitacoes_Servico_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Novo_Projeto_Z") )
               {
                  gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Usuario_Z") )
               {
                  gxTv_SdtSolicitacoes_Solicitacoes_usuario_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Data_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacoes_Solicitacoes_data_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSolicitacoes_Solicitacoes_data_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Usuario_Ult_Z") )
               {
                  gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Data_Ult_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Status_Z") )
               {
                  gxTv_SdtSolicitacoes_Solicitacoes_status_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Solicitacoes_Objetivo_N") )
               {
                  gxTv_SdtSolicitacoes_Solicitacoes_objetivo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Solicitacoes";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Solicitacoes_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Solicitacoes_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Contratada_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Sistema_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Sistema_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Servico_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Servico_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Solicitacoes_Novo_Projeto", StringUtil.RTrim( gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Solicitacoes_Objetivo", StringUtil.RTrim( gxTv_SdtSolicitacoes_Solicitacoes_objetivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Solicitacoes_Usuario", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Solicitacoes_usuario), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSolicitacoes_Solicitacoes_data) )
         {
            oWriter.WriteStartElement("Solicitacoes_Data");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacoes_Solicitacoes_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacoes_Solicitacoes_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacoes_Solicitacoes_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSolicitacoes_Solicitacoes_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSolicitacoes_Solicitacoes_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSolicitacoes_Solicitacoes_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Solicitacoes_Data", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("Solicitacoes_Usuario_Ult", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSolicitacoes_Solicitacoes_data_ult) )
         {
            oWriter.WriteStartElement("Solicitacoes_Data_Ult");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacoes_Solicitacoes_data_ult)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacoes_Solicitacoes_data_ult)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacoes_Solicitacoes_data_ult)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSolicitacoes_Solicitacoes_data_ult)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSolicitacoes_Solicitacoes_data_ult)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSolicitacoes_Solicitacoes_data_ult)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Solicitacoes_Data_Ult", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("Solicitacoes_Status", StringUtil.RTrim( gxTv_SdtSolicitacoes_Solicitacoes_status));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtSolicitacoes_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Solicitacoes_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Solicitacoes_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Contratada_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Sistema_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Sistema_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Servico_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Servico_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Solicitacoes_Novo_Projeto_Z", StringUtil.RTrim( gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Solicitacoes_Usuario_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Solicitacoes_usuario_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtSolicitacoes_Solicitacoes_data_Z) )
            {
               oWriter.WriteStartElement("Solicitacoes_Data_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacoes_Solicitacoes_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacoes_Solicitacoes_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacoes_Solicitacoes_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSolicitacoes_Solicitacoes_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSolicitacoes_Solicitacoes_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSolicitacoes_Solicitacoes_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Solicitacoes_Data_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("Solicitacoes_Usuario_Ult_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z) )
            {
               oWriter.WriteStartElement("Solicitacoes_Data_Ult_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Solicitacoes_Data_Ult_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("Solicitacoes_Status_Z", StringUtil.RTrim( gxTv_SdtSolicitacoes_Solicitacoes_status_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Solicitacoes_Objetivo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacoes_Solicitacoes_objetivo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Solicitacoes_Codigo", gxTv_SdtSolicitacoes_Solicitacoes_codigo, false);
         AddObjectProperty("Contratada_Codigo", gxTv_SdtSolicitacoes_Contratada_codigo, false);
         AddObjectProperty("Sistema_Codigo", gxTv_SdtSolicitacoes_Sistema_codigo, false);
         AddObjectProperty("Servico_Codigo", gxTv_SdtSolicitacoes_Servico_codigo, false);
         AddObjectProperty("Solicitacoes_Novo_Projeto", gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto, false);
         AddObjectProperty("Solicitacoes_Objetivo", gxTv_SdtSolicitacoes_Solicitacoes_objetivo, false);
         AddObjectProperty("Solicitacoes_Usuario", gxTv_SdtSolicitacoes_Solicitacoes_usuario, false);
         datetime_STZ = gxTv_SdtSolicitacoes_Solicitacoes_data;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Solicitacoes_Data", sDateCnv, false);
         AddObjectProperty("Solicitacoes_Usuario_Ult", gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult, false);
         datetime_STZ = gxTv_SdtSolicitacoes_Solicitacoes_data_ult;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Solicitacoes_Data_Ult", sDateCnv, false);
         AddObjectProperty("Solicitacoes_Status", gxTv_SdtSolicitacoes_Solicitacoes_status, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtSolicitacoes_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtSolicitacoes_Initialized, false);
            AddObjectProperty("Solicitacoes_Codigo_Z", gxTv_SdtSolicitacoes_Solicitacoes_codigo_Z, false);
            AddObjectProperty("Contratada_Codigo_Z", gxTv_SdtSolicitacoes_Contratada_codigo_Z, false);
            AddObjectProperty("Sistema_Codigo_Z", gxTv_SdtSolicitacoes_Sistema_codigo_Z, false);
            AddObjectProperty("Servico_Codigo_Z", gxTv_SdtSolicitacoes_Servico_codigo_Z, false);
            AddObjectProperty("Solicitacoes_Novo_Projeto_Z", gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto_Z, false);
            AddObjectProperty("Solicitacoes_Usuario_Z", gxTv_SdtSolicitacoes_Solicitacoes_usuario_Z, false);
            datetime_STZ = gxTv_SdtSolicitacoes_Solicitacoes_data_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Solicitacoes_Data_Z", sDateCnv, false);
            AddObjectProperty("Solicitacoes_Usuario_Ult_Z", gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult_Z, false);
            datetime_STZ = gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Solicitacoes_Data_Ult_Z", sDateCnv, false);
            AddObjectProperty("Solicitacoes_Status_Z", gxTv_SdtSolicitacoes_Solicitacoes_status_Z, false);
            AddObjectProperty("Solicitacoes_Objetivo_N", gxTv_SdtSolicitacoes_Solicitacoes_objetivo_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Solicitacoes_Codigo" )]
      [  XmlElement( ElementName = "Solicitacoes_Codigo"   )]
      public int gxTpr_Solicitacoes_codigo
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_codigo ;
         }

         set {
            if ( gxTv_SdtSolicitacoes_Solicitacoes_codigo != value )
            {
               gxTv_SdtSolicitacoes_Mode = "INS";
               this.gxTv_SdtSolicitacoes_Solicitacoes_codigo_Z_SetNull( );
               this.gxTv_SdtSolicitacoes_Contratada_codigo_Z_SetNull( );
               this.gxTv_SdtSolicitacoes_Sistema_codigo_Z_SetNull( );
               this.gxTv_SdtSolicitacoes_Servico_codigo_Z_SetNull( );
               this.gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto_Z_SetNull( );
               this.gxTv_SdtSolicitacoes_Solicitacoes_usuario_Z_SetNull( );
               this.gxTv_SdtSolicitacoes_Solicitacoes_data_Z_SetNull( );
               this.gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult_Z_SetNull( );
               this.gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z_SetNull( );
               this.gxTv_SdtSolicitacoes_Solicitacoes_status_Z_SetNull( );
            }
            gxTv_SdtSolicitacoes_Solicitacoes_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_Codigo" )]
      [  XmlElement( ElementName = "Contratada_Codigo"   )]
      public int gxTpr_Contratada_codigo
      {
         get {
            return gxTv_SdtSolicitacoes_Contratada_codigo ;
         }

         set {
            gxTv_SdtSolicitacoes_Contratada_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Sistema_Codigo" )]
      [  XmlElement( ElementName = "Sistema_Codigo"   )]
      public int gxTpr_Sistema_codigo
      {
         get {
            return gxTv_SdtSolicitacoes_Sistema_codigo ;
         }

         set {
            gxTv_SdtSolicitacoes_Sistema_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Servico_Codigo" )]
      [  XmlElement( ElementName = "Servico_Codigo"   )]
      public int gxTpr_Servico_codigo
      {
         get {
            return gxTv_SdtSolicitacoes_Servico_codigo ;
         }

         set {
            gxTv_SdtSolicitacoes_Servico_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Solicitacoes_Novo_Projeto" )]
      [  XmlElement( ElementName = "Solicitacoes_Novo_Projeto"   )]
      public String gxTpr_Solicitacoes_novo_projeto
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Solicitacoes_Objetivo" )]
      [  XmlElement( ElementName = "Solicitacoes_Objetivo"   )]
      public String gxTpr_Solicitacoes_objetivo
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_objetivo ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_objetivo_N = 0;
            gxTv_SdtSolicitacoes_Solicitacoes_objetivo = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Solicitacoes_objetivo_SetNull( )
      {
         gxTv_SdtSolicitacoes_Solicitacoes_objetivo_N = 1;
         gxTv_SdtSolicitacoes_Solicitacoes_objetivo = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Solicitacoes_objetivo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Solicitacoes_Usuario" )]
      [  XmlElement( ElementName = "Solicitacoes_Usuario"   )]
      public int gxTpr_Solicitacoes_usuario
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_usuario ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_usuario = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Solicitacoes_Data" )]
      [  XmlElement( ElementName = "Solicitacoes_Data"  , IsNullable=true )]
      public string gxTpr_Solicitacoes_data_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacoes_Solicitacoes_data == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSolicitacoes_Solicitacoes_data).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSolicitacoes_Solicitacoes_data = DateTime.MinValue;
            else
               gxTv_SdtSolicitacoes_Solicitacoes_data = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Solicitacoes_data
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_data ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_data = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Solicitacoes_Usuario_Ult" )]
      [  XmlElement( ElementName = "Solicitacoes_Usuario_Ult"   )]
      public int gxTpr_Solicitacoes_usuario_ult
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Solicitacoes_Data_Ult" )]
      [  XmlElement( ElementName = "Solicitacoes_Data_Ult"  , IsNullable=true )]
      public string gxTpr_Solicitacoes_data_ult_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacoes_Solicitacoes_data_ult == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSolicitacoes_Solicitacoes_data_ult).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSolicitacoes_Solicitacoes_data_ult = DateTime.MinValue;
            else
               gxTv_SdtSolicitacoes_Solicitacoes_data_ult = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Solicitacoes_data_ult
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_data_ult ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_data_ult = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Solicitacoes_Status" )]
      [  XmlElement( ElementName = "Solicitacoes_Status"   )]
      public String gxTpr_Solicitacoes_status
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_status ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_status = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtSolicitacoes_Mode ;
         }

         set {
            gxTv_SdtSolicitacoes_Mode = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Mode_SetNull( )
      {
         gxTv_SdtSolicitacoes_Mode = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtSolicitacoes_Initialized ;
         }

         set {
            gxTv_SdtSolicitacoes_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Initialized_SetNull( )
      {
         gxTv_SdtSolicitacoes_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Solicitacoes_Codigo_Z" )]
      [  XmlElement( ElementName = "Solicitacoes_Codigo_Z"   )]
      public int gxTpr_Solicitacoes_codigo_Z
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_codigo_Z ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Solicitacoes_codigo_Z_SetNull( )
      {
         gxTv_SdtSolicitacoes_Solicitacoes_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Solicitacoes_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_Codigo_Z" )]
      [  XmlElement( ElementName = "Contratada_Codigo_Z"   )]
      public int gxTpr_Contratada_codigo_Z
      {
         get {
            return gxTv_SdtSolicitacoes_Contratada_codigo_Z ;
         }

         set {
            gxTv_SdtSolicitacoes_Contratada_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Contratada_codigo_Z_SetNull( )
      {
         gxTv_SdtSolicitacoes_Contratada_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Contratada_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Codigo_Z" )]
      [  XmlElement( ElementName = "Sistema_Codigo_Z"   )]
      public int gxTpr_Sistema_codigo_Z
      {
         get {
            return gxTv_SdtSolicitacoes_Sistema_codigo_Z ;
         }

         set {
            gxTv_SdtSolicitacoes_Sistema_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Sistema_codigo_Z_SetNull( )
      {
         gxTv_SdtSolicitacoes_Sistema_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Sistema_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Codigo_Z" )]
      [  XmlElement( ElementName = "Servico_Codigo_Z"   )]
      public int gxTpr_Servico_codigo_Z
      {
         get {
            return gxTv_SdtSolicitacoes_Servico_codigo_Z ;
         }

         set {
            gxTv_SdtSolicitacoes_Servico_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Servico_codigo_Z_SetNull( )
      {
         gxTv_SdtSolicitacoes_Servico_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Servico_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Solicitacoes_Novo_Projeto_Z" )]
      [  XmlElement( ElementName = "Solicitacoes_Novo_Projeto_Z"   )]
      public String gxTpr_Solicitacoes_novo_projeto_Z
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto_Z ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto_Z = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto_Z_SetNull( )
      {
         gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto_Z = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Solicitacoes_Usuario_Z" )]
      [  XmlElement( ElementName = "Solicitacoes_Usuario_Z"   )]
      public int gxTpr_Solicitacoes_usuario_Z
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_usuario_Z ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_usuario_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Solicitacoes_usuario_Z_SetNull( )
      {
         gxTv_SdtSolicitacoes_Solicitacoes_usuario_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Solicitacoes_usuario_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Solicitacoes_Data_Z" )]
      [  XmlElement( ElementName = "Solicitacoes_Data_Z"  , IsNullable=true )]
      public string gxTpr_Solicitacoes_data_Z_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacoes_Solicitacoes_data_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSolicitacoes_Solicitacoes_data_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSolicitacoes_Solicitacoes_data_Z = DateTime.MinValue;
            else
               gxTv_SdtSolicitacoes_Solicitacoes_data_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Solicitacoes_data_Z
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_data_Z ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_data_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Solicitacoes_data_Z_SetNull( )
      {
         gxTv_SdtSolicitacoes_Solicitacoes_data_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Solicitacoes_data_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Solicitacoes_Usuario_Ult_Z" )]
      [  XmlElement( ElementName = "Solicitacoes_Usuario_Ult_Z"   )]
      public int gxTpr_Solicitacoes_usuario_ult_Z
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult_Z ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult_Z_SetNull( )
      {
         gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Solicitacoes_Data_Ult_Z" )]
      [  XmlElement( ElementName = "Solicitacoes_Data_Ult_Z"  , IsNullable=true )]
      public string gxTpr_Solicitacoes_data_ult_Z_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z = DateTime.MinValue;
            else
               gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Solicitacoes_data_ult_Z
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z_SetNull( )
      {
         gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Solicitacoes_Status_Z" )]
      [  XmlElement( ElementName = "Solicitacoes_Status_Z"   )]
      public String gxTpr_Solicitacoes_status_Z
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_status_Z ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_status_Z = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Solicitacoes_status_Z_SetNull( )
      {
         gxTv_SdtSolicitacoes_Solicitacoes_status_Z = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Solicitacoes_status_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Solicitacoes_Objetivo_N" )]
      [  XmlElement( ElementName = "Solicitacoes_Objetivo_N"   )]
      public short gxTpr_Solicitacoes_objetivo_N
      {
         get {
            return gxTv_SdtSolicitacoes_Solicitacoes_objetivo_N ;
         }

         set {
            gxTv_SdtSolicitacoes_Solicitacoes_objetivo_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacoes_Solicitacoes_objetivo_N_SetNull( )
      {
         gxTv_SdtSolicitacoes_Solicitacoes_objetivo_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacoes_Solicitacoes_objetivo_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto = "";
         gxTv_SdtSolicitacoes_Solicitacoes_objetivo = "";
         gxTv_SdtSolicitacoes_Solicitacoes_data = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacoes_Solicitacoes_data_ult = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacoes_Solicitacoes_status = "";
         gxTv_SdtSolicitacoes_Mode = "";
         gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto_Z = "";
         gxTv_SdtSolicitacoes_Solicitacoes_data_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacoes_Solicitacoes_status_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "solicitacoes", "GeneXus.Programs.solicitacoes_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtSolicitacoes_Initialized ;
      private short gxTv_SdtSolicitacoes_Solicitacoes_objetivo_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtSolicitacoes_Solicitacoes_codigo ;
      private int gxTv_SdtSolicitacoes_Contratada_codigo ;
      private int gxTv_SdtSolicitacoes_Sistema_codigo ;
      private int gxTv_SdtSolicitacoes_Servico_codigo ;
      private int gxTv_SdtSolicitacoes_Solicitacoes_usuario ;
      private int gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult ;
      private int gxTv_SdtSolicitacoes_Solicitacoes_codigo_Z ;
      private int gxTv_SdtSolicitacoes_Contratada_codigo_Z ;
      private int gxTv_SdtSolicitacoes_Sistema_codigo_Z ;
      private int gxTv_SdtSolicitacoes_Servico_codigo_Z ;
      private int gxTv_SdtSolicitacoes_Solicitacoes_usuario_Z ;
      private int gxTv_SdtSolicitacoes_Solicitacoes_usuario_ult_Z ;
      private String gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto ;
      private String gxTv_SdtSolicitacoes_Solicitacoes_status ;
      private String gxTv_SdtSolicitacoes_Mode ;
      private String gxTv_SdtSolicitacoes_Solicitacoes_novo_projeto_Z ;
      private String gxTv_SdtSolicitacoes_Solicitacoes_status_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtSolicitacoes_Solicitacoes_data ;
      private DateTime gxTv_SdtSolicitacoes_Solicitacoes_data_ult ;
      private DateTime gxTv_SdtSolicitacoes_Solicitacoes_data_Z ;
      private DateTime gxTv_SdtSolicitacoes_Solicitacoes_data_ult_Z ;
      private DateTime datetime_STZ ;
      private String gxTv_SdtSolicitacoes_Solicitacoes_objetivo ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Solicitacoes", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSolicitacoes_RESTInterface : GxGenericCollectionItem<SdtSolicitacoes>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSolicitacoes_RESTInterface( ) : base()
      {
      }

      public SdtSolicitacoes_RESTInterface( SdtSolicitacoes psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Solicitacoes_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Solicitacoes_codigo
      {
         get {
            return sdt.gxTpr_Solicitacoes_codigo ;
         }

         set {
            sdt.gxTpr_Solicitacoes_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_Codigo" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratada_codigo
      {
         get {
            return sdt.gxTpr_Contratada_codigo ;
         }

         set {
            sdt.gxTpr_Contratada_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sistema_Codigo" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Sistema_codigo
      {
         get {
            return sdt.gxTpr_Sistema_codigo ;
         }

         set {
            sdt.gxTpr_Sistema_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_Codigo" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servico_codigo
      {
         get {
            return sdt.gxTpr_Servico_codigo ;
         }

         set {
            sdt.gxTpr_Servico_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Solicitacoes_Novo_Projeto" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Solicitacoes_novo_projeto
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Solicitacoes_novo_projeto) ;
         }

         set {
            sdt.gxTpr_Solicitacoes_novo_projeto = (String)(value);
         }

      }

      [DataMember( Name = "Solicitacoes_Objetivo" , Order = 5 )]
      public String gxTpr_Solicitacoes_objetivo
      {
         get {
            return sdt.gxTpr_Solicitacoes_objetivo ;
         }

         set {
            sdt.gxTpr_Solicitacoes_objetivo = (String)(value);
         }

      }

      [DataMember( Name = "Solicitacoes_Usuario" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Solicitacoes_usuario
      {
         get {
            return sdt.gxTpr_Solicitacoes_usuario ;
         }

         set {
            sdt.gxTpr_Solicitacoes_usuario = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Solicitacoes_Data" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Solicitacoes_data
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Solicitacoes_data) ;
         }

         set {
            sdt.gxTpr_Solicitacoes_data = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "Solicitacoes_Usuario_Ult" , Order = 8 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Solicitacoes_usuario_ult
      {
         get {
            return sdt.gxTpr_Solicitacoes_usuario_ult ;
         }

         set {
            sdt.gxTpr_Solicitacoes_usuario_ult = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Solicitacoes_Data_Ult" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Solicitacoes_data_ult
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Solicitacoes_data_ult) ;
         }

         set {
            sdt.gxTpr_Solicitacoes_data_ult = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "Solicitacoes_Status" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Solicitacoes_status
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Solicitacoes_status) ;
         }

         set {
            sdt.gxTpr_Solicitacoes_status = (String)(value);
         }

      }

      public SdtSolicitacoes sdt
      {
         get {
            return (SdtSolicitacoes)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSolicitacoes() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 24 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
