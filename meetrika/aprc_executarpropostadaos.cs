/*
               File: PRC_ExecutarPropostaDaOS
        Description: Executar Proposta
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:9:45.98
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_executarpropostadaos : GXProcedure
   {
      public aprc_executarpropostadaos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_executarpropostadaos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Proposta_OSCodigo ,
                           int aP1_Contratada_Codigo ,
                           int aP2_Owner ,
                           DateTime aP3_DataInicio ,
                           ref DateTime aP4_DataFim )
      {
         this.A1686Proposta_OSCodigo = aP0_Proposta_OSCodigo;
         this.AV11Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV15Owner = aP2_Owner;
         this.AV36DataInicio = aP3_DataInicio;
         this.AV35DataFim = aP4_DataFim;
         initialize();
         executePrivate();
         aP0_Proposta_OSCodigo=this.A1686Proposta_OSCodigo;
         aP4_DataFim=this.AV35DataFim;
      }

      public DateTime executeUdp( ref int aP0_Proposta_OSCodigo ,
                                  int aP1_Contratada_Codigo ,
                                  int aP2_Owner ,
                                  DateTime aP3_DataInicio )
      {
         this.A1686Proposta_OSCodigo = aP0_Proposta_OSCodigo;
         this.AV11Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV15Owner = aP2_Owner;
         this.AV36DataInicio = aP3_DataInicio;
         this.AV35DataFim = aP4_DataFim;
         initialize();
         executePrivate();
         aP0_Proposta_OSCodigo=this.A1686Proposta_OSCodigo;
         aP4_DataFim=this.AV35DataFim;
         return AV35DataFim ;
      }

      public void executeSubmit( ref int aP0_Proposta_OSCodigo ,
                                 int aP1_Contratada_Codigo ,
                                 int aP2_Owner ,
                                 DateTime aP3_DataInicio ,
                                 ref DateTime aP4_DataFim )
      {
         aprc_executarpropostadaos objaprc_executarpropostadaos;
         objaprc_executarpropostadaos = new aprc_executarpropostadaos();
         objaprc_executarpropostadaos.A1686Proposta_OSCodigo = aP0_Proposta_OSCodigo;
         objaprc_executarpropostadaos.AV11Contratada_Codigo = aP1_Contratada_Codigo;
         objaprc_executarpropostadaos.AV15Owner = aP2_Owner;
         objaprc_executarpropostadaos.AV36DataInicio = aP3_DataInicio;
         objaprc_executarpropostadaos.AV35DataFim = aP4_DataFim;
         objaprc_executarpropostadaos.context.SetSubmitInitialConfig(context);
         objaprc_executarpropostadaos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_executarpropostadaos);
         aP0_Proposta_OSCodigo=this.A1686Proposta_OSCodigo;
         aP4_DataFim=this.AV35DataFim;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_executarpropostadaos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00C62 */
         pr_default.execute(0, new Object[] {A1686Proposta_OSCodigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1705Proposta_OSServico = P00C62_A1705Proposta_OSServico[0];
            n1705Proposta_OSServico = P00C62_n1705Proposta_OSServico[0];
            A1711Proposta_OSDmnFM = P00C62_A1711Proposta_OSDmnFM[0];
            n1711Proposta_OSDmnFM = P00C62_n1711Proposta_OSDmnFM[0];
            A1699Proposta_ContratadaCod = P00C62_A1699Proposta_ContratadaCod[0];
            A1704Proposta_CntSrvCod = P00C62_A1704Proposta_CntSrvCod[0];
            A1690Proposta_Objetivo = P00C62_A1690Proposta_Objetivo[0];
            A1707Proposta_OSPrzTpDias = P00C62_A1707Proposta_OSPrzTpDias[0];
            n1707Proposta_OSPrzTpDias = P00C62_n1707Proposta_OSPrzTpDias[0];
            A1703Proposta_Esforco = P00C62_A1703Proposta_Esforco[0];
            A1722Proposta_ValorUndCnt = P00C62_A1722Proposta_ValorUndCnt[0];
            A484ContagemResultado_StatusDmn = P00C62_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00C62_n484ContagemResultado_StatusDmn[0];
            A1685Proposta_Codigo = P00C62_A1685Proposta_Codigo[0];
            A1705Proposta_OSServico = P00C62_A1705Proposta_OSServico[0];
            n1705Proposta_OSServico = P00C62_n1705Proposta_OSServico[0];
            A1711Proposta_OSDmnFM = P00C62_A1711Proposta_OSDmnFM[0];
            n1711Proposta_OSDmnFM = P00C62_n1711Proposta_OSDmnFM[0];
            A484ContagemResultado_StatusDmn = P00C62_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00C62_n484ContagemResultado_StatusDmn[0];
            A1707Proposta_OSPrzTpDias = P00C62_A1707Proposta_OSPrzTpDias[0];
            n1707Proposta_OSPrzTpDias = P00C62_n1707Proposta_OSPrzTpDias[0];
            AV18Proposta_OSDmnFM = A1711Proposta_OSDmnFM;
            AV17Proposta_ContratadaCod = A1699Proposta_ContratadaCod;
            AV16Proposta_CntSrvCod = A1704Proposta_CntSrvCod;
            AV8Proposta_Objetivo = A1690Proposta_Objetivo;
            AV19Proposta_OSPrzTpDias = A1707Proposta_OSPrzTpDias;
            AV24Proposta_Esforco = A1703Proposta_Esforco;
            AV26ValorPF = A1722Proposta_ValorUndCnt;
            AV32StatusDmn = A484ContagemResultado_StatusDmn;
            AV33TipoDias = A1707Proposta_OSPrzTpDias;
            /* Using cursor P00C63 */
            pr_default.execute(1, new Object[] {A1686Proposta_OSCodigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A896LogResponsavel_Owner = P00C63_A896LogResponsavel_Owner[0];
               A1797LogResponsavel_Codigo = P00C63_A1797LogResponsavel_Codigo[0];
               A891LogResponsavel_UsuarioCod = P00C63_A891LogResponsavel_UsuarioCod[0];
               n891LogResponsavel_UsuarioCod = P00C63_n891LogResponsavel_UsuarioCod[0];
               A892LogResponsavel_DemandaCod = P00C63_A892LogResponsavel_DemandaCod[0];
               n892LogResponsavel_DemandaCod = P00C63_n892LogResponsavel_DemandaCod[0];
               GXt_boolean1 = A1148LogResponsavel_UsuarioEhContratante;
               new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A891LogResponsavel_UsuarioCod, out  GXt_boolean1) ;
               A1148LogResponsavel_UsuarioEhContratante = GXt_boolean1;
               if ( ! A1148LogResponsavel_UsuarioEhContratante )
               {
                  AV23Proposta_Owner = (short)(A896LogResponsavel_Owner);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         /* Execute user subroutine: 'NOVAOS' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'NOVAOS' Routine */
         GXt_int2 = AV34Dias;
         new prc_diasuteisentre(context ).execute(  AV36DataInicio,  AV35DataFim,  AV33TipoDias, out  GXt_int2) ;
         AV34Dias = GXt_int2;
         if ( StringUtil.StrCmp(AV32StatusDmn, "I") == 0 )
         {
            AV14NovaOS.Load(A1686Proposta_OSCodigo);
            AV14NovaOS.gxTv_SdtContagemResultado_Contagemresultado_responsavel_SetNull();
         }
         else
         {
            AV14NovaOS = new SdtContagemResultado(context);
            AV10Contratada.Load(AV11Contratada_Codigo);
            GXt_int3 = AV10Contratada.gxTpr_Contratada_areatrabalhocod;
            if ( new prc_osautomatica(context).executeUdp( ref  GXt_int3) )
            {
               AV10Contratada.gxTpr_Contratada_os = (int)(AV10Contratada.gxTpr_Contratada_os+1);
               AV14NovaOS.gxTpr_Contagemresultado_demandafm = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Contratada.gxTpr_Contratada_os), 8, 0));
            }
            else
            {
               AV14NovaOS.gxTpr_Contagemresultado_demandafm = AV18Proposta_OSDmnFM;
            }
            AV14NovaOS.gxTpr_Contagemresultado_demanda = AV18Proposta_OSDmnFM;
            AV14NovaOS.gxTpr_Contagemresultado_datadmn = DateTimeUtil.ServerDate( context, "DEFAULT");
            AV14NovaOS.gxTpr_Contagemresultado_datainicio = AV36DataInicio;
            AV14NovaOS.gxTpr_Contagemresultado_contratadacod = AV17Proposta_ContratadaCod;
            AV14NovaOS.gxTpr_Contagemresultado_cntsrvcod = AV16Proposta_CntSrvCod;
            AV14NovaOS.gxTpr_Contagemresultado_descricao = AV8Proposta_Objetivo;
            AV14NovaOS.gxTpr_Contagemresultado_owner = AV15Owner;
            AV14NovaOS.gxTpr_Contagemresultado_osvinculada = A1686Proposta_OSCodigo;
            GXt_int2 = 0;
            new prc_diasuteisentre(context ).execute(  AV36DataInicio,  AV35DataFim,  AV19Proposta_OSPrzTpDias, out  GXt_int2) ;
            AV14NovaOS.gxTpr_Contagemresultado_prazoinicialdias = GXt_int2;
            AV14NovaOS.gxTpr_Contagemresultado_valorpf = AV26ValorPF;
            AV14NovaOS.gxTpr_Contagemresultado_evento = 1;
            AV14NovaOS.gxTpr_Contagemresultado_tiporegistro = 1;
            AV14NovaOS.gxTpr_Contagemresultado_combinada = true;
            AV14NovaOS.gxTpr_Contagemresultado_ss = 0;
            AV14NovaOS.gxTpr_Contagemresultado_datacadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         }
         AV14NovaOS.gxTpr_Contagemresultado_statusdmn = "S";
         AV14NovaOS.gxTpr_Contagemresultado_dataentrega = AV35DataFim;
         AV14NovaOS.gxTpr_Contagemresultado_prazoinicialdias = AV34Dias;
         AV14NovaOS.Save();
         if ( AV14NovaOS.Success() )
         {
            if ( ! ( StringUtil.StrCmp(AV32StatusDmn, "I") == 0 ) )
            {
               GXt_char4 = AV28CalculoPFinal;
               GXt_int3 = AV14NovaOS.gxTpr_Contagemresultado_cntsrvcod;
               GXt_int5 = AV23Proposta_Owner;
               new prc_cstuntprdnrm(context ).execute( ref  GXt_int3, ref  GXt_int5, ref  AV27CstUntNrm, out  GXt_char4) ;
               AV14NovaOS.gxTpr_Contagemresultado_cntsrvcod = GXt_int3;
               AV23Proposta_Owner = (short)(Convert.ToInt16(GXt_int5));
               AV28CalculoPFinal = GXt_char4;
               AV22RE = new SdtContagemResultadoContagens(context);
               AV22RE.gxTpr_Contagemresultado_codigo = AV14NovaOS.gxTpr_Contagemresultado_codigo;
               AV22RE.gxTpr_Contagemresultado_datacnt = DateTimeUtil.ServerDate( context, "DEFAULT");
               AV22RE.gxTpr_Contagemresultado_horacnt = Gx_time;
               AV22RE.gxTpr_Contagemresultado_contadorfmcod = AV23Proposta_Owner;
               AV22RE.gxTpr_Contagemresultado_divergencia = (decimal)(0);
               AV22RE.gxTpr_Contagemresultado_statuscnt = 5;
               if ( StringUtil.StrCmp(AV10Contratada.gxTpr_Contratada_tipofabrica, "S") == 0 )
               {
                  AV22RE.gxTpr_Contagemresultado_pfbfs = (decimal)(AV24Proposta_Esforco);
                  AV22RE.gxTpr_Contagemresultado_pflfs = (decimal)(AV24Proposta_Esforco);
               }
               else
               {
                  AV22RE.gxTpr_Contagemresultado_pfbfm = (decimal)(AV24Proposta_Esforco);
                  AV22RE.gxTpr_Contagemresultado_pflfm = (decimal)(AV24Proposta_Esforco);
               }
               AV22RE.gxTpr_Contagemresultadocontagens_esforco = 0;
               AV22RE.gxTpr_Contagemresultado_deflator = (decimal)(0);
               AV22RE.gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_SetNull();
               AV22RE.gxTpr_Contagemresultado_cstuntprd = AV27CstUntNrm;
               AV22RE.gxTpr_Contagemresultado_ultima = true;
               AV22RE.Save();
               AV10Contratada.Save();
               AV14NovaOS.Load(AV14NovaOS.gxTpr_Contagemresultado_codigo);
               AV14NovaOS.gxTpr_Contagemresultado_statusdmn = "S";
               AV14NovaOS.Save();
               new prc_contagemresultadoalterastatus(context ).execute( ref  A1686Proposta_OSCodigo,  "H",  0,  "Autorizada a execu��o da proposta.", ref  AV9Confirmado) ;
            }
            AV29Contrato_Codigo = AV14NovaOS.gxTpr_Contagemresultado_cntcod;
            AV30Valor_Debito = AV14NovaOS.gxTpr_Contagemresultado_valorfinal;
            GXt_int5 = AV14NovaOS.gxTpr_Contagemresultado_codigo;
            new prc_inslogresponsavel(context ).execute( ref  GXt_int5,  AV14NovaOS.gxTpr_Contagemresultado_responsavel,  "S",  "D",  AV15Owner,  0,  "",  "S",  "Homologada a proposta "+AV18Proposta_OSDmnFM,  AV35DataFim,  true) ;
            AV14NovaOS.gxTpr_Contagemresultado_codigo = GXt_int5;
            context.CommitDataStores( "PRC_ExecutarPropostaDaOS");
         }
         else
         {
            context.RollbackDataStores( "PRC_ExecutarPropostaDaOS");
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00C62_A1705Proposta_OSServico = new int[1] ;
         P00C62_n1705Proposta_OSServico = new bool[] {false} ;
         P00C62_A1686Proposta_OSCodigo = new int[1] ;
         P00C62_A1711Proposta_OSDmnFM = new String[] {""} ;
         P00C62_n1711Proposta_OSDmnFM = new bool[] {false} ;
         P00C62_A1699Proposta_ContratadaCod = new int[1] ;
         P00C62_A1704Proposta_CntSrvCod = new int[1] ;
         P00C62_A1690Proposta_Objetivo = new String[] {""} ;
         P00C62_A1707Proposta_OSPrzTpDias = new String[] {""} ;
         P00C62_n1707Proposta_OSPrzTpDias = new bool[] {false} ;
         P00C62_A1703Proposta_Esforco = new int[1] ;
         P00C62_A1722Proposta_ValorUndCnt = new decimal[1] ;
         P00C62_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00C62_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00C62_A1685Proposta_Codigo = new int[1] ;
         A1711Proposta_OSDmnFM = "";
         A1690Proposta_Objetivo = "";
         A1707Proposta_OSPrzTpDias = "";
         A484ContagemResultado_StatusDmn = "";
         AV18Proposta_OSDmnFM = "";
         AV8Proposta_Objetivo = "";
         AV19Proposta_OSPrzTpDias = "";
         AV32StatusDmn = "";
         AV33TipoDias = "";
         P00C63_A896LogResponsavel_Owner = new int[1] ;
         P00C63_A1797LogResponsavel_Codigo = new long[1] ;
         P00C63_A891LogResponsavel_UsuarioCod = new int[1] ;
         P00C63_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         P00C63_A892LogResponsavel_DemandaCod = new int[1] ;
         P00C63_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         AV14NovaOS = new SdtContagemResultado(context);
         AV10Contratada = new SdtContratada(context);
         AV28CalculoPFinal = "";
         GXt_char4 = "";
         AV22RE = new SdtContagemResultadoContagens(context);
         Gx_time = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_executarpropostadaos__default(),
            new Object[][] {
                new Object[] {
               P00C62_A1705Proposta_OSServico, P00C62_n1705Proposta_OSServico, P00C62_A1686Proposta_OSCodigo, P00C62_A1711Proposta_OSDmnFM, P00C62_n1711Proposta_OSDmnFM, P00C62_A1699Proposta_ContratadaCod, P00C62_A1704Proposta_CntSrvCod, P00C62_A1690Proposta_Objetivo, P00C62_A1707Proposta_OSPrzTpDias, P00C62_n1707Proposta_OSPrzTpDias,
               P00C62_A1703Proposta_Esforco, P00C62_A1722Proposta_ValorUndCnt, P00C62_A484ContagemResultado_StatusDmn, P00C62_n484ContagemResultado_StatusDmn, P00C62_A1685Proposta_Codigo
               }
               , new Object[] {
               P00C63_A896LogResponsavel_Owner, P00C63_A1797LogResponsavel_Codigo, P00C63_A891LogResponsavel_UsuarioCod, P00C63_n891LogResponsavel_UsuarioCod, P00C63_A892LogResponsavel_DemandaCod, P00C63_n892LogResponsavel_DemandaCod
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         /* GeneXus formulas. */
         Gx_time = context.localUtil.Time( );
         context.Gx_err = 0;
      }

      private short AV23Proposta_Owner ;
      private short AV34Dias ;
      private short GXt_int2 ;
      private int A1686Proposta_OSCodigo ;
      private int AV11Contratada_Codigo ;
      private int AV15Owner ;
      private int A1705Proposta_OSServico ;
      private int A1699Proposta_ContratadaCod ;
      private int A1704Proposta_CntSrvCod ;
      private int A1703Proposta_Esforco ;
      private int A1685Proposta_Codigo ;
      private int AV17Proposta_ContratadaCod ;
      private int AV16Proposta_CntSrvCod ;
      private int AV24Proposta_Esforco ;
      private int A896LogResponsavel_Owner ;
      private int A891LogResponsavel_UsuarioCod ;
      private int A892LogResponsavel_DemandaCod ;
      private int GXt_int3 ;
      private int AV29Contrato_Codigo ;
      private int GXt_int5 ;
      private long A1797LogResponsavel_Codigo ;
      private decimal A1722Proposta_ValorUndCnt ;
      private decimal AV26ValorPF ;
      private decimal AV27CstUntNrm ;
      private decimal AV30Valor_Debito ;
      private String scmdbuf ;
      private String A1707Proposta_OSPrzTpDias ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV19Proposta_OSPrzTpDias ;
      private String AV32StatusDmn ;
      private String AV33TipoDias ;
      private String AV28CalculoPFinal ;
      private String GXt_char4 ;
      private String Gx_time ;
      private DateTime AV36DataInicio ;
      private DateTime AV35DataFim ;
      private bool n1705Proposta_OSServico ;
      private bool n1711Proposta_OSDmnFM ;
      private bool n1707Proposta_OSPrzTpDias ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n891LogResponsavel_UsuarioCod ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool A1148LogResponsavel_UsuarioEhContratante ;
      private bool GXt_boolean1 ;
      private bool returnInSub ;
      private bool AV9Confirmado ;
      private String A1690Proposta_Objetivo ;
      private String AV8Proposta_Objetivo ;
      private String A1711Proposta_OSDmnFM ;
      private String AV18Proposta_OSDmnFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Proposta_OSCodigo ;
      private DateTime aP4_DataFim ;
      private IDataStoreProvider pr_default ;
      private int[] P00C62_A1705Proposta_OSServico ;
      private bool[] P00C62_n1705Proposta_OSServico ;
      private int[] P00C62_A1686Proposta_OSCodigo ;
      private String[] P00C62_A1711Proposta_OSDmnFM ;
      private bool[] P00C62_n1711Proposta_OSDmnFM ;
      private int[] P00C62_A1699Proposta_ContratadaCod ;
      private int[] P00C62_A1704Proposta_CntSrvCod ;
      private String[] P00C62_A1690Proposta_Objetivo ;
      private String[] P00C62_A1707Proposta_OSPrzTpDias ;
      private bool[] P00C62_n1707Proposta_OSPrzTpDias ;
      private int[] P00C62_A1703Proposta_Esforco ;
      private decimal[] P00C62_A1722Proposta_ValorUndCnt ;
      private String[] P00C62_A484ContagemResultado_StatusDmn ;
      private bool[] P00C62_n484ContagemResultado_StatusDmn ;
      private int[] P00C62_A1685Proposta_Codigo ;
      private int[] P00C63_A896LogResponsavel_Owner ;
      private long[] P00C63_A1797LogResponsavel_Codigo ;
      private int[] P00C63_A891LogResponsavel_UsuarioCod ;
      private bool[] P00C63_n891LogResponsavel_UsuarioCod ;
      private int[] P00C63_A892LogResponsavel_DemandaCod ;
      private bool[] P00C63_n892LogResponsavel_DemandaCod ;
      private SdtContratada AV10Contratada ;
      private SdtContagemResultado AV14NovaOS ;
      private SdtContagemResultadoContagens AV22RE ;
   }

   public class aprc_executarpropostadaos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00C62 ;
          prmP00C62 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00C63 ;
          prmP00C63 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00C62", "SELECT TOP 1 T2.[ContagemResultado_CntSrvCod] AS Proposta_OSServico, T1.[Proposta_OSCodigo] AS Proposta_OSCodigo, T2.[ContagemResultado_DemandaFM] AS Proposta_OSDmnFM, T1.[Proposta_PrestatoraCod], T1.[Proposta_CntSrvCod], T1.[Proposta_Objetivo], T3.[ContratoServicos_PrazoTpDias] AS Proposta_OSPrzTpDias, T1.[Proposta_Esforco], T1.[Proposta_ValorUndCnt], T2.[ContagemResultado_StatusDmn], T1.[Proposta_Codigo] FROM ((dbo.[Proposta] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[Proposta_OSCodigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE T1.[Proposta_OSCodigo] = @Proposta_OSCodigo ORDER BY T1.[Proposta_OSCodigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00C62,1,0,true,true )
             ,new CursorDef("P00C63", "SELECT [LogResponsavel_Owner], [LogResponsavel_Codigo], [LogResponsavel_UsuarioCod], [LogResponsavel_DemandaCod] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @Proposta_OSCodigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00C63,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((decimal[]) buf[11])[0] = rslt.getDecimal(9) ;
                ((String[]) buf[12])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((int[]) buf[14])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
