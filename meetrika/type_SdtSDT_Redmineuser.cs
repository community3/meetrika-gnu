/*
               File: type_SdtSDT_Redmineuser
        Description: SDT_Redmineuser
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:5.69
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "user" )]
   [XmlType(TypeName =  "user" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_Redmineuser_memberships ))]
   [Serializable]
   public class SdtSDT_Redmineuser : GxUserType
   {
      public SdtSDT_Redmineuser( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Redmineuser_Login = "";
         gxTv_SdtSDT_Redmineuser_Firstname = "";
         gxTv_SdtSDT_Redmineuser_Lastname = "";
         gxTv_SdtSDT_Redmineuser_Mail = "";
         gxTv_SdtSDT_Redmineuser_Created_on = "";
         gxTv_SdtSDT_Redmineuser_Last_login_on = "";
         gxTv_SdtSDT_Redmineuser_Api_key = "";
      }

      public SdtSDT_Redmineuser( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Redmineuser deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Redmineuser)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Redmineuser obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Login = deserialized.gxTpr_Login;
         obj.gxTpr_Firstname = deserialized.gxTpr_Firstname;
         obj.gxTpr_Lastname = deserialized.gxTpr_Lastname;
         obj.gxTpr_Mail = deserialized.gxTpr_Mail;
         obj.gxTpr_Created_on = deserialized.gxTpr_Created_on;
         obj.gxTpr_Last_login_on = deserialized.gxTpr_Last_login_on;
         obj.gxTpr_Api_key = deserialized.gxTpr_Api_key;
         obj.gxTpr_Memberships = deserialized.gxTpr_Memberships;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "id") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineuser_Id = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "login") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineuser_Login = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "firstname") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineuser_Firstname = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "lastname") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineuser_Lastname = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "mail") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineuser_Mail = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "created_on") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineuser_Created_on = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "last_login_on") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineuser_Last_login_on = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "api_key") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineuser_Api_key = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "memberships") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_Redmineuser_Memberships == null )
                  {
                     gxTv_SdtSDT_Redmineuser_Memberships = new SdtSDT_Redmineuser_memberships(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_Redmineuser_Memberships.readxml(oReader, "memberships");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "user";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Redmineuser_Id), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("login", StringUtil.RTrim( gxTv_SdtSDT_Redmineuser_Login));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("firstname", StringUtil.RTrim( gxTv_SdtSDT_Redmineuser_Firstname));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("lastname", StringUtil.RTrim( gxTv_SdtSDT_Redmineuser_Lastname));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("mail", StringUtil.RTrim( gxTv_SdtSDT_Redmineuser_Mail));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("created_on", StringUtil.RTrim( gxTv_SdtSDT_Redmineuser_Created_on));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("last_login_on", StringUtil.RTrim( gxTv_SdtSDT_Redmineuser_Last_login_on));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("api_key", StringUtil.RTrim( gxTv_SdtSDT_Redmineuser_Api_key));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         if ( gxTv_SdtSDT_Redmineuser_Memberships != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_Redmineuser_Memberships.writexml(oWriter, "memberships", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("id", gxTv_SdtSDT_Redmineuser_Id, false);
         AddObjectProperty("login", gxTv_SdtSDT_Redmineuser_Login, false);
         AddObjectProperty("firstname", gxTv_SdtSDT_Redmineuser_Firstname, false);
         AddObjectProperty("lastname", gxTv_SdtSDT_Redmineuser_Lastname, false);
         AddObjectProperty("mail", gxTv_SdtSDT_Redmineuser_Mail, false);
         AddObjectProperty("created_on", gxTv_SdtSDT_Redmineuser_Created_on, false);
         AddObjectProperty("last_login_on", gxTv_SdtSDT_Redmineuser_Last_login_on, false);
         AddObjectProperty("api_key", gxTv_SdtSDT_Redmineuser_Api_key, false);
         if ( gxTv_SdtSDT_Redmineuser_Memberships != null )
         {
            AddObjectProperty("memberships", gxTv_SdtSDT_Redmineuser_Memberships, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "id" )]
      [  XmlElement( ElementName = "id" , Namespace = ""  )]
      public int gxTpr_Id
      {
         get {
            return gxTv_SdtSDT_Redmineuser_Id ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_Id = (int)(value);
         }

      }

      [  SoapElement( ElementName = "login" )]
      [  XmlElement( ElementName = "login" , Namespace = ""  )]
      public String gxTpr_Login
      {
         get {
            return gxTv_SdtSDT_Redmineuser_Login ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_Login = (String)(value);
         }

      }

      [  SoapElement( ElementName = "firstname" )]
      [  XmlElement( ElementName = "firstname" , Namespace = ""  )]
      public String gxTpr_Firstname
      {
         get {
            return gxTv_SdtSDT_Redmineuser_Firstname ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_Firstname = (String)(value);
         }

      }

      [  SoapElement( ElementName = "lastname" )]
      [  XmlElement( ElementName = "lastname" , Namespace = ""  )]
      public String gxTpr_Lastname
      {
         get {
            return gxTv_SdtSDT_Redmineuser_Lastname ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_Lastname = (String)(value);
         }

      }

      [  SoapElement( ElementName = "mail" )]
      [  XmlElement( ElementName = "mail" , Namespace = ""  )]
      public String gxTpr_Mail
      {
         get {
            return gxTv_SdtSDT_Redmineuser_Mail ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_Mail = (String)(value);
         }

      }

      [  SoapElement( ElementName = "created_on" )]
      [  XmlElement( ElementName = "created_on" , Namespace = ""  )]
      public String gxTpr_Created_on
      {
         get {
            return gxTv_SdtSDT_Redmineuser_Created_on ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_Created_on = (String)(value);
         }

      }

      [  SoapElement( ElementName = "last_login_on" )]
      [  XmlElement( ElementName = "last_login_on" , Namespace = ""  )]
      public String gxTpr_Last_login_on
      {
         get {
            return gxTv_SdtSDT_Redmineuser_Last_login_on ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_Last_login_on = (String)(value);
         }

      }

      [  SoapElement( ElementName = "api_key" )]
      [  XmlElement( ElementName = "api_key" , Namespace = ""  )]
      public String gxTpr_Api_key
      {
         get {
            return gxTv_SdtSDT_Redmineuser_Api_key ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_Api_key = (String)(value);
         }

      }

      [  SoapElement( ElementName = "memberships" )]
      [  XmlElement( ElementName = "memberships"   )]
      public SdtSDT_Redmineuser_memberships gxTpr_Memberships
      {
         get {
            if ( gxTv_SdtSDT_Redmineuser_Memberships == null )
            {
               gxTv_SdtSDT_Redmineuser_Memberships = new SdtSDT_Redmineuser_memberships(context);
            }
            return gxTv_SdtSDT_Redmineuser_Memberships ;
         }

         set {
            gxTv_SdtSDT_Redmineuser_Memberships = value;
         }

      }

      public void gxTv_SdtSDT_Redmineuser_Memberships_SetNull( )
      {
         gxTv_SdtSDT_Redmineuser_Memberships = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Redmineuser_Memberships_IsNull( )
      {
         if ( gxTv_SdtSDT_Redmineuser_Memberships == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_Redmineuser_Login = "";
         gxTv_SdtSDT_Redmineuser_Firstname = "";
         gxTv_SdtSDT_Redmineuser_Lastname = "";
         gxTv_SdtSDT_Redmineuser_Mail = "";
         gxTv_SdtSDT_Redmineuser_Created_on = "";
         gxTv_SdtSDT_Redmineuser_Last_login_on = "";
         gxTv_SdtSDT_Redmineuser_Api_key = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_Redmineuser_Id ;
      protected String gxTv_SdtSDT_Redmineuser_Login ;
      protected String gxTv_SdtSDT_Redmineuser_Firstname ;
      protected String gxTv_SdtSDT_Redmineuser_Lastname ;
      protected String gxTv_SdtSDT_Redmineuser_Mail ;
      protected String gxTv_SdtSDT_Redmineuser_Created_on ;
      protected String gxTv_SdtSDT_Redmineuser_Last_login_on ;
      protected String gxTv_SdtSDT_Redmineuser_Api_key ;
      protected String sTagName ;
      protected SdtSDT_Redmineuser_memberships gxTv_SdtSDT_Redmineuser_Memberships=null ;
   }

   [DataContract(Name = @"SDT_Redmineuser", Namespace = "")]
   public class SdtSDT_Redmineuser_RESTInterface : GxGenericCollectionItem<SdtSDT_Redmineuser>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Redmineuser_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Redmineuser_RESTInterface( SdtSDT_Redmineuser psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "id" , Order = 0 )]
      public String gxTpr_Id
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Id), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Id = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "login" , Order = 1 )]
      public String gxTpr_Login
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Login) ;
         }

         set {
            sdt.gxTpr_Login = (String)(value);
         }

      }

      [DataMember( Name = "firstname" , Order = 2 )]
      public String gxTpr_Firstname
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Firstname) ;
         }

         set {
            sdt.gxTpr_Firstname = (String)(value);
         }

      }

      [DataMember( Name = "lastname" , Order = 3 )]
      public String gxTpr_Lastname
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Lastname) ;
         }

         set {
            sdt.gxTpr_Lastname = (String)(value);
         }

      }

      [DataMember( Name = "mail" , Order = 4 )]
      public String gxTpr_Mail
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Mail) ;
         }

         set {
            sdt.gxTpr_Mail = (String)(value);
         }

      }

      [DataMember( Name = "created_on" , Order = 5 )]
      public String gxTpr_Created_on
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Created_on) ;
         }

         set {
            sdt.gxTpr_Created_on = (String)(value);
         }

      }

      [DataMember( Name = "last_login_on" , Order = 6 )]
      public String gxTpr_Last_login_on
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Last_login_on) ;
         }

         set {
            sdt.gxTpr_Last_login_on = (String)(value);
         }

      }

      [DataMember( Name = "api_key" , Order = 7 )]
      public String gxTpr_Api_key
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Api_key) ;
         }

         set {
            sdt.gxTpr_Api_key = (String)(value);
         }

      }

      [DataMember( Name = "memberships" , Order = 8 )]
      public SdtSDT_Redmineuser_memberships_RESTInterface gxTpr_Memberships
      {
         get {
            return new SdtSDT_Redmineuser_memberships_RESTInterface(sdt.gxTpr_Memberships) ;
         }

         set {
            sdt.gxTpr_Memberships = value.sdt;
         }

      }

      public SdtSDT_Redmineuser sdt
      {
         get {
            return (SdtSDT_Redmineuser)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Redmineuser() ;
         }
      }

   }

}
