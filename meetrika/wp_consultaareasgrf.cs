/*
               File: WP_ConsultaAreasGrf
        Description: Consulta de Produtividade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:52:46.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_consultaareasgrf : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_consultaareasgrf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_consultaareasgrf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContadorFMCod ,
                           int aP1_Servico_Codigo ,
                           ref bool aP2_UserEhContratada ,
                           ref bool aP3_UserEhContratante )
      {
         this.AV38ContadorFMCod = aP0_ContadorFMCod;
         this.AV39Servico_Codigo = aP1_Servico_Codigo;
         this.AV40UserEhContratada = aP2_UserEhContratada;
         this.AV41UserEhContratante = aP3_UserEhContratante;
         executePrivate();
         aP0_ContadorFMCod=this.AV38ContadorFMCod;
         aP2_UserEhContratada=this.AV40UserEhContratada;
         aP3_UserEhContratante=this.AV41UserEhContratante;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavContagemresultado_contadorfmcod = new GXCombobox();
         cmbavGraficar = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV38ContadorFMCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ContadorFMCod), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV39Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Servico_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV39Servico_Codigo), "ZZZZZ9")));
                  AV40UserEhContratada = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40UserEhContratada", AV40UserEhContratada);
                  AV41UserEhContratante = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41UserEhContratante", AV41UserEhContratante);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PADY2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTDY2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423524686");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("GxChart/gxChart.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_consultaareasgrf.aspx") + "?" + UrlEncode("" +AV38ContadorFMCod) + "," + UrlEncode("" +AV39Servico_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV40UserEhContratada)) + "," + UrlEncode(StringUtil.BoolToStr(AV41UserEhContratante))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGXCHARTDATA", GxChartData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGXCHARTDATA", GxChartData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGXCHARTDATANULL", GxChartDataNull);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGXCHARTDATANULL", GxChartDataNull);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGXCHARTSERIENULL", GxChartSerieNull);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGXCHARTSERIENULL", GxChartSerieNull);
         }
         GxWebStd.gx_hidden_field( context, "vTODAY", context.localUtil.DToC( Gx_date, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTADORFMNOM", StringUtil.RTrim( A474ContagemResultado_ContadorFMNom));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_LOTEACEITECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATACNT", context.localUtil.DToC( A473ContagemResultado_DataCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTADORFMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A470ContagemResultado_ContadorFMCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vUSEREHCONTRATANTE", AV41UserEhContratante);
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_CRFMEHCONTRATADA", A999ContagemResultado_CrFMEhContratada);
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_CRFMEHCONTRATANTE", A1000ContagemResultado_CrFMEhContratante);
         GxWebStd.gx_hidden_field( context, "vDATAINI", context.localUtil.DToC( AV31DataIni, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vDATAFIM", context.localUtil.DToC( AV32DataFim, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_ULTIMA", A517ContagemResultado_Ultima);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFFINAL", StringUtil.LTrim( StringUtil.NToC( A574ContagemResultado_PFFinal, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_VALORPF", StringUtil.LTrim( StringUtil.NToC( A512ContagemResultado_ValorPF, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTADORFMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38ContadorFMCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vUSEREHCONTRATADA", AV40UserEhContratada);
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV39Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV39Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Title", StringUtil.RTrim( Gxchartcontrol_Title));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Width", StringUtil.RTrim( Gxchartcontrol_Width));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Height", StringUtil.RTrim( Gxchartcontrol_Height));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Opacity", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gxchartcontrol_Opacity), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Charttype", StringUtil.RTrim( Gxchartcontrol_Charttype));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_X_axistitle", StringUtil.RTrim( Gxchartcontrol_X_axistitle));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Y_axistitle", StringUtil.RTrim( Gxchartcontrol_Y_axistitle));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Backgroundcolor1", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gxchartcontrol_Backgroundcolor1), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Graphcolor1", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gxchartcontrol_Graphcolor1), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Graphcolor2", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gxchartcontrol_Graphcolor2), 9, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEDY2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTDY2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_consultaareasgrf.aspx") + "?" + UrlEncode("" +AV38ContadorFMCod) + "," + UrlEncode("" +AV39Servico_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV40UserEhContratada)) + "," + UrlEncode(StringUtil.BoolToStr(AV41UserEhContratante)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_ConsultaAreasGrf" ;
      }

      public override String GetPgmdesc( )
      {
         return "Consulta de Produtividade" ;
      }

      protected void WBDY0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_DY2( true) ;
         }
         else
         {
            wb_table1_2_DY2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DY2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTDY2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Consulta de Produtividade", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPDY0( ) ;
      }

      protected void WSDY2( )
      {
         STARTDY2( ) ;
         EVTDY2( ) ;
      }

      protected void EVTDY2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11DY2 */
                              E11DY2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'GRAFICAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12DY2 */
                              E12DY2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13DY2 */
                              E13DY2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDY2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PADY2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavContagemresultado_contadorfmcod.Name = "vCONTAGEMRESULTADO_CONTADORFMCOD";
            cmbavContagemresultado_contadorfmcod.WebTags = "";
            cmbavContagemresultado_contadorfmcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Todos)", 0);
            if ( cmbavContagemresultado_contadorfmcod.ItemCount > 0 )
            {
               AV6ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( cmbavContagemresultado_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContadorFMCod), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContadorFMCod), 6, 0)));
            }
            cmbavGraficar.Name = "vGRAFICAR";
            cmbavGraficar.WebTags = "";
            cmbavGraficar.addItem("PF", "Pontos de Fun�ao", 0);
            cmbavGraficar.addItem("FIN", "Financeiro", 0);
            if ( cmbavGraficar.ItemCount > 0 )
            {
               AV33Graficar = cmbavGraficar.getValidValue(AV33Graficar);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Graficar", AV33Graficar);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavContagemresultado_contadorfmcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavContagemresultado_contadorfmcod.ItemCount > 0 )
         {
            AV6ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( cmbavContagemresultado_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContadorFMCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContadorFMCod), 6, 0)));
         }
         if ( cmbavGraficar.ItemCount > 0 )
         {
            AV33Graficar = cmbavGraficar.getValidValue(AV33Graficar);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Graficar", AV33Graficar);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDY2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      protected void RFDY2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E13DY2 */
            E13DY2 ();
            WBDY0( ) ;
         }
      }

      protected void STRUPDY0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11DY2 */
         E11DY2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vGXCHARTDATA"), GxChartData);
            /* Read variables values. */
            cmbavContagemresultado_contadorfmcod.CurrentValue = cgiGet( cmbavContagemresultado_contadorfmcod_Internalname);
            AV6ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( cgiGet( cmbavContagemresultado_contadorfmcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContadorFMCod), 6, 0)));
            cmbavGraficar.CurrentValue = cgiGet( cmbavGraficar_Internalname);
            AV33Graficar = cgiGet( cmbavGraficar_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Graficar", AV33Graficar);
            /* Read saved values. */
            Gxchartcontrol_Title = cgiGet( "GXCHARTCONTROL_Title");
            Gxchartcontrol_Width = cgiGet( "GXCHARTCONTROL_Width");
            Gxchartcontrol_Height = cgiGet( "GXCHARTCONTROL_Height");
            Gxchartcontrol_Opacity = (int)(context.localUtil.CToN( cgiGet( "GXCHARTCONTROL_Opacity"), ",", "."));
            Gxchartcontrol_Charttype = cgiGet( "GXCHARTCONTROL_Charttype");
            Gxchartcontrol_X_axistitle = cgiGet( "GXCHARTCONTROL_X_axistitle");
            Gxchartcontrol_Y_axistitle = cgiGet( "GXCHARTCONTROL_Y_axistitle");
            Gxchartcontrol_Backgroundcolor1 = (int)(context.localUtil.CToN( cgiGet( "GXCHARTCONTROL_Backgroundcolor1"), ",", "."));
            Gxchartcontrol_Graphcolor1 = (int)(context.localUtil.CToN( cgiGet( "GXCHARTCONTROL_Graphcolor1"), ",", "."));
            Gxchartcontrol_Graphcolor2 = (int)(context.localUtil.CToN( cgiGet( "GXCHARTCONTROL_Graphcolor2"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11DY2 */
         E11DY2 ();
         if (returnInSub) return;
      }

      protected void E11DY2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV35WWPContext) ;
         /* Execute user subroutine: 'MESES' */
         S112 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'CONTADORES' */
         S122 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'GERARGRAFICO' */
         S132 ();
         if (returnInSub) return;
      }

      protected void E12DY2( )
      {
         /* 'Graficar' Routine */
         GxChartData = (SdtGxChart)(GxChartDataNull.Clone());
         GxChartSerie = (SdtGxChart_Serie)(GxChartSerieNull.Clone());
         /* Execute user subroutine: 'MESES' */
         S112 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'GERARGRAFICO' */
         S132 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "GxChartData", GxChartData);
      }

      protected void S132( )
      {
         /* 'GERARGRAFICO' Routine */
         AV17mPFBFM[AV13i-1] = 0;
         AV14mCTPF[AV13i-1] = 0;
         GxChartSerie = new SdtGxChart_Serie(context);
         if ( StringUtil.StrCmp(AV33Graficar, "PF") == 0 )
         {
            GxChartSerie.gxTpr_Name = "PF Final";
         }
         else if ( StringUtil.StrCmp(AV33Graficar, "FIN") == 0 )
         {
            GxChartSerie.gxTpr_Name = "R$";
         }
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV6ContagemResultado_ContadorFMCod ,
                                              AV41UserEhContratante ,
                                              AV39Servico_Codigo ,
                                              A470ContagemResultado_ContadorFMCod ,
                                              A999ContagemResultado_CrFMEhContratada ,
                                              A1000ContagemResultado_CrFMEhContratante ,
                                              A601ContagemResultado_Servico ,
                                              A473ContagemResultado_DataCnt ,
                                              AV31DataIni ,
                                              AV32DataFim ,
                                              A597ContagemResultado_LoteAceiteCod ,
                                              A517ContagemResultado_Ultima },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00DY2 */
         pr_default.execute(0, new Object[] {AV31DataIni, AV32DataFim, AV6ContagemResultado_ContadorFMCod, AV39Servico_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00DY2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00DY2_n1553ContagemResultado_CntSrvCod[0];
            A479ContagemResultado_CrFMPessoaCod = H00DY2_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = H00DY2_n479ContagemResultado_CrFMPessoaCod[0];
            A517ContagemResultado_Ultima = H00DY2_A517ContagemResultado_Ultima[0];
            A601ContagemResultado_Servico = H00DY2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00DY2_n601ContagemResultado_Servico[0];
            A597ContagemResultado_LoteAceiteCod = H00DY2_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = H00DY2_n597ContagemResultado_LoteAceiteCod[0];
            A473ContagemResultado_DataCnt = H00DY2_A473ContagemResultado_DataCnt[0];
            A1000ContagemResultado_CrFMEhContratante = H00DY2_A1000ContagemResultado_CrFMEhContratante[0];
            n1000ContagemResultado_CrFMEhContratante = H00DY2_n1000ContagemResultado_CrFMEhContratante[0];
            A999ContagemResultado_CrFMEhContratada = H00DY2_A999ContagemResultado_CrFMEhContratada[0];
            n999ContagemResultado_CrFMEhContratada = H00DY2_n999ContagemResultado_CrFMEhContratada[0];
            A470ContagemResultado_ContadorFMCod = H00DY2_A470ContagemResultado_ContadorFMCod[0];
            A512ContagemResultado_ValorPF = H00DY2_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = H00DY2_n512ContagemResultado_ValorPF[0];
            A474ContagemResultado_ContadorFMNom = H00DY2_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = H00DY2_n474ContagemResultado_ContadorFMNom[0];
            A456ContagemResultado_Codigo = H00DY2_A456ContagemResultado_Codigo[0];
            A479ContagemResultado_CrFMPessoaCod = H00DY2_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = H00DY2_n479ContagemResultado_CrFMPessoaCod[0];
            A1000ContagemResultado_CrFMEhContratante = H00DY2_A1000ContagemResultado_CrFMEhContratante[0];
            n1000ContagemResultado_CrFMEhContratante = H00DY2_n1000ContagemResultado_CrFMEhContratante[0];
            A999ContagemResultado_CrFMEhContratada = H00DY2_A999ContagemResultado_CrFMEhContratada[0];
            n999ContagemResultado_CrFMEhContratada = H00DY2_n999ContagemResultado_CrFMEhContratada[0];
            A474ContagemResultado_ContadorFMNom = H00DY2_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = H00DY2_n474ContagemResultado_ContadorFMNom[0];
            A1553ContagemResultado_CntSrvCod = H00DY2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00DY2_n1553ContagemResultado_CntSrvCod[0];
            A597ContagemResultado_LoteAceiteCod = H00DY2_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = H00DY2_n597ContagemResultado_LoteAceiteCod[0];
            A512ContagemResultado_ValorPF = H00DY2_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = H00DY2_n512ContagemResultado_ValorPF[0];
            A601ContagemResultado_Servico = H00DY2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00DY2_n601ContagemResultado_Servico[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A574ContagemResultado_PFFinal = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
            AV13i = (short)(DateTimeUtil.Month( A473ContagemResultado_DataCnt));
            AV17mPFBFM[AV13i-1] = (decimal)(AV17mPFBFM[AV13i-1]+A574ContagemResultado_PFFinal);
            AV14mCTPF[AV13i-1] = (decimal)(AV14mCTPF[AV13i-1]+(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF));
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV30Data = AV31DataIni;
         AV13i = (short)(DateTimeUtil.Month( AV31DataIni));
         while ( AV13i <= 12 )
         {
            if ( StringUtil.StrCmp(AV33Graficar, "PF") == 0 )
            {
               GxChartSerie.gxTpr_Values.Add(AV17mPFBFM[AV13i-1], 0);
            }
            else if ( StringUtil.StrCmp(AV33Graficar, "FIN") == 0 )
            {
               GxChartSerie.gxTpr_Values.Add(AV14mCTPF[AV13i-1], 0);
            }
            AV13i = (short)(AV13i+1);
         }
         if ( DateTimeUtil.Month( AV31DataIni) > 1 )
         {
            AV13i = 1;
            while ( AV13i <= DateTimeUtil.Month( AV32DataFim) )
            {
               if ( StringUtil.StrCmp(AV33Graficar, "PF") == 0 )
               {
                  GxChartSerie.gxTpr_Values.Add(AV17mPFBFM[AV13i-1], 0);
               }
               else if ( StringUtil.StrCmp(AV33Graficar, "FIN") == 0 )
               {
                  GxChartSerie.gxTpr_Values.Add(AV14mCTPF[AV13i-1], 0);
               }
               AV13i = (short)(AV13i+1);
            }
         }
         GxChartData.gxTpr_Series.Add(GxChartSerie, 0);
      }

      protected void S112( )
      {
         /* 'MESES' Routine */
         AV30Data = Gx_date;
         AV31DataIni = DateTimeUtil.AddYr( Gx_date, -1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DataIni", context.localUtil.Format(AV31DataIni, "99/99/99"));
         AV31DataIni = DateTimeUtil.DAdd(DateTimeUtil.DateEndOfMonth( AV31DataIni),+((int)(1)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DataIni", context.localUtil.Format(AV31DataIni, "99/99/99"));
         AV32DataFim = DateTimeUtil.DateEndOfMonth( Gx_date);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DataFim", context.localUtil.Format(AV32DataFim, "99/99/99"));
         AV30Data = AV31DataIni;
         AV13i = (short)(DateTimeUtil.Month( AV31DataIni));
         while ( AV13i <= 12 )
         {
            GxChartData.gxTpr_Categories.Add(DateTimeUtil.CMonth( AV30Data, "por"), 0);
            AV30Data = DateTimeUtil.AddMth( AV30Data, 1);
            AV13i = (short)(AV13i+1);
         }
         if ( DateTimeUtil.Month( AV31DataIni) > 1 )
         {
            AV13i = 1;
            while ( AV13i <= DateTimeUtil.Month( AV32DataFim) )
            {
               GxChartData.gxTpr_Categories.Add(DateTimeUtil.CMonth( AV30Data, "por"), 0);
               AV30Data = DateTimeUtil.AddMth( AV30Data, 1);
               AV13i = (short)(AV13i+1);
            }
         }
      }

      protected void S122( )
      {
         /* 'CONTADORES' Routine */
         if ( AV35WWPContext.gxTpr_Userehcontratante )
         {
            /* Using cursor H00DY3 */
            pr_default.execute(1, new Object[] {AV35WWPContext.gxTpr_Contratante_codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A61ContratanteUsuario_UsuarioPessoaCod = H00DY3_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00DY3_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A63ContratanteUsuario_ContratanteCod = H00DY3_A63ContratanteUsuario_ContratanteCod[0];
               A60ContratanteUsuario_UsuarioCod = H00DY3_A60ContratanteUsuario_UsuarioCod[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H00DY3_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H00DY3_n62ContratanteUsuario_UsuarioPessoaNom[0];
               A61ContratanteUsuario_UsuarioPessoaCod = H00DY3_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00DY3_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H00DY3_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H00DY3_n62ContratanteUsuario_UsuarioPessoaNom[0];
               cmbavContagemresultado_contadorfmcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)), A62ContratanteUsuario_UsuarioPessoaNom, 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Using cursor H00DY4 */
            pr_default.execute(2, new Object[] {AV35WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A66ContratadaUsuario_ContratadaCod = H00DY4_A66ContratadaUsuario_ContratadaCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00DY4_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00DY4_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H00DY4_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H00DY4_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A1018ContratadaUsuario_UsuarioEhContratada = H00DY4_A1018ContratadaUsuario_UsuarioEhContratada[0];
               n1018ContratadaUsuario_UsuarioEhContratada = H00DY4_n1018ContratadaUsuario_UsuarioEhContratada[0];
               A69ContratadaUsuario_UsuarioCod = H00DY4_A69ContratadaUsuario_UsuarioCod[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00DY4_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00DY4_n71ContratadaUsuario_UsuarioPessoaNom[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H00DY4_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H00DY4_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00DY4_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00DY4_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A1018ContratadaUsuario_UsuarioEhContratada = H00DY4_A1018ContratadaUsuario_UsuarioEhContratada[0];
               n1018ContratadaUsuario_UsuarioEhContratada = H00DY4_n1018ContratadaUsuario_UsuarioEhContratada[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00DY4_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00DY4_n71ContratadaUsuario_UsuarioPessoaNom[0];
               cmbavContagemresultado_contadorfmcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)), A71ContratadaUsuario_UsuarioPessoaNom, 0);
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         else
         {
            /* Using cursor H00DY5 */
            pr_default.execute(3, new Object[] {AV35WWPContext.gxTpr_Contratada_pessoacod});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A70ContratadaUsuario_UsuarioPessoaCod = H00DY5_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00DY5_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A69ContratadaUsuario_UsuarioCod = H00DY5_A69ContratadaUsuario_UsuarioCod[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00DY5_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00DY5_n71ContratadaUsuario_UsuarioPessoaNom[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00DY5_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00DY5_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00DY5_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00DY5_n71ContratadaUsuario_UsuarioPessoaNom[0];
               cmbavContagemresultado_contadorfmcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)), A71ContratadaUsuario_UsuarioPessoaNom, 0);
               pr_default.readNext(3);
            }
            pr_default.close(3);
         }
         AV6ContagemResultado_ContadorFMCod = AV38ContadorFMCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContadorFMCod), 6, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E13DY2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_DY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; ") ;
            wb_table2_7_DY2( true) ;
         }
         else
         {
            wb_table2_7_DY2( false) ;
         }
         return  ;
      }

      protected void wb_table2_7_DY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXCHARTCONTROLContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DY2e( true) ;
         }
         else
         {
            wb_table1_2_DY2e( false) ;
         }
      }

      protected void wb_table2_7_DY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(50), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 10, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Contador:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaAreasGrf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_contadorfmcod, cmbavContagemresultado_contadorfmcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContadorFMCod), 6, 0)), 1, cmbavContagemresultado_contadorfmcod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,12);\"", "", true, "HLP_WP_ConsultaAreasGrf.htm");
            cmbavContagemresultado_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_ContadorFMCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_contadorfmcod_Internalname, "Values", (String)(cmbavContagemresultado_contadorfmcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Tipo:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaAreasGrf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavGraficar, cmbavGraficar_Internalname, StringUtil.RTrim( AV33Graficar), 1, cmbavGraficar_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "", true, "HLP_WP_ConsultaAreasGrf.htm");
            cmbavGraficar.CurrentValue = StringUtil.RTrim( AV33Graficar);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGraficar_Internalname, "Values", (String)(cmbavGraficar.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtngraficar_Internalname, "", "Graficar", bttBtngraficar_Jsonclick, 5, "Graficar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'GRAFICAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ConsultaAreasGrf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_7_DY2e( true) ;
         }
         else
         {
            wb_table2_7_DY2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV38ContadorFMCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ContadorFMCod), 6, 0)));
         AV39Servico_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Servico_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV39Servico_Codigo), "ZZZZZ9")));
         AV40UserEhContratada = (bool)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40UserEhContratada", AV40UserEhContratada);
         AV41UserEhContratante = (bool)getParm(obj,3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41UserEhContratante", AV41UserEhContratante);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADY2( ) ;
         WSDY2( ) ;
         WEDY2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423524732");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_consultaareasgrf.js", "?202032423524732");
         context.AddJavascriptSource("GxChart/gxChart.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         cmbavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         cmbavGraficar_Internalname = "vGRAFICAR";
         bttBtngraficar_Internalname = "BTNGRAFICAR";
         tblTable2_Internalname = "TABLE2";
         Gxchartcontrol_Internalname = "GXCHARTCONTROL";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavGraficar_Jsonclick = "";
         cmbavContagemresultado_contadorfmcod_Jsonclick = "";
         Gxchartcontrol_Graphcolor2 = (int)(0x808080);
         Gxchartcontrol_Graphcolor1 = (int)(0xFFFFFF);
         Gxchartcontrol_Backgroundcolor1 = (int)(0xD3D3D3);
         Gxchartcontrol_Y_axistitle = "";
         Gxchartcontrol_X_axistitle = "";
         Gxchartcontrol_Charttype = "column";
         Gxchartcontrol_Opacity = 255;
         Gxchartcontrol_Height = "500";
         Gxchartcontrol_Width = "900";
         Gxchartcontrol_Title = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Consulta de Produtividade";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'GRAFICAR'","{handler:'E12DY2',iparms:[{av:'GxChartDataNull',fld:'vGXCHARTDATANULL',pic:'',nv:null},{av:'GxChartSerieNull',fld:'vGXCHARTSERIENULL',pic:'',nv:null},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''},{av:'GxChartData',fld:'vGXCHARTDATA',pic:'',nv:null},{av:'AV33Graficar',fld:'vGRAFICAR',pic:'',nv:''},{av:'A474ContagemResultado_ContadorFMNom',fld:'CONTAGEMRESULTADO_CONTADORFMNOM',pic:'@!',nv:''},{av:'A597ContagemResultado_LoteAceiteCod',fld:'CONTAGEMRESULTADO_LOTEACEITECOD',pic:'ZZZZZ9',nv:0},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV6ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'A470ContagemResultado_ContadorFMCod',fld:'CONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV41UserEhContratante',fld:'vUSEREHCONTRATANTE',pic:'',nv:false},{av:'A999ContagemResultado_CrFMEhContratada',fld:'CONTAGEMRESULTADO_CRFMEHCONTRATADA',pic:'',nv:false},{av:'A1000ContagemResultado_CrFMEhContratante',fld:'CONTAGEMRESULTADO_CRFMEHCONTRATANTE',pic:'',nv:false},{av:'AV31DataIni',fld:'vDATAINI',pic:'',nv:''},{av:'AV32DataFim',fld:'vDATAFIM',pic:'',nv:''},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV39Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A517ContagemResultado_Ultima',fld:'CONTAGEMRESULTADO_ULTIMA',pic:'',nv:false},{av:'A574ContagemResultado_PFFinal',fld:'CONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A512ContagemResultado_ValorPF',fld:'CONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'GxChartData',fld:'vGXCHARTDATA',pic:'',nv:null},{av:'AV31DataIni',fld:'vDATAINI',pic:'',nv:''},{av:'AV32DataFim',fld:'vDATAFIM',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GxChartData = new SdtGxChart(context);
         GxChartDataNull = new SdtGxChart(context);
         GxChartSerieNull = new SdtGxChart_Serie(context);
         Gx_date = DateTime.MinValue;
         A474ContagemResultado_ContadorFMNom = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         AV31DataIni = DateTime.MinValue;
         AV32DataFim = DateTime.MinValue;
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV33Graficar = "";
         AV35WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GxChartSerie = new SdtGxChart_Serie(context);
         AV17mPFBFM = new decimal [12] ;
         AV14mCTPF = new decimal [12] ;
         scmdbuf = "";
         H00DY2_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00DY2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00DY2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00DY2_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         H00DY2_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         H00DY2_A517ContagemResultado_Ultima = new bool[] {false} ;
         H00DY2_A601ContagemResultado_Servico = new int[1] ;
         H00DY2_n601ContagemResultado_Servico = new bool[] {false} ;
         H00DY2_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         H00DY2_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         H00DY2_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00DY2_A1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         H00DY2_n1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         H00DY2_A999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         H00DY2_n999ContagemResultado_CrFMEhContratada = new bool[] {false} ;
         H00DY2_A470ContagemResultado_ContadorFMCod = new int[1] ;
         H00DY2_A512ContagemResultado_ValorPF = new decimal[1] ;
         H00DY2_n512ContagemResultado_ValorPF = new bool[] {false} ;
         H00DY2_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         H00DY2_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         H00DY2_A456ContagemResultado_Codigo = new int[1] ;
         AV30Data = DateTime.MinValue;
         H00DY3_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H00DY3_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00DY3_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00DY3_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00DY3_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00DY3_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         H00DY4_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00DY4_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00DY4_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00DY4_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00DY4_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00DY4_A1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         H00DY4_n1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         H00DY4_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00DY4_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00DY4_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         H00DY5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00DY5_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00DY5_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00DY5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00DY5_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00DY5_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         sStyleString = "";
         lblTextblock1_Jsonclick = "";
         TempTags = "";
         lblTextblock2_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtngraficar_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_consultaareasgrf__default(),
            new Object[][] {
                new Object[] {
               H00DY2_A511ContagemResultado_HoraCnt, H00DY2_A1553ContagemResultado_CntSrvCod, H00DY2_n1553ContagemResultado_CntSrvCod, H00DY2_A479ContagemResultado_CrFMPessoaCod, H00DY2_n479ContagemResultado_CrFMPessoaCod, H00DY2_A517ContagemResultado_Ultima, H00DY2_A601ContagemResultado_Servico, H00DY2_n601ContagemResultado_Servico, H00DY2_A597ContagemResultado_LoteAceiteCod, H00DY2_n597ContagemResultado_LoteAceiteCod,
               H00DY2_A473ContagemResultado_DataCnt, H00DY2_A1000ContagemResultado_CrFMEhContratante, H00DY2_n1000ContagemResultado_CrFMEhContratante, H00DY2_A999ContagemResultado_CrFMEhContratada, H00DY2_n999ContagemResultado_CrFMEhContratada, H00DY2_A470ContagemResultado_ContadorFMCod, H00DY2_A512ContagemResultado_ValorPF, H00DY2_n512ContagemResultado_ValorPF, H00DY2_A474ContagemResultado_ContadorFMNom, H00DY2_n474ContagemResultado_ContadorFMNom,
               H00DY2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00DY3_A61ContratanteUsuario_UsuarioPessoaCod, H00DY3_n61ContratanteUsuario_UsuarioPessoaCod, H00DY3_A63ContratanteUsuario_ContratanteCod, H00DY3_A60ContratanteUsuario_UsuarioCod, H00DY3_A62ContratanteUsuario_UsuarioPessoaNom, H00DY3_n62ContratanteUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00DY4_A66ContratadaUsuario_ContratadaCod, H00DY4_A70ContratadaUsuario_UsuarioPessoaCod, H00DY4_n70ContratadaUsuario_UsuarioPessoaCod, H00DY4_A1228ContratadaUsuario_AreaTrabalhoCod, H00DY4_n1228ContratadaUsuario_AreaTrabalhoCod, H00DY4_A1018ContratadaUsuario_UsuarioEhContratada, H00DY4_n1018ContratadaUsuario_UsuarioEhContratada, H00DY4_A69ContratadaUsuario_UsuarioCod, H00DY4_A71ContratadaUsuario_UsuarioPessoaNom, H00DY4_n71ContratadaUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00DY5_A66ContratadaUsuario_ContratadaCod, H00DY5_A70ContratadaUsuario_UsuarioPessoaCod, H00DY5_n70ContratadaUsuario_UsuarioPessoaCod, H00DY5_A69ContratadaUsuario_UsuarioCod, H00DY5_A71ContratadaUsuario_UsuarioPessoaNom, H00DY5_n71ContratadaUsuario_UsuarioPessoaNom
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV13i ;
      private short nGXWrapped ;
      private int AV38ContadorFMCod ;
      private int AV39Servico_Codigo ;
      private int wcpOAV38ContadorFMCod ;
      private int wcpOAV39Servico_Codigo ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A601ContagemResultado_Servico ;
      private int Gxchartcontrol_Opacity ;
      private int Gxchartcontrol_Backgroundcolor1 ;
      private int Gxchartcontrol_Graphcolor1 ;
      private int Gxchartcontrol_Graphcolor2 ;
      private int AV6ContagemResultado_ContadorFMCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A479ContagemResultado_CrFMPessoaCod ;
      private int A456ContagemResultado_Codigo ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int idxLst ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal [] AV17mPFBFM ;
      private decimal [] AV14mCTPF ;
      private decimal GXt_decimal1 ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A474ContagemResultado_ContadorFMNom ;
      private String Gxchartcontrol_Title ;
      private String Gxchartcontrol_Width ;
      private String Gxchartcontrol_Height ;
      private String Gxchartcontrol_Charttype ;
      private String Gxchartcontrol_X_axistitle ;
      private String Gxchartcontrol_Y_axistitle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV33Graficar ;
      private String cmbavContagemresultado_contadorfmcod_Internalname ;
      private String cmbavGraficar_Internalname ;
      private String scmdbuf ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String tblTable2_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String TempTags ;
      private String cmbavContagemresultado_contadorfmcod_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String cmbavGraficar_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtngraficar_Internalname ;
      private String bttBtngraficar_Jsonclick ;
      private String Gxchartcontrol_Internalname ;
      private DateTime Gx_date ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime AV31DataIni ;
      private DateTime AV32DataFim ;
      private DateTime AV30Data ;
      private bool AV40UserEhContratada ;
      private bool AV41UserEhContratante ;
      private bool wcpOAV40UserEhContratada ;
      private bool wcpOAV41UserEhContratante ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A999ContagemResultado_CrFMEhContratada ;
      private bool A1000ContagemResultado_CrFMEhContratante ;
      private bool A517ContagemResultado_Ultima ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n479ContagemResultado_CrFMPessoaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n1000ContagemResultado_CrFMEhContratante ;
      private bool n999ContagemResultado_CrFMEhContratada ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n474ContagemResultado_ContadorFMNom ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool n1018ContratadaUsuario_UsuarioEhContratada ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContadorFMCod ;
      private bool aP2_UserEhContratada ;
      private bool aP3_UserEhContratante ;
      private GXCombobox cmbavContagemresultado_contadorfmcod ;
      private GXCombobox cmbavGraficar ;
      private IDataStoreProvider pr_default ;
      private String[] H00DY2_A511ContagemResultado_HoraCnt ;
      private int[] H00DY2_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00DY2_n1553ContagemResultado_CntSrvCod ;
      private int[] H00DY2_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] H00DY2_n479ContagemResultado_CrFMPessoaCod ;
      private bool[] H00DY2_A517ContagemResultado_Ultima ;
      private int[] H00DY2_A601ContagemResultado_Servico ;
      private bool[] H00DY2_n601ContagemResultado_Servico ;
      private int[] H00DY2_A597ContagemResultado_LoteAceiteCod ;
      private bool[] H00DY2_n597ContagemResultado_LoteAceiteCod ;
      private DateTime[] H00DY2_A473ContagemResultado_DataCnt ;
      private bool[] H00DY2_A1000ContagemResultado_CrFMEhContratante ;
      private bool[] H00DY2_n1000ContagemResultado_CrFMEhContratante ;
      private bool[] H00DY2_A999ContagemResultado_CrFMEhContratada ;
      private bool[] H00DY2_n999ContagemResultado_CrFMEhContratada ;
      private int[] H00DY2_A470ContagemResultado_ContadorFMCod ;
      private decimal[] H00DY2_A512ContagemResultado_ValorPF ;
      private bool[] H00DY2_n512ContagemResultado_ValorPF ;
      private String[] H00DY2_A474ContagemResultado_ContadorFMNom ;
      private bool[] H00DY2_n474ContagemResultado_ContadorFMNom ;
      private int[] H00DY2_A456ContagemResultado_Codigo ;
      private int[] H00DY3_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00DY3_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H00DY3_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00DY3_A60ContratanteUsuario_UsuarioCod ;
      private String[] H00DY3_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H00DY3_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] H00DY4_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00DY4_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00DY4_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00DY4_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00DY4_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00DY4_A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] H00DY4_n1018ContratadaUsuario_UsuarioEhContratada ;
      private int[] H00DY4_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00DY4_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00DY4_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00DY5_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00DY5_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00DY5_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00DY5_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00DY5_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00DY5_n71ContratadaUsuario_UsuarioPessoaNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private SdtGxChart GxChartData ;
      private SdtGxChart GxChartDataNull ;
      private SdtGxChart_Serie GxChartSerieNull ;
      private SdtGxChart_Serie GxChartSerie ;
      private wwpbaseobjects.SdtWWPContext AV35WWPContext ;
   }

   public class wp_consultaareasgrf__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00DY2( IGxContext context ,
                                             int AV6ContagemResultado_ContadorFMCod ,
                                             bool AV41UserEhContratante ,
                                             int AV39Servico_Codigo ,
                                             int A470ContagemResultado_ContadorFMCod ,
                                             bool A999ContagemResultado_CrFMEhContratada ,
                                             bool A1000ContagemResultado_CrFMEhContratante ,
                                             int A601ContagemResultado_Servico ,
                                             DateTime A473ContagemResultado_DataCnt ,
                                             DateTime AV31DataIni ,
                                             DateTime AV32DataFim ,
                                             int A597ContagemResultado_LoteAceiteCod ,
                                             bool A517ContagemResultado_Ultima )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [4] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_HoraCnt], T4.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, T1.[ContagemResultado_Ultima], T5.[Servico_Codigo] AS ContagemResultado_Servico, T4.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_DataCnt], T2.[Usuario_EhContratante] AS ContagemResultado_CrFMEhContratante, T2.[Usuario_EhContratada] AS ContagemResultado_CrFMEhContratada, T1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, T4.[ContagemResultado_ValorPF], T3.[Pessoa_Nome] AS ContagemResultado_ContadorFMNom, T1.[ContagemResultado_Codigo] FROM (((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T4.[ContagemResultado_CntSrvCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_DataCnt] >= @AV31DataIni)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_DataCnt] <= @AV32DataFim)";
         scmdbuf = scmdbuf + " and (Not (T4.[ContagemResultado_LoteAceiteCod] = convert(int, 0)))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Ultima] = 1)";
         if ( ! (0==AV6ContagemResultado_ContadorFMCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContadorFMCod] = @AV6ContagemResultado_ContadorFMCod)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! AV41UserEhContratante )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_EhContratada] = 1)";
         }
         if ( AV41UserEhContratante )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_EhContratante] = 1)";
         }
         if ( ! (0==AV39Servico_Codigo) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Codigo] = @AV39Servico_Codigo)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( (0==AV6ContagemResultado_ContadorFMCod) )
         {
            scmdbuf = scmdbuf + " ORDER BY T4.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_DataCnt]";
         }
         else if ( ! (0==AV6ContagemResultado_ContadorFMCod) )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome], T4.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_DataCnt]";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt]";
         }
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00DY2(context, (int)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (bool)dynConstraints[5] , (int)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (int)dynConstraints[10] , (bool)dynConstraints[11] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DY3 ;
          prmH00DY3 = new Object[] {
          new Object[] {"@AV35WWPC_1Contratante_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DY4 ;
          prmH00DY4 = new Object[] {
          new Object[] {"@AV35WWPC_2Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DY5 ;
          prmH00DY5 = new Object[] {
          new Object[] {"@AV35WWPC_3Contratada_pessoaco",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00DY2 ;
          prmH00DY2 = new Object[] {
          new Object[] {"@AV31DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV32DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV6ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DY2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DY2,100,0,true,false )
             ,new CursorDef("H00DY3", "SELECT T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPess, T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @AV35WWPC_1Contratante_codigo ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DY3,100,0,false,false )
             ,new CursorDef("H00DY4", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T3.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPesso, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T3.[Usuario_EhContratada] AS ContratadaUsuario_UsuarioEhCon, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE (Not T3.[Usuario_EhContratada] = 1) AND (T2.[Contratada_AreaTrabalhoCod] = @AV35WWPC_2Areatrabalho_codigo) ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DY4,100,0,false,false )
             ,new CursorDef("H00DY5", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T2.[Usuario_PessoaCod] = @AV35WWPC_3Contratada_pessoaco ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DY5,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((bool[]) buf[13])[0] = rslt.getBool(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((decimal[]) buf[16])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 100) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
