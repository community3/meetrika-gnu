/*
               File: GetContratoContratoDadosCertameWCFilterData
        Description: Get Contrato Contrato Dados Certame WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/28/2019 16:48:33.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratocontratodadoscertamewcfilterdata : GXProcedure
   {
      public getcontratocontratodadoscertamewcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratocontratodadoscertamewcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratocontratodadoscertamewcfilterdata objgetcontratocontratodadoscertamewcfilterdata;
         objgetcontratocontratodadoscertamewcfilterdata = new getcontratocontratodadoscertamewcfilterdata();
         objgetcontratocontratodadoscertamewcfilterdata.AV24DDOName = aP0_DDOName;
         objgetcontratocontratodadoscertamewcfilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetcontratocontratodadoscertamewcfilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratocontratodadoscertamewcfilterdata.AV28OptionsJson = "" ;
         objgetcontratocontratodadoscertamewcfilterdata.AV31OptionsDescJson = "" ;
         objgetcontratocontratodadoscertamewcfilterdata.AV33OptionIndexesJson = "" ;
         objgetcontratocontratodadoscertamewcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratocontratodadoscertamewcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratocontratodadoscertamewcfilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratocontratodadoscertamewcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATODADOSCERTAME_MODALIDADE") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATODADOSCERTAME_MODALIDADEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("ContratoContratoDadosCertameWCGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoContratoDadosCertameWCGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("ContratoContratoDadosCertameWCGridState"), "");
         }
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV43GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_MODALIDADE") == 0 )
            {
               AV10TFContratoDadosCertame_Modalidade = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_MODALIDADE_SEL") == 0 )
            {
               AV11TFContratoDadosCertame_Modalidade_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_NUMERO") == 0 )
            {
               AV12TFContratoDadosCertame_Numero = (long)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContratoDadosCertame_Numero_To = (long)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_UASG") == 0 )
            {
               AV14TFContratoDadosCertame_Uasg = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV15TFContratoDadosCertame_Uasg_To = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_DATA") == 0 )
            {
               AV16TFContratoDadosCertame_Data = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Value, 2);
               AV17TFContratoDadosCertame_Data_To = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_DATAHOMOLOGACAO") == 0 )
            {
               AV18TFContratoDadosCertame_DataHomologacao = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Value, 2);
               AV19TFContratoDadosCertame_DataHomologacao_To = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_DATAADJUDICACAO") == 0 )
            {
               AV20TFContratoDadosCertame_DataAdjudicacao = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Value, 2);
               AV21TFContratoDadosCertame_DataAdjudicacao_To = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATO_CODIGO") == 0 )
            {
               AV40Contrato_Codigo = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
            }
            AV43GXV1 = (int)(AV43GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATODADOSCERTAME_MODALIDADEOPTIONS' Routine */
         AV10TFContratoDadosCertame_Modalidade = AV22SearchTxt;
         AV11TFContratoDadosCertame_Modalidade_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFContratoDadosCertame_Modalidade_Sel ,
                                              AV10TFContratoDadosCertame_Modalidade ,
                                              AV12TFContratoDadosCertame_Numero ,
                                              AV13TFContratoDadosCertame_Numero_To ,
                                              AV14TFContratoDadosCertame_Uasg ,
                                              AV15TFContratoDadosCertame_Uasg_To ,
                                              AV16TFContratoDadosCertame_Data ,
                                              AV17TFContratoDadosCertame_Data_To ,
                                              AV18TFContratoDadosCertame_DataHomologacao ,
                                              AV19TFContratoDadosCertame_DataHomologacao_To ,
                                              AV20TFContratoDadosCertame_DataAdjudicacao ,
                                              AV21TFContratoDadosCertame_DataAdjudicacao_To ,
                                              A307ContratoDadosCertame_Modalidade ,
                                              A308ContratoDadosCertame_Numero ,
                                              A310ContratoDadosCertame_Uasg ,
                                              A311ContratoDadosCertame_Data ,
                                              A312ContratoDadosCertame_DataHomologacao ,
                                              A313ContratoDadosCertame_DataAdjudicacao ,
                                              AV40Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.LONG, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFContratoDadosCertame_Modalidade = StringUtil.Concat( StringUtil.RTrim( AV10TFContratoDadosCertame_Modalidade), "%", "");
         /* Using cursor P00UF2 */
         pr_default.execute(0, new Object[] {AV40Contrato_Codigo, lV10TFContratoDadosCertame_Modalidade, AV11TFContratoDadosCertame_Modalidade_Sel, AV12TFContratoDadosCertame_Numero, AV13TFContratoDadosCertame_Numero_To, AV14TFContratoDadosCertame_Uasg, AV15TFContratoDadosCertame_Uasg_To, AV16TFContratoDadosCertame_Data, AV17TFContratoDadosCertame_Data_To, AV18TFContratoDadosCertame_DataHomologacao, AV19TFContratoDadosCertame_DataHomologacao_To, AV20TFContratoDadosCertame_DataAdjudicacao, AV21TFContratoDadosCertame_DataAdjudicacao_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUF2 = false;
            A74Contrato_Codigo = P00UF2_A74Contrato_Codigo[0];
            A307ContratoDadosCertame_Modalidade = P00UF2_A307ContratoDadosCertame_Modalidade[0];
            A313ContratoDadosCertame_DataAdjudicacao = P00UF2_A313ContratoDadosCertame_DataAdjudicacao[0];
            n313ContratoDadosCertame_DataAdjudicacao = P00UF2_n313ContratoDadosCertame_DataAdjudicacao[0];
            A312ContratoDadosCertame_DataHomologacao = P00UF2_A312ContratoDadosCertame_DataHomologacao[0];
            n312ContratoDadosCertame_DataHomologacao = P00UF2_n312ContratoDadosCertame_DataHomologacao[0];
            A311ContratoDadosCertame_Data = P00UF2_A311ContratoDadosCertame_Data[0];
            A310ContratoDadosCertame_Uasg = P00UF2_A310ContratoDadosCertame_Uasg[0];
            n310ContratoDadosCertame_Uasg = P00UF2_n310ContratoDadosCertame_Uasg[0];
            A308ContratoDadosCertame_Numero = P00UF2_A308ContratoDadosCertame_Numero[0];
            n308ContratoDadosCertame_Numero = P00UF2_n308ContratoDadosCertame_Numero[0];
            A314ContratoDadosCertame_Codigo = P00UF2_A314ContratoDadosCertame_Codigo[0];
            AV34count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00UF2_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( StringUtil.StrCmp(P00UF2_A307ContratoDadosCertame_Modalidade[0], A307ContratoDadosCertame_Modalidade) == 0 ) )
            {
               BRKUF2 = false;
               A314ContratoDadosCertame_Codigo = P00UF2_A314ContratoDadosCertame_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKUF2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A307ContratoDadosCertame_Modalidade)) )
            {
               AV26Option = A307ContratoDadosCertame_Modalidade;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUF2 )
            {
               BRKUF2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratoDadosCertame_Modalidade = "";
         AV11TFContratoDadosCertame_Modalidade_Sel = "";
         AV16TFContratoDadosCertame_Data = DateTime.MinValue;
         AV17TFContratoDadosCertame_Data_To = DateTime.MinValue;
         AV18TFContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         AV19TFContratoDadosCertame_DataHomologacao_To = DateTime.MinValue;
         AV20TFContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         AV21TFContratoDadosCertame_DataAdjudicacao_To = DateTime.MinValue;
         scmdbuf = "";
         lV10TFContratoDadosCertame_Modalidade = "";
         A307ContratoDadosCertame_Modalidade = "";
         A311ContratoDadosCertame_Data = DateTime.MinValue;
         A312ContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         A313ContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         P00UF2_A74Contrato_Codigo = new int[1] ;
         P00UF2_A307ContratoDadosCertame_Modalidade = new String[] {""} ;
         P00UF2_A313ContratoDadosCertame_DataAdjudicacao = new DateTime[] {DateTime.MinValue} ;
         P00UF2_n313ContratoDadosCertame_DataAdjudicacao = new bool[] {false} ;
         P00UF2_A312ContratoDadosCertame_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00UF2_n312ContratoDadosCertame_DataHomologacao = new bool[] {false} ;
         P00UF2_A311ContratoDadosCertame_Data = new DateTime[] {DateTime.MinValue} ;
         P00UF2_A310ContratoDadosCertame_Uasg = new short[1] ;
         P00UF2_n310ContratoDadosCertame_Uasg = new bool[] {false} ;
         P00UF2_A308ContratoDadosCertame_Numero = new long[1] ;
         P00UF2_n308ContratoDadosCertame_Numero = new bool[] {false} ;
         P00UF2_A314ContratoDadosCertame_Codigo = new int[1] ;
         AV26Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratocontratodadoscertamewcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UF2_A74Contrato_Codigo, P00UF2_A307ContratoDadosCertame_Modalidade, P00UF2_A313ContratoDadosCertame_DataAdjudicacao, P00UF2_n313ContratoDadosCertame_DataAdjudicacao, P00UF2_A312ContratoDadosCertame_DataHomologacao, P00UF2_n312ContratoDadosCertame_DataHomologacao, P00UF2_A311ContratoDadosCertame_Data, P00UF2_A310ContratoDadosCertame_Uasg, P00UF2_n310ContratoDadosCertame_Uasg, P00UF2_A308ContratoDadosCertame_Numero,
               P00UF2_n308ContratoDadosCertame_Numero, P00UF2_A314ContratoDadosCertame_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14TFContratoDadosCertame_Uasg ;
      private short AV15TFContratoDadosCertame_Uasg_To ;
      private short A310ContratoDadosCertame_Uasg ;
      private int AV43GXV1 ;
      private int AV40Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A314ContratoDadosCertame_Codigo ;
      private long AV12TFContratoDadosCertame_Numero ;
      private long AV13TFContratoDadosCertame_Numero_To ;
      private long A308ContratoDadosCertame_Numero ;
      private long AV34count ;
      private String scmdbuf ;
      private DateTime AV16TFContratoDadosCertame_Data ;
      private DateTime AV17TFContratoDadosCertame_Data_To ;
      private DateTime AV18TFContratoDadosCertame_DataHomologacao ;
      private DateTime AV19TFContratoDadosCertame_DataHomologacao_To ;
      private DateTime AV20TFContratoDadosCertame_DataAdjudicacao ;
      private DateTime AV21TFContratoDadosCertame_DataAdjudicacao_To ;
      private DateTime A311ContratoDadosCertame_Data ;
      private DateTime A312ContratoDadosCertame_DataHomologacao ;
      private DateTime A313ContratoDadosCertame_DataAdjudicacao ;
      private bool returnInSub ;
      private bool BRKUF2 ;
      private bool n313ContratoDadosCertame_DataAdjudicacao ;
      private bool n312ContratoDadosCertame_DataHomologacao ;
      private bool n310ContratoDadosCertame_Uasg ;
      private bool n308ContratoDadosCertame_Numero ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV10TFContratoDadosCertame_Modalidade ;
      private String AV11TFContratoDadosCertame_Modalidade_Sel ;
      private String lV10TFContratoDadosCertame_Modalidade ;
      private String A307ContratoDadosCertame_Modalidade ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00UF2_A74Contrato_Codigo ;
      private String[] P00UF2_A307ContratoDadosCertame_Modalidade ;
      private DateTime[] P00UF2_A313ContratoDadosCertame_DataAdjudicacao ;
      private bool[] P00UF2_n313ContratoDadosCertame_DataAdjudicacao ;
      private DateTime[] P00UF2_A312ContratoDadosCertame_DataHomologacao ;
      private bool[] P00UF2_n312ContratoDadosCertame_DataHomologacao ;
      private DateTime[] P00UF2_A311ContratoDadosCertame_Data ;
      private short[] P00UF2_A310ContratoDadosCertame_Uasg ;
      private bool[] P00UF2_n310ContratoDadosCertame_Uasg ;
      private long[] P00UF2_A308ContratoDadosCertame_Numero ;
      private bool[] P00UF2_n308ContratoDadosCertame_Numero ;
      private int[] P00UF2_A314ContratoDadosCertame_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
   }

   public class getcontratocontratodadoscertamewcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UF2( IGxContext context ,
                                             String AV11TFContratoDadosCertame_Modalidade_Sel ,
                                             String AV10TFContratoDadosCertame_Modalidade ,
                                             long AV12TFContratoDadosCertame_Numero ,
                                             long AV13TFContratoDadosCertame_Numero_To ,
                                             short AV14TFContratoDadosCertame_Uasg ,
                                             short AV15TFContratoDadosCertame_Uasg_To ,
                                             DateTime AV16TFContratoDadosCertame_Data ,
                                             DateTime AV17TFContratoDadosCertame_Data_To ,
                                             DateTime AV18TFContratoDadosCertame_DataHomologacao ,
                                             DateTime AV19TFContratoDadosCertame_DataHomologacao_To ,
                                             DateTime AV20TFContratoDadosCertame_DataAdjudicacao ,
                                             DateTime AV21TFContratoDadosCertame_DataAdjudicacao_To ,
                                             String A307ContratoDadosCertame_Modalidade ,
                                             long A308ContratoDadosCertame_Numero ,
                                             short A310ContratoDadosCertame_Uasg ,
                                             DateTime A311ContratoDadosCertame_Data ,
                                             DateTime A312ContratoDadosCertame_DataHomologacao ,
                                             DateTime A313ContratoDadosCertame_DataAdjudicacao ,
                                             int AV40Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [13] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Contrato_Codigo], [ContratoDadosCertame_Modalidade], [ContratoDadosCertame_DataAdjudicacao], [ContratoDadosCertame_DataHomologacao], [ContratoDadosCertame_Data], [ContratoDadosCertame_Uasg], [ContratoDadosCertame_Numero], [ContratoDadosCertame_Codigo] FROM [ContratoDadosCertame] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contrato_Codigo] = @AV40Contrato_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoDadosCertame_Modalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratoDadosCertame_Modalidade)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoDadosCertame_Modalidade] like @lV10TFContratoDadosCertame_Modalidade)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoDadosCertame_Modalidade_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoDadosCertame_Modalidade] = @AV11TFContratoDadosCertame_Modalidade_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (0==AV12TFContratoDadosCertame_Numero) )
         {
            sWhereString = sWhereString + " and ([ContratoDadosCertame_Numero] >= @AV12TFContratoDadosCertame_Numero)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV13TFContratoDadosCertame_Numero_To) )
         {
            sWhereString = sWhereString + " and ([ContratoDadosCertame_Numero] <= @AV13TFContratoDadosCertame_Numero_To)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV14TFContratoDadosCertame_Uasg) )
         {
            sWhereString = sWhereString + " and ([ContratoDadosCertame_Uasg] >= @AV14TFContratoDadosCertame_Uasg)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV15TFContratoDadosCertame_Uasg_To) )
         {
            sWhereString = sWhereString + " and ([ContratoDadosCertame_Uasg] <= @AV15TFContratoDadosCertame_Uasg_To)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (DateTime.MinValue==AV16TFContratoDadosCertame_Data) )
         {
            sWhereString = sWhereString + " and ([ContratoDadosCertame_Data] >= @AV16TFContratoDadosCertame_Data)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (DateTime.MinValue==AV17TFContratoDadosCertame_Data_To) )
         {
            sWhereString = sWhereString + " and ([ContratoDadosCertame_Data] <= @AV17TFContratoDadosCertame_Data_To)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFContratoDadosCertame_DataHomologacao) )
         {
            sWhereString = sWhereString + " and ([ContratoDadosCertame_DataHomologacao] >= @AV18TFContratoDadosCertame_DataHomologacao)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV19TFContratoDadosCertame_DataHomologacao_To) )
         {
            sWhereString = sWhereString + " and ([ContratoDadosCertame_DataHomologacao] <= @AV19TFContratoDadosCertame_DataHomologacao_To)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV20TFContratoDadosCertame_DataAdjudicacao) )
         {
            sWhereString = sWhereString + " and ([ContratoDadosCertame_DataAdjudicacao] >= @AV20TFContratoDadosCertame_DataAdjudicacao)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV21TFContratoDadosCertame_DataAdjudicacao_To) )
         {
            sWhereString = sWhereString + " and ([ContratoDadosCertame_DataAdjudicacao] <= @AV21TFContratoDadosCertame_DataAdjudicacao_To)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Contrato_Codigo], [ContratoDadosCertame_Modalidade]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UF2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (long)dynConstraints[2] , (long)dynConstraints[3] , (short)dynConstraints[4] , (short)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (String)dynConstraints[12] , (long)dynConstraints[13] , (short)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UF2 ;
          prmP00UF2 = new Object[] {
          new Object[] {"@AV40Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratoDadosCertame_Modalidade",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV11TFContratoDadosCertame_Modalidade_Sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV12TFContratoDadosCertame_Numero",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV13TFContratoDadosCertame_Numero_To",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV14TFContratoDadosCertame_Uasg",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV15TFContratoDadosCertame_Uasg_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV16TFContratoDadosCertame_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17TFContratoDadosCertame_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV18TFContratoDadosCertame_DataHomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV19TFContratoDadosCertame_DataHomologacao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20TFContratoDadosCertame_DataAdjudicacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21TFContratoDadosCertame_DataAdjudicacao_To",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UF2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UF2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((long[]) buf[9])[0] = rslt.getLong(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratocontratodadoscertamewcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratocontratodadoscertamewcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratocontratodadoscertamewcfilterdata") )
          {
             return  ;
          }
          getcontratocontratodadoscertamewcfilterdata worker = new getcontratocontratodadoscertamewcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
