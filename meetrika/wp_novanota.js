/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:18:40.32
*/
gx.evt.autoSkip = false;
gx.define('wp_novanota', false, function () {
   this.ServerClass =  "wp_novanota" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV42Codigos=gx.fn.getControlValue("vCODIGOS") ;
      this.AV41ContagemResultadoNotas=gx.fn.getControlValue("vCONTAGEMRESULTADONOTAS") ;
      this.AV47ContagemResultado_StatusUltCnt=gx.fn.getIntegerValue("vCONTAGEMRESULTADO_STATUSULTCNT",'.') ;
      this.AV34WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.AV46ContagemResultado_Codigo=gx.fn.getIntegerValue("vCONTAGEMRESULTADO_CODIGO",'.') ;
   };
   this.Validv_Datahora=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDATAHORA");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV37DataHora)==0) || new gx.date.gxdate( this.AV37DataHora ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Data Hora fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("BTNENTER","Visible", (this.AV34WWPContext.Insert) );
   };
   this.e12jz2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e13jz2_client=function()
   {
      this.executeServerEvent("'DOFECHAR'", false, null, false, false);
   };
   this.e16jz2_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,13,15,17,20,22,25,33];
   this.GXLastCtrlId =33;
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[11]={fld:"TEXTBLOCKDATAHORA", format:0,grid:0};
   GXValidFnc[13]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Datahora,isvalid:null,rgrid:[],fld:"vDATAHORA",gxz:"ZV37DataHora",gxold:"OV37DataHora",gxvar:"AV37DataHora",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[13],ip:[13],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV37DataHora=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV37DataHora=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDATAHORA",gx.O.AV37DataHora,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV37DataHora=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDATAHORA")},nac:gx.falseFn};
   this.declareDomainHdlr( 13 , function() {
   });
   GXValidFnc[15]={fld:"TEXTBLOCKUSUARIO_CODIGO", format:0,grid:0};
   GXValidFnc[17]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vUSUARIO_CODIGO",gxz:"ZV36Usuario_Codigo",gxold:"OV36Usuario_Codigo",gxvar:"AV36Usuario_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV36Usuario_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV36Usuario_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vUSUARIO_CODIGO",gx.O.AV36Usuario_Codigo)},c2v:function(){if(this.val()!==undefined)gx.O.AV36Usuario_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vUSUARIO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[20]={fld:"TEXTBLOCKNOTA", format:0,grid:0};
   GXValidFnc[22]={lvl:0,type:"vchar",len:2097152,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vNOTA",gxz:"ZV38Nota",gxold:"OV38Nota",gxvar:"AV38Nota",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV38Nota=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV38Nota=Value},v2c:function(){gx.fn.setControlValue("vNOTA",gx.O.AV38Nota,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV38Nota=this.val()},val:function(){return gx.fn.getControlValue("vNOTA")},nac:gx.falseFn};
   GXValidFnc[25]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[33]={fld:"TBJAVA", format:1,grid:0};
   this.AV37DataHora = gx.date.nullDate() ;
   this.ZV37DataHora = gx.date.nullDate() ;
   this.OV37DataHora = gx.date.nullDate() ;
   this.AV36Usuario_Codigo = 0 ;
   this.ZV36Usuario_Codigo = 0 ;
   this.OV36Usuario_Codigo = 0 ;
   this.AV38Nota = "" ;
   this.ZV38Nota = "" ;
   this.OV38Nota = "" ;
   this.AV37DataHora = gx.date.nullDate() ;
   this.AV36Usuario_Codigo = 0 ;
   this.AV38Nota = "" ;
   this.AV46ContagemResultado_Codigo = 0 ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.A803ContagemResultado_ContratadaSigla = "" ;
   this.A457ContagemResultado_Demanda = "" ;
   this.A531ContagemResultado_StatusUltCnt = 0 ;
   this.A490ContagemResultado_ContratadaCod = 0 ;
   this.AV42Codigos = [ ] ;
   this.AV41ContagemResultadoNotas = {} ;
   this.AV47ContagemResultado_StatusUltCnt = 0 ;
   this.AV34WWPContext = {} ;
   this.Events = {"e12jz2_client": ["ENTER", true] ,"e13jz2_client": ["'DOFECHAR'", true] ,"e16jz2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'AV47ContagemResultado_StatusUltCnt',fld:'vCONTAGEMRESULTADO_STATUSULTCNT',pic:'Z9',nv:0},{av:'AV34WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],[{av:'AV34WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV37DataHora',fld:'vDATAHORA',pic:'',hsh:true,nv:''},{av:'AV36Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'vUSUARIO_CODIGO'},{ctrl:'BTNENTER',prop:'Visible'}]];
   this.EvtParms["ENTER"] = [[{av:'AV38Nota',fld:'vNOTA',pic:'',nv:''},{av:'AV42Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV41ContagemResultadoNotas',fld:'vCONTAGEMRESULTADONOTAS',pic:'',nv:null},{av:'AV36Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV41ContagemResultadoNotas',fld:'vCONTAGEMRESULTADONOTAS',pic:'',nv:null}]];
   this.EvtParms["'DOFECHAR'"] = [[],[]];
   this.EnterCtrl = ["BTNENTER"];
   this.setVCMap("AV42Codigos", "vCODIGOS", 0, "Collint");
   this.setVCMap("AV41ContagemResultadoNotas", "vCONTAGEMRESULTADONOTAS", 0, "ContagemResultadoNotas");
   this.setVCMap("AV47ContagemResultado_StatusUltCnt", "vCONTAGEMRESULTADO_STATUSULTCNT", 0, "int");
   this.setVCMap("AV34WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV46ContagemResultado_Codigo", "vCONTAGEMRESULTADO_CODIGO", 0, "int");
   this.InitStandaloneVars( );
});
gx.createParentObj(wp_novanota);
