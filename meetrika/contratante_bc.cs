/*
               File: Contratante_BC
        Description: Contratante
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:15:18.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Mail;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratante_bc : GXHttpHandler, IGxSilentTrn
   {
      public contratante_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratante_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow05124( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey05124( ) ;
         standaloneModal( ) ;
         AddRow05124( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11052 */
            E11052 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z29Contratante_Codigo = A29Contratante_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_050( )
      {
         BeforeValidate05124( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls05124( ) ;
            }
            else
            {
               CheckExtendedTable05124( ) ;
               if ( AnyError == 0 )
               {
                  ZM05124( 25) ;
                  ZM05124( 26) ;
               }
               CloseExtendedTableCursors05124( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12052( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV45Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV46GXV1 = 1;
            while ( AV46GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV46GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Contratante_PessoaCod") == 0 )
               {
                  AV16Insert_Contratante_PessoaCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Municipio_Codigo") == 0 )
               {
                  AV17Insert_Municipio_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV46GXV1 = (int)(AV46GXV1+1);
            }
         }
      }

      protected void E11052( )
      {
         /* After Trn Routine */
         new wwpbaseobjects.audittransaction(context ).execute(  AV29AuditingObject,  AV45Pgmname) ;
         new wwpbaseobjects.audittransaction(context ).execute(  AV29AuditingObject,  AV45Pgmname) ;
      }

      protected void E13052( )
      {
         /* 'DoTestarEmail' Routine */
         AV37EmailText = "Teste de envio de email";
         AV38Subject = " - " + "Teste de envio de email (No reply)";
         AV34Usuarios.Clear();
         AV34Usuarios.Add(AV8WWPContext.gxTpr_Userid, 0);
         new prc_enviaremail(context ).execute(  AV8WWPContext.gxTpr_Areatrabalho_codigo,  AV34Usuarios,  AV38Subject,  AV37EmailText,  AV35Attachments, ref  AV36Resultado) ;
         GX_msglist.addItem(AV36Resultado);
      }

      protected void E14052( )
      {
         /* 'DoTesteCSharp' Routine */
         if ( (0==AV43retorno) )
         {
            GX_msglist.addItem("Teste N�O conlu�do!");
         }
         else
         {
            GX_msglist.addItem("Envio com sucesso!");
         }
      }

      protected void E15052( )
      {
         /* 'DoTestarEmail2' Routine */
         AV41SMTPSession.ErrDisplay = 1;
         AV19MailRecipient.Name = "daniel.rodriguez@efor.com.br";
         AV19MailRecipient.Address = "daniel.rodriguez@efor.com.br";
         AV39Email.Clear();
         AV39Email.To.Add(AV19MailRecipient) ;
         AV39Email.Subject = "Teste de envio de email";
         AV39Email.Text = "Teste de envio de email";
         AV41SMTPSession.Sender.Name = "meetrika@eficaciaorganizacao.com.br";
         AV41SMTPSession.Sender.Address = "meetrika@eficaciaorganizacao.com.br";
         AV41SMTPSession.Host = "192.168.11.25";
         AV41SMTPSession.UserName = "meetrika@eficaciaorganizacao.com.br";
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(A549Contratante_EmailSdaPass, O549Contratante_EmailSdaPass) != 0 ) )
         {
            AV41SMTPSession.Password = StringUtil.Trim( A549Contratante_EmailSdaPass);
         }
         else
         {
            AV41SMTPSession.Password = Crypto.Decrypt64( A549Contratante_EmailSdaPass, A550Contratante_EmailSdaKey);
         }
         AV41SMTPSession.Port = 25;
         AV41SMTPSession.Authentication = 0;
         AV41SMTPSession.Secure = 0;
         AV41SMTPSession.Login();
         if ( AV41SMTPSession.ErrCode != 0 )
         {
            GX_msglist.addItem("Retorno do Login: "+AV41SMTPSession.ErrDescription);
         }
         else
         {
            GX_msglist.addItem("Login bem sucedido!");
         }
         AV41SMTPSession.Send(AV39Email);
         if ( AV41SMTPSession.ErrCode != 0 )
         {
            GX_msglist.addItem("Retorno do Send: "+AV41SMTPSession.ErrDescription+": "+A548Contratante_EmailSdaUser);
         }
         else
         {
            GX_msglist.addItem("Mensagem enviada com sucesso!");
         }
         AV41SMTPSession.Logout();
      }

      protected void ZM05124( short GX_JID )
      {
         if ( ( GX_JID == 24 ) || ( GX_JID == 0 ) )
         {
            Z550Contratante_EmailSdaKey = A550Contratante_EmailSdaKey;
            Z549Contratante_EmailSdaPass = A549Contratante_EmailSdaPass;
            Z10Contratante_NomeFantasia = A10Contratante_NomeFantasia;
            Z11Contratante_IE = A11Contratante_IE;
            Z13Contratante_WebSite = A13Contratante_WebSite;
            Z14Contratante_Email = A14Contratante_Email;
            Z31Contratante_Telefone = A31Contratante_Telefone;
            Z32Contratante_Ramal = A32Contratante_Ramal;
            Z33Contratante_Fax = A33Contratante_Fax;
            Z336Contratante_AgenciaNome = A336Contratante_AgenciaNome;
            Z337Contratante_AgenciaNro = A337Contratante_AgenciaNro;
            Z338Contratante_BancoNome = A338Contratante_BancoNome;
            Z339Contratante_BancoNro = A339Contratante_BancoNro;
            Z16Contratante_ContaCorrente = A16Contratante_ContaCorrente;
            Z30Contratante_Ativo = A30Contratante_Ativo;
            Z547Contratante_EmailSdaHost = A547Contratante_EmailSdaHost;
            Z548Contratante_EmailSdaUser = A548Contratante_EmailSdaUser;
            Z551Contratante_EmailSdaAut = A551Contratante_EmailSdaAut;
            Z552Contratante_EmailSdaPort = A552Contratante_EmailSdaPort;
            Z1048Contratante_EmailSdaSec = A1048Contratante_EmailSdaSec;
            Z593Contratante_OSAutomatica = A593Contratante_OSAutomatica;
            Z1652Contratante_SSAutomatica = A1652Contratante_SSAutomatica;
            Z594Contratante_ExisteConferencia = A594Contratante_ExisteConferencia;
            Z1448Contratante_InicioDoExpediente = A1448Contratante_InicioDoExpediente;
            Z1192Contratante_FimDoExpediente = A1192Contratante_FimDoExpediente;
            Z1727Contratante_ServicoSSPadrao = A1727Contratante_ServicoSSPadrao;
            Z1729Contratante_RetornaSS = A1729Contratante_RetornaSS;
            Z1732Contratante_SSStatusPlanejamento = A1732Contratante_SSStatusPlanejamento;
            Z1733Contratante_SSPrestadoraUnica = A1733Contratante_SSPrestadoraUnica;
            Z1738Contratante_PropostaRqrSrv = A1738Contratante_PropostaRqrSrv;
            Z1739Contratante_PropostaRqrPrz = A1739Contratante_PropostaRqrPrz;
            Z1740Contratante_PropostaRqrEsf = A1740Contratante_PropostaRqrEsf;
            Z1741Contratante_PropostaRqrCnt = A1741Contratante_PropostaRqrCnt;
            Z1742Contratante_PropostaNvlCnt = A1742Contratante_PropostaNvlCnt;
            Z1757Contratante_OSGeraOS = A1757Contratante_OSGeraOS;
            Z1758Contratante_OSGeraTRP = A1758Contratante_OSGeraTRP;
            Z1759Contratante_OSGeraTH = A1759Contratante_OSGeraTH;
            Z1760Contratante_OSGeraTRD = A1760Contratante_OSGeraTRD;
            Z1761Contratante_OSGeraTR = A1761Contratante_OSGeraTR;
            Z1803Contratante_OSHmlgComPnd = A1803Contratante_OSHmlgComPnd;
            Z1805Contratante_AtivoCirculante = A1805Contratante_AtivoCirculante;
            Z1806Contratante_PassivoCirculante = A1806Contratante_PassivoCirculante;
            Z1807Contratante_PatrimonioLiquido = A1807Contratante_PatrimonioLiquido;
            Z1808Contratante_ReceitaBruta = A1808Contratante_ReceitaBruta;
            Z1822Contratante_UsaOSistema = A1822Contratante_UsaOSistema;
            Z2034Contratante_TtlRltGerencial = A2034Contratante_TtlRltGerencial;
            Z2084Contratante_PrzAoRtr = A2084Contratante_PrzAoRtr;
            Z2083Contratante_PrzActRtr = A2083Contratante_PrzActRtr;
            Z2085Contratante_RequerOrigem = A2085Contratante_RequerOrigem;
            Z2089Contratante_SelecionaResponsavelOS = A2089Contratante_SelecionaResponsavelOS;
            Z2090Contratante_ExibePF = A2090Contratante_ExibePF;
            Z25Municipio_Codigo = A25Municipio_Codigo;
            Z335Contratante_PessoaCod = A335Contratante_PessoaCod;
         }
         if ( ( GX_JID == 25 ) || ( GX_JID == 0 ) )
         {
            Z26Municipio_Nome = A26Municipio_Nome;
            Z23Estado_UF = A23Estado_UF;
         }
         if ( ( GX_JID == 26 ) || ( GX_JID == 0 ) )
         {
            Z12Contratante_CNPJ = A12Contratante_CNPJ;
            Z9Contratante_RazaoSocial = A9Contratante_RazaoSocial;
         }
         if ( GX_JID == -24 )
         {
            Z29Contratante_Codigo = A29Contratante_Codigo;
            Z550Contratante_EmailSdaKey = A550Contratante_EmailSdaKey;
            Z549Contratante_EmailSdaPass = A549Contratante_EmailSdaPass;
            Z10Contratante_NomeFantasia = A10Contratante_NomeFantasia;
            Z11Contratante_IE = A11Contratante_IE;
            Z13Contratante_WebSite = A13Contratante_WebSite;
            Z14Contratante_Email = A14Contratante_Email;
            Z31Contratante_Telefone = A31Contratante_Telefone;
            Z32Contratante_Ramal = A32Contratante_Ramal;
            Z33Contratante_Fax = A33Contratante_Fax;
            Z336Contratante_AgenciaNome = A336Contratante_AgenciaNome;
            Z337Contratante_AgenciaNro = A337Contratante_AgenciaNro;
            Z338Contratante_BancoNome = A338Contratante_BancoNome;
            Z339Contratante_BancoNro = A339Contratante_BancoNro;
            Z16Contratante_ContaCorrente = A16Contratante_ContaCorrente;
            Z30Contratante_Ativo = A30Contratante_Ativo;
            Z547Contratante_EmailSdaHost = A547Contratante_EmailSdaHost;
            Z548Contratante_EmailSdaUser = A548Contratante_EmailSdaUser;
            Z551Contratante_EmailSdaAut = A551Contratante_EmailSdaAut;
            Z552Contratante_EmailSdaPort = A552Contratante_EmailSdaPort;
            Z1048Contratante_EmailSdaSec = A1048Contratante_EmailSdaSec;
            Z593Contratante_OSAutomatica = A593Contratante_OSAutomatica;
            Z1652Contratante_SSAutomatica = A1652Contratante_SSAutomatica;
            Z594Contratante_ExisteConferencia = A594Contratante_ExisteConferencia;
            Z1124Contratante_LogoArquivo = A1124Contratante_LogoArquivo;
            Z1448Contratante_InicioDoExpediente = A1448Contratante_InicioDoExpediente;
            Z1192Contratante_FimDoExpediente = A1192Contratante_FimDoExpediente;
            Z1727Contratante_ServicoSSPadrao = A1727Contratante_ServicoSSPadrao;
            Z1729Contratante_RetornaSS = A1729Contratante_RetornaSS;
            Z1732Contratante_SSStatusPlanejamento = A1732Contratante_SSStatusPlanejamento;
            Z1733Contratante_SSPrestadoraUnica = A1733Contratante_SSPrestadoraUnica;
            Z1738Contratante_PropostaRqrSrv = A1738Contratante_PropostaRqrSrv;
            Z1739Contratante_PropostaRqrPrz = A1739Contratante_PropostaRqrPrz;
            Z1740Contratante_PropostaRqrEsf = A1740Contratante_PropostaRqrEsf;
            Z1741Contratante_PropostaRqrCnt = A1741Contratante_PropostaRqrCnt;
            Z1742Contratante_PropostaNvlCnt = A1742Contratante_PropostaNvlCnt;
            Z1757Contratante_OSGeraOS = A1757Contratante_OSGeraOS;
            Z1758Contratante_OSGeraTRP = A1758Contratante_OSGeraTRP;
            Z1759Contratante_OSGeraTH = A1759Contratante_OSGeraTH;
            Z1760Contratante_OSGeraTRD = A1760Contratante_OSGeraTRD;
            Z1761Contratante_OSGeraTR = A1761Contratante_OSGeraTR;
            Z1803Contratante_OSHmlgComPnd = A1803Contratante_OSHmlgComPnd;
            Z1805Contratante_AtivoCirculante = A1805Contratante_AtivoCirculante;
            Z1806Contratante_PassivoCirculante = A1806Contratante_PassivoCirculante;
            Z1807Contratante_PatrimonioLiquido = A1807Contratante_PatrimonioLiquido;
            Z1808Contratante_ReceitaBruta = A1808Contratante_ReceitaBruta;
            Z1822Contratante_UsaOSistema = A1822Contratante_UsaOSistema;
            Z2034Contratante_TtlRltGerencial = A2034Contratante_TtlRltGerencial;
            Z2084Contratante_PrzAoRtr = A2084Contratante_PrzAoRtr;
            Z2083Contratante_PrzActRtr = A2083Contratante_PrzActRtr;
            Z2085Contratante_RequerOrigem = A2085Contratante_RequerOrigem;
            Z2089Contratante_SelecionaResponsavelOS = A2089Contratante_SelecionaResponsavelOS;
            Z2090Contratante_ExibePF = A2090Contratante_ExibePF;
            Z1126Contratante_LogoTipoArq = A1126Contratante_LogoTipoArq;
            Z1125Contratante_LogoNomeArq = A1125Contratante_LogoNomeArq;
            Z25Municipio_Codigo = A25Municipio_Codigo;
            Z335Contratante_PessoaCod = A335Contratante_PessoaCod;
            Z12Contratante_CNPJ = A12Contratante_CNPJ;
            Z9Contratante_RazaoSocial = A9Contratante_RazaoSocial;
            Z26Municipio_Nome = A26Municipio_Nome;
            Z23Estado_UF = A23Estado_UF;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV45Pgmname = "Contratante_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A2085Contratante_RequerOrigem) && ( Gx_BScreen == 0 ) )
         {
            A2085Contratante_RequerOrigem = true;
            n2085Contratante_RequerOrigem = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A2034Contratante_TtlRltGerencial)) && ( Gx_BScreen == 0 ) )
         {
            A2034Contratante_TtlRltGerencial = "Relat�rio Gerencial";
            n2034Contratante_TtlRltGerencial = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1822Contratante_UsaOSistema) && ( Gx_BScreen == 0 ) )
         {
            A1822Contratante_UsaOSistema = false;
            n1822Contratante_UsaOSistema = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1742Contratante_PropostaNvlCnt) && ( Gx_BScreen == 0 ) )
         {
            A1742Contratante_PropostaNvlCnt = 0;
            n1742Contratante_PropostaNvlCnt = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1738Contratante_PropostaRqrSrv) && ( Gx_BScreen == 0 ) )
         {
            A1738Contratante_PropostaRqrSrv = true;
            n1738Contratante_PropostaRqrSrv = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A1732Contratante_SSStatusPlanejamento)) && ( Gx_BScreen == 0 ) )
         {
            A1732Contratante_SSStatusPlanejamento = "B";
            n1732Contratante_SSStatusPlanejamento = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A551Contratante_EmailSdaAut) && ( Gx_BScreen == 0 ) )
         {
            A551Contratante_EmailSdaAut = false;
            n551Contratante_EmailSdaAut = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A30Contratante_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A30Contratante_Ativo = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load05124( )
      {
         /* Using cursor BC00056 */
         pr_default.execute(4, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound124 = 1;
            A550Contratante_EmailSdaKey = BC00056_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = BC00056_n550Contratante_EmailSdaKey[0];
            A549Contratante_EmailSdaPass = BC00056_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = BC00056_n549Contratante_EmailSdaPass[0];
            A12Contratante_CNPJ = BC00056_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = BC00056_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = BC00056_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = BC00056_n9Contratante_RazaoSocial[0];
            A10Contratante_NomeFantasia = BC00056_A10Contratante_NomeFantasia[0];
            A11Contratante_IE = BC00056_A11Contratante_IE[0];
            A13Contratante_WebSite = BC00056_A13Contratante_WebSite[0];
            n13Contratante_WebSite = BC00056_n13Contratante_WebSite[0];
            A14Contratante_Email = BC00056_A14Contratante_Email[0];
            n14Contratante_Email = BC00056_n14Contratante_Email[0];
            A31Contratante_Telefone = BC00056_A31Contratante_Telefone[0];
            A32Contratante_Ramal = BC00056_A32Contratante_Ramal[0];
            n32Contratante_Ramal = BC00056_n32Contratante_Ramal[0];
            A33Contratante_Fax = BC00056_A33Contratante_Fax[0];
            n33Contratante_Fax = BC00056_n33Contratante_Fax[0];
            A336Contratante_AgenciaNome = BC00056_A336Contratante_AgenciaNome[0];
            n336Contratante_AgenciaNome = BC00056_n336Contratante_AgenciaNome[0];
            A337Contratante_AgenciaNro = BC00056_A337Contratante_AgenciaNro[0];
            n337Contratante_AgenciaNro = BC00056_n337Contratante_AgenciaNro[0];
            A338Contratante_BancoNome = BC00056_A338Contratante_BancoNome[0];
            n338Contratante_BancoNome = BC00056_n338Contratante_BancoNome[0];
            A339Contratante_BancoNro = BC00056_A339Contratante_BancoNro[0];
            n339Contratante_BancoNro = BC00056_n339Contratante_BancoNro[0];
            A16Contratante_ContaCorrente = BC00056_A16Contratante_ContaCorrente[0];
            n16Contratante_ContaCorrente = BC00056_n16Contratante_ContaCorrente[0];
            A26Municipio_Nome = BC00056_A26Municipio_Nome[0];
            A30Contratante_Ativo = BC00056_A30Contratante_Ativo[0];
            A547Contratante_EmailSdaHost = BC00056_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = BC00056_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = BC00056_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = BC00056_n548Contratante_EmailSdaUser[0];
            A551Contratante_EmailSdaAut = BC00056_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = BC00056_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = BC00056_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = BC00056_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = BC00056_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = BC00056_n1048Contratante_EmailSdaSec[0];
            A593Contratante_OSAutomatica = BC00056_A593Contratante_OSAutomatica[0];
            A1652Contratante_SSAutomatica = BC00056_A1652Contratante_SSAutomatica[0];
            n1652Contratante_SSAutomatica = BC00056_n1652Contratante_SSAutomatica[0];
            A594Contratante_ExisteConferencia = BC00056_A594Contratante_ExisteConferencia[0];
            A1448Contratante_InicioDoExpediente = BC00056_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = BC00056_n1448Contratante_InicioDoExpediente[0];
            A1192Contratante_FimDoExpediente = BC00056_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = BC00056_n1192Contratante_FimDoExpediente[0];
            A1727Contratante_ServicoSSPadrao = BC00056_A1727Contratante_ServicoSSPadrao[0];
            n1727Contratante_ServicoSSPadrao = BC00056_n1727Contratante_ServicoSSPadrao[0];
            A1729Contratante_RetornaSS = BC00056_A1729Contratante_RetornaSS[0];
            n1729Contratante_RetornaSS = BC00056_n1729Contratante_RetornaSS[0];
            A1732Contratante_SSStatusPlanejamento = BC00056_A1732Contratante_SSStatusPlanejamento[0];
            n1732Contratante_SSStatusPlanejamento = BC00056_n1732Contratante_SSStatusPlanejamento[0];
            A1733Contratante_SSPrestadoraUnica = BC00056_A1733Contratante_SSPrestadoraUnica[0];
            n1733Contratante_SSPrestadoraUnica = BC00056_n1733Contratante_SSPrestadoraUnica[0];
            A1738Contratante_PropostaRqrSrv = BC00056_A1738Contratante_PropostaRqrSrv[0];
            n1738Contratante_PropostaRqrSrv = BC00056_n1738Contratante_PropostaRqrSrv[0];
            A1739Contratante_PropostaRqrPrz = BC00056_A1739Contratante_PropostaRqrPrz[0];
            n1739Contratante_PropostaRqrPrz = BC00056_n1739Contratante_PropostaRqrPrz[0];
            A1740Contratante_PropostaRqrEsf = BC00056_A1740Contratante_PropostaRqrEsf[0];
            n1740Contratante_PropostaRqrEsf = BC00056_n1740Contratante_PropostaRqrEsf[0];
            A1741Contratante_PropostaRqrCnt = BC00056_A1741Contratante_PropostaRqrCnt[0];
            n1741Contratante_PropostaRqrCnt = BC00056_n1741Contratante_PropostaRqrCnt[0];
            A1742Contratante_PropostaNvlCnt = BC00056_A1742Contratante_PropostaNvlCnt[0];
            n1742Contratante_PropostaNvlCnt = BC00056_n1742Contratante_PropostaNvlCnt[0];
            A1757Contratante_OSGeraOS = BC00056_A1757Contratante_OSGeraOS[0];
            n1757Contratante_OSGeraOS = BC00056_n1757Contratante_OSGeraOS[0];
            A1758Contratante_OSGeraTRP = BC00056_A1758Contratante_OSGeraTRP[0];
            n1758Contratante_OSGeraTRP = BC00056_n1758Contratante_OSGeraTRP[0];
            A1759Contratante_OSGeraTH = BC00056_A1759Contratante_OSGeraTH[0];
            n1759Contratante_OSGeraTH = BC00056_n1759Contratante_OSGeraTH[0];
            A1760Contratante_OSGeraTRD = BC00056_A1760Contratante_OSGeraTRD[0];
            n1760Contratante_OSGeraTRD = BC00056_n1760Contratante_OSGeraTRD[0];
            A1761Contratante_OSGeraTR = BC00056_A1761Contratante_OSGeraTR[0];
            n1761Contratante_OSGeraTR = BC00056_n1761Contratante_OSGeraTR[0];
            A1803Contratante_OSHmlgComPnd = BC00056_A1803Contratante_OSHmlgComPnd[0];
            n1803Contratante_OSHmlgComPnd = BC00056_n1803Contratante_OSHmlgComPnd[0];
            A1805Contratante_AtivoCirculante = BC00056_A1805Contratante_AtivoCirculante[0];
            n1805Contratante_AtivoCirculante = BC00056_n1805Contratante_AtivoCirculante[0];
            A1806Contratante_PassivoCirculante = BC00056_A1806Contratante_PassivoCirculante[0];
            n1806Contratante_PassivoCirculante = BC00056_n1806Contratante_PassivoCirculante[0];
            A1807Contratante_PatrimonioLiquido = BC00056_A1807Contratante_PatrimonioLiquido[0];
            n1807Contratante_PatrimonioLiquido = BC00056_n1807Contratante_PatrimonioLiquido[0];
            A1808Contratante_ReceitaBruta = BC00056_A1808Contratante_ReceitaBruta[0];
            n1808Contratante_ReceitaBruta = BC00056_n1808Contratante_ReceitaBruta[0];
            A1822Contratante_UsaOSistema = BC00056_A1822Contratante_UsaOSistema[0];
            n1822Contratante_UsaOSistema = BC00056_n1822Contratante_UsaOSistema[0];
            A2034Contratante_TtlRltGerencial = BC00056_A2034Contratante_TtlRltGerencial[0];
            n2034Contratante_TtlRltGerencial = BC00056_n2034Contratante_TtlRltGerencial[0];
            A2084Contratante_PrzAoRtr = BC00056_A2084Contratante_PrzAoRtr[0];
            n2084Contratante_PrzAoRtr = BC00056_n2084Contratante_PrzAoRtr[0];
            A2083Contratante_PrzActRtr = BC00056_A2083Contratante_PrzActRtr[0];
            n2083Contratante_PrzActRtr = BC00056_n2083Contratante_PrzActRtr[0];
            A2085Contratante_RequerOrigem = BC00056_A2085Contratante_RequerOrigem[0];
            n2085Contratante_RequerOrigem = BC00056_n2085Contratante_RequerOrigem[0];
            A2089Contratante_SelecionaResponsavelOS = BC00056_A2089Contratante_SelecionaResponsavelOS[0];
            A2090Contratante_ExibePF = BC00056_A2090Contratante_ExibePF[0];
            A1126Contratante_LogoTipoArq = BC00056_A1126Contratante_LogoTipoArq[0];
            n1126Contratante_LogoTipoArq = BC00056_n1126Contratante_LogoTipoArq[0];
            A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
            A1125Contratante_LogoNomeArq = BC00056_A1125Contratante_LogoNomeArq[0];
            n1125Contratante_LogoNomeArq = BC00056_n1125Contratante_LogoNomeArq[0];
            A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
            A25Municipio_Codigo = BC00056_A25Municipio_Codigo[0];
            n25Municipio_Codigo = BC00056_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = BC00056_A335Contratante_PessoaCod[0];
            A23Estado_UF = BC00056_A23Estado_UF[0];
            A1124Contratante_LogoArquivo = BC00056_A1124Contratante_LogoArquivo[0];
            n1124Contratante_LogoArquivo = BC00056_n1124Contratante_LogoArquivo[0];
            ZM05124( -24) ;
         }
         pr_default.close(4);
         OnLoadActions05124( ) ;
      }

      protected void OnLoadActions05124( )
      {
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV15Estado_UF = A23Estado_UF;
         }
      }

      protected void CheckExtendedTable05124( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00055 */
         pr_default.execute(3, new Object[] {A335Contratante_PessoaCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratante_Pessoa'.", "ForeignKeyNotFound", 1, "CONTRATANTE_PESSOACOD");
            AnyError = 1;
         }
         A12Contratante_CNPJ = BC00055_A12Contratante_CNPJ[0];
         n12Contratante_CNPJ = BC00055_n12Contratante_CNPJ[0];
         A9Contratante_RazaoSocial = BC00055_A9Contratante_RazaoSocial[0];
         n9Contratante_RazaoSocial = BC00055_n9Contratante_RazaoSocial[0];
         pr_default.close(3);
         if ( ! ( GxRegex.IsMatch(A14Contratante_Email,"^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") || String.IsNullOrEmpty(StringUtil.RTrim( A14Contratante_Email)) ) )
         {
            GX_msglist.addItem("O valor de Email n�o coincide com o padr�o especificado", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC00054 */
         pr_default.execute(2, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A25Municipio_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Municipio'.", "ForeignKeyNotFound", 1, "MUNICIPIO_CODIGO");
               AnyError = 1;
            }
         }
         A26Municipio_Nome = BC00054_A26Municipio_Nome[0];
         A23Estado_UF = BC00054_A23Estado_UF[0];
         pr_default.close(2);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV15Estado_UF = A23Estado_UF;
         }
         if ( ! ( ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "B") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "S") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "E") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "A") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "R") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "C") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "D") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "H") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "O") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "P") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "L") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "X") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "N") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "J") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "I") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "T") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "Q") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "G") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "M") == 0 ) || ( StringUtil.StrCmp(A1732Contratante_SSStatusPlanejamento, "U") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1732Contratante_SSStatusPlanejamento)) ) )
         {
            GX_msglist.addItem("Campo Status em planejamento fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A1742Contratante_PropostaNvlCnt == 0 ) || ( A1742Contratante_PropostaNvlCnt == 1 ) || ( A1742Contratante_PropostaNvlCnt == 2 ) || (0==A1742Contratante_PropostaNvlCnt) ) )
         {
            GX_msglist.addItem("Campo Nivel dos valores? fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors05124( )
      {
         pr_default.close(3);
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey05124( )
      {
         /* Using cursor BC00057 */
         pr_default.execute(5, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound124 = 1;
         }
         else
         {
            RcdFound124 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00053 */
         pr_default.execute(1, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM05124( 24) ;
            RcdFound124 = 1;
            A29Contratante_Codigo = BC00053_A29Contratante_Codigo[0];
            n29Contratante_Codigo = BC00053_n29Contratante_Codigo[0];
            A550Contratante_EmailSdaKey = BC00053_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = BC00053_n550Contratante_EmailSdaKey[0];
            A549Contratante_EmailSdaPass = BC00053_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = BC00053_n549Contratante_EmailSdaPass[0];
            A10Contratante_NomeFantasia = BC00053_A10Contratante_NomeFantasia[0];
            A11Contratante_IE = BC00053_A11Contratante_IE[0];
            A13Contratante_WebSite = BC00053_A13Contratante_WebSite[0];
            n13Contratante_WebSite = BC00053_n13Contratante_WebSite[0];
            A14Contratante_Email = BC00053_A14Contratante_Email[0];
            n14Contratante_Email = BC00053_n14Contratante_Email[0];
            A31Contratante_Telefone = BC00053_A31Contratante_Telefone[0];
            A32Contratante_Ramal = BC00053_A32Contratante_Ramal[0];
            n32Contratante_Ramal = BC00053_n32Contratante_Ramal[0];
            A33Contratante_Fax = BC00053_A33Contratante_Fax[0];
            n33Contratante_Fax = BC00053_n33Contratante_Fax[0];
            A336Contratante_AgenciaNome = BC00053_A336Contratante_AgenciaNome[0];
            n336Contratante_AgenciaNome = BC00053_n336Contratante_AgenciaNome[0];
            A337Contratante_AgenciaNro = BC00053_A337Contratante_AgenciaNro[0];
            n337Contratante_AgenciaNro = BC00053_n337Contratante_AgenciaNro[0];
            A338Contratante_BancoNome = BC00053_A338Contratante_BancoNome[0];
            n338Contratante_BancoNome = BC00053_n338Contratante_BancoNome[0];
            A339Contratante_BancoNro = BC00053_A339Contratante_BancoNro[0];
            n339Contratante_BancoNro = BC00053_n339Contratante_BancoNro[0];
            A16Contratante_ContaCorrente = BC00053_A16Contratante_ContaCorrente[0];
            n16Contratante_ContaCorrente = BC00053_n16Contratante_ContaCorrente[0];
            A30Contratante_Ativo = BC00053_A30Contratante_Ativo[0];
            A547Contratante_EmailSdaHost = BC00053_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = BC00053_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = BC00053_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = BC00053_n548Contratante_EmailSdaUser[0];
            A551Contratante_EmailSdaAut = BC00053_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = BC00053_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = BC00053_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = BC00053_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = BC00053_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = BC00053_n1048Contratante_EmailSdaSec[0];
            A593Contratante_OSAutomatica = BC00053_A593Contratante_OSAutomatica[0];
            A1652Contratante_SSAutomatica = BC00053_A1652Contratante_SSAutomatica[0];
            n1652Contratante_SSAutomatica = BC00053_n1652Contratante_SSAutomatica[0];
            A594Contratante_ExisteConferencia = BC00053_A594Contratante_ExisteConferencia[0];
            A1448Contratante_InicioDoExpediente = BC00053_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = BC00053_n1448Contratante_InicioDoExpediente[0];
            A1192Contratante_FimDoExpediente = BC00053_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = BC00053_n1192Contratante_FimDoExpediente[0];
            A1727Contratante_ServicoSSPadrao = BC00053_A1727Contratante_ServicoSSPadrao[0];
            n1727Contratante_ServicoSSPadrao = BC00053_n1727Contratante_ServicoSSPadrao[0];
            A1729Contratante_RetornaSS = BC00053_A1729Contratante_RetornaSS[0];
            n1729Contratante_RetornaSS = BC00053_n1729Contratante_RetornaSS[0];
            A1732Contratante_SSStatusPlanejamento = BC00053_A1732Contratante_SSStatusPlanejamento[0];
            n1732Contratante_SSStatusPlanejamento = BC00053_n1732Contratante_SSStatusPlanejamento[0];
            A1733Contratante_SSPrestadoraUnica = BC00053_A1733Contratante_SSPrestadoraUnica[0];
            n1733Contratante_SSPrestadoraUnica = BC00053_n1733Contratante_SSPrestadoraUnica[0];
            A1738Contratante_PropostaRqrSrv = BC00053_A1738Contratante_PropostaRqrSrv[0];
            n1738Contratante_PropostaRqrSrv = BC00053_n1738Contratante_PropostaRqrSrv[0];
            A1739Contratante_PropostaRqrPrz = BC00053_A1739Contratante_PropostaRqrPrz[0];
            n1739Contratante_PropostaRqrPrz = BC00053_n1739Contratante_PropostaRqrPrz[0];
            A1740Contratante_PropostaRqrEsf = BC00053_A1740Contratante_PropostaRqrEsf[0];
            n1740Contratante_PropostaRqrEsf = BC00053_n1740Contratante_PropostaRqrEsf[0];
            A1741Contratante_PropostaRqrCnt = BC00053_A1741Contratante_PropostaRqrCnt[0];
            n1741Contratante_PropostaRqrCnt = BC00053_n1741Contratante_PropostaRqrCnt[0];
            A1742Contratante_PropostaNvlCnt = BC00053_A1742Contratante_PropostaNvlCnt[0];
            n1742Contratante_PropostaNvlCnt = BC00053_n1742Contratante_PropostaNvlCnt[0];
            A1757Contratante_OSGeraOS = BC00053_A1757Contratante_OSGeraOS[0];
            n1757Contratante_OSGeraOS = BC00053_n1757Contratante_OSGeraOS[0];
            A1758Contratante_OSGeraTRP = BC00053_A1758Contratante_OSGeraTRP[0];
            n1758Contratante_OSGeraTRP = BC00053_n1758Contratante_OSGeraTRP[0];
            A1759Contratante_OSGeraTH = BC00053_A1759Contratante_OSGeraTH[0];
            n1759Contratante_OSGeraTH = BC00053_n1759Contratante_OSGeraTH[0];
            A1760Contratante_OSGeraTRD = BC00053_A1760Contratante_OSGeraTRD[0];
            n1760Contratante_OSGeraTRD = BC00053_n1760Contratante_OSGeraTRD[0];
            A1761Contratante_OSGeraTR = BC00053_A1761Contratante_OSGeraTR[0];
            n1761Contratante_OSGeraTR = BC00053_n1761Contratante_OSGeraTR[0];
            A1803Contratante_OSHmlgComPnd = BC00053_A1803Contratante_OSHmlgComPnd[0];
            n1803Contratante_OSHmlgComPnd = BC00053_n1803Contratante_OSHmlgComPnd[0];
            A1805Contratante_AtivoCirculante = BC00053_A1805Contratante_AtivoCirculante[0];
            n1805Contratante_AtivoCirculante = BC00053_n1805Contratante_AtivoCirculante[0];
            A1806Contratante_PassivoCirculante = BC00053_A1806Contratante_PassivoCirculante[0];
            n1806Contratante_PassivoCirculante = BC00053_n1806Contratante_PassivoCirculante[0];
            A1807Contratante_PatrimonioLiquido = BC00053_A1807Contratante_PatrimonioLiquido[0];
            n1807Contratante_PatrimonioLiquido = BC00053_n1807Contratante_PatrimonioLiquido[0];
            A1808Contratante_ReceitaBruta = BC00053_A1808Contratante_ReceitaBruta[0];
            n1808Contratante_ReceitaBruta = BC00053_n1808Contratante_ReceitaBruta[0];
            A1822Contratante_UsaOSistema = BC00053_A1822Contratante_UsaOSistema[0];
            n1822Contratante_UsaOSistema = BC00053_n1822Contratante_UsaOSistema[0];
            A2034Contratante_TtlRltGerencial = BC00053_A2034Contratante_TtlRltGerencial[0];
            n2034Contratante_TtlRltGerencial = BC00053_n2034Contratante_TtlRltGerencial[0];
            A2084Contratante_PrzAoRtr = BC00053_A2084Contratante_PrzAoRtr[0];
            n2084Contratante_PrzAoRtr = BC00053_n2084Contratante_PrzAoRtr[0];
            A2083Contratante_PrzActRtr = BC00053_A2083Contratante_PrzActRtr[0];
            n2083Contratante_PrzActRtr = BC00053_n2083Contratante_PrzActRtr[0];
            A2085Contratante_RequerOrigem = BC00053_A2085Contratante_RequerOrigem[0];
            n2085Contratante_RequerOrigem = BC00053_n2085Contratante_RequerOrigem[0];
            A2089Contratante_SelecionaResponsavelOS = BC00053_A2089Contratante_SelecionaResponsavelOS[0];
            A2090Contratante_ExibePF = BC00053_A2090Contratante_ExibePF[0];
            A1126Contratante_LogoTipoArq = BC00053_A1126Contratante_LogoTipoArq[0];
            n1126Contratante_LogoTipoArq = BC00053_n1126Contratante_LogoTipoArq[0];
            A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
            A1125Contratante_LogoNomeArq = BC00053_A1125Contratante_LogoNomeArq[0];
            n1125Contratante_LogoNomeArq = BC00053_n1125Contratante_LogoNomeArq[0];
            A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
            A25Municipio_Codigo = BC00053_A25Municipio_Codigo[0];
            n25Municipio_Codigo = BC00053_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = BC00053_A335Contratante_PessoaCod[0];
            A1124Contratante_LogoArquivo = BC00053_A1124Contratante_LogoArquivo[0];
            n1124Contratante_LogoArquivo = BC00053_n1124Contratante_LogoArquivo[0];
            O549Contratante_EmailSdaPass = A549Contratante_EmailSdaPass;
            n549Contratante_EmailSdaPass = false;
            Z29Contratante_Codigo = A29Contratante_Codigo;
            sMode124 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load05124( ) ;
            if ( AnyError == 1 )
            {
               RcdFound124 = 0;
               InitializeNonKey05124( ) ;
            }
            Gx_mode = sMode124;
         }
         else
         {
            RcdFound124 = 0;
            InitializeNonKey05124( ) ;
            sMode124 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode124;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey05124( ) ;
         if ( RcdFound124 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_050( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency05124( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00052 */
            pr_default.execute(0, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contratante"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z550Contratante_EmailSdaKey, BC00052_A550Contratante_EmailSdaKey[0]) != 0 ) || ( StringUtil.StrCmp(Z549Contratante_EmailSdaPass, BC00052_A549Contratante_EmailSdaPass[0]) != 0 ) || ( StringUtil.StrCmp(Z10Contratante_NomeFantasia, BC00052_A10Contratante_NomeFantasia[0]) != 0 ) || ( StringUtil.StrCmp(Z11Contratante_IE, BC00052_A11Contratante_IE[0]) != 0 ) || ( StringUtil.StrCmp(Z13Contratante_WebSite, BC00052_A13Contratante_WebSite[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z14Contratante_Email, BC00052_A14Contratante_Email[0]) != 0 ) || ( StringUtil.StrCmp(Z31Contratante_Telefone, BC00052_A31Contratante_Telefone[0]) != 0 ) || ( StringUtil.StrCmp(Z32Contratante_Ramal, BC00052_A32Contratante_Ramal[0]) != 0 ) || ( StringUtil.StrCmp(Z33Contratante_Fax, BC00052_A33Contratante_Fax[0]) != 0 ) || ( StringUtil.StrCmp(Z336Contratante_AgenciaNome, BC00052_A336Contratante_AgenciaNome[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z337Contratante_AgenciaNro, BC00052_A337Contratante_AgenciaNro[0]) != 0 ) || ( StringUtil.StrCmp(Z338Contratante_BancoNome, BC00052_A338Contratante_BancoNome[0]) != 0 ) || ( StringUtil.StrCmp(Z339Contratante_BancoNro, BC00052_A339Contratante_BancoNro[0]) != 0 ) || ( StringUtil.StrCmp(Z16Contratante_ContaCorrente, BC00052_A16Contratante_ContaCorrente[0]) != 0 ) || ( Z30Contratante_Ativo != BC00052_A30Contratante_Ativo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z547Contratante_EmailSdaHost, BC00052_A547Contratante_EmailSdaHost[0]) != 0 ) || ( StringUtil.StrCmp(Z548Contratante_EmailSdaUser, BC00052_A548Contratante_EmailSdaUser[0]) != 0 ) || ( Z551Contratante_EmailSdaAut != BC00052_A551Contratante_EmailSdaAut[0] ) || ( Z552Contratante_EmailSdaPort != BC00052_A552Contratante_EmailSdaPort[0] ) || ( Z1048Contratante_EmailSdaSec != BC00052_A1048Contratante_EmailSdaSec[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z593Contratante_OSAutomatica != BC00052_A593Contratante_OSAutomatica[0] ) || ( Z1652Contratante_SSAutomatica != BC00052_A1652Contratante_SSAutomatica[0] ) || ( Z594Contratante_ExisteConferencia != BC00052_A594Contratante_ExisteConferencia[0] ) || ( Z1448Contratante_InicioDoExpediente != BC00052_A1448Contratante_InicioDoExpediente[0] ) || ( Z1192Contratante_FimDoExpediente != BC00052_A1192Contratante_FimDoExpediente[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1727Contratante_ServicoSSPadrao != BC00052_A1727Contratante_ServicoSSPadrao[0] ) || ( Z1729Contratante_RetornaSS != BC00052_A1729Contratante_RetornaSS[0] ) || ( StringUtil.StrCmp(Z1732Contratante_SSStatusPlanejamento, BC00052_A1732Contratante_SSStatusPlanejamento[0]) != 0 ) || ( Z1733Contratante_SSPrestadoraUnica != BC00052_A1733Contratante_SSPrestadoraUnica[0] ) || ( Z1738Contratante_PropostaRqrSrv != BC00052_A1738Contratante_PropostaRqrSrv[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1739Contratante_PropostaRqrPrz != BC00052_A1739Contratante_PropostaRqrPrz[0] ) || ( Z1740Contratante_PropostaRqrEsf != BC00052_A1740Contratante_PropostaRqrEsf[0] ) || ( Z1741Contratante_PropostaRqrCnt != BC00052_A1741Contratante_PropostaRqrCnt[0] ) || ( Z1742Contratante_PropostaNvlCnt != BC00052_A1742Contratante_PropostaNvlCnt[0] ) || ( Z1757Contratante_OSGeraOS != BC00052_A1757Contratante_OSGeraOS[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1758Contratante_OSGeraTRP != BC00052_A1758Contratante_OSGeraTRP[0] ) || ( Z1759Contratante_OSGeraTH != BC00052_A1759Contratante_OSGeraTH[0] ) || ( Z1760Contratante_OSGeraTRD != BC00052_A1760Contratante_OSGeraTRD[0] ) || ( Z1761Contratante_OSGeraTR != BC00052_A1761Contratante_OSGeraTR[0] ) || ( Z1803Contratante_OSHmlgComPnd != BC00052_A1803Contratante_OSHmlgComPnd[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1805Contratante_AtivoCirculante != BC00052_A1805Contratante_AtivoCirculante[0] ) || ( Z1806Contratante_PassivoCirculante != BC00052_A1806Contratante_PassivoCirculante[0] ) || ( Z1807Contratante_PatrimonioLiquido != BC00052_A1807Contratante_PatrimonioLiquido[0] ) || ( Z1808Contratante_ReceitaBruta != BC00052_A1808Contratante_ReceitaBruta[0] ) || ( Z1822Contratante_UsaOSistema != BC00052_A1822Contratante_UsaOSistema[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z2034Contratante_TtlRltGerencial, BC00052_A2034Contratante_TtlRltGerencial[0]) != 0 ) || ( Z2084Contratante_PrzAoRtr != BC00052_A2084Contratante_PrzAoRtr[0] ) || ( Z2083Contratante_PrzActRtr != BC00052_A2083Contratante_PrzActRtr[0] ) || ( Z2085Contratante_RequerOrigem != BC00052_A2085Contratante_RequerOrigem[0] ) || ( Z2089Contratante_SelecionaResponsavelOS != BC00052_A2089Contratante_SelecionaResponsavelOS[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2090Contratante_ExibePF != BC00052_A2090Contratante_ExibePF[0] ) || ( Z25Municipio_Codigo != BC00052_A25Municipio_Codigo[0] ) || ( Z335Contratante_PessoaCod != BC00052_A335Contratante_PessoaCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Contratante"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert05124( )
      {
         BeforeValidate05124( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable05124( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM05124( 0) ;
            CheckOptimisticConcurrency05124( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm05124( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert05124( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00058 */
                     pr_default.execute(6, new Object[] {n550Contratante_EmailSdaKey, A550Contratante_EmailSdaKey, n549Contratante_EmailSdaPass, A549Contratante_EmailSdaPass, A10Contratante_NomeFantasia, A11Contratante_IE, n13Contratante_WebSite, A13Contratante_WebSite, n14Contratante_Email, A14Contratante_Email, A31Contratante_Telefone, n32Contratante_Ramal, A32Contratante_Ramal, n33Contratante_Fax, A33Contratante_Fax, n336Contratante_AgenciaNome, A336Contratante_AgenciaNome, n337Contratante_AgenciaNro, A337Contratante_AgenciaNro, n338Contratante_BancoNome, A338Contratante_BancoNome, n339Contratante_BancoNro, A339Contratante_BancoNro, n16Contratante_ContaCorrente, A16Contratante_ContaCorrente, A30Contratante_Ativo, n547Contratante_EmailSdaHost, A547Contratante_EmailSdaHost, n548Contratante_EmailSdaUser, A548Contratante_EmailSdaUser, n551Contratante_EmailSdaAut, A551Contratante_EmailSdaAut, n552Contratante_EmailSdaPort, A552Contratante_EmailSdaPort, n1048Contratante_EmailSdaSec, A1048Contratante_EmailSdaSec, A593Contratante_OSAutomatica, n1652Contratante_SSAutomatica, A1652Contratante_SSAutomatica, A594Contratante_ExisteConferencia, n1124Contratante_LogoArquivo, A1124Contratante_LogoArquivo, n1448Contratante_InicioDoExpediente, A1448Contratante_InicioDoExpediente, n1192Contratante_FimDoExpediente, A1192Contratante_FimDoExpediente, n1727Contratante_ServicoSSPadrao, A1727Contratante_ServicoSSPadrao, n1729Contratante_RetornaSS, A1729Contratante_RetornaSS, n1732Contratante_SSStatusPlanejamento, A1732Contratante_SSStatusPlanejamento, n1733Contratante_SSPrestadoraUnica, A1733Contratante_SSPrestadoraUnica, n1738Contratante_PropostaRqrSrv, A1738Contratante_PropostaRqrSrv, n1739Contratante_PropostaRqrPrz, A1739Contratante_PropostaRqrPrz, n1740Contratante_PropostaRqrEsf, A1740Contratante_PropostaRqrEsf, n1741Contratante_PropostaRqrCnt, A1741Contratante_PropostaRqrCnt, n1742Contratante_PropostaNvlCnt, A1742Contratante_PropostaNvlCnt, n1757Contratante_OSGeraOS, A1757Contratante_OSGeraOS, n1758Contratante_OSGeraTRP, A1758Contratante_OSGeraTRP, n1759Contratante_OSGeraTH, A1759Contratante_OSGeraTH, n1760Contratante_OSGeraTRD, A1760Contratante_OSGeraTRD, n1761Contratante_OSGeraTR, A1761Contratante_OSGeraTR, n1803Contratante_OSHmlgComPnd, A1803Contratante_OSHmlgComPnd, n1805Contratante_AtivoCirculante, A1805Contratante_AtivoCirculante, n1806Contratante_PassivoCirculante, A1806Contratante_PassivoCirculante, n1807Contratante_PatrimonioLiquido, A1807Contratante_PatrimonioLiquido, n1808Contratante_ReceitaBruta, A1808Contratante_ReceitaBruta, n1822Contratante_UsaOSistema, A1822Contratante_UsaOSistema, n2034Contratante_TtlRltGerencial, A2034Contratante_TtlRltGerencial, n2084Contratante_PrzAoRtr, A2084Contratante_PrzAoRtr, n2083Contratante_PrzActRtr, A2083Contratante_PrzActRtr, n2085Contratante_RequerOrigem, A2085Contratante_RequerOrigem, A2089Contratante_SelecionaResponsavelOS, A2090Contratante_ExibePF, n1126Contratante_LogoTipoArq, A1126Contratante_LogoTipoArq, n1125Contratante_LogoNomeArq, A1125Contratante_LogoNomeArq, n25Municipio_Codigo, A25Municipio_Codigo, A335Contratante_PessoaCod});
                     A29Contratante_Codigo = BC00058_A29Contratante_Codigo[0];
                     n29Contratante_Codigo = BC00058_n29Contratante_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Contratante") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load05124( ) ;
            }
            EndLevel05124( ) ;
         }
         CloseExtendedTableCursors05124( ) ;
      }

      protected void Update05124( )
      {
         BeforeValidate05124( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable05124( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency05124( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm05124( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate05124( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00059 */
                     pr_default.execute(7, new Object[] {n550Contratante_EmailSdaKey, A550Contratante_EmailSdaKey, n549Contratante_EmailSdaPass, A549Contratante_EmailSdaPass, A10Contratante_NomeFantasia, A11Contratante_IE, n13Contratante_WebSite, A13Contratante_WebSite, n14Contratante_Email, A14Contratante_Email, A31Contratante_Telefone, n32Contratante_Ramal, A32Contratante_Ramal, n33Contratante_Fax, A33Contratante_Fax, n336Contratante_AgenciaNome, A336Contratante_AgenciaNome, n337Contratante_AgenciaNro, A337Contratante_AgenciaNro, n338Contratante_BancoNome, A338Contratante_BancoNome, n339Contratante_BancoNro, A339Contratante_BancoNro, n16Contratante_ContaCorrente, A16Contratante_ContaCorrente, A30Contratante_Ativo, n547Contratante_EmailSdaHost, A547Contratante_EmailSdaHost, n548Contratante_EmailSdaUser, A548Contratante_EmailSdaUser, n551Contratante_EmailSdaAut, A551Contratante_EmailSdaAut, n552Contratante_EmailSdaPort, A552Contratante_EmailSdaPort, n1048Contratante_EmailSdaSec, A1048Contratante_EmailSdaSec, A593Contratante_OSAutomatica, n1652Contratante_SSAutomatica, A1652Contratante_SSAutomatica, A594Contratante_ExisteConferencia, n1448Contratante_InicioDoExpediente, A1448Contratante_InicioDoExpediente, n1192Contratante_FimDoExpediente, A1192Contratante_FimDoExpediente, n1727Contratante_ServicoSSPadrao, A1727Contratante_ServicoSSPadrao, n1729Contratante_RetornaSS, A1729Contratante_RetornaSS, n1732Contratante_SSStatusPlanejamento, A1732Contratante_SSStatusPlanejamento, n1733Contratante_SSPrestadoraUnica, A1733Contratante_SSPrestadoraUnica, n1738Contratante_PropostaRqrSrv, A1738Contratante_PropostaRqrSrv, n1739Contratante_PropostaRqrPrz, A1739Contratante_PropostaRqrPrz, n1740Contratante_PropostaRqrEsf, A1740Contratante_PropostaRqrEsf, n1741Contratante_PropostaRqrCnt, A1741Contratante_PropostaRqrCnt, n1742Contratante_PropostaNvlCnt, A1742Contratante_PropostaNvlCnt, n1757Contratante_OSGeraOS, A1757Contratante_OSGeraOS, n1758Contratante_OSGeraTRP, A1758Contratante_OSGeraTRP, n1759Contratante_OSGeraTH, A1759Contratante_OSGeraTH, n1760Contratante_OSGeraTRD, A1760Contratante_OSGeraTRD, n1761Contratante_OSGeraTR, A1761Contratante_OSGeraTR, n1803Contratante_OSHmlgComPnd, A1803Contratante_OSHmlgComPnd, n1805Contratante_AtivoCirculante, A1805Contratante_AtivoCirculante, n1806Contratante_PassivoCirculante, A1806Contratante_PassivoCirculante, n1807Contratante_PatrimonioLiquido, A1807Contratante_PatrimonioLiquido, n1808Contratante_ReceitaBruta, A1808Contratante_ReceitaBruta, n1822Contratante_UsaOSistema, A1822Contratante_UsaOSistema, n2034Contratante_TtlRltGerencial, A2034Contratante_TtlRltGerencial, n2084Contratante_PrzAoRtr, A2084Contratante_PrzAoRtr, n2083Contratante_PrzActRtr, A2083Contratante_PrzActRtr, n2085Contratante_RequerOrigem, A2085Contratante_RequerOrigem, A2089Contratante_SelecionaResponsavelOS, A2090Contratante_ExibePF, n1126Contratante_LogoTipoArq, A1126Contratante_LogoTipoArq, n1125Contratante_LogoNomeArq, A1125Contratante_LogoNomeArq, n25Municipio_Codigo, A25Municipio_Codigo, A335Contratante_PessoaCod, n29Contratante_Codigo, A29Contratante_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Contratante") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contratante"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate05124( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel05124( ) ;
         }
         CloseExtendedTableCursors05124( ) ;
      }

      protected void DeferredUpdate05124( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC000510 */
            pr_default.execute(8, new Object[] {n1124Contratante_LogoArquivo, A1124Contratante_LogoArquivo, n29Contratante_Codigo, A29Contratante_Codigo});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("Contratante") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate05124( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency05124( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls05124( ) ;
            AfterConfirm05124( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete05124( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000511 */
                  pr_default.execute(9, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
                  pr_default.close(9);
                  dsDefault.SmartCacheProvider.SetUpdated("Contratante") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode124 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel05124( ) ;
         Gx_mode = sMode124;
      }

      protected void OnDeleteControls05124( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000512 */
            pr_default.execute(10, new Object[] {A335Contratante_PessoaCod});
            A12Contratante_CNPJ = BC000512_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = BC000512_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = BC000512_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = BC000512_n9Contratante_RazaoSocial[0];
            pr_default.close(10);
            /* Using cursor BC000513 */
            pr_default.execute(11, new Object[] {n25Municipio_Codigo, A25Municipio_Codigo});
            A26Municipio_Nome = BC000513_A26Municipio_Nome[0];
            A23Estado_UF = BC000513_A23Estado_UF[0];
            pr_default.close(11);
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV15Estado_UF = A23Estado_UF;
            }
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000514 */
            pr_default.execute(12, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Redmine"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor BC000515 */
            pr_default.execute(13, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratante Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
            /* Using cursor BC000516 */
            pr_default.execute(14, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Area de Trabalho"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
         }
      }

      protected void EndLevel05124( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete05124( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart05124( )
      {
         /* Scan By routine */
         /* Using cursor BC000517 */
         pr_default.execute(15, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
         RcdFound124 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound124 = 1;
            A29Contratante_Codigo = BC000517_A29Contratante_Codigo[0];
            n29Contratante_Codigo = BC000517_n29Contratante_Codigo[0];
            A550Contratante_EmailSdaKey = BC000517_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = BC000517_n550Contratante_EmailSdaKey[0];
            A549Contratante_EmailSdaPass = BC000517_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = BC000517_n549Contratante_EmailSdaPass[0];
            A12Contratante_CNPJ = BC000517_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = BC000517_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = BC000517_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = BC000517_n9Contratante_RazaoSocial[0];
            A10Contratante_NomeFantasia = BC000517_A10Contratante_NomeFantasia[0];
            A11Contratante_IE = BC000517_A11Contratante_IE[0];
            A13Contratante_WebSite = BC000517_A13Contratante_WebSite[0];
            n13Contratante_WebSite = BC000517_n13Contratante_WebSite[0];
            A14Contratante_Email = BC000517_A14Contratante_Email[0];
            n14Contratante_Email = BC000517_n14Contratante_Email[0];
            A31Contratante_Telefone = BC000517_A31Contratante_Telefone[0];
            A32Contratante_Ramal = BC000517_A32Contratante_Ramal[0];
            n32Contratante_Ramal = BC000517_n32Contratante_Ramal[0];
            A33Contratante_Fax = BC000517_A33Contratante_Fax[0];
            n33Contratante_Fax = BC000517_n33Contratante_Fax[0];
            A336Contratante_AgenciaNome = BC000517_A336Contratante_AgenciaNome[0];
            n336Contratante_AgenciaNome = BC000517_n336Contratante_AgenciaNome[0];
            A337Contratante_AgenciaNro = BC000517_A337Contratante_AgenciaNro[0];
            n337Contratante_AgenciaNro = BC000517_n337Contratante_AgenciaNro[0];
            A338Contratante_BancoNome = BC000517_A338Contratante_BancoNome[0];
            n338Contratante_BancoNome = BC000517_n338Contratante_BancoNome[0];
            A339Contratante_BancoNro = BC000517_A339Contratante_BancoNro[0];
            n339Contratante_BancoNro = BC000517_n339Contratante_BancoNro[0];
            A16Contratante_ContaCorrente = BC000517_A16Contratante_ContaCorrente[0];
            n16Contratante_ContaCorrente = BC000517_n16Contratante_ContaCorrente[0];
            A26Municipio_Nome = BC000517_A26Municipio_Nome[0];
            A30Contratante_Ativo = BC000517_A30Contratante_Ativo[0];
            A547Contratante_EmailSdaHost = BC000517_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = BC000517_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = BC000517_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = BC000517_n548Contratante_EmailSdaUser[0];
            A551Contratante_EmailSdaAut = BC000517_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = BC000517_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = BC000517_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = BC000517_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = BC000517_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = BC000517_n1048Contratante_EmailSdaSec[0];
            A593Contratante_OSAutomatica = BC000517_A593Contratante_OSAutomatica[0];
            A1652Contratante_SSAutomatica = BC000517_A1652Contratante_SSAutomatica[0];
            n1652Contratante_SSAutomatica = BC000517_n1652Contratante_SSAutomatica[0];
            A594Contratante_ExisteConferencia = BC000517_A594Contratante_ExisteConferencia[0];
            A1448Contratante_InicioDoExpediente = BC000517_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = BC000517_n1448Contratante_InicioDoExpediente[0];
            A1192Contratante_FimDoExpediente = BC000517_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = BC000517_n1192Contratante_FimDoExpediente[0];
            A1727Contratante_ServicoSSPadrao = BC000517_A1727Contratante_ServicoSSPadrao[0];
            n1727Contratante_ServicoSSPadrao = BC000517_n1727Contratante_ServicoSSPadrao[0];
            A1729Contratante_RetornaSS = BC000517_A1729Contratante_RetornaSS[0];
            n1729Contratante_RetornaSS = BC000517_n1729Contratante_RetornaSS[0];
            A1732Contratante_SSStatusPlanejamento = BC000517_A1732Contratante_SSStatusPlanejamento[0];
            n1732Contratante_SSStatusPlanejamento = BC000517_n1732Contratante_SSStatusPlanejamento[0];
            A1733Contratante_SSPrestadoraUnica = BC000517_A1733Contratante_SSPrestadoraUnica[0];
            n1733Contratante_SSPrestadoraUnica = BC000517_n1733Contratante_SSPrestadoraUnica[0];
            A1738Contratante_PropostaRqrSrv = BC000517_A1738Contratante_PropostaRqrSrv[0];
            n1738Contratante_PropostaRqrSrv = BC000517_n1738Contratante_PropostaRqrSrv[0];
            A1739Contratante_PropostaRqrPrz = BC000517_A1739Contratante_PropostaRqrPrz[0];
            n1739Contratante_PropostaRqrPrz = BC000517_n1739Contratante_PropostaRqrPrz[0];
            A1740Contratante_PropostaRqrEsf = BC000517_A1740Contratante_PropostaRqrEsf[0];
            n1740Contratante_PropostaRqrEsf = BC000517_n1740Contratante_PropostaRqrEsf[0];
            A1741Contratante_PropostaRqrCnt = BC000517_A1741Contratante_PropostaRqrCnt[0];
            n1741Contratante_PropostaRqrCnt = BC000517_n1741Contratante_PropostaRqrCnt[0];
            A1742Contratante_PropostaNvlCnt = BC000517_A1742Contratante_PropostaNvlCnt[0];
            n1742Contratante_PropostaNvlCnt = BC000517_n1742Contratante_PropostaNvlCnt[0];
            A1757Contratante_OSGeraOS = BC000517_A1757Contratante_OSGeraOS[0];
            n1757Contratante_OSGeraOS = BC000517_n1757Contratante_OSGeraOS[0];
            A1758Contratante_OSGeraTRP = BC000517_A1758Contratante_OSGeraTRP[0];
            n1758Contratante_OSGeraTRP = BC000517_n1758Contratante_OSGeraTRP[0];
            A1759Contratante_OSGeraTH = BC000517_A1759Contratante_OSGeraTH[0];
            n1759Contratante_OSGeraTH = BC000517_n1759Contratante_OSGeraTH[0];
            A1760Contratante_OSGeraTRD = BC000517_A1760Contratante_OSGeraTRD[0];
            n1760Contratante_OSGeraTRD = BC000517_n1760Contratante_OSGeraTRD[0];
            A1761Contratante_OSGeraTR = BC000517_A1761Contratante_OSGeraTR[0];
            n1761Contratante_OSGeraTR = BC000517_n1761Contratante_OSGeraTR[0];
            A1803Contratante_OSHmlgComPnd = BC000517_A1803Contratante_OSHmlgComPnd[0];
            n1803Contratante_OSHmlgComPnd = BC000517_n1803Contratante_OSHmlgComPnd[0];
            A1805Contratante_AtivoCirculante = BC000517_A1805Contratante_AtivoCirculante[0];
            n1805Contratante_AtivoCirculante = BC000517_n1805Contratante_AtivoCirculante[0];
            A1806Contratante_PassivoCirculante = BC000517_A1806Contratante_PassivoCirculante[0];
            n1806Contratante_PassivoCirculante = BC000517_n1806Contratante_PassivoCirculante[0];
            A1807Contratante_PatrimonioLiquido = BC000517_A1807Contratante_PatrimonioLiquido[0];
            n1807Contratante_PatrimonioLiquido = BC000517_n1807Contratante_PatrimonioLiquido[0];
            A1808Contratante_ReceitaBruta = BC000517_A1808Contratante_ReceitaBruta[0];
            n1808Contratante_ReceitaBruta = BC000517_n1808Contratante_ReceitaBruta[0];
            A1822Contratante_UsaOSistema = BC000517_A1822Contratante_UsaOSistema[0];
            n1822Contratante_UsaOSistema = BC000517_n1822Contratante_UsaOSistema[0];
            A2034Contratante_TtlRltGerencial = BC000517_A2034Contratante_TtlRltGerencial[0];
            n2034Contratante_TtlRltGerencial = BC000517_n2034Contratante_TtlRltGerencial[0];
            A2084Contratante_PrzAoRtr = BC000517_A2084Contratante_PrzAoRtr[0];
            n2084Contratante_PrzAoRtr = BC000517_n2084Contratante_PrzAoRtr[0];
            A2083Contratante_PrzActRtr = BC000517_A2083Contratante_PrzActRtr[0];
            n2083Contratante_PrzActRtr = BC000517_n2083Contratante_PrzActRtr[0];
            A2085Contratante_RequerOrigem = BC000517_A2085Contratante_RequerOrigem[0];
            n2085Contratante_RequerOrigem = BC000517_n2085Contratante_RequerOrigem[0];
            A2089Contratante_SelecionaResponsavelOS = BC000517_A2089Contratante_SelecionaResponsavelOS[0];
            A2090Contratante_ExibePF = BC000517_A2090Contratante_ExibePF[0];
            A1126Contratante_LogoTipoArq = BC000517_A1126Contratante_LogoTipoArq[0];
            n1126Contratante_LogoTipoArq = BC000517_n1126Contratante_LogoTipoArq[0];
            A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
            A1125Contratante_LogoNomeArq = BC000517_A1125Contratante_LogoNomeArq[0];
            n1125Contratante_LogoNomeArq = BC000517_n1125Contratante_LogoNomeArq[0];
            A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
            A25Municipio_Codigo = BC000517_A25Municipio_Codigo[0];
            n25Municipio_Codigo = BC000517_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = BC000517_A335Contratante_PessoaCod[0];
            A23Estado_UF = BC000517_A23Estado_UF[0];
            A1124Contratante_LogoArquivo = BC000517_A1124Contratante_LogoArquivo[0];
            n1124Contratante_LogoArquivo = BC000517_n1124Contratante_LogoArquivo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext05124( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound124 = 0;
         ScanKeyLoad05124( ) ;
      }

      protected void ScanKeyLoad05124( )
      {
         sMode124 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound124 = 1;
            A29Contratante_Codigo = BC000517_A29Contratante_Codigo[0];
            n29Contratante_Codigo = BC000517_n29Contratante_Codigo[0];
            A550Contratante_EmailSdaKey = BC000517_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = BC000517_n550Contratante_EmailSdaKey[0];
            A549Contratante_EmailSdaPass = BC000517_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = BC000517_n549Contratante_EmailSdaPass[0];
            A12Contratante_CNPJ = BC000517_A12Contratante_CNPJ[0];
            n12Contratante_CNPJ = BC000517_n12Contratante_CNPJ[0];
            A9Contratante_RazaoSocial = BC000517_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = BC000517_n9Contratante_RazaoSocial[0];
            A10Contratante_NomeFantasia = BC000517_A10Contratante_NomeFantasia[0];
            A11Contratante_IE = BC000517_A11Contratante_IE[0];
            A13Contratante_WebSite = BC000517_A13Contratante_WebSite[0];
            n13Contratante_WebSite = BC000517_n13Contratante_WebSite[0];
            A14Contratante_Email = BC000517_A14Contratante_Email[0];
            n14Contratante_Email = BC000517_n14Contratante_Email[0];
            A31Contratante_Telefone = BC000517_A31Contratante_Telefone[0];
            A32Contratante_Ramal = BC000517_A32Contratante_Ramal[0];
            n32Contratante_Ramal = BC000517_n32Contratante_Ramal[0];
            A33Contratante_Fax = BC000517_A33Contratante_Fax[0];
            n33Contratante_Fax = BC000517_n33Contratante_Fax[0];
            A336Contratante_AgenciaNome = BC000517_A336Contratante_AgenciaNome[0];
            n336Contratante_AgenciaNome = BC000517_n336Contratante_AgenciaNome[0];
            A337Contratante_AgenciaNro = BC000517_A337Contratante_AgenciaNro[0];
            n337Contratante_AgenciaNro = BC000517_n337Contratante_AgenciaNro[0];
            A338Contratante_BancoNome = BC000517_A338Contratante_BancoNome[0];
            n338Contratante_BancoNome = BC000517_n338Contratante_BancoNome[0];
            A339Contratante_BancoNro = BC000517_A339Contratante_BancoNro[0];
            n339Contratante_BancoNro = BC000517_n339Contratante_BancoNro[0];
            A16Contratante_ContaCorrente = BC000517_A16Contratante_ContaCorrente[0];
            n16Contratante_ContaCorrente = BC000517_n16Contratante_ContaCorrente[0];
            A26Municipio_Nome = BC000517_A26Municipio_Nome[0];
            A30Contratante_Ativo = BC000517_A30Contratante_Ativo[0];
            A547Contratante_EmailSdaHost = BC000517_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = BC000517_n547Contratante_EmailSdaHost[0];
            A548Contratante_EmailSdaUser = BC000517_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = BC000517_n548Contratante_EmailSdaUser[0];
            A551Contratante_EmailSdaAut = BC000517_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = BC000517_n551Contratante_EmailSdaAut[0];
            A552Contratante_EmailSdaPort = BC000517_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = BC000517_n552Contratante_EmailSdaPort[0];
            A1048Contratante_EmailSdaSec = BC000517_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = BC000517_n1048Contratante_EmailSdaSec[0];
            A593Contratante_OSAutomatica = BC000517_A593Contratante_OSAutomatica[0];
            A1652Contratante_SSAutomatica = BC000517_A1652Contratante_SSAutomatica[0];
            n1652Contratante_SSAutomatica = BC000517_n1652Contratante_SSAutomatica[0];
            A594Contratante_ExisteConferencia = BC000517_A594Contratante_ExisteConferencia[0];
            A1448Contratante_InicioDoExpediente = BC000517_A1448Contratante_InicioDoExpediente[0];
            n1448Contratante_InicioDoExpediente = BC000517_n1448Contratante_InicioDoExpediente[0];
            A1192Contratante_FimDoExpediente = BC000517_A1192Contratante_FimDoExpediente[0];
            n1192Contratante_FimDoExpediente = BC000517_n1192Contratante_FimDoExpediente[0];
            A1727Contratante_ServicoSSPadrao = BC000517_A1727Contratante_ServicoSSPadrao[0];
            n1727Contratante_ServicoSSPadrao = BC000517_n1727Contratante_ServicoSSPadrao[0];
            A1729Contratante_RetornaSS = BC000517_A1729Contratante_RetornaSS[0];
            n1729Contratante_RetornaSS = BC000517_n1729Contratante_RetornaSS[0];
            A1732Contratante_SSStatusPlanejamento = BC000517_A1732Contratante_SSStatusPlanejamento[0];
            n1732Contratante_SSStatusPlanejamento = BC000517_n1732Contratante_SSStatusPlanejamento[0];
            A1733Contratante_SSPrestadoraUnica = BC000517_A1733Contratante_SSPrestadoraUnica[0];
            n1733Contratante_SSPrestadoraUnica = BC000517_n1733Contratante_SSPrestadoraUnica[0];
            A1738Contratante_PropostaRqrSrv = BC000517_A1738Contratante_PropostaRqrSrv[0];
            n1738Contratante_PropostaRqrSrv = BC000517_n1738Contratante_PropostaRqrSrv[0];
            A1739Contratante_PropostaRqrPrz = BC000517_A1739Contratante_PropostaRqrPrz[0];
            n1739Contratante_PropostaRqrPrz = BC000517_n1739Contratante_PropostaRqrPrz[0];
            A1740Contratante_PropostaRqrEsf = BC000517_A1740Contratante_PropostaRqrEsf[0];
            n1740Contratante_PropostaRqrEsf = BC000517_n1740Contratante_PropostaRqrEsf[0];
            A1741Contratante_PropostaRqrCnt = BC000517_A1741Contratante_PropostaRqrCnt[0];
            n1741Contratante_PropostaRqrCnt = BC000517_n1741Contratante_PropostaRqrCnt[0];
            A1742Contratante_PropostaNvlCnt = BC000517_A1742Contratante_PropostaNvlCnt[0];
            n1742Contratante_PropostaNvlCnt = BC000517_n1742Contratante_PropostaNvlCnt[0];
            A1757Contratante_OSGeraOS = BC000517_A1757Contratante_OSGeraOS[0];
            n1757Contratante_OSGeraOS = BC000517_n1757Contratante_OSGeraOS[0];
            A1758Contratante_OSGeraTRP = BC000517_A1758Contratante_OSGeraTRP[0];
            n1758Contratante_OSGeraTRP = BC000517_n1758Contratante_OSGeraTRP[0];
            A1759Contratante_OSGeraTH = BC000517_A1759Contratante_OSGeraTH[0];
            n1759Contratante_OSGeraTH = BC000517_n1759Contratante_OSGeraTH[0];
            A1760Contratante_OSGeraTRD = BC000517_A1760Contratante_OSGeraTRD[0];
            n1760Contratante_OSGeraTRD = BC000517_n1760Contratante_OSGeraTRD[0];
            A1761Contratante_OSGeraTR = BC000517_A1761Contratante_OSGeraTR[0];
            n1761Contratante_OSGeraTR = BC000517_n1761Contratante_OSGeraTR[0];
            A1803Contratante_OSHmlgComPnd = BC000517_A1803Contratante_OSHmlgComPnd[0];
            n1803Contratante_OSHmlgComPnd = BC000517_n1803Contratante_OSHmlgComPnd[0];
            A1805Contratante_AtivoCirculante = BC000517_A1805Contratante_AtivoCirculante[0];
            n1805Contratante_AtivoCirculante = BC000517_n1805Contratante_AtivoCirculante[0];
            A1806Contratante_PassivoCirculante = BC000517_A1806Contratante_PassivoCirculante[0];
            n1806Contratante_PassivoCirculante = BC000517_n1806Contratante_PassivoCirculante[0];
            A1807Contratante_PatrimonioLiquido = BC000517_A1807Contratante_PatrimonioLiquido[0];
            n1807Contratante_PatrimonioLiquido = BC000517_n1807Contratante_PatrimonioLiquido[0];
            A1808Contratante_ReceitaBruta = BC000517_A1808Contratante_ReceitaBruta[0];
            n1808Contratante_ReceitaBruta = BC000517_n1808Contratante_ReceitaBruta[0];
            A1822Contratante_UsaOSistema = BC000517_A1822Contratante_UsaOSistema[0];
            n1822Contratante_UsaOSistema = BC000517_n1822Contratante_UsaOSistema[0];
            A2034Contratante_TtlRltGerencial = BC000517_A2034Contratante_TtlRltGerencial[0];
            n2034Contratante_TtlRltGerencial = BC000517_n2034Contratante_TtlRltGerencial[0];
            A2084Contratante_PrzAoRtr = BC000517_A2084Contratante_PrzAoRtr[0];
            n2084Contratante_PrzAoRtr = BC000517_n2084Contratante_PrzAoRtr[0];
            A2083Contratante_PrzActRtr = BC000517_A2083Contratante_PrzActRtr[0];
            n2083Contratante_PrzActRtr = BC000517_n2083Contratante_PrzActRtr[0];
            A2085Contratante_RequerOrigem = BC000517_A2085Contratante_RequerOrigem[0];
            n2085Contratante_RequerOrigem = BC000517_n2085Contratante_RequerOrigem[0];
            A2089Contratante_SelecionaResponsavelOS = BC000517_A2089Contratante_SelecionaResponsavelOS[0];
            A2090Contratante_ExibePF = BC000517_A2090Contratante_ExibePF[0];
            A1126Contratante_LogoTipoArq = BC000517_A1126Contratante_LogoTipoArq[0];
            n1126Contratante_LogoTipoArq = BC000517_n1126Contratante_LogoTipoArq[0];
            A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
            A1125Contratante_LogoNomeArq = BC000517_A1125Contratante_LogoNomeArq[0];
            n1125Contratante_LogoNomeArq = BC000517_n1125Contratante_LogoNomeArq[0];
            A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
            A25Municipio_Codigo = BC000517_A25Municipio_Codigo[0];
            n25Municipio_Codigo = BC000517_n25Municipio_Codigo[0];
            A335Contratante_PessoaCod = BC000517_A335Contratante_PessoaCod[0];
            A23Estado_UF = BC000517_A23Estado_UF[0];
            A1124Contratante_LogoArquivo = BC000517_A1124Contratante_LogoArquivo[0];
            n1124Contratante_LogoArquivo = BC000517_n1124Contratante_LogoArquivo[0];
         }
         Gx_mode = sMode124;
      }

      protected void ScanKeyEnd05124( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm05124( )
      {
         /* After Confirm Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( StringUtil.StrCmp(A549Contratante_EmailSdaPass, O549Contratante_EmailSdaPass) != 0 ) )
         {
            A550Contratante_EmailSdaKey = Crypto.GetEncryptionKey( );
            n550Contratante_EmailSdaKey = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( StringUtil.StrCmp(A549Contratante_EmailSdaPass, O549Contratante_EmailSdaPass) != 0 ) )
         {
            A549Contratante_EmailSdaPass = Crypto.Encrypt64( O549Contratante_EmailSdaPass, A550Contratante_EmailSdaKey);
            n549Contratante_EmailSdaPass = false;
         }
      }

      protected void BeforeInsert05124( )
      {
         /* Before Insert Rules */
         if ( (0==A25Municipio_Codigo) )
         {
            A25Municipio_Codigo = 0;
            n25Municipio_Codigo = false;
            n25Municipio_Codigo = true;
         }
      }

      protected void BeforeUpdate05124( )
      {
         /* Before Update Rules */
         new loadauditcontratante(context ).execute(  "Y", ref  AV29AuditingObject,  A29Contratante_Codigo,  Gx_mode) ;
         if ( (0==A25Municipio_Codigo) )
         {
            A25Municipio_Codigo = 0;
            n25Municipio_Codigo = false;
            n25Municipio_Codigo = true;
         }
      }

      protected void BeforeDelete05124( )
      {
         /* Before Delete Rules */
         new loadauditcontratante(context ).execute(  "Y", ref  AV29AuditingObject,  A29Contratante_Codigo,  Gx_mode) ;
      }

      protected void BeforeComplete05124( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontratante(context ).execute(  "N", ref  AV29AuditingObject,  A29Contratante_Codigo,  Gx_mode) ;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontratante(context ).execute(  "N", ref  AV29AuditingObject,  A29Contratante_Codigo,  Gx_mode) ;
         }
      }

      protected void BeforeValidate05124( )
      {
         /* Before Validate Rules */
         if ( StringUtil.StrCmp(A549Contratante_EmailSdaPass, O549Contratante_EmailSdaPass) != 0 )
         {
            A550Contratante_EmailSdaKey = Crypto.GetEncryptionKey( );
            n550Contratante_EmailSdaKey = false;
         }
         if ( StringUtil.StrCmp(A549Contratante_EmailSdaPass, O549Contratante_EmailSdaPass) != 0 )
         {
            A549Contratante_EmailSdaPass = Crypto.Encrypt64( StringUtil.Trim( A549Contratante_EmailSdaPass), A550Contratante_EmailSdaKey);
            n549Contratante_EmailSdaPass = false;
         }
      }

      protected void DisableAttributes05124( )
      {
      }

      protected void AddRow05124( )
      {
         VarsToRow124( bcContratante) ;
      }

      protected void ReadRow05124( )
      {
         RowToVars124( bcContratante, 1) ;
      }

      protected void InitializeNonKey05124( )
      {
         AV29AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV15Estado_UF = "";
         A550Contratante_EmailSdaKey = "";
         n550Contratante_EmailSdaKey = false;
         A549Contratante_EmailSdaPass = "";
         n549Contratante_EmailSdaPass = false;
         A335Contratante_PessoaCod = 0;
         A12Contratante_CNPJ = "";
         n12Contratante_CNPJ = false;
         A9Contratante_RazaoSocial = "";
         n9Contratante_RazaoSocial = false;
         A10Contratante_NomeFantasia = "";
         A11Contratante_IE = "";
         A13Contratante_WebSite = "";
         n13Contratante_WebSite = false;
         A14Contratante_Email = "";
         n14Contratante_Email = false;
         A31Contratante_Telefone = "";
         A32Contratante_Ramal = "";
         n32Contratante_Ramal = false;
         A33Contratante_Fax = "";
         n33Contratante_Fax = false;
         A336Contratante_AgenciaNome = "";
         n336Contratante_AgenciaNome = false;
         A337Contratante_AgenciaNro = "";
         n337Contratante_AgenciaNro = false;
         A338Contratante_BancoNome = "";
         n338Contratante_BancoNome = false;
         A339Contratante_BancoNro = "";
         n339Contratante_BancoNro = false;
         A16Contratante_ContaCorrente = "";
         n16Contratante_ContaCorrente = false;
         A25Municipio_Codigo = 0;
         n25Municipio_Codigo = false;
         A26Municipio_Nome = "";
         A23Estado_UF = "";
         A547Contratante_EmailSdaHost = "";
         n547Contratante_EmailSdaHost = false;
         A548Contratante_EmailSdaUser = "";
         n548Contratante_EmailSdaUser = false;
         A552Contratante_EmailSdaPort = 0;
         n552Contratante_EmailSdaPort = false;
         A1048Contratante_EmailSdaSec = 0;
         n1048Contratante_EmailSdaSec = false;
         A593Contratante_OSAutomatica = false;
         A1652Contratante_SSAutomatica = false;
         n1652Contratante_SSAutomatica = false;
         A594Contratante_ExisteConferencia = false;
         A1124Contratante_LogoArquivo = "";
         n1124Contratante_LogoArquivo = false;
         A1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         n1448Contratante_InicioDoExpediente = false;
         A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         n1192Contratante_FimDoExpediente = false;
         A1727Contratante_ServicoSSPadrao = 0;
         n1727Contratante_ServicoSSPadrao = false;
         A1729Contratante_RetornaSS = false;
         n1729Contratante_RetornaSS = false;
         A1733Contratante_SSPrestadoraUnica = false;
         n1733Contratante_SSPrestadoraUnica = false;
         A1739Contratante_PropostaRqrPrz = false;
         n1739Contratante_PropostaRqrPrz = false;
         A1740Contratante_PropostaRqrEsf = false;
         n1740Contratante_PropostaRqrEsf = false;
         A1741Contratante_PropostaRqrCnt = false;
         n1741Contratante_PropostaRqrCnt = false;
         A1757Contratante_OSGeraOS = false;
         n1757Contratante_OSGeraOS = false;
         A1758Contratante_OSGeraTRP = false;
         n1758Contratante_OSGeraTRP = false;
         A1759Contratante_OSGeraTH = false;
         n1759Contratante_OSGeraTH = false;
         A1760Contratante_OSGeraTRD = false;
         n1760Contratante_OSGeraTRD = false;
         A1761Contratante_OSGeraTR = false;
         n1761Contratante_OSGeraTR = false;
         A1803Contratante_OSHmlgComPnd = false;
         n1803Contratante_OSHmlgComPnd = false;
         A1805Contratante_AtivoCirculante = 0;
         n1805Contratante_AtivoCirculante = false;
         A1806Contratante_PassivoCirculante = 0;
         n1806Contratante_PassivoCirculante = false;
         A1807Contratante_PatrimonioLiquido = 0;
         n1807Contratante_PatrimonioLiquido = false;
         A1808Contratante_ReceitaBruta = 0;
         n1808Contratante_ReceitaBruta = false;
         A2084Contratante_PrzAoRtr = 0;
         n2084Contratante_PrzAoRtr = false;
         A2083Contratante_PrzActRtr = 0;
         n2083Contratante_PrzActRtr = false;
         A2089Contratante_SelecionaResponsavelOS = false;
         A2090Contratante_ExibePF = false;
         A1126Contratante_LogoTipoArq = "";
         n1126Contratante_LogoTipoArq = false;
         A1125Contratante_LogoNomeArq = "";
         n1125Contratante_LogoNomeArq = false;
         A30Contratante_Ativo = true;
         A551Contratante_EmailSdaAut = false;
         n551Contratante_EmailSdaAut = false;
         A1732Contratante_SSStatusPlanejamento = "B";
         n1732Contratante_SSStatusPlanejamento = false;
         A1738Contratante_PropostaRqrSrv = true;
         n1738Contratante_PropostaRqrSrv = false;
         A1742Contratante_PropostaNvlCnt = 0;
         n1742Contratante_PropostaNvlCnt = false;
         A1822Contratante_UsaOSistema = false;
         n1822Contratante_UsaOSistema = false;
         A2034Contratante_TtlRltGerencial = "Relat�rio Gerencial";
         n2034Contratante_TtlRltGerencial = false;
         A2085Contratante_RequerOrigem = true;
         n2085Contratante_RequerOrigem = false;
         O549Contratante_EmailSdaPass = A549Contratante_EmailSdaPass;
         n549Contratante_EmailSdaPass = false;
         Z550Contratante_EmailSdaKey = "";
         Z549Contratante_EmailSdaPass = "";
         Z10Contratante_NomeFantasia = "";
         Z11Contratante_IE = "";
         Z13Contratante_WebSite = "";
         Z14Contratante_Email = "";
         Z31Contratante_Telefone = "";
         Z32Contratante_Ramal = "";
         Z33Contratante_Fax = "";
         Z336Contratante_AgenciaNome = "";
         Z337Contratante_AgenciaNro = "";
         Z338Contratante_BancoNome = "";
         Z339Contratante_BancoNro = "";
         Z16Contratante_ContaCorrente = "";
         Z30Contratante_Ativo = false;
         Z547Contratante_EmailSdaHost = "";
         Z548Contratante_EmailSdaUser = "";
         Z551Contratante_EmailSdaAut = false;
         Z552Contratante_EmailSdaPort = 0;
         Z1048Contratante_EmailSdaSec = 0;
         Z593Contratante_OSAutomatica = false;
         Z1652Contratante_SSAutomatica = false;
         Z594Contratante_ExisteConferencia = false;
         Z1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         Z1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         Z1727Contratante_ServicoSSPadrao = 0;
         Z1729Contratante_RetornaSS = false;
         Z1732Contratante_SSStatusPlanejamento = "";
         Z1733Contratante_SSPrestadoraUnica = false;
         Z1738Contratante_PropostaRqrSrv = false;
         Z1739Contratante_PropostaRqrPrz = false;
         Z1740Contratante_PropostaRqrEsf = false;
         Z1741Contratante_PropostaRqrCnt = false;
         Z1742Contratante_PropostaNvlCnt = 0;
         Z1757Contratante_OSGeraOS = false;
         Z1758Contratante_OSGeraTRP = false;
         Z1759Contratante_OSGeraTH = false;
         Z1760Contratante_OSGeraTRD = false;
         Z1761Contratante_OSGeraTR = false;
         Z1803Contratante_OSHmlgComPnd = false;
         Z1805Contratante_AtivoCirculante = 0;
         Z1806Contratante_PassivoCirculante = 0;
         Z1807Contratante_PatrimonioLiquido = 0;
         Z1808Contratante_ReceitaBruta = 0;
         Z1822Contratante_UsaOSistema = false;
         Z2034Contratante_TtlRltGerencial = "";
         Z2084Contratante_PrzAoRtr = 0;
         Z2083Contratante_PrzActRtr = 0;
         Z2085Contratante_RequerOrigem = false;
         Z2089Contratante_SelecionaResponsavelOS = false;
         Z2090Contratante_ExibePF = false;
         Z25Municipio_Codigo = 0;
         Z335Contratante_PessoaCod = 0;
      }

      protected void InitAll05124( )
      {
         A29Contratante_Codigo = 0;
         n29Contratante_Codigo = false;
         InitializeNonKey05124( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2085Contratante_RequerOrigem = i2085Contratante_RequerOrigem;
         n2085Contratante_RequerOrigem = false;
         A2034Contratante_TtlRltGerencial = i2034Contratante_TtlRltGerencial;
         n2034Contratante_TtlRltGerencial = false;
         A1822Contratante_UsaOSistema = i1822Contratante_UsaOSistema;
         n1822Contratante_UsaOSistema = false;
         A1742Contratante_PropostaNvlCnt = i1742Contratante_PropostaNvlCnt;
         n1742Contratante_PropostaNvlCnt = false;
         A1738Contratante_PropostaRqrSrv = i1738Contratante_PropostaRqrSrv;
         n1738Contratante_PropostaRqrSrv = false;
         A1732Contratante_SSStatusPlanejamento = i1732Contratante_SSStatusPlanejamento;
         n1732Contratante_SSStatusPlanejamento = false;
         A551Contratante_EmailSdaAut = i551Contratante_EmailSdaAut;
         n551Contratante_EmailSdaAut = false;
         A30Contratante_Ativo = i30Contratante_Ativo;
      }

      public void VarsToRow124( SdtContratante obj124 )
      {
         obj124.gxTpr_Mode = Gx_mode;
         obj124.gxTpr_Contratante_emailsdakey = A550Contratante_EmailSdaKey;
         obj124.gxTpr_Contratante_emailsdapass = A549Contratante_EmailSdaPass;
         obj124.gxTpr_Contratante_pessoacod = A335Contratante_PessoaCod;
         obj124.gxTpr_Contratante_cnpj = A12Contratante_CNPJ;
         obj124.gxTpr_Contratante_razaosocial = A9Contratante_RazaoSocial;
         obj124.gxTpr_Contratante_nomefantasia = A10Contratante_NomeFantasia;
         obj124.gxTpr_Contratante_ie = A11Contratante_IE;
         obj124.gxTpr_Contratante_website = A13Contratante_WebSite;
         obj124.gxTpr_Contratante_email = A14Contratante_Email;
         obj124.gxTpr_Contratante_telefone = A31Contratante_Telefone;
         obj124.gxTpr_Contratante_ramal = A32Contratante_Ramal;
         obj124.gxTpr_Contratante_fax = A33Contratante_Fax;
         obj124.gxTpr_Contratante_agencianome = A336Contratante_AgenciaNome;
         obj124.gxTpr_Contratante_agencianro = A337Contratante_AgenciaNro;
         obj124.gxTpr_Contratante_banconome = A338Contratante_BancoNome;
         obj124.gxTpr_Contratante_banconro = A339Contratante_BancoNro;
         obj124.gxTpr_Contratante_contacorrente = A16Contratante_ContaCorrente;
         obj124.gxTpr_Municipio_codigo = A25Municipio_Codigo;
         obj124.gxTpr_Municipio_nome = A26Municipio_Nome;
         obj124.gxTpr_Estado_uf = A23Estado_UF;
         obj124.gxTpr_Contratante_emailsdahost = A547Contratante_EmailSdaHost;
         obj124.gxTpr_Contratante_emailsdauser = A548Contratante_EmailSdaUser;
         obj124.gxTpr_Contratante_emailsdaport = A552Contratante_EmailSdaPort;
         obj124.gxTpr_Contratante_emailsdasec = A1048Contratante_EmailSdaSec;
         obj124.gxTpr_Contratante_osautomatica = A593Contratante_OSAutomatica;
         obj124.gxTpr_Contratante_ssautomatica = A1652Contratante_SSAutomatica;
         obj124.gxTpr_Contratante_existeconferencia = A594Contratante_ExisteConferencia;
         obj124.gxTpr_Contratante_logoarquivo = A1124Contratante_LogoArquivo;
         obj124.gxTpr_Contratante_iniciodoexpediente = A1448Contratante_InicioDoExpediente;
         obj124.gxTpr_Contratante_fimdoexpediente = A1192Contratante_FimDoExpediente;
         obj124.gxTpr_Contratante_servicosspadrao = A1727Contratante_ServicoSSPadrao;
         obj124.gxTpr_Contratante_retornass = A1729Contratante_RetornaSS;
         obj124.gxTpr_Contratante_ssprestadoraunica = A1733Contratante_SSPrestadoraUnica;
         obj124.gxTpr_Contratante_propostarqrprz = A1739Contratante_PropostaRqrPrz;
         obj124.gxTpr_Contratante_propostarqresf = A1740Contratante_PropostaRqrEsf;
         obj124.gxTpr_Contratante_propostarqrcnt = A1741Contratante_PropostaRqrCnt;
         obj124.gxTpr_Contratante_osgeraos = A1757Contratante_OSGeraOS;
         obj124.gxTpr_Contratante_osgeratrp = A1758Contratante_OSGeraTRP;
         obj124.gxTpr_Contratante_osgerath = A1759Contratante_OSGeraTH;
         obj124.gxTpr_Contratante_osgeratrd = A1760Contratante_OSGeraTRD;
         obj124.gxTpr_Contratante_osgeratr = A1761Contratante_OSGeraTR;
         obj124.gxTpr_Contratante_oshmlgcompnd = A1803Contratante_OSHmlgComPnd;
         obj124.gxTpr_Contratante_ativocirculante = A1805Contratante_AtivoCirculante;
         obj124.gxTpr_Contratante_passivocirculante = A1806Contratante_PassivoCirculante;
         obj124.gxTpr_Contratante_patrimonioliquido = A1807Contratante_PatrimonioLiquido;
         obj124.gxTpr_Contratante_receitabruta = A1808Contratante_ReceitaBruta;
         obj124.gxTpr_Contratante_przaortr = A2084Contratante_PrzAoRtr;
         obj124.gxTpr_Contratante_przactrtr = A2083Contratante_PrzActRtr;
         obj124.gxTpr_Contratante_selecionaresponsavelos = A2089Contratante_SelecionaResponsavelOS;
         obj124.gxTpr_Contratante_exibepf = A2090Contratante_ExibePF;
         obj124.gxTpr_Contratante_logotipoarq = A1126Contratante_LogoTipoArq;
         obj124.gxTpr_Contratante_logonomearq = A1125Contratante_LogoNomeArq;
         obj124.gxTpr_Contratante_ativo = A30Contratante_Ativo;
         obj124.gxTpr_Contratante_emailsdaaut = A551Contratante_EmailSdaAut;
         obj124.gxTpr_Contratante_ssstatusplanejamento = A1732Contratante_SSStatusPlanejamento;
         obj124.gxTpr_Contratante_propostarqrsrv = A1738Contratante_PropostaRqrSrv;
         obj124.gxTpr_Contratante_propostanvlcnt = A1742Contratante_PropostaNvlCnt;
         obj124.gxTpr_Contratante_usaosistema = A1822Contratante_UsaOSistema;
         obj124.gxTpr_Contratante_ttlrltgerencial = A2034Contratante_TtlRltGerencial;
         obj124.gxTpr_Contratante_requerorigem = A2085Contratante_RequerOrigem;
         obj124.gxTpr_Contratante_codigo = A29Contratante_Codigo;
         obj124.gxTpr_Contratante_codigo_Z = Z29Contratante_Codigo;
         obj124.gxTpr_Contratante_pessoacod_Z = Z335Contratante_PessoaCod;
         obj124.gxTpr_Contratante_cnpj_Z = Z12Contratante_CNPJ;
         obj124.gxTpr_Contratante_razaosocial_Z = Z9Contratante_RazaoSocial;
         obj124.gxTpr_Contratante_nomefantasia_Z = Z10Contratante_NomeFantasia;
         obj124.gxTpr_Contratante_ie_Z = Z11Contratante_IE;
         obj124.gxTpr_Contratante_website_Z = Z13Contratante_WebSite;
         obj124.gxTpr_Contratante_email_Z = Z14Contratante_Email;
         obj124.gxTpr_Contratante_telefone_Z = Z31Contratante_Telefone;
         obj124.gxTpr_Contratante_ramal_Z = Z32Contratante_Ramal;
         obj124.gxTpr_Contratante_fax_Z = Z33Contratante_Fax;
         obj124.gxTpr_Contratante_agencianome_Z = Z336Contratante_AgenciaNome;
         obj124.gxTpr_Contratante_agencianro_Z = Z337Contratante_AgenciaNro;
         obj124.gxTpr_Contratante_banconome_Z = Z338Contratante_BancoNome;
         obj124.gxTpr_Contratante_banconro_Z = Z339Contratante_BancoNro;
         obj124.gxTpr_Contratante_contacorrente_Z = Z16Contratante_ContaCorrente;
         obj124.gxTpr_Municipio_codigo_Z = Z25Municipio_Codigo;
         obj124.gxTpr_Municipio_nome_Z = Z26Municipio_Nome;
         obj124.gxTpr_Estado_uf_Z = Z23Estado_UF;
         obj124.gxTpr_Contratante_ativo_Z = Z30Contratante_Ativo;
         obj124.gxTpr_Contratante_emailsdahost_Z = Z547Contratante_EmailSdaHost;
         obj124.gxTpr_Contratante_emailsdauser_Z = Z548Contratante_EmailSdaUser;
         obj124.gxTpr_Contratante_emailsdapass_Z = Z549Contratante_EmailSdaPass;
         obj124.gxTpr_Contratante_emailsdakey_Z = Z550Contratante_EmailSdaKey;
         obj124.gxTpr_Contratante_emailsdaaut_Z = Z551Contratante_EmailSdaAut;
         obj124.gxTpr_Contratante_emailsdaport_Z = Z552Contratante_EmailSdaPort;
         obj124.gxTpr_Contratante_emailsdasec_Z = Z1048Contratante_EmailSdaSec;
         obj124.gxTpr_Contratante_osautomatica_Z = Z593Contratante_OSAutomatica;
         obj124.gxTpr_Contratante_ssautomatica_Z = Z1652Contratante_SSAutomatica;
         obj124.gxTpr_Contratante_existeconferencia_Z = Z594Contratante_ExisteConferencia;
         obj124.gxTpr_Contratante_logonomearq_Z = Z1125Contratante_LogoNomeArq;
         obj124.gxTpr_Contratante_logotipoarq_Z = Z1126Contratante_LogoTipoArq;
         obj124.gxTpr_Contratante_iniciodoexpediente_Z = Z1448Contratante_InicioDoExpediente;
         obj124.gxTpr_Contratante_fimdoexpediente_Z = Z1192Contratante_FimDoExpediente;
         obj124.gxTpr_Contratante_servicosspadrao_Z = Z1727Contratante_ServicoSSPadrao;
         obj124.gxTpr_Contratante_retornass_Z = Z1729Contratante_RetornaSS;
         obj124.gxTpr_Contratante_ssstatusplanejamento_Z = Z1732Contratante_SSStatusPlanejamento;
         obj124.gxTpr_Contratante_ssprestadoraunica_Z = Z1733Contratante_SSPrestadoraUnica;
         obj124.gxTpr_Contratante_propostarqrsrv_Z = Z1738Contratante_PropostaRqrSrv;
         obj124.gxTpr_Contratante_propostarqrprz_Z = Z1739Contratante_PropostaRqrPrz;
         obj124.gxTpr_Contratante_propostarqresf_Z = Z1740Contratante_PropostaRqrEsf;
         obj124.gxTpr_Contratante_propostarqrcnt_Z = Z1741Contratante_PropostaRqrCnt;
         obj124.gxTpr_Contratante_propostanvlcnt_Z = Z1742Contratante_PropostaNvlCnt;
         obj124.gxTpr_Contratante_osgeraos_Z = Z1757Contratante_OSGeraOS;
         obj124.gxTpr_Contratante_osgeratrp_Z = Z1758Contratante_OSGeraTRP;
         obj124.gxTpr_Contratante_osgerath_Z = Z1759Contratante_OSGeraTH;
         obj124.gxTpr_Contratante_osgeratrd_Z = Z1760Contratante_OSGeraTRD;
         obj124.gxTpr_Contratante_osgeratr_Z = Z1761Contratante_OSGeraTR;
         obj124.gxTpr_Contratante_oshmlgcompnd_Z = Z1803Contratante_OSHmlgComPnd;
         obj124.gxTpr_Contratante_ativocirculante_Z = Z1805Contratante_AtivoCirculante;
         obj124.gxTpr_Contratante_passivocirculante_Z = Z1806Contratante_PassivoCirculante;
         obj124.gxTpr_Contratante_patrimonioliquido_Z = Z1807Contratante_PatrimonioLiquido;
         obj124.gxTpr_Contratante_receitabruta_Z = Z1808Contratante_ReceitaBruta;
         obj124.gxTpr_Contratante_usaosistema_Z = Z1822Contratante_UsaOSistema;
         obj124.gxTpr_Contratante_ttlrltgerencial_Z = Z2034Contratante_TtlRltGerencial;
         obj124.gxTpr_Contratante_przaortr_Z = Z2084Contratante_PrzAoRtr;
         obj124.gxTpr_Contratante_przactrtr_Z = Z2083Contratante_PrzActRtr;
         obj124.gxTpr_Contratante_requerorigem_Z = Z2085Contratante_RequerOrigem;
         obj124.gxTpr_Contratante_selecionaresponsavelos_Z = Z2089Contratante_SelecionaResponsavelOS;
         obj124.gxTpr_Contratante_exibepf_Z = Z2090Contratante_ExibePF;
         obj124.gxTpr_Contratante_codigo_N = (short)(Convert.ToInt16(n29Contratante_Codigo));
         obj124.gxTpr_Contratante_cnpj_N = (short)(Convert.ToInt16(n12Contratante_CNPJ));
         obj124.gxTpr_Contratante_razaosocial_N = (short)(Convert.ToInt16(n9Contratante_RazaoSocial));
         obj124.gxTpr_Contratante_website_N = (short)(Convert.ToInt16(n13Contratante_WebSite));
         obj124.gxTpr_Contratante_email_N = (short)(Convert.ToInt16(n14Contratante_Email));
         obj124.gxTpr_Contratante_ramal_N = (short)(Convert.ToInt16(n32Contratante_Ramal));
         obj124.gxTpr_Contratante_fax_N = (short)(Convert.ToInt16(n33Contratante_Fax));
         obj124.gxTpr_Contratante_agencianome_N = (short)(Convert.ToInt16(n336Contratante_AgenciaNome));
         obj124.gxTpr_Contratante_agencianro_N = (short)(Convert.ToInt16(n337Contratante_AgenciaNro));
         obj124.gxTpr_Contratante_banconome_N = (short)(Convert.ToInt16(n338Contratante_BancoNome));
         obj124.gxTpr_Contratante_banconro_N = (short)(Convert.ToInt16(n339Contratante_BancoNro));
         obj124.gxTpr_Contratante_contacorrente_N = (short)(Convert.ToInt16(n16Contratante_ContaCorrente));
         obj124.gxTpr_Municipio_codigo_N = (short)(Convert.ToInt16(n25Municipio_Codigo));
         obj124.gxTpr_Contratante_emailsdahost_N = (short)(Convert.ToInt16(n547Contratante_EmailSdaHost));
         obj124.gxTpr_Contratante_emailsdauser_N = (short)(Convert.ToInt16(n548Contratante_EmailSdaUser));
         obj124.gxTpr_Contratante_emailsdapass_N = (short)(Convert.ToInt16(n549Contratante_EmailSdaPass));
         obj124.gxTpr_Contratante_emailsdakey_N = (short)(Convert.ToInt16(n550Contratante_EmailSdaKey));
         obj124.gxTpr_Contratante_emailsdaaut_N = (short)(Convert.ToInt16(n551Contratante_EmailSdaAut));
         obj124.gxTpr_Contratante_emailsdaport_N = (short)(Convert.ToInt16(n552Contratante_EmailSdaPort));
         obj124.gxTpr_Contratante_emailsdasec_N = (short)(Convert.ToInt16(n1048Contratante_EmailSdaSec));
         obj124.gxTpr_Contratante_ssautomatica_N = (short)(Convert.ToInt16(n1652Contratante_SSAutomatica));
         obj124.gxTpr_Contratante_logoarquivo_N = (short)(Convert.ToInt16(n1124Contratante_LogoArquivo));
         obj124.gxTpr_Contratante_logonomearq_N = (short)(Convert.ToInt16(n1125Contratante_LogoNomeArq));
         obj124.gxTpr_Contratante_logotipoarq_N = (short)(Convert.ToInt16(n1126Contratante_LogoTipoArq));
         obj124.gxTpr_Contratante_iniciodoexpediente_N = (short)(Convert.ToInt16(n1448Contratante_InicioDoExpediente));
         obj124.gxTpr_Contratante_fimdoexpediente_N = (short)(Convert.ToInt16(n1192Contratante_FimDoExpediente));
         obj124.gxTpr_Contratante_servicosspadrao_N = (short)(Convert.ToInt16(n1727Contratante_ServicoSSPadrao));
         obj124.gxTpr_Contratante_retornass_N = (short)(Convert.ToInt16(n1729Contratante_RetornaSS));
         obj124.gxTpr_Contratante_ssstatusplanejamento_N = (short)(Convert.ToInt16(n1732Contratante_SSStatusPlanejamento));
         obj124.gxTpr_Contratante_ssprestadoraunica_N = (short)(Convert.ToInt16(n1733Contratante_SSPrestadoraUnica));
         obj124.gxTpr_Contratante_propostarqrsrv_N = (short)(Convert.ToInt16(n1738Contratante_PropostaRqrSrv));
         obj124.gxTpr_Contratante_propostarqrprz_N = (short)(Convert.ToInt16(n1739Contratante_PropostaRqrPrz));
         obj124.gxTpr_Contratante_propostarqresf_N = (short)(Convert.ToInt16(n1740Contratante_PropostaRqrEsf));
         obj124.gxTpr_Contratante_propostarqrcnt_N = (short)(Convert.ToInt16(n1741Contratante_PropostaRqrCnt));
         obj124.gxTpr_Contratante_propostanvlcnt_N = (short)(Convert.ToInt16(n1742Contratante_PropostaNvlCnt));
         obj124.gxTpr_Contratante_osgeraos_N = (short)(Convert.ToInt16(n1757Contratante_OSGeraOS));
         obj124.gxTpr_Contratante_osgeratrp_N = (short)(Convert.ToInt16(n1758Contratante_OSGeraTRP));
         obj124.gxTpr_Contratante_osgerath_N = (short)(Convert.ToInt16(n1759Contratante_OSGeraTH));
         obj124.gxTpr_Contratante_osgeratrd_N = (short)(Convert.ToInt16(n1760Contratante_OSGeraTRD));
         obj124.gxTpr_Contratante_osgeratr_N = (short)(Convert.ToInt16(n1761Contratante_OSGeraTR));
         obj124.gxTpr_Contratante_oshmlgcompnd_N = (short)(Convert.ToInt16(n1803Contratante_OSHmlgComPnd));
         obj124.gxTpr_Contratante_ativocirculante_N = (short)(Convert.ToInt16(n1805Contratante_AtivoCirculante));
         obj124.gxTpr_Contratante_passivocirculante_N = (short)(Convert.ToInt16(n1806Contratante_PassivoCirculante));
         obj124.gxTpr_Contratante_patrimonioliquido_N = (short)(Convert.ToInt16(n1807Contratante_PatrimonioLiquido));
         obj124.gxTpr_Contratante_receitabruta_N = (short)(Convert.ToInt16(n1808Contratante_ReceitaBruta));
         obj124.gxTpr_Contratante_usaosistema_N = (short)(Convert.ToInt16(n1822Contratante_UsaOSistema));
         obj124.gxTpr_Contratante_ttlrltgerencial_N = (short)(Convert.ToInt16(n2034Contratante_TtlRltGerencial));
         obj124.gxTpr_Contratante_przaortr_N = (short)(Convert.ToInt16(n2084Contratante_PrzAoRtr));
         obj124.gxTpr_Contratante_przactrtr_N = (short)(Convert.ToInt16(n2083Contratante_PrzActRtr));
         obj124.gxTpr_Contratante_requerorigem_N = (short)(Convert.ToInt16(n2085Contratante_RequerOrigem));
         obj124.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow124( SdtContratante obj124 )
      {
         obj124.gxTpr_Contratante_codigo = A29Contratante_Codigo;
         return  ;
      }

      public void RowToVars124( SdtContratante obj124 ,
                                int forceLoad )
      {
         Gx_mode = obj124.gxTpr_Mode;
         A550Contratante_EmailSdaKey = obj124.gxTpr_Contratante_emailsdakey;
         n550Contratante_EmailSdaKey = false;
         A549Contratante_EmailSdaPass = obj124.gxTpr_Contratante_emailsdapass;
         n549Contratante_EmailSdaPass = false;
         A335Contratante_PessoaCod = obj124.gxTpr_Contratante_pessoacod;
         A12Contratante_CNPJ = obj124.gxTpr_Contratante_cnpj;
         n12Contratante_CNPJ = false;
         A9Contratante_RazaoSocial = obj124.gxTpr_Contratante_razaosocial;
         n9Contratante_RazaoSocial = false;
         A10Contratante_NomeFantasia = obj124.gxTpr_Contratante_nomefantasia;
         A11Contratante_IE = obj124.gxTpr_Contratante_ie;
         A13Contratante_WebSite = obj124.gxTpr_Contratante_website;
         n13Contratante_WebSite = false;
         A14Contratante_Email = obj124.gxTpr_Contratante_email;
         n14Contratante_Email = false;
         A31Contratante_Telefone = obj124.gxTpr_Contratante_telefone;
         A32Contratante_Ramal = obj124.gxTpr_Contratante_ramal;
         n32Contratante_Ramal = false;
         A33Contratante_Fax = obj124.gxTpr_Contratante_fax;
         n33Contratante_Fax = false;
         A336Contratante_AgenciaNome = obj124.gxTpr_Contratante_agencianome;
         n336Contratante_AgenciaNome = false;
         A337Contratante_AgenciaNro = obj124.gxTpr_Contratante_agencianro;
         n337Contratante_AgenciaNro = false;
         A338Contratante_BancoNome = obj124.gxTpr_Contratante_banconome;
         n338Contratante_BancoNome = false;
         A339Contratante_BancoNro = obj124.gxTpr_Contratante_banconro;
         n339Contratante_BancoNro = false;
         A16Contratante_ContaCorrente = obj124.gxTpr_Contratante_contacorrente;
         n16Contratante_ContaCorrente = false;
         A25Municipio_Codigo = obj124.gxTpr_Municipio_codigo;
         n25Municipio_Codigo = false;
         A26Municipio_Nome = obj124.gxTpr_Municipio_nome;
         A23Estado_UF = obj124.gxTpr_Estado_uf;
         A547Contratante_EmailSdaHost = obj124.gxTpr_Contratante_emailsdahost;
         n547Contratante_EmailSdaHost = false;
         A548Contratante_EmailSdaUser = obj124.gxTpr_Contratante_emailsdauser;
         n548Contratante_EmailSdaUser = false;
         A552Contratante_EmailSdaPort = obj124.gxTpr_Contratante_emailsdaport;
         n552Contratante_EmailSdaPort = false;
         A1048Contratante_EmailSdaSec = obj124.gxTpr_Contratante_emailsdasec;
         n1048Contratante_EmailSdaSec = false;
         A593Contratante_OSAutomatica = obj124.gxTpr_Contratante_osautomatica;
         A1652Contratante_SSAutomatica = obj124.gxTpr_Contratante_ssautomatica;
         n1652Contratante_SSAutomatica = false;
         A594Contratante_ExisteConferencia = obj124.gxTpr_Contratante_existeconferencia;
         A1124Contratante_LogoArquivo = obj124.gxTpr_Contratante_logoarquivo;
         n1124Contratante_LogoArquivo = false;
         A1448Contratante_InicioDoExpediente = obj124.gxTpr_Contratante_iniciodoexpediente;
         n1448Contratante_InicioDoExpediente = false;
         A1192Contratante_FimDoExpediente = obj124.gxTpr_Contratante_fimdoexpediente;
         n1192Contratante_FimDoExpediente = false;
         A1727Contratante_ServicoSSPadrao = obj124.gxTpr_Contratante_servicosspadrao;
         n1727Contratante_ServicoSSPadrao = false;
         A1729Contratante_RetornaSS = obj124.gxTpr_Contratante_retornass;
         n1729Contratante_RetornaSS = false;
         A1733Contratante_SSPrestadoraUnica = obj124.gxTpr_Contratante_ssprestadoraunica;
         n1733Contratante_SSPrestadoraUnica = false;
         A1739Contratante_PropostaRqrPrz = obj124.gxTpr_Contratante_propostarqrprz;
         n1739Contratante_PropostaRqrPrz = false;
         A1740Contratante_PropostaRqrEsf = obj124.gxTpr_Contratante_propostarqresf;
         n1740Contratante_PropostaRqrEsf = false;
         A1741Contratante_PropostaRqrCnt = obj124.gxTpr_Contratante_propostarqrcnt;
         n1741Contratante_PropostaRqrCnt = false;
         A1757Contratante_OSGeraOS = obj124.gxTpr_Contratante_osgeraos;
         n1757Contratante_OSGeraOS = false;
         A1758Contratante_OSGeraTRP = obj124.gxTpr_Contratante_osgeratrp;
         n1758Contratante_OSGeraTRP = false;
         A1759Contratante_OSGeraTH = obj124.gxTpr_Contratante_osgerath;
         n1759Contratante_OSGeraTH = false;
         A1760Contratante_OSGeraTRD = obj124.gxTpr_Contratante_osgeratrd;
         n1760Contratante_OSGeraTRD = false;
         A1761Contratante_OSGeraTR = obj124.gxTpr_Contratante_osgeratr;
         n1761Contratante_OSGeraTR = false;
         A1803Contratante_OSHmlgComPnd = obj124.gxTpr_Contratante_oshmlgcompnd;
         n1803Contratante_OSHmlgComPnd = false;
         A1805Contratante_AtivoCirculante = obj124.gxTpr_Contratante_ativocirculante;
         n1805Contratante_AtivoCirculante = false;
         A1806Contratante_PassivoCirculante = obj124.gxTpr_Contratante_passivocirculante;
         n1806Contratante_PassivoCirculante = false;
         A1807Contratante_PatrimonioLiquido = obj124.gxTpr_Contratante_patrimonioliquido;
         n1807Contratante_PatrimonioLiquido = false;
         A1808Contratante_ReceitaBruta = obj124.gxTpr_Contratante_receitabruta;
         n1808Contratante_ReceitaBruta = false;
         A2084Contratante_PrzAoRtr = obj124.gxTpr_Contratante_przaortr;
         n2084Contratante_PrzAoRtr = false;
         A2083Contratante_PrzActRtr = obj124.gxTpr_Contratante_przactrtr;
         n2083Contratante_PrzActRtr = false;
         A2089Contratante_SelecionaResponsavelOS = obj124.gxTpr_Contratante_selecionaresponsavelos;
         A2090Contratante_ExibePF = obj124.gxTpr_Contratante_exibepf;
         A1126Contratante_LogoTipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( obj124.gxTpr_Contratante_logotipoarq)) ? FileUtil.GetFileType( A1124Contratante_LogoArquivo) : obj124.gxTpr_Contratante_logotipoarq);
         n1126Contratante_LogoTipoArq = false;
         A1125Contratante_LogoNomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( obj124.gxTpr_Contratante_logonomearq)) ? FileUtil.GetFileName( A1124Contratante_LogoArquivo) : obj124.gxTpr_Contratante_logonomearq);
         n1125Contratante_LogoNomeArq = false;
         A30Contratante_Ativo = obj124.gxTpr_Contratante_ativo;
         A551Contratante_EmailSdaAut = obj124.gxTpr_Contratante_emailsdaaut;
         n551Contratante_EmailSdaAut = false;
         A1732Contratante_SSStatusPlanejamento = obj124.gxTpr_Contratante_ssstatusplanejamento;
         n1732Contratante_SSStatusPlanejamento = false;
         A1738Contratante_PropostaRqrSrv = obj124.gxTpr_Contratante_propostarqrsrv;
         n1738Contratante_PropostaRqrSrv = false;
         A1742Contratante_PropostaNvlCnt = obj124.gxTpr_Contratante_propostanvlcnt;
         n1742Contratante_PropostaNvlCnt = false;
         A1822Contratante_UsaOSistema = obj124.gxTpr_Contratante_usaosistema;
         n1822Contratante_UsaOSistema = false;
         A2034Contratante_TtlRltGerencial = obj124.gxTpr_Contratante_ttlrltgerencial;
         n2034Contratante_TtlRltGerencial = false;
         A2085Contratante_RequerOrigem = obj124.gxTpr_Contratante_requerorigem;
         n2085Contratante_RequerOrigem = false;
         A29Contratante_Codigo = obj124.gxTpr_Contratante_codigo;
         n29Contratante_Codigo = false;
         Z29Contratante_Codigo = obj124.gxTpr_Contratante_codigo_Z;
         Z335Contratante_PessoaCod = obj124.gxTpr_Contratante_pessoacod_Z;
         Z12Contratante_CNPJ = obj124.gxTpr_Contratante_cnpj_Z;
         Z9Contratante_RazaoSocial = obj124.gxTpr_Contratante_razaosocial_Z;
         Z10Contratante_NomeFantasia = obj124.gxTpr_Contratante_nomefantasia_Z;
         Z11Contratante_IE = obj124.gxTpr_Contratante_ie_Z;
         Z13Contratante_WebSite = obj124.gxTpr_Contratante_website_Z;
         Z14Contratante_Email = obj124.gxTpr_Contratante_email_Z;
         Z31Contratante_Telefone = obj124.gxTpr_Contratante_telefone_Z;
         Z32Contratante_Ramal = obj124.gxTpr_Contratante_ramal_Z;
         Z33Contratante_Fax = obj124.gxTpr_Contratante_fax_Z;
         Z336Contratante_AgenciaNome = obj124.gxTpr_Contratante_agencianome_Z;
         Z337Contratante_AgenciaNro = obj124.gxTpr_Contratante_agencianro_Z;
         Z338Contratante_BancoNome = obj124.gxTpr_Contratante_banconome_Z;
         Z339Contratante_BancoNro = obj124.gxTpr_Contratante_banconro_Z;
         Z16Contratante_ContaCorrente = obj124.gxTpr_Contratante_contacorrente_Z;
         Z25Municipio_Codigo = obj124.gxTpr_Municipio_codigo_Z;
         Z26Municipio_Nome = obj124.gxTpr_Municipio_nome_Z;
         Z23Estado_UF = obj124.gxTpr_Estado_uf_Z;
         Z30Contratante_Ativo = obj124.gxTpr_Contratante_ativo_Z;
         Z547Contratante_EmailSdaHost = obj124.gxTpr_Contratante_emailsdahost_Z;
         Z548Contratante_EmailSdaUser = obj124.gxTpr_Contratante_emailsdauser_Z;
         Z549Contratante_EmailSdaPass = obj124.gxTpr_Contratante_emailsdapass_Z;
         O549Contratante_EmailSdaPass = obj124.gxTpr_Contratante_emailsdapass_Z;
         Z550Contratante_EmailSdaKey = obj124.gxTpr_Contratante_emailsdakey_Z;
         Z551Contratante_EmailSdaAut = obj124.gxTpr_Contratante_emailsdaaut_Z;
         Z552Contratante_EmailSdaPort = obj124.gxTpr_Contratante_emailsdaport_Z;
         Z1048Contratante_EmailSdaSec = obj124.gxTpr_Contratante_emailsdasec_Z;
         Z593Contratante_OSAutomatica = obj124.gxTpr_Contratante_osautomatica_Z;
         Z1652Contratante_SSAutomatica = obj124.gxTpr_Contratante_ssautomatica_Z;
         Z594Contratante_ExisteConferencia = obj124.gxTpr_Contratante_existeconferencia_Z;
         Z1125Contratante_LogoNomeArq = obj124.gxTpr_Contratante_logonomearq_Z;
         Z1126Contratante_LogoTipoArq = obj124.gxTpr_Contratante_logotipoarq_Z;
         Z1448Contratante_InicioDoExpediente = obj124.gxTpr_Contratante_iniciodoexpediente_Z;
         Z1192Contratante_FimDoExpediente = obj124.gxTpr_Contratante_fimdoexpediente_Z;
         Z1727Contratante_ServicoSSPadrao = obj124.gxTpr_Contratante_servicosspadrao_Z;
         Z1729Contratante_RetornaSS = obj124.gxTpr_Contratante_retornass_Z;
         Z1732Contratante_SSStatusPlanejamento = obj124.gxTpr_Contratante_ssstatusplanejamento_Z;
         Z1733Contratante_SSPrestadoraUnica = obj124.gxTpr_Contratante_ssprestadoraunica_Z;
         Z1738Contratante_PropostaRqrSrv = obj124.gxTpr_Contratante_propostarqrsrv_Z;
         Z1739Contratante_PropostaRqrPrz = obj124.gxTpr_Contratante_propostarqrprz_Z;
         Z1740Contratante_PropostaRqrEsf = obj124.gxTpr_Contratante_propostarqresf_Z;
         Z1741Contratante_PropostaRqrCnt = obj124.gxTpr_Contratante_propostarqrcnt_Z;
         Z1742Contratante_PropostaNvlCnt = obj124.gxTpr_Contratante_propostanvlcnt_Z;
         Z1757Contratante_OSGeraOS = obj124.gxTpr_Contratante_osgeraos_Z;
         Z1758Contratante_OSGeraTRP = obj124.gxTpr_Contratante_osgeratrp_Z;
         Z1759Contratante_OSGeraTH = obj124.gxTpr_Contratante_osgerath_Z;
         Z1760Contratante_OSGeraTRD = obj124.gxTpr_Contratante_osgeratrd_Z;
         Z1761Contratante_OSGeraTR = obj124.gxTpr_Contratante_osgeratr_Z;
         Z1803Contratante_OSHmlgComPnd = obj124.gxTpr_Contratante_oshmlgcompnd_Z;
         Z1805Contratante_AtivoCirculante = obj124.gxTpr_Contratante_ativocirculante_Z;
         Z1806Contratante_PassivoCirculante = obj124.gxTpr_Contratante_passivocirculante_Z;
         Z1807Contratante_PatrimonioLiquido = obj124.gxTpr_Contratante_patrimonioliquido_Z;
         Z1808Contratante_ReceitaBruta = obj124.gxTpr_Contratante_receitabruta_Z;
         Z1822Contratante_UsaOSistema = obj124.gxTpr_Contratante_usaosistema_Z;
         Z2034Contratante_TtlRltGerencial = obj124.gxTpr_Contratante_ttlrltgerencial_Z;
         Z2084Contratante_PrzAoRtr = obj124.gxTpr_Contratante_przaortr_Z;
         Z2083Contratante_PrzActRtr = obj124.gxTpr_Contratante_przactrtr_Z;
         Z2085Contratante_RequerOrigem = obj124.gxTpr_Contratante_requerorigem_Z;
         Z2089Contratante_SelecionaResponsavelOS = obj124.gxTpr_Contratante_selecionaresponsavelos_Z;
         Z2090Contratante_ExibePF = obj124.gxTpr_Contratante_exibepf_Z;
         n29Contratante_Codigo = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_codigo_N));
         n12Contratante_CNPJ = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_cnpj_N));
         n9Contratante_RazaoSocial = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_razaosocial_N));
         n13Contratante_WebSite = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_website_N));
         n14Contratante_Email = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_email_N));
         n32Contratante_Ramal = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_ramal_N));
         n33Contratante_Fax = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_fax_N));
         n336Contratante_AgenciaNome = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_agencianome_N));
         n337Contratante_AgenciaNro = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_agencianro_N));
         n338Contratante_BancoNome = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_banconome_N));
         n339Contratante_BancoNro = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_banconro_N));
         n16Contratante_ContaCorrente = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_contacorrente_N));
         n25Municipio_Codigo = (bool)(Convert.ToBoolean(obj124.gxTpr_Municipio_codigo_N));
         n547Contratante_EmailSdaHost = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_emailsdahost_N));
         n548Contratante_EmailSdaUser = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_emailsdauser_N));
         n549Contratante_EmailSdaPass = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_emailsdapass_N));
         n550Contratante_EmailSdaKey = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_emailsdakey_N));
         n551Contratante_EmailSdaAut = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_emailsdaaut_N));
         n552Contratante_EmailSdaPort = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_emailsdaport_N));
         n1048Contratante_EmailSdaSec = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_emailsdasec_N));
         n1652Contratante_SSAutomatica = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_ssautomatica_N));
         n1124Contratante_LogoArquivo = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_logoarquivo_N));
         n1125Contratante_LogoNomeArq = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_logonomearq_N));
         n1126Contratante_LogoTipoArq = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_logotipoarq_N));
         n1448Contratante_InicioDoExpediente = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_iniciodoexpediente_N));
         n1192Contratante_FimDoExpediente = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_fimdoexpediente_N));
         n1727Contratante_ServicoSSPadrao = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_servicosspadrao_N));
         n1729Contratante_RetornaSS = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_retornass_N));
         n1732Contratante_SSStatusPlanejamento = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_ssstatusplanejamento_N));
         n1733Contratante_SSPrestadoraUnica = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_ssprestadoraunica_N));
         n1738Contratante_PropostaRqrSrv = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_propostarqrsrv_N));
         n1739Contratante_PropostaRqrPrz = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_propostarqrprz_N));
         n1740Contratante_PropostaRqrEsf = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_propostarqresf_N));
         n1741Contratante_PropostaRqrCnt = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_propostarqrcnt_N));
         n1742Contratante_PropostaNvlCnt = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_propostanvlcnt_N));
         n1757Contratante_OSGeraOS = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_osgeraos_N));
         n1758Contratante_OSGeraTRP = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_osgeratrp_N));
         n1759Contratante_OSGeraTH = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_osgerath_N));
         n1760Contratante_OSGeraTRD = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_osgeratrd_N));
         n1761Contratante_OSGeraTR = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_osgeratr_N));
         n1803Contratante_OSHmlgComPnd = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_oshmlgcompnd_N));
         n1805Contratante_AtivoCirculante = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_ativocirculante_N));
         n1806Contratante_PassivoCirculante = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_passivocirculante_N));
         n1807Contratante_PatrimonioLiquido = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_patrimonioliquido_N));
         n1808Contratante_ReceitaBruta = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_receitabruta_N));
         n1822Contratante_UsaOSistema = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_usaosistema_N));
         n2034Contratante_TtlRltGerencial = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_ttlrltgerencial_N));
         n2084Contratante_PrzAoRtr = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_przaortr_N));
         n2083Contratante_PrzActRtr = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_przactrtr_N));
         n2085Contratante_RequerOrigem = (bool)(Convert.ToBoolean(obj124.gxTpr_Contratante_requerorigem_N));
         Gx_mode = obj124.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A29Contratante_Codigo = (int)getParm(obj,0);
         n29Contratante_Codigo = false;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey05124( ) ;
         ScanKeyStart05124( ) ;
         if ( RcdFound124 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z29Contratante_Codigo = A29Contratante_Codigo;
            O549Contratante_EmailSdaPass = A549Contratante_EmailSdaPass;
            n549Contratante_EmailSdaPass = false;
         }
         ZM05124( -24) ;
         OnLoadActions05124( ) ;
         AddRow05124( ) ;
         ScanKeyEnd05124( ) ;
         if ( RcdFound124 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars124( bcContratante, 0) ;
         ScanKeyStart05124( ) ;
         if ( RcdFound124 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z29Contratante_Codigo = A29Contratante_Codigo;
            O549Contratante_EmailSdaPass = A549Contratante_EmailSdaPass;
            n549Contratante_EmailSdaPass = false;
         }
         ZM05124( -24) ;
         OnLoadActions05124( ) ;
         AddRow05124( ) ;
         ScanKeyEnd05124( ) ;
         if ( RcdFound124 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars124( bcContratante, 0) ;
         nKeyPressed = 1;
         GetKey05124( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert05124( ) ;
         }
         else
         {
            if ( RcdFound124 == 1 )
            {
               if ( A29Contratante_Codigo != Z29Contratante_Codigo )
               {
                  A29Contratante_Codigo = Z29Contratante_Codigo;
                  n29Contratante_Codigo = false;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update05124( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A29Contratante_Codigo != Z29Contratante_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert05124( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert05124( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow124( bcContratante) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars124( bcContratante, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey05124( ) ;
         if ( RcdFound124 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A29Contratante_Codigo != Z29Contratante_Codigo )
            {
               A29Contratante_Codigo = Z29Contratante_Codigo;
               n29Contratante_Codigo = false;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A29Contratante_Codigo != Z29Contratante_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(10);
         context.RollbackDataStores( "Contratante_BC");
         VarsToRow124( bcContratante) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContratante.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContratante.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContratante )
         {
            bcContratante = (SdtContratante)(sdt);
            if ( StringUtil.StrCmp(bcContratante.gxTpr_Mode, "") == 0 )
            {
               bcContratante.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow124( bcContratante) ;
            }
            else
            {
               RowToVars124( bcContratante, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContratante.gxTpr_Mode, "") == 0 )
            {
               bcContratante.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars124( bcContratante, 1) ;
         return  ;
      }

      public SdtContratante Contratante_BC
      {
         get {
            return bcContratante ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(10);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV45Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV29AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV37EmailText = "";
         AV38Subject = "";
         AV34Usuarios = new GxSimpleCollection();
         AV35Attachments = new GxSimpleCollection();
         AV36Resultado = "";
         AV41SMTPSession = new GeneXus.Mail.GXSMTPSession(context.GetPhysicalPath());
         AV19MailRecipient = new GeneXus.Mail.GXMailRecipient();
         AV39Email = new GeneXus.Mail.GXMailMessage();
         A549Contratante_EmailSdaPass = "";
         O549Contratante_EmailSdaPass = "";
         A550Contratante_EmailSdaKey = "";
         A548Contratante_EmailSdaUser = "";
         Z550Contratante_EmailSdaKey = "";
         Z549Contratante_EmailSdaPass = "";
         Z10Contratante_NomeFantasia = "";
         A10Contratante_NomeFantasia = "";
         Z11Contratante_IE = "";
         A11Contratante_IE = "";
         Z13Contratante_WebSite = "";
         A13Contratante_WebSite = "";
         Z14Contratante_Email = "";
         A14Contratante_Email = "";
         Z31Contratante_Telefone = "";
         A31Contratante_Telefone = "";
         Z32Contratante_Ramal = "";
         A32Contratante_Ramal = "";
         Z33Contratante_Fax = "";
         A33Contratante_Fax = "";
         Z336Contratante_AgenciaNome = "";
         A336Contratante_AgenciaNome = "";
         Z337Contratante_AgenciaNro = "";
         A337Contratante_AgenciaNro = "";
         Z338Contratante_BancoNome = "";
         A338Contratante_BancoNome = "";
         Z339Contratante_BancoNro = "";
         A339Contratante_BancoNro = "";
         Z16Contratante_ContaCorrente = "";
         A16Contratante_ContaCorrente = "";
         Z547Contratante_EmailSdaHost = "";
         A547Contratante_EmailSdaHost = "";
         Z548Contratante_EmailSdaUser = "";
         Z1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         A1448Contratante_InicioDoExpediente = (DateTime)(DateTime.MinValue);
         Z1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         A1192Contratante_FimDoExpediente = (DateTime)(DateTime.MinValue);
         Z1732Contratante_SSStatusPlanejamento = "";
         A1732Contratante_SSStatusPlanejamento = "";
         Z2034Contratante_TtlRltGerencial = "";
         A2034Contratante_TtlRltGerencial = "";
         Z26Municipio_Nome = "";
         A26Municipio_Nome = "";
         Z23Estado_UF = "";
         A23Estado_UF = "";
         Z12Contratante_CNPJ = "";
         A12Contratante_CNPJ = "";
         Z9Contratante_RazaoSocial = "";
         A9Contratante_RazaoSocial = "";
         Z1124Contratante_LogoArquivo = "";
         A1124Contratante_LogoArquivo = "";
         Z1126Contratante_LogoTipoArq = "";
         A1126Contratante_LogoTipoArq = "";
         Z1125Contratante_LogoNomeArq = "";
         A1125Contratante_LogoNomeArq = "";
         BC00056_A29Contratante_Codigo = new int[1] ;
         BC00056_n29Contratante_Codigo = new bool[] {false} ;
         BC00056_A550Contratante_EmailSdaKey = new String[] {""} ;
         BC00056_n550Contratante_EmailSdaKey = new bool[] {false} ;
         BC00056_A549Contratante_EmailSdaPass = new String[] {""} ;
         BC00056_n549Contratante_EmailSdaPass = new bool[] {false} ;
         BC00056_A12Contratante_CNPJ = new String[] {""} ;
         BC00056_n12Contratante_CNPJ = new bool[] {false} ;
         BC00056_A9Contratante_RazaoSocial = new String[] {""} ;
         BC00056_n9Contratante_RazaoSocial = new bool[] {false} ;
         BC00056_A10Contratante_NomeFantasia = new String[] {""} ;
         BC00056_A11Contratante_IE = new String[] {""} ;
         BC00056_A13Contratante_WebSite = new String[] {""} ;
         BC00056_n13Contratante_WebSite = new bool[] {false} ;
         BC00056_A14Contratante_Email = new String[] {""} ;
         BC00056_n14Contratante_Email = new bool[] {false} ;
         BC00056_A31Contratante_Telefone = new String[] {""} ;
         BC00056_A32Contratante_Ramal = new String[] {""} ;
         BC00056_n32Contratante_Ramal = new bool[] {false} ;
         BC00056_A33Contratante_Fax = new String[] {""} ;
         BC00056_n33Contratante_Fax = new bool[] {false} ;
         BC00056_A336Contratante_AgenciaNome = new String[] {""} ;
         BC00056_n336Contratante_AgenciaNome = new bool[] {false} ;
         BC00056_A337Contratante_AgenciaNro = new String[] {""} ;
         BC00056_n337Contratante_AgenciaNro = new bool[] {false} ;
         BC00056_A338Contratante_BancoNome = new String[] {""} ;
         BC00056_n338Contratante_BancoNome = new bool[] {false} ;
         BC00056_A339Contratante_BancoNro = new String[] {""} ;
         BC00056_n339Contratante_BancoNro = new bool[] {false} ;
         BC00056_A16Contratante_ContaCorrente = new String[] {""} ;
         BC00056_n16Contratante_ContaCorrente = new bool[] {false} ;
         BC00056_A26Municipio_Nome = new String[] {""} ;
         BC00056_A30Contratante_Ativo = new bool[] {false} ;
         BC00056_A547Contratante_EmailSdaHost = new String[] {""} ;
         BC00056_n547Contratante_EmailSdaHost = new bool[] {false} ;
         BC00056_A548Contratante_EmailSdaUser = new String[] {""} ;
         BC00056_n548Contratante_EmailSdaUser = new bool[] {false} ;
         BC00056_A551Contratante_EmailSdaAut = new bool[] {false} ;
         BC00056_n551Contratante_EmailSdaAut = new bool[] {false} ;
         BC00056_A552Contratante_EmailSdaPort = new short[1] ;
         BC00056_n552Contratante_EmailSdaPort = new bool[] {false} ;
         BC00056_A1048Contratante_EmailSdaSec = new short[1] ;
         BC00056_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         BC00056_A593Contratante_OSAutomatica = new bool[] {false} ;
         BC00056_A1652Contratante_SSAutomatica = new bool[] {false} ;
         BC00056_n1652Contratante_SSAutomatica = new bool[] {false} ;
         BC00056_A594Contratante_ExisteConferencia = new bool[] {false} ;
         BC00056_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         BC00056_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         BC00056_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         BC00056_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         BC00056_A1727Contratante_ServicoSSPadrao = new int[1] ;
         BC00056_n1727Contratante_ServicoSSPadrao = new bool[] {false} ;
         BC00056_A1729Contratante_RetornaSS = new bool[] {false} ;
         BC00056_n1729Contratante_RetornaSS = new bool[] {false} ;
         BC00056_A1732Contratante_SSStatusPlanejamento = new String[] {""} ;
         BC00056_n1732Contratante_SSStatusPlanejamento = new bool[] {false} ;
         BC00056_A1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         BC00056_n1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         BC00056_A1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         BC00056_n1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         BC00056_A1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         BC00056_n1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         BC00056_A1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         BC00056_n1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         BC00056_A1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         BC00056_n1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         BC00056_A1742Contratante_PropostaNvlCnt = new short[1] ;
         BC00056_n1742Contratante_PropostaNvlCnt = new bool[] {false} ;
         BC00056_A1757Contratante_OSGeraOS = new bool[] {false} ;
         BC00056_n1757Contratante_OSGeraOS = new bool[] {false} ;
         BC00056_A1758Contratante_OSGeraTRP = new bool[] {false} ;
         BC00056_n1758Contratante_OSGeraTRP = new bool[] {false} ;
         BC00056_A1759Contratante_OSGeraTH = new bool[] {false} ;
         BC00056_n1759Contratante_OSGeraTH = new bool[] {false} ;
         BC00056_A1760Contratante_OSGeraTRD = new bool[] {false} ;
         BC00056_n1760Contratante_OSGeraTRD = new bool[] {false} ;
         BC00056_A1761Contratante_OSGeraTR = new bool[] {false} ;
         BC00056_n1761Contratante_OSGeraTR = new bool[] {false} ;
         BC00056_A1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         BC00056_n1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         BC00056_A1805Contratante_AtivoCirculante = new decimal[1] ;
         BC00056_n1805Contratante_AtivoCirculante = new bool[] {false} ;
         BC00056_A1806Contratante_PassivoCirculante = new decimal[1] ;
         BC00056_n1806Contratante_PassivoCirculante = new bool[] {false} ;
         BC00056_A1807Contratante_PatrimonioLiquido = new decimal[1] ;
         BC00056_n1807Contratante_PatrimonioLiquido = new bool[] {false} ;
         BC00056_A1808Contratante_ReceitaBruta = new decimal[1] ;
         BC00056_n1808Contratante_ReceitaBruta = new bool[] {false} ;
         BC00056_A1822Contratante_UsaOSistema = new bool[] {false} ;
         BC00056_n1822Contratante_UsaOSistema = new bool[] {false} ;
         BC00056_A2034Contratante_TtlRltGerencial = new String[] {""} ;
         BC00056_n2034Contratante_TtlRltGerencial = new bool[] {false} ;
         BC00056_A2084Contratante_PrzAoRtr = new short[1] ;
         BC00056_n2084Contratante_PrzAoRtr = new bool[] {false} ;
         BC00056_A2083Contratante_PrzActRtr = new short[1] ;
         BC00056_n2083Contratante_PrzActRtr = new bool[] {false} ;
         BC00056_A2085Contratante_RequerOrigem = new bool[] {false} ;
         BC00056_n2085Contratante_RequerOrigem = new bool[] {false} ;
         BC00056_A2089Contratante_SelecionaResponsavelOS = new bool[] {false} ;
         BC00056_A2090Contratante_ExibePF = new bool[] {false} ;
         BC00056_A1126Contratante_LogoTipoArq = new String[] {""} ;
         BC00056_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         BC00056_A1125Contratante_LogoNomeArq = new String[] {""} ;
         BC00056_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         BC00056_A25Municipio_Codigo = new int[1] ;
         BC00056_n25Municipio_Codigo = new bool[] {false} ;
         BC00056_A335Contratante_PessoaCod = new int[1] ;
         BC00056_A23Estado_UF = new String[] {""} ;
         BC00056_A1124Contratante_LogoArquivo = new String[] {""} ;
         BC00056_n1124Contratante_LogoArquivo = new bool[] {false} ;
         A1124Contratante_LogoArquivo_Filetype = "";
         A1124Contratante_LogoArquivo_Filename = "";
         AV15Estado_UF = "";
         BC00055_A12Contratante_CNPJ = new String[] {""} ;
         BC00055_n12Contratante_CNPJ = new bool[] {false} ;
         BC00055_A9Contratante_RazaoSocial = new String[] {""} ;
         BC00055_n9Contratante_RazaoSocial = new bool[] {false} ;
         BC00054_A26Municipio_Nome = new String[] {""} ;
         BC00054_A23Estado_UF = new String[] {""} ;
         BC00057_A29Contratante_Codigo = new int[1] ;
         BC00057_n29Contratante_Codigo = new bool[] {false} ;
         BC00053_A29Contratante_Codigo = new int[1] ;
         BC00053_n29Contratante_Codigo = new bool[] {false} ;
         BC00053_A550Contratante_EmailSdaKey = new String[] {""} ;
         BC00053_n550Contratante_EmailSdaKey = new bool[] {false} ;
         BC00053_A549Contratante_EmailSdaPass = new String[] {""} ;
         BC00053_n549Contratante_EmailSdaPass = new bool[] {false} ;
         BC00053_A10Contratante_NomeFantasia = new String[] {""} ;
         BC00053_A11Contratante_IE = new String[] {""} ;
         BC00053_A13Contratante_WebSite = new String[] {""} ;
         BC00053_n13Contratante_WebSite = new bool[] {false} ;
         BC00053_A14Contratante_Email = new String[] {""} ;
         BC00053_n14Contratante_Email = new bool[] {false} ;
         BC00053_A31Contratante_Telefone = new String[] {""} ;
         BC00053_A32Contratante_Ramal = new String[] {""} ;
         BC00053_n32Contratante_Ramal = new bool[] {false} ;
         BC00053_A33Contratante_Fax = new String[] {""} ;
         BC00053_n33Contratante_Fax = new bool[] {false} ;
         BC00053_A336Contratante_AgenciaNome = new String[] {""} ;
         BC00053_n336Contratante_AgenciaNome = new bool[] {false} ;
         BC00053_A337Contratante_AgenciaNro = new String[] {""} ;
         BC00053_n337Contratante_AgenciaNro = new bool[] {false} ;
         BC00053_A338Contratante_BancoNome = new String[] {""} ;
         BC00053_n338Contratante_BancoNome = new bool[] {false} ;
         BC00053_A339Contratante_BancoNro = new String[] {""} ;
         BC00053_n339Contratante_BancoNro = new bool[] {false} ;
         BC00053_A16Contratante_ContaCorrente = new String[] {""} ;
         BC00053_n16Contratante_ContaCorrente = new bool[] {false} ;
         BC00053_A30Contratante_Ativo = new bool[] {false} ;
         BC00053_A547Contratante_EmailSdaHost = new String[] {""} ;
         BC00053_n547Contratante_EmailSdaHost = new bool[] {false} ;
         BC00053_A548Contratante_EmailSdaUser = new String[] {""} ;
         BC00053_n548Contratante_EmailSdaUser = new bool[] {false} ;
         BC00053_A551Contratante_EmailSdaAut = new bool[] {false} ;
         BC00053_n551Contratante_EmailSdaAut = new bool[] {false} ;
         BC00053_A552Contratante_EmailSdaPort = new short[1] ;
         BC00053_n552Contratante_EmailSdaPort = new bool[] {false} ;
         BC00053_A1048Contratante_EmailSdaSec = new short[1] ;
         BC00053_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         BC00053_A593Contratante_OSAutomatica = new bool[] {false} ;
         BC00053_A1652Contratante_SSAutomatica = new bool[] {false} ;
         BC00053_n1652Contratante_SSAutomatica = new bool[] {false} ;
         BC00053_A594Contratante_ExisteConferencia = new bool[] {false} ;
         BC00053_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         BC00053_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         BC00053_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         BC00053_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         BC00053_A1727Contratante_ServicoSSPadrao = new int[1] ;
         BC00053_n1727Contratante_ServicoSSPadrao = new bool[] {false} ;
         BC00053_A1729Contratante_RetornaSS = new bool[] {false} ;
         BC00053_n1729Contratante_RetornaSS = new bool[] {false} ;
         BC00053_A1732Contratante_SSStatusPlanejamento = new String[] {""} ;
         BC00053_n1732Contratante_SSStatusPlanejamento = new bool[] {false} ;
         BC00053_A1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         BC00053_n1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         BC00053_A1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         BC00053_n1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         BC00053_A1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         BC00053_n1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         BC00053_A1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         BC00053_n1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         BC00053_A1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         BC00053_n1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         BC00053_A1742Contratante_PropostaNvlCnt = new short[1] ;
         BC00053_n1742Contratante_PropostaNvlCnt = new bool[] {false} ;
         BC00053_A1757Contratante_OSGeraOS = new bool[] {false} ;
         BC00053_n1757Contratante_OSGeraOS = new bool[] {false} ;
         BC00053_A1758Contratante_OSGeraTRP = new bool[] {false} ;
         BC00053_n1758Contratante_OSGeraTRP = new bool[] {false} ;
         BC00053_A1759Contratante_OSGeraTH = new bool[] {false} ;
         BC00053_n1759Contratante_OSGeraTH = new bool[] {false} ;
         BC00053_A1760Contratante_OSGeraTRD = new bool[] {false} ;
         BC00053_n1760Contratante_OSGeraTRD = new bool[] {false} ;
         BC00053_A1761Contratante_OSGeraTR = new bool[] {false} ;
         BC00053_n1761Contratante_OSGeraTR = new bool[] {false} ;
         BC00053_A1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         BC00053_n1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         BC00053_A1805Contratante_AtivoCirculante = new decimal[1] ;
         BC00053_n1805Contratante_AtivoCirculante = new bool[] {false} ;
         BC00053_A1806Contratante_PassivoCirculante = new decimal[1] ;
         BC00053_n1806Contratante_PassivoCirculante = new bool[] {false} ;
         BC00053_A1807Contratante_PatrimonioLiquido = new decimal[1] ;
         BC00053_n1807Contratante_PatrimonioLiquido = new bool[] {false} ;
         BC00053_A1808Contratante_ReceitaBruta = new decimal[1] ;
         BC00053_n1808Contratante_ReceitaBruta = new bool[] {false} ;
         BC00053_A1822Contratante_UsaOSistema = new bool[] {false} ;
         BC00053_n1822Contratante_UsaOSistema = new bool[] {false} ;
         BC00053_A2034Contratante_TtlRltGerencial = new String[] {""} ;
         BC00053_n2034Contratante_TtlRltGerencial = new bool[] {false} ;
         BC00053_A2084Contratante_PrzAoRtr = new short[1] ;
         BC00053_n2084Contratante_PrzAoRtr = new bool[] {false} ;
         BC00053_A2083Contratante_PrzActRtr = new short[1] ;
         BC00053_n2083Contratante_PrzActRtr = new bool[] {false} ;
         BC00053_A2085Contratante_RequerOrigem = new bool[] {false} ;
         BC00053_n2085Contratante_RequerOrigem = new bool[] {false} ;
         BC00053_A2089Contratante_SelecionaResponsavelOS = new bool[] {false} ;
         BC00053_A2090Contratante_ExibePF = new bool[] {false} ;
         BC00053_A1126Contratante_LogoTipoArq = new String[] {""} ;
         BC00053_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         BC00053_A1125Contratante_LogoNomeArq = new String[] {""} ;
         BC00053_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         BC00053_A25Municipio_Codigo = new int[1] ;
         BC00053_n25Municipio_Codigo = new bool[] {false} ;
         BC00053_A335Contratante_PessoaCod = new int[1] ;
         BC00053_A1124Contratante_LogoArquivo = new String[] {""} ;
         BC00053_n1124Contratante_LogoArquivo = new bool[] {false} ;
         sMode124 = "";
         BC00052_A29Contratante_Codigo = new int[1] ;
         BC00052_n29Contratante_Codigo = new bool[] {false} ;
         BC00052_A550Contratante_EmailSdaKey = new String[] {""} ;
         BC00052_n550Contratante_EmailSdaKey = new bool[] {false} ;
         BC00052_A549Contratante_EmailSdaPass = new String[] {""} ;
         BC00052_n549Contratante_EmailSdaPass = new bool[] {false} ;
         BC00052_A10Contratante_NomeFantasia = new String[] {""} ;
         BC00052_A11Contratante_IE = new String[] {""} ;
         BC00052_A13Contratante_WebSite = new String[] {""} ;
         BC00052_n13Contratante_WebSite = new bool[] {false} ;
         BC00052_A14Contratante_Email = new String[] {""} ;
         BC00052_n14Contratante_Email = new bool[] {false} ;
         BC00052_A31Contratante_Telefone = new String[] {""} ;
         BC00052_A32Contratante_Ramal = new String[] {""} ;
         BC00052_n32Contratante_Ramal = new bool[] {false} ;
         BC00052_A33Contratante_Fax = new String[] {""} ;
         BC00052_n33Contratante_Fax = new bool[] {false} ;
         BC00052_A336Contratante_AgenciaNome = new String[] {""} ;
         BC00052_n336Contratante_AgenciaNome = new bool[] {false} ;
         BC00052_A337Contratante_AgenciaNro = new String[] {""} ;
         BC00052_n337Contratante_AgenciaNro = new bool[] {false} ;
         BC00052_A338Contratante_BancoNome = new String[] {""} ;
         BC00052_n338Contratante_BancoNome = new bool[] {false} ;
         BC00052_A339Contratante_BancoNro = new String[] {""} ;
         BC00052_n339Contratante_BancoNro = new bool[] {false} ;
         BC00052_A16Contratante_ContaCorrente = new String[] {""} ;
         BC00052_n16Contratante_ContaCorrente = new bool[] {false} ;
         BC00052_A30Contratante_Ativo = new bool[] {false} ;
         BC00052_A547Contratante_EmailSdaHost = new String[] {""} ;
         BC00052_n547Contratante_EmailSdaHost = new bool[] {false} ;
         BC00052_A548Contratante_EmailSdaUser = new String[] {""} ;
         BC00052_n548Contratante_EmailSdaUser = new bool[] {false} ;
         BC00052_A551Contratante_EmailSdaAut = new bool[] {false} ;
         BC00052_n551Contratante_EmailSdaAut = new bool[] {false} ;
         BC00052_A552Contratante_EmailSdaPort = new short[1] ;
         BC00052_n552Contratante_EmailSdaPort = new bool[] {false} ;
         BC00052_A1048Contratante_EmailSdaSec = new short[1] ;
         BC00052_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         BC00052_A593Contratante_OSAutomatica = new bool[] {false} ;
         BC00052_A1652Contratante_SSAutomatica = new bool[] {false} ;
         BC00052_n1652Contratante_SSAutomatica = new bool[] {false} ;
         BC00052_A594Contratante_ExisteConferencia = new bool[] {false} ;
         BC00052_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         BC00052_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         BC00052_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         BC00052_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         BC00052_A1727Contratante_ServicoSSPadrao = new int[1] ;
         BC00052_n1727Contratante_ServicoSSPadrao = new bool[] {false} ;
         BC00052_A1729Contratante_RetornaSS = new bool[] {false} ;
         BC00052_n1729Contratante_RetornaSS = new bool[] {false} ;
         BC00052_A1732Contratante_SSStatusPlanejamento = new String[] {""} ;
         BC00052_n1732Contratante_SSStatusPlanejamento = new bool[] {false} ;
         BC00052_A1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         BC00052_n1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         BC00052_A1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         BC00052_n1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         BC00052_A1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         BC00052_n1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         BC00052_A1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         BC00052_n1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         BC00052_A1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         BC00052_n1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         BC00052_A1742Contratante_PropostaNvlCnt = new short[1] ;
         BC00052_n1742Contratante_PropostaNvlCnt = new bool[] {false} ;
         BC00052_A1757Contratante_OSGeraOS = new bool[] {false} ;
         BC00052_n1757Contratante_OSGeraOS = new bool[] {false} ;
         BC00052_A1758Contratante_OSGeraTRP = new bool[] {false} ;
         BC00052_n1758Contratante_OSGeraTRP = new bool[] {false} ;
         BC00052_A1759Contratante_OSGeraTH = new bool[] {false} ;
         BC00052_n1759Contratante_OSGeraTH = new bool[] {false} ;
         BC00052_A1760Contratante_OSGeraTRD = new bool[] {false} ;
         BC00052_n1760Contratante_OSGeraTRD = new bool[] {false} ;
         BC00052_A1761Contratante_OSGeraTR = new bool[] {false} ;
         BC00052_n1761Contratante_OSGeraTR = new bool[] {false} ;
         BC00052_A1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         BC00052_n1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         BC00052_A1805Contratante_AtivoCirculante = new decimal[1] ;
         BC00052_n1805Contratante_AtivoCirculante = new bool[] {false} ;
         BC00052_A1806Contratante_PassivoCirculante = new decimal[1] ;
         BC00052_n1806Contratante_PassivoCirculante = new bool[] {false} ;
         BC00052_A1807Contratante_PatrimonioLiquido = new decimal[1] ;
         BC00052_n1807Contratante_PatrimonioLiquido = new bool[] {false} ;
         BC00052_A1808Contratante_ReceitaBruta = new decimal[1] ;
         BC00052_n1808Contratante_ReceitaBruta = new bool[] {false} ;
         BC00052_A1822Contratante_UsaOSistema = new bool[] {false} ;
         BC00052_n1822Contratante_UsaOSistema = new bool[] {false} ;
         BC00052_A2034Contratante_TtlRltGerencial = new String[] {""} ;
         BC00052_n2034Contratante_TtlRltGerencial = new bool[] {false} ;
         BC00052_A2084Contratante_PrzAoRtr = new short[1] ;
         BC00052_n2084Contratante_PrzAoRtr = new bool[] {false} ;
         BC00052_A2083Contratante_PrzActRtr = new short[1] ;
         BC00052_n2083Contratante_PrzActRtr = new bool[] {false} ;
         BC00052_A2085Contratante_RequerOrigem = new bool[] {false} ;
         BC00052_n2085Contratante_RequerOrigem = new bool[] {false} ;
         BC00052_A2089Contratante_SelecionaResponsavelOS = new bool[] {false} ;
         BC00052_A2090Contratante_ExibePF = new bool[] {false} ;
         BC00052_A1126Contratante_LogoTipoArq = new String[] {""} ;
         BC00052_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         BC00052_A1125Contratante_LogoNomeArq = new String[] {""} ;
         BC00052_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         BC00052_A25Municipio_Codigo = new int[1] ;
         BC00052_n25Municipio_Codigo = new bool[] {false} ;
         BC00052_A335Contratante_PessoaCod = new int[1] ;
         BC00052_A1124Contratante_LogoArquivo = new String[] {""} ;
         BC00052_n1124Contratante_LogoArquivo = new bool[] {false} ;
         BC00058_A29Contratante_Codigo = new int[1] ;
         BC00058_n29Contratante_Codigo = new bool[] {false} ;
         BC000512_A12Contratante_CNPJ = new String[] {""} ;
         BC000512_n12Contratante_CNPJ = new bool[] {false} ;
         BC000512_A9Contratante_RazaoSocial = new String[] {""} ;
         BC000512_n9Contratante_RazaoSocial = new bool[] {false} ;
         BC000513_A26Municipio_Nome = new String[] {""} ;
         BC000513_A23Estado_UF = new String[] {""} ;
         BC000514_A1380Redmine_Codigo = new int[1] ;
         BC000515_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC000515_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         BC000516_A5AreaTrabalho_Codigo = new int[1] ;
         BC000517_A29Contratante_Codigo = new int[1] ;
         BC000517_n29Contratante_Codigo = new bool[] {false} ;
         BC000517_A550Contratante_EmailSdaKey = new String[] {""} ;
         BC000517_n550Contratante_EmailSdaKey = new bool[] {false} ;
         BC000517_A549Contratante_EmailSdaPass = new String[] {""} ;
         BC000517_n549Contratante_EmailSdaPass = new bool[] {false} ;
         BC000517_A12Contratante_CNPJ = new String[] {""} ;
         BC000517_n12Contratante_CNPJ = new bool[] {false} ;
         BC000517_A9Contratante_RazaoSocial = new String[] {""} ;
         BC000517_n9Contratante_RazaoSocial = new bool[] {false} ;
         BC000517_A10Contratante_NomeFantasia = new String[] {""} ;
         BC000517_A11Contratante_IE = new String[] {""} ;
         BC000517_A13Contratante_WebSite = new String[] {""} ;
         BC000517_n13Contratante_WebSite = new bool[] {false} ;
         BC000517_A14Contratante_Email = new String[] {""} ;
         BC000517_n14Contratante_Email = new bool[] {false} ;
         BC000517_A31Contratante_Telefone = new String[] {""} ;
         BC000517_A32Contratante_Ramal = new String[] {""} ;
         BC000517_n32Contratante_Ramal = new bool[] {false} ;
         BC000517_A33Contratante_Fax = new String[] {""} ;
         BC000517_n33Contratante_Fax = new bool[] {false} ;
         BC000517_A336Contratante_AgenciaNome = new String[] {""} ;
         BC000517_n336Contratante_AgenciaNome = new bool[] {false} ;
         BC000517_A337Contratante_AgenciaNro = new String[] {""} ;
         BC000517_n337Contratante_AgenciaNro = new bool[] {false} ;
         BC000517_A338Contratante_BancoNome = new String[] {""} ;
         BC000517_n338Contratante_BancoNome = new bool[] {false} ;
         BC000517_A339Contratante_BancoNro = new String[] {""} ;
         BC000517_n339Contratante_BancoNro = new bool[] {false} ;
         BC000517_A16Contratante_ContaCorrente = new String[] {""} ;
         BC000517_n16Contratante_ContaCorrente = new bool[] {false} ;
         BC000517_A26Municipio_Nome = new String[] {""} ;
         BC000517_A30Contratante_Ativo = new bool[] {false} ;
         BC000517_A547Contratante_EmailSdaHost = new String[] {""} ;
         BC000517_n547Contratante_EmailSdaHost = new bool[] {false} ;
         BC000517_A548Contratante_EmailSdaUser = new String[] {""} ;
         BC000517_n548Contratante_EmailSdaUser = new bool[] {false} ;
         BC000517_A551Contratante_EmailSdaAut = new bool[] {false} ;
         BC000517_n551Contratante_EmailSdaAut = new bool[] {false} ;
         BC000517_A552Contratante_EmailSdaPort = new short[1] ;
         BC000517_n552Contratante_EmailSdaPort = new bool[] {false} ;
         BC000517_A1048Contratante_EmailSdaSec = new short[1] ;
         BC000517_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         BC000517_A593Contratante_OSAutomatica = new bool[] {false} ;
         BC000517_A1652Contratante_SSAutomatica = new bool[] {false} ;
         BC000517_n1652Contratante_SSAutomatica = new bool[] {false} ;
         BC000517_A594Contratante_ExisteConferencia = new bool[] {false} ;
         BC000517_A1448Contratante_InicioDoExpediente = new DateTime[] {DateTime.MinValue} ;
         BC000517_n1448Contratante_InicioDoExpediente = new bool[] {false} ;
         BC000517_A1192Contratante_FimDoExpediente = new DateTime[] {DateTime.MinValue} ;
         BC000517_n1192Contratante_FimDoExpediente = new bool[] {false} ;
         BC000517_A1727Contratante_ServicoSSPadrao = new int[1] ;
         BC000517_n1727Contratante_ServicoSSPadrao = new bool[] {false} ;
         BC000517_A1729Contratante_RetornaSS = new bool[] {false} ;
         BC000517_n1729Contratante_RetornaSS = new bool[] {false} ;
         BC000517_A1732Contratante_SSStatusPlanejamento = new String[] {""} ;
         BC000517_n1732Contratante_SSStatusPlanejamento = new bool[] {false} ;
         BC000517_A1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         BC000517_n1733Contratante_SSPrestadoraUnica = new bool[] {false} ;
         BC000517_A1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         BC000517_n1738Contratante_PropostaRqrSrv = new bool[] {false} ;
         BC000517_A1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         BC000517_n1739Contratante_PropostaRqrPrz = new bool[] {false} ;
         BC000517_A1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         BC000517_n1740Contratante_PropostaRqrEsf = new bool[] {false} ;
         BC000517_A1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         BC000517_n1741Contratante_PropostaRqrCnt = new bool[] {false} ;
         BC000517_A1742Contratante_PropostaNvlCnt = new short[1] ;
         BC000517_n1742Contratante_PropostaNvlCnt = new bool[] {false} ;
         BC000517_A1757Contratante_OSGeraOS = new bool[] {false} ;
         BC000517_n1757Contratante_OSGeraOS = new bool[] {false} ;
         BC000517_A1758Contratante_OSGeraTRP = new bool[] {false} ;
         BC000517_n1758Contratante_OSGeraTRP = new bool[] {false} ;
         BC000517_A1759Contratante_OSGeraTH = new bool[] {false} ;
         BC000517_n1759Contratante_OSGeraTH = new bool[] {false} ;
         BC000517_A1760Contratante_OSGeraTRD = new bool[] {false} ;
         BC000517_n1760Contratante_OSGeraTRD = new bool[] {false} ;
         BC000517_A1761Contratante_OSGeraTR = new bool[] {false} ;
         BC000517_n1761Contratante_OSGeraTR = new bool[] {false} ;
         BC000517_A1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         BC000517_n1803Contratante_OSHmlgComPnd = new bool[] {false} ;
         BC000517_A1805Contratante_AtivoCirculante = new decimal[1] ;
         BC000517_n1805Contratante_AtivoCirculante = new bool[] {false} ;
         BC000517_A1806Contratante_PassivoCirculante = new decimal[1] ;
         BC000517_n1806Contratante_PassivoCirculante = new bool[] {false} ;
         BC000517_A1807Contratante_PatrimonioLiquido = new decimal[1] ;
         BC000517_n1807Contratante_PatrimonioLiquido = new bool[] {false} ;
         BC000517_A1808Contratante_ReceitaBruta = new decimal[1] ;
         BC000517_n1808Contratante_ReceitaBruta = new bool[] {false} ;
         BC000517_A1822Contratante_UsaOSistema = new bool[] {false} ;
         BC000517_n1822Contratante_UsaOSistema = new bool[] {false} ;
         BC000517_A2034Contratante_TtlRltGerencial = new String[] {""} ;
         BC000517_n2034Contratante_TtlRltGerencial = new bool[] {false} ;
         BC000517_A2084Contratante_PrzAoRtr = new short[1] ;
         BC000517_n2084Contratante_PrzAoRtr = new bool[] {false} ;
         BC000517_A2083Contratante_PrzActRtr = new short[1] ;
         BC000517_n2083Contratante_PrzActRtr = new bool[] {false} ;
         BC000517_A2085Contratante_RequerOrigem = new bool[] {false} ;
         BC000517_n2085Contratante_RequerOrigem = new bool[] {false} ;
         BC000517_A2089Contratante_SelecionaResponsavelOS = new bool[] {false} ;
         BC000517_A2090Contratante_ExibePF = new bool[] {false} ;
         BC000517_A1126Contratante_LogoTipoArq = new String[] {""} ;
         BC000517_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         BC000517_A1125Contratante_LogoNomeArq = new String[] {""} ;
         BC000517_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         BC000517_A25Municipio_Codigo = new int[1] ;
         BC000517_n25Municipio_Codigo = new bool[] {false} ;
         BC000517_A335Contratante_PessoaCod = new int[1] ;
         BC000517_A23Estado_UF = new String[] {""} ;
         BC000517_A1124Contratante_LogoArquivo = new String[] {""} ;
         BC000517_n1124Contratante_LogoArquivo = new bool[] {false} ;
         i2034Contratante_TtlRltGerencial = "";
         i1732Contratante_SSStatusPlanejamento = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratante_bc__default(),
            new Object[][] {
                new Object[] {
               BC00052_A29Contratante_Codigo, BC00052_A550Contratante_EmailSdaKey, BC00052_n550Contratante_EmailSdaKey, BC00052_A549Contratante_EmailSdaPass, BC00052_n549Contratante_EmailSdaPass, BC00052_A10Contratante_NomeFantasia, BC00052_A11Contratante_IE, BC00052_A13Contratante_WebSite, BC00052_n13Contratante_WebSite, BC00052_A14Contratante_Email,
               BC00052_n14Contratante_Email, BC00052_A31Contratante_Telefone, BC00052_A32Contratante_Ramal, BC00052_n32Contratante_Ramal, BC00052_A33Contratante_Fax, BC00052_n33Contratante_Fax, BC00052_A336Contratante_AgenciaNome, BC00052_n336Contratante_AgenciaNome, BC00052_A337Contratante_AgenciaNro, BC00052_n337Contratante_AgenciaNro,
               BC00052_A338Contratante_BancoNome, BC00052_n338Contratante_BancoNome, BC00052_A339Contratante_BancoNro, BC00052_n339Contratante_BancoNro, BC00052_A16Contratante_ContaCorrente, BC00052_n16Contratante_ContaCorrente, BC00052_A30Contratante_Ativo, BC00052_A547Contratante_EmailSdaHost, BC00052_n547Contratante_EmailSdaHost, BC00052_A548Contratante_EmailSdaUser,
               BC00052_n548Contratante_EmailSdaUser, BC00052_A551Contratante_EmailSdaAut, BC00052_n551Contratante_EmailSdaAut, BC00052_A552Contratante_EmailSdaPort, BC00052_n552Contratante_EmailSdaPort, BC00052_A1048Contratante_EmailSdaSec, BC00052_n1048Contratante_EmailSdaSec, BC00052_A593Contratante_OSAutomatica, BC00052_A1652Contratante_SSAutomatica, BC00052_n1652Contratante_SSAutomatica,
               BC00052_A594Contratante_ExisteConferencia, BC00052_A1448Contratante_InicioDoExpediente, BC00052_n1448Contratante_InicioDoExpediente, BC00052_A1192Contratante_FimDoExpediente, BC00052_n1192Contratante_FimDoExpediente, BC00052_A1727Contratante_ServicoSSPadrao, BC00052_n1727Contratante_ServicoSSPadrao, BC00052_A1729Contratante_RetornaSS, BC00052_n1729Contratante_RetornaSS, BC00052_A1732Contratante_SSStatusPlanejamento,
               BC00052_n1732Contratante_SSStatusPlanejamento, BC00052_A1733Contratante_SSPrestadoraUnica, BC00052_n1733Contratante_SSPrestadoraUnica, BC00052_A1738Contratante_PropostaRqrSrv, BC00052_n1738Contratante_PropostaRqrSrv, BC00052_A1739Contratante_PropostaRqrPrz, BC00052_n1739Contratante_PropostaRqrPrz, BC00052_A1740Contratante_PropostaRqrEsf, BC00052_n1740Contratante_PropostaRqrEsf, BC00052_A1741Contratante_PropostaRqrCnt,
               BC00052_n1741Contratante_PropostaRqrCnt, BC00052_A1742Contratante_PropostaNvlCnt, BC00052_n1742Contratante_PropostaNvlCnt, BC00052_A1757Contratante_OSGeraOS, BC00052_n1757Contratante_OSGeraOS, BC00052_A1758Contratante_OSGeraTRP, BC00052_n1758Contratante_OSGeraTRP, BC00052_A1759Contratante_OSGeraTH, BC00052_n1759Contratante_OSGeraTH, BC00052_A1760Contratante_OSGeraTRD,
               BC00052_n1760Contratante_OSGeraTRD, BC00052_A1761Contratante_OSGeraTR, BC00052_n1761Contratante_OSGeraTR, BC00052_A1803Contratante_OSHmlgComPnd, BC00052_n1803Contratante_OSHmlgComPnd, BC00052_A1805Contratante_AtivoCirculante, BC00052_n1805Contratante_AtivoCirculante, BC00052_A1806Contratante_PassivoCirculante, BC00052_n1806Contratante_PassivoCirculante, BC00052_A1807Contratante_PatrimonioLiquido,
               BC00052_n1807Contratante_PatrimonioLiquido, BC00052_A1808Contratante_ReceitaBruta, BC00052_n1808Contratante_ReceitaBruta, BC00052_A1822Contratante_UsaOSistema, BC00052_n1822Contratante_UsaOSistema, BC00052_A2034Contratante_TtlRltGerencial, BC00052_n2034Contratante_TtlRltGerencial, BC00052_A2084Contratante_PrzAoRtr, BC00052_n2084Contratante_PrzAoRtr, BC00052_A2083Contratante_PrzActRtr,
               BC00052_n2083Contratante_PrzActRtr, BC00052_A2085Contratante_RequerOrigem, BC00052_n2085Contratante_RequerOrigem, BC00052_A2089Contratante_SelecionaResponsavelOS, BC00052_A2090Contratante_ExibePF, BC00052_A1126Contratante_LogoTipoArq, BC00052_n1126Contratante_LogoTipoArq, BC00052_A1125Contratante_LogoNomeArq, BC00052_n1125Contratante_LogoNomeArq, BC00052_A25Municipio_Codigo,
               BC00052_n25Municipio_Codigo, BC00052_A335Contratante_PessoaCod, BC00052_A1124Contratante_LogoArquivo, BC00052_n1124Contratante_LogoArquivo
               }
               , new Object[] {
               BC00053_A29Contratante_Codigo, BC00053_A550Contratante_EmailSdaKey, BC00053_n550Contratante_EmailSdaKey, BC00053_A549Contratante_EmailSdaPass, BC00053_n549Contratante_EmailSdaPass, BC00053_A10Contratante_NomeFantasia, BC00053_A11Contratante_IE, BC00053_A13Contratante_WebSite, BC00053_n13Contratante_WebSite, BC00053_A14Contratante_Email,
               BC00053_n14Contratante_Email, BC00053_A31Contratante_Telefone, BC00053_A32Contratante_Ramal, BC00053_n32Contratante_Ramal, BC00053_A33Contratante_Fax, BC00053_n33Contratante_Fax, BC00053_A336Contratante_AgenciaNome, BC00053_n336Contratante_AgenciaNome, BC00053_A337Contratante_AgenciaNro, BC00053_n337Contratante_AgenciaNro,
               BC00053_A338Contratante_BancoNome, BC00053_n338Contratante_BancoNome, BC00053_A339Contratante_BancoNro, BC00053_n339Contratante_BancoNro, BC00053_A16Contratante_ContaCorrente, BC00053_n16Contratante_ContaCorrente, BC00053_A30Contratante_Ativo, BC00053_A547Contratante_EmailSdaHost, BC00053_n547Contratante_EmailSdaHost, BC00053_A548Contratante_EmailSdaUser,
               BC00053_n548Contratante_EmailSdaUser, BC00053_A551Contratante_EmailSdaAut, BC00053_n551Contratante_EmailSdaAut, BC00053_A552Contratante_EmailSdaPort, BC00053_n552Contratante_EmailSdaPort, BC00053_A1048Contratante_EmailSdaSec, BC00053_n1048Contratante_EmailSdaSec, BC00053_A593Contratante_OSAutomatica, BC00053_A1652Contratante_SSAutomatica, BC00053_n1652Contratante_SSAutomatica,
               BC00053_A594Contratante_ExisteConferencia, BC00053_A1448Contratante_InicioDoExpediente, BC00053_n1448Contratante_InicioDoExpediente, BC00053_A1192Contratante_FimDoExpediente, BC00053_n1192Contratante_FimDoExpediente, BC00053_A1727Contratante_ServicoSSPadrao, BC00053_n1727Contratante_ServicoSSPadrao, BC00053_A1729Contratante_RetornaSS, BC00053_n1729Contratante_RetornaSS, BC00053_A1732Contratante_SSStatusPlanejamento,
               BC00053_n1732Contratante_SSStatusPlanejamento, BC00053_A1733Contratante_SSPrestadoraUnica, BC00053_n1733Contratante_SSPrestadoraUnica, BC00053_A1738Contratante_PropostaRqrSrv, BC00053_n1738Contratante_PropostaRqrSrv, BC00053_A1739Contratante_PropostaRqrPrz, BC00053_n1739Contratante_PropostaRqrPrz, BC00053_A1740Contratante_PropostaRqrEsf, BC00053_n1740Contratante_PropostaRqrEsf, BC00053_A1741Contratante_PropostaRqrCnt,
               BC00053_n1741Contratante_PropostaRqrCnt, BC00053_A1742Contratante_PropostaNvlCnt, BC00053_n1742Contratante_PropostaNvlCnt, BC00053_A1757Contratante_OSGeraOS, BC00053_n1757Contratante_OSGeraOS, BC00053_A1758Contratante_OSGeraTRP, BC00053_n1758Contratante_OSGeraTRP, BC00053_A1759Contratante_OSGeraTH, BC00053_n1759Contratante_OSGeraTH, BC00053_A1760Contratante_OSGeraTRD,
               BC00053_n1760Contratante_OSGeraTRD, BC00053_A1761Contratante_OSGeraTR, BC00053_n1761Contratante_OSGeraTR, BC00053_A1803Contratante_OSHmlgComPnd, BC00053_n1803Contratante_OSHmlgComPnd, BC00053_A1805Contratante_AtivoCirculante, BC00053_n1805Contratante_AtivoCirculante, BC00053_A1806Contratante_PassivoCirculante, BC00053_n1806Contratante_PassivoCirculante, BC00053_A1807Contratante_PatrimonioLiquido,
               BC00053_n1807Contratante_PatrimonioLiquido, BC00053_A1808Contratante_ReceitaBruta, BC00053_n1808Contratante_ReceitaBruta, BC00053_A1822Contratante_UsaOSistema, BC00053_n1822Contratante_UsaOSistema, BC00053_A2034Contratante_TtlRltGerencial, BC00053_n2034Contratante_TtlRltGerencial, BC00053_A2084Contratante_PrzAoRtr, BC00053_n2084Contratante_PrzAoRtr, BC00053_A2083Contratante_PrzActRtr,
               BC00053_n2083Contratante_PrzActRtr, BC00053_A2085Contratante_RequerOrigem, BC00053_n2085Contratante_RequerOrigem, BC00053_A2089Contratante_SelecionaResponsavelOS, BC00053_A2090Contratante_ExibePF, BC00053_A1126Contratante_LogoTipoArq, BC00053_n1126Contratante_LogoTipoArq, BC00053_A1125Contratante_LogoNomeArq, BC00053_n1125Contratante_LogoNomeArq, BC00053_A25Municipio_Codigo,
               BC00053_n25Municipio_Codigo, BC00053_A335Contratante_PessoaCod, BC00053_A1124Contratante_LogoArquivo, BC00053_n1124Contratante_LogoArquivo
               }
               , new Object[] {
               BC00054_A26Municipio_Nome, BC00054_A23Estado_UF
               }
               , new Object[] {
               BC00055_A12Contratante_CNPJ, BC00055_n12Contratante_CNPJ, BC00055_A9Contratante_RazaoSocial, BC00055_n9Contratante_RazaoSocial
               }
               , new Object[] {
               BC00056_A29Contratante_Codigo, BC00056_A550Contratante_EmailSdaKey, BC00056_n550Contratante_EmailSdaKey, BC00056_A549Contratante_EmailSdaPass, BC00056_n549Contratante_EmailSdaPass, BC00056_A12Contratante_CNPJ, BC00056_n12Contratante_CNPJ, BC00056_A9Contratante_RazaoSocial, BC00056_n9Contratante_RazaoSocial, BC00056_A10Contratante_NomeFantasia,
               BC00056_A11Contratante_IE, BC00056_A13Contratante_WebSite, BC00056_n13Contratante_WebSite, BC00056_A14Contratante_Email, BC00056_n14Contratante_Email, BC00056_A31Contratante_Telefone, BC00056_A32Contratante_Ramal, BC00056_n32Contratante_Ramal, BC00056_A33Contratante_Fax, BC00056_n33Contratante_Fax,
               BC00056_A336Contratante_AgenciaNome, BC00056_n336Contratante_AgenciaNome, BC00056_A337Contratante_AgenciaNro, BC00056_n337Contratante_AgenciaNro, BC00056_A338Contratante_BancoNome, BC00056_n338Contratante_BancoNome, BC00056_A339Contratante_BancoNro, BC00056_n339Contratante_BancoNro, BC00056_A16Contratante_ContaCorrente, BC00056_n16Contratante_ContaCorrente,
               BC00056_A26Municipio_Nome, BC00056_A30Contratante_Ativo, BC00056_A547Contratante_EmailSdaHost, BC00056_n547Contratante_EmailSdaHost, BC00056_A548Contratante_EmailSdaUser, BC00056_n548Contratante_EmailSdaUser, BC00056_A551Contratante_EmailSdaAut, BC00056_n551Contratante_EmailSdaAut, BC00056_A552Contratante_EmailSdaPort, BC00056_n552Contratante_EmailSdaPort,
               BC00056_A1048Contratante_EmailSdaSec, BC00056_n1048Contratante_EmailSdaSec, BC00056_A593Contratante_OSAutomatica, BC00056_A1652Contratante_SSAutomatica, BC00056_n1652Contratante_SSAutomatica, BC00056_A594Contratante_ExisteConferencia, BC00056_A1448Contratante_InicioDoExpediente, BC00056_n1448Contratante_InicioDoExpediente, BC00056_A1192Contratante_FimDoExpediente, BC00056_n1192Contratante_FimDoExpediente,
               BC00056_A1727Contratante_ServicoSSPadrao, BC00056_n1727Contratante_ServicoSSPadrao, BC00056_A1729Contratante_RetornaSS, BC00056_n1729Contratante_RetornaSS, BC00056_A1732Contratante_SSStatusPlanejamento, BC00056_n1732Contratante_SSStatusPlanejamento, BC00056_A1733Contratante_SSPrestadoraUnica, BC00056_n1733Contratante_SSPrestadoraUnica, BC00056_A1738Contratante_PropostaRqrSrv, BC00056_n1738Contratante_PropostaRqrSrv,
               BC00056_A1739Contratante_PropostaRqrPrz, BC00056_n1739Contratante_PropostaRqrPrz, BC00056_A1740Contratante_PropostaRqrEsf, BC00056_n1740Contratante_PropostaRqrEsf, BC00056_A1741Contratante_PropostaRqrCnt, BC00056_n1741Contratante_PropostaRqrCnt, BC00056_A1742Contratante_PropostaNvlCnt, BC00056_n1742Contratante_PropostaNvlCnt, BC00056_A1757Contratante_OSGeraOS, BC00056_n1757Contratante_OSGeraOS,
               BC00056_A1758Contratante_OSGeraTRP, BC00056_n1758Contratante_OSGeraTRP, BC00056_A1759Contratante_OSGeraTH, BC00056_n1759Contratante_OSGeraTH, BC00056_A1760Contratante_OSGeraTRD, BC00056_n1760Contratante_OSGeraTRD, BC00056_A1761Contratante_OSGeraTR, BC00056_n1761Contratante_OSGeraTR, BC00056_A1803Contratante_OSHmlgComPnd, BC00056_n1803Contratante_OSHmlgComPnd,
               BC00056_A1805Contratante_AtivoCirculante, BC00056_n1805Contratante_AtivoCirculante, BC00056_A1806Contratante_PassivoCirculante, BC00056_n1806Contratante_PassivoCirculante, BC00056_A1807Contratante_PatrimonioLiquido, BC00056_n1807Contratante_PatrimonioLiquido, BC00056_A1808Contratante_ReceitaBruta, BC00056_n1808Contratante_ReceitaBruta, BC00056_A1822Contratante_UsaOSistema, BC00056_n1822Contratante_UsaOSistema,
               BC00056_A2034Contratante_TtlRltGerencial, BC00056_n2034Contratante_TtlRltGerencial, BC00056_A2084Contratante_PrzAoRtr, BC00056_n2084Contratante_PrzAoRtr, BC00056_A2083Contratante_PrzActRtr, BC00056_n2083Contratante_PrzActRtr, BC00056_A2085Contratante_RequerOrigem, BC00056_n2085Contratante_RequerOrigem, BC00056_A2089Contratante_SelecionaResponsavelOS, BC00056_A2090Contratante_ExibePF,
               BC00056_A1126Contratante_LogoTipoArq, BC00056_n1126Contratante_LogoTipoArq, BC00056_A1125Contratante_LogoNomeArq, BC00056_n1125Contratante_LogoNomeArq, BC00056_A25Municipio_Codigo, BC00056_n25Municipio_Codigo, BC00056_A335Contratante_PessoaCod, BC00056_A23Estado_UF, BC00056_A1124Contratante_LogoArquivo, BC00056_n1124Contratante_LogoArquivo
               }
               , new Object[] {
               BC00057_A29Contratante_Codigo
               }
               , new Object[] {
               BC00058_A29Contratante_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000512_A12Contratante_CNPJ, BC000512_n12Contratante_CNPJ, BC000512_A9Contratante_RazaoSocial, BC000512_n9Contratante_RazaoSocial
               }
               , new Object[] {
               BC000513_A26Municipio_Nome, BC000513_A23Estado_UF
               }
               , new Object[] {
               BC000514_A1380Redmine_Codigo
               }
               , new Object[] {
               BC000515_A63ContratanteUsuario_ContratanteCod, BC000515_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               BC000516_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               BC000517_A29Contratante_Codigo, BC000517_A550Contratante_EmailSdaKey, BC000517_n550Contratante_EmailSdaKey, BC000517_A549Contratante_EmailSdaPass, BC000517_n549Contratante_EmailSdaPass, BC000517_A12Contratante_CNPJ, BC000517_n12Contratante_CNPJ, BC000517_A9Contratante_RazaoSocial, BC000517_n9Contratante_RazaoSocial, BC000517_A10Contratante_NomeFantasia,
               BC000517_A11Contratante_IE, BC000517_A13Contratante_WebSite, BC000517_n13Contratante_WebSite, BC000517_A14Contratante_Email, BC000517_n14Contratante_Email, BC000517_A31Contratante_Telefone, BC000517_A32Contratante_Ramal, BC000517_n32Contratante_Ramal, BC000517_A33Contratante_Fax, BC000517_n33Contratante_Fax,
               BC000517_A336Contratante_AgenciaNome, BC000517_n336Contratante_AgenciaNome, BC000517_A337Contratante_AgenciaNro, BC000517_n337Contratante_AgenciaNro, BC000517_A338Contratante_BancoNome, BC000517_n338Contratante_BancoNome, BC000517_A339Contratante_BancoNro, BC000517_n339Contratante_BancoNro, BC000517_A16Contratante_ContaCorrente, BC000517_n16Contratante_ContaCorrente,
               BC000517_A26Municipio_Nome, BC000517_A30Contratante_Ativo, BC000517_A547Contratante_EmailSdaHost, BC000517_n547Contratante_EmailSdaHost, BC000517_A548Contratante_EmailSdaUser, BC000517_n548Contratante_EmailSdaUser, BC000517_A551Contratante_EmailSdaAut, BC000517_n551Contratante_EmailSdaAut, BC000517_A552Contratante_EmailSdaPort, BC000517_n552Contratante_EmailSdaPort,
               BC000517_A1048Contratante_EmailSdaSec, BC000517_n1048Contratante_EmailSdaSec, BC000517_A593Contratante_OSAutomatica, BC000517_A1652Contratante_SSAutomatica, BC000517_n1652Contratante_SSAutomatica, BC000517_A594Contratante_ExisteConferencia, BC000517_A1448Contratante_InicioDoExpediente, BC000517_n1448Contratante_InicioDoExpediente, BC000517_A1192Contratante_FimDoExpediente, BC000517_n1192Contratante_FimDoExpediente,
               BC000517_A1727Contratante_ServicoSSPadrao, BC000517_n1727Contratante_ServicoSSPadrao, BC000517_A1729Contratante_RetornaSS, BC000517_n1729Contratante_RetornaSS, BC000517_A1732Contratante_SSStatusPlanejamento, BC000517_n1732Contratante_SSStatusPlanejamento, BC000517_A1733Contratante_SSPrestadoraUnica, BC000517_n1733Contratante_SSPrestadoraUnica, BC000517_A1738Contratante_PropostaRqrSrv, BC000517_n1738Contratante_PropostaRqrSrv,
               BC000517_A1739Contratante_PropostaRqrPrz, BC000517_n1739Contratante_PropostaRqrPrz, BC000517_A1740Contratante_PropostaRqrEsf, BC000517_n1740Contratante_PropostaRqrEsf, BC000517_A1741Contratante_PropostaRqrCnt, BC000517_n1741Contratante_PropostaRqrCnt, BC000517_A1742Contratante_PropostaNvlCnt, BC000517_n1742Contratante_PropostaNvlCnt, BC000517_A1757Contratante_OSGeraOS, BC000517_n1757Contratante_OSGeraOS,
               BC000517_A1758Contratante_OSGeraTRP, BC000517_n1758Contratante_OSGeraTRP, BC000517_A1759Contratante_OSGeraTH, BC000517_n1759Contratante_OSGeraTH, BC000517_A1760Contratante_OSGeraTRD, BC000517_n1760Contratante_OSGeraTRD, BC000517_A1761Contratante_OSGeraTR, BC000517_n1761Contratante_OSGeraTR, BC000517_A1803Contratante_OSHmlgComPnd, BC000517_n1803Contratante_OSHmlgComPnd,
               BC000517_A1805Contratante_AtivoCirculante, BC000517_n1805Contratante_AtivoCirculante, BC000517_A1806Contratante_PassivoCirculante, BC000517_n1806Contratante_PassivoCirculante, BC000517_A1807Contratante_PatrimonioLiquido, BC000517_n1807Contratante_PatrimonioLiquido, BC000517_A1808Contratante_ReceitaBruta, BC000517_n1808Contratante_ReceitaBruta, BC000517_A1822Contratante_UsaOSistema, BC000517_n1822Contratante_UsaOSistema,
               BC000517_A2034Contratante_TtlRltGerencial, BC000517_n2034Contratante_TtlRltGerencial, BC000517_A2084Contratante_PrzAoRtr, BC000517_n2084Contratante_PrzAoRtr, BC000517_A2083Contratante_PrzActRtr, BC000517_n2083Contratante_PrzActRtr, BC000517_A2085Contratante_RequerOrigem, BC000517_n2085Contratante_RequerOrigem, BC000517_A2089Contratante_SelecionaResponsavelOS, BC000517_A2090Contratante_ExibePF,
               BC000517_A1126Contratante_LogoTipoArq, BC000517_n1126Contratante_LogoTipoArq, BC000517_A1125Contratante_LogoNomeArq, BC000517_n1125Contratante_LogoNomeArq, BC000517_A25Municipio_Codigo, BC000517_n25Municipio_Codigo, BC000517_A335Contratante_PessoaCod, BC000517_A23Estado_UF, BC000517_A1124Contratante_LogoArquivo, BC000517_n1124Contratante_LogoArquivo
               }
            }
         );
         Z2085Contratante_RequerOrigem = true;
         n2085Contratante_RequerOrigem = false;
         A2085Contratante_RequerOrigem = true;
         n2085Contratante_RequerOrigem = false;
         i2085Contratante_RequerOrigem = true;
         n2085Contratante_RequerOrigem = false;
         Z2034Contratante_TtlRltGerencial = "Relat�rio Gerencial";
         n2034Contratante_TtlRltGerencial = false;
         A2034Contratante_TtlRltGerencial = "Relat�rio Gerencial";
         n2034Contratante_TtlRltGerencial = false;
         i2034Contratante_TtlRltGerencial = "Relat�rio Gerencial";
         n2034Contratante_TtlRltGerencial = false;
         Z1822Contratante_UsaOSistema = false;
         n1822Contratante_UsaOSistema = false;
         A1822Contratante_UsaOSistema = false;
         n1822Contratante_UsaOSistema = false;
         i1822Contratante_UsaOSistema = false;
         n1822Contratante_UsaOSistema = false;
         Z1742Contratante_PropostaNvlCnt = 0;
         n1742Contratante_PropostaNvlCnt = false;
         A1742Contratante_PropostaNvlCnt = 0;
         n1742Contratante_PropostaNvlCnt = false;
         i1742Contratante_PropostaNvlCnt = 0;
         n1742Contratante_PropostaNvlCnt = false;
         Z1738Contratante_PropostaRqrSrv = true;
         n1738Contratante_PropostaRqrSrv = false;
         A1738Contratante_PropostaRqrSrv = true;
         n1738Contratante_PropostaRqrSrv = false;
         i1738Contratante_PropostaRqrSrv = true;
         n1738Contratante_PropostaRqrSrv = false;
         Z1732Contratante_SSStatusPlanejamento = "B";
         n1732Contratante_SSStatusPlanejamento = false;
         A1732Contratante_SSStatusPlanejamento = "B";
         n1732Contratante_SSStatusPlanejamento = false;
         i1732Contratante_SSStatusPlanejamento = "B";
         n1732Contratante_SSStatusPlanejamento = false;
         Z551Contratante_EmailSdaAut = false;
         n551Contratante_EmailSdaAut = false;
         A551Contratante_EmailSdaAut = false;
         n551Contratante_EmailSdaAut = false;
         i551Contratante_EmailSdaAut = false;
         n551Contratante_EmailSdaAut = false;
         Z30Contratante_Ativo = true;
         A30Contratante_Ativo = true;
         i30Contratante_Ativo = true;
         AV45Pgmname = "Contratante_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12052 */
         E12052 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short AV43retorno ;
      private short GX_JID ;
      private short Z552Contratante_EmailSdaPort ;
      private short A552Contratante_EmailSdaPort ;
      private short Z1048Contratante_EmailSdaSec ;
      private short A1048Contratante_EmailSdaSec ;
      private short Z1742Contratante_PropostaNvlCnt ;
      private short A1742Contratante_PropostaNvlCnt ;
      private short Z2084Contratante_PrzAoRtr ;
      private short A2084Contratante_PrzAoRtr ;
      private short Z2083Contratante_PrzActRtr ;
      private short A2083Contratante_PrzActRtr ;
      private short Gx_BScreen ;
      private short RcdFound124 ;
      private short i1742Contratante_PropostaNvlCnt ;
      private int trnEnded ;
      private int Z29Contratante_Codigo ;
      private int A29Contratante_Codigo ;
      private int AV46GXV1 ;
      private int AV16Insert_Contratante_PessoaCod ;
      private int AV17Insert_Municipio_Codigo ;
      private int Z1727Contratante_ServicoSSPadrao ;
      private int A1727Contratante_ServicoSSPadrao ;
      private int Z25Municipio_Codigo ;
      private int A25Municipio_Codigo ;
      private int Z335Contratante_PessoaCod ;
      private int A335Contratante_PessoaCod ;
      private decimal Z1805Contratante_AtivoCirculante ;
      private decimal A1805Contratante_AtivoCirculante ;
      private decimal Z1806Contratante_PassivoCirculante ;
      private decimal A1806Contratante_PassivoCirculante ;
      private decimal Z1807Contratante_PatrimonioLiquido ;
      private decimal A1807Contratante_PatrimonioLiquido ;
      private decimal Z1808Contratante_ReceitaBruta ;
      private decimal A1808Contratante_ReceitaBruta ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV45Pgmname ;
      private String AV38Subject ;
      private String AV36Resultado ;
      private String A550Contratante_EmailSdaKey ;
      private String Z550Contratante_EmailSdaKey ;
      private String Z10Contratante_NomeFantasia ;
      private String A10Contratante_NomeFantasia ;
      private String Z11Contratante_IE ;
      private String A11Contratante_IE ;
      private String Z31Contratante_Telefone ;
      private String A31Contratante_Telefone ;
      private String Z32Contratante_Ramal ;
      private String A32Contratante_Ramal ;
      private String Z33Contratante_Fax ;
      private String A33Contratante_Fax ;
      private String Z336Contratante_AgenciaNome ;
      private String A336Contratante_AgenciaNome ;
      private String Z337Contratante_AgenciaNro ;
      private String A337Contratante_AgenciaNro ;
      private String Z338Contratante_BancoNome ;
      private String A338Contratante_BancoNome ;
      private String Z339Contratante_BancoNro ;
      private String A339Contratante_BancoNro ;
      private String Z16Contratante_ContaCorrente ;
      private String A16Contratante_ContaCorrente ;
      private String Z1732Contratante_SSStatusPlanejamento ;
      private String A1732Contratante_SSStatusPlanejamento ;
      private String Z26Municipio_Nome ;
      private String A26Municipio_Nome ;
      private String Z23Estado_UF ;
      private String A23Estado_UF ;
      private String Z9Contratante_RazaoSocial ;
      private String A9Contratante_RazaoSocial ;
      private String Z1126Contratante_LogoTipoArq ;
      private String A1126Contratante_LogoTipoArq ;
      private String Z1125Contratante_LogoNomeArq ;
      private String A1125Contratante_LogoNomeArq ;
      private String A1124Contratante_LogoArquivo_Filetype ;
      private String A1124Contratante_LogoArquivo_Filename ;
      private String AV15Estado_UF ;
      private String sMode124 ;
      private String i1732Contratante_SSStatusPlanejamento ;
      private DateTime Z1448Contratante_InicioDoExpediente ;
      private DateTime A1448Contratante_InicioDoExpediente ;
      private DateTime Z1192Contratante_FimDoExpediente ;
      private DateTime A1192Contratante_FimDoExpediente ;
      private bool Z30Contratante_Ativo ;
      private bool A30Contratante_Ativo ;
      private bool Z551Contratante_EmailSdaAut ;
      private bool A551Contratante_EmailSdaAut ;
      private bool Z593Contratante_OSAutomatica ;
      private bool A593Contratante_OSAutomatica ;
      private bool Z1652Contratante_SSAutomatica ;
      private bool A1652Contratante_SSAutomatica ;
      private bool Z594Contratante_ExisteConferencia ;
      private bool A594Contratante_ExisteConferencia ;
      private bool Z1729Contratante_RetornaSS ;
      private bool A1729Contratante_RetornaSS ;
      private bool Z1733Contratante_SSPrestadoraUnica ;
      private bool A1733Contratante_SSPrestadoraUnica ;
      private bool Z1738Contratante_PropostaRqrSrv ;
      private bool A1738Contratante_PropostaRqrSrv ;
      private bool Z1739Contratante_PropostaRqrPrz ;
      private bool A1739Contratante_PropostaRqrPrz ;
      private bool Z1740Contratante_PropostaRqrEsf ;
      private bool A1740Contratante_PropostaRqrEsf ;
      private bool Z1741Contratante_PropostaRqrCnt ;
      private bool A1741Contratante_PropostaRqrCnt ;
      private bool Z1757Contratante_OSGeraOS ;
      private bool A1757Contratante_OSGeraOS ;
      private bool Z1758Contratante_OSGeraTRP ;
      private bool A1758Contratante_OSGeraTRP ;
      private bool Z1759Contratante_OSGeraTH ;
      private bool A1759Contratante_OSGeraTH ;
      private bool Z1760Contratante_OSGeraTRD ;
      private bool A1760Contratante_OSGeraTRD ;
      private bool Z1761Contratante_OSGeraTR ;
      private bool A1761Contratante_OSGeraTR ;
      private bool Z1803Contratante_OSHmlgComPnd ;
      private bool A1803Contratante_OSHmlgComPnd ;
      private bool Z1822Contratante_UsaOSistema ;
      private bool A1822Contratante_UsaOSistema ;
      private bool Z2085Contratante_RequerOrigem ;
      private bool A2085Contratante_RequerOrigem ;
      private bool Z2089Contratante_SelecionaResponsavelOS ;
      private bool A2089Contratante_SelecionaResponsavelOS ;
      private bool Z2090Contratante_ExibePF ;
      private bool A2090Contratante_ExibePF ;
      private bool n2085Contratante_RequerOrigem ;
      private bool n2034Contratante_TtlRltGerencial ;
      private bool n1822Contratante_UsaOSistema ;
      private bool n1742Contratante_PropostaNvlCnt ;
      private bool n1738Contratante_PropostaRqrSrv ;
      private bool n1732Contratante_SSStatusPlanejamento ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n29Contratante_Codigo ;
      private bool n550Contratante_EmailSdaKey ;
      private bool n549Contratante_EmailSdaPass ;
      private bool n12Contratante_CNPJ ;
      private bool n9Contratante_RazaoSocial ;
      private bool n13Contratante_WebSite ;
      private bool n14Contratante_Email ;
      private bool n32Contratante_Ramal ;
      private bool n33Contratante_Fax ;
      private bool n336Contratante_AgenciaNome ;
      private bool n337Contratante_AgenciaNro ;
      private bool n338Contratante_BancoNome ;
      private bool n339Contratante_BancoNro ;
      private bool n16Contratante_ContaCorrente ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n552Contratante_EmailSdaPort ;
      private bool n1048Contratante_EmailSdaSec ;
      private bool n1652Contratante_SSAutomatica ;
      private bool n1448Contratante_InicioDoExpediente ;
      private bool n1192Contratante_FimDoExpediente ;
      private bool n1727Contratante_ServicoSSPadrao ;
      private bool n1729Contratante_RetornaSS ;
      private bool n1733Contratante_SSPrestadoraUnica ;
      private bool n1739Contratante_PropostaRqrPrz ;
      private bool n1740Contratante_PropostaRqrEsf ;
      private bool n1741Contratante_PropostaRqrCnt ;
      private bool n1757Contratante_OSGeraOS ;
      private bool n1758Contratante_OSGeraTRP ;
      private bool n1759Contratante_OSGeraTH ;
      private bool n1760Contratante_OSGeraTRD ;
      private bool n1761Contratante_OSGeraTR ;
      private bool n1803Contratante_OSHmlgComPnd ;
      private bool n1805Contratante_AtivoCirculante ;
      private bool n1806Contratante_PassivoCirculante ;
      private bool n1807Contratante_PatrimonioLiquido ;
      private bool n1808Contratante_ReceitaBruta ;
      private bool n2084Contratante_PrzAoRtr ;
      private bool n2083Contratante_PrzActRtr ;
      private bool n1126Contratante_LogoTipoArq ;
      private bool n1125Contratante_LogoNomeArq ;
      private bool n25Municipio_Codigo ;
      private bool n1124Contratante_LogoArquivo ;
      private bool Gx_longc ;
      private bool i2085Contratante_RequerOrigem ;
      private bool i1822Contratante_UsaOSistema ;
      private bool i1738Contratante_PropostaRqrSrv ;
      private bool i551Contratante_EmailSdaAut ;
      private bool i30Contratante_Ativo ;
      private String AV37EmailText ;
      private String A549Contratante_EmailSdaPass ;
      private String O549Contratante_EmailSdaPass ;
      private String A548Contratante_EmailSdaUser ;
      private String Z549Contratante_EmailSdaPass ;
      private String Z13Contratante_WebSite ;
      private String A13Contratante_WebSite ;
      private String Z14Contratante_Email ;
      private String A14Contratante_Email ;
      private String Z547Contratante_EmailSdaHost ;
      private String A547Contratante_EmailSdaHost ;
      private String Z548Contratante_EmailSdaUser ;
      private String Z2034Contratante_TtlRltGerencial ;
      private String A2034Contratante_TtlRltGerencial ;
      private String Z12Contratante_CNPJ ;
      private String A12Contratante_CNPJ ;
      private String i2034Contratante_TtlRltGerencial ;
      private String Z1124Contratante_LogoArquivo ;
      private String A1124Contratante_LogoArquivo ;
      private IGxSession AV10WebSession ;
      private SdtContratante bcContratante ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC00056_A29Contratante_Codigo ;
      private bool[] BC00056_n29Contratante_Codigo ;
      private String[] BC00056_A550Contratante_EmailSdaKey ;
      private bool[] BC00056_n550Contratante_EmailSdaKey ;
      private String[] BC00056_A549Contratante_EmailSdaPass ;
      private bool[] BC00056_n549Contratante_EmailSdaPass ;
      private String[] BC00056_A12Contratante_CNPJ ;
      private bool[] BC00056_n12Contratante_CNPJ ;
      private String[] BC00056_A9Contratante_RazaoSocial ;
      private bool[] BC00056_n9Contratante_RazaoSocial ;
      private String[] BC00056_A10Contratante_NomeFantasia ;
      private String[] BC00056_A11Contratante_IE ;
      private String[] BC00056_A13Contratante_WebSite ;
      private bool[] BC00056_n13Contratante_WebSite ;
      private String[] BC00056_A14Contratante_Email ;
      private bool[] BC00056_n14Contratante_Email ;
      private String[] BC00056_A31Contratante_Telefone ;
      private String[] BC00056_A32Contratante_Ramal ;
      private bool[] BC00056_n32Contratante_Ramal ;
      private String[] BC00056_A33Contratante_Fax ;
      private bool[] BC00056_n33Contratante_Fax ;
      private String[] BC00056_A336Contratante_AgenciaNome ;
      private bool[] BC00056_n336Contratante_AgenciaNome ;
      private String[] BC00056_A337Contratante_AgenciaNro ;
      private bool[] BC00056_n337Contratante_AgenciaNro ;
      private String[] BC00056_A338Contratante_BancoNome ;
      private bool[] BC00056_n338Contratante_BancoNome ;
      private String[] BC00056_A339Contratante_BancoNro ;
      private bool[] BC00056_n339Contratante_BancoNro ;
      private String[] BC00056_A16Contratante_ContaCorrente ;
      private bool[] BC00056_n16Contratante_ContaCorrente ;
      private String[] BC00056_A26Municipio_Nome ;
      private bool[] BC00056_A30Contratante_Ativo ;
      private String[] BC00056_A547Contratante_EmailSdaHost ;
      private bool[] BC00056_n547Contratante_EmailSdaHost ;
      private String[] BC00056_A548Contratante_EmailSdaUser ;
      private bool[] BC00056_n548Contratante_EmailSdaUser ;
      private bool[] BC00056_A551Contratante_EmailSdaAut ;
      private bool[] BC00056_n551Contratante_EmailSdaAut ;
      private short[] BC00056_A552Contratante_EmailSdaPort ;
      private bool[] BC00056_n552Contratante_EmailSdaPort ;
      private short[] BC00056_A1048Contratante_EmailSdaSec ;
      private bool[] BC00056_n1048Contratante_EmailSdaSec ;
      private bool[] BC00056_A593Contratante_OSAutomatica ;
      private bool[] BC00056_A1652Contratante_SSAutomatica ;
      private bool[] BC00056_n1652Contratante_SSAutomatica ;
      private bool[] BC00056_A594Contratante_ExisteConferencia ;
      private DateTime[] BC00056_A1448Contratante_InicioDoExpediente ;
      private bool[] BC00056_n1448Contratante_InicioDoExpediente ;
      private DateTime[] BC00056_A1192Contratante_FimDoExpediente ;
      private bool[] BC00056_n1192Contratante_FimDoExpediente ;
      private int[] BC00056_A1727Contratante_ServicoSSPadrao ;
      private bool[] BC00056_n1727Contratante_ServicoSSPadrao ;
      private bool[] BC00056_A1729Contratante_RetornaSS ;
      private bool[] BC00056_n1729Contratante_RetornaSS ;
      private String[] BC00056_A1732Contratante_SSStatusPlanejamento ;
      private bool[] BC00056_n1732Contratante_SSStatusPlanejamento ;
      private bool[] BC00056_A1733Contratante_SSPrestadoraUnica ;
      private bool[] BC00056_n1733Contratante_SSPrestadoraUnica ;
      private bool[] BC00056_A1738Contratante_PropostaRqrSrv ;
      private bool[] BC00056_n1738Contratante_PropostaRqrSrv ;
      private bool[] BC00056_A1739Contratante_PropostaRqrPrz ;
      private bool[] BC00056_n1739Contratante_PropostaRqrPrz ;
      private bool[] BC00056_A1740Contratante_PropostaRqrEsf ;
      private bool[] BC00056_n1740Contratante_PropostaRqrEsf ;
      private bool[] BC00056_A1741Contratante_PropostaRqrCnt ;
      private bool[] BC00056_n1741Contratante_PropostaRqrCnt ;
      private short[] BC00056_A1742Contratante_PropostaNvlCnt ;
      private bool[] BC00056_n1742Contratante_PropostaNvlCnt ;
      private bool[] BC00056_A1757Contratante_OSGeraOS ;
      private bool[] BC00056_n1757Contratante_OSGeraOS ;
      private bool[] BC00056_A1758Contratante_OSGeraTRP ;
      private bool[] BC00056_n1758Contratante_OSGeraTRP ;
      private bool[] BC00056_A1759Contratante_OSGeraTH ;
      private bool[] BC00056_n1759Contratante_OSGeraTH ;
      private bool[] BC00056_A1760Contratante_OSGeraTRD ;
      private bool[] BC00056_n1760Contratante_OSGeraTRD ;
      private bool[] BC00056_A1761Contratante_OSGeraTR ;
      private bool[] BC00056_n1761Contratante_OSGeraTR ;
      private bool[] BC00056_A1803Contratante_OSHmlgComPnd ;
      private bool[] BC00056_n1803Contratante_OSHmlgComPnd ;
      private decimal[] BC00056_A1805Contratante_AtivoCirculante ;
      private bool[] BC00056_n1805Contratante_AtivoCirculante ;
      private decimal[] BC00056_A1806Contratante_PassivoCirculante ;
      private bool[] BC00056_n1806Contratante_PassivoCirculante ;
      private decimal[] BC00056_A1807Contratante_PatrimonioLiquido ;
      private bool[] BC00056_n1807Contratante_PatrimonioLiquido ;
      private decimal[] BC00056_A1808Contratante_ReceitaBruta ;
      private bool[] BC00056_n1808Contratante_ReceitaBruta ;
      private bool[] BC00056_A1822Contratante_UsaOSistema ;
      private bool[] BC00056_n1822Contratante_UsaOSistema ;
      private String[] BC00056_A2034Contratante_TtlRltGerencial ;
      private bool[] BC00056_n2034Contratante_TtlRltGerencial ;
      private short[] BC00056_A2084Contratante_PrzAoRtr ;
      private bool[] BC00056_n2084Contratante_PrzAoRtr ;
      private short[] BC00056_A2083Contratante_PrzActRtr ;
      private bool[] BC00056_n2083Contratante_PrzActRtr ;
      private bool[] BC00056_A2085Contratante_RequerOrigem ;
      private bool[] BC00056_n2085Contratante_RequerOrigem ;
      private bool[] BC00056_A2089Contratante_SelecionaResponsavelOS ;
      private bool[] BC00056_A2090Contratante_ExibePF ;
      private String[] BC00056_A1126Contratante_LogoTipoArq ;
      private bool[] BC00056_n1126Contratante_LogoTipoArq ;
      private String[] BC00056_A1125Contratante_LogoNomeArq ;
      private bool[] BC00056_n1125Contratante_LogoNomeArq ;
      private int[] BC00056_A25Municipio_Codigo ;
      private bool[] BC00056_n25Municipio_Codigo ;
      private int[] BC00056_A335Contratante_PessoaCod ;
      private String[] BC00056_A23Estado_UF ;
      private String[] BC00056_A1124Contratante_LogoArquivo ;
      private bool[] BC00056_n1124Contratante_LogoArquivo ;
      private String[] BC00055_A12Contratante_CNPJ ;
      private bool[] BC00055_n12Contratante_CNPJ ;
      private String[] BC00055_A9Contratante_RazaoSocial ;
      private bool[] BC00055_n9Contratante_RazaoSocial ;
      private String[] BC00054_A26Municipio_Nome ;
      private String[] BC00054_A23Estado_UF ;
      private int[] BC00057_A29Contratante_Codigo ;
      private bool[] BC00057_n29Contratante_Codigo ;
      private int[] BC00053_A29Contratante_Codigo ;
      private bool[] BC00053_n29Contratante_Codigo ;
      private String[] BC00053_A550Contratante_EmailSdaKey ;
      private bool[] BC00053_n550Contratante_EmailSdaKey ;
      private String[] BC00053_A549Contratante_EmailSdaPass ;
      private bool[] BC00053_n549Contratante_EmailSdaPass ;
      private String[] BC00053_A10Contratante_NomeFantasia ;
      private String[] BC00053_A11Contratante_IE ;
      private String[] BC00053_A13Contratante_WebSite ;
      private bool[] BC00053_n13Contratante_WebSite ;
      private String[] BC00053_A14Contratante_Email ;
      private bool[] BC00053_n14Contratante_Email ;
      private String[] BC00053_A31Contratante_Telefone ;
      private String[] BC00053_A32Contratante_Ramal ;
      private bool[] BC00053_n32Contratante_Ramal ;
      private String[] BC00053_A33Contratante_Fax ;
      private bool[] BC00053_n33Contratante_Fax ;
      private String[] BC00053_A336Contratante_AgenciaNome ;
      private bool[] BC00053_n336Contratante_AgenciaNome ;
      private String[] BC00053_A337Contratante_AgenciaNro ;
      private bool[] BC00053_n337Contratante_AgenciaNro ;
      private String[] BC00053_A338Contratante_BancoNome ;
      private bool[] BC00053_n338Contratante_BancoNome ;
      private String[] BC00053_A339Contratante_BancoNro ;
      private bool[] BC00053_n339Contratante_BancoNro ;
      private String[] BC00053_A16Contratante_ContaCorrente ;
      private bool[] BC00053_n16Contratante_ContaCorrente ;
      private bool[] BC00053_A30Contratante_Ativo ;
      private String[] BC00053_A547Contratante_EmailSdaHost ;
      private bool[] BC00053_n547Contratante_EmailSdaHost ;
      private String[] BC00053_A548Contratante_EmailSdaUser ;
      private bool[] BC00053_n548Contratante_EmailSdaUser ;
      private bool[] BC00053_A551Contratante_EmailSdaAut ;
      private bool[] BC00053_n551Contratante_EmailSdaAut ;
      private short[] BC00053_A552Contratante_EmailSdaPort ;
      private bool[] BC00053_n552Contratante_EmailSdaPort ;
      private short[] BC00053_A1048Contratante_EmailSdaSec ;
      private bool[] BC00053_n1048Contratante_EmailSdaSec ;
      private bool[] BC00053_A593Contratante_OSAutomatica ;
      private bool[] BC00053_A1652Contratante_SSAutomatica ;
      private bool[] BC00053_n1652Contratante_SSAutomatica ;
      private bool[] BC00053_A594Contratante_ExisteConferencia ;
      private DateTime[] BC00053_A1448Contratante_InicioDoExpediente ;
      private bool[] BC00053_n1448Contratante_InicioDoExpediente ;
      private DateTime[] BC00053_A1192Contratante_FimDoExpediente ;
      private bool[] BC00053_n1192Contratante_FimDoExpediente ;
      private int[] BC00053_A1727Contratante_ServicoSSPadrao ;
      private bool[] BC00053_n1727Contratante_ServicoSSPadrao ;
      private bool[] BC00053_A1729Contratante_RetornaSS ;
      private bool[] BC00053_n1729Contratante_RetornaSS ;
      private String[] BC00053_A1732Contratante_SSStatusPlanejamento ;
      private bool[] BC00053_n1732Contratante_SSStatusPlanejamento ;
      private bool[] BC00053_A1733Contratante_SSPrestadoraUnica ;
      private bool[] BC00053_n1733Contratante_SSPrestadoraUnica ;
      private bool[] BC00053_A1738Contratante_PropostaRqrSrv ;
      private bool[] BC00053_n1738Contratante_PropostaRqrSrv ;
      private bool[] BC00053_A1739Contratante_PropostaRqrPrz ;
      private bool[] BC00053_n1739Contratante_PropostaRqrPrz ;
      private bool[] BC00053_A1740Contratante_PropostaRqrEsf ;
      private bool[] BC00053_n1740Contratante_PropostaRqrEsf ;
      private bool[] BC00053_A1741Contratante_PropostaRqrCnt ;
      private bool[] BC00053_n1741Contratante_PropostaRqrCnt ;
      private short[] BC00053_A1742Contratante_PropostaNvlCnt ;
      private bool[] BC00053_n1742Contratante_PropostaNvlCnt ;
      private bool[] BC00053_A1757Contratante_OSGeraOS ;
      private bool[] BC00053_n1757Contratante_OSGeraOS ;
      private bool[] BC00053_A1758Contratante_OSGeraTRP ;
      private bool[] BC00053_n1758Contratante_OSGeraTRP ;
      private bool[] BC00053_A1759Contratante_OSGeraTH ;
      private bool[] BC00053_n1759Contratante_OSGeraTH ;
      private bool[] BC00053_A1760Contratante_OSGeraTRD ;
      private bool[] BC00053_n1760Contratante_OSGeraTRD ;
      private bool[] BC00053_A1761Contratante_OSGeraTR ;
      private bool[] BC00053_n1761Contratante_OSGeraTR ;
      private bool[] BC00053_A1803Contratante_OSHmlgComPnd ;
      private bool[] BC00053_n1803Contratante_OSHmlgComPnd ;
      private decimal[] BC00053_A1805Contratante_AtivoCirculante ;
      private bool[] BC00053_n1805Contratante_AtivoCirculante ;
      private decimal[] BC00053_A1806Contratante_PassivoCirculante ;
      private bool[] BC00053_n1806Contratante_PassivoCirculante ;
      private decimal[] BC00053_A1807Contratante_PatrimonioLiquido ;
      private bool[] BC00053_n1807Contratante_PatrimonioLiquido ;
      private decimal[] BC00053_A1808Contratante_ReceitaBruta ;
      private bool[] BC00053_n1808Contratante_ReceitaBruta ;
      private bool[] BC00053_A1822Contratante_UsaOSistema ;
      private bool[] BC00053_n1822Contratante_UsaOSistema ;
      private String[] BC00053_A2034Contratante_TtlRltGerencial ;
      private bool[] BC00053_n2034Contratante_TtlRltGerencial ;
      private short[] BC00053_A2084Contratante_PrzAoRtr ;
      private bool[] BC00053_n2084Contratante_PrzAoRtr ;
      private short[] BC00053_A2083Contratante_PrzActRtr ;
      private bool[] BC00053_n2083Contratante_PrzActRtr ;
      private bool[] BC00053_A2085Contratante_RequerOrigem ;
      private bool[] BC00053_n2085Contratante_RequerOrigem ;
      private bool[] BC00053_A2089Contratante_SelecionaResponsavelOS ;
      private bool[] BC00053_A2090Contratante_ExibePF ;
      private String[] BC00053_A1126Contratante_LogoTipoArq ;
      private bool[] BC00053_n1126Contratante_LogoTipoArq ;
      private String[] BC00053_A1125Contratante_LogoNomeArq ;
      private bool[] BC00053_n1125Contratante_LogoNomeArq ;
      private int[] BC00053_A25Municipio_Codigo ;
      private bool[] BC00053_n25Municipio_Codigo ;
      private int[] BC00053_A335Contratante_PessoaCod ;
      private String[] BC00053_A1124Contratante_LogoArquivo ;
      private bool[] BC00053_n1124Contratante_LogoArquivo ;
      private int[] BC00052_A29Contratante_Codigo ;
      private bool[] BC00052_n29Contratante_Codigo ;
      private String[] BC00052_A550Contratante_EmailSdaKey ;
      private bool[] BC00052_n550Contratante_EmailSdaKey ;
      private String[] BC00052_A549Contratante_EmailSdaPass ;
      private bool[] BC00052_n549Contratante_EmailSdaPass ;
      private String[] BC00052_A10Contratante_NomeFantasia ;
      private String[] BC00052_A11Contratante_IE ;
      private String[] BC00052_A13Contratante_WebSite ;
      private bool[] BC00052_n13Contratante_WebSite ;
      private String[] BC00052_A14Contratante_Email ;
      private bool[] BC00052_n14Contratante_Email ;
      private String[] BC00052_A31Contratante_Telefone ;
      private String[] BC00052_A32Contratante_Ramal ;
      private bool[] BC00052_n32Contratante_Ramal ;
      private String[] BC00052_A33Contratante_Fax ;
      private bool[] BC00052_n33Contratante_Fax ;
      private String[] BC00052_A336Contratante_AgenciaNome ;
      private bool[] BC00052_n336Contratante_AgenciaNome ;
      private String[] BC00052_A337Contratante_AgenciaNro ;
      private bool[] BC00052_n337Contratante_AgenciaNro ;
      private String[] BC00052_A338Contratante_BancoNome ;
      private bool[] BC00052_n338Contratante_BancoNome ;
      private String[] BC00052_A339Contratante_BancoNro ;
      private bool[] BC00052_n339Contratante_BancoNro ;
      private String[] BC00052_A16Contratante_ContaCorrente ;
      private bool[] BC00052_n16Contratante_ContaCorrente ;
      private bool[] BC00052_A30Contratante_Ativo ;
      private String[] BC00052_A547Contratante_EmailSdaHost ;
      private bool[] BC00052_n547Contratante_EmailSdaHost ;
      private String[] BC00052_A548Contratante_EmailSdaUser ;
      private bool[] BC00052_n548Contratante_EmailSdaUser ;
      private bool[] BC00052_A551Contratante_EmailSdaAut ;
      private bool[] BC00052_n551Contratante_EmailSdaAut ;
      private short[] BC00052_A552Contratante_EmailSdaPort ;
      private bool[] BC00052_n552Contratante_EmailSdaPort ;
      private short[] BC00052_A1048Contratante_EmailSdaSec ;
      private bool[] BC00052_n1048Contratante_EmailSdaSec ;
      private bool[] BC00052_A593Contratante_OSAutomatica ;
      private bool[] BC00052_A1652Contratante_SSAutomatica ;
      private bool[] BC00052_n1652Contratante_SSAutomatica ;
      private bool[] BC00052_A594Contratante_ExisteConferencia ;
      private DateTime[] BC00052_A1448Contratante_InicioDoExpediente ;
      private bool[] BC00052_n1448Contratante_InicioDoExpediente ;
      private DateTime[] BC00052_A1192Contratante_FimDoExpediente ;
      private bool[] BC00052_n1192Contratante_FimDoExpediente ;
      private int[] BC00052_A1727Contratante_ServicoSSPadrao ;
      private bool[] BC00052_n1727Contratante_ServicoSSPadrao ;
      private bool[] BC00052_A1729Contratante_RetornaSS ;
      private bool[] BC00052_n1729Contratante_RetornaSS ;
      private String[] BC00052_A1732Contratante_SSStatusPlanejamento ;
      private bool[] BC00052_n1732Contratante_SSStatusPlanejamento ;
      private bool[] BC00052_A1733Contratante_SSPrestadoraUnica ;
      private bool[] BC00052_n1733Contratante_SSPrestadoraUnica ;
      private bool[] BC00052_A1738Contratante_PropostaRqrSrv ;
      private bool[] BC00052_n1738Contratante_PropostaRqrSrv ;
      private bool[] BC00052_A1739Contratante_PropostaRqrPrz ;
      private bool[] BC00052_n1739Contratante_PropostaRqrPrz ;
      private bool[] BC00052_A1740Contratante_PropostaRqrEsf ;
      private bool[] BC00052_n1740Contratante_PropostaRqrEsf ;
      private bool[] BC00052_A1741Contratante_PropostaRqrCnt ;
      private bool[] BC00052_n1741Contratante_PropostaRqrCnt ;
      private short[] BC00052_A1742Contratante_PropostaNvlCnt ;
      private bool[] BC00052_n1742Contratante_PropostaNvlCnt ;
      private bool[] BC00052_A1757Contratante_OSGeraOS ;
      private bool[] BC00052_n1757Contratante_OSGeraOS ;
      private bool[] BC00052_A1758Contratante_OSGeraTRP ;
      private bool[] BC00052_n1758Contratante_OSGeraTRP ;
      private bool[] BC00052_A1759Contratante_OSGeraTH ;
      private bool[] BC00052_n1759Contratante_OSGeraTH ;
      private bool[] BC00052_A1760Contratante_OSGeraTRD ;
      private bool[] BC00052_n1760Contratante_OSGeraTRD ;
      private bool[] BC00052_A1761Contratante_OSGeraTR ;
      private bool[] BC00052_n1761Contratante_OSGeraTR ;
      private bool[] BC00052_A1803Contratante_OSHmlgComPnd ;
      private bool[] BC00052_n1803Contratante_OSHmlgComPnd ;
      private decimal[] BC00052_A1805Contratante_AtivoCirculante ;
      private bool[] BC00052_n1805Contratante_AtivoCirculante ;
      private decimal[] BC00052_A1806Contratante_PassivoCirculante ;
      private bool[] BC00052_n1806Contratante_PassivoCirculante ;
      private decimal[] BC00052_A1807Contratante_PatrimonioLiquido ;
      private bool[] BC00052_n1807Contratante_PatrimonioLiquido ;
      private decimal[] BC00052_A1808Contratante_ReceitaBruta ;
      private bool[] BC00052_n1808Contratante_ReceitaBruta ;
      private bool[] BC00052_A1822Contratante_UsaOSistema ;
      private bool[] BC00052_n1822Contratante_UsaOSistema ;
      private String[] BC00052_A2034Contratante_TtlRltGerencial ;
      private bool[] BC00052_n2034Contratante_TtlRltGerencial ;
      private short[] BC00052_A2084Contratante_PrzAoRtr ;
      private bool[] BC00052_n2084Contratante_PrzAoRtr ;
      private short[] BC00052_A2083Contratante_PrzActRtr ;
      private bool[] BC00052_n2083Contratante_PrzActRtr ;
      private bool[] BC00052_A2085Contratante_RequerOrigem ;
      private bool[] BC00052_n2085Contratante_RequerOrigem ;
      private bool[] BC00052_A2089Contratante_SelecionaResponsavelOS ;
      private bool[] BC00052_A2090Contratante_ExibePF ;
      private String[] BC00052_A1126Contratante_LogoTipoArq ;
      private bool[] BC00052_n1126Contratante_LogoTipoArq ;
      private String[] BC00052_A1125Contratante_LogoNomeArq ;
      private bool[] BC00052_n1125Contratante_LogoNomeArq ;
      private int[] BC00052_A25Municipio_Codigo ;
      private bool[] BC00052_n25Municipio_Codigo ;
      private int[] BC00052_A335Contratante_PessoaCod ;
      private String[] BC00052_A1124Contratante_LogoArquivo ;
      private bool[] BC00052_n1124Contratante_LogoArquivo ;
      private int[] BC00058_A29Contratante_Codigo ;
      private bool[] BC00058_n29Contratante_Codigo ;
      private String[] BC000512_A12Contratante_CNPJ ;
      private bool[] BC000512_n12Contratante_CNPJ ;
      private String[] BC000512_A9Contratante_RazaoSocial ;
      private bool[] BC000512_n9Contratante_RazaoSocial ;
      private String[] BC000513_A26Municipio_Nome ;
      private String[] BC000513_A23Estado_UF ;
      private int[] BC000514_A1380Redmine_Codigo ;
      private int[] BC000515_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC000515_A60ContratanteUsuario_UsuarioCod ;
      private int[] BC000516_A5AreaTrabalho_Codigo ;
      private int[] BC000517_A29Contratante_Codigo ;
      private bool[] BC000517_n29Contratante_Codigo ;
      private String[] BC000517_A550Contratante_EmailSdaKey ;
      private bool[] BC000517_n550Contratante_EmailSdaKey ;
      private String[] BC000517_A549Contratante_EmailSdaPass ;
      private bool[] BC000517_n549Contratante_EmailSdaPass ;
      private String[] BC000517_A12Contratante_CNPJ ;
      private bool[] BC000517_n12Contratante_CNPJ ;
      private String[] BC000517_A9Contratante_RazaoSocial ;
      private bool[] BC000517_n9Contratante_RazaoSocial ;
      private String[] BC000517_A10Contratante_NomeFantasia ;
      private String[] BC000517_A11Contratante_IE ;
      private String[] BC000517_A13Contratante_WebSite ;
      private bool[] BC000517_n13Contratante_WebSite ;
      private String[] BC000517_A14Contratante_Email ;
      private bool[] BC000517_n14Contratante_Email ;
      private String[] BC000517_A31Contratante_Telefone ;
      private String[] BC000517_A32Contratante_Ramal ;
      private bool[] BC000517_n32Contratante_Ramal ;
      private String[] BC000517_A33Contratante_Fax ;
      private bool[] BC000517_n33Contratante_Fax ;
      private String[] BC000517_A336Contratante_AgenciaNome ;
      private bool[] BC000517_n336Contratante_AgenciaNome ;
      private String[] BC000517_A337Contratante_AgenciaNro ;
      private bool[] BC000517_n337Contratante_AgenciaNro ;
      private String[] BC000517_A338Contratante_BancoNome ;
      private bool[] BC000517_n338Contratante_BancoNome ;
      private String[] BC000517_A339Contratante_BancoNro ;
      private bool[] BC000517_n339Contratante_BancoNro ;
      private String[] BC000517_A16Contratante_ContaCorrente ;
      private bool[] BC000517_n16Contratante_ContaCorrente ;
      private String[] BC000517_A26Municipio_Nome ;
      private bool[] BC000517_A30Contratante_Ativo ;
      private String[] BC000517_A547Contratante_EmailSdaHost ;
      private bool[] BC000517_n547Contratante_EmailSdaHost ;
      private String[] BC000517_A548Contratante_EmailSdaUser ;
      private bool[] BC000517_n548Contratante_EmailSdaUser ;
      private bool[] BC000517_A551Contratante_EmailSdaAut ;
      private bool[] BC000517_n551Contratante_EmailSdaAut ;
      private short[] BC000517_A552Contratante_EmailSdaPort ;
      private bool[] BC000517_n552Contratante_EmailSdaPort ;
      private short[] BC000517_A1048Contratante_EmailSdaSec ;
      private bool[] BC000517_n1048Contratante_EmailSdaSec ;
      private bool[] BC000517_A593Contratante_OSAutomatica ;
      private bool[] BC000517_A1652Contratante_SSAutomatica ;
      private bool[] BC000517_n1652Contratante_SSAutomatica ;
      private bool[] BC000517_A594Contratante_ExisteConferencia ;
      private DateTime[] BC000517_A1448Contratante_InicioDoExpediente ;
      private bool[] BC000517_n1448Contratante_InicioDoExpediente ;
      private DateTime[] BC000517_A1192Contratante_FimDoExpediente ;
      private bool[] BC000517_n1192Contratante_FimDoExpediente ;
      private int[] BC000517_A1727Contratante_ServicoSSPadrao ;
      private bool[] BC000517_n1727Contratante_ServicoSSPadrao ;
      private bool[] BC000517_A1729Contratante_RetornaSS ;
      private bool[] BC000517_n1729Contratante_RetornaSS ;
      private String[] BC000517_A1732Contratante_SSStatusPlanejamento ;
      private bool[] BC000517_n1732Contratante_SSStatusPlanejamento ;
      private bool[] BC000517_A1733Contratante_SSPrestadoraUnica ;
      private bool[] BC000517_n1733Contratante_SSPrestadoraUnica ;
      private bool[] BC000517_A1738Contratante_PropostaRqrSrv ;
      private bool[] BC000517_n1738Contratante_PropostaRqrSrv ;
      private bool[] BC000517_A1739Contratante_PropostaRqrPrz ;
      private bool[] BC000517_n1739Contratante_PropostaRqrPrz ;
      private bool[] BC000517_A1740Contratante_PropostaRqrEsf ;
      private bool[] BC000517_n1740Contratante_PropostaRqrEsf ;
      private bool[] BC000517_A1741Contratante_PropostaRqrCnt ;
      private bool[] BC000517_n1741Contratante_PropostaRqrCnt ;
      private short[] BC000517_A1742Contratante_PropostaNvlCnt ;
      private bool[] BC000517_n1742Contratante_PropostaNvlCnt ;
      private bool[] BC000517_A1757Contratante_OSGeraOS ;
      private bool[] BC000517_n1757Contratante_OSGeraOS ;
      private bool[] BC000517_A1758Contratante_OSGeraTRP ;
      private bool[] BC000517_n1758Contratante_OSGeraTRP ;
      private bool[] BC000517_A1759Contratante_OSGeraTH ;
      private bool[] BC000517_n1759Contratante_OSGeraTH ;
      private bool[] BC000517_A1760Contratante_OSGeraTRD ;
      private bool[] BC000517_n1760Contratante_OSGeraTRD ;
      private bool[] BC000517_A1761Contratante_OSGeraTR ;
      private bool[] BC000517_n1761Contratante_OSGeraTR ;
      private bool[] BC000517_A1803Contratante_OSHmlgComPnd ;
      private bool[] BC000517_n1803Contratante_OSHmlgComPnd ;
      private decimal[] BC000517_A1805Contratante_AtivoCirculante ;
      private bool[] BC000517_n1805Contratante_AtivoCirculante ;
      private decimal[] BC000517_A1806Contratante_PassivoCirculante ;
      private bool[] BC000517_n1806Contratante_PassivoCirculante ;
      private decimal[] BC000517_A1807Contratante_PatrimonioLiquido ;
      private bool[] BC000517_n1807Contratante_PatrimonioLiquido ;
      private decimal[] BC000517_A1808Contratante_ReceitaBruta ;
      private bool[] BC000517_n1808Contratante_ReceitaBruta ;
      private bool[] BC000517_A1822Contratante_UsaOSistema ;
      private bool[] BC000517_n1822Contratante_UsaOSistema ;
      private String[] BC000517_A2034Contratante_TtlRltGerencial ;
      private bool[] BC000517_n2034Contratante_TtlRltGerencial ;
      private short[] BC000517_A2084Contratante_PrzAoRtr ;
      private bool[] BC000517_n2084Contratante_PrzAoRtr ;
      private short[] BC000517_A2083Contratante_PrzActRtr ;
      private bool[] BC000517_n2083Contratante_PrzActRtr ;
      private bool[] BC000517_A2085Contratante_RequerOrigem ;
      private bool[] BC000517_n2085Contratante_RequerOrigem ;
      private bool[] BC000517_A2089Contratante_SelecionaResponsavelOS ;
      private bool[] BC000517_A2090Contratante_ExibePF ;
      private String[] BC000517_A1126Contratante_LogoTipoArq ;
      private bool[] BC000517_n1126Contratante_LogoTipoArq ;
      private String[] BC000517_A1125Contratante_LogoNomeArq ;
      private bool[] BC000517_n1125Contratante_LogoNomeArq ;
      private int[] BC000517_A25Municipio_Codigo ;
      private bool[] BC000517_n25Municipio_Codigo ;
      private int[] BC000517_A335Contratante_PessoaCod ;
      private String[] BC000517_A23Estado_UF ;
      private String[] BC000517_A1124Contratante_LogoArquivo ;
      private bool[] BC000517_n1124Contratante_LogoArquivo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GeneXus.Mail.GXMailMessage AV39Email ;
      private GeneXus.Mail.GXMailRecipient AV19MailRecipient ;
      private GeneXus.Mail.GXSMTPSession AV41SMTPSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV34Usuarios ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV35Attachments ;
      private wwpbaseobjects.SdtAuditingObject AV29AuditingObject ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class contratante_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00056 ;
          prmBC00056 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC00056 ;
          cmdBufferBC00056=" SELECT TM1.[Contratante_Codigo], TM1.[Contratante_EmailSdaKey], TM1.[Contratante_EmailSdaPass], T2.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Pessoa_Nome] AS Contratante_RazaoSocial, TM1.[Contratante_NomeFantasia], TM1.[Contratante_IE], TM1.[Contratante_WebSite], TM1.[Contratante_Email], TM1.[Contratante_Telefone], TM1.[Contratante_Ramal], TM1.[Contratante_Fax], TM1.[Contratante_AgenciaNome], TM1.[Contratante_AgenciaNro], TM1.[Contratante_BancoNome], TM1.[Contratante_BancoNro], TM1.[Contratante_ContaCorrente], T3.[Municipio_Nome], TM1.[Contratante_Ativo], TM1.[Contratante_EmailSdaHost], TM1.[Contratante_EmailSdaUser], TM1.[Contratante_EmailSdaAut], TM1.[Contratante_EmailSdaPort], TM1.[Contratante_EmailSdaSec], TM1.[Contratante_OSAutomatica], TM1.[Contratante_SSAutomatica], TM1.[Contratante_ExisteConferencia], TM1.[Contratante_InicioDoExpediente], TM1.[Contratante_FimDoExpediente], TM1.[Contratante_ServicoSSPadrao], TM1.[Contratante_RetornaSS], TM1.[Contratante_SSStatusPlanejamento], TM1.[Contratante_SSPrestadoraUnica], TM1.[Contratante_PropostaRqrSrv], TM1.[Contratante_PropostaRqrPrz], TM1.[Contratante_PropostaRqrEsf], TM1.[Contratante_PropostaRqrCnt], TM1.[Contratante_PropostaNvlCnt], TM1.[Contratante_OSGeraOS], TM1.[Contratante_OSGeraTRP], TM1.[Contratante_OSGeraTH], TM1.[Contratante_OSGeraTRD], TM1.[Contratante_OSGeraTR], TM1.[Contratante_OSHmlgComPnd], TM1.[Contratante_AtivoCirculante], TM1.[Contratante_PassivoCirculante], TM1.[Contratante_PatrimonioLiquido], TM1.[Contratante_ReceitaBruta], TM1.[Contratante_UsaOSistema], TM1.[Contratante_TtlRltGerencial], TM1.[Contratante_PrzAoRtr], TM1.[Contratante_PrzActRtr], TM1.[Contratante_RequerOrigem], TM1.[Contratante_SelecionaResponsavelOS], TM1.[Contratante_ExibePF], TM1.[Contratante_LogoTipoArq], TM1.[Contratante_LogoNomeArq], "
          + " TM1.[Municipio_Codigo], TM1.[Contratante_PessoaCod] AS Contratante_PessoaCod, T3.[Estado_UF], TM1.[Contratante_LogoArquivo] FROM (([Contratante] TM1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = TM1.[Contratante_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = TM1.[Municipio_Codigo]) WHERE TM1.[Contratante_Codigo] = @Contratante_Codigo ORDER BY TM1.[Contratante_Codigo]  OPTION (FAST 100)" ;
          Object[] prmBC00055 ;
          prmBC00055 = new Object[] {
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00054 ;
          prmBC00054 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00057 ;
          prmBC00057 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00053 ;
          prmBC00053 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00052 ;
          prmBC00052 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00058 ;
          prmBC00058 = new Object[] {
          new Object[] {"@Contratante_EmailSdaKey",SqlDbType.Char,32,0} ,
          new Object[] {"@Contratante_EmailSdaPass",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_NomeFantasia",SqlDbType.Char,100,0} ,
          new Object[] {"@Contratante_IE",SqlDbType.Char,15,0} ,
          new Object[] {"@Contratante_WebSite",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_Email",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_Telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@Contratante_Ramal",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_Fax",SqlDbType.Char,20,0} ,
          new Object[] {"@Contratante_AgenciaNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratante_AgenciaNro",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_BancoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratante_BancoNro",SqlDbType.Char,6,0} ,
          new Object[] {"@Contratante_ContaCorrente",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_EmailSdaHost",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Contratante_EmailSdaUser",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Contratante_EmailSdaAut",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_EmailSdaPort",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_EmailSdaSec",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_OSAutomatica",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_SSAutomatica",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_ExisteConferencia",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_LogoArquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Contratante_InicioDoExpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@Contratante_FimDoExpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@Contratante_ServicoSSPadrao",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratante_RetornaSS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_SSStatusPlanejamento",SqlDbType.Char,1,0} ,
          new Object[] {"@Contratante_SSPrestadoraUnica",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrSrv",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrPrz",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrEsf",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrCnt",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaNvlCnt",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_OSGeraOS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTRP",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTH",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTRD",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTR",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSHmlgComPnd",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_AtivoCirculante",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_PassivoCirculante",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_PatrimonioLiquido",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_ReceitaBruta",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_UsaOSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_TtlRltGerencial",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_PrzAoRtr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_PrzActRtr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_RequerOrigem",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_SelecionaResponsavelOS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_ExibePF",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_LogoTipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_LogoNomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC00058 ;
          cmdBufferBC00058=" INSERT INTO [Contratante]([Contratante_EmailSdaKey], [Contratante_EmailSdaPass], [Contratante_NomeFantasia], [Contratante_IE], [Contratante_WebSite], [Contratante_Email], [Contratante_Telefone], [Contratante_Ramal], [Contratante_Fax], [Contratante_AgenciaNome], [Contratante_AgenciaNro], [Contratante_BancoNome], [Contratante_BancoNro], [Contratante_ContaCorrente], [Contratante_Ativo], [Contratante_EmailSdaHost], [Contratante_EmailSdaUser], [Contratante_EmailSdaAut], [Contratante_EmailSdaPort], [Contratante_EmailSdaSec], [Contratante_OSAutomatica], [Contratante_SSAutomatica], [Contratante_ExisteConferencia], [Contratante_LogoArquivo], [Contratante_InicioDoExpediente], [Contratante_FimDoExpediente], [Contratante_ServicoSSPadrao], [Contratante_RetornaSS], [Contratante_SSStatusPlanejamento], [Contratante_SSPrestadoraUnica], [Contratante_PropostaRqrSrv], [Contratante_PropostaRqrPrz], [Contratante_PropostaRqrEsf], [Contratante_PropostaRqrCnt], [Contratante_PropostaNvlCnt], [Contratante_OSGeraOS], [Contratante_OSGeraTRP], [Contratante_OSGeraTH], [Contratante_OSGeraTRD], [Contratante_OSGeraTR], [Contratante_OSHmlgComPnd], [Contratante_AtivoCirculante], [Contratante_PassivoCirculante], [Contratante_PatrimonioLiquido], [Contratante_ReceitaBruta], [Contratante_UsaOSistema], [Contratante_TtlRltGerencial], [Contratante_PrzAoRtr], [Contratante_PrzActRtr], [Contratante_RequerOrigem], [Contratante_SelecionaResponsavelOS], [Contratante_ExibePF], [Contratante_LogoTipoArq], [Contratante_LogoNomeArq], [Municipio_Codigo], [Contratante_PessoaCod]) VALUES(@Contratante_EmailSdaKey, @Contratante_EmailSdaPass, @Contratante_NomeFantasia, @Contratante_IE, @Contratante_WebSite, @Contratante_Email, @Contratante_Telefone, @Contratante_Ramal, @Contratante_Fax, @Contratante_AgenciaNome, @Contratante_AgenciaNro, "
          + " @Contratante_BancoNome, @Contratante_BancoNro, @Contratante_ContaCorrente, @Contratante_Ativo, @Contratante_EmailSdaHost, @Contratante_EmailSdaUser, @Contratante_EmailSdaAut, @Contratante_EmailSdaPort, @Contratante_EmailSdaSec, @Contratante_OSAutomatica, @Contratante_SSAutomatica, @Contratante_ExisteConferencia, @Contratante_LogoArquivo, @Contratante_InicioDoExpediente, @Contratante_FimDoExpediente, @Contratante_ServicoSSPadrao, @Contratante_RetornaSS, @Contratante_SSStatusPlanejamento, @Contratante_SSPrestadoraUnica, @Contratante_PropostaRqrSrv, @Contratante_PropostaRqrPrz, @Contratante_PropostaRqrEsf, @Contratante_PropostaRqrCnt, @Contratante_PropostaNvlCnt, @Contratante_OSGeraOS, @Contratante_OSGeraTRP, @Contratante_OSGeraTH, @Contratante_OSGeraTRD, @Contratante_OSGeraTR, @Contratante_OSHmlgComPnd, @Contratante_AtivoCirculante, @Contratante_PassivoCirculante, @Contratante_PatrimonioLiquido, @Contratante_ReceitaBruta, @Contratante_UsaOSistema, @Contratante_TtlRltGerencial, @Contratante_PrzAoRtr, @Contratante_PrzActRtr, @Contratante_RequerOrigem, @Contratante_SelecionaResponsavelOS, @Contratante_ExibePF, @Contratante_LogoTipoArq, @Contratante_LogoNomeArq, @Municipio_Codigo, @Contratante_PessoaCod); SELECT SCOPE_IDENTITY()" ;
          Object[] prmBC00059 ;
          prmBC00059 = new Object[] {
          new Object[] {"@Contratante_EmailSdaKey",SqlDbType.Char,32,0} ,
          new Object[] {"@Contratante_EmailSdaPass",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_NomeFantasia",SqlDbType.Char,100,0} ,
          new Object[] {"@Contratante_IE",SqlDbType.Char,15,0} ,
          new Object[] {"@Contratante_WebSite",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_Email",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_Telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@Contratante_Ramal",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_Fax",SqlDbType.Char,20,0} ,
          new Object[] {"@Contratante_AgenciaNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratante_AgenciaNro",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_BancoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratante_BancoNro",SqlDbType.Char,6,0} ,
          new Object[] {"@Contratante_ContaCorrente",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_EmailSdaHost",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Contratante_EmailSdaUser",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Contratante_EmailSdaAut",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_EmailSdaPort",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_EmailSdaSec",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_OSAutomatica",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_SSAutomatica",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_ExisteConferencia",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_InicioDoExpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@Contratante_FimDoExpediente",SqlDbType.DateTime,0,5} ,
          new Object[] {"@Contratante_ServicoSSPadrao",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratante_RetornaSS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_SSStatusPlanejamento",SqlDbType.Char,1,0} ,
          new Object[] {"@Contratante_SSPrestadoraUnica",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrSrv",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrPrz",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrEsf",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaRqrCnt",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_PropostaNvlCnt",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_OSGeraOS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTRP",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTH",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTRD",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSGeraTR",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_OSHmlgComPnd",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_AtivoCirculante",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_PassivoCirculante",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_PatrimonioLiquido",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_ReceitaBruta",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratante_UsaOSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_TtlRltGerencial",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contratante_PrzAoRtr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_PrzActRtr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratante_RequerOrigem",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_SelecionaResponsavelOS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_ExibePF",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratante_LogoTipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratante_LogoNomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC00059 ;
          cmdBufferBC00059=" UPDATE [Contratante] SET [Contratante_EmailSdaKey]=@Contratante_EmailSdaKey, [Contratante_EmailSdaPass]=@Contratante_EmailSdaPass, [Contratante_NomeFantasia]=@Contratante_NomeFantasia, [Contratante_IE]=@Contratante_IE, [Contratante_WebSite]=@Contratante_WebSite, [Contratante_Email]=@Contratante_Email, [Contratante_Telefone]=@Contratante_Telefone, [Contratante_Ramal]=@Contratante_Ramal, [Contratante_Fax]=@Contratante_Fax, [Contratante_AgenciaNome]=@Contratante_AgenciaNome, [Contratante_AgenciaNro]=@Contratante_AgenciaNro, [Contratante_BancoNome]=@Contratante_BancoNome, [Contratante_BancoNro]=@Contratante_BancoNro, [Contratante_ContaCorrente]=@Contratante_ContaCorrente, [Contratante_Ativo]=@Contratante_Ativo, [Contratante_EmailSdaHost]=@Contratante_EmailSdaHost, [Contratante_EmailSdaUser]=@Contratante_EmailSdaUser, [Contratante_EmailSdaAut]=@Contratante_EmailSdaAut, [Contratante_EmailSdaPort]=@Contratante_EmailSdaPort, [Contratante_EmailSdaSec]=@Contratante_EmailSdaSec, [Contratante_OSAutomatica]=@Contratante_OSAutomatica, [Contratante_SSAutomatica]=@Contratante_SSAutomatica, [Contratante_ExisteConferencia]=@Contratante_ExisteConferencia, [Contratante_InicioDoExpediente]=@Contratante_InicioDoExpediente, [Contratante_FimDoExpediente]=@Contratante_FimDoExpediente, [Contratante_ServicoSSPadrao]=@Contratante_ServicoSSPadrao, [Contratante_RetornaSS]=@Contratante_RetornaSS, [Contratante_SSStatusPlanejamento]=@Contratante_SSStatusPlanejamento, [Contratante_SSPrestadoraUnica]=@Contratante_SSPrestadoraUnica, [Contratante_PropostaRqrSrv]=@Contratante_PropostaRqrSrv, [Contratante_PropostaRqrPrz]=@Contratante_PropostaRqrPrz, [Contratante_PropostaRqrEsf]=@Contratante_PropostaRqrEsf, [Contratante_PropostaRqrCnt]=@Contratante_PropostaRqrCnt, [Contratante_PropostaNvlCnt]=@Contratante_PropostaNvlCnt, "
          + " [Contratante_OSGeraOS]=@Contratante_OSGeraOS, [Contratante_OSGeraTRP]=@Contratante_OSGeraTRP, [Contratante_OSGeraTH]=@Contratante_OSGeraTH, [Contratante_OSGeraTRD]=@Contratante_OSGeraTRD, [Contratante_OSGeraTR]=@Contratante_OSGeraTR, [Contratante_OSHmlgComPnd]=@Contratante_OSHmlgComPnd, [Contratante_AtivoCirculante]=@Contratante_AtivoCirculante, [Contratante_PassivoCirculante]=@Contratante_PassivoCirculante, [Contratante_PatrimonioLiquido]=@Contratante_PatrimonioLiquido, [Contratante_ReceitaBruta]=@Contratante_ReceitaBruta, [Contratante_UsaOSistema]=@Contratante_UsaOSistema, [Contratante_TtlRltGerencial]=@Contratante_TtlRltGerencial, [Contratante_PrzAoRtr]=@Contratante_PrzAoRtr, [Contratante_PrzActRtr]=@Contratante_PrzActRtr, [Contratante_RequerOrigem]=@Contratante_RequerOrigem, [Contratante_SelecionaResponsavelOS]=@Contratante_SelecionaResponsavelOS, [Contratante_ExibePF]=@Contratante_ExibePF, [Contratante_LogoTipoArq]=@Contratante_LogoTipoArq, [Contratante_LogoNomeArq]=@Contratante_LogoNomeArq, [Municipio_Codigo]=@Municipio_Codigo, [Contratante_PessoaCod]=@Contratante_PessoaCod  WHERE [Contratante_Codigo] = @Contratante_Codigo" ;
          Object[] prmBC000510 ;
          prmBC000510 = new Object[] {
          new Object[] {"@Contratante_LogoArquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000511 ;
          prmBC000511 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000512 ;
          prmBC000512 = new Object[] {
          new Object[] {"@Contratante_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000513 ;
          prmBC000513 = new Object[] {
          new Object[] {"@Municipio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000514 ;
          prmBC000514 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000515 ;
          prmBC000515 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000516 ;
          prmBC000516 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000517 ;
          prmBC000517 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC000517 ;
          cmdBufferBC000517=" SELECT TM1.[Contratante_Codigo], TM1.[Contratante_EmailSdaKey], TM1.[Contratante_EmailSdaPass], T2.[Pessoa_Docto] AS Contratante_CNPJ, T2.[Pessoa_Nome] AS Contratante_RazaoSocial, TM1.[Contratante_NomeFantasia], TM1.[Contratante_IE], TM1.[Contratante_WebSite], TM1.[Contratante_Email], TM1.[Contratante_Telefone], TM1.[Contratante_Ramal], TM1.[Contratante_Fax], TM1.[Contratante_AgenciaNome], TM1.[Contratante_AgenciaNro], TM1.[Contratante_BancoNome], TM1.[Contratante_BancoNro], TM1.[Contratante_ContaCorrente], T3.[Municipio_Nome], TM1.[Contratante_Ativo], TM1.[Contratante_EmailSdaHost], TM1.[Contratante_EmailSdaUser], TM1.[Contratante_EmailSdaAut], TM1.[Contratante_EmailSdaPort], TM1.[Contratante_EmailSdaSec], TM1.[Contratante_OSAutomatica], TM1.[Contratante_SSAutomatica], TM1.[Contratante_ExisteConferencia], TM1.[Contratante_InicioDoExpediente], TM1.[Contratante_FimDoExpediente], TM1.[Contratante_ServicoSSPadrao], TM1.[Contratante_RetornaSS], TM1.[Contratante_SSStatusPlanejamento], TM1.[Contratante_SSPrestadoraUnica], TM1.[Contratante_PropostaRqrSrv], TM1.[Contratante_PropostaRqrPrz], TM1.[Contratante_PropostaRqrEsf], TM1.[Contratante_PropostaRqrCnt], TM1.[Contratante_PropostaNvlCnt], TM1.[Contratante_OSGeraOS], TM1.[Contratante_OSGeraTRP], TM1.[Contratante_OSGeraTH], TM1.[Contratante_OSGeraTRD], TM1.[Contratante_OSGeraTR], TM1.[Contratante_OSHmlgComPnd], TM1.[Contratante_AtivoCirculante], TM1.[Contratante_PassivoCirculante], TM1.[Contratante_PatrimonioLiquido], TM1.[Contratante_ReceitaBruta], TM1.[Contratante_UsaOSistema], TM1.[Contratante_TtlRltGerencial], TM1.[Contratante_PrzAoRtr], TM1.[Contratante_PrzActRtr], TM1.[Contratante_RequerOrigem], TM1.[Contratante_SelecionaResponsavelOS], TM1.[Contratante_ExibePF], TM1.[Contratante_LogoTipoArq], TM1.[Contratante_LogoNomeArq], "
          + " TM1.[Municipio_Codigo], TM1.[Contratante_PessoaCod] AS Contratante_PessoaCod, T3.[Estado_UF], TM1.[Contratante_LogoArquivo] FROM (([Contratante] TM1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = TM1.[Contratante_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = TM1.[Municipio_Codigo]) WHERE TM1.[Contratante_Codigo] = @Contratante_Codigo ORDER BY TM1.[Contratante_Codigo]  OPTION (FAST 100)" ;
          def= new CursorDef[] {
              new CursorDef("BC00052", "SELECT [Contratante_Codigo], [Contratante_EmailSdaKey], [Contratante_EmailSdaPass], [Contratante_NomeFantasia], [Contratante_IE], [Contratante_WebSite], [Contratante_Email], [Contratante_Telefone], [Contratante_Ramal], [Contratante_Fax], [Contratante_AgenciaNome], [Contratante_AgenciaNro], [Contratante_BancoNome], [Contratante_BancoNro], [Contratante_ContaCorrente], [Contratante_Ativo], [Contratante_EmailSdaHost], [Contratante_EmailSdaUser], [Contratante_EmailSdaAut], [Contratante_EmailSdaPort], [Contratante_EmailSdaSec], [Contratante_OSAutomatica], [Contratante_SSAutomatica], [Contratante_ExisteConferencia], [Contratante_InicioDoExpediente], [Contratante_FimDoExpediente], [Contratante_ServicoSSPadrao], [Contratante_RetornaSS], [Contratante_SSStatusPlanejamento], [Contratante_SSPrestadoraUnica], [Contratante_PropostaRqrSrv], [Contratante_PropostaRqrPrz], [Contratante_PropostaRqrEsf], [Contratante_PropostaRqrCnt], [Contratante_PropostaNvlCnt], [Contratante_OSGeraOS], [Contratante_OSGeraTRP], [Contratante_OSGeraTH], [Contratante_OSGeraTRD], [Contratante_OSGeraTR], [Contratante_OSHmlgComPnd], [Contratante_AtivoCirculante], [Contratante_PassivoCirculante], [Contratante_PatrimonioLiquido], [Contratante_ReceitaBruta], [Contratante_UsaOSistema], [Contratante_TtlRltGerencial], [Contratante_PrzAoRtr], [Contratante_PrzActRtr], [Contratante_RequerOrigem], [Contratante_SelecionaResponsavelOS], [Contratante_ExibePF], [Contratante_LogoTipoArq], [Contratante_LogoNomeArq], [Municipio_Codigo], [Contratante_PessoaCod] AS Contratante_PessoaCod, [Contratante_LogoArquivo] FROM [Contratante] WITH (UPDLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00052,1,0,true,false )
             ,new CursorDef("BC00053", "SELECT [Contratante_Codigo], [Contratante_EmailSdaKey], [Contratante_EmailSdaPass], [Contratante_NomeFantasia], [Contratante_IE], [Contratante_WebSite], [Contratante_Email], [Contratante_Telefone], [Contratante_Ramal], [Contratante_Fax], [Contratante_AgenciaNome], [Contratante_AgenciaNro], [Contratante_BancoNome], [Contratante_BancoNro], [Contratante_ContaCorrente], [Contratante_Ativo], [Contratante_EmailSdaHost], [Contratante_EmailSdaUser], [Contratante_EmailSdaAut], [Contratante_EmailSdaPort], [Contratante_EmailSdaSec], [Contratante_OSAutomatica], [Contratante_SSAutomatica], [Contratante_ExisteConferencia], [Contratante_InicioDoExpediente], [Contratante_FimDoExpediente], [Contratante_ServicoSSPadrao], [Contratante_RetornaSS], [Contratante_SSStatusPlanejamento], [Contratante_SSPrestadoraUnica], [Contratante_PropostaRqrSrv], [Contratante_PropostaRqrPrz], [Contratante_PropostaRqrEsf], [Contratante_PropostaRqrCnt], [Contratante_PropostaNvlCnt], [Contratante_OSGeraOS], [Contratante_OSGeraTRP], [Contratante_OSGeraTH], [Contratante_OSGeraTRD], [Contratante_OSGeraTR], [Contratante_OSHmlgComPnd], [Contratante_AtivoCirculante], [Contratante_PassivoCirculante], [Contratante_PatrimonioLiquido], [Contratante_ReceitaBruta], [Contratante_UsaOSistema], [Contratante_TtlRltGerencial], [Contratante_PrzAoRtr], [Contratante_PrzActRtr], [Contratante_RequerOrigem], [Contratante_SelecionaResponsavelOS], [Contratante_ExibePF], [Contratante_LogoTipoArq], [Contratante_LogoNomeArq], [Municipio_Codigo], [Contratante_PessoaCod] AS Contratante_PessoaCod, [Contratante_LogoArquivo] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00053,1,0,true,false )
             ,new CursorDef("BC00054", "SELECT [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00054,1,0,true,false )
             ,new CursorDef("BC00055", "SELECT [Pessoa_Docto] AS Contratante_CNPJ, [Pessoa_Nome] AS Contratante_RazaoSocial FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratante_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00055,1,0,true,false )
             ,new CursorDef("BC00056", cmdBufferBC00056,true, GxErrorMask.GX_NOMASK, false, this,prmBC00056,100,0,true,false )
             ,new CursorDef("BC00057", "SELECT [Contratante_Codigo] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00057,1,0,true,false )
             ,new CursorDef("BC00058", cmdBufferBC00058, GxErrorMask.GX_NOMASK,prmBC00058)
             ,new CursorDef("BC00059", cmdBufferBC00059, GxErrorMask.GX_NOMASK,prmBC00059)
             ,new CursorDef("BC000510", "UPDATE [Contratante] SET [Contratante_LogoArquivo]=@Contratante_LogoArquivo  WHERE [Contratante_Codigo] = @Contratante_Codigo", GxErrorMask.GX_NOMASK,prmBC000510)
             ,new CursorDef("BC000511", "DELETE FROM [Contratante]  WHERE [Contratante_Codigo] = @Contratante_Codigo", GxErrorMask.GX_NOMASK,prmBC000511)
             ,new CursorDef("BC000512", "SELECT [Pessoa_Docto] AS Contratante_CNPJ, [Pessoa_Nome] AS Contratante_RazaoSocial FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratante_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000512,1,0,true,false )
             ,new CursorDef("BC000513", "SELECT [Municipio_Nome], [Estado_UF] FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Municipio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000513,1,0,true,false )
             ,new CursorDef("BC000514", "SELECT TOP 1 [Redmine_Codigo] FROM [Redmine] WITH (NOLOCK) WHERE [Redmine_ContratanteCod] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000514,1,0,true,true )
             ,new CursorDef("BC000515", "SELECT TOP 1 [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod] FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000515,1,0,true,true )
             ,new CursorDef("BC000516", "SELECT TOP 1 [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000516,1,0,true,true )
             ,new CursorDef("BC000517", cmdBufferBC000517,true, GxErrorMask.GX_NOMASK, false, this,prmBC000517,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 32) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 20) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 20) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((String[]) buf[22])[0] = rslt.getString(14, 6) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((String[]) buf[24])[0] = rslt.getString(15, 10) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((bool[]) buf[26])[0] = rslt.getBool(16) ;
                ((String[]) buf[27])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((bool[]) buf[31])[0] = rslt.getBool(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((short[]) buf[33])[0] = rslt.getShort(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((short[]) buf[35])[0] = rslt.getShort(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((bool[]) buf[37])[0] = rslt.getBool(22) ;
                ((bool[]) buf[38])[0] = rslt.getBool(23) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((bool[]) buf[40])[0] = rslt.getBool(24) ;
                ((DateTime[]) buf[41])[0] = rslt.getGXDateTime(25) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(25);
                ((DateTime[]) buf[43])[0] = rslt.getGXDateTime(26) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(26);
                ((int[]) buf[45])[0] = rslt.getInt(27) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(27);
                ((bool[]) buf[47])[0] = rslt.getBool(28) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(28);
                ((String[]) buf[49])[0] = rslt.getString(29, 1) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(29);
                ((bool[]) buf[51])[0] = rslt.getBool(30) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(30);
                ((bool[]) buf[53])[0] = rslt.getBool(31) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(31);
                ((bool[]) buf[55])[0] = rslt.getBool(32) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(32);
                ((bool[]) buf[57])[0] = rslt.getBool(33) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(33);
                ((bool[]) buf[59])[0] = rslt.getBool(34) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(34);
                ((short[]) buf[61])[0] = rslt.getShort(35) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(35);
                ((bool[]) buf[63])[0] = rslt.getBool(36) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(36);
                ((bool[]) buf[65])[0] = rslt.getBool(37) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(37);
                ((bool[]) buf[67])[0] = rslt.getBool(38) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(38);
                ((bool[]) buf[69])[0] = rslt.getBool(39) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(39);
                ((bool[]) buf[71])[0] = rslt.getBool(40) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(40);
                ((bool[]) buf[73])[0] = rslt.getBool(41) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(41);
                ((decimal[]) buf[75])[0] = rslt.getDecimal(42) ;
                ((bool[]) buf[76])[0] = rslt.wasNull(42);
                ((decimal[]) buf[77])[0] = rslt.getDecimal(43) ;
                ((bool[]) buf[78])[0] = rslt.wasNull(43);
                ((decimal[]) buf[79])[0] = rslt.getDecimal(44) ;
                ((bool[]) buf[80])[0] = rslt.wasNull(44);
                ((decimal[]) buf[81])[0] = rslt.getDecimal(45) ;
                ((bool[]) buf[82])[0] = rslt.wasNull(45);
                ((bool[]) buf[83])[0] = rslt.getBool(46) ;
                ((bool[]) buf[84])[0] = rslt.wasNull(46);
                ((String[]) buf[85])[0] = rslt.getVarchar(47) ;
                ((bool[]) buf[86])[0] = rslt.wasNull(47);
                ((short[]) buf[87])[0] = rslt.getShort(48) ;
                ((bool[]) buf[88])[0] = rslt.wasNull(48);
                ((short[]) buf[89])[0] = rslt.getShort(49) ;
                ((bool[]) buf[90])[0] = rslt.wasNull(49);
                ((bool[]) buf[91])[0] = rslt.getBool(50) ;
                ((bool[]) buf[92])[0] = rslt.wasNull(50);
                ((bool[]) buf[93])[0] = rslt.getBool(51) ;
                ((bool[]) buf[94])[0] = rslt.getBool(52) ;
                ((String[]) buf[95])[0] = rslt.getString(53, 10) ;
                ((bool[]) buf[96])[0] = rslt.wasNull(53);
                ((String[]) buf[97])[0] = rslt.getString(54, 50) ;
                ((bool[]) buf[98])[0] = rslt.wasNull(54);
                ((int[]) buf[99])[0] = rslt.getInt(55) ;
                ((bool[]) buf[100])[0] = rslt.wasNull(55);
                ((int[]) buf[101])[0] = rslt.getInt(56) ;
                ((String[]) buf[102])[0] = rslt.getBLOBFile(57, rslt.getString(53, 10), rslt.getString(54, 50)) ;
                ((bool[]) buf[103])[0] = rslt.wasNull(57);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 32) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 20) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 20) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((String[]) buf[22])[0] = rslt.getString(14, 6) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((String[]) buf[24])[0] = rslt.getString(15, 10) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((bool[]) buf[26])[0] = rslt.getBool(16) ;
                ((String[]) buf[27])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((bool[]) buf[31])[0] = rslt.getBool(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((short[]) buf[33])[0] = rslt.getShort(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((short[]) buf[35])[0] = rslt.getShort(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((bool[]) buf[37])[0] = rslt.getBool(22) ;
                ((bool[]) buf[38])[0] = rslt.getBool(23) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((bool[]) buf[40])[0] = rslt.getBool(24) ;
                ((DateTime[]) buf[41])[0] = rslt.getGXDateTime(25) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(25);
                ((DateTime[]) buf[43])[0] = rslt.getGXDateTime(26) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(26);
                ((int[]) buf[45])[0] = rslt.getInt(27) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(27);
                ((bool[]) buf[47])[0] = rslt.getBool(28) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(28);
                ((String[]) buf[49])[0] = rslt.getString(29, 1) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(29);
                ((bool[]) buf[51])[0] = rslt.getBool(30) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(30);
                ((bool[]) buf[53])[0] = rslt.getBool(31) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(31);
                ((bool[]) buf[55])[0] = rslt.getBool(32) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(32);
                ((bool[]) buf[57])[0] = rslt.getBool(33) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(33);
                ((bool[]) buf[59])[0] = rslt.getBool(34) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(34);
                ((short[]) buf[61])[0] = rslt.getShort(35) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(35);
                ((bool[]) buf[63])[0] = rslt.getBool(36) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(36);
                ((bool[]) buf[65])[0] = rslt.getBool(37) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(37);
                ((bool[]) buf[67])[0] = rslt.getBool(38) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(38);
                ((bool[]) buf[69])[0] = rslt.getBool(39) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(39);
                ((bool[]) buf[71])[0] = rslt.getBool(40) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(40);
                ((bool[]) buf[73])[0] = rslt.getBool(41) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(41);
                ((decimal[]) buf[75])[0] = rslt.getDecimal(42) ;
                ((bool[]) buf[76])[0] = rslt.wasNull(42);
                ((decimal[]) buf[77])[0] = rslt.getDecimal(43) ;
                ((bool[]) buf[78])[0] = rslt.wasNull(43);
                ((decimal[]) buf[79])[0] = rslt.getDecimal(44) ;
                ((bool[]) buf[80])[0] = rslt.wasNull(44);
                ((decimal[]) buf[81])[0] = rslt.getDecimal(45) ;
                ((bool[]) buf[82])[0] = rslt.wasNull(45);
                ((bool[]) buf[83])[0] = rslt.getBool(46) ;
                ((bool[]) buf[84])[0] = rslt.wasNull(46);
                ((String[]) buf[85])[0] = rslt.getVarchar(47) ;
                ((bool[]) buf[86])[0] = rslt.wasNull(47);
                ((short[]) buf[87])[0] = rslt.getShort(48) ;
                ((bool[]) buf[88])[0] = rslt.wasNull(48);
                ((short[]) buf[89])[0] = rslt.getShort(49) ;
                ((bool[]) buf[90])[0] = rslt.wasNull(49);
                ((bool[]) buf[91])[0] = rslt.getBool(50) ;
                ((bool[]) buf[92])[0] = rslt.wasNull(50);
                ((bool[]) buf[93])[0] = rslt.getBool(51) ;
                ((bool[]) buf[94])[0] = rslt.getBool(52) ;
                ((String[]) buf[95])[0] = rslt.getString(53, 10) ;
                ((bool[]) buf[96])[0] = rslt.wasNull(53);
                ((String[]) buf[97])[0] = rslt.getString(54, 50) ;
                ((bool[]) buf[98])[0] = rslt.wasNull(54);
                ((int[]) buf[99])[0] = rslt.getInt(55) ;
                ((bool[]) buf[100])[0] = rslt.wasNull(55);
                ((int[]) buf[101])[0] = rslt.getInt(56) ;
                ((String[]) buf[102])[0] = rslt.getBLOBFile(57, rslt.getString(53, 10), rslt.getString(54, 50)) ;
                ((bool[]) buf[103])[0] = rslt.wasNull(57);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 32) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 100) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 15) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 20) ;
                ((String[]) buf[16])[0] = rslt.getString(11, 10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 20) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((String[]) buf[22])[0] = rslt.getString(14, 10) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((String[]) buf[24])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((String[]) buf[26])[0] = rslt.getString(16, 6) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((String[]) buf[28])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((String[]) buf[30])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[31])[0] = rslt.getBool(19) ;
                ((String[]) buf[32])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((String[]) buf[34])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((bool[]) buf[36])[0] = rslt.getBool(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((short[]) buf[38])[0] = rslt.getShort(23) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((short[]) buf[40])[0] = rslt.getShort(24) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(24);
                ((bool[]) buf[42])[0] = rslt.getBool(25) ;
                ((bool[]) buf[43])[0] = rslt.getBool(26) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(26);
                ((bool[]) buf[45])[0] = rslt.getBool(27) ;
                ((DateTime[]) buf[46])[0] = rslt.getGXDateTime(28) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(28);
                ((DateTime[]) buf[48])[0] = rslt.getGXDateTime(29) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(29);
                ((int[]) buf[50])[0] = rslt.getInt(30) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(30);
                ((bool[]) buf[52])[0] = rslt.getBool(31) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(31);
                ((String[]) buf[54])[0] = rslt.getString(32, 1) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(32);
                ((bool[]) buf[56])[0] = rslt.getBool(33) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(33);
                ((bool[]) buf[58])[0] = rslt.getBool(34) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(34);
                ((bool[]) buf[60])[0] = rslt.getBool(35) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(35);
                ((bool[]) buf[62])[0] = rslt.getBool(36) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(36);
                ((bool[]) buf[64])[0] = rslt.getBool(37) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(37);
                ((short[]) buf[66])[0] = rslt.getShort(38) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(38);
                ((bool[]) buf[68])[0] = rslt.getBool(39) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(39);
                ((bool[]) buf[70])[0] = rslt.getBool(40) ;
                ((bool[]) buf[71])[0] = rslt.wasNull(40);
                ((bool[]) buf[72])[0] = rslt.getBool(41) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(41);
                ((bool[]) buf[74])[0] = rslt.getBool(42) ;
                ((bool[]) buf[75])[0] = rslt.wasNull(42);
                ((bool[]) buf[76])[0] = rslt.getBool(43) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(43);
                ((bool[]) buf[78])[0] = rslt.getBool(44) ;
                ((bool[]) buf[79])[0] = rslt.wasNull(44);
                ((decimal[]) buf[80])[0] = rslt.getDecimal(45) ;
                ((bool[]) buf[81])[0] = rslt.wasNull(45);
                ((decimal[]) buf[82])[0] = rslt.getDecimal(46) ;
                ((bool[]) buf[83])[0] = rslt.wasNull(46);
                ((decimal[]) buf[84])[0] = rslt.getDecimal(47) ;
                ((bool[]) buf[85])[0] = rslt.wasNull(47);
                ((decimal[]) buf[86])[0] = rslt.getDecimal(48) ;
                ((bool[]) buf[87])[0] = rslt.wasNull(48);
                ((bool[]) buf[88])[0] = rslt.getBool(49) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(49);
                ((String[]) buf[90])[0] = rslt.getVarchar(50) ;
                ((bool[]) buf[91])[0] = rslt.wasNull(50);
                ((short[]) buf[92])[0] = rslt.getShort(51) ;
                ((bool[]) buf[93])[0] = rslt.wasNull(51);
                ((short[]) buf[94])[0] = rslt.getShort(52) ;
                ((bool[]) buf[95])[0] = rslt.wasNull(52);
                ((bool[]) buf[96])[0] = rslt.getBool(53) ;
                ((bool[]) buf[97])[0] = rslt.wasNull(53);
                ((bool[]) buf[98])[0] = rslt.getBool(54) ;
                ((bool[]) buf[99])[0] = rslt.getBool(55) ;
                ((String[]) buf[100])[0] = rslt.getString(56, 10) ;
                ((bool[]) buf[101])[0] = rslt.wasNull(56);
                ((String[]) buf[102])[0] = rslt.getString(57, 50) ;
                ((bool[]) buf[103])[0] = rslt.wasNull(57);
                ((int[]) buf[104])[0] = rslt.getInt(58) ;
                ((bool[]) buf[105])[0] = rslt.wasNull(58);
                ((int[]) buf[106])[0] = rslt.getInt(59) ;
                ((String[]) buf[107])[0] = rslt.getString(60, 2) ;
                ((String[]) buf[108])[0] = rslt.getBLOBFile(61, rslt.getString(56, 10), rslt.getString(57, 50)) ;
                ((bool[]) buf[109])[0] = rslt.wasNull(61);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 32) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 100) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 15) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 20) ;
                ((String[]) buf[16])[0] = rslt.getString(11, 10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 20) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((String[]) buf[22])[0] = rslt.getString(14, 10) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((String[]) buf[24])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((String[]) buf[26])[0] = rslt.getString(16, 6) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((String[]) buf[28])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((String[]) buf[30])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[31])[0] = rslt.getBool(19) ;
                ((String[]) buf[32])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((String[]) buf[34])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((bool[]) buf[36])[0] = rslt.getBool(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((short[]) buf[38])[0] = rslt.getShort(23) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((short[]) buf[40])[0] = rslt.getShort(24) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(24);
                ((bool[]) buf[42])[0] = rslt.getBool(25) ;
                ((bool[]) buf[43])[0] = rslt.getBool(26) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(26);
                ((bool[]) buf[45])[0] = rslt.getBool(27) ;
                ((DateTime[]) buf[46])[0] = rslt.getGXDateTime(28) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(28);
                ((DateTime[]) buf[48])[0] = rslt.getGXDateTime(29) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(29);
                ((int[]) buf[50])[0] = rslt.getInt(30) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(30);
                ((bool[]) buf[52])[0] = rslt.getBool(31) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(31);
                ((String[]) buf[54])[0] = rslt.getString(32, 1) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(32);
                ((bool[]) buf[56])[0] = rslt.getBool(33) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(33);
                ((bool[]) buf[58])[0] = rslt.getBool(34) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(34);
                ((bool[]) buf[60])[0] = rslt.getBool(35) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(35);
                ((bool[]) buf[62])[0] = rslt.getBool(36) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(36);
                ((bool[]) buf[64])[0] = rslt.getBool(37) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(37);
                ((short[]) buf[66])[0] = rslt.getShort(38) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(38);
                ((bool[]) buf[68])[0] = rslt.getBool(39) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(39);
                ((bool[]) buf[70])[0] = rslt.getBool(40) ;
                ((bool[]) buf[71])[0] = rslt.wasNull(40);
                ((bool[]) buf[72])[0] = rslt.getBool(41) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(41);
                ((bool[]) buf[74])[0] = rslt.getBool(42) ;
                ((bool[]) buf[75])[0] = rslt.wasNull(42);
                ((bool[]) buf[76])[0] = rslt.getBool(43) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(43);
                ((bool[]) buf[78])[0] = rslt.getBool(44) ;
                ((bool[]) buf[79])[0] = rslt.wasNull(44);
                ((decimal[]) buf[80])[0] = rslt.getDecimal(45) ;
                ((bool[]) buf[81])[0] = rslt.wasNull(45);
                ((decimal[]) buf[82])[0] = rslt.getDecimal(46) ;
                ((bool[]) buf[83])[0] = rslt.wasNull(46);
                ((decimal[]) buf[84])[0] = rslt.getDecimal(47) ;
                ((bool[]) buf[85])[0] = rslt.wasNull(47);
                ((decimal[]) buf[86])[0] = rslt.getDecimal(48) ;
                ((bool[]) buf[87])[0] = rslt.wasNull(48);
                ((bool[]) buf[88])[0] = rslt.getBool(49) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(49);
                ((String[]) buf[90])[0] = rslt.getVarchar(50) ;
                ((bool[]) buf[91])[0] = rslt.wasNull(50);
                ((short[]) buf[92])[0] = rslt.getShort(51) ;
                ((bool[]) buf[93])[0] = rslt.wasNull(51);
                ((short[]) buf[94])[0] = rslt.getShort(52) ;
                ((bool[]) buf[95])[0] = rslt.wasNull(52);
                ((bool[]) buf[96])[0] = rslt.getBool(53) ;
                ((bool[]) buf[97])[0] = rslt.wasNull(53);
                ((bool[]) buf[98])[0] = rslt.getBool(54) ;
                ((bool[]) buf[99])[0] = rslt.getBool(55) ;
                ((String[]) buf[100])[0] = rslt.getString(56, 10) ;
                ((bool[]) buf[101])[0] = rslt.wasNull(56);
                ((String[]) buf[102])[0] = rslt.getString(57, 50) ;
                ((bool[]) buf[103])[0] = rslt.wasNull(57);
                ((int[]) buf[104])[0] = rslt.getInt(58) ;
                ((bool[]) buf[105])[0] = rslt.wasNull(58);
                ((int[]) buf[106])[0] = rslt.getInt(59) ;
                ((String[]) buf[107])[0] = rslt.getString(60, 2) ;
                ((String[]) buf[108])[0] = rslt.getBLOBFile(61, rslt.getString(56, 10), rslt.getString(57, 50)) ;
                ((bool[]) buf[109])[0] = rslt.wasNull(61);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (String)parms[4]);
                stmt.SetParameter(4, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                stmt.SetParameter(7, (String)parms[10]);
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[24]);
                }
                stmt.SetParameter(15, (bool)parms[25]);
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(18, (bool)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 19 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(19, (short)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 20 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(20, (short)parms[35]);
                }
                stmt.SetParameter(21, (bool)parms[36]);
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 22 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(22, (bool)parms[38]);
                }
                stmt.SetParameter(23, (bool)parms[39]);
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 24 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(24, (String)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 25 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(25, (DateTime)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 26 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(26, (DateTime)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 28 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(28, (bool)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 29 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(29, (String)parms[51]);
                }
                if ( (bool)parms[52] )
                {
                   stmt.setNull( 30 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(30, (bool)parms[53]);
                }
                if ( (bool)parms[54] )
                {
                   stmt.setNull( 31 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(31, (bool)parms[55]);
                }
                if ( (bool)parms[56] )
                {
                   stmt.setNull( 32 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(32, (bool)parms[57]);
                }
                if ( (bool)parms[58] )
                {
                   stmt.setNull( 33 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(33, (bool)parms[59]);
                }
                if ( (bool)parms[60] )
                {
                   stmt.setNull( 34 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(34, (bool)parms[61]);
                }
                if ( (bool)parms[62] )
                {
                   stmt.setNull( 35 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(35, (short)parms[63]);
                }
                if ( (bool)parms[64] )
                {
                   stmt.setNull( 36 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(36, (bool)parms[65]);
                }
                if ( (bool)parms[66] )
                {
                   stmt.setNull( 37 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(37, (bool)parms[67]);
                }
                if ( (bool)parms[68] )
                {
                   stmt.setNull( 38 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(38, (bool)parms[69]);
                }
                if ( (bool)parms[70] )
                {
                   stmt.setNull( 39 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(39, (bool)parms[71]);
                }
                if ( (bool)parms[72] )
                {
                   stmt.setNull( 40 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(40, (bool)parms[73]);
                }
                if ( (bool)parms[74] )
                {
                   stmt.setNull( 41 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(41, (bool)parms[75]);
                }
                if ( (bool)parms[76] )
                {
                   stmt.setNull( 42 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(42, (decimal)parms[77]);
                }
                if ( (bool)parms[78] )
                {
                   stmt.setNull( 43 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(43, (decimal)parms[79]);
                }
                if ( (bool)parms[80] )
                {
                   stmt.setNull( 44 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(44, (decimal)parms[81]);
                }
                if ( (bool)parms[82] )
                {
                   stmt.setNull( 45 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(45, (decimal)parms[83]);
                }
                if ( (bool)parms[84] )
                {
                   stmt.setNull( 46 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(46, (bool)parms[85]);
                }
                if ( (bool)parms[86] )
                {
                   stmt.setNull( 47 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(47, (String)parms[87]);
                }
                if ( (bool)parms[88] )
                {
                   stmt.setNull( 48 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(48, (short)parms[89]);
                }
                if ( (bool)parms[90] )
                {
                   stmt.setNull( 49 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(49, (short)parms[91]);
                }
                if ( (bool)parms[92] )
                {
                   stmt.setNull( 50 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(50, (bool)parms[93]);
                }
                stmt.SetParameter(51, (bool)parms[94]);
                stmt.SetParameter(52, (bool)parms[95]);
                if ( (bool)parms[96] )
                {
                   stmt.setNull( 53 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(53, (String)parms[97]);
                }
                if ( (bool)parms[98] )
                {
                   stmt.setNull( 54 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(54, (String)parms[99]);
                }
                if ( (bool)parms[100] )
                {
                   stmt.setNull( 55 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(55, (int)parms[101]);
                }
                stmt.SetParameter(56, (int)parms[102]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (String)parms[4]);
                stmt.SetParameter(4, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                stmt.SetParameter(7, (String)parms[10]);
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[24]);
                }
                stmt.SetParameter(15, (bool)parms[25]);
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(18, (bool)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 19 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(19, (short)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 20 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(20, (short)parms[35]);
                }
                stmt.SetParameter(21, (bool)parms[36]);
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 22 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(22, (bool)parms[38]);
                }
                stmt.SetParameter(23, (bool)parms[39]);
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 24 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(24, (DateTime)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 25 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(25, (DateTime)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 27 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(27, (bool)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 28 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(28, (String)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 29 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(29, (bool)parms[51]);
                }
                if ( (bool)parms[52] )
                {
                   stmt.setNull( 30 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(30, (bool)parms[53]);
                }
                if ( (bool)parms[54] )
                {
                   stmt.setNull( 31 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(31, (bool)parms[55]);
                }
                if ( (bool)parms[56] )
                {
                   stmt.setNull( 32 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(32, (bool)parms[57]);
                }
                if ( (bool)parms[58] )
                {
                   stmt.setNull( 33 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(33, (bool)parms[59]);
                }
                if ( (bool)parms[60] )
                {
                   stmt.setNull( 34 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(34, (short)parms[61]);
                }
                if ( (bool)parms[62] )
                {
                   stmt.setNull( 35 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(35, (bool)parms[63]);
                }
                if ( (bool)parms[64] )
                {
                   stmt.setNull( 36 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(36, (bool)parms[65]);
                }
                if ( (bool)parms[66] )
                {
                   stmt.setNull( 37 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(37, (bool)parms[67]);
                }
                if ( (bool)parms[68] )
                {
                   stmt.setNull( 38 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(38, (bool)parms[69]);
                }
                if ( (bool)parms[70] )
                {
                   stmt.setNull( 39 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(39, (bool)parms[71]);
                }
                if ( (bool)parms[72] )
                {
                   stmt.setNull( 40 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(40, (bool)parms[73]);
                }
                if ( (bool)parms[74] )
                {
                   stmt.setNull( 41 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(41, (decimal)parms[75]);
                }
                if ( (bool)parms[76] )
                {
                   stmt.setNull( 42 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(42, (decimal)parms[77]);
                }
                if ( (bool)parms[78] )
                {
                   stmt.setNull( 43 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(43, (decimal)parms[79]);
                }
                if ( (bool)parms[80] )
                {
                   stmt.setNull( 44 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(44, (decimal)parms[81]);
                }
                if ( (bool)parms[82] )
                {
                   stmt.setNull( 45 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(45, (bool)parms[83]);
                }
                if ( (bool)parms[84] )
                {
                   stmt.setNull( 46 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(46, (String)parms[85]);
                }
                if ( (bool)parms[86] )
                {
                   stmt.setNull( 47 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(47, (short)parms[87]);
                }
                if ( (bool)parms[88] )
                {
                   stmt.setNull( 48 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(48, (short)parms[89]);
                }
                if ( (bool)parms[90] )
                {
                   stmt.setNull( 49 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(49, (bool)parms[91]);
                }
                stmt.SetParameter(50, (bool)parms[92]);
                stmt.SetParameter(51, (bool)parms[93]);
                if ( (bool)parms[94] )
                {
                   stmt.setNull( 52 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(52, (String)parms[95]);
                }
                if ( (bool)parms[96] )
                {
                   stmt.setNull( 53 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(53, (String)parms[97]);
                }
                if ( (bool)parms[98] )
                {
                   stmt.setNull( 54 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(54, (int)parms[99]);
                }
                stmt.SetParameter(55, (int)parms[100]);
                if ( (bool)parms[101] )
                {
                   stmt.setNull( 56 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(56, (int)parms[102]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
