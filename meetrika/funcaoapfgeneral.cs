/*
               File: FuncaoAPFGeneral
        Description: Funcao APFGeneral
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:21.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaoapfgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public funcaoapfgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public funcaoapfgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPF_Codigo ,
                           int aP1_FuncaoAPF_SistemaCod )
      {
         this.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV12FuncaoAPF_SistemaCod = aP1_FuncaoAPF_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbFuncaoAPF_Tipo = new GXCombobox();
         cmbFuncaoAPF_Complexidade = new GXCombobox();
         cmbFuncaoAPF_Ativo = new GXCombobox();
         cmbFuncaoAPF_UpdAoImpBsln = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
                  AV12FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12FuncaoAPF_SistemaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A165FuncaoAPF_Codigo,(int)AV12FuncaoAPF_SistemaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA972( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV16Pgmname = "FuncaoAPFGeneral";
               context.Gx_err = 0;
               /* Using cursor H00972 */
               pr_default.execute(0, new Object[] {A165FuncaoAPF_Codigo});
               A358FuncaoAPF_FunAPFPaiCod = H00972_A358FuncaoAPF_FunAPFPaiCod[0];
               n358FuncaoAPF_FunAPFPaiCod = H00972_n358FuncaoAPF_FunAPFPaiCod[0];
               A359FuncaoAPF_ModuloCod = H00972_A359FuncaoAPF_ModuloCod[0];
               n359FuncaoAPF_ModuloCod = H00972_n359FuncaoAPF_ModuloCod[0];
               A360FuncaoAPF_SistemaCod = H00972_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = H00972_n360FuncaoAPF_SistemaCod[0];
               A1268FuncaoAPF_UpdAoImpBsln = H00972_A1268FuncaoAPF_UpdAoImpBsln[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
               n1268FuncaoAPF_UpdAoImpBsln = H00972_n1268FuncaoAPF_UpdAoImpBsln[0];
               A183FuncaoAPF_Ativo = H00972_A183FuncaoAPF_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
               A1233FuncaoAPF_ParecerSE = H00972_A1233FuncaoAPF_ParecerSE[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1233FuncaoAPF_ParecerSE", A1233FuncaoAPF_ParecerSE);
               n1233FuncaoAPF_ParecerSE = H00972_n1233FuncaoAPF_ParecerSE[0];
               A1244FuncaoAPF_Observacao = H00972_A1244FuncaoAPF_Observacao[0];
               n1244FuncaoAPF_Observacao = H00972_n1244FuncaoAPF_Observacao[0];
               A167FuncaoAPF_Descricao = H00972_A167FuncaoAPF_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A167FuncaoAPF_Descricao", A167FuncaoAPF_Descricao);
               A432FuncaoAPF_Link = H00972_A432FuncaoAPF_Link[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A432FuncaoAPF_Link", A432FuncaoAPF_Link);
               n432FuncaoAPF_Link = H00972_n432FuncaoAPF_Link[0];
               A184FuncaoAPF_Tipo = H00972_A184FuncaoAPF_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
               A166FuncaoAPF_Nome = H00972_A166FuncaoAPF_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
               pr_default.close(0);
               /* Using cursor H00973 */
               pr_default.execute(1, new Object[] {n358FuncaoAPF_FunAPFPaiCod, A358FuncaoAPF_FunAPFPaiCod});
               A363FuncaoAPF_FunAPFPaiNom = H00973_A363FuncaoAPF_FunAPFPaiNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A363FuncaoAPF_FunAPFPaiNom", A363FuncaoAPF_FunAPFPaiNom);
               n363FuncaoAPF_FunAPFPaiNom = H00973_n363FuncaoAPF_FunAPFPaiNom[0];
               pr_default.close(1);
               /* Using cursor H00974 */
               pr_default.execute(2, new Object[] {n359FuncaoAPF_ModuloCod, A359FuncaoAPF_ModuloCod});
               A361FuncaoAPF_ModuloNom = H00974_A361FuncaoAPF_ModuloNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A361FuncaoAPF_ModuloNom", A361FuncaoAPF_ModuloNom);
               n361FuncaoAPF_ModuloNom = H00974_n361FuncaoAPF_ModuloNom[0];
               pr_default.close(2);
               GXt_char1 = A185FuncaoAPF_Complexidade;
               new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A185FuncaoAPF_Complexidade = GXt_char1;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
               GXt_int2 = A388FuncaoAPF_TD;
               new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A388FuncaoAPF_TD = GXt_int2;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_TD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")));
               GXt_int2 = A387FuncaoAPF_AR;
               new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A387FuncaoAPF_AR = GXt_int2;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_AR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")));
               GXt_decimal3 = A386FuncaoAPF_PF;
               new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A386FuncaoAPF_PF = GXt_decimal3;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
               WS972( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Funcao APFGeneral") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311772210");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaoapfgeneral.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV12FuncaoAPF_SistemaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_OBSERVACAO", A1244FuncaoAPF_Observacao);
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV12FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV12FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_TD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_AR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_OBSERVACAO_Enabled", StringUtil.BoolToStr( Funcaoapf_observacao_Enabled));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm972( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("funcaoapfgeneral.js", "?2020311772213");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "FuncaoAPFGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Funcao APFGeneral" ;
      }

      protected void WB970( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "funcaoapfgeneral.aspx");
               context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
               context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
            }
            wb_table1_2_972( true) ;
         }
         else
         {
            wb_table1_2_972( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_972e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START972( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Funcao APFGeneral", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP970( ) ;
            }
         }
      }

      protected void WS972( )
      {
         START972( ) ;
         EVT972( ) ;
      }

      protected void EVT972( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP970( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP970( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11972 */
                                    E11972 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP970( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12972 */
                                    E12972 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP970( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13972 */
                                    E13972 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP970( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP970( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE972( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm972( ) ;
            }
         }
      }

      protected void PA972( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbFuncaoAPF_Tipo.Name = "FUNCAOAPF_TIPO";
            cmbFuncaoAPF_Tipo.WebTags = "";
            cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
            cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
            cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
            cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
            if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
            {
               A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
            }
            cmbFuncaoAPF_Complexidade.Name = "FUNCAOAPF_COMPLEXIDADE";
            cmbFuncaoAPF_Complexidade.WebTags = "";
            cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
            cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
            {
               A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
            }
            cmbFuncaoAPF_Ativo.Name = "FUNCAOAPF_ATIVO";
            cmbFuncaoAPF_Ativo.WebTags = "";
            cmbFuncaoAPF_Ativo.addItem("A", "Ativa", 0);
            cmbFuncaoAPF_Ativo.addItem("E", "Exclu�da", 0);
            cmbFuncaoAPF_Ativo.addItem("R", "Rejeitada", 0);
            if ( cmbFuncaoAPF_Ativo.ItemCount > 0 )
            {
               A183FuncaoAPF_Ativo = cmbFuncaoAPF_Ativo.getValidValue(A183FuncaoAPF_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
            }
            cmbFuncaoAPF_UpdAoImpBsln.Name = "FUNCAOAPF_UPDAOIMPBSLN";
            cmbFuncaoAPF_UpdAoImpBsln.WebTags = "";
            cmbFuncaoAPF_UpdAoImpBsln.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbFuncaoAPF_UpdAoImpBsln.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbFuncaoAPF_UpdAoImpBsln.ItemCount > 0 )
            {
               A1268FuncaoAPF_UpdAoImpBsln = StringUtil.StrToBool( cmbFuncaoAPF_UpdAoImpBsln.getValidValue(StringUtil.BoolToStr( A1268FuncaoAPF_UpdAoImpBsln)));
               n1268FuncaoAPF_UpdAoImpBsln = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
         {
            A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
         }
         if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
         {
            A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
         }
         if ( cmbFuncaoAPF_Ativo.ItemCount > 0 )
         {
            A183FuncaoAPF_Ativo = cmbFuncaoAPF_Ativo.getValidValue(A183FuncaoAPF_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
         }
         if ( cmbFuncaoAPF_UpdAoImpBsln.ItemCount > 0 )
         {
            A1268FuncaoAPF_UpdAoImpBsln = StringUtil.StrToBool( cmbFuncaoAPF_UpdAoImpBsln.getValidValue(StringUtil.BoolToStr( A1268FuncaoAPF_UpdAoImpBsln)));
            n1268FuncaoAPF_UpdAoImpBsln = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF972( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV16Pgmname = "FuncaoAPFGeneral";
         context.Gx_err = 0;
      }

      protected void RF972( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00975 */
            pr_default.execute(3, new Object[] {A165FuncaoAPF_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A364FuncaoAPFAtributos_AtributosCod = H00975_A364FuncaoAPFAtributos_AtributosCod[0];
               /* Execute user event: E12972 */
               E12972 ();
               pr_default.readNext(3);
            }
            pr_default.close(3);
            WB970( ) ;
         }
      }

      protected void STRUP970( )
      {
         /* Before Start, stand alone formulas. */
         AV16Pgmname = "FuncaoAPFGeneral";
         context.Gx_err = 0;
         /* Using cursor H00976 */
         pr_default.execute(4, new Object[] {A165FuncaoAPF_Codigo});
         A358FuncaoAPF_FunAPFPaiCod = H00976_A358FuncaoAPF_FunAPFPaiCod[0];
         n358FuncaoAPF_FunAPFPaiCod = H00976_n358FuncaoAPF_FunAPFPaiCod[0];
         A359FuncaoAPF_ModuloCod = H00976_A359FuncaoAPF_ModuloCod[0];
         n359FuncaoAPF_ModuloCod = H00976_n359FuncaoAPF_ModuloCod[0];
         A360FuncaoAPF_SistemaCod = H00976_A360FuncaoAPF_SistemaCod[0];
         n360FuncaoAPF_SistemaCod = H00976_n360FuncaoAPF_SistemaCod[0];
         A1268FuncaoAPF_UpdAoImpBsln = H00976_A1268FuncaoAPF_UpdAoImpBsln[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
         n1268FuncaoAPF_UpdAoImpBsln = H00976_n1268FuncaoAPF_UpdAoImpBsln[0];
         A183FuncaoAPF_Ativo = H00976_A183FuncaoAPF_Ativo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
         A1233FuncaoAPF_ParecerSE = H00976_A1233FuncaoAPF_ParecerSE[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1233FuncaoAPF_ParecerSE", A1233FuncaoAPF_ParecerSE);
         n1233FuncaoAPF_ParecerSE = H00976_n1233FuncaoAPF_ParecerSE[0];
         A1244FuncaoAPF_Observacao = H00976_A1244FuncaoAPF_Observacao[0];
         n1244FuncaoAPF_Observacao = H00976_n1244FuncaoAPF_Observacao[0];
         A167FuncaoAPF_Descricao = H00976_A167FuncaoAPF_Descricao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A167FuncaoAPF_Descricao", A167FuncaoAPF_Descricao);
         A432FuncaoAPF_Link = H00976_A432FuncaoAPF_Link[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A432FuncaoAPF_Link", A432FuncaoAPF_Link);
         n432FuncaoAPF_Link = H00976_n432FuncaoAPF_Link[0];
         A184FuncaoAPF_Tipo = H00976_A184FuncaoAPF_Tipo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
         A166FuncaoAPF_Nome = H00976_A166FuncaoAPF_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
         pr_default.close(4);
         /* Using cursor H00977 */
         pr_default.execute(5, new Object[] {n358FuncaoAPF_FunAPFPaiCod, A358FuncaoAPF_FunAPFPaiCod});
         A363FuncaoAPF_FunAPFPaiNom = H00977_A363FuncaoAPF_FunAPFPaiNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A363FuncaoAPF_FunAPFPaiNom", A363FuncaoAPF_FunAPFPaiNom);
         n363FuncaoAPF_FunAPFPaiNom = H00977_n363FuncaoAPF_FunAPFPaiNom[0];
         pr_default.close(5);
         /* Using cursor H00978 */
         pr_default.execute(6, new Object[] {n359FuncaoAPF_ModuloCod, A359FuncaoAPF_ModuloCod});
         A361FuncaoAPF_ModuloNom = H00978_A361FuncaoAPF_ModuloNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A361FuncaoAPF_ModuloNom", A361FuncaoAPF_ModuloNom);
         n361FuncaoAPF_ModuloNom = H00978_n361FuncaoAPF_ModuloNom[0];
         pr_default.close(6);
         GXt_char1 = A185FuncaoAPF_Complexidade;
         new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A185FuncaoAPF_Complexidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
         GXt_int2 = A388FuncaoAPF_TD;
         new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A388FuncaoAPF_TD = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_TD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")));
         GXt_int2 = A387FuncaoAPF_AR;
         new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A387FuncaoAPF_AR = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_AR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")));
         GXt_decimal3 = A386FuncaoAPF_PF;
         new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A386FuncaoAPF_PF = GXt_decimal3;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11972 */
         E11972 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            cmbFuncaoAPF_Tipo.CurrentValue = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
            A184FuncaoAPF_Tipo = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
            cmbFuncaoAPF_Complexidade.CurrentValue = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
            A185FuncaoAPF_Complexidade = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
            A388FuncaoAPF_TD = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_TD_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_TD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")));
            A387FuncaoAPF_AR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_AR_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_AR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")));
            A386FuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtFuncaoAPF_PF_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
            A363FuncaoAPF_FunAPFPaiNom = cgiGet( edtFuncaoAPF_FunAPFPaiNom_Internalname);
            n363FuncaoAPF_FunAPFPaiNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A363FuncaoAPF_FunAPFPaiNom", A363FuncaoAPF_FunAPFPaiNom);
            A361FuncaoAPF_ModuloNom = StringUtil.Upper( cgiGet( edtFuncaoAPF_ModuloNom_Internalname));
            n361FuncaoAPF_ModuloNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A361FuncaoAPF_ModuloNom", A361FuncaoAPF_ModuloNom);
            A432FuncaoAPF_Link = cgiGet( edtFuncaoAPF_Link_Internalname);
            n432FuncaoAPF_Link = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A432FuncaoAPF_Link", A432FuncaoAPF_Link);
            A167FuncaoAPF_Descricao = cgiGet( edtFuncaoAPF_Descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A167FuncaoAPF_Descricao", A167FuncaoAPF_Descricao);
            A1233FuncaoAPF_ParecerSE = cgiGet( edtFuncaoAPF_ParecerSE_Internalname);
            n1233FuncaoAPF_ParecerSE = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1233FuncaoAPF_ParecerSE", A1233FuncaoAPF_ParecerSE);
            cmbFuncaoAPF_Ativo.CurrentValue = cgiGet( cmbFuncaoAPF_Ativo_Internalname);
            A183FuncaoAPF_Ativo = cgiGet( cmbFuncaoAPF_Ativo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
            cmbFuncaoAPF_UpdAoImpBsln.CurrentValue = cgiGet( cmbFuncaoAPF_UpdAoImpBsln_Internalname);
            A1268FuncaoAPF_UpdAoImpBsln = StringUtil.StrToBool( cgiGet( cmbFuncaoAPF_UpdAoImpBsln_Internalname));
            n1268FuncaoAPF_UpdAoImpBsln = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1268FuncaoAPF_UpdAoImpBsln", A1268FuncaoAPF_UpdAoImpBsln);
            /* Read saved values. */
            A1244FuncaoAPF_Observacao = cgiGet( sPrefix+"FUNCAOAPF_OBSERVACAO");
            wcpOA165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA165FuncaoAPF_Codigo"), ",", "."));
            wcpOAV12FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV12FuncaoAPF_SistemaCod"), ",", "."));
            Funcaoapf_observacao_Enabled = StringUtil.StrToBool( cgiGet( sPrefix+"FUNCAOAPF_OBSERVACAO_Enabled"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11972 */
         E11972 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11972( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12972( )
      {
         /* Load Routine */
         edtFuncaoAPF_FunAPFPaiNom_Link = formatLink("viewfuncoesapfatributos.aspx") + "?" + UrlEncode("" +A358FuncaoAPF_FunAPFPaiCod) + "," + UrlEncode("" +A364FuncaoAPFAtributos_AtributosCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_FunAPFPaiNom_Internalname, "Link", edtFuncaoAPF_FunAPFPaiNom_Link);
         edtFuncaoAPF_ModuloNom_Link = formatLink("viewmodulo.aspx") + "?" + UrlEncode("" +A359FuncaoAPF_ModuloCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_ModuloNom_Internalname, "Link", edtFuncaoAPF_ModuloNom_Link);
         edtFuncaoAPF_Link_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_Link_Internalname, "Linktarget", edtFuncaoAPF_Link_Linktarget);
         edtFuncaoAPF_Link_Link = A432FuncaoAPF_Link;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_Link_Internalname, "Link", edtFuncaoAPF_Link_Link);
      }

      protected void E13972( )
      {
         /* 'DoFechar' Routine */
         AV13WebSession.Remove("FuncaoDados_Codigo");
         AV13WebSession.Remove("APFTabela_Codigo");
         context.wjLoc = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +A360FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim("FuncaoAPF"));
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV16Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "FuncaoAPF";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "FuncaoAPF_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_972( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_8_972( true) ;
         }
         else
         {
            wb_table2_8_972( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_972e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_82_972( true) ;
         }
         else
         {
            wb_table3_82_972( false) ;
         }
         return  ;
      }

      protected void wb_table3_82_972e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_972e( true) ;
         }
         else
         {
            wb_table1_2_972e( false) ;
         }
      }

      protected void wb_table3_82_972( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableatributoswc_Internalname, tblTableatributoswc_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_82_972e( true) ;
         }
         else
         {
            wb_table3_82_972e( false) ;
         }
      }

      protected void wb_table2_8_972( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_nome_Internalname, "Nome", "", "", lblTextblockfuncaoapf_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Nome_Internalname, A166FuncaoAPF_Nome, "", "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_tipo_Internalname, "Tipo", "", "", lblTextblockfuncaoapf_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_Tipo, cmbFuncaoAPF_Tipo_Internalname, StringUtil.RTrim( A184FuncaoAPF_Tipo), 1, cmbFuncaoAPF_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_FuncaoAPFGeneral.htm");
            cmbFuncaoAPF_Tipo.CurrentValue = StringUtil.RTrim( A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Tipo_Internalname, "Values", (String)(cmbFuncaoAPF_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_complexidade_Internalname, "Complexidade", "", "", lblTextblockfuncaoapf_complexidade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_Complexidade, cmbFuncaoAPF_Complexidade_Internalname, StringUtil.RTrim( A185FuncaoAPF_Complexidade), 1, cmbFuncaoAPF_Complexidade_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_FuncaoAPFGeneral.htm");
            cmbFuncaoAPF_Complexidade.CurrentValue = StringUtil.RTrim( A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Complexidade_Internalname, "Values", (String)(cmbFuncaoAPF_Complexidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_td_Internalname, "DER", "", "", lblTextblockfuncaoapf_td_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_TD_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_TD_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_ar_Internalname, "ARL", "", "", lblTextblockfuncaoapf_ar_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_AR_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_AR_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_pf_Internalname, "PF", "", "", lblTextblockfuncaoapf_pf_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_PF_Internalname, StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ",", "")), context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_PF_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_funapfpainom_Internalname, "Vinculada com", "", "", lblTextblockfuncaoapf_funapfpainom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_FunAPFPaiNom_Internalname, A363FuncaoAPF_FunAPFPaiNom, edtFuncaoAPF_FunAPFPaiNom_Link, "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_modulonom_Internalname, "M�dulo", "", "", lblTextblockfuncaoapf_modulonom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_ModuloNom_Internalname, StringUtil.RTrim( A361FuncaoAPF_ModuloNom), StringUtil.RTrim( context.localUtil.Format( A361FuncaoAPF_ModuloNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtFuncaoAPF_ModuloNom_Link, "", "", "", edtFuncaoAPF_ModuloNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_link_Internalname, "Link", "", "", lblTextblockfuncaoapf_link_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Link_Internalname, A432FuncaoAPF_Link, edtFuncaoAPF_Link_Link, "", 0, 1, 0, 0, 566, "px", 2, "row", StyleString, ClassString, "", "1000", 1, edtFuncaoAPF_Link_Linktarget, "", -1, true, "URLString", "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td rowspan=\"2\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_descricao_Internalname, "Descri��o", "", "", lblTextblockfuncaoapf_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\" rowspan=\"2\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Descricao_Internalname, A167FuncaoAPF_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_observacao_Internalname, "Observa��o", "", "", lblTextblockfuncaoapf_observacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"FUNCAOAPF_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_parecerse_Internalname, "Parecer SE", "", "", lblTextblockfuncaoapf_parecerse_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_ParecerSE_Internalname, A1233FuncaoAPF_ParecerSE, "", "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_ativo_Internalname, "Status", "", "", lblTextblockfuncaoapf_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_Ativo, cmbFuncaoAPF_Ativo_Internalname, StringUtil.RTrim( A183FuncaoAPF_Ativo), 1, cmbFuncaoAPF_Ativo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_FuncaoAPFGeneral.htm");
            cmbFuncaoAPF_Ativo.CurrentValue = StringUtil.RTrim( A183FuncaoAPF_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Ativo_Internalname, "Values", (String)(cmbFuncaoAPF_Ativo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_updaoimpbsln_Internalname, "Atualizar ao importar Baseline", "", "", lblTextblockfuncaoapf_updaoimpbsln_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_UpdAoImpBsln, cmbFuncaoAPF_UpdAoImpBsln_Internalname, StringUtil.BoolToStr( A1268FuncaoAPF_UpdAoImpBsln), 1, cmbFuncaoAPF_UpdAoImpBsln_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_FuncaoAPFGeneral.htm");
            cmbFuncaoAPF_UpdAoImpBsln.CurrentValue = StringUtil.BoolToStr( A1268FuncaoAPF_UpdAoImpBsln);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_UpdAoImpBsln_Internalname, "Values", (String)(cmbFuncaoAPF_UpdAoImpBsln.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_972e( true) ;
         }
         else
         {
            wb_table2_8_972e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A165FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         AV12FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12FuncaoAPF_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA972( ) ;
         WS972( ) ;
         WE972( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA165FuncaoAPF_Codigo = (String)((String)getParm(obj,0));
         sCtrlAV12FuncaoAPF_SistemaCod = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA972( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "funcaoapfgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA972( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A165FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            AV12FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12FuncaoAPF_SistemaCod), 6, 0)));
         }
         wcpOA165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA165FuncaoAPF_Codigo"), ",", "."));
         wcpOAV12FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV12FuncaoAPF_SistemaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A165FuncaoAPF_Codigo != wcpOA165FuncaoAPF_Codigo ) || ( AV12FuncaoAPF_SistemaCod != wcpOAV12FuncaoAPF_SistemaCod ) ) )
         {
            setjustcreated();
         }
         wcpOA165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
         wcpOAV12FuncaoAPF_SistemaCod = AV12FuncaoAPF_SistemaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA165FuncaoAPF_Codigo = cgiGet( sPrefix+"A165FuncaoAPF_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA165FuncaoAPF_Codigo) > 0 )
         {
            A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA165FuncaoAPF_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         }
         else
         {
            A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A165FuncaoAPF_Codigo_PARM"), ",", "."));
         }
         sCtrlAV12FuncaoAPF_SistemaCod = cgiGet( sPrefix+"AV12FuncaoAPF_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV12FuncaoAPF_SistemaCod) > 0 )
         {
            AV12FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV12FuncaoAPF_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12FuncaoAPF_SistemaCod), 6, 0)));
         }
         else
         {
            AV12FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV12FuncaoAPF_SistemaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA972( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS972( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS972( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A165FuncaoAPF_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA165FuncaoAPF_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A165FuncaoAPF_Codigo_CTRL", StringUtil.RTrim( sCtrlA165FuncaoAPF_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV12FuncaoAPF_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV12FuncaoAPF_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV12FuncaoAPF_SistemaCod_CTRL", StringUtil.RTrim( sCtrlAV12FuncaoAPF_SistemaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE972( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311772286");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("funcaoapfgeneral.js", "?2020311772287");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockfuncaoapf_nome_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_NOME";
         edtFuncaoAPF_Nome_Internalname = sPrefix+"FUNCAOAPF_NOME";
         lblTextblockfuncaoapf_tipo_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_TIPO";
         cmbFuncaoAPF_Tipo_Internalname = sPrefix+"FUNCAOAPF_TIPO";
         lblTextblockfuncaoapf_complexidade_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_COMPLEXIDADE";
         cmbFuncaoAPF_Complexidade_Internalname = sPrefix+"FUNCAOAPF_COMPLEXIDADE";
         lblTextblockfuncaoapf_td_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_TD";
         edtFuncaoAPF_TD_Internalname = sPrefix+"FUNCAOAPF_TD";
         lblTextblockfuncaoapf_ar_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_AR";
         edtFuncaoAPF_AR_Internalname = sPrefix+"FUNCAOAPF_AR";
         lblTextblockfuncaoapf_pf_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_PF";
         edtFuncaoAPF_PF_Internalname = sPrefix+"FUNCAOAPF_PF";
         lblTextblockfuncaoapf_funapfpainom_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_FUNAPFPAINOM";
         edtFuncaoAPF_FunAPFPaiNom_Internalname = sPrefix+"FUNCAOAPF_FUNAPFPAINOM";
         lblTextblockfuncaoapf_modulonom_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_MODULONOM";
         edtFuncaoAPF_ModuloNom_Internalname = sPrefix+"FUNCAOAPF_MODULONOM";
         lblTextblockfuncaoapf_link_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_LINK";
         edtFuncaoAPF_Link_Internalname = sPrefix+"FUNCAOAPF_LINK";
         lblTextblockfuncaoapf_descricao_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_DESCRICAO";
         edtFuncaoAPF_Descricao_Internalname = sPrefix+"FUNCAOAPF_DESCRICAO";
         lblTextblockfuncaoapf_observacao_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_OBSERVACAO";
         Funcaoapf_observacao_Internalname = sPrefix+"FUNCAOAPF_OBSERVACAO";
         lblTextblockfuncaoapf_parecerse_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_PARECERSE";
         edtFuncaoAPF_ParecerSE_Internalname = sPrefix+"FUNCAOAPF_PARECERSE";
         lblTextblockfuncaoapf_ativo_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_ATIVO";
         cmbFuncaoAPF_Ativo_Internalname = sPrefix+"FUNCAOAPF_ATIVO";
         lblTextblockfuncaoapf_updaoimpbsln_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_UPDAOIMPBSLN";
         cmbFuncaoAPF_UpdAoImpBsln_Internalname = sPrefix+"FUNCAOAPF_UPDAOIMPBSLN";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableatributoswc_Internalname = sPrefix+"TABLEATRIBUTOSWC";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbFuncaoAPF_UpdAoImpBsln_Jsonclick = "";
         cmbFuncaoAPF_Ativo_Jsonclick = "";
         Funcaoapf_observacao_Enabled = Convert.ToBoolean( 0);
         edtFuncaoAPF_ModuloNom_Jsonclick = "";
         edtFuncaoAPF_PF_Jsonclick = "";
         edtFuncaoAPF_AR_Jsonclick = "";
         edtFuncaoAPF_TD_Jsonclick = "";
         cmbFuncaoAPF_Complexidade_Jsonclick = "";
         cmbFuncaoAPF_Tipo_Jsonclick = "";
         edtFuncaoAPF_Link_Link = "";
         edtFuncaoAPF_Link_Linktarget = "";
         edtFuncaoAPF_ModuloNom_Link = "";
         edtFuncaoAPF_FunAPFPaiNom_Link = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E13972',iparms:[{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16Pgmname = "";
         scmdbuf = "";
         H00972_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         H00972_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         H00972_A359FuncaoAPF_ModuloCod = new int[1] ;
         H00972_n359FuncaoAPF_ModuloCod = new bool[] {false} ;
         H00972_A360FuncaoAPF_SistemaCod = new int[1] ;
         H00972_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H00972_A1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         H00972_n1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         H00972_A183FuncaoAPF_Ativo = new String[] {""} ;
         H00972_A1233FuncaoAPF_ParecerSE = new String[] {""} ;
         H00972_n1233FuncaoAPF_ParecerSE = new bool[] {false} ;
         H00972_A1244FuncaoAPF_Observacao = new String[] {""} ;
         H00972_n1244FuncaoAPF_Observacao = new bool[] {false} ;
         H00972_A167FuncaoAPF_Descricao = new String[] {""} ;
         H00972_A432FuncaoAPF_Link = new String[] {""} ;
         H00972_n432FuncaoAPF_Link = new bool[] {false} ;
         H00972_A184FuncaoAPF_Tipo = new String[] {""} ;
         H00972_A166FuncaoAPF_Nome = new String[] {""} ;
         A183FuncaoAPF_Ativo = "";
         A1233FuncaoAPF_ParecerSE = "";
         A1244FuncaoAPF_Observacao = "";
         A167FuncaoAPF_Descricao = "";
         A432FuncaoAPF_Link = "";
         A184FuncaoAPF_Tipo = "";
         A166FuncaoAPF_Nome = "";
         H00973_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         H00973_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         A363FuncaoAPF_FunAPFPaiNom = "";
         H00974_A361FuncaoAPF_ModuloNom = new String[] {""} ;
         H00974_n361FuncaoAPF_ModuloNom = new bool[] {false} ;
         A361FuncaoAPF_ModuloNom = "";
         A185FuncaoAPF_Complexidade = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H00975_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         H00975_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         H00975_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         H00975_A359FuncaoAPF_ModuloCod = new int[1] ;
         H00975_n359FuncaoAPF_ModuloCod = new bool[] {false} ;
         H00975_A360FuncaoAPF_SistemaCod = new int[1] ;
         H00975_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H00975_A1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         H00975_n1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         H00975_A183FuncaoAPF_Ativo = new String[] {""} ;
         H00975_A1233FuncaoAPF_ParecerSE = new String[] {""} ;
         H00975_n1233FuncaoAPF_ParecerSE = new bool[] {false} ;
         H00975_A1244FuncaoAPF_Observacao = new String[] {""} ;
         H00975_n1244FuncaoAPF_Observacao = new bool[] {false} ;
         H00975_A167FuncaoAPF_Descricao = new String[] {""} ;
         H00975_A432FuncaoAPF_Link = new String[] {""} ;
         H00975_n432FuncaoAPF_Link = new bool[] {false} ;
         H00975_A361FuncaoAPF_ModuloNom = new String[] {""} ;
         H00975_n361FuncaoAPF_ModuloNom = new bool[] {false} ;
         H00975_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         H00975_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         H00975_A184FuncaoAPF_Tipo = new String[] {""} ;
         H00975_A166FuncaoAPF_Nome = new String[] {""} ;
         H00975_A165FuncaoAPF_Codigo = new int[1] ;
         H00976_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         H00976_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         H00976_A359FuncaoAPF_ModuloCod = new int[1] ;
         H00976_n359FuncaoAPF_ModuloCod = new bool[] {false} ;
         H00976_A360FuncaoAPF_SistemaCod = new int[1] ;
         H00976_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H00976_A1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         H00976_n1268FuncaoAPF_UpdAoImpBsln = new bool[] {false} ;
         H00976_A183FuncaoAPF_Ativo = new String[] {""} ;
         H00976_A1233FuncaoAPF_ParecerSE = new String[] {""} ;
         H00976_n1233FuncaoAPF_ParecerSE = new bool[] {false} ;
         H00976_A1244FuncaoAPF_Observacao = new String[] {""} ;
         H00976_n1244FuncaoAPF_Observacao = new bool[] {false} ;
         H00976_A167FuncaoAPF_Descricao = new String[] {""} ;
         H00976_A432FuncaoAPF_Link = new String[] {""} ;
         H00976_n432FuncaoAPF_Link = new bool[] {false} ;
         H00976_A184FuncaoAPF_Tipo = new String[] {""} ;
         H00976_A166FuncaoAPF_Nome = new String[] {""} ;
         H00977_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         H00977_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         H00978_A361FuncaoAPF_ModuloNom = new String[] {""} ;
         H00978_n361FuncaoAPF_ModuloNom = new bool[] {false} ;
         GXt_char1 = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV13WebSession = context.GetSession();
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockfuncaoapf_nome_Jsonclick = "";
         lblTextblockfuncaoapf_tipo_Jsonclick = "";
         lblTextblockfuncaoapf_complexidade_Jsonclick = "";
         lblTextblockfuncaoapf_td_Jsonclick = "";
         lblTextblockfuncaoapf_ar_Jsonclick = "";
         lblTextblockfuncaoapf_pf_Jsonclick = "";
         lblTextblockfuncaoapf_funapfpainom_Jsonclick = "";
         lblTextblockfuncaoapf_modulonom_Jsonclick = "";
         lblTextblockfuncaoapf_link_Jsonclick = "";
         lblTextblockfuncaoapf_descricao_Jsonclick = "";
         lblTextblockfuncaoapf_observacao_Jsonclick = "";
         lblTextblockfuncaoapf_parecerse_Jsonclick = "";
         lblTextblockfuncaoapf_ativo_Jsonclick = "";
         lblTextblockfuncaoapf_updaoimpbsln_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA165FuncaoAPF_Codigo = "";
         sCtrlAV12FuncaoAPF_SistemaCod = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaoapfgeneral__default(),
            new Object[][] {
                new Object[] {
               H00972_A358FuncaoAPF_FunAPFPaiCod, H00972_n358FuncaoAPF_FunAPFPaiCod, H00972_A359FuncaoAPF_ModuloCod, H00972_n359FuncaoAPF_ModuloCod, H00972_A360FuncaoAPF_SistemaCod, H00972_n360FuncaoAPF_SistemaCod, H00972_A1268FuncaoAPF_UpdAoImpBsln, H00972_n1268FuncaoAPF_UpdAoImpBsln, H00972_A183FuncaoAPF_Ativo, H00972_A1233FuncaoAPF_ParecerSE,
               H00972_n1233FuncaoAPF_ParecerSE, H00972_A1244FuncaoAPF_Observacao, H00972_n1244FuncaoAPF_Observacao, H00972_A167FuncaoAPF_Descricao, H00972_A432FuncaoAPF_Link, H00972_n432FuncaoAPF_Link, H00972_A184FuncaoAPF_Tipo, H00972_A166FuncaoAPF_Nome
               }
               , new Object[] {
               H00973_A363FuncaoAPF_FunAPFPaiNom, H00973_n363FuncaoAPF_FunAPFPaiNom
               }
               , new Object[] {
               H00974_A361FuncaoAPF_ModuloNom, H00974_n361FuncaoAPF_ModuloNom
               }
               , new Object[] {
               H00975_A364FuncaoAPFAtributos_AtributosCod, H00975_A358FuncaoAPF_FunAPFPaiCod, H00975_n358FuncaoAPF_FunAPFPaiCod, H00975_A359FuncaoAPF_ModuloCod, H00975_n359FuncaoAPF_ModuloCod, H00975_A360FuncaoAPF_SistemaCod, H00975_n360FuncaoAPF_SistemaCod, H00975_A1268FuncaoAPF_UpdAoImpBsln, H00975_n1268FuncaoAPF_UpdAoImpBsln, H00975_A183FuncaoAPF_Ativo,
               H00975_A1233FuncaoAPF_ParecerSE, H00975_n1233FuncaoAPF_ParecerSE, H00975_A1244FuncaoAPF_Observacao, H00975_n1244FuncaoAPF_Observacao, H00975_A167FuncaoAPF_Descricao, H00975_A432FuncaoAPF_Link, H00975_n432FuncaoAPF_Link, H00975_A361FuncaoAPF_ModuloNom, H00975_n361FuncaoAPF_ModuloNom, H00975_A363FuncaoAPF_FunAPFPaiNom,
               H00975_n363FuncaoAPF_FunAPFPaiNom, H00975_A184FuncaoAPF_Tipo, H00975_A166FuncaoAPF_Nome, H00975_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               H00976_A358FuncaoAPF_FunAPFPaiCod, H00976_n358FuncaoAPF_FunAPFPaiCod, H00976_A359FuncaoAPF_ModuloCod, H00976_n359FuncaoAPF_ModuloCod, H00976_A360FuncaoAPF_SistemaCod, H00976_n360FuncaoAPF_SistemaCod, H00976_A1268FuncaoAPF_UpdAoImpBsln, H00976_n1268FuncaoAPF_UpdAoImpBsln, H00976_A183FuncaoAPF_Ativo, H00976_A1233FuncaoAPF_ParecerSE,
               H00976_n1233FuncaoAPF_ParecerSE, H00976_A1244FuncaoAPF_Observacao, H00976_n1244FuncaoAPF_Observacao, H00976_A167FuncaoAPF_Descricao, H00976_A432FuncaoAPF_Link, H00976_n432FuncaoAPF_Link, H00976_A184FuncaoAPF_Tipo, H00976_A166FuncaoAPF_Nome
               }
               , new Object[] {
               H00977_A363FuncaoAPF_FunAPFPaiNom, H00977_n363FuncaoAPF_FunAPFPaiNom
               }
               , new Object[] {
               H00978_A361FuncaoAPF_ModuloNom, H00978_n361FuncaoAPF_ModuloNom
               }
            }
         );
         AV16Pgmname = "FuncaoAPFGeneral";
         /* GeneXus formulas. */
         AV16Pgmname = "FuncaoAPFGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A388FuncaoAPF_TD ;
      private short A387FuncaoAPF_AR ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short GXt_int2 ;
      private short nGXWrapped ;
      private int A165FuncaoAPF_Codigo ;
      private int AV12FuncaoAPF_SistemaCod ;
      private int wcpOA165FuncaoAPF_Codigo ;
      private int wcpOAV12FuncaoAPF_SistemaCod ;
      private int A358FuncaoAPF_FunAPFPaiCod ;
      private int A359FuncaoAPF_ModuloCod ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int AV7FuncaoAPF_Codigo ;
      private int idxLst ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal3 ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV16Pgmname ;
      private String scmdbuf ;
      private String A183FuncaoAPF_Ativo ;
      private String A184FuncaoAPF_Tipo ;
      private String A361FuncaoAPF_ModuloNom ;
      private String A185FuncaoAPF_Complexidade ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXt_char1 ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String cmbFuncaoAPF_Tipo_Internalname ;
      private String cmbFuncaoAPF_Complexidade_Internalname ;
      private String edtFuncaoAPF_TD_Internalname ;
      private String edtFuncaoAPF_AR_Internalname ;
      private String edtFuncaoAPF_PF_Internalname ;
      private String edtFuncaoAPF_FunAPFPaiNom_Internalname ;
      private String edtFuncaoAPF_ModuloNom_Internalname ;
      private String edtFuncaoAPF_Link_Internalname ;
      private String edtFuncaoAPF_Descricao_Internalname ;
      private String edtFuncaoAPF_ParecerSE_Internalname ;
      private String cmbFuncaoAPF_Ativo_Internalname ;
      private String cmbFuncaoAPF_UpdAoImpBsln_Internalname ;
      private String edtFuncaoAPF_FunAPFPaiNom_Link ;
      private String edtFuncaoAPF_ModuloNom_Link ;
      private String edtFuncaoAPF_Link_Linktarget ;
      private String edtFuncaoAPF_Link_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableatributoswc_Internalname ;
      private String TempTags ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockfuncaoapf_nome_Internalname ;
      private String lblTextblockfuncaoapf_nome_Jsonclick ;
      private String lblTextblockfuncaoapf_tipo_Internalname ;
      private String lblTextblockfuncaoapf_tipo_Jsonclick ;
      private String cmbFuncaoAPF_Tipo_Jsonclick ;
      private String lblTextblockfuncaoapf_complexidade_Internalname ;
      private String lblTextblockfuncaoapf_complexidade_Jsonclick ;
      private String cmbFuncaoAPF_Complexidade_Jsonclick ;
      private String lblTextblockfuncaoapf_td_Internalname ;
      private String lblTextblockfuncaoapf_td_Jsonclick ;
      private String edtFuncaoAPF_TD_Jsonclick ;
      private String lblTextblockfuncaoapf_ar_Internalname ;
      private String lblTextblockfuncaoapf_ar_Jsonclick ;
      private String edtFuncaoAPF_AR_Jsonclick ;
      private String lblTextblockfuncaoapf_pf_Internalname ;
      private String lblTextblockfuncaoapf_pf_Jsonclick ;
      private String edtFuncaoAPF_PF_Jsonclick ;
      private String lblTextblockfuncaoapf_funapfpainom_Internalname ;
      private String lblTextblockfuncaoapf_funapfpainom_Jsonclick ;
      private String lblTextblockfuncaoapf_modulonom_Internalname ;
      private String lblTextblockfuncaoapf_modulonom_Jsonclick ;
      private String edtFuncaoAPF_ModuloNom_Jsonclick ;
      private String lblTextblockfuncaoapf_link_Internalname ;
      private String lblTextblockfuncaoapf_link_Jsonclick ;
      private String lblTextblockfuncaoapf_descricao_Internalname ;
      private String lblTextblockfuncaoapf_descricao_Jsonclick ;
      private String lblTextblockfuncaoapf_observacao_Internalname ;
      private String lblTextblockfuncaoapf_observacao_Jsonclick ;
      private String lblTextblockfuncaoapf_parecerse_Internalname ;
      private String lblTextblockfuncaoapf_parecerse_Jsonclick ;
      private String lblTextblockfuncaoapf_ativo_Internalname ;
      private String lblTextblockfuncaoapf_ativo_Jsonclick ;
      private String cmbFuncaoAPF_Ativo_Jsonclick ;
      private String lblTextblockfuncaoapf_updaoimpbsln_Internalname ;
      private String lblTextblockfuncaoapf_updaoimpbsln_Jsonclick ;
      private String cmbFuncaoAPF_UpdAoImpBsln_Jsonclick ;
      private String sCtrlA165FuncaoAPF_Codigo ;
      private String sCtrlAV12FuncaoAPF_SistemaCod ;
      private String Funcaoapf_observacao_Internalname ;
      private bool entryPointCalled ;
      private bool n358FuncaoAPF_FunAPFPaiCod ;
      private bool n359FuncaoAPF_ModuloCod ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool A1268FuncaoAPF_UpdAoImpBsln ;
      private bool n1268FuncaoAPF_UpdAoImpBsln ;
      private bool n1233FuncaoAPF_ParecerSE ;
      private bool n1244FuncaoAPF_Observacao ;
      private bool n432FuncaoAPF_Link ;
      private bool n363FuncaoAPF_FunAPFPaiNom ;
      private bool n361FuncaoAPF_ModuloNom ;
      private bool toggleJsOutput ;
      private bool Funcaoapf_observacao_Enabled ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String A1233FuncaoAPF_ParecerSE ;
      private String A1244FuncaoAPF_Observacao ;
      private String A167FuncaoAPF_Descricao ;
      private String A432FuncaoAPF_Link ;
      private String A166FuncaoAPF_Nome ;
      private String A363FuncaoAPF_FunAPFPaiNom ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbFuncaoAPF_Tipo ;
      private GXCombobox cmbFuncaoAPF_Complexidade ;
      private GXCombobox cmbFuncaoAPF_Ativo ;
      private GXCombobox cmbFuncaoAPF_UpdAoImpBsln ;
      private IDataStoreProvider pr_default ;
      private int[] H00972_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] H00972_n358FuncaoAPF_FunAPFPaiCod ;
      private int[] H00972_A359FuncaoAPF_ModuloCod ;
      private bool[] H00972_n359FuncaoAPF_ModuloCod ;
      private int[] H00972_A360FuncaoAPF_SistemaCod ;
      private bool[] H00972_n360FuncaoAPF_SistemaCod ;
      private bool[] H00972_A1268FuncaoAPF_UpdAoImpBsln ;
      private bool[] H00972_n1268FuncaoAPF_UpdAoImpBsln ;
      private String[] H00972_A183FuncaoAPF_Ativo ;
      private String[] H00972_A1233FuncaoAPF_ParecerSE ;
      private bool[] H00972_n1233FuncaoAPF_ParecerSE ;
      private String[] H00972_A1244FuncaoAPF_Observacao ;
      private bool[] H00972_n1244FuncaoAPF_Observacao ;
      private String[] H00972_A167FuncaoAPF_Descricao ;
      private String[] H00972_A432FuncaoAPF_Link ;
      private bool[] H00972_n432FuncaoAPF_Link ;
      private String[] H00972_A184FuncaoAPF_Tipo ;
      private String[] H00972_A166FuncaoAPF_Nome ;
      private String[] H00973_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] H00973_n363FuncaoAPF_FunAPFPaiNom ;
      private String[] H00974_A361FuncaoAPF_ModuloNom ;
      private bool[] H00974_n361FuncaoAPF_ModuloNom ;
      private int[] H00975_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] H00975_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] H00975_n358FuncaoAPF_FunAPFPaiCod ;
      private int[] H00975_A359FuncaoAPF_ModuloCod ;
      private bool[] H00975_n359FuncaoAPF_ModuloCod ;
      private int[] H00975_A360FuncaoAPF_SistemaCod ;
      private bool[] H00975_n360FuncaoAPF_SistemaCod ;
      private bool[] H00975_A1268FuncaoAPF_UpdAoImpBsln ;
      private bool[] H00975_n1268FuncaoAPF_UpdAoImpBsln ;
      private String[] H00975_A183FuncaoAPF_Ativo ;
      private String[] H00975_A1233FuncaoAPF_ParecerSE ;
      private bool[] H00975_n1233FuncaoAPF_ParecerSE ;
      private String[] H00975_A1244FuncaoAPF_Observacao ;
      private bool[] H00975_n1244FuncaoAPF_Observacao ;
      private String[] H00975_A167FuncaoAPF_Descricao ;
      private String[] H00975_A432FuncaoAPF_Link ;
      private bool[] H00975_n432FuncaoAPF_Link ;
      private String[] H00975_A361FuncaoAPF_ModuloNom ;
      private bool[] H00975_n361FuncaoAPF_ModuloNom ;
      private String[] H00975_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] H00975_n363FuncaoAPF_FunAPFPaiNom ;
      private String[] H00975_A184FuncaoAPF_Tipo ;
      private String[] H00975_A166FuncaoAPF_Nome ;
      private int[] H00975_A165FuncaoAPF_Codigo ;
      private int[] H00976_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] H00976_n358FuncaoAPF_FunAPFPaiCod ;
      private int[] H00976_A359FuncaoAPF_ModuloCod ;
      private bool[] H00976_n359FuncaoAPF_ModuloCod ;
      private int[] H00976_A360FuncaoAPF_SistemaCod ;
      private bool[] H00976_n360FuncaoAPF_SistemaCod ;
      private bool[] H00976_A1268FuncaoAPF_UpdAoImpBsln ;
      private bool[] H00976_n1268FuncaoAPF_UpdAoImpBsln ;
      private String[] H00976_A183FuncaoAPF_Ativo ;
      private String[] H00976_A1233FuncaoAPF_ParecerSE ;
      private bool[] H00976_n1233FuncaoAPF_ParecerSE ;
      private String[] H00976_A1244FuncaoAPF_Observacao ;
      private bool[] H00976_n1244FuncaoAPF_Observacao ;
      private String[] H00976_A167FuncaoAPF_Descricao ;
      private String[] H00976_A432FuncaoAPF_Link ;
      private bool[] H00976_n432FuncaoAPF_Link ;
      private String[] H00976_A184FuncaoAPF_Tipo ;
      private String[] H00976_A166FuncaoAPF_Nome ;
      private String[] H00977_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] H00977_n363FuncaoAPF_FunAPFPaiNom ;
      private String[] H00978_A361FuncaoAPF_ModuloNom ;
      private bool[] H00978_n361FuncaoAPF_ModuloNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV13WebSession ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class funcaoapfgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00972 ;
          prmH00972 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00973 ;
          prmH00973 = new Object[] {
          new Object[] {"@FuncaoAPF_FunAPFPaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00974 ;
          prmH00974 = new Object[] {
          new Object[] {"@FuncaoAPF_ModuloCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00975 ;
          prmH00975 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00976 ;
          prmH00976 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00977 ;
          prmH00977 = new Object[] {
          new Object[] {"@FuncaoAPF_FunAPFPaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00978 ;
          prmH00978 = new Object[] {
          new Object[] {"@FuncaoAPF_ModuloCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00972", "SELECT [FuncaoAPF_FunAPFPaiCod] AS FuncaoAPF_FunAPFPaiCod, [FuncaoAPF_ModuloCod] AS FuncaoAPF_ModuloCod, [FuncaoAPF_SistemaCod], [FuncaoAPF_UpdAoImpBsln], [FuncaoAPF_Ativo], [FuncaoAPF_ParecerSE], [FuncaoAPF_Observacao], [FuncaoAPF_Descricao], [FuncaoAPF_Link], [FuncaoAPF_Tipo], [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00972,1,0,true,false )
             ,new CursorDef("H00973", "SELECT [FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_FunAPFPaiCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00973,1,0,true,false )
             ,new CursorDef("H00974", "SELECT [Modulo_Nome] AS FuncaoAPF_ModuloNom FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @FuncaoAPF_ModuloCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00974,1,0,true,false )
             ,new CursorDef("H00975", "SELECT T1.[FuncaoAPFAtributos_AtributosCod], T2.[FuncaoAPF_FunAPFPaiCod] AS FuncaoAPF_FunAPFPaiCod, T2.[FuncaoAPF_ModuloCod] AS FuncaoAPF_ModuloCod, T2.[FuncaoAPF_SistemaCod], T2.[FuncaoAPF_UpdAoImpBsln], T2.[FuncaoAPF_Ativo], T2.[FuncaoAPF_ParecerSE], T2.[FuncaoAPF_Observacao], T2.[FuncaoAPF_Descricao], T2.[FuncaoAPF_Link], T4.[Modulo_Nome] AS FuncaoAPF_ModuloNom, T3.[FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom, T2.[FuncaoAPF_Tipo], T2.[FuncaoAPF_Nome], T1.[FuncaoAPF_Codigo] FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo]) LEFT JOIN [FuncoesAPF] T3 WITH (NOLOCK) ON T3.[FuncaoAPF_Codigo] = T2.[FuncaoAPF_FunAPFPaiCod]) LEFT JOIN [Modulo] T4 WITH (NOLOCK) ON T4.[Modulo_Codigo] = T2.[FuncaoAPF_ModuloCod]) WHERE T1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY T1.[FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00975,100,0,true,false )
             ,new CursorDef("H00976", "SELECT [FuncaoAPF_FunAPFPaiCod] AS FuncaoAPF_FunAPFPaiCod, [FuncaoAPF_ModuloCod] AS FuncaoAPF_ModuloCod, [FuncaoAPF_SistemaCod], [FuncaoAPF_UpdAoImpBsln], [FuncaoAPF_Ativo], [FuncaoAPF_ParecerSE], [FuncaoAPF_Observacao], [FuncaoAPF_Descricao], [FuncaoAPF_Link], [FuncaoAPF_Tipo], [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00976,1,0,true,false )
             ,new CursorDef("H00977", "SELECT [FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_FunAPFPaiCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00977,1,0,true,false )
             ,new CursorDef("H00978", "SELECT [Modulo_Nome] AS FuncaoAPF_ModuloNom FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @FuncaoAPF_ModuloCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00978,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[9])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(8) ;
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 3) ;
                ((String[]) buf[17])[0] = rslt.getVarchar(11) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(9) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 3) ;
                ((String[]) buf[22])[0] = rslt.getVarchar(14) ;
                ((int[]) buf[23])[0] = rslt.getInt(15) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[9])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(8) ;
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 3) ;
                ((String[]) buf[17])[0] = rslt.getVarchar(11) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
