/*
               File: type_SdtSDT_Redmineissue_journals_journal
        Description: SDT_Redmineissue
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/18/2020 21:37:15.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Redmineissue.journals.journal" )]
   [XmlType(TypeName =  "SDT_Redmineissue.journals.journal" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_Redmineissue_journals_journal_user ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_Redmineissue_journals_journal_details ))]
   [Serializable]
   public class SdtSDT_Redmineissue_journals_journal : GxUserType
   {
      public SdtSDT_Redmineissue_journals_journal( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Redmineissue_journals_journal_Notes = "";
         gxTv_SdtSDT_Redmineissue_journals_journal_Created_on = "";
         gxTv_SdtSDT_Redmineissue_journals_journal_Private_notes = "";
      }

      public SdtSDT_Redmineissue_journals_journal( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Redmineissue_journals_journal deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Redmineissue_journals_journal)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Redmineissue_journals_journal obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_User = deserialized.gxTpr_User;
         obj.gxTpr_Notes = deserialized.gxTpr_Notes;
         obj.gxTpr_Created_on = deserialized.gxTpr_Created_on;
         obj.gxTpr_Private_notes = deserialized.gxTpr_Private_notes;
         obj.gxTpr_Details = deserialized.gxTpr_Details;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("id") == 1 )
         {
            gxTv_SdtSDT_Redmineissue_journals_journal_Id = (short)(NumberUtil.Val( oReader.GetAttributeByName("id"), "."));
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "user") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_Redmineissue_journals_journal_User == null )
                  {
                     gxTv_SdtSDT_Redmineissue_journals_journal_User = new SdtSDT_Redmineissue_journals_journal_user(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_Redmineissue_journals_journal_User.readxml(oReader, "user");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "notes") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_journals_journal_Notes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "created_on") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_journals_journal_Created_on = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "private_notes") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_journals_journal_Private_notes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "details") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_Redmineissue_journals_journal_Details == null )
                  {
                     gxTv_SdtSDT_Redmineissue_journals_journal_Details = new SdtSDT_Redmineissue_journals_journal_details(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_Redmineissue_journals_journal_Details.readxml(oReader, "details");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Redmineissue.journals.journal";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Redmineissue_journals_journal_Id), 4, 0)));
         if ( gxTv_SdtSDT_Redmineissue_journals_journal_User != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_Redmineissue_journals_journal_User.writexml(oWriter, "user", sNameSpace1);
         }
         oWriter.WriteElement("notes", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_journals_journal_Notes));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("created_on", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_journals_journal_Created_on));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("private_notes", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_journals_journal_Private_notes));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         if ( gxTv_SdtSDT_Redmineissue_journals_journal_Details != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_Redmineissue_journals_journal_Details.writexml(oWriter, "details", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("id", gxTv_SdtSDT_Redmineissue_journals_journal_Id, false);
         if ( gxTv_SdtSDT_Redmineissue_journals_journal_User != null )
         {
            AddObjectProperty("user", gxTv_SdtSDT_Redmineissue_journals_journal_User, false);
         }
         AddObjectProperty("notes", gxTv_SdtSDT_Redmineissue_journals_journal_Notes, false);
         AddObjectProperty("created_on", gxTv_SdtSDT_Redmineissue_journals_journal_Created_on, false);
         AddObjectProperty("private_notes", gxTv_SdtSDT_Redmineissue_journals_journal_Private_notes, false);
         if ( gxTv_SdtSDT_Redmineissue_journals_journal_Details != null )
         {
            AddObjectProperty("details", gxTv_SdtSDT_Redmineissue_journals_journal_Details, false);
         }
         return  ;
      }

      [SoapAttribute( AttributeName = "id" )]
      [XmlAttribute( AttributeName = "id" )]
      public short gxTpr_Id
      {
         get {
            return gxTv_SdtSDT_Redmineissue_journals_journal_Id ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_journals_journal_Id = (short)(value);
         }

      }

      [  SoapElement( ElementName = "user" )]
      [  XmlElement( ElementName = "user"   )]
      public SdtSDT_Redmineissue_journals_journal_user gxTpr_User
      {
         get {
            if ( gxTv_SdtSDT_Redmineissue_journals_journal_User == null )
            {
               gxTv_SdtSDT_Redmineissue_journals_journal_User = new SdtSDT_Redmineissue_journals_journal_user(context);
            }
            return gxTv_SdtSDT_Redmineissue_journals_journal_User ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_journals_journal_User = value;
         }

      }

      public void gxTv_SdtSDT_Redmineissue_journals_journal_User_SetNull( )
      {
         gxTv_SdtSDT_Redmineissue_journals_journal_User = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Redmineissue_journals_journal_User_IsNull( )
      {
         if ( gxTv_SdtSDT_Redmineissue_journals_journal_User == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "notes" )]
      [  XmlElement( ElementName = "notes" , Namespace = ""  )]
      public String gxTpr_Notes
      {
         get {
            return gxTv_SdtSDT_Redmineissue_journals_journal_Notes ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_journals_journal_Notes = (String)(value);
         }

      }

      [  SoapElement( ElementName = "created_on" )]
      [  XmlElement( ElementName = "created_on" , Namespace = ""  )]
      public String gxTpr_Created_on
      {
         get {
            return gxTv_SdtSDT_Redmineissue_journals_journal_Created_on ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_journals_journal_Created_on = (String)(value);
         }

      }

      [  SoapElement( ElementName = "private_notes" )]
      [  XmlElement( ElementName = "private_notes" , Namespace = ""  )]
      public String gxTpr_Private_notes
      {
         get {
            return gxTv_SdtSDT_Redmineissue_journals_journal_Private_notes ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_journals_journal_Private_notes = (String)(value);
         }

      }

      [  SoapElement( ElementName = "details" )]
      [  XmlElement( ElementName = "details"   )]
      public SdtSDT_Redmineissue_journals_journal_details gxTpr_Details
      {
         get {
            if ( gxTv_SdtSDT_Redmineissue_journals_journal_Details == null )
            {
               gxTv_SdtSDT_Redmineissue_journals_journal_Details = new SdtSDT_Redmineissue_journals_journal_details(context);
            }
            return gxTv_SdtSDT_Redmineissue_journals_journal_Details ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_journals_journal_Details = value;
         }

      }

      public void gxTv_SdtSDT_Redmineissue_journals_journal_Details_SetNull( )
      {
         gxTv_SdtSDT_Redmineissue_journals_journal_Details = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Redmineissue_journals_journal_Details_IsNull( )
      {
         if ( gxTv_SdtSDT_Redmineissue_journals_journal_Details == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_Redmineissue_journals_journal_Notes = "";
         gxTv_SdtSDT_Redmineissue_journals_journal_Created_on = "";
         gxTv_SdtSDT_Redmineissue_journals_journal_Private_notes = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_Redmineissue_journals_journal_Id ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_Redmineissue_journals_journal_Notes ;
      protected String gxTv_SdtSDT_Redmineissue_journals_journal_Created_on ;
      protected String gxTv_SdtSDT_Redmineissue_journals_journal_Private_notes ;
      protected String sTagName ;
      protected SdtSDT_Redmineissue_journals_journal_user gxTv_SdtSDT_Redmineissue_journals_journal_User=null ;
      protected SdtSDT_Redmineissue_journals_journal_details gxTv_SdtSDT_Redmineissue_journals_journal_Details=null ;
   }

   [DataContract(Name = @"SDT_Redmineissue.journals.journal", Namespace = "")]
   public class SdtSDT_Redmineissue_journals_journal_RESTInterface : GxGenericCollectionItem<SdtSDT_Redmineissue_journals_journal>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Redmineissue_journals_journal_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Redmineissue_journals_journal_RESTInterface( SdtSDT_Redmineissue_journals_journal psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "id" , Order = 0 )]
      public Nullable<short> gxTpr_Id
      {
         get {
            return sdt.gxTpr_Id ;
         }

         set {
            sdt.gxTpr_Id = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "user" , Order = 1 )]
      public SdtSDT_Redmineissue_journals_journal_user_RESTInterface gxTpr_User
      {
         get {
            return new SdtSDT_Redmineissue_journals_journal_user_RESTInterface(sdt.gxTpr_User) ;
         }

         set {
            sdt.gxTpr_User = value.sdt;
         }

      }

      [DataMember( Name = "notes" , Order = 2 )]
      public String gxTpr_Notes
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Notes) ;
         }

         set {
            sdt.gxTpr_Notes = (String)(value);
         }

      }

      [DataMember( Name = "created_on" , Order = 3 )]
      public String gxTpr_Created_on
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Created_on) ;
         }

         set {
            sdt.gxTpr_Created_on = (String)(value);
         }

      }

      [DataMember( Name = "private_notes" , Order = 4 )]
      public String gxTpr_Private_notes
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Private_notes) ;
         }

         set {
            sdt.gxTpr_Private_notes = (String)(value);
         }

      }

      [DataMember( Name = "details" , Order = 5 )]
      public SdtSDT_Redmineissue_journals_journal_details_RESTInterface gxTpr_Details
      {
         get {
            return new SdtSDT_Redmineissue_journals_journal_details_RESTInterface(sdt.gxTpr_Details) ;
         }

         set {
            sdt.gxTpr_Details = value.sdt;
         }

      }

      public SdtSDT_Redmineissue_journals_journal sdt
      {
         get {
            return (SdtSDT_Redmineissue_journals_journal)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Redmineissue_journals_journal() ;
         }
      }

   }

}
