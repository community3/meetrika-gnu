/*
               File: WC_SolicitacaoArtefatos
        Description: Solicitação de Artefatos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:6:23.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_solicitacaoartefatos : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_solicitacaoartefatos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_solicitacaoartefatos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         chkavCtlartefatos_obrigatorio = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A160ContratoServicos_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_5 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_5_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_5_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6sdt_Artefatos);
                  A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  A1751Artefatos_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1751Artefatos_Descricao", A1751Artefatos_Descricao);
                  A1749Artefatos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV5sdt_Artefato);
                  A1768ContratoServicosArtefato_Obrigatorio = (bool)(BooleanUtil.Val(GetNextPar( )));
                  n1768ContratoServicosArtefato_Obrigatorio = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1768ContratoServicosArtefato_Obrigatorio", A1768ContratoServicosArtefato_Obrigatorio);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( AV6sdt_Artefatos, A160ContratoServicos_Codigo, A1751Artefatos_Descricao, A1749Artefatos_Codigo, AV5sdt_Artefato, A1768ContratoServicosArtefato_Obrigatorio, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAN82( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavCtlartefatos_codigo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlartefatos_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_codigo_Enabled), 5, 0)));
               chkavCtlartefatos_obrigatorio.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavCtlartefatos_obrigatorio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavCtlartefatos_obrigatorio.Enabled), 5, 0)));
               edtavCtlartefatos_descricao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlartefatos_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_descricao_Enabled), 5, 0)));
               WSN82( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Solicitação de Artefatos") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311762315");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_solicitacaoartefatos.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"Sdt_artefatos", AV6sdt_Artefatos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"Sdt_artefatos", AV6sdt_Artefatos);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_5", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_5), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA160ContratoServicos_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSDT_ARTEFATOS", AV6sdt_Artefatos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSDT_ARTEFATOS", AV6sdt_Artefatos);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ARTEFATOS_DESCRICAO", A1751Artefatos_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"ARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1749Artefatos_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSDT_ARTEFATO", AV5sdt_Artefato);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSDT_ARTEFATO", AV5sdt_Artefato);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"CONTRATOSERVICOSARTEFATO_OBRIGATORIO", A1768ContratoServicosArtefato_Obrigatorio);
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormN82( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_solicitacaoartefatos.js", "?2020311762317");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_SolicitacaoArtefatos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Solicitação de Artefatos" ;
      }

      protected void WBN80( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_solicitacaoartefatos.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_N82( true) ;
         }
         else
         {
            wb_table1_2_N82( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_N82e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WC_SolicitacaoArtefatos.htm");
         }
         wbLoad = true;
      }

      protected void STARTN82( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Solicitação de Artefatos", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPN80( ) ;
            }
         }
      }

      protected void WSN82( )
      {
         STARTN82( ) ;
         EVTN82( ) ;
      }

      protected void EVTN82( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCOPIAR.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11N82 */
                                    E11N82 ();
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavCtlartefatos_codigo_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VDELETE.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VDELETE.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN80( ) ;
                              }
                              nGXsfl_5_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
                              SubsflControlProps_52( ) ;
                              AV13GXV1 = nGXsfl_5_idx;
                              if ( ( AV6sdt_Artefatos.Count >= AV13GXV1 ) && ( AV13GXV1 > 0 ) )
                              {
                                 AV6sdt_Artefatos.CurrentItem = ((SdtSDT_Artefatos)AV6sdt_Artefatos.Item(AV13GXV1));
                                 AV7Delete = cgiGet( edtavDelete_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV7Delete)) ? AV18Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV7Delete))));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCtlartefatos_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12N82 */
                                          E12N82 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCtlartefatos_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13N82 */
                                          E13N82 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCtlartefatos_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14N82 */
                                          E14N82 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VDELETE.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCtlartefatos_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15N82 */
                                          E15N82 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPN80( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavCtlartefatos_codigo_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEN82( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormN82( ) ;
            }
         }
      }

      protected void PAN82( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "CTLARTEFATOS_OBRIGATORIO_" + sGXsfl_5_idx;
            chkavCtlartefatos_obrigatorio.Name = GXCCtl;
            chkavCtlartefatos_obrigatorio.WebTags = "";
            chkavCtlartefatos_obrigatorio.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavCtlartefatos_obrigatorio_Internalname, "TitleCaption", chkavCtlartefatos_obrigatorio.Caption);
            chkavCtlartefatos_obrigatorio.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_52( ) ;
         while ( nGXsfl_5_idx <= nRC_GXsfl_5 )
         {
            sendrow_52( ) ;
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( IGxCollection AV6sdt_Artefatos ,
                                       int A160ContratoServicos_Codigo ,
                                       String A1751Artefatos_Descricao ,
                                       int A1749Artefatos_Codigo ,
                                       SdtSDT_Artefatos AV5sdt_Artefato ,
                                       bool A1768ContratoServicosArtefato_Obrigatorio ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFN82( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFN82( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlartefatos_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlartefatos_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_codigo_Enabled), 5, 0)));
         chkavCtlartefatos_obrigatorio.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavCtlartefatos_obrigatorio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavCtlartefatos_obrigatorio.Enabled), 5, 0)));
         edtavCtlartefatos_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlartefatos_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_descricao_Enabled), 5, 0)));
      }

      protected void RFN82( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 5;
         /* Execute user event: E13N82 */
         E13N82 ();
         nGXsfl_5_idx = 1;
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         nGXsfl_5_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "Grid");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_52( ) ;
            /* Execute user event: E14N82 */
            E14N82 ();
            wbEnd = 5;
            WBN80( ) ;
         }
         nGXsfl_5_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPN80( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtlartefatos_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlartefatos_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_codigo_Enabled), 5, 0)));
         chkavCtlartefatos_obrigatorio.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavCtlartefatos_obrigatorio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavCtlartefatos_obrigatorio.Enabled), 5, 0)));
         edtavCtlartefatos_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlartefatos_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlartefatos_descricao_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12N82 */
         E12N82 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"Sdt_artefatos"), AV6sdt_Artefatos);
            /* Read variables values. */
            AV9Copiar = cgiGet( imgavCopiar_Internalname);
            /* Read saved values. */
            nRC_GXsfl_5 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_5"), ",", "."));
            wcpOA160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA160ContratoServicos_Codigo"), ",", "."));
            nRC_GXsfl_5 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_5"), ",", "."));
            nGXsfl_5_fel_idx = 0;
            while ( nGXsfl_5_fel_idx < nRC_GXsfl_5 )
            {
               nGXsfl_5_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_fel_idx+1));
               sGXsfl_5_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_52( ) ;
               AV13GXV1 = nGXsfl_5_fel_idx;
               if ( ( AV6sdt_Artefatos.Count >= AV13GXV1 ) && ( AV13GXV1 > 0 ) )
               {
                  AV6sdt_Artefatos.CurrentItem = ((SdtSDT_Artefatos)AV6sdt_Artefatos.Item(AV13GXV1));
                  AV7Delete = cgiGet( edtavDelete_Internalname);
               }
            }
            if ( nGXsfl_5_fel_idx == 0 )
            {
               nGXsfl_5_idx = 1;
               sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
               SubsflControlProps_52( ) ;
            }
            nGXsfl_5_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12N82 */
         E12N82 ();
         if (returnInSub) return;
      }

      protected void E12N82( )
      {
         /* Start Routine */
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         AV6sdt_Artefatos.FromXml(AV8WebSession.Get("Artefatos"), "SDT_ArtefatosCollection");
         gx_BV5 = true;
      }

      protected void E13N82( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         if ( (0==AV6sdt_Artefatos.Count) )
         {
            /* Execute user subroutine: 'CARREGAARTEFATOS' */
            S112 ();
            if (returnInSub) return;
         }
         else
         {
            AV8WebSession.Set("Artefatos", AV6sdt_Artefatos.ToXml(false, true, "SDT_ArtefatosCollection", "GxEv3Up14_Meetrika"));
         }
         AV9Copiar = context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgavCopiar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV9Copiar)) ? AV17Copiar_GXI : context.convertURL( context.PathToRelativeUrl( AV9Copiar))));
         AV17Copiar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgavCopiar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV9Copiar)) ? AV17Copiar_GXI : context.convertURL( context.PathToRelativeUrl( AV9Copiar))));
         AV7Delete = context.GetImagePath( "28da6cce-b945-4cc5-8e39-a91759ac9224", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV7Delete)) ? AV18Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV7Delete))));
         AV18Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "28da6cce-b945-4cc5-8e39-a91759ac9224", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV7Delete)) ? AV18Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV7Delete))));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV5sdt_Artefato", AV5sdt_Artefato);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6sdt_Artefatos", AV6sdt_Artefatos);
      }

      private void E14N82( )
      {
         /* Load Routine */
         AV13GXV1 = 1;
         while ( AV13GXV1 <= AV6sdt_Artefatos.Count )
         {
            AV6sdt_Artefatos.CurrentItem = ((SdtSDT_Artefatos)AV6sdt_Artefatos.Item(AV13GXV1));
            if ( ((SdtSDT_Artefatos)(AV6sdt_Artefatos.CurrentItem)).gxTpr_Artefatos_obrigatorio )
            {
               edtavDelete_Visible = 0;
            }
            else
            {
               edtavDelete_Visible = 1;
            }
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 5;
            }
            sendrow_52( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(5, GridRow);
            }
            AV13GXV1 = (short)(AV13GXV1+1);
         }
      }

      protected void E15N82( )
      {
         AV13GXV1 = nGXsfl_5_idx;
         if ( AV6sdt_Artefatos.Count >= AV13GXV1 )
         {
            AV6sdt_Artefatos.CurrentItem = ((SdtSDT_Artefatos)AV6sdt_Artefatos.Item(AV13GXV1));
         }
         /* Delete_Click Routine */
         AV6sdt_Artefatos.RemoveItem(AV6sdt_Artefatos.IndexOf(((SdtSDT_Artefatos)(AV6sdt_Artefatos.CurrentItem))));
         gx_BV5 = true;
         bttBtnfechar_Caption = "Confirmar";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnfechar_Internalname, "Caption", bttBtnfechar_Caption);
         context.DoAjaxRefreshCmp(sPrefix);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6sdt_Artefatos", AV6sdt_Artefatos);
         nGXsfl_5_bak_idx = nGXsfl_5_idx;
         gxgrGrid_refresh( AV6sdt_Artefatos, A160ContratoServicos_Codigo, A1751Artefatos_Descricao, A1749Artefatos_Codigo, AV5sdt_Artefato, A1768ContratoServicosArtefato_Obrigatorio, sPrefix) ;
         nGXsfl_5_idx = nGXsfl_5_bak_idx;
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
      }

      protected void E11N82( )
      {
         AV13GXV1 = nGXsfl_5_idx;
         if ( AV6sdt_Artefatos.Count >= AV13GXV1 )
         {
            AV6sdt_Artefatos.CurrentItem = ((SdtSDT_Artefatos)AV6sdt_Artefatos.Item(AV13GXV1));
         }
         /* Copiar_Click Routine */
         AV6sdt_Artefatos.Clear();
         gx_BV5 = true;
         /* Execute user subroutine: 'CARREGAARTEFATOS' */
         S112 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6sdt_Artefatos", AV6sdt_Artefatos);
         nGXsfl_5_bak_idx = nGXsfl_5_idx;
         gxgrGrid_refresh( AV6sdt_Artefatos, A160ContratoServicos_Codigo, A1751Artefatos_Descricao, A1749Artefatos_Codigo, AV5sdt_Artefato, A1768ContratoServicosArtefato_Obrigatorio, sPrefix) ;
         nGXsfl_5_idx = nGXsfl_5_bak_idx;
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV5sdt_Artefato", AV5sdt_Artefato);
      }

      protected void S112( )
      {
         /* 'CARREGAARTEFATOS' Routine */
         /* Using cursor H00N82 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1749Artefatos_Codigo = H00N82_A1749Artefatos_Codigo[0];
            A1768ContratoServicosArtefato_Obrigatorio = H00N82_A1768ContratoServicosArtefato_Obrigatorio[0];
            n1768ContratoServicosArtefato_Obrigatorio = H00N82_n1768ContratoServicosArtefato_Obrigatorio[0];
            A1751Artefatos_Descricao = H00N82_A1751Artefatos_Descricao[0];
            A1751Artefatos_Descricao = H00N82_A1751Artefatos_Descricao[0];
            AV5sdt_Artefato.gxTpr_Artefatos_codigo = A1749Artefatos_Codigo;
            AV5sdt_Artefato.gxTpr_Artefatos_descricao = A1751Artefatos_Descricao;
            AV5sdt_Artefato.gxTpr_Artefatos_obrigatorio = A1768ContratoServicosArtefato_Obrigatorio;
            AV6sdt_Artefatos.Add(AV5sdt_Artefato, 0);
            gx_BV5 = true;
            AV5sdt_Artefato = new SdtSDT_Artefatos(context);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV6sdt_Artefatos.Sort("Artefatos_Obrigatorio,Artefatos_Descricao");
         gx_BV5 = true;
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void wb_table1_2_N82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "center", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"5\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Código") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(30), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(18), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Descrição") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "Grid");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlartefatos_codigo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkavCtlartefatos_obrigatorio.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV7Delete));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlartefatos_descricao_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 5 )
         {
            wbEnd = 0;
            nRC_GXsfl_5 = (short)(nGXsfl_5_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV13GXV1 = nGXsfl_5_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(5), 1, 0)+","+"null"+");", bttBtnfechar_Caption, bttBtnfechar_Jsonclick, 1, "Voltar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WC_SolicitacaoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active Bitmap Variable */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            AV9Copiar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV9Copiar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV17Copiar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV9Copiar)));
            GxWebStd.gx_bitmap( context, imgavCopiar_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV9Copiar)) ? AV17Copiar_GXI : context.PathToRelativeUrl( AV9Copiar)), "", "", "", context.GetTheme( ), 1, 1, "", "", 0, -1, 0, "", 0, "", 0, 0, 5, imgavCopiar_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVCOPIAR.CLICK."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, AV9Copiar_IsBlob, false, "HLP_WC_SolicitacaoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_N82e( true) ;
         }
         else
         {
            wb_table1_2_N82e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A160ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAN82( ) ;
         WSN82( ) ;
         WEN82( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA160ContratoServicos_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAN82( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_solicitacaoartefatos");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAN82( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A160ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         }
         wcpOA160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA160ContratoServicos_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A160ContratoServicos_Codigo != wcpOA160ContratoServicos_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA160ContratoServicos_Codigo = cgiGet( sPrefix+"A160ContratoServicos_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA160ContratoServicos_Codigo) > 0 )
         {
            A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA160ContratoServicos_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         }
         else
         {
            A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A160ContratoServicos_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAN82( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSN82( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSN82( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A160ContratoServicos_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA160ContratoServicos_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A160ContratoServicos_Codigo_CTRL", StringUtil.RTrim( sCtrlA160ContratoServicos_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEN82( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311762350");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("wc_solicitacaoartefatos.js", "?2020311762350");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_52( )
      {
         edtavCtlartefatos_codigo_Internalname = sPrefix+"CTLARTEFATOS_CODIGO_"+sGXsfl_5_idx;
         chkavCtlartefatos_obrigatorio_Internalname = sPrefix+"CTLARTEFATOS_OBRIGATORIO_"+sGXsfl_5_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_5_idx;
         edtavCtlartefatos_descricao_Internalname = sPrefix+"CTLARTEFATOS_DESCRICAO_"+sGXsfl_5_idx;
      }

      protected void SubsflControlProps_fel_52( )
      {
         edtavCtlartefatos_codigo_Internalname = sPrefix+"CTLARTEFATOS_CODIGO_"+sGXsfl_5_fel_idx;
         chkavCtlartefatos_obrigatorio_Internalname = sPrefix+"CTLARTEFATOS_OBRIGATORIO_"+sGXsfl_5_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_5_fel_idx;
         edtavCtlartefatos_descricao_Internalname = sPrefix+"CTLARTEFATOS_DESCRICAO_"+sGXsfl_5_fel_idx;
      }

      protected void sendrow_52( )
      {
         SubsflControlProps_52( ) ;
         WBN80( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xF0F0F0);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_5_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xE5E5E5);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xF0F0F0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_5_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlartefatos_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Artefatos)AV6sdt_Artefatos.Item(AV13GXV1)).gxTpr_Artefatos_codigo), 6, 0, ",", "")),((edtavCtlartefatos_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Artefatos)AV6sdt_Artefatos.Item(AV13GXV1)).gxTpr_Artefatos_codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_Artefatos)AV6sdt_Artefatos.Item(AV13GXV1)).gxTpr_Artefatos_codigo), "ZZZZZ9")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlartefatos_codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCtlartefatos_codigo_Enabled,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Check box */
         ClassString = "Attribute";
         StyleString = "";
         GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavCtlartefatos_obrigatorio_Internalname,StringUtil.BoolToStr( ((SdtSDT_Artefatos)AV6sdt_Artefatos.Item(AV13GXV1)).gxTpr_Artefatos_obrigatorio),(String)"",(String)"",(short)0,chkavCtlartefatos_obrigatorio.Enabled,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavDelete_Enabled!=0)&&(edtavDelete_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 8,'"+sPrefix+"',false,'',5)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV7Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV7Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV18Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV7Delete)));
         GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV7Delete)) ? AV18Delete_GXI : context.PathToRelativeUrl( AV7Delete)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)"",(short)0,(short)1,(short)18,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavDelete_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDELETE.CLICK."+sGXsfl_5_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV7Delete_IsBlob,(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlartefatos_descricao_Internalname,((SdtSDT_Artefatos)AV6sdt_Artefatos.Item(AV13GXV1)).gxTpr_Artefatos_descricao,StringUtil.RTrim( context.localUtil.Format( ((SdtSDT_Artefatos)AV6sdt_Artefatos.Item(AV13GXV1)).gxTpr_Artefatos_descricao, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlartefatos_descricao_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlartefatos_descricao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"left",(bool)true});
         GridContainer.AddRow(GridRow);
         nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         /* End function sendrow_52 */
      }

      protected void init_default_properties( )
      {
         edtavCtlartefatos_codigo_Internalname = sPrefix+"CTLARTEFATOS_CODIGO";
         chkavCtlartefatos_obrigatorio_Internalname = sPrefix+"CTLARTEFATOS_OBRIGATORIO";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavCtlartefatos_descricao_Internalname = sPrefix+"CTLARTEFATOS_DESCRICAO";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         imgavCopiar_Internalname = sPrefix+"vCOPIAR";
         tblTable1_Internalname = sPrefix+"TABLE1";
         lblTbjava_Internalname = sPrefix+"TBJAVA";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavCtlartefatos_descricao_Jsonclick = "";
         edtavDelete_Jsonclick = "";
         edtavDelete_Enabled = 1;
         edtavCtlartefatos_codigo_Jsonclick = "";
         imgavCopiar_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavCtlartefatos_descricao_Enabled = 0;
         chkavCtlartefatos_obrigatorio.Enabled = 0;
         edtavCtlartefatos_codigo_Enabled = 0;
         edtavDelete_Visible = -1;
         subGrid_Class = "Grid";
         bttBtnfechar_Caption = "Voltar";
         subGrid_Backcolorstyle = 3;
         chkavCtlartefatos_obrigatorio.Caption = "";
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         edtavCtlartefatos_descricao_Enabled = -1;
         chkavCtlartefatos_obrigatorio.Enabled = -1;
         edtavCtlartefatos_codigo_Enabled = -1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'sPrefix',nv:''},{av:'AV6sdt_Artefatos',fld:'vSDT_ARTEFATOS',grid:5,pic:'',nv:null},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5sdt_Artefato',fld:'vSDT_ARTEFATO',pic:'',nv:null},{av:'A1768ContratoServicosArtefato_Obrigatorio',fld:'CONTRATOSERVICOSARTEFATO_OBRIGATORIO',pic:'',nv:false}],oparms:[{av:'AV9Copiar',fld:'vCOPIAR',pic:'',nv:''},{av:'AV7Delete',fld:'vDELETE',pic:'',nv:''},{av:'AV5sdt_Artefato',fld:'vSDT_ARTEFATO',pic:'',nv:null},{av:'AV6sdt_Artefatos',fld:'vSDT_ARTEFATOS',grid:5,pic:'',nv:null}]}");
         setEventMetadata("VDELETE.CLICK","{handler:'E15N82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV6sdt_Artefatos',fld:'vSDT_ARTEFATOS',grid:5,pic:'',nv:null},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5sdt_Artefato',fld:'vSDT_ARTEFATO',pic:'',nv:null},{av:'A1768ContratoServicosArtefato_Obrigatorio',fld:'CONTRATOSERVICOSARTEFATO_OBRIGATORIO',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[{av:'AV6sdt_Artefatos',fld:'vSDT_ARTEFATOS',grid:5,pic:'',nv:null},{ctrl:'BTNFECHAR',prop:'Caption'}]}");
         setEventMetadata("VCOPIAR.CLICK","{handler:'E11N82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV6sdt_Artefatos',fld:'vSDT_ARTEFATOS',grid:5,pic:'',nv:null},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5sdt_Artefato',fld:'vSDT_ARTEFATO',pic:'',nv:null},{av:'A1768ContratoServicosArtefato_Obrigatorio',fld:'CONTRATOSERVICOSARTEFATO_OBRIGATORIO',pic:'',nv:false},{av:'sPrefix',nv:''}],oparms:[{av:'AV6sdt_Artefatos',fld:'vSDT_ARTEFATOS',grid:5,pic:'',nv:null},{av:'AV5sdt_Artefato',fld:'vSDT_ARTEFATO',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV6sdt_Artefatos = new GxObjectCollection( context, "SDT_Artefatos", "GxEv3Up14_Meetrika", "SdtSDT_Artefatos", "GeneXus.Programs");
         A1751Artefatos_Descricao = "";
         AV5sdt_Artefato = new SdtSDT_Artefatos(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         lblTbjava_Jsonclick = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV7Delete = "";
         AV18Delete_GXI = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV9Copiar = "";
         AV8WebSession = context.GetSession();
         AV17Copiar_GXI = "";
         GridRow = new GXWebRow();
         scmdbuf = "";
         H00N82_A160ContratoServicos_Codigo = new int[1] ;
         H00N82_A1749Artefatos_Codigo = new int[1] ;
         H00N82_A1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         H00N82_n1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         H00N82_A1751Artefatos_Descricao = new String[] {""} ;
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnfechar_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA160ContratoServicos_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_solicitacaoartefatos__default(),
            new Object[][] {
                new Object[] {
               H00N82_A160ContratoServicos_Codigo, H00N82_A1749Artefatos_Codigo, H00N82_A1768ContratoServicosArtefato_Obrigatorio, H00N82_n1768ContratoServicosArtefato_Obrigatorio, H00N82_A1751Artefatos_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlartefatos_codigo_Enabled = 0;
         chkavCtlartefatos_obrigatorio.Enabled = 0;
         edtavCtlartefatos_descricao_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_5 ;
      private short nGXsfl_5_idx=1 ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short AV13GXV1 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_5_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short nGXsfl_5_fel_idx=1 ;
      private short GRID_nEOF ;
      private short nGXsfl_5_bak_idx=1 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int A160ContratoServicos_Codigo ;
      private int wcpOA160ContratoServicos_Codigo ;
      private int A1749Artefatos_Codigo ;
      private int edtavCtlartefatos_codigo_Enabled ;
      private int edtavCtlartefatos_descricao_Enabled ;
      private int lblTbjava_Visible ;
      private int subGrid_Islastpage ;
      private int edtavDelete_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavDelete_Enabled ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_5_idx="0001" ;
      private String GXKey ;
      private String edtavCtlartefatos_codigo_Internalname ;
      private String chkavCtlartefatos_obrigatorio_Internalname ;
      private String edtavCtlartefatos_descricao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavDelete_Internalname ;
      private String GXCCtl ;
      private String imgavCopiar_Internalname ;
      private String sGXsfl_5_fel_idx="0001" ;
      private String bttBtnfechar_Caption ;
      private String bttBtnfechar_Internalname ;
      private String scmdbuf ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnfechar_Jsonclick ;
      private String imgavCopiar_Jsonclick ;
      private String sCtrlA160ContratoServicos_Codigo ;
      private String ROClassString ;
      private String edtavCtlartefatos_codigo_Jsonclick ;
      private String edtavDelete_Jsonclick ;
      private String edtavCtlartefatos_descricao_Jsonclick ;
      private bool entryPointCalled ;
      private bool A1768ContratoServicosArtefato_Obrigatorio ;
      private bool n1768ContratoServicosArtefato_Obrigatorio ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_BV5 ;
      private bool gx_refresh_fired ;
      private bool AV9Copiar_IsBlob ;
      private bool AV7Delete_IsBlob ;
      private String A1751Artefatos_Descricao ;
      private String AV18Delete_GXI ;
      private String AV17Copiar_GXI ;
      private String AV7Delete ;
      private String AV9Copiar ;
      private IGxSession AV8WebSession ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private GXCheckbox chkavCtlartefatos_obrigatorio ;
      private IDataStoreProvider pr_default ;
      private int[] H00N82_A160ContratoServicos_Codigo ;
      private int[] H00N82_A1749Artefatos_Codigo ;
      private bool[] H00N82_A1768ContratoServicosArtefato_Obrigatorio ;
      private bool[] H00N82_n1768ContratoServicosArtefato_Obrigatorio ;
      private String[] H00N82_A1751Artefatos_Descricao ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Artefatos ))]
      private IGxCollection AV6sdt_Artefatos ;
      private SdtSDT_Artefatos AV5sdt_Artefato ;
   }

   public class wc_solicitacaoartefatos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00N82 ;
          prmH00N82 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00N82", "SELECT T1.[ContratoServicos_Codigo], T1.[Artefatos_Codigo], T1.[ContratoServicosArtefato_Obrigatorio], T2.[Artefatos_Descricao] FROM ([ContratoServicosArtefatos] T1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = T1.[Artefatos_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo], T2.[Artefatos_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N82,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
