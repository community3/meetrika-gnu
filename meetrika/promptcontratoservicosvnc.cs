/*
               File: PromptContratoServicosVnc
        Description: Selecione Regra de Vinculo entre Servi�os
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:44:11.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratoservicosvnc : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratoservicosvnc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratoservicosvnc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratoSrvVnc_Codigo )
      {
         this.AV7InOutContratoSrvVnc_Codigo = aP0_InOutContratoSrvVnc_Codigo;
         executePrivate();
         aP0_InOutContratoSrvVnc_Codigo=this.AV7InOutContratoSrvVnc_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbContratoSrvVnc_StatusDmn = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_27 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_27_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_27_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV8OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0)));
               AV9OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedDsc", AV9OrderedDsc);
               AV14TFContratoSrvVnc_ServicoSigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TFContratoSrvVnc_ServicoSigla", AV14TFContratoSrvVnc_ServicoSigla);
               AV15TFContratoSrvVnc_ServicoSigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15TFContratoSrvVnc_ServicoSigla_Sel", AV15TFContratoSrvVnc_ServicoSigla_Sel);
               AV22TFContratoSrvVnc_ServicoVncSigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFContratoSrvVnc_ServicoVncSigla", AV22TFContratoSrvVnc_ServicoVncSigla);
               AV23TFContratoSrvVnc_ServicoVncSigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TFContratoSrvVnc_ServicoVncSigla_Sel", AV23TFContratoSrvVnc_ServicoVncSigla_Sel);
               AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace", AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace);
               AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace", AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace);
               AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace", AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV19TFContratoSrvVnc_StatusDmn_Sels);
               AV32Pgmname = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV8OrderedBy, AV9OrderedDsc, AV14TFContratoSrvVnc_ServicoSigla, AV15TFContratoSrvVnc_ServicoSigla_Sel, AV22TFContratoSrvVnc_ServicoVncSigla, AV23TFContratoSrvVnc_ServicoVncSigla_Sel, AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV19TFContratoSrvVnc_StatusDmn_Sels, AV32Pgmname) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratoSrvVnc_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoSrvVnc_Codigo), 6, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAGD2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV32Pgmname = "PromptContratoServicosVnc";
               context.Gx_err = 0;
               WSGD2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEGD2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120441126");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratoservicosvnc.aspx") + "?" + UrlEncode("" +AV7InOutContratoSrvVnc_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV9OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSRVVNC_SERVICOSIGLA", StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL", StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoSigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA", StringUtil.RTrim( AV22TFContratoSrvVnc_ServicoVncSigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL", StringUtil.RTrim( AV23TFContratoSrvVnc_ServicoVncSigla_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_27", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_27), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV25DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV25DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSRVVNC_SERVICOSIGLATITLEFILTERDATA", AV13ContratoSrvVnc_ServicoSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSRVVNC_SERVICOSIGLATITLEFILTERDATA", AV13ContratoSrvVnc_ServicoSiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSRVVNC_STATUSDMNTITLEFILTERDATA", AV17ContratoSrvVnc_StatusDmnTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSRVVNC_STATUSDMNTITLEFILTERDATA", AV17ContratoSrvVnc_StatusDmnTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSRVVNC_SERVICOVNCSIGLATITLEFILTERDATA", AV21ContratoSrvVnc_ServicoVncSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSRVVNC_SERVICOVNCSIGLATITLEFILTERDATA", AV21ContratoSrvVnc_ServicoVncSiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFCONTRATOSRVVNC_STATUSDMN_SELS", AV19TFContratoSrvVnc_StatusDmn_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFCONTRATOSRVVNC_STATUSDMN_SELS", AV19TFContratoSrvVnc_StatusDmn_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV32Pgmname));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOSRVVNC_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratoSrvVnc_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicosigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicosigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicosigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filtertype", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicosigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicosigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Datalistproc", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratosrvvnc_servicosigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Loadingdata", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Allowmultipleselection", StringUtil.BoolToStr( Ddo_contratosrvvnc_statusdmn_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Caption", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Tooltip", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Cls", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortedstatus", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filtertype", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_contratosrvvnc_servicovncsigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalisttype", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalistproc", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratosrvvnc_servicovncsigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortasc", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortdsc", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Loadingdata", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_servicosigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_STATUSDMN_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_statusdmn_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormGD2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratoServicosVnc" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Regra de Vinculo entre Servi�os" ;
      }

      protected void WBGD0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_GD2( true) ;
         }
         else
         {
            wb_table1_2_GD2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GD2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_servicosigla_Internalname, StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( AV14TFContratoSrvVnc_ServicoSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_servicosigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_servicosigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_servicosigla_sel_Internalname, StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV15TFContratoSrvVnc_ServicoSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_servicosigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_servicosigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_servicovncsigla_Internalname, StringUtil.RTrim( AV22TFContratoSrvVnc_ServicoVncSigla), StringUtil.RTrim( context.localUtil.Format( AV22TFContratoSrvVnc_ServicoVncSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_servicovncsigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_servicovncsigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosVnc.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname, StringUtil.RTrim( AV23TFContratoSrvVnc_ServicoVncSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV23TFContratoSrvVnc_ServicoVncSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratosrvvnc_servicovncsigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratosrvvnc_servicovncsigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoServicosVnc.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSRVVNC_SERVICOSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Internalname, AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 0, edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosVnc.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSRVVNC_STATUSDMNContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname, AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", 0, edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosVnc.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSRVVNC_SERVICOVNCSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname, AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", 0, edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosVnc.htm");
         }
         wbLoad = true;
      }

      protected void STARTGD2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Regra de Vinculo entre Servi�os", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPGD0( ) ;
      }

      protected void WSGD2( )
      {
         STARTGD2( ) ;
         EVTGD2( ) ;
      }

      protected void EVTGD2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11GD2 */
                           E11GD2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_SERVICOSIGLA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12GD2 */
                           E12GD2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_STATUSDMN.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13GD2 */
                           E13GD2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14GD2 */
                           E14GD2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15GD2 */
                           E15GD2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16GD2 */
                           E16GD2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_27_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
                           SubsflControlProps_272( ) ;
                           AV10Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV10Select)) ? AV31Select_GXI : context.convertURL( context.PathToRelativeUrl( AV10Select))));
                           A917ContratoSrvVnc_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoSrvVnc_Codigo_Internalname), ",", "."));
                           A922ContratoSrvVnc_ServicoSigla = StringUtil.Upper( cgiGet( edtContratoSrvVnc_ServicoSigla_Internalname));
                           n922ContratoSrvVnc_ServicoSigla = false;
                           cmbContratoSrvVnc_StatusDmn.Name = cmbContratoSrvVnc_StatusDmn_Internalname;
                           cmbContratoSrvVnc_StatusDmn.CurrentValue = cgiGet( cmbContratoSrvVnc_StatusDmn_Internalname);
                           A1084ContratoSrvVnc_StatusDmn = cgiGet( cmbContratoSrvVnc_StatusDmn_Internalname);
                           n1084ContratoSrvVnc_StatusDmn = false;
                           A924ContratoSrvVnc_ServicoVncSigla = StringUtil.Upper( cgiGet( edtContratoSrvVnc_ServicoVncSigla_Internalname));
                           n924ContratoSrvVnc_ServicoVncSigla = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E17GD2 */
                                 E17GD2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E18GD2 */
                                 E18GD2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E19GD2 */
                                 E19GD2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV8OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV9OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratosrvvnc_servicosigla Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOSIGLA"), AV14TFContratoSrvVnc_ServicoSigla) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratosrvvnc_servicosigla_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL"), AV15TFContratoSrvVnc_ServicoSigla_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratosrvvnc_servicovncsigla Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA"), AV22TFContratoSrvVnc_ServicoVncSigla) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratosrvvnc_servicovncsigla_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL"), AV23TFContratoSrvVnc_ServicoVncSigla_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E20GD2 */
                                       E20GD2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEGD2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormGD2( ) ;
            }
         }
      }

      protected void PAGD2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV8OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0)));
            }
            GXCCtl = "CONTRATOSRVVNC_STATUSDMN_" + sGXsfl_27_idx;
            cmbContratoSrvVnc_StatusDmn.Name = GXCCtl;
            cmbContratoSrvVnc_StatusDmn.WebTags = "";
            cmbContratoSrvVnc_StatusDmn.addItem("B", "Stand by", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("S", "Solicitada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("E", "Em An�lise", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("A", "Em execu��o", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("R", "Resolvida", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("C", "Conferida", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("D", "Rejeitada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("H", "Homologada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("O", "Aceite", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("P", "A Pagar", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("L", "Liquidada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("X", "Cancelada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("J", "Planejamento", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContratoSrvVnc_StatusDmn.ItemCount > 0 )
            {
               A1084ContratoSrvVnc_StatusDmn = cmbContratoSrvVnc_StatusDmn.getValidValue(A1084ContratoSrvVnc_StatusDmn);
               n1084ContratoSrvVnc_StatusDmn = false;
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_272( ) ;
         while ( nGXsfl_27_idx <= nRC_GXsfl_27 )
         {
            sendrow_272( ) ;
            nGXsfl_27_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_27_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_27_idx+1));
            sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
            SubsflControlProps_272( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV8OrderedBy ,
                                       bool AV9OrderedDsc ,
                                       String AV14TFContratoSrvVnc_ServicoSigla ,
                                       String AV15TFContratoSrvVnc_ServicoSigla_Sel ,
                                       String AV22TFContratoSrvVnc_ServicoVncSigla ,
                                       String AV23TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                       String AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace ,
                                       String AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace ,
                                       String AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace ,
                                       IGxCollection AV19TFContratoSrvVnc_StatusDmn_Sels ,
                                       String AV32Pgmname )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFGD2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSRVVNC_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSRVVNC_STATUSDMN", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1084ContratoSrvVnc_StatusDmn, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATOSRVVNC_STATUSDMN", StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV8OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGD2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV32Pgmname = "PromptContratoServicosVnc";
         context.Gx_err = 0;
      }

      protected void RFGD2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 27;
         /* Execute user event: E18GD2 */
         E18GD2 ();
         nGXsfl_27_idx = 1;
         sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
         SubsflControlProps_272( ) ;
         nGXsfl_27_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_272( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A1084ContratoSrvVnc_StatusDmn ,
                                                 AV19TFContratoSrvVnc_StatusDmn_Sels ,
                                                 AV15TFContratoSrvVnc_ServicoSigla_Sel ,
                                                 AV14TFContratoSrvVnc_ServicoSigla ,
                                                 AV19TFContratoSrvVnc_StatusDmn_Sels.Count ,
                                                 AV23TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                                 AV22TFContratoSrvVnc_ServicoVncSigla ,
                                                 A922ContratoSrvVnc_ServicoSigla ,
                                                 A924ContratoSrvVnc_ServicoVncSigla ,
                                                 AV8OrderedBy ,
                                                 AV9OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV14TFContratoSrvVnc_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoSigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TFContratoSrvVnc_ServicoSigla", AV14TFContratoSrvVnc_ServicoSigla);
            lV22TFContratoSrvVnc_ServicoVncSigla = StringUtil.PadR( StringUtil.RTrim( AV22TFContratoSrvVnc_ServicoVncSigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFContratoSrvVnc_ServicoVncSigla", AV22TFContratoSrvVnc_ServicoVncSigla);
            /* Using cursor H00GD2 */
            pr_default.execute(0, new Object[] {lV14TFContratoSrvVnc_ServicoSigla, AV15TFContratoSrvVnc_ServicoSigla_Sel, lV22TFContratoSrvVnc_ServicoVncSigla, AV23TFContratoSrvVnc_ServicoVncSigla_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_27_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A915ContratoSrvVnc_CntSrvCod = H00GD2_A915ContratoSrvVnc_CntSrvCod[0];
               A921ContratoSrvVnc_ServicoCod = H00GD2_A921ContratoSrvVnc_ServicoCod[0];
               n921ContratoSrvVnc_ServicoCod = H00GD2_n921ContratoSrvVnc_ServicoCod[0];
               A1589ContratoSrvVnc_SrvVncCntSrvCod = H00GD2_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               n1589ContratoSrvVnc_SrvVncCntSrvCod = H00GD2_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
               A923ContratoSrvVnc_ServicoVncCod = H00GD2_A923ContratoSrvVnc_ServicoVncCod[0];
               n923ContratoSrvVnc_ServicoVncCod = H00GD2_n923ContratoSrvVnc_ServicoVncCod[0];
               A924ContratoSrvVnc_ServicoVncSigla = H00GD2_A924ContratoSrvVnc_ServicoVncSigla[0];
               n924ContratoSrvVnc_ServicoVncSigla = H00GD2_n924ContratoSrvVnc_ServicoVncSigla[0];
               A1084ContratoSrvVnc_StatusDmn = H00GD2_A1084ContratoSrvVnc_StatusDmn[0];
               n1084ContratoSrvVnc_StatusDmn = H00GD2_n1084ContratoSrvVnc_StatusDmn[0];
               A922ContratoSrvVnc_ServicoSigla = H00GD2_A922ContratoSrvVnc_ServicoSigla[0];
               n922ContratoSrvVnc_ServicoSigla = H00GD2_n922ContratoSrvVnc_ServicoSigla[0];
               A917ContratoSrvVnc_Codigo = H00GD2_A917ContratoSrvVnc_Codigo[0];
               A921ContratoSrvVnc_ServicoCod = H00GD2_A921ContratoSrvVnc_ServicoCod[0];
               n921ContratoSrvVnc_ServicoCod = H00GD2_n921ContratoSrvVnc_ServicoCod[0];
               A922ContratoSrvVnc_ServicoSigla = H00GD2_A922ContratoSrvVnc_ServicoSigla[0];
               n922ContratoSrvVnc_ServicoSigla = H00GD2_n922ContratoSrvVnc_ServicoSigla[0];
               A923ContratoSrvVnc_ServicoVncCod = H00GD2_A923ContratoSrvVnc_ServicoVncCod[0];
               n923ContratoSrvVnc_ServicoVncCod = H00GD2_n923ContratoSrvVnc_ServicoVncCod[0];
               A924ContratoSrvVnc_ServicoVncSigla = H00GD2_A924ContratoSrvVnc_ServicoVncSigla[0];
               n924ContratoSrvVnc_ServicoVncSigla = H00GD2_n924ContratoSrvVnc_ServicoVncSigla[0];
               /* Execute user event: E19GD2 */
               E19GD2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 27;
            WBGD0( ) ;
         }
         nGXsfl_27_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1084ContratoSrvVnc_StatusDmn ,
                                              AV19TFContratoSrvVnc_StatusDmn_Sels ,
                                              AV15TFContratoSrvVnc_ServicoSigla_Sel ,
                                              AV14TFContratoSrvVnc_ServicoSigla ,
                                              AV19TFContratoSrvVnc_StatusDmn_Sels.Count ,
                                              AV23TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                              AV22TFContratoSrvVnc_ServicoVncSigla ,
                                              A922ContratoSrvVnc_ServicoSigla ,
                                              A924ContratoSrvVnc_ServicoVncSigla ,
                                              AV8OrderedBy ,
                                              AV9OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV14TFContratoSrvVnc_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoSigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TFContratoSrvVnc_ServicoSigla", AV14TFContratoSrvVnc_ServicoSigla);
         lV22TFContratoSrvVnc_ServicoVncSigla = StringUtil.PadR( StringUtil.RTrim( AV22TFContratoSrvVnc_ServicoVncSigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFContratoSrvVnc_ServicoVncSigla", AV22TFContratoSrvVnc_ServicoVncSigla);
         /* Using cursor H00GD3 */
         pr_default.execute(1, new Object[] {lV14TFContratoSrvVnc_ServicoSigla, AV15TFContratoSrvVnc_ServicoSigla_Sel, lV22TFContratoSrvVnc_ServicoVncSigla, AV23TFContratoSrvVnc_ServicoVncSigla_Sel});
         GRID_nRecordCount = H00GD3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV8OrderedBy, AV9OrderedDsc, AV14TFContratoSrvVnc_ServicoSigla, AV15TFContratoSrvVnc_ServicoSigla_Sel, AV22TFContratoSrvVnc_ServicoVncSigla, AV23TFContratoSrvVnc_ServicoVncSigla_Sel, AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV19TFContratoSrvVnc_StatusDmn_Sels, AV32Pgmname) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV8OrderedBy, AV9OrderedDsc, AV14TFContratoSrvVnc_ServicoSigla, AV15TFContratoSrvVnc_ServicoSigla_Sel, AV22TFContratoSrvVnc_ServicoVncSigla, AV23TFContratoSrvVnc_ServicoVncSigla_Sel, AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV19TFContratoSrvVnc_StatusDmn_Sels, AV32Pgmname) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV8OrderedBy, AV9OrderedDsc, AV14TFContratoSrvVnc_ServicoSigla, AV15TFContratoSrvVnc_ServicoSigla_Sel, AV22TFContratoSrvVnc_ServicoVncSigla, AV23TFContratoSrvVnc_ServicoVncSigla_Sel, AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV19TFContratoSrvVnc_StatusDmn_Sels, AV32Pgmname) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV8OrderedBy, AV9OrderedDsc, AV14TFContratoSrvVnc_ServicoSigla, AV15TFContratoSrvVnc_ServicoSigla_Sel, AV22TFContratoSrvVnc_ServicoVncSigla, AV23TFContratoSrvVnc_ServicoVncSigla_Sel, AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV19TFContratoSrvVnc_StatusDmn_Sels, AV32Pgmname) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV8OrderedBy, AV9OrderedDsc, AV14TFContratoSrvVnc_ServicoSigla, AV15TFContratoSrvVnc_ServicoSigla_Sel, AV22TFContratoSrvVnc_ServicoVncSigla, AV23TFContratoSrvVnc_ServicoVncSigla_Sel, AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, AV19TFContratoSrvVnc_StatusDmn_Sels, AV32Pgmname) ;
         }
         return (int)(0) ;
      }

      protected void STRUPGD0( )
      {
         /* Before Start, stand alone formulas. */
         AV32Pgmname = "PromptContratoServicosVnc";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E17GD2 */
         E17GD2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV25DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSRVVNC_SERVICOSIGLATITLEFILTERDATA"), AV13ContratoSrvVnc_ServicoSiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSRVVNC_STATUSDMNTITLEFILTERDATA"), AV17ContratoSrvVnc_StatusDmnTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSRVVNC_SERVICOVNCSIGLATITLEFILTERDATA"), AV21ContratoSrvVnc_ServicoVncSiglaTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV8OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0)));
            AV9OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedDsc", AV9OrderedDsc);
            AV14TFContratoSrvVnc_ServicoSigla = StringUtil.Upper( cgiGet( edtavTfcontratosrvvnc_servicosigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TFContratoSrvVnc_ServicoSigla", AV14TFContratoSrvVnc_ServicoSigla);
            AV15TFContratoSrvVnc_ServicoSigla_Sel = StringUtil.Upper( cgiGet( edtavTfcontratosrvvnc_servicosigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15TFContratoSrvVnc_ServicoSigla_Sel", AV15TFContratoSrvVnc_ServicoSigla_Sel);
            AV22TFContratoSrvVnc_ServicoVncSigla = StringUtil.Upper( cgiGet( edtavTfcontratosrvvnc_servicovncsigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFContratoSrvVnc_ServicoVncSigla", AV22TFContratoSrvVnc_ServicoVncSigla);
            AV23TFContratoSrvVnc_ServicoVncSigla_Sel = StringUtil.Upper( cgiGet( edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TFContratoSrvVnc_ServicoVncSigla_Sel", AV23TFContratoSrvVnc_ServicoVncSigla_Sel);
            AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace", AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace);
            AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace", AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace);
            AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace = cgiGet( edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace", AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_27 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_27"), ",", "."));
            AV27GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV28GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratosrvvnc_servicosigla_Caption = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Caption");
            Ddo_contratosrvvnc_servicosigla_Tooltip = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Tooltip");
            Ddo_contratosrvvnc_servicosigla_Cls = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Cls");
            Ddo_contratosrvvnc_servicosigla_Filteredtext_set = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filteredtext_set");
            Ddo_contratosrvvnc_servicosigla_Selectedvalue_set = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Selectedvalue_set");
            Ddo_contratosrvvnc_servicosigla_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Dropdownoptionstype");
            Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_servicosigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includesortasc"));
            Ddo_contratosrvvnc_servicosigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includesortdsc"));
            Ddo_contratosrvvnc_servicosigla_Sortedstatus = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Sortedstatus");
            Ddo_contratosrvvnc_servicosigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includefilter"));
            Ddo_contratosrvvnc_servicosigla_Filtertype = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filtertype");
            Ddo_contratosrvvnc_servicosigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filterisrange"));
            Ddo_contratosrvvnc_servicosigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Includedatalist"));
            Ddo_contratosrvvnc_servicosigla_Datalisttype = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Datalisttype");
            Ddo_contratosrvvnc_servicosigla_Datalistproc = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Datalistproc");
            Ddo_contratosrvvnc_servicosigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratosrvvnc_servicosigla_Sortasc = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Sortasc");
            Ddo_contratosrvvnc_servicosigla_Sortdsc = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Sortdsc");
            Ddo_contratosrvvnc_servicosigla_Loadingdata = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Loadingdata");
            Ddo_contratosrvvnc_servicosigla_Cleanfilter = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Cleanfilter");
            Ddo_contratosrvvnc_servicosigla_Noresultsfound = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Noresultsfound");
            Ddo_contratosrvvnc_servicosigla_Searchbuttontext = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Searchbuttontext");
            Ddo_contratosrvvnc_statusdmn_Caption = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Caption");
            Ddo_contratosrvvnc_statusdmn_Tooltip = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Tooltip");
            Ddo_contratosrvvnc_statusdmn_Cls = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Cls");
            Ddo_contratosrvvnc_statusdmn_Selectedvalue_set = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Selectedvalue_set");
            Ddo_contratosrvvnc_statusdmn_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Dropdownoptionstype");
            Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_statusdmn_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Includesortasc"));
            Ddo_contratosrvvnc_statusdmn_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Includesortdsc"));
            Ddo_contratosrvvnc_statusdmn_Sortedstatus = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Sortedstatus");
            Ddo_contratosrvvnc_statusdmn_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Includefilter"));
            Ddo_contratosrvvnc_statusdmn_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Includedatalist"));
            Ddo_contratosrvvnc_statusdmn_Datalisttype = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Datalisttype");
            Ddo_contratosrvvnc_statusdmn_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Allowmultipleselection"));
            Ddo_contratosrvvnc_statusdmn_Datalistfixedvalues = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Datalistfixedvalues");
            Ddo_contratosrvvnc_statusdmn_Sortasc = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Sortasc");
            Ddo_contratosrvvnc_statusdmn_Sortdsc = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Sortdsc");
            Ddo_contratosrvvnc_statusdmn_Cleanfilter = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Cleanfilter");
            Ddo_contratosrvvnc_statusdmn_Searchbuttontext = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Searchbuttontext");
            Ddo_contratosrvvnc_servicovncsigla_Caption = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Caption");
            Ddo_contratosrvvnc_servicovncsigla_Tooltip = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Tooltip");
            Ddo_contratosrvvnc_servicovncsigla_Cls = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Cls");
            Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filteredtext_set");
            Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Selectedvalue_set");
            Ddo_contratosrvvnc_servicovncsigla_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Dropdownoptionstype");
            Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Titlecontrolidtoreplace");
            Ddo_contratosrvvnc_servicovncsigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includesortasc"));
            Ddo_contratosrvvnc_servicovncsigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includesortdsc"));
            Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortedstatus");
            Ddo_contratosrvvnc_servicovncsigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includefilter"));
            Ddo_contratosrvvnc_servicovncsigla_Filtertype = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filtertype");
            Ddo_contratosrvvnc_servicovncsigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filterisrange"));
            Ddo_contratosrvvnc_servicovncsigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Includedatalist"));
            Ddo_contratosrvvnc_servicovncsigla_Datalisttype = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalisttype");
            Ddo_contratosrvvnc_servicovncsigla_Datalistproc = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalistproc");
            Ddo_contratosrvvnc_servicovncsigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratosrvvnc_servicovncsigla_Sortasc = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortasc");
            Ddo_contratosrvvnc_servicovncsigla_Sortdsc = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Sortdsc");
            Ddo_contratosrvvnc_servicovncsigla_Loadingdata = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Loadingdata");
            Ddo_contratosrvvnc_servicovncsigla_Cleanfilter = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Cleanfilter");
            Ddo_contratosrvvnc_servicovncsigla_Noresultsfound = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Noresultsfound");
            Ddo_contratosrvvnc_servicovncsigla_Searchbuttontext = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratosrvvnc_servicosigla_Activeeventkey = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Activeeventkey");
            Ddo_contratosrvvnc_servicosigla_Filteredtext_get = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Filteredtext_get");
            Ddo_contratosrvvnc_servicosigla_Selectedvalue_get = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOSIGLA_Selectedvalue_get");
            Ddo_contratosrvvnc_statusdmn_Activeeventkey = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Activeeventkey");
            Ddo_contratosrvvnc_statusdmn_Selectedvalue_get = cgiGet( "DDO_CONTRATOSRVVNC_STATUSDMN_Selectedvalue_get");
            Ddo_contratosrvvnc_servicovncsigla_Activeeventkey = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Activeeventkey");
            Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Filteredtext_get");
            Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get = cgiGet( "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV8OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV9OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOSIGLA"), AV14TFContratoSrvVnc_ServicoSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL"), AV15TFContratoSrvVnc_ServicoSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA"), AV22TFContratoSrvVnc_ServicoVncSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL"), AV23TFContratoSrvVnc_ServicoVncSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E17GD2 */
         E17GD2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17GD2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontratosrvvnc_servicosigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratosrvvnc_servicosigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_servicosigla_Visible), 5, 0)));
         edtavTfcontratosrvvnc_servicosigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratosrvvnc_servicosigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_servicosigla_sel_Visible), 5, 0)));
         edtavTfcontratosrvvnc_servicovncsigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratosrvvnc_servicovncsigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_servicovncsigla_Visible), 5, 0)));
         edtavTfcontratosrvvnc_servicovncsigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratosrvvnc_servicovncsigla_sel_Visible), 5, 0)));
         Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_ServicoSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace);
         AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace = Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace", AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_StatusDmn";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_statusdmn_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace);
         AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace = Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace", AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoSrvVnc_ServicoVncSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "TitleControlIdToReplace", Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace);
         AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace = Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace", AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace);
         edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Regra de Vinculo entre Servi�os";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Servi�o", 0);
         cmbavOrderedby.addItem("2", "No status", 0);
         cmbavOrderedby.addItem("3", "Novo servi�o", 0);
         if ( AV8OrderedBy < 1 )
         {
            AV8OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV25DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV25DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E18GD2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV13ContratoSrvVnc_ServicoSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV17ContratoSrvVnc_StatusDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV21ContratoSrvVnc_ServicoVncSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoSrvVnc_ServicoSigla_Titleformat = 2;
         edtContratoSrvVnc_ServicoSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Servi�o", AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoSrvVnc_ServicoSigla_Internalname, "Title", edtContratoSrvVnc_ServicoSigla_Title);
         cmbContratoSrvVnc_StatusDmn_Titleformat = 2;
         cmbContratoSrvVnc_StatusDmn.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "No status", AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_StatusDmn_Internalname, "Title", cmbContratoSrvVnc_StatusDmn.Title.Text);
         edtContratoSrvVnc_ServicoVncSigla_Titleformat = 2;
         edtContratoSrvVnc_ServicoVncSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Novo servi�o", AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoSrvVnc_ServicoVncSigla_Internalname, "Title", edtContratoSrvVnc_ServicoVncSigla_Title);
         AV27GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27GridCurrentPage), 10, 0)));
         AV28GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV13ContratoSrvVnc_ServicoSiglaTitleFilterData", AV13ContratoSrvVnc_ServicoSiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV17ContratoSrvVnc_StatusDmnTitleFilterData", AV17ContratoSrvVnc_StatusDmnTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV21ContratoSrvVnc_ServicoVncSiglaTitleFilterData", AV21ContratoSrvVnc_ServicoVncSiglaTitleFilterData);
      }

      protected void E11GD2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV26PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV26PageToGo) ;
         }
      }

      protected void E12GD2( )
      {
         /* Ddo_contratosrvvnc_servicosigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicosigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV8OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0)));
            AV9OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedDsc", AV9OrderedDsc);
            Ddo_contratosrvvnc_servicosigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicosigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicosigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV8OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0)));
            AV9OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedDsc", AV9OrderedDsc);
            Ddo_contratosrvvnc_servicosigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicosigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicosigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV14TFContratoSrvVnc_ServicoSigla = Ddo_contratosrvvnc_servicosigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TFContratoSrvVnc_ServicoSigla", AV14TFContratoSrvVnc_ServicoSigla);
            AV15TFContratoSrvVnc_ServicoSigla_Sel = Ddo_contratosrvvnc_servicosigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15TFContratoSrvVnc_ServicoSigla_Sel", AV15TFContratoSrvVnc_ServicoSigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13GD2( )
      {
         /* Ddo_contratosrvvnc_statusdmn_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_statusdmn_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV8OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0)));
            AV9OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedDsc", AV9OrderedDsc);
            Ddo_contratosrvvnc_statusdmn_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_statusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_statusdmn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_statusdmn_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV8OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0)));
            AV9OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedDsc", AV9OrderedDsc);
            Ddo_contratosrvvnc_statusdmn_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_statusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_statusdmn_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_statusdmn_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV18TFContratoSrvVnc_StatusDmn_SelsJson = Ddo_contratosrvvnc_statusdmn_Selectedvalue_get;
            AV19TFContratoSrvVnc_StatusDmn_Sels.FromJSonString(AV18TFContratoSrvVnc_StatusDmn_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19TFContratoSrvVnc_StatusDmn_Sels", AV19TFContratoSrvVnc_StatusDmn_Sels);
      }

      protected void E14GD2( )
      {
         /* Ddo_contratosrvvnc_servicovncsigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicovncsigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV8OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0)));
            AV9OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedDsc", AV9OrderedDsc);
            Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicovncsigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicovncsigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV8OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0)));
            AV9OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9OrderedDsc", AV9OrderedDsc);
            Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicovncsigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratosrvvnc_servicovncsigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV22TFContratoSrvVnc_ServicoVncSigla = Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFContratoSrvVnc_ServicoVncSigla", AV22TFContratoSrvVnc_ServicoVncSigla);
            AV23TFContratoSrvVnc_ServicoVncSigla_Sel = Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TFContratoSrvVnc_ServicoVncSigla_Sel", AV23TFContratoSrvVnc_ServicoVncSigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E19GD2( )
      {
         /* Grid_Load Routine */
         AV10Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV10Select);
         AV31Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 27;
         }
         sendrow_272( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_27_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(27, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E20GD2 */
         E20GD2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20GD2( )
      {
         /* Enter Routine */
         AV7InOutContratoSrvVnc_Codigo = A917ContratoSrvVnc_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoSrvVnc_Codigo), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratoSrvVnc_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E15GD2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E16GD2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19TFContratoSrvVnc_StatusDmn_Sels", AV19TFContratoSrvVnc_StatusDmn_Sels);
      }

      protected void S132( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratosrvvnc_servicosigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicosigla_Sortedstatus);
         Ddo_contratosrvvnc_statusdmn_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_statusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_statusdmn_Sortedstatus);
         Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicovncsigla_Sortedstatus);
      }

      protected void S112( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV8OrderedBy == 1 )
         {
            Ddo_contratosrvvnc_servicosigla_Sortedstatus = (AV9OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicosigla_Sortedstatus);
         }
         else if ( AV8OrderedBy == 2 )
         {
            Ddo_contratosrvvnc_statusdmn_Sortedstatus = (AV9OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_statusdmn_Internalname, "SortedStatus", Ddo_contratosrvvnc_statusdmn_Sortedstatus);
         }
         else if ( AV8OrderedBy == 3 )
         {
            Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = (AV9OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SortedStatus", Ddo_contratosrvvnc_servicovncsigla_Sortedstatus);
         }
      }

      protected void S142( )
      {
         /* 'CLEANFILTERS' Routine */
         AV14TFContratoSrvVnc_ServicoSigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14TFContratoSrvVnc_ServicoSigla", AV14TFContratoSrvVnc_ServicoSigla);
         Ddo_contratosrvvnc_servicosigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "FilteredText_set", Ddo_contratosrvvnc_servicosigla_Filteredtext_set);
         AV15TFContratoSrvVnc_ServicoSigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15TFContratoSrvVnc_ServicoSigla_Sel", AV15TFContratoSrvVnc_ServicoSigla_Sel);
         Ddo_contratosrvvnc_servicosigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicosigla_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_servicosigla_Selectedvalue_set);
         AV19TFContratoSrvVnc_StatusDmn_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_contratosrvvnc_statusdmn_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_statusdmn_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_statusdmn_Selectedvalue_set);
         AV22TFContratoSrvVnc_ServicoVncSigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFContratoSrvVnc_ServicoVncSigla", AV22TFContratoSrvVnc_ServicoVncSigla);
         Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "FilteredText_set", Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set);
         AV23TFContratoSrvVnc_ServicoVncSigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TFContratoSrvVnc_ServicoVncSigla_Sel", AV23TFContratoSrvVnc_ServicoVncSigla_Sel);
         Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratosrvvnc_servicovncsigla_Internalname, "SelectedValue_set", Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set);
      }

      protected void S122( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV11GridState.gxTpr_Orderedby = AV8OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV9OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoSigla)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SERVICOSIGLA";
            AV12GridStateFilterValue.gxTpr_Value = AV14TFContratoSrvVnc_ServicoSigla;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoSigla_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SERVICOSIGLA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV15TFContratoSrvVnc_ServicoSigla_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV19TFContratoSrvVnc_StatusDmn_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_STATUSDMN_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV19TFContratoSrvVnc_StatusDmn_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoSrvVnc_ServicoVncSigla)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SERVICOVNCSIGLA";
            AV12GridStateFilterValue.gxTpr_Value = AV22TFContratoSrvVnc_ServicoVncSigla;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoSrvVnc_ServicoVncSigla_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV23TFContratoSrvVnc_ServicoVncSigla_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV32Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void wb_table1_2_GD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_GD2( true) ;
         }
         else
         {
            wb_table2_5_GD2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_GD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_21_GD2( true) ;
         }
         else
         {
            wb_table3_21_GD2( false) ;
         }
         return  ;
      }

      protected void wb_table3_21_GD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GD2e( true) ;
         }
         else
         {
            wb_table1_2_GD2e( false) ;
         }
      }

      protected void wb_table3_21_GD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_24_GD2( true) ;
         }
         else
         {
            wb_table4_24_GD2( false) ;
         }
         return  ;
      }

      protected void wb_table4_24_GD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_21_GD2e( true) ;
         }
         else
         {
            wb_table3_21_GD2e( false) ;
         }
      }

      protected void wb_table4_24_GD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"27\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoSrvVnc_ServicoSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoSrvVnc_ServicoSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoSrvVnc_ServicoSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratoSrvVnc_StatusDmn_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratoSrvVnc_StatusDmn.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratoSrvVnc_StatusDmn.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoSrvVnc_ServicoVncSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoSrvVnc_ServicoVncSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoSrvVnc_ServicoVncSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV10Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A922ContratoSrvVnc_ServicoSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoSrvVnc_ServicoSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoSrvVnc_ServicoSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratoSrvVnc_StatusDmn.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratoSrvVnc_StatusDmn_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A924ContratoSrvVnc_ServicoVncSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoSrvVnc_ServicoVncSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoSrvVnc_ServicoVncSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 27 )
         {
            wbEnd = 0;
            nRC_GXsfl_27 = (short)(nGXsfl_27_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_24_GD2e( true) ;
         }
         else
         {
            wb_table4_24_GD2e( false) ;
         }
      }

      protected void wb_table2_5_GD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_27_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratoServicosVnc.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV9OrderedDsc), StringUtil.BoolToStr( AV9OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_GD2( true) ;
         }
         else
         {
            wb_table5_14_GD2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_GD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_GD2e( true) ;
         }
         else
         {
            wb_table2_5_GD2e( false) ;
         }
      }

      protected void wb_table5_14_GD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosVnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_GD2e( true) ;
         }
         else
         {
            wb_table5_14_GD2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratoSrvVnc_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoSrvVnc_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGD2( ) ;
         WSGD2( ) ;
         WEGD2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120441379");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratoservicosvnc.js", "?20203120441379");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_272( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_27_idx;
         edtContratoSrvVnc_Codigo_Internalname = "CONTRATOSRVVNC_CODIGO_"+sGXsfl_27_idx;
         edtContratoSrvVnc_ServicoSigla_Internalname = "CONTRATOSRVVNC_SERVICOSIGLA_"+sGXsfl_27_idx;
         cmbContratoSrvVnc_StatusDmn_Internalname = "CONTRATOSRVVNC_STATUSDMN_"+sGXsfl_27_idx;
         edtContratoSrvVnc_ServicoVncSigla_Internalname = "CONTRATOSRVVNC_SERVICOVNCSIGLA_"+sGXsfl_27_idx;
      }

      protected void SubsflControlProps_fel_272( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_27_fel_idx;
         edtContratoSrvVnc_Codigo_Internalname = "CONTRATOSRVVNC_CODIGO_"+sGXsfl_27_fel_idx;
         edtContratoSrvVnc_ServicoSigla_Internalname = "CONTRATOSRVVNC_SERVICOSIGLA_"+sGXsfl_27_fel_idx;
         cmbContratoSrvVnc_StatusDmn_Internalname = "CONTRATOSRVVNC_STATUSDMN_"+sGXsfl_27_fel_idx;
         edtContratoSrvVnc_ServicoVncSigla_Internalname = "CONTRATOSRVVNC_SERVICOVNCSIGLA_"+sGXsfl_27_fel_idx;
      }

      protected void sendrow_272( )
      {
         SubsflControlProps_272( ) ;
         WBGD0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_27_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_27_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_27_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 28,'',false,'',27)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV10Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV10Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV31Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV10Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV10Select)) ? AV31Select_GXI : context.PathToRelativeUrl( AV10Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_27_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV10Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_ServicoSigla_Internalname,StringUtil.RTrim( A922ContratoSrvVnc_ServicoSigla),StringUtil.RTrim( context.localUtil.Format( A922ContratoSrvVnc_ServicoSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_ServicoSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_27_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATOSRVVNC_STATUSDMN_" + sGXsfl_27_idx;
               cmbContratoSrvVnc_StatusDmn.Name = GXCCtl;
               cmbContratoSrvVnc_StatusDmn.WebTags = "";
               cmbContratoSrvVnc_StatusDmn.addItem("B", "Stand by", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("S", "Solicitada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("E", "Em An�lise", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("A", "Em execu��o", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("R", "Resolvida", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("C", "Conferida", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("D", "Rejeitada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("H", "Homologada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("O", "Aceite", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("P", "A Pagar", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("L", "Liquidada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("X", "Cancelada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("N", "N�o Faturada", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("J", "Planejamento", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("I", "An�lise Planejamento", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("T", "Validacao T�cnica", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("G", "Em Homologa��o", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
               cmbContratoSrvVnc_StatusDmn.addItem("U", "Rascunho", 0);
               if ( cmbContratoSrvVnc_StatusDmn.ItemCount > 0 )
               {
                  A1084ContratoSrvVnc_StatusDmn = cmbContratoSrvVnc_StatusDmn.getValidValue(A1084ContratoSrvVnc_StatusDmn);
                  n1084ContratoSrvVnc_StatusDmn = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratoSrvVnc_StatusDmn,(String)cmbContratoSrvVnc_StatusDmn_Internalname,StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn),(short)1,(String)cmbContratoSrvVnc_StatusDmn_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratoSrvVnc_StatusDmn.CurrentValue = StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoSrvVnc_StatusDmn_Internalname, "Values", (String)(cmbContratoSrvVnc_StatusDmn.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoSrvVnc_ServicoVncSigla_Internalname,StringUtil.RTrim( A924ContratoSrvVnc_ServicoVncSigla),StringUtil.RTrim( context.localUtil.Format( A924ContratoSrvVnc_ServicoVncSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoSrvVnc_ServicoVncSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSRVVNC_CODIGO"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sGXsfl_27_idx, context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSRVVNC_STATUSDMN"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sGXsfl_27_idx, StringUtil.RTrim( context.localUtil.Format( A1084ContratoSrvVnc_StatusDmn, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_27_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_27_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_27_idx+1));
            sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
            SubsflControlProps_272( ) ;
         }
         /* End function sendrow_272 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContratoSrvVnc_Codigo_Internalname = "CONTRATOSRVVNC_CODIGO";
         edtContratoSrvVnc_ServicoSigla_Internalname = "CONTRATOSRVVNC_SERVICOSIGLA";
         cmbContratoSrvVnc_StatusDmn_Internalname = "CONTRATOSRVVNC_STATUSDMN";
         edtContratoSrvVnc_ServicoVncSigla_Internalname = "CONTRATOSRVVNC_SERVICOVNCSIGLA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTfcontratosrvvnc_servicosigla_Internalname = "vTFCONTRATOSRVVNC_SERVICOSIGLA";
         edtavTfcontratosrvvnc_servicosigla_sel_Internalname = "vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL";
         edtavTfcontratosrvvnc_servicovncsigla_Internalname = "vTFCONTRATOSRVVNC_SERVICOVNCSIGLA";
         edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname = "vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL";
         Ddo_contratosrvvnc_servicosigla_Internalname = "DDO_CONTRATOSRVVNC_SERVICOSIGLA";
         edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE";
         Ddo_contratosrvvnc_statusdmn_Internalname = "DDO_CONTRATOSRVVNC_STATUSDMN";
         edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE";
         Ddo_contratosrvvnc_servicovncsigla_Internalname = "DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA";
         edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoSrvVnc_ServicoVncSigla_Jsonclick = "";
         cmbContratoSrvVnc_StatusDmn_Jsonclick = "";
         edtContratoSrvVnc_ServicoSigla_Jsonclick = "";
         edtContratoSrvVnc_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContratoSrvVnc_ServicoVncSigla_Titleformat = 0;
         cmbContratoSrvVnc_StatusDmn_Titleformat = 0;
         edtContratoSrvVnc_ServicoSigla_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtContratoSrvVnc_ServicoVncSigla_Title = "Novo servi�o";
         cmbContratoSrvVnc_StatusDmn.Title.Text = "No status";
         edtContratoSrvVnc_ServicoSigla_Title = "Servi�o";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratosrvvnc_servicovncsigla_sel_Jsonclick = "";
         edtavTfcontratosrvvnc_servicovncsigla_sel_Visible = 1;
         edtavTfcontratosrvvnc_servicovncsigla_Jsonclick = "";
         edtavTfcontratosrvvnc_servicovncsigla_Visible = 1;
         edtavTfcontratosrvvnc_servicosigla_sel_Jsonclick = "";
         edtavTfcontratosrvvnc_servicosigla_sel_Visible = 1;
         edtavTfcontratosrvvnc_servicosigla_Jsonclick = "";
         edtavTfcontratosrvvnc_servicosigla_Visible = 1;
         Ddo_contratosrvvnc_servicovncsigla_Searchbuttontext = "Pesquisar";
         Ddo_contratosrvvnc_servicovncsigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratosrvvnc_servicovncsigla_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_servicovncsigla_Loadingdata = "Carregando dados...";
         Ddo_contratosrvvnc_servicovncsigla_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_servicovncsigla_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_servicovncsigla_Datalistupdateminimumcharacters = 0;
         Ddo_contratosrvvnc_servicovncsigla_Datalistproc = "GetPromptContratoServicosVncFilterData";
         Ddo_contratosrvvnc_servicovncsigla_Datalisttype = "Dynamic";
         Ddo_contratosrvvnc_servicovncsigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicovncsigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_servicovncsigla_Filtertype = "Character";
         Ddo_contratosrvvnc_servicovncsigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicovncsigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicovncsigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_servicovncsigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_servicovncsigla_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_servicovncsigla_Tooltip = "Op��es";
         Ddo_contratosrvvnc_servicovncsigla_Caption = "";
         Ddo_contratosrvvnc_statusdmn_Searchbuttontext = "Filtrar Selecionados";
         Ddo_contratosrvvnc_statusdmn_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_statusdmn_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_statusdmn_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_statusdmn_Datalistfixedvalues = "B:Stand by,S:Solicitada,E:Em An�lise,A:Em execu��o,R:Resolvida,C:Conferida,D:Rejeitada,H:Homologada,O:Aceite,P:A Pagar,L:Liquidada,X:Cancelada,N:N�o Faturada,J:Planejamento,I:An�lise Planejamento,T:Validacao T�cnica,Q:Validacao Qualidade,G:Em Homologa��o,M:Valida��o Mensura��o,U:Rascunho";
         Ddo_contratosrvvnc_statusdmn_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_statusdmn_Datalisttype = "FixedValues";
         Ddo_contratosrvvnc_statusdmn_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_statusdmn_Includefilter = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_statusdmn_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_statusdmn_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_statusdmn_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_statusdmn_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_statusdmn_Tooltip = "Op��es";
         Ddo_contratosrvvnc_statusdmn_Caption = "";
         Ddo_contratosrvvnc_servicosigla_Searchbuttontext = "Pesquisar";
         Ddo_contratosrvvnc_servicosigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratosrvvnc_servicosigla_Cleanfilter = "Limpar pesquisa";
         Ddo_contratosrvvnc_servicosigla_Loadingdata = "Carregando dados...";
         Ddo_contratosrvvnc_servicosigla_Sortdsc = "Ordenar de Z � A";
         Ddo_contratosrvvnc_servicosigla_Sortasc = "Ordenar de A � Z";
         Ddo_contratosrvvnc_servicosigla_Datalistupdateminimumcharacters = 0;
         Ddo_contratosrvvnc_servicosigla_Datalistproc = "GetPromptContratoServicosVncFilterData";
         Ddo_contratosrvvnc_servicosigla_Datalisttype = "Dynamic";
         Ddo_contratosrvvnc_servicosigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicosigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratosrvvnc_servicosigla_Filtertype = "Character";
         Ddo_contratosrvvnc_servicosigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicosigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicosigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace = "";
         Ddo_contratosrvvnc_servicosigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratosrvvnc_servicosigla_Cls = "ColumnSettings";
         Ddo_contratosrvvnc_servicosigla_Tooltip = "Op��es";
         Ddo_contratosrvvnc_servicosigla_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Regra de Vinculo entre Servi�os";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV8OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV9OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV15TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV19TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV22TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV23TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV32Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV13ContratoSrvVnc_ServicoSiglaTitleFilterData',fld:'vCONTRATOSRVVNC_SERVICOSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV17ContratoSrvVnc_StatusDmnTitleFilterData',fld:'vCONTRATOSRVVNC_STATUSDMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV21ContratoSrvVnc_ServicoVncSiglaTitleFilterData',fld:'vCONTRATOSRVVNC_SERVICOVNCSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoSrvVnc_ServicoSigla_Titleformat',ctrl:'CONTRATOSRVVNC_SERVICOSIGLA',prop:'Titleformat'},{av:'edtContratoSrvVnc_ServicoSigla_Title',ctrl:'CONTRATOSRVVNC_SERVICOSIGLA',prop:'Title'},{av:'cmbContratoSrvVnc_StatusDmn'},{av:'edtContratoSrvVnc_ServicoVncSigla_Titleformat',ctrl:'CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'Titleformat'},{av:'edtContratoSrvVnc_ServicoVncSigla_Title',ctrl:'CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'Title'},{av:'AV27GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV28GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11GD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV9OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV15TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV23TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV19TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV32Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_SERVICOSIGLA.ONOPTIONCLICKED","{handler:'E12GD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV9OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV15TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV23TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV19TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV32Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_contratosrvvnc_servicosigla_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_servicosigla_Filteredtext_get',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'FilteredText_get'},{av:'Ddo_contratosrvvnc_servicosigla_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV8OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV9OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SortedStatus'},{av:'AV14TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV15TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_STATUSDMN.ONOPTIONCLICKED","{handler:'E13GD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV9OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV15TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV23TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV19TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV32Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_contratosrvvnc_statusdmn_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_statusdmn_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SelectedValue_get'}],oparms:[{av:'AV8OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV9OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'},{av:'AV19TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'Ddo_contratosrvvnc_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA.ONOPTIONCLICKED","{handler:'E14GD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV9OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV15TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV23TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV19TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV32Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_contratosrvvnc_servicovncsigla_Activeeventkey',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'ActiveEventKey'},{av:'Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'FilteredText_get'},{av:'Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV8OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV9OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratosrvvnc_servicovncsigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SortedStatus'},{av:'AV22TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV23TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_servicosigla_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SortedStatus'},{av:'Ddo_contratosrvvnc_statusdmn_Sortedstatus',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E19GD2',iparms:[],oparms:[{av:'AV10Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E20GD2',iparms:[{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV7InOutContratoSrvVnc_Codigo',fld:'vINOUTCONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15GD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV9OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV15TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV23TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV19TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV32Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E16GD2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV9OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV15TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV22TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'AV23TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_STATUSDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace',fld:'vDDO_CONTRATOSRVVNC_SERVICOVNCSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV19TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'AV32Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV14TFContratoSrvVnc_ServicoSigla',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_servicosigla_Filteredtext_set',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'FilteredText_set'},{av:'AV15TFContratoSrvVnc_ServicoSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_servicosigla_Selectedvalue_set',ctrl:'DDO_CONTRATOSRVVNC_SERVICOSIGLA',prop:'SelectedValue_set'},{av:'AV19TFContratoSrvVnc_StatusDmn_Sels',fld:'vTFCONTRATOSRVVNC_STATUSDMN_SELS',pic:'',nv:null},{av:'Ddo_contratosrvvnc_statusdmn_Selectedvalue_set',ctrl:'DDO_CONTRATOSRVVNC_STATUSDMN',prop:'SelectedValue_set'},{av:'AV22TFContratoSrvVnc_ServicoVncSigla',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'FilteredText_set'},{av:'AV23TFContratoSrvVnc_ServicoVncSigla_Sel',fld:'vTFCONTRATOSRVVNC_SERVICOVNCSIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set',ctrl:'DDO_CONTRATOSRVVNC_SERVICOVNCSIGLA',prop:'SelectedValue_set'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratosrvvnc_servicosigla_Activeeventkey = "";
         Ddo_contratosrvvnc_servicosigla_Filteredtext_get = "";
         Ddo_contratosrvvnc_servicosigla_Selectedvalue_get = "";
         Ddo_contratosrvvnc_statusdmn_Activeeventkey = "";
         Ddo_contratosrvvnc_statusdmn_Selectedvalue_get = "";
         Ddo_contratosrvvnc_servicovncsigla_Activeeventkey = "";
         Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get = "";
         Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV14TFContratoSrvVnc_ServicoSigla = "";
         AV15TFContratoSrvVnc_ServicoSigla_Sel = "";
         AV22TFContratoSrvVnc_ServicoVncSigla = "";
         AV23TFContratoSrvVnc_ServicoVncSigla_Sel = "";
         AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace = "";
         AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace = "";
         AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace = "";
         AV19TFContratoSrvVnc_StatusDmn_Sels = new GxSimpleCollection();
         AV32Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV25DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV13ContratoSrvVnc_ServicoSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV17ContratoSrvVnc_StatusDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV21ContratoSrvVnc_ServicoVncSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratosrvvnc_servicosigla_Filteredtext_set = "";
         Ddo_contratosrvvnc_servicosigla_Selectedvalue_set = "";
         Ddo_contratosrvvnc_servicosigla_Sortedstatus = "";
         Ddo_contratosrvvnc_statusdmn_Selectedvalue_set = "";
         Ddo_contratosrvvnc_statusdmn_Sortedstatus = "";
         Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set = "";
         Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set = "";
         Ddo_contratosrvvnc_servicovncsigla_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV10Select = "";
         AV31Select_GXI = "";
         A922ContratoSrvVnc_ServicoSigla = "";
         A1084ContratoSrvVnc_StatusDmn = "";
         A924ContratoSrvVnc_ServicoVncSigla = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV14TFContratoSrvVnc_ServicoSigla = "";
         lV22TFContratoSrvVnc_ServicoVncSigla = "";
         H00GD2_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         H00GD2_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         H00GD2_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         H00GD2_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         H00GD2_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         H00GD2_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         H00GD2_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         H00GD2_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         H00GD2_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         H00GD2_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         H00GD2_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         H00GD2_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         H00GD2_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         H00GD2_A917ContratoSrvVnc_Codigo = new int[1] ;
         H00GD3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV18TFContratoSrvVnc_StatusDmn_SelsJson = "";
         GridRow = new GXWebRow();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratoservicosvnc__default(),
            new Object[][] {
                new Object[] {
               H00GD2_A915ContratoSrvVnc_CntSrvCod, H00GD2_A921ContratoSrvVnc_ServicoCod, H00GD2_n921ContratoSrvVnc_ServicoCod, H00GD2_A1589ContratoSrvVnc_SrvVncCntSrvCod, H00GD2_n1589ContratoSrvVnc_SrvVncCntSrvCod, H00GD2_A923ContratoSrvVnc_ServicoVncCod, H00GD2_n923ContratoSrvVnc_ServicoVncCod, H00GD2_A924ContratoSrvVnc_ServicoVncSigla, H00GD2_n924ContratoSrvVnc_ServicoVncSigla, H00GD2_A1084ContratoSrvVnc_StatusDmn,
               H00GD2_n1084ContratoSrvVnc_StatusDmn, H00GD2_A922ContratoSrvVnc_ServicoSigla, H00GD2_n922ContratoSrvVnc_ServicoSigla, H00GD2_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               H00GD3_AGRID_nRecordCount
               }
            }
         );
         AV32Pgmname = "PromptContratoServicosVnc";
         /* GeneXus formulas. */
         AV32Pgmname = "PromptContratoServicosVnc";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_27 ;
      private short nGXsfl_27_idx=1 ;
      private short AV8OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_27_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoSrvVnc_ServicoSigla_Titleformat ;
      private short cmbContratoSrvVnc_StatusDmn_Titleformat ;
      private short edtContratoSrvVnc_ServicoVncSigla_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratoSrvVnc_Codigo ;
      private int wcpOAV7InOutContratoSrvVnc_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratosrvvnc_servicosigla_Datalistupdateminimumcharacters ;
      private int Ddo_contratosrvvnc_servicovncsigla_Datalistupdateminimumcharacters ;
      private int edtavTfcontratosrvvnc_servicosigla_Visible ;
      private int edtavTfcontratosrvvnc_servicosigla_sel_Visible ;
      private int edtavTfcontratosrvvnc_servicovncsigla_Visible ;
      private int edtavTfcontratosrvvnc_servicovncsigla_sel_Visible ;
      private int edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Visible ;
      private int A917ContratoSrvVnc_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV19TFContratoSrvVnc_StatusDmn_Sels_Count ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A921ContratoSrvVnc_ServicoCod ;
      private int A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int A923ContratoSrvVnc_ServicoVncCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV26PageToGo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV27GridCurrentPage ;
      private long AV28GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratosrvvnc_servicosigla_Activeeventkey ;
      private String Ddo_contratosrvvnc_servicosigla_Filteredtext_get ;
      private String Ddo_contratosrvvnc_servicosigla_Selectedvalue_get ;
      private String Ddo_contratosrvvnc_statusdmn_Activeeventkey ;
      private String Ddo_contratosrvvnc_statusdmn_Selectedvalue_get ;
      private String Ddo_contratosrvvnc_servicovncsigla_Activeeventkey ;
      private String Ddo_contratosrvvnc_servicovncsigla_Filteredtext_get ;
      private String Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_27_idx="0001" ;
      private String AV14TFContratoSrvVnc_ServicoSigla ;
      private String AV15TFContratoSrvVnc_ServicoSigla_Sel ;
      private String AV22TFContratoSrvVnc_ServicoVncSigla ;
      private String AV23TFContratoSrvVnc_ServicoVncSigla_Sel ;
      private String AV32Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratosrvvnc_servicosigla_Caption ;
      private String Ddo_contratosrvvnc_servicosigla_Tooltip ;
      private String Ddo_contratosrvvnc_servicosigla_Cls ;
      private String Ddo_contratosrvvnc_servicosigla_Filteredtext_set ;
      private String Ddo_contratosrvvnc_servicosigla_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_servicosigla_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_servicosigla_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_servicosigla_Sortedstatus ;
      private String Ddo_contratosrvvnc_servicosigla_Filtertype ;
      private String Ddo_contratosrvvnc_servicosigla_Datalisttype ;
      private String Ddo_contratosrvvnc_servicosigla_Datalistproc ;
      private String Ddo_contratosrvvnc_servicosigla_Sortasc ;
      private String Ddo_contratosrvvnc_servicosigla_Sortdsc ;
      private String Ddo_contratosrvvnc_servicosigla_Loadingdata ;
      private String Ddo_contratosrvvnc_servicosigla_Cleanfilter ;
      private String Ddo_contratosrvvnc_servicosigla_Noresultsfound ;
      private String Ddo_contratosrvvnc_servicosigla_Searchbuttontext ;
      private String Ddo_contratosrvvnc_statusdmn_Caption ;
      private String Ddo_contratosrvvnc_statusdmn_Tooltip ;
      private String Ddo_contratosrvvnc_statusdmn_Cls ;
      private String Ddo_contratosrvvnc_statusdmn_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_statusdmn_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_statusdmn_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_statusdmn_Sortedstatus ;
      private String Ddo_contratosrvvnc_statusdmn_Datalisttype ;
      private String Ddo_contratosrvvnc_statusdmn_Datalistfixedvalues ;
      private String Ddo_contratosrvvnc_statusdmn_Sortasc ;
      private String Ddo_contratosrvvnc_statusdmn_Sortdsc ;
      private String Ddo_contratosrvvnc_statusdmn_Cleanfilter ;
      private String Ddo_contratosrvvnc_statusdmn_Searchbuttontext ;
      private String Ddo_contratosrvvnc_servicovncsigla_Caption ;
      private String Ddo_contratosrvvnc_servicovncsigla_Tooltip ;
      private String Ddo_contratosrvvnc_servicovncsigla_Cls ;
      private String Ddo_contratosrvvnc_servicovncsigla_Filteredtext_set ;
      private String Ddo_contratosrvvnc_servicovncsigla_Selectedvalue_set ;
      private String Ddo_contratosrvvnc_servicovncsigla_Dropdownoptionstype ;
      private String Ddo_contratosrvvnc_servicovncsigla_Titlecontrolidtoreplace ;
      private String Ddo_contratosrvvnc_servicovncsigla_Sortedstatus ;
      private String Ddo_contratosrvvnc_servicovncsigla_Filtertype ;
      private String Ddo_contratosrvvnc_servicovncsigla_Datalisttype ;
      private String Ddo_contratosrvvnc_servicovncsigla_Datalistproc ;
      private String Ddo_contratosrvvnc_servicovncsigla_Sortasc ;
      private String Ddo_contratosrvvnc_servicovncsigla_Sortdsc ;
      private String Ddo_contratosrvvnc_servicovncsigla_Loadingdata ;
      private String Ddo_contratosrvvnc_servicovncsigla_Cleanfilter ;
      private String Ddo_contratosrvvnc_servicovncsigla_Noresultsfound ;
      private String Ddo_contratosrvvnc_servicovncsigla_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavTfcontratosrvvnc_servicosigla_Internalname ;
      private String edtavTfcontratosrvvnc_servicosigla_Jsonclick ;
      private String edtavTfcontratosrvvnc_servicosigla_sel_Internalname ;
      private String edtavTfcontratosrvvnc_servicosigla_sel_Jsonclick ;
      private String edtavTfcontratosrvvnc_servicovncsigla_Internalname ;
      private String edtavTfcontratosrvvnc_servicovncsigla_Jsonclick ;
      private String edtavTfcontratosrvvnc_servicovncsigla_sel_Internalname ;
      private String edtavTfcontratosrvvnc_servicovncsigla_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contratosrvvnc_servicosiglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratosrvvnc_statusdmntitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratosrvvnc_servicovncsiglatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContratoSrvVnc_Codigo_Internalname ;
      private String A922ContratoSrvVnc_ServicoSigla ;
      private String edtContratoSrvVnc_ServicoSigla_Internalname ;
      private String cmbContratoSrvVnc_StatusDmn_Internalname ;
      private String A1084ContratoSrvVnc_StatusDmn ;
      private String A924ContratoSrvVnc_ServicoVncSigla ;
      private String edtContratoSrvVnc_ServicoVncSigla_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV14TFContratoSrvVnc_ServicoSigla ;
      private String lV22TFContratoSrvVnc_ServicoVncSigla ;
      private String edtavOrdereddsc_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratosrvvnc_servicosigla_Internalname ;
      private String Ddo_contratosrvvnc_statusdmn_Internalname ;
      private String Ddo_contratosrvvnc_servicovncsigla_Internalname ;
      private String edtContratoSrvVnc_ServicoSigla_Title ;
      private String edtContratoSrvVnc_ServicoVncSigla_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String sGXsfl_27_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContratoSrvVnc_Codigo_Jsonclick ;
      private String edtContratoSrvVnc_ServicoSigla_Jsonclick ;
      private String cmbContratoSrvVnc_StatusDmn_Jsonclick ;
      private String edtContratoSrvVnc_ServicoVncSigla_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV9OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratosrvvnc_servicosigla_Includesortasc ;
      private bool Ddo_contratosrvvnc_servicosigla_Includesortdsc ;
      private bool Ddo_contratosrvvnc_servicosigla_Includefilter ;
      private bool Ddo_contratosrvvnc_servicosigla_Filterisrange ;
      private bool Ddo_contratosrvvnc_servicosigla_Includedatalist ;
      private bool Ddo_contratosrvvnc_statusdmn_Includesortasc ;
      private bool Ddo_contratosrvvnc_statusdmn_Includesortdsc ;
      private bool Ddo_contratosrvvnc_statusdmn_Includefilter ;
      private bool Ddo_contratosrvvnc_statusdmn_Includedatalist ;
      private bool Ddo_contratosrvvnc_statusdmn_Allowmultipleselection ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Includesortasc ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Includesortdsc ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Includefilter ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Filterisrange ;
      private bool Ddo_contratosrvvnc_servicovncsigla_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n922ContratoSrvVnc_ServicoSigla ;
      private bool n1084ContratoSrvVnc_StatusDmn ;
      private bool n924ContratoSrvVnc_ServicoVncSigla ;
      private bool n921ContratoSrvVnc_ServicoCod ;
      private bool n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool n923ContratoSrvVnc_ServicoVncCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV10Select_IsBlob ;
      private String AV18TFContratoSrvVnc_StatusDmn_SelsJson ;
      private String AV16ddo_ContratoSrvVnc_ServicoSiglaTitleControlIdToReplace ;
      private String AV20ddo_ContratoSrvVnc_StatusDmnTitleControlIdToReplace ;
      private String AV24ddo_ContratoSrvVnc_ServicoVncSiglaTitleControlIdToReplace ;
      private String AV31Select_GXI ;
      private String AV10Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratoSrvVnc_Codigo ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbContratoSrvVnc_StatusDmn ;
      private IDataStoreProvider pr_default ;
      private int[] H00GD2_A915ContratoSrvVnc_CntSrvCod ;
      private int[] H00GD2_A921ContratoSrvVnc_ServicoCod ;
      private bool[] H00GD2_n921ContratoSrvVnc_ServicoCod ;
      private int[] H00GD2_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] H00GD2_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] H00GD2_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] H00GD2_n923ContratoSrvVnc_ServicoVncCod ;
      private String[] H00GD2_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] H00GD2_n924ContratoSrvVnc_ServicoVncSigla ;
      private String[] H00GD2_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] H00GD2_n1084ContratoSrvVnc_StatusDmn ;
      private String[] H00GD2_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] H00GD2_n922ContratoSrvVnc_ServicoSigla ;
      private int[] H00GD2_A917ContratoSrvVnc_Codigo ;
      private long[] H00GD3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19TFContratoSrvVnc_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV13ContratoSrvVnc_ServicoSiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV17ContratoSrvVnc_StatusDmnTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV21ContratoSrvVnc_ServicoVncSiglaTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV25DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratoservicosvnc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00GD2( IGxContext context ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             IGxCollection AV19TFContratoSrvVnc_StatusDmn_Sels ,
                                             String AV15TFContratoSrvVnc_ServicoSigla_Sel ,
                                             String AV14TFContratoSrvVnc_ServicoSigla ,
                                             int AV19TFContratoSrvVnc_StatusDmn_Sels_Count ,
                                             String AV23TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                             String AV22TFContratoSrvVnc_ServicoVncSigla ,
                                             String A922ContratoSrvVnc_ServicoSigla ,
                                             String A924ContratoSrvVnc_ServicoVncSigla ,
                                             short AV8OrderedBy ,
                                             bool AV9OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [9] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoCod, T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T4.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T5.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoSrvVnc_StatusDmn], T3.[Servico_Sigla] AS ContratoSrvVnc_ServicoSigla, T1.[ContratoSrvVnc_Codigo]";
         sFromString = " FROM (((([ContratoServicosVnc] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[Servico_Codigo])";
         sOrderString = "";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV14TFContratoSrvVnc_ServicoSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV14TFContratoSrvVnc_ServicoSigla)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV15TFContratoSrvVnc_ServicoSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] = @AV15TFContratoSrvVnc_ServicoSigla_Sel)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV19TFContratoSrvVnc_StatusDmn_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV19TFContratoSrvVnc_StatusDmn_Sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV19TFContratoSrvVnc_StatusDmn_Sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoSrvVnc_ServicoVncSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoSrvVnc_ServicoVncSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV22TFContratoSrvVnc_ServicoVncSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] like @lV22TFContratoSrvVnc_ServicoVncSigla)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoSrvVnc_ServicoVncSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] = @AV23TFContratoSrvVnc_ServicoVncSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] = @AV23TFContratoSrvVnc_ServicoVncSigla_Sel)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV8OrderedBy == 1 ) && ! AV9OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Servico_Sigla]";
         }
         else if ( ( AV8OrderedBy == 1 ) && ( AV9OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Servico_Sigla] DESC";
         }
         else if ( ( AV8OrderedBy == 2 ) && ! AV9OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_StatusDmn]";
         }
         else if ( ( AV8OrderedBy == 2 ) && ( AV9OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_StatusDmn] DESC";
         }
         else if ( ( AV8OrderedBy == 3 ) && ! AV9OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Servico_Sigla]";
         }
         else if ( ( AV8OrderedBy == 3 ) && ( AV9OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Servico_Sigla] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoSrvVnc_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00GD3( IGxContext context ,
                                             String A1084ContratoSrvVnc_StatusDmn ,
                                             IGxCollection AV19TFContratoSrvVnc_StatusDmn_Sels ,
                                             String AV15TFContratoSrvVnc_ServicoSigla_Sel ,
                                             String AV14TFContratoSrvVnc_ServicoSigla ,
                                             int AV19TFContratoSrvVnc_StatusDmn_Sels_Count ,
                                             String AV23TFContratoSrvVnc_ServicoVncSigla_Sel ,
                                             String AV22TFContratoSrvVnc_ServicoVncSigla ,
                                             String A922ContratoSrvVnc_ServicoSigla ,
                                             String A924ContratoSrvVnc_ServicoVncSigla ,
                                             short AV8OrderedBy ,
                                             bool AV9OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [4] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (((([ContratoServicosVnc] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[Servico_Codigo])";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoSrvVnc_ServicoSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV14TFContratoSrvVnc_ServicoSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV14TFContratoSrvVnc_ServicoSigla)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoSrvVnc_ServicoSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV15TFContratoSrvVnc_ServicoSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] = @AV15TFContratoSrvVnc_ServicoSigla_Sel)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV19TFContratoSrvVnc_StatusDmn_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV19TFContratoSrvVnc_StatusDmn_Sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV19TFContratoSrvVnc_StatusDmn_Sels, "T1.[ContratoSrvVnc_StatusDmn] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoSrvVnc_ServicoVncSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoSrvVnc_ServicoVncSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV22TFContratoSrvVnc_ServicoVncSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] like @lV22TFContratoSrvVnc_ServicoVncSigla)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoSrvVnc_ServicoVncSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Servico_Sigla] = @AV23TFContratoSrvVnc_ServicoVncSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Servico_Sigla] = @AV23TFContratoSrvVnc_ServicoVncSigla_Sel)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV8OrderedBy == 1 ) && ! AV9OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV8OrderedBy == 1 ) && ( AV9OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV8OrderedBy == 2 ) && ! AV9OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV8OrderedBy == 2 ) && ( AV9OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV8OrderedBy == 3 ) && ! AV9OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV8OrderedBy == 3 ) && ( AV9OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00GD2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] );
               case 1 :
                     return conditional_H00GD3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GD2 ;
          prmH00GD2 = new Object[] {
          new Object[] {"@lV14TFContratoSrvVnc_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFContratoSrvVnc_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV22TFContratoSrvVnc_ServicoVncSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV23TFContratoSrvVnc_ServicoVncSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00GD3 ;
          prmH00GD3 = new Object[] {
          new Object[] {"@lV14TFContratoSrvVnc_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFContratoSrvVnc_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV22TFContratoSrvVnc_ServicoVncSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV23TFContratoSrvVnc_ServicoVncSigla_Sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GD2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GD2,11,0,true,false )
             ,new CursorDef("H00GD3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GD3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
       }
    }

 }

}
