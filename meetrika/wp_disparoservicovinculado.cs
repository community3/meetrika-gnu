/*
               File: WP_DisparoServicoVinculado
        Description: Disparo de Regras
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:52:12.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_disparoservicovinculado : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_disparoservicovinculado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_disparoservicovinculado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContratada_codigo = new GXCombobox();
         dynavContrato_codigo = new GXCombobox();
         cmbavContratoservicos_codigo2 = new GXCombobox();
         dynavContratosrvvnc_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV8Context);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGOSM2( AV8Context) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATO_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV8Context);
               AV9Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATO_CODIGOSM2( AV8Context, AV9Contratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATOSRVVNC_CODIGO") == 0 )
            {
               AV43ContratoServicos_Codigo2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoServicos_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43ContratoServicos_Codigo2), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATOSRVVNC_CODIGOSM2( AV43ContratoServicos_Codigo2) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PASM2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTSM2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423521294");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_disparoservicovinculado.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_boolean_hidden_field( context, "vCHECKREQUIREDFIELDSRESULT", AV39CheckRequiredFieldsResult);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOSOS", AV22CodigosOS);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOSOS", AV22CodigosOS);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCLIENTID", StringUtil.RTrim( AV47clientId));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTIFICATIONINFO", AV48NotificationInfo);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTIFICATIONINFO", AV48NotificationInfo);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV77Pgmname));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAHOST", A547Contratante_EmailSdaHost);
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAPORT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A552Contratante_EmailSdaPort), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAILSDAUSER", A548Contratante_EmailSdaUser);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIOS", AV51Usuarios);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIOS", AV51Usuarios);
         }
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_SERVICOSIGLA", StringUtil.RTrim( AV70ContagemResultado_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_DEMANDAFM", AV53ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_DESCRICAO", AV54ContagemResultado_Descricao);
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_DATADMN", context.localUtil.DToC( AV55ContagemResultado_DataDmn, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_DATAENTREGA", context.localUtil.DToC( AV67ContagemResultado_DataEntrega, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_OWNER_NOME", StringUtil.RTrim( AV69ContagemResultado_Owner_Nome));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68ContagemResultado_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vNEWCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65NewCodigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV62Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV62Codigos);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vATTACHMENTS", AV60Attachments);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vATTACHMENTS", AV60Attachments);
         }
         GxWebStd.gx_hidden_field( context, "vRESULTADO", StringUtil.RTrim( AV61Resultado));
         GxWebStd.gx_hidden_field( context, "vRETORNO", AV19Retorno);
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_PREPOSTOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1013Contrato_PrepostoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOGESTOR_USUARIOEHCONTRATANTE", A1135ContratoGestor_UsuarioEhContratante);
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DESCRICAO", A494ContagemResultado_Descricao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATADMN", context.localUtil.DToC( A471ContagemResultado_DataDmn, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATAENTREGA", context.localUtil.DToC( A472ContagemResultado_DataEntrega, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A508ContagemResultado_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICOSIGLA", StringUtil.RTrim( A801ContagemResultado_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_CNTSRVCOD", AV42ContagemResultado_CntSrvCod);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_CNTSRVCOD", AV42ContagemResultado_CntSrvCod);
         }
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOSERVICOS_ATIVO", A638ContratoServicos_Ativo);
         GxWebStd.gx_hidden_field( context, "SERVICO_IDENTIFICACAO", A2107Servico_Identificacao);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTEXT", AV8Context);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTEXT", AV8Context);
         }
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WESM2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTSM2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_disparoservicovinculado.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_DisparoServicoVinculado" ;
      }

      public override String GetPgmdesc( )
      {
         return "Disparo de Regras" ;
      }

      protected void WBSM0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_SM2( true) ;
         }
         else
         {
            wb_table1_2_SM2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_SM2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTSM2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Disparo de Regras", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPSM0( ) ;
      }

      protected void WSSM2( )
      {
         STARTSM2( ) ;
         EVTSM2( ) ;
      }

      protected void EVTSM2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11SM2 */
                              E11SM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12SM2 */
                                    E12SM2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13SM2 */
                              E13SM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTRATO_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14SM2 */
                              E14SM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15SM2 */
                              E15SM2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WESM2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PASM2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            dynavContratada_codigo.WebTags = "";
            dynavContrato_codigo.Name = "vCONTRATO_CODIGO";
            dynavContrato_codigo.WebTags = "";
            cmbavContratoservicos_codigo2.Name = "vCONTRATOSERVICOS_CODIGO2";
            cmbavContratoservicos_codigo2.WebTags = "";
            cmbavContratoservicos_codigo2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Selecione", 0);
            if ( cmbavContratoservicos_codigo2.ItemCount > 0 )
            {
               AV43ContratoServicos_Codigo2 = (int)(NumberUtil.Val( cmbavContratoservicos_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV43ContratoServicos_Codigo2), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoServicos_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43ContratoServicos_Codigo2), 6, 0)));
            }
            dynavContratosrvvnc_codigo.Name = "vCONTRATOSRVVNC_CODIGO";
            dynavContratosrvvnc_codigo.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavContratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvCONTRATO_CODIGO_htmlSM2( AV8Context, AV9Contratada_Codigo) ;
         GXVvCONTRATO_CODIGO_htmlSM2( AV8Context, AV9Contratada_Codigo) ;
         GXVvCONTRATOSRVVNC_CODIGO_htmlSM2( AV43ContratoServicos_Codigo2) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADA_CODIGOSM2( wwpbaseobjects.SdtWWPContext AV8Context )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO_dataSM2( AV8Context) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO_htmlSM2( wwpbaseobjects.SdtWWPContext AV8Context )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO_dataSM2( AV8Context) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV9Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratada_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO_dataSM2( wwpbaseobjects.SdtWWPContext AV8Context )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A39Contratada_Codigo ,
                                              AV23ContagemResultado_ContratadaCod ,
                                              AV8Context.gxTpr_Areatrabalho_codigo ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A43Contratada_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00SM2 */
         pr_default.execute(0, new Object[] {AV8Context.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00SM2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00SM2_A438Contratada_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTRATO_CODIGOSM2( wwpbaseobjects.SdtWWPContext AV8Context ,
                                               int AV9Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATO_CODIGO_dataSM2( AV8Context, AV9Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATO_CODIGO_htmlSM2( wwpbaseobjects.SdtWWPContext AV8Context ,
                                                  int AV9Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATO_CODIGO_dataSM2( AV8Context, AV9Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavContrato_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContrato_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContrato_codigo.ItemCount > 0 )
         {
            AV11Contrato_Codigo = (int)(NumberUtil.Val( dynavContrato_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11Contrato_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contrato_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATO_CODIGO_dataSM2( wwpbaseobjects.SdtWWPContext AV8Context ,
                                                    int AV9Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00SM3 */
         pr_default.execute(1, new Object[] {AV8Context.gxTpr_Areatrabalho_codigo, AV9Contratada_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00SM3_A74Contrato_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00SM3_A77Contrato_Numero[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvCONTRATOSRVVNC_CODIGOSM2( int AV43ContratoServicos_Codigo2 )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATOSRVVNC_CODIGO_dataSM2( AV43ContratoServicos_Codigo2) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATOSRVVNC_CODIGO_htmlSM2( int AV43ContratoServicos_Codigo2 )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATOSRVVNC_CODIGO_dataSM2( AV43ContratoServicos_Codigo2) ;
         gxdynajaxindex = 1;
         dynavContratosrvvnc_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratosrvvnc_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratosrvvnc_codigo.ItemCount > 0 )
         {
            AV13ContratoSrvVnc_Codigo = (int)(NumberUtil.Val( dynavContratosrvvnc_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13ContratoSrvVnc_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ContratoSrvVnc_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATOSRVVNC_CODIGO_dataSM2( int AV43ContratoServicos_Codigo2 )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00SM4 */
         pr_default.execute(2, new Object[] {AV43ContratoServicos_Codigo2});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00SM4_A917ContratoSrvVnc_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00SM4_A2108ContratoServicosVnc_Descricao[0]);
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV9Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratada_Codigo), 6, 0)));
         }
         if ( dynavContrato_codigo.ItemCount > 0 )
         {
            AV11Contrato_Codigo = (int)(NumberUtil.Val( dynavContrato_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11Contrato_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contrato_Codigo), 6, 0)));
         }
         if ( cmbavContratoservicos_codigo2.ItemCount > 0 )
         {
            AV43ContratoServicos_Codigo2 = (int)(NumberUtil.Val( cmbavContratoservicos_codigo2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV43ContratoServicos_Codigo2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoServicos_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43ContratoServicos_Codigo2), 6, 0)));
         }
         if ( dynavContratosrvvnc_codigo.ItemCount > 0 )
         {
            AV13ContratoSrvVnc_Codigo = (int)(NumberUtil.Val( dynavContratosrvvnc_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13ContratoSrvVnc_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ContratoSrvVnc_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFSM2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV77Pgmname = "WP_DisparoServicoVinculado";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Pgmname", AV77Pgmname);
         context.Gx_err = 0;
      }

      protected void RFSM2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E13SM2 */
         E13SM2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E15SM2 */
            E15SM2 ();
            WBSM0( ) ;
         }
      }

      protected void STRUPSM0( )
      {
         /* Before Start, stand alone formulas. */
         AV77Pgmname = "WP_DisparoServicoVinculado";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Pgmname", AV77Pgmname);
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11SM2 */
         E11SM2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTRATADA_CODIGO_htmlSM2( AV8Context) ;
         GXVvCONTRATO_CODIGO_htmlSM2( AV8Context, AV9Contratada_Codigo) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavContratada_codigo.CurrentValue = cgiGet( dynavContratada_codigo_Internalname);
            AV9Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratada_Codigo), 6, 0)));
            dynavContrato_codigo.CurrentValue = cgiGet( dynavContrato_codigo_Internalname);
            AV11Contrato_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContrato_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contrato_Codigo), 6, 0)));
            cmbavContratoservicos_codigo2.CurrentValue = cgiGet( cmbavContratoservicos_codigo2_Internalname);
            AV43ContratoServicos_Codigo2 = (int)(NumberUtil.Val( cgiGet( cmbavContratoservicos_codigo2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoServicos_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43ContratoServicos_Codigo2), 6, 0)));
            dynavContratosrvvnc_codigo.CurrentValue = cgiGet( dynavContratosrvvnc_codigo_Internalname);
            AV13ContratoSrvVnc_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratosrvvnc_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ContratoSrvVnc_Codigo), 6, 0)));
            /* Read saved values. */
            Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
            Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
            Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
            Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
            Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
            Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
            Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
            Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
            Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
            Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvCONTRATADA_CODIGO_htmlSM2( AV8Context) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11SM2 */
         E11SM2 ();
         if (returnInSub) return;
      }

      protected void E11SM2( )
      {
         /* Start Routine */
         AV47clientId = AV46webnotification.gxTpr_Clientid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47clientId", AV47clientId);
         AV19Retorno = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Retorno", AV19Retorno);
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8Context) ;
         AV22CodigosOS.FromXml(AV24WebSession.Get("CodigosOS"), "Collection");
         /* Execute user subroutine: 'CARREGA.COMBOS' */
         S112 ();
         if (returnInSub) return;
      }

      public void GXEnter( )
      {
         /* Execute user event: E12SM2 */
         E12SM2 ();
         if (returnInSub) return;
      }

      protected void E12SM2( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'CHECKREQUIREDFIELDS' */
         S122 ();
         if (returnInSub) return;
         if ( AV39CheckRequiredFieldsResult )
         {
            /* Execute user subroutine: 'EXECULTA.REGRAS' */
            S132 ();
            if (returnInSub) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48NotificationInfo", AV48NotificationInfo);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62Codigos", AV62Codigos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51Usuarios", AV51Usuarios);
      }

      protected void S122( )
      {
         /* 'CHECKREQUIREDFIELDS' Routine */
         AV39CheckRequiredFieldsResult = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39CheckRequiredFieldsResult", AV39CheckRequiredFieldsResult);
         if ( (0==AV9Contratada_Codigo) )
         {
            GX_msglist.addItem("Contratada � obrigat�rio.");
            AV39CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39CheckRequiredFieldsResult", AV39CheckRequiredFieldsResult);
         }
         if ( (0==AV11Contrato_Codigo) )
         {
            GX_msglist.addItem("Contrato � obrigat�rio.");
            AV39CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39CheckRequiredFieldsResult", AV39CheckRequiredFieldsResult);
         }
         if ( (0==AV43ContratoServicos_Codigo2) )
         {
            GX_msglist.addItem("Servi�o � obrigat�rio.");
            AV39CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39CheckRequiredFieldsResult", AV39CheckRequiredFieldsResult);
         }
         if ( (0==AV13ContratoSrvVnc_Codigo) )
         {
            GX_msglist.addItem("Regra � obrigat�rio.");
            AV39CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39CheckRequiredFieldsResult", AV39CheckRequiredFieldsResult);
         }
      }

      protected void S152( )
      {
         /* 'SHOW MESSAGES' Routine */
         AV73GXV1 = 1;
         while ( AV73GXV1 <= AV7Messages.Count )
         {
            AV6Message = ((SdtMessages_Message)AV7Messages.Item(AV73GXV1));
            GX_msglist.addItem(AV6Message.gxTpr_Description);
            AV73GXV1 = (int)(AV73GXV1+1);
         }
      }

      protected void E13SM2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8Context) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV8Context", AV8Context);
      }

      protected void E14SM2( )
      {
         /* Contrato_codigo_Click Routine */
         /* Execute user subroutine: 'CARREGA.CONTRATO.SERVICOS' */
         S142 ();
         if (returnInSub) return;
         cmbavContratoservicos_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV43ContratoServicos_Codigo2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo2_Internalname, "Values", cmbavContratoservicos_codigo2.ToJavascriptSource());
      }

      protected void S112( )
      {
         /* 'CARREGA.COMBOS' Routine */
         AV23ContagemResultado_ContratadaCod.Clear();
         AV42ContagemResultado_CntSrvCod.Clear();
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV22CodigosOS },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor H00SM5 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            A456ContagemResultado_Codigo = H00SM5_A456ContagemResultado_Codigo[0];
            A490ContagemResultado_ContratadaCod = H00SM5_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00SM5_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = H00SM5_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00SM5_n1553ContagemResultado_CntSrvCod[0];
            if ( (0==AV23ContagemResultado_ContratadaCod.IndexOf(A490ContagemResultado_ContratadaCod)) )
            {
               AV23ContagemResultado_ContratadaCod.Add(A490ContagemResultado_ContratadaCod, 0);
            }
            if ( (0==AV42ContagemResultado_CntSrvCod.IndexOf(A1553ContagemResultado_CntSrvCod)) )
            {
               AV42ContagemResultado_CntSrvCod.Add(A1553ContagemResultado_CntSrvCod, 0);
            }
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void S142( )
      {
         /* 'CARREGA.CONTRATO.SERVICOS' Routine */
         cmbavContratoservicos_codigo2.removeAllItems();
         cmbavContratoservicos_codigo2.addItem("0", "Selecione", 0);
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A160ContratoServicos_Codigo ,
                                              AV42ContagemResultado_CntSrvCod ,
                                              A638ContratoServicos_Ativo ,
                                              AV11Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00SM6 */
         pr_default.execute(4, new Object[] {AV11Contrato_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A155Servico_Codigo = H00SM6_A155Servico_Codigo[0];
            A638ContratoServicos_Ativo = H00SM6_A638ContratoServicos_Ativo[0];
            A160ContratoServicos_Codigo = H00SM6_A160ContratoServicos_Codigo[0];
            A74Contrato_Codigo = H00SM6_A74Contrato_Codigo[0];
            A608Servico_Nome = H00SM6_A608Servico_Nome[0];
            A605Servico_Sigla = H00SM6_A605Servico_Sigla[0];
            A608Servico_Nome = H00SM6_A608Servico_Nome[0];
            A605Servico_Sigla = H00SM6_A605Servico_Sigla[0];
            A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2107Servico_Identificacao", A2107Servico_Identificacao);
            cmbavContratoservicos_codigo2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)), A2107Servico_Identificacao, 0);
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void S132( )
      {
         /* 'EXECULTA.REGRAS' Routine */
         AV45RetornoAux = "";
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV22CodigosOS ,
                                              A490ContagemResultado_ContratadaCod ,
                                              AV9Contratada_Codigo ,
                                              A1553ContagemResultado_CntSrvCod ,
                                              AV43ContratoServicos_Codigo2 },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         /* Using cursor H00SM7 */
         pr_default.execute(5, new Object[] {AV9Contratada_Codigo, AV43ContratoServicos_Codigo2});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00SM7_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00SM7_n1553ContagemResultado_CntSrvCod[0];
            A490ContagemResultado_ContratadaCod = H00SM7_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00SM7_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = H00SM7_A456ContagemResultado_Codigo[0];
            AV24WebSession.Set("ProximaEtapa", StringUtil.Trim( StringUtil.Str( (decimal)(AV13ContratoSrvVnc_Codigo), 6, 0)));
            new prc_disparoservicovinculado(context ).execute(  A456ContagemResultado_Codigo,  AV8Context.gxTpr_Userid) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            if ( ! (Convert.ToDecimal(0)==NumberUtil.Val( AV24WebSession.Get("&NewCodigo"), ".")) )
            {
               AV45RetornoAux = AV45RetornoAux + StringUtil.Trim( AV24WebSession.Get("&ContagemResultado_OsFsOsFm")) + " / ";
            }
            pr_default.readNext(5);
         }
         pr_default.close(5);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45RetornoAux)) )
         {
            AV19Retorno = StringUtil.Format( "As seguintes demandas foram criadas a partir das op��es elecionadas: %1.", StringUtil.Trim( AV45RetornoAux), "", "", "", "", "", "", "", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Retorno", AV19Retorno);
            /* Execute user subroutine: 'ENVIO.NOTIFICACAO.EMAIL' */
            S162 ();
            if (returnInSub) return;
         }
         else
         {
            AV19Retorno = "Nenhuma demanda foi criada de acordo com as op��es selecionadas.";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Retorno", AV19Retorno);
         }
         AV24WebSession.Remove("&NewCodigo");
         AV24WebSession.Remove("&ContagemResultado_OsFsOsFm");
         AV48NotificationInfo.gxTpr_Id = AV47clientId;
         AV48NotificationInfo.gxTpr_Message = AV19Retorno;
         AV48NotificationInfo.gxTpr_Object = "WP_MonitorDmn";
         AV46webnotification.notifyclient( AV47clientId,  AV48NotificationInfo);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47clientId", AV47clientId);
         new geralog(context ).execute( ref  AV77Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Pgmname", AV77Pgmname);
         GXt_char1 = "Dados da Notifica��o: " + AV48NotificationInfo.ToXml(false, true, "NotificationInfo", "GxEv3Up14_Meetrika");
         new geralog(context ).execute( ref  GXt_char1) ;
         new geralog(context ).execute( ref  AV77Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Pgmname", AV77Pgmname);
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S162( )
      {
         /* 'ENVIO.NOTIFICACAO.EMAIL' Routine */
         /* Using cursor H00SM8 */
         pr_default.execute(6, new Object[] {AV8Context.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A29Contratante_Codigo = H00SM8_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00SM8_n29Contratante_Codigo[0];
            A548Contratante_EmailSdaUser = H00SM8_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = H00SM8_n548Contratante_EmailSdaUser[0];
            A552Contratante_EmailSdaPort = H00SM8_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = H00SM8_n552Contratante_EmailSdaPort[0];
            A547Contratante_EmailSdaHost = H00SM8_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = H00SM8_n547Contratante_EmailSdaHost[0];
            A5AreaTrabalho_Codigo = H00SM8_A5AreaTrabalho_Codigo[0];
            A548Contratante_EmailSdaUser = H00SM8_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = H00SM8_n548Contratante_EmailSdaUser[0];
            A552Contratante_EmailSdaPort = H00SM8_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = H00SM8_n552Contratante_EmailSdaPort[0];
            A547Contratante_EmailSdaHost = H00SM8_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = H00SM8_n547Contratante_EmailSdaHost[0];
            AV50ContratanteSemEmailSda = true;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(6);
         if ( AV50ContratanteSemEmailSda )
         {
            AV61Resultado = "Cadastro da Contratante sem dados configurados para envio de e-mails!";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Resultado", AV61Resultado);
         }
         else
         {
            /* Execute user subroutine: 'GETEMAILS' */
            S172 ();
            if (returnInSub) return;
            if ( AV51Usuarios.Count > 0 )
            {
               /* Execute user subroutine: 'BUSCA.DADOS.NOVA.OS' */
               S182 ();
               if (returnInSub) return;
               AV49EmailText = StringUtil.NewLine( ) + "Prezado(a)," + StringUtil.NewLine( );
               AV49EmailText = AV49EmailText + "H� uma nova solicita��o para a prestadora do servi�o de " + AV70ContagemResultado_ServicoSigla + " conforme segue abaixo os detalhes." + StringUtil.NewLine( ) + StringUtil.NewLine( );
               AV49EmailText = AV49EmailText + "No da OS�������������������" + AV53ContagemResultado_DemandaFM + StringUtil.NewLine( );
               AV49EmailText = AV49EmailText + "Servi�o��������������������" + AV70ContagemResultado_ServicoSigla + StringUtil.NewLine( );
               AV49EmailText = AV49EmailText + "Descri��o������������������" + StringUtil.Trim( AV54ContagemResultado_Descricao) + StringUtil.NewLine( );
               AV49EmailText = AV49EmailText + "Data de Abertura�����������" + context.localUtil.DToC( AV55ContagemResultado_DataDmn, 2, "/") + StringUtil.NewLine( );
               AV49EmailText = AV49EmailText + "Expectativa de Atendimento�" + context.localUtil.DToC( AV67ContagemResultado_DataEntrega, 2, "/") + StringUtil.NewLine( );
               AV49EmailText = AV49EmailText + "Requisitante���������������" + AV69ContagemResultado_Owner_Nome + StringUtil.NewLine( );
               AV49EmailText = AV49EmailText + "Telefone�������������������" + AV8Context.gxTpr_Contratante_telefone + StringUtil.NewLine( );
               AV49EmailText = AV49EmailText + "Unidade Organizacional�����" + StringUtil.Trim( AV8Context.gxTpr_Areatrabalho_descricao) + StringUtil.NewLine( ) + StringUtil.NewLine( );
               AV49EmailText = AV49EmailText + "Para atendimento da demanda acesse o MEETRIKA - Sistema de Gest�o Contratual e Automa��o de Processos " + StringUtil.NewLine( );
               AV49EmailText = AV49EmailText + StringUtil.NewLine( ) + StringUtil.Trim( AV8Context.gxTpr_Areatrabalho_descricao) + ", usu�rio " + StringUtil.Trim( AV8Context.gxTpr_Username) + StringUtil.NewLine( );
               if ( AV68ContagemResultado_Owner > 0 )
               {
                  AV49EmailText = AV49EmailText + " (autorizado por " + AV69ContagemResultado_Owner_Nome + ")" + StringUtil.NewLine( );
               }
               else
               {
                  AV49EmailText = AV49EmailText + StringUtil.NewLine( );
               }
               AV49EmailText = AV49EmailText + StringUtil.NewLine( ) + StringUtil.NewLine( ) + "Esta mensagem pode conter informa��o confidencial e/ou privilegiada. Se voc� n�o for o destinat�rio ou a pessoa autorizada a receber esta mensagem, n�o pode usar, copiar ou divulgar as informa��es nela contidas ou tomar qualquer a��o baseada nessas informa��es. Se voc� recebeu esta mensagem por engano, por favor avise imediatamente o remetente, respondendo o e-mail e em seguida apague-o.";
               AV58Subject = "[" + StringUtil.Trim( AV8Context.gxTpr_Parametrossistema_nomesistema) + " - " + StringUtil.Trim( AV8Context.gxTpr_Areatrabalho_descricao) + "] OS " + StringUtil.Trim( AV53ContagemResultado_DemandaFM) + " solicitada" + " (No reply)";
               AV62Codigos.Add(AV65NewCodigo, 0);
               AV24WebSession.Set("DemandaCodigo", AV62Codigos.ToXml(false, true, "Collection", ""));
               new prc_enviaremail(context ).execute(  AV8Context.gxTpr_Areatrabalho_codigo,  AV51Usuarios,  AV58Subject,  AV49EmailText,  AV60Attachments, ref  AV61Resultado) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Resultado", AV61Resultado);
               AV24WebSession.Remove("DemandaCodigo");
            }
            else
            {
               AV61Resultado = "Sem usu�rio preposto no requisitado para envio da notifica��o!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Resultado", AV61Resultado);
            }
            AV19Retorno = AV19Retorno + StringUtil.NewLine( ) + AV61Resultado;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Retorno", AV19Retorno);
         }
      }

      protected void S172( )
      {
         /* 'GETEMAILS' Routine */
         AV51Usuarios.Clear();
         /* Using cursor H00SM9 */
         pr_default.execute(7, new Object[] {AV11Contrato_Codigo});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A74Contrato_Codigo = H00SM9_A74Contrato_Codigo[0];
            A75Contrato_AreaTrabalhoCod = H00SM9_A75Contrato_AreaTrabalhoCod[0];
            A39Contratada_Codigo = H00SM9_A39Contratada_Codigo[0];
            A92Contrato_Ativo = H00SM9_A92Contrato_Ativo[0];
            A52Contratada_AreaTrabalhoCod = H00SM9_A52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00SM9_A43Contratada_Ativo[0];
            A1013Contrato_PrepostoCod = H00SM9_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00SM9_n1013Contrato_PrepostoCod[0];
            A52Contratada_AreaTrabalhoCod = H00SM9_A52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00SM9_A43Contratada_Ativo[0];
            if ( A1013Contrato_PrepostoCod > 0 )
            {
               AV51Usuarios.Add(A1013Contrato_PrepostoCod, 0);
            }
            else
            {
               /* Using cursor H00SM10 */
               pr_default.execute(8, new Object[] {A74Contrato_Codigo});
               while ( (pr_default.getStatus(8) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = H00SM10_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = H00SM10_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00SM10_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00SM10_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00SM10_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00SM10_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean2 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean2) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean2;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                  if ( ! A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV51Usuarios.Add(A1079ContratoGestor_UsuarioCod, 0);
                  }
                  pr_default.readNext(8);
               }
               pr_default.close(8);
            }
            /* Using cursor H00SM11 */
            pr_default.execute(9, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A1078ContratoGestor_ContratoCod = H00SM11_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = H00SM11_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = H00SM11_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = H00SM11_n1446ContratoGestor_ContratadaAreaCod[0];
               A1446ContratoGestor_ContratadaAreaCod = H00SM11_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = H00SM11_n1446ContratoGestor_ContratadaAreaCod[0];
               GXt_boolean2 = A1135ContratoGestor_UsuarioEhContratante;
               new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean2) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
               A1135ContratoGestor_UsuarioEhContratante = GXt_boolean2;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
               if ( A1135ContratoGestor_UsuarioEhContratante )
               {
                  AV51Usuarios.Add(A1079ContratoGestor_UsuarioCod, 0);
               }
               pr_default.readNext(9);
            }
            pr_default.close(9);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(7);
      }

      protected void S182( )
      {
         /* 'BUSCA.DADOS.NOVA.OS' Routine */
         AV65NewCodigo = (int)(NumberUtil.Val( AV24WebSession.Get("&NewCodigo"), "."));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65NewCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65NewCodigo), 6, 0)));
         AV54ContagemResultado_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_Descricao", AV54ContagemResultado_Descricao);
         AV53ContagemResultado_DemandaFM = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContagemResultado_DemandaFM", AV53ContagemResultado_DemandaFM);
         AV55ContagemResultado_DataDmn = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_DataDmn", context.localUtil.Format(AV55ContagemResultado_DataDmn, "99/99/99"));
         AV67ContagemResultado_DataEntrega = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_DataEntrega", context.localUtil.Format(AV67ContagemResultado_DataEntrega, "99/99/99"));
         AV68ContagemResultado_Owner = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ContagemResultado_Owner), 6, 0)));
         AV69ContagemResultado_Owner_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_Owner_Nome", AV69ContagemResultado_Owner_Nome);
         /* Using cursor H00SM13 */
         pr_default.execute(10, new Object[] {AV65NewCodigo});
         while ( (pr_default.getStatus(10) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00SM13_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00SM13_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = H00SM13_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00SM13_n601ContagemResultado_Servico[0];
            A456ContagemResultado_Codigo = H00SM13_A456ContagemResultado_Codigo[0];
            A494ContagemResultado_Descricao = H00SM13_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = H00SM13_n494ContagemResultado_Descricao[0];
            A493ContagemResultado_DemandaFM = H00SM13_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00SM13_n493ContagemResultado_DemandaFM[0];
            A471ContagemResultado_DataDmn = H00SM13_A471ContagemResultado_DataDmn[0];
            A472ContagemResultado_DataEntrega = H00SM13_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = H00SM13_n472ContagemResultado_DataEntrega[0];
            A508ContagemResultado_Owner = H00SM13_A508ContagemResultado_Owner[0];
            A801ContagemResultado_ServicoSigla = H00SM13_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00SM13_n801ContagemResultado_ServicoSigla[0];
            A40000Usuario_PessoaNom = H00SM13_A40000Usuario_PessoaNom[0];
            n40000Usuario_PessoaNom = H00SM13_n40000Usuario_PessoaNom[0];
            A601ContagemResultado_Servico = H00SM13_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00SM13_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = H00SM13_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00SM13_n801ContagemResultado_ServicoSigla[0];
            A40000Usuario_PessoaNom = H00SM13_A40000Usuario_PessoaNom[0];
            n40000Usuario_PessoaNom = H00SM13_n40000Usuario_PessoaNom[0];
            AV54ContagemResultado_Descricao = A494ContagemResultado_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ContagemResultado_Descricao", AV54ContagemResultado_Descricao);
            AV53ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContagemResultado_DemandaFM", AV53ContagemResultado_DemandaFM);
            AV55ContagemResultado_DataDmn = A471ContagemResultado_DataDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_DataDmn", context.localUtil.Format(AV55ContagemResultado_DataDmn, "99/99/99"));
            AV67ContagemResultado_DataEntrega = A472ContagemResultado_DataEntrega;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_DataEntrega", context.localUtil.Format(AV67ContagemResultado_DataEntrega, "99/99/99"));
            AV68ContagemResultado_Owner = A508ContagemResultado_Owner;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ContagemResultado_Owner), 6, 0)));
            AV69ContagemResultado_Owner_Nome = A40000Usuario_PessoaNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_Owner_Nome", AV69ContagemResultado_Owner_Nome);
            AV70ContagemResultado_ServicoSigla = A801ContagemResultado_ServicoSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ContagemResultado_ServicoSigla", AV70ContagemResultado_ServicoSigla);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(10);
      }

      protected void nextLoad( )
      {
      }

      protected void E15SM2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_SM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_SM2( true) ;
         }
         else
         {
            wb_table2_5_SM2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_SM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table3_39_SM2( true) ;
         }
         else
         {
            wb_table3_39_SM2( false) ;
         }
         return  ;
      }

      protected void wb_table3_39_SM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_SM2e( true) ;
         }
         else
         {
            wb_table1_2_SM2e( false) ;
         }
      }

      protected void wb_table3_39_SM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_DisparoServicoVinculado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_DisparoServicoVinculado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_39_SM2e( true) ;
         }
         else
         {
            wb_table3_39_SM2e( false) ;
         }
      }

      protected void wb_table2_5_SM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_SM2( true) ;
         }
         else
         {
            wb_table4_13_SM2( false) ;
         }
         return  ;
      }

      protected void wb_table4_13_SM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_SM2e( true) ;
         }
         else
         {
            wb_table2_5_SM2e( false) ;
         }
      }

      protected void wb_table4_13_SM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(500), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_16_SM2( true) ;
         }
         else
         {
            wb_table5_16_SM2( false) ;
         }
         return  ;
      }

      protected void wb_table5_16_SM2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_SM2e( true) ;
         }
         else
         {
            wb_table4_13_SM2e( false) ;
         }
      }

      protected void wb_table5_16_SM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable_Internalname, tblTable_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_codigo_Internalname, "Contratada", "", "", lblTextblockcontratada_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DisparoServicoVinculado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo, dynavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9Contratada_Codigo), 6, 0)), 1, dynavContratada_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "", true, "HLP_WP_DisparoServicoVinculado.htm");
            dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", (String)(dynavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_codigo_Internalname, "Contrato", "", "", lblTextblockcontrato_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DisparoServicoVinculado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContrato_codigo, dynavContrato_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV11Contrato_Codigo), 6, 0)), 1, dynavContrato_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATO_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "", true, "HLP_WP_DisparoServicoVinculado.htm");
            dynavContrato_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV11Contrato_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContrato_codigo_Internalname, "Values", (String)(dynavContrato_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_codigo2_Internalname, "Servi�o", "", "", lblTextblockcontratoservicos_codigo2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DisparoServicoVinculado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratoservicos_codigo2, cmbavContratoservicos_codigo2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV43ContratoServicos_Codigo2), 6, 0)), 1, cmbavContratoservicos_codigo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_WP_DisparoServicoVinculado.htm");
            cmbavContratoservicos_codigo2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV43ContratoServicos_Codigo2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_codigo2_Internalname, "Values", (String)(cmbavContratoservicos_codigo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_codigo_Internalname, "Regra", "", "", lblTextblockcontratosrvvnc_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DisparoServicoVinculado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratosrvvnc_codigo, dynavContratosrvvnc_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13ContratoSrvVnc_Codigo), 6, 0)), 1, dynavContratosrvvnc_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_WP_DisparoServicoVinculado.htm");
            dynavContratosrvvnc_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13ContratoSrvVnc_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratosrvvnc_codigo_Internalname, "Values", (String)(dynavContratosrvvnc_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_16_SM2e( true) ;
         }
         else
         {
            wb_table5_16_SM2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PASM2( ) ;
         WSSM2( ) ;
         WESM2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423521395");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_disparoservicovinculado.js", "?202032423521396");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratada_codigo_Internalname = "TEXTBLOCKCONTRATADA_CODIGO";
         dynavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         lblTextblockcontrato_codigo_Internalname = "TEXTBLOCKCONTRATO_CODIGO";
         dynavContrato_codigo_Internalname = "vCONTRATO_CODIGO";
         lblTextblockcontratoservicos_codigo2_Internalname = "TEXTBLOCKCONTRATOSERVICOS_CODIGO2";
         cmbavContratoservicos_codigo2_Internalname = "vCONTRATOSERVICOS_CODIGO2";
         lblTextblockcontratosrvvnc_codigo_Internalname = "TEXTBLOCKCONTRATOSRVVNC_CODIGO";
         dynavContratosrvvnc_codigo_Internalname = "vCONTRATOSRVVNC_CODIGO";
         tblTable_Internalname = "TABLE";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         dynavContratosrvvnc_codigo_Jsonclick = "";
         cmbavContratoservicos_codigo2_Jsonclick = "";
         dynavContrato_codigo_Jsonclick = "";
         dynavContratada_codigo_Jsonclick = "";
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Informa��es";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Disparo de Regras";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Contratada_codigo( wwpbaseobjects.SdtWWPContext GX_Parm1 ,
                                            GXCombobox dynGX_Parm2 ,
                                            GXCombobox dynGX_Parm3 )
      {
         AV8Context = GX_Parm1;
         dynavContratada_codigo = dynGX_Parm2;
         AV9Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.CurrentValue, "."));
         dynavContrato_codigo = dynGX_Parm3;
         AV11Contrato_Codigo = (int)(NumberUtil.Val( dynavContrato_codigo.CurrentValue, "."));
         GXVvCONTRATO_CODIGO_htmlSM2( AV8Context, AV9Contratada_Codigo) ;
         dynload_actions( ) ;
         if ( dynavContrato_codigo.ItemCount > 0 )
         {
            AV11Contrato_Codigo = (int)(NumberUtil.Val( dynavContrato_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11Contrato_Codigo), 6, 0))), "."));
         }
         dynavContrato_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV11Contrato_Codigo), 6, 0));
         isValidOutput.Add(dynavContrato_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contratoservicos_codigo2( GXCombobox cmbGX_Parm1 ,
                                                   GXCombobox dynGX_Parm2 )
      {
         cmbavContratoservicos_codigo2 = cmbGX_Parm1;
         AV43ContratoServicos_Codigo2 = (int)(NumberUtil.Val( cmbavContratoservicos_codigo2.CurrentValue, "."));
         dynavContratosrvvnc_codigo = dynGX_Parm2;
         AV13ContratoSrvVnc_Codigo = (int)(NumberUtil.Val( dynavContratosrvvnc_codigo.CurrentValue, "."));
         GXVvCONTRATOSRVVNC_CODIGO_htmlSM2( AV43ContratoServicos_Codigo2) ;
         dynload_actions( ) ;
         if ( dynavContratosrvvnc_codigo.ItemCount > 0 )
         {
            AV13ContratoSrvVnc_Codigo = (int)(NumberUtil.Val( dynavContratosrvvnc_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13ContratoSrvVnc_Codigo), 6, 0))), "."));
         }
         dynavContratosrvvnc_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13ContratoSrvVnc_Codigo), 6, 0));
         isValidOutput.Add(dynavContratosrvvnc_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[{av:'AV8Context',fld:'vCONTEXT',pic:'',nv:null}]}");
         setEventMetadata("ENTER","{handler:'E12SM2',iparms:[{av:'AV39CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV9Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV13ContratoSrvVnc_Codigo',fld:'vCONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22CodigosOS',fld:'vCODIGOSOS',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV8Context',fld:'vCONTEXT',pic:'',nv:null},{av:'AV47clientId',fld:'vCLIENTID',pic:'',nv:''},{av:'AV48NotificationInfo',fld:'vNOTIFICATIONINFO',pic:'',nv:null},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A547Contratante_EmailSdaHost',fld:'CONTRATANTE_EMAILSDAHOST',pic:'',nv:''},{av:'A552Contratante_EmailSdaPort',fld:'CONTRATANTE_EMAILSDAPORT',pic:'ZZZ9',nv:0},{av:'A548Contratante_EmailSdaUser',fld:'CONTRATANTE_EMAILSDAUSER',pic:'',nv:''},{av:'AV51Usuarios',fld:'vUSUARIOS',pic:'',nv:null},{av:'AV70ContagemResultado_ServicoSigla',fld:'vCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV53ContagemResultado_DemandaFM',fld:'vCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV54ContagemResultado_Descricao',fld:'vCONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'AV55ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV67ContagemResultado_DataEntrega',fld:'vCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV69ContagemResultado_Owner_Nome',fld:'vCONTAGEMRESULTADO_OWNER_NOME',pic:'@!',nv:''},{av:'AV68ContagemResultado_Owner',fld:'vCONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV65NewCodigo',fld:'vNEWCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV60Attachments',fld:'vATTACHMENTS',pic:'',nv:null},{av:'AV61Resultado',fld:'vRESULTADO',pic:'',nv:''},{av:'AV19Retorno',fld:'vRETORNO',pic:'',nv:''},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A472ContagemResultado_DataEntrega',fld:'CONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''}],oparms:[{av:'AV39CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV19Retorno',fld:'vRETORNO',pic:'',nv:''},{av:'AV48NotificationInfo',fld:'vNOTIFICATIONINFO',pic:'',nv:null},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV62Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV61Resultado',fld:'vRESULTADO',pic:'',nv:''},{av:'AV51Usuarios',fld:'vUSUARIOS',pic:'',nv:null},{av:'AV65NewCodigo',fld:'vNEWCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV54ContagemResultado_Descricao',fld:'vCONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'AV53ContagemResultado_DemandaFM',fld:'vCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV55ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV67ContagemResultado_DataEntrega',fld:'vCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV68ContagemResultado_Owner',fld:'vCONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_Owner_Nome',fld:'vCONTAGEMRESULTADO_OWNER_NOME',pic:'@!',nv:''},{av:'AV70ContagemResultado_ServicoSigla',fld:'vCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''}]}");
         setEventMetadata("VCONTRATO_CODIGO.CLICK","{handler:'E14SM2',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ContagemResultado_CntSrvCod',fld:'vCONTAGEMRESULTADO_CNTSRVCOD',pic:'',nv:null},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A2107Servico_Identificacao',fld:'SERVICO_IDENTIFICACAO',pic:'',nv:''}],oparms:[{av:'AV43ContratoServicos_Codigo2',fld:'vCONTRATOSERVICOS_CODIGO2',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV8Context = new wwpbaseobjects.SdtWWPContext(context);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV22CodigosOS = new GxSimpleCollection();
         AV47clientId = "";
         AV48NotificationInfo = new SdtNotificationInfo(context);
         AV77Pgmname = "";
         A547Contratante_EmailSdaHost = "";
         A548Contratante_EmailSdaUser = "";
         AV51Usuarios = new GxSimpleCollection();
         AV70ContagemResultado_ServicoSigla = "";
         AV53ContagemResultado_DemandaFM = "";
         AV54ContagemResultado_Descricao = "";
         AV55ContagemResultado_DataDmn = DateTime.MinValue;
         AV67ContagemResultado_DataEntrega = DateTime.MinValue;
         AV69ContagemResultado_Owner_Nome = "";
         AV62Codigos = new GxSimpleCollection();
         AV60Attachments = new GxSimpleCollection();
         AV61Resultado = "";
         AV19Retorno = "";
         A494ContagemResultado_Descricao = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A801ContagemResultado_ServicoSigla = "";
         AV42ContagemResultado_CntSrvCod = new GxSimpleCollection();
         A2107Servico_Identificacao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         AV23ContagemResultado_ContratadaCod = new GxSimpleCollection();
         H00SM2_A39Contratada_Codigo = new int[1] ;
         H00SM2_A438Contratada_Sigla = new String[] {""} ;
         H00SM2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00SM2_A43Contratada_Ativo = new bool[] {false} ;
         H00SM3_A74Contrato_Codigo = new int[1] ;
         H00SM3_A77Contrato_Numero = new String[] {""} ;
         H00SM3_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00SM3_A92Contrato_Ativo = new bool[] {false} ;
         H00SM3_A39Contratada_Codigo = new int[1] ;
         H00SM4_A917ContratoSrvVnc_Codigo = new int[1] ;
         H00SM4_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         H00SM4_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         H00SM4_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         H00SM4_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         H00SM4_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         AV46webnotification = new SdtWebNotification(context);
         AV24WebSession = context.GetSession();
         AV7Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV6Message = new SdtMessages_Message(context);
         H00SM5_A456ContagemResultado_Codigo = new int[1] ;
         H00SM5_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00SM5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00SM5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00SM5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00SM6_A155Servico_Codigo = new int[1] ;
         H00SM6_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00SM6_A160ContratoServicos_Codigo = new int[1] ;
         H00SM6_A74Contrato_Codigo = new int[1] ;
         H00SM6_A608Servico_Nome = new String[] {""} ;
         H00SM6_A605Servico_Sigla = new String[] {""} ;
         A608Servico_Nome = "";
         A605Servico_Sigla = "";
         AV45RetornoAux = "";
         H00SM7_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00SM7_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00SM7_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00SM7_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00SM7_A456ContagemResultado_Codigo = new int[1] ;
         GXt_char1 = "";
         H00SM8_A29Contratante_Codigo = new int[1] ;
         H00SM8_n29Contratante_Codigo = new bool[] {false} ;
         H00SM8_A548Contratante_EmailSdaUser = new String[] {""} ;
         H00SM8_n548Contratante_EmailSdaUser = new bool[] {false} ;
         H00SM8_A552Contratante_EmailSdaPort = new short[1] ;
         H00SM8_n552Contratante_EmailSdaPort = new bool[] {false} ;
         H00SM8_A547Contratante_EmailSdaHost = new String[] {""} ;
         H00SM8_n547Contratante_EmailSdaHost = new bool[] {false} ;
         H00SM8_A5AreaTrabalho_Codigo = new int[1] ;
         AV49EmailText = "";
         AV58Subject = "";
         H00SM9_A74Contrato_Codigo = new int[1] ;
         H00SM9_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00SM9_A39Contratada_Codigo = new int[1] ;
         H00SM9_A92Contrato_Ativo = new bool[] {false} ;
         H00SM9_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00SM9_A43Contratada_Ativo = new bool[] {false} ;
         H00SM9_A1013Contrato_PrepostoCod = new int[1] ;
         H00SM9_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00SM10_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00SM10_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00SM10_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00SM10_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00SM11_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00SM11_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00SM11_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00SM11_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00SM13_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00SM13_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00SM13_A601ContagemResultado_Servico = new int[1] ;
         H00SM13_n601ContagemResultado_Servico = new bool[] {false} ;
         H00SM13_A456ContagemResultado_Codigo = new int[1] ;
         H00SM13_A494ContagemResultado_Descricao = new String[] {""} ;
         H00SM13_n494ContagemResultado_Descricao = new bool[] {false} ;
         H00SM13_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00SM13_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00SM13_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00SM13_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00SM13_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00SM13_A508ContagemResultado_Owner = new int[1] ;
         H00SM13_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00SM13_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00SM13_A40000Usuario_PessoaNom = new String[] {""} ;
         H00SM13_n40000Usuario_PessoaNom = new bool[] {false} ;
         A40000Usuario_PessoaNom = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnenter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblTextblockcontratada_codigo_Jsonclick = "";
         lblTextblockcontrato_codigo_Jsonclick = "";
         lblTextblockcontratoservicos_codigo2_Jsonclick = "";
         lblTextblockcontratosrvvnc_codigo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_disparoservicovinculado__default(),
            new Object[][] {
                new Object[] {
               H00SM2_A39Contratada_Codigo, H00SM2_A438Contratada_Sigla, H00SM2_A52Contratada_AreaTrabalhoCod, H00SM2_A43Contratada_Ativo
               }
               , new Object[] {
               H00SM3_A74Contrato_Codigo, H00SM3_A77Contrato_Numero, H00SM3_A75Contrato_AreaTrabalhoCod, H00SM3_A92Contrato_Ativo, H00SM3_A39Contratada_Codigo
               }
               , new Object[] {
               H00SM4_A917ContratoSrvVnc_Codigo, H00SM4_A2108ContratoServicosVnc_Descricao, H00SM4_n2108ContratoServicosVnc_Descricao, H00SM4_A915ContratoSrvVnc_CntSrvCod, H00SM4_A1453ContratoServicosVnc_Ativo, H00SM4_n1453ContratoServicosVnc_Ativo
               }
               , new Object[] {
               H00SM5_A456ContagemResultado_Codigo, H00SM5_A490ContagemResultado_ContratadaCod, H00SM5_n490ContagemResultado_ContratadaCod, H00SM5_A1553ContagemResultado_CntSrvCod, H00SM5_n1553ContagemResultado_CntSrvCod
               }
               , new Object[] {
               H00SM6_A155Servico_Codigo, H00SM6_A638ContratoServicos_Ativo, H00SM6_A160ContratoServicos_Codigo, H00SM6_A74Contrato_Codigo, H00SM6_A608Servico_Nome, H00SM6_A605Servico_Sigla
               }
               , new Object[] {
               H00SM7_A1553ContagemResultado_CntSrvCod, H00SM7_n1553ContagemResultado_CntSrvCod, H00SM7_A490ContagemResultado_ContratadaCod, H00SM7_n490ContagemResultado_ContratadaCod, H00SM7_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00SM8_A29Contratante_Codigo, H00SM8_n29Contratante_Codigo, H00SM8_A548Contratante_EmailSdaUser, H00SM8_n548Contratante_EmailSdaUser, H00SM8_A552Contratante_EmailSdaPort, H00SM8_n552Contratante_EmailSdaPort, H00SM8_A547Contratante_EmailSdaHost, H00SM8_n547Contratante_EmailSdaHost, H00SM8_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               H00SM9_A74Contrato_Codigo, H00SM9_A75Contrato_AreaTrabalhoCod, H00SM9_A39Contratada_Codigo, H00SM9_A92Contrato_Ativo, H00SM9_A52Contratada_AreaTrabalhoCod, H00SM9_A43Contratada_Ativo, H00SM9_A1013Contrato_PrepostoCod, H00SM9_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               H00SM10_A1078ContratoGestor_ContratoCod, H00SM10_A1079ContratoGestor_UsuarioCod, H00SM10_A1446ContratoGestor_ContratadaAreaCod, H00SM10_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00SM11_A1078ContratoGestor_ContratoCod, H00SM11_A1079ContratoGestor_UsuarioCod, H00SM11_A1446ContratoGestor_ContratadaAreaCod, H00SM11_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00SM13_A1553ContagemResultado_CntSrvCod, H00SM13_n1553ContagemResultado_CntSrvCod, H00SM13_A601ContagemResultado_Servico, H00SM13_n601ContagemResultado_Servico, H00SM13_A456ContagemResultado_Codigo, H00SM13_A494ContagemResultado_Descricao, H00SM13_n494ContagemResultado_Descricao, H00SM13_A493ContagemResultado_DemandaFM, H00SM13_n493ContagemResultado_DemandaFM, H00SM13_A471ContagemResultado_DataDmn,
               H00SM13_A472ContagemResultado_DataEntrega, H00SM13_n472ContagemResultado_DataEntrega, H00SM13_A508ContagemResultado_Owner, H00SM13_A801ContagemResultado_ServicoSigla, H00SM13_n801ContagemResultado_ServicoSigla, H00SM13_A40000Usuario_PessoaNom, H00SM13_n40000Usuario_PessoaNom
               }
            }
         );
         AV77Pgmname = "WP_DisparoServicoVinculado";
         /* GeneXus formulas. */
         AV77Pgmname = "WP_DisparoServicoVinculado";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short A552Contratante_EmailSdaPort ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private short wbTemp ;
      private int AV9Contratada_Codigo ;
      private int AV43ContratoServicos_Codigo2 ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A5AreaTrabalho_Codigo ;
      private int AV68ContagemResultado_Owner ;
      private int AV65NewCodigo ;
      private int A74Contrato_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A508ContagemResultado_Owner ;
      private int A160ContratoServicos_Codigo ;
      private int gxdynajaxindex ;
      private int AV8Context_gxTpr_Areatrabalho_codigo ;
      private int A39Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int AV11Contrato_Codigo ;
      private int AV13ContratoSrvVnc_Codigo ;
      private int AV73GXV1 ;
      private int A155Servico_Codigo ;
      private int A29Contratante_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A601ContagemResultado_Servico ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV47clientId ;
      private String AV77Pgmname ;
      private String AV70ContagemResultado_ServicoSigla ;
      private String AV69ContagemResultado_Owner_Nome ;
      private String AV61Resultado ;
      private String A801ContagemResultado_ServicoSigla ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String dynavContratada_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynavContrato_codigo_Internalname ;
      private String cmbavContratoservicos_codigo2_Internalname ;
      private String dynavContratosrvvnc_codigo_Internalname ;
      private String A608Servico_Nome ;
      private String A605Servico_Sigla ;
      private String GXt_char1 ;
      private String AV49EmailText ;
      private String AV58Subject ;
      private String A40000Usuario_PessoaNom ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String tblTable_Internalname ;
      private String lblTextblockcontratada_codigo_Internalname ;
      private String lblTextblockcontratada_codigo_Jsonclick ;
      private String dynavContratada_codigo_Jsonclick ;
      private String lblTextblockcontrato_codigo_Internalname ;
      private String lblTextblockcontrato_codigo_Jsonclick ;
      private String dynavContrato_codigo_Jsonclick ;
      private String lblTextblockcontratoservicos_codigo2_Internalname ;
      private String lblTextblockcontratoservicos_codigo2_Jsonclick ;
      private String cmbavContratoservicos_codigo2_Jsonclick ;
      private String lblTextblockcontratosrvvnc_codigo_Internalname ;
      private String lblTextblockcontratosrvvnc_codigo_Jsonclick ;
      private String dynavContratosrvvnc_codigo_Jsonclick ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime AV55ContagemResultado_DataDmn ;
      private DateTime AV67ContagemResultado_DataEntrega ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV39CheckRequiredFieldsResult ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool A638ContratoServicos_Ativo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A43Contratada_Ativo ;
      private bool returnInSub ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n29Contratante_Codigo ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n552Contratante_EmailSdaPort ;
      private bool n547Contratante_EmailSdaHost ;
      private bool AV50ContratanteSemEmailSda ;
      private bool A92Contrato_Ativo ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool GXt_boolean2 ;
      private bool n601ContagemResultado_Servico ;
      private bool n494ContagemResultado_Descricao ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n40000Usuario_PessoaNom ;
      private String A547Contratante_EmailSdaHost ;
      private String A548Contratante_EmailSdaUser ;
      private String AV53ContagemResultado_DemandaFM ;
      private String AV54ContagemResultado_Descricao ;
      private String AV19Retorno ;
      private String A494ContagemResultado_Descricao ;
      private String A493ContagemResultado_DemandaFM ;
      private String A2107Servico_Identificacao ;
      private String AV45RetornoAux ;
      private IGxSession AV24WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavContratada_codigo ;
      private GXCombobox dynavContrato_codigo ;
      private GXCombobox cmbavContratoservicos_codigo2 ;
      private GXCombobox dynavContratosrvvnc_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00SM2_A39Contratada_Codigo ;
      private String[] H00SM2_A438Contratada_Sigla ;
      private int[] H00SM2_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00SM2_A43Contratada_Ativo ;
      private int[] H00SM3_A74Contrato_Codigo ;
      private String[] H00SM3_A77Contrato_Numero ;
      private int[] H00SM3_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00SM3_A92Contrato_Ativo ;
      private int[] H00SM3_A39Contratada_Codigo ;
      private int[] H00SM4_A917ContratoSrvVnc_Codigo ;
      private String[] H00SM4_A2108ContratoServicosVnc_Descricao ;
      private bool[] H00SM4_n2108ContratoServicosVnc_Descricao ;
      private int[] H00SM4_A915ContratoSrvVnc_CntSrvCod ;
      private bool[] H00SM4_A1453ContratoServicosVnc_Ativo ;
      private bool[] H00SM4_n1453ContratoServicosVnc_Ativo ;
      private int[] H00SM5_A456ContagemResultado_Codigo ;
      private int[] H00SM5_A490ContagemResultado_ContratadaCod ;
      private bool[] H00SM5_n490ContagemResultado_ContratadaCod ;
      private int[] H00SM5_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00SM5_n1553ContagemResultado_CntSrvCod ;
      private int[] H00SM6_A155Servico_Codigo ;
      private bool[] H00SM6_A638ContratoServicos_Ativo ;
      private int[] H00SM6_A160ContratoServicos_Codigo ;
      private int[] H00SM6_A74Contrato_Codigo ;
      private String[] H00SM6_A608Servico_Nome ;
      private String[] H00SM6_A605Servico_Sigla ;
      private int[] H00SM7_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00SM7_n1553ContagemResultado_CntSrvCod ;
      private int[] H00SM7_A490ContagemResultado_ContratadaCod ;
      private bool[] H00SM7_n490ContagemResultado_ContratadaCod ;
      private int[] H00SM7_A456ContagemResultado_Codigo ;
      private int[] H00SM8_A29Contratante_Codigo ;
      private bool[] H00SM8_n29Contratante_Codigo ;
      private String[] H00SM8_A548Contratante_EmailSdaUser ;
      private bool[] H00SM8_n548Contratante_EmailSdaUser ;
      private short[] H00SM8_A552Contratante_EmailSdaPort ;
      private bool[] H00SM8_n552Contratante_EmailSdaPort ;
      private String[] H00SM8_A547Contratante_EmailSdaHost ;
      private bool[] H00SM8_n547Contratante_EmailSdaHost ;
      private int[] H00SM8_A5AreaTrabalho_Codigo ;
      private int[] H00SM9_A74Contrato_Codigo ;
      private int[] H00SM9_A75Contrato_AreaTrabalhoCod ;
      private int[] H00SM9_A39Contratada_Codigo ;
      private bool[] H00SM9_A92Contrato_Ativo ;
      private int[] H00SM9_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00SM9_A43Contratada_Ativo ;
      private int[] H00SM9_A1013Contrato_PrepostoCod ;
      private bool[] H00SM9_n1013Contrato_PrepostoCod ;
      private int[] H00SM10_A1078ContratoGestor_ContratoCod ;
      private int[] H00SM10_A1079ContratoGestor_UsuarioCod ;
      private int[] H00SM10_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00SM10_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00SM11_A1078ContratoGestor_ContratoCod ;
      private int[] H00SM11_A1079ContratoGestor_UsuarioCod ;
      private int[] H00SM11_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00SM11_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00SM13_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00SM13_n1553ContagemResultado_CntSrvCod ;
      private int[] H00SM13_A601ContagemResultado_Servico ;
      private bool[] H00SM13_n601ContagemResultado_Servico ;
      private int[] H00SM13_A456ContagemResultado_Codigo ;
      private String[] H00SM13_A494ContagemResultado_Descricao ;
      private bool[] H00SM13_n494ContagemResultado_Descricao ;
      private String[] H00SM13_A493ContagemResultado_DemandaFM ;
      private bool[] H00SM13_n493ContagemResultado_DemandaFM ;
      private DateTime[] H00SM13_A471ContagemResultado_DataDmn ;
      private DateTime[] H00SM13_A472ContagemResultado_DataEntrega ;
      private bool[] H00SM13_n472ContagemResultado_DataEntrega ;
      private int[] H00SM13_A508ContagemResultado_Owner ;
      private String[] H00SM13_A801ContagemResultado_ServicoSigla ;
      private bool[] H00SM13_n801ContagemResultado_ServicoSigla ;
      private String[] H00SM13_A40000Usuario_PessoaNom ;
      private bool[] H00SM13_n40000Usuario_PessoaNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV22CodigosOS ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV51Usuarios ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV62Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV42ContagemResultado_CntSrvCod ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV23ContagemResultado_ContratadaCod ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV60Attachments ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV7Messages ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8Context ;
      private SdtMessages_Message AV6Message ;
      private SdtNotificationInfo AV48NotificationInfo ;
      private SdtWebNotification AV46webnotification ;
   }

   public class wp_disparoservicovinculado__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00SM2( IGxContext context ,
                                             int A39Contratada_Codigo ,
                                             IGxCollection AV23ContagemResultado_ContratadaCod ,
                                             int AV8Context_gxTpr_Areatrabalho_codigo ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             bool A43Contratada_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [1] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [Contratada_Codigo], [Contratada_Sigla], [Contratada_AreaTrabalhoCod], [Contratada_Ativo] FROM [Contratada] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contratada_Ativo] = 1 and " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV23ContagemResultado_ContratadaCod, "[Contratada_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and ([Contratada_AreaTrabalhoCod] = @AV8Conte_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Contratada_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00SM5( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV22CodigosOS )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_Codigo], [ContagemResultado_ContratadaCod], [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV22CodigosOS, "[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_Codigo]";
         GXv_Object5[0] = scmdbuf;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H00SM6( IGxContext context ,
                                             int A160ContratoServicos_Codigo ,
                                             IGxCollection AV42ContagemResultado_CntSrvCod ,
                                             bool A638ContratoServicos_Ativo ,
                                             int AV11Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [1] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[Servico_Codigo], T1.[ContratoServicos_Ativo], T1.[ContratoServicos_Codigo], T1.[Contrato_Codigo], T2.[Servico_Nome], T2.[Servico_Sigla] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contrato_Codigo] = @AV11Contrato_Codigo)";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV42ContagemResultado_CntSrvCod, "T1.[ContratoServicos_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContratoServicos_Ativo] = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_Codigo]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_H00SM7( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV22CodigosOS ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int AV9Contratada_Codigo ,
                                             int A1553ContagemResultado_CntSrvCod ,
                                             int AV43ContratoServicos_Codigo2 )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [2] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_ContratadaCod], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV22CodigosOS, "[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and ([ContagemResultado_ContratadaCod] = @AV9Contratada_Codigo)";
         scmdbuf = scmdbuf + " and ([ContagemResultado_CntSrvCod] = @AV43ContratoServicos_Codigo2)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_Codigo]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00SM2(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] );
               case 3 :
                     return conditional_H00SM5(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 4 :
                     return conditional_H00SM6(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
               case 5 :
                     return conditional_H00SM7(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00SM3 ;
          prmH00SM3 = new Object[] {
          new Object[] {"@AV8Conte_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SM4 ;
          prmH00SM4 = new Object[] {
          new Object[] {"@AV43ContratoServicos_Codigo2",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SM8 ;
          prmH00SM8 = new Object[] {
          new Object[] {"@AV8Conte_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SM9 ;
          prmH00SM9 = new Object[] {
          new Object[] {"@AV11Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SM10 ;
          prmH00SM10 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SM11 ;
          prmH00SM11 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SM13 ;
          prmH00SM13 = new Object[] {
          new Object[] {"@AV65NewCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SM2 ;
          prmH00SM2 = new Object[] {
          new Object[] {"@AV8Conte_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SM5 ;
          prmH00SM5 = new Object[] {
          } ;
          Object[] prmH00SM6 ;
          prmH00SM6 = new Object[] {
          new Object[] {"@AV11Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SM7 ;
          prmH00SM7 = new Object[] {
          new Object[] {"@AV9Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV43ContratoServicos_Codigo2",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00SM2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SM2,0,0,true,false )
             ,new CursorDef("H00SM3", "SELECT [Contrato_Codigo], [Contrato_Numero], [Contrato_AreaTrabalhoCod], [Contrato_Ativo], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE ([Contrato_Ativo] = 1) AND ([Contrato_AreaTrabalhoCod] = @AV8Conte_1Areatrabalho_codigo) AND ([Contratada_Codigo] = @AV9Contratada_Codigo) ORDER BY [Contrato_Numero] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SM3,0,0,true,false )
             ,new CursorDef("H00SM4", "SELECT [ContratoSrvVnc_Codigo], [ContratoServicosVnc_Descricao], [ContratoSrvVnc_CntSrvCod], [ContratoServicosVnc_Ativo] FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE ([ContratoServicosVnc_Ativo] = 1) AND ([ContratoSrvVnc_CntSrvCod] = @AV43ContratoServicos_Codigo2) ORDER BY [ContratoServicosVnc_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SM4,0,0,true,false )
             ,new CursorDef("H00SM5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SM5,100,0,false,false )
             ,new CursorDef("H00SM6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SM6,100,0,false,false )
             ,new CursorDef("H00SM7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SM7,100,0,true,false )
             ,new CursorDef("H00SM8", "SELECT T1.[Contratante_Codigo], T2.[Contratante_EmailSdaUser], T2.[Contratante_EmailSdaPort], T2.[Contratante_EmailSdaHost], T1.[AreaTrabalho_Codigo] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE (T1.[AreaTrabalho_Codigo] = @AV8Conte_1Areatrabalho_codigo) AND (T2.[Contratante_EmailSdaHost] IS NULL or T2.[Contratante_EmailSdaPort] IS NULL or T2.[Contratante_EmailSdaUser] IS NULL) ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SM8,1,0,false,true )
             ,new CursorDef("H00SM9", "SELECT TOP 1 T1.[Contrato_Codigo], T1.[Contrato_AreaTrabalhoCod], T1.[Contratada_Codigo], T1.[Contrato_Ativo], T2.[Contratada_AreaTrabalhoCod], T2.[Contratada_Ativo], T1.[Contrato_PrepostoCod] FROM ([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) WHERE T1.[Contrato_Codigo] = @AV11Contrato_Codigo ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SM9,1,0,true,true )
             ,new CursorDef("H00SM10", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SM10,100,0,true,false )
             ,new CursorDef("H00SM11", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SM11,100,0,true,false )
             ,new CursorDef("H00SM13", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_Owner], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, COALESCE( T4.[Usuario_PessoaNom], '') AS Usuario_PessoaNom FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT T6.[Pessoa_Nome] AS Usuario_PessoaNom, T5.[Usuario_Codigo] FROM ([Usuario] T5 WITH (NOLOCK) INNER JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) ) T4 ON T4.[Usuario_Codigo] = T1.[ContagemResultado_Owner]) WHERE T1.[ContagemResultado_Codigo] = @AV65NewCodigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SM13,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 15) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.getBool(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(6) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((String[]) buf[13])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
