/*
               File: type_SdtTabela
        Description: Tabela
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:43.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Tabela" )]
   [XmlType(TypeName =  "Tabela" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtTabela : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtTabela( )
      {
         /* Constructor for serialization */
         gxTv_SdtTabela_Tabela_nome = "";
         gxTv_SdtTabela_Tabela_descricao = "";
         gxTv_SdtTabela_Tabela_sistemades = "";
         gxTv_SdtTabela_Tabela_modulodes = "";
         gxTv_SdtTabela_Tabela_painom = "";
         gxTv_SdtTabela_Mode = "";
         gxTv_SdtTabela_Tabela_nome_Z = "";
         gxTv_SdtTabela_Tabela_sistemades_Z = "";
         gxTv_SdtTabela_Tabela_modulodes_Z = "";
         gxTv_SdtTabela_Tabela_painom_Z = "";
      }

      public SdtTabela( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV172Tabela_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV172Tabela_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Tabela_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Tabela");
         metadata.Set("BT", "Tabela");
         metadata.Set("PK", "[ \"Tabela_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Tabela_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Modulo_Codigo\" ],\"FKMap\":[ \"Tabela_ModuloCod-Modulo_Codigo\" ] },{ \"FK\":[ \"Sistema_Codigo\" ],\"FKMap\":[ \"Tabela_SistemaCod-Sistema_Codigo\" ] },{ \"FK\":[ \"Tabela_Codigo\" ],\"FKMap\":[ \"Tabela_MelhoraCod-Tabela_Codigo\" ] },{ \"FK\":[ \"Tabela_Codigo\" ],\"FKMap\":[ \"Tabela_PaiCod-Tabela_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_sistemacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_sistemades_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_modulocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_modulodes_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_paicod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_painom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_melhoracod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_sistemades_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_modulocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_modulodes_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_paicod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_painom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tabela_melhoracod_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtTabela deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtTabela)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtTabela obj ;
         obj = this;
         obj.gxTpr_Tabela_codigo = deserialized.gxTpr_Tabela_codigo;
         obj.gxTpr_Tabela_nome = deserialized.gxTpr_Tabela_nome;
         obj.gxTpr_Tabela_descricao = deserialized.gxTpr_Tabela_descricao;
         obj.gxTpr_Tabela_sistemacod = deserialized.gxTpr_Tabela_sistemacod;
         obj.gxTpr_Tabela_sistemades = deserialized.gxTpr_Tabela_sistemades;
         obj.gxTpr_Tabela_modulocod = deserialized.gxTpr_Tabela_modulocod;
         obj.gxTpr_Tabela_modulodes = deserialized.gxTpr_Tabela_modulodes;
         obj.gxTpr_Tabela_paicod = deserialized.gxTpr_Tabela_paicod;
         obj.gxTpr_Tabela_painom = deserialized.gxTpr_Tabela_painom;
         obj.gxTpr_Tabela_melhoracod = deserialized.gxTpr_Tabela_melhoracod;
         obj.gxTpr_Tabela_ativo = deserialized.gxTpr_Tabela_ativo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Tabela_codigo_Z = deserialized.gxTpr_Tabela_codigo_Z;
         obj.gxTpr_Tabela_nome_Z = deserialized.gxTpr_Tabela_nome_Z;
         obj.gxTpr_Tabela_sistemacod_Z = deserialized.gxTpr_Tabela_sistemacod_Z;
         obj.gxTpr_Tabela_sistemades_Z = deserialized.gxTpr_Tabela_sistemades_Z;
         obj.gxTpr_Tabela_modulocod_Z = deserialized.gxTpr_Tabela_modulocod_Z;
         obj.gxTpr_Tabela_modulodes_Z = deserialized.gxTpr_Tabela_modulodes_Z;
         obj.gxTpr_Tabela_paicod_Z = deserialized.gxTpr_Tabela_paicod_Z;
         obj.gxTpr_Tabela_painom_Z = deserialized.gxTpr_Tabela_painom_Z;
         obj.gxTpr_Tabela_melhoracod_Z = deserialized.gxTpr_Tabela_melhoracod_Z;
         obj.gxTpr_Tabela_ativo_Z = deserialized.gxTpr_Tabela_ativo_Z;
         obj.gxTpr_Tabela_descricao_N = deserialized.gxTpr_Tabela_descricao_N;
         obj.gxTpr_Tabela_sistemades_N = deserialized.gxTpr_Tabela_sistemades_N;
         obj.gxTpr_Tabela_modulocod_N = deserialized.gxTpr_Tabela_modulocod_N;
         obj.gxTpr_Tabela_modulodes_N = deserialized.gxTpr_Tabela_modulodes_N;
         obj.gxTpr_Tabela_paicod_N = deserialized.gxTpr_Tabela_paicod_N;
         obj.gxTpr_Tabela_painom_N = deserialized.gxTpr_Tabela_painom_N;
         obj.gxTpr_Tabela_melhoracod_N = deserialized.gxTpr_Tabela_melhoracod_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_Codigo") )
               {
                  gxTv_SdtTabela_Tabela_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_Nome") )
               {
                  gxTv_SdtTabela_Tabela_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_Descricao") )
               {
                  gxTv_SdtTabela_Tabela_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_SistemaCod") )
               {
                  gxTv_SdtTabela_Tabela_sistemacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_SistemaDes") )
               {
                  gxTv_SdtTabela_Tabela_sistemades = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_ModuloCod") )
               {
                  gxTv_SdtTabela_Tabela_modulocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_ModuloDes") )
               {
                  gxTv_SdtTabela_Tabela_modulodes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_PaiCod") )
               {
                  gxTv_SdtTabela_Tabela_paicod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_PaiNom") )
               {
                  gxTv_SdtTabela_Tabela_painom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_MelhoraCod") )
               {
                  gxTv_SdtTabela_Tabela_melhoracod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_Ativo") )
               {
                  gxTv_SdtTabela_Tabela_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtTabela_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtTabela_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_Codigo_Z") )
               {
                  gxTv_SdtTabela_Tabela_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_Nome_Z") )
               {
                  gxTv_SdtTabela_Tabela_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_SistemaCod_Z") )
               {
                  gxTv_SdtTabela_Tabela_sistemacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_SistemaDes_Z") )
               {
                  gxTv_SdtTabela_Tabela_sistemades_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_ModuloCod_Z") )
               {
                  gxTv_SdtTabela_Tabela_modulocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_ModuloDes_Z") )
               {
                  gxTv_SdtTabela_Tabela_modulodes_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_PaiCod_Z") )
               {
                  gxTv_SdtTabela_Tabela_paicod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_PaiNom_Z") )
               {
                  gxTv_SdtTabela_Tabela_painom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_MelhoraCod_Z") )
               {
                  gxTv_SdtTabela_Tabela_melhoracod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_Ativo_Z") )
               {
                  gxTv_SdtTabela_Tabela_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_Descricao_N") )
               {
                  gxTv_SdtTabela_Tabela_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_SistemaDes_N") )
               {
                  gxTv_SdtTabela_Tabela_sistemades_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_ModuloCod_N") )
               {
                  gxTv_SdtTabela_Tabela_modulocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_ModuloDes_N") )
               {
                  gxTv_SdtTabela_Tabela_modulodes_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_PaiCod_N") )
               {
                  gxTv_SdtTabela_Tabela_paicod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_PaiNom_N") )
               {
                  gxTv_SdtTabela_Tabela_painom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tabela_MelhoraCod_N") )
               {
                  gxTv_SdtTabela_Tabela_melhoracod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Tabela";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Tabela_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tabela_Nome", StringUtil.RTrim( gxTv_SdtTabela_Tabela_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tabela_Descricao", StringUtil.RTrim( gxTv_SdtTabela_Tabela_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tabela_SistemaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_sistemacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tabela_SistemaDes", StringUtil.RTrim( gxTv_SdtTabela_Tabela_sistemades));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tabela_ModuloCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_modulocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tabela_ModuloDes", StringUtil.RTrim( gxTv_SdtTabela_Tabela_modulodes));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tabela_PaiCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_paicod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tabela_PaiNom", StringUtil.RTrim( gxTv_SdtTabela_Tabela_painom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tabela_MelhoraCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_melhoracod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tabela_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtTabela_Tabela_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtTabela_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_Nome_Z", StringUtil.RTrim( gxTv_SdtTabela_Tabela_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_SistemaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_sistemacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_SistemaDes_Z", StringUtil.RTrim( gxTv_SdtTabela_Tabela_sistemades_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_ModuloCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_modulocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_ModuloDes_Z", StringUtil.RTrim( gxTv_SdtTabela_Tabela_modulodes_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_PaiCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_paicod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_PaiNom_Z", StringUtil.RTrim( gxTv_SdtTabela_Tabela_painom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_MelhoraCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_melhoracod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtTabela_Tabela_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_SistemaDes_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_sistemades_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_ModuloCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_modulocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_ModuloDes_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_modulodes_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_PaiCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_paicod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_PaiNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_painom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Tabela_MelhoraCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtTabela_Tabela_melhoracod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Tabela_Codigo", gxTv_SdtTabela_Tabela_codigo, false);
         AddObjectProperty("Tabela_Nome", gxTv_SdtTabela_Tabela_nome, false);
         AddObjectProperty("Tabela_Descricao", gxTv_SdtTabela_Tabela_descricao, false);
         AddObjectProperty("Tabela_SistemaCod", gxTv_SdtTabela_Tabela_sistemacod, false);
         AddObjectProperty("Tabela_SistemaDes", gxTv_SdtTabela_Tabela_sistemades, false);
         AddObjectProperty("Tabela_ModuloCod", gxTv_SdtTabela_Tabela_modulocod, false);
         AddObjectProperty("Tabela_ModuloDes", gxTv_SdtTabela_Tabela_modulodes, false);
         AddObjectProperty("Tabela_PaiCod", gxTv_SdtTabela_Tabela_paicod, false);
         AddObjectProperty("Tabela_PaiNom", gxTv_SdtTabela_Tabela_painom, false);
         AddObjectProperty("Tabela_MelhoraCod", gxTv_SdtTabela_Tabela_melhoracod, false);
         AddObjectProperty("Tabela_Ativo", gxTv_SdtTabela_Tabela_ativo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtTabela_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtTabela_Initialized, false);
            AddObjectProperty("Tabela_Codigo_Z", gxTv_SdtTabela_Tabela_codigo_Z, false);
            AddObjectProperty("Tabela_Nome_Z", gxTv_SdtTabela_Tabela_nome_Z, false);
            AddObjectProperty("Tabela_SistemaCod_Z", gxTv_SdtTabela_Tabela_sistemacod_Z, false);
            AddObjectProperty("Tabela_SistemaDes_Z", gxTv_SdtTabela_Tabela_sistemades_Z, false);
            AddObjectProperty("Tabela_ModuloCod_Z", gxTv_SdtTabela_Tabela_modulocod_Z, false);
            AddObjectProperty("Tabela_ModuloDes_Z", gxTv_SdtTabela_Tabela_modulodes_Z, false);
            AddObjectProperty("Tabela_PaiCod_Z", gxTv_SdtTabela_Tabela_paicod_Z, false);
            AddObjectProperty("Tabela_PaiNom_Z", gxTv_SdtTabela_Tabela_painom_Z, false);
            AddObjectProperty("Tabela_MelhoraCod_Z", gxTv_SdtTabela_Tabela_melhoracod_Z, false);
            AddObjectProperty("Tabela_Ativo_Z", gxTv_SdtTabela_Tabela_ativo_Z, false);
            AddObjectProperty("Tabela_Descricao_N", gxTv_SdtTabela_Tabela_descricao_N, false);
            AddObjectProperty("Tabela_SistemaDes_N", gxTv_SdtTabela_Tabela_sistemades_N, false);
            AddObjectProperty("Tabela_ModuloCod_N", gxTv_SdtTabela_Tabela_modulocod_N, false);
            AddObjectProperty("Tabela_ModuloDes_N", gxTv_SdtTabela_Tabela_modulodes_N, false);
            AddObjectProperty("Tabela_PaiCod_N", gxTv_SdtTabela_Tabela_paicod_N, false);
            AddObjectProperty("Tabela_PaiNom_N", gxTv_SdtTabela_Tabela_painom_N, false);
            AddObjectProperty("Tabela_MelhoraCod_N", gxTv_SdtTabela_Tabela_melhoracod_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Tabela_Codigo" )]
      [  XmlElement( ElementName = "Tabela_Codigo"   )]
      public int gxTpr_Tabela_codigo
      {
         get {
            return gxTv_SdtTabela_Tabela_codigo ;
         }

         set {
            if ( gxTv_SdtTabela_Tabela_codigo != value )
            {
               gxTv_SdtTabela_Mode = "INS";
               this.gxTv_SdtTabela_Tabela_codigo_Z_SetNull( );
               this.gxTv_SdtTabela_Tabela_nome_Z_SetNull( );
               this.gxTv_SdtTabela_Tabela_sistemacod_Z_SetNull( );
               this.gxTv_SdtTabela_Tabela_sistemades_Z_SetNull( );
               this.gxTv_SdtTabela_Tabela_modulocod_Z_SetNull( );
               this.gxTv_SdtTabela_Tabela_modulodes_Z_SetNull( );
               this.gxTv_SdtTabela_Tabela_paicod_Z_SetNull( );
               this.gxTv_SdtTabela_Tabela_painom_Z_SetNull( );
               this.gxTv_SdtTabela_Tabela_melhoracod_Z_SetNull( );
               this.gxTv_SdtTabela_Tabela_ativo_Z_SetNull( );
            }
            gxTv_SdtTabela_Tabela_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Tabela_Nome" )]
      [  XmlElement( ElementName = "Tabela_Nome"   )]
      public String gxTpr_Tabela_nome
      {
         get {
            return gxTv_SdtTabela_Tabela_nome ;
         }

         set {
            gxTv_SdtTabela_Tabela_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Tabela_Descricao" )]
      [  XmlElement( ElementName = "Tabela_Descricao"   )]
      public String gxTpr_Tabela_descricao
      {
         get {
            return gxTv_SdtTabela_Tabela_descricao ;
         }

         set {
            gxTv_SdtTabela_Tabela_descricao_N = 0;
            gxTv_SdtTabela_Tabela_descricao = (String)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_descricao_SetNull( )
      {
         gxTv_SdtTabela_Tabela_descricao_N = 1;
         gxTv_SdtTabela_Tabela_descricao = "";
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_SistemaCod" )]
      [  XmlElement( ElementName = "Tabela_SistemaCod"   )]
      public int gxTpr_Tabela_sistemacod
      {
         get {
            return gxTv_SdtTabela_Tabela_sistemacod ;
         }

         set {
            gxTv_SdtTabela_Tabela_sistemacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Tabela_SistemaDes" )]
      [  XmlElement( ElementName = "Tabela_SistemaDes"   )]
      public String gxTpr_Tabela_sistemades
      {
         get {
            return gxTv_SdtTabela_Tabela_sistemades ;
         }

         set {
            gxTv_SdtTabela_Tabela_sistemades_N = 0;
            gxTv_SdtTabela_Tabela_sistemades = (String)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_sistemades_SetNull( )
      {
         gxTv_SdtTabela_Tabela_sistemades_N = 1;
         gxTv_SdtTabela_Tabela_sistemades = "";
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_sistemades_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_ModuloCod" )]
      [  XmlElement( ElementName = "Tabela_ModuloCod"   )]
      public int gxTpr_Tabela_modulocod
      {
         get {
            return gxTv_SdtTabela_Tabela_modulocod ;
         }

         set {
            gxTv_SdtTabela_Tabela_modulocod_N = 0;
            gxTv_SdtTabela_Tabela_modulocod = (int)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_modulocod_SetNull( )
      {
         gxTv_SdtTabela_Tabela_modulocod_N = 1;
         gxTv_SdtTabela_Tabela_modulocod = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_modulocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_ModuloDes" )]
      [  XmlElement( ElementName = "Tabela_ModuloDes"   )]
      public String gxTpr_Tabela_modulodes
      {
         get {
            return gxTv_SdtTabela_Tabela_modulodes ;
         }

         set {
            gxTv_SdtTabela_Tabela_modulodes_N = 0;
            gxTv_SdtTabela_Tabela_modulodes = (String)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_modulodes_SetNull( )
      {
         gxTv_SdtTabela_Tabela_modulodes_N = 1;
         gxTv_SdtTabela_Tabela_modulodes = "";
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_modulodes_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_PaiCod" )]
      [  XmlElement( ElementName = "Tabela_PaiCod"   )]
      public int gxTpr_Tabela_paicod
      {
         get {
            return gxTv_SdtTabela_Tabela_paicod ;
         }

         set {
            gxTv_SdtTabela_Tabela_paicod_N = 0;
            gxTv_SdtTabela_Tabela_paicod = (int)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_paicod_SetNull( )
      {
         gxTv_SdtTabela_Tabela_paicod_N = 1;
         gxTv_SdtTabela_Tabela_paicod = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_paicod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_PaiNom" )]
      [  XmlElement( ElementName = "Tabela_PaiNom"   )]
      public String gxTpr_Tabela_painom
      {
         get {
            return gxTv_SdtTabela_Tabela_painom ;
         }

         set {
            gxTv_SdtTabela_Tabela_painom_N = 0;
            gxTv_SdtTabela_Tabela_painom = (String)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_painom_SetNull( )
      {
         gxTv_SdtTabela_Tabela_painom_N = 1;
         gxTv_SdtTabela_Tabela_painom = "";
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_painom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_MelhoraCod" )]
      [  XmlElement( ElementName = "Tabela_MelhoraCod"   )]
      public int gxTpr_Tabela_melhoracod
      {
         get {
            return gxTv_SdtTabela_Tabela_melhoracod ;
         }

         set {
            gxTv_SdtTabela_Tabela_melhoracod_N = 0;
            gxTv_SdtTabela_Tabela_melhoracod = (int)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_melhoracod_SetNull( )
      {
         gxTv_SdtTabela_Tabela_melhoracod_N = 1;
         gxTv_SdtTabela_Tabela_melhoracod = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_melhoracod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_Ativo" )]
      [  XmlElement( ElementName = "Tabela_Ativo"   )]
      public bool gxTpr_Tabela_ativo
      {
         get {
            return gxTv_SdtTabela_Tabela_ativo ;
         }

         set {
            gxTv_SdtTabela_Tabela_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtTabela_Mode ;
         }

         set {
            gxTv_SdtTabela_Mode = (String)(value);
         }

      }

      public void gxTv_SdtTabela_Mode_SetNull( )
      {
         gxTv_SdtTabela_Mode = "";
         return  ;
      }

      public bool gxTv_SdtTabela_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtTabela_Initialized ;
         }

         set {
            gxTv_SdtTabela_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtTabela_Initialized_SetNull( )
      {
         gxTv_SdtTabela_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_Codigo_Z" )]
      [  XmlElement( ElementName = "Tabela_Codigo_Z"   )]
      public int gxTpr_Tabela_codigo_Z
      {
         get {
            return gxTv_SdtTabela_Tabela_codigo_Z ;
         }

         set {
            gxTv_SdtTabela_Tabela_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_codigo_Z_SetNull( )
      {
         gxTv_SdtTabela_Tabela_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_Nome_Z" )]
      [  XmlElement( ElementName = "Tabela_Nome_Z"   )]
      public String gxTpr_Tabela_nome_Z
      {
         get {
            return gxTv_SdtTabela_Tabela_nome_Z ;
         }

         set {
            gxTv_SdtTabela_Tabela_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_nome_Z_SetNull( )
      {
         gxTv_SdtTabela_Tabela_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_SistemaCod_Z" )]
      [  XmlElement( ElementName = "Tabela_SistemaCod_Z"   )]
      public int gxTpr_Tabela_sistemacod_Z
      {
         get {
            return gxTv_SdtTabela_Tabela_sistemacod_Z ;
         }

         set {
            gxTv_SdtTabela_Tabela_sistemacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_sistemacod_Z_SetNull( )
      {
         gxTv_SdtTabela_Tabela_sistemacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_sistemacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_SistemaDes_Z" )]
      [  XmlElement( ElementName = "Tabela_SistemaDes_Z"   )]
      public String gxTpr_Tabela_sistemades_Z
      {
         get {
            return gxTv_SdtTabela_Tabela_sistemades_Z ;
         }

         set {
            gxTv_SdtTabela_Tabela_sistemades_Z = (String)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_sistemades_Z_SetNull( )
      {
         gxTv_SdtTabela_Tabela_sistemades_Z = "";
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_sistemades_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_ModuloCod_Z" )]
      [  XmlElement( ElementName = "Tabela_ModuloCod_Z"   )]
      public int gxTpr_Tabela_modulocod_Z
      {
         get {
            return gxTv_SdtTabela_Tabela_modulocod_Z ;
         }

         set {
            gxTv_SdtTabela_Tabela_modulocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_modulocod_Z_SetNull( )
      {
         gxTv_SdtTabela_Tabela_modulocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_modulocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_ModuloDes_Z" )]
      [  XmlElement( ElementName = "Tabela_ModuloDes_Z"   )]
      public String gxTpr_Tabela_modulodes_Z
      {
         get {
            return gxTv_SdtTabela_Tabela_modulodes_Z ;
         }

         set {
            gxTv_SdtTabela_Tabela_modulodes_Z = (String)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_modulodes_Z_SetNull( )
      {
         gxTv_SdtTabela_Tabela_modulodes_Z = "";
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_modulodes_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_PaiCod_Z" )]
      [  XmlElement( ElementName = "Tabela_PaiCod_Z"   )]
      public int gxTpr_Tabela_paicod_Z
      {
         get {
            return gxTv_SdtTabela_Tabela_paicod_Z ;
         }

         set {
            gxTv_SdtTabela_Tabela_paicod_Z = (int)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_paicod_Z_SetNull( )
      {
         gxTv_SdtTabela_Tabela_paicod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_paicod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_PaiNom_Z" )]
      [  XmlElement( ElementName = "Tabela_PaiNom_Z"   )]
      public String gxTpr_Tabela_painom_Z
      {
         get {
            return gxTv_SdtTabela_Tabela_painom_Z ;
         }

         set {
            gxTv_SdtTabela_Tabela_painom_Z = (String)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_painom_Z_SetNull( )
      {
         gxTv_SdtTabela_Tabela_painom_Z = "";
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_painom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_MelhoraCod_Z" )]
      [  XmlElement( ElementName = "Tabela_MelhoraCod_Z"   )]
      public int gxTpr_Tabela_melhoracod_Z
      {
         get {
            return gxTv_SdtTabela_Tabela_melhoracod_Z ;
         }

         set {
            gxTv_SdtTabela_Tabela_melhoracod_Z = (int)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_melhoracod_Z_SetNull( )
      {
         gxTv_SdtTabela_Tabela_melhoracod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_melhoracod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_Ativo_Z" )]
      [  XmlElement( ElementName = "Tabela_Ativo_Z"   )]
      public bool gxTpr_Tabela_ativo_Z
      {
         get {
            return gxTv_SdtTabela_Tabela_ativo_Z ;
         }

         set {
            gxTv_SdtTabela_Tabela_ativo_Z = value;
         }

      }

      public void gxTv_SdtTabela_Tabela_ativo_Z_SetNull( )
      {
         gxTv_SdtTabela_Tabela_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_Descricao_N" )]
      [  XmlElement( ElementName = "Tabela_Descricao_N"   )]
      public short gxTpr_Tabela_descricao_N
      {
         get {
            return gxTv_SdtTabela_Tabela_descricao_N ;
         }

         set {
            gxTv_SdtTabela_Tabela_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_descricao_N_SetNull( )
      {
         gxTv_SdtTabela_Tabela_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_SistemaDes_N" )]
      [  XmlElement( ElementName = "Tabela_SistemaDes_N"   )]
      public short gxTpr_Tabela_sistemades_N
      {
         get {
            return gxTv_SdtTabela_Tabela_sistemades_N ;
         }

         set {
            gxTv_SdtTabela_Tabela_sistemades_N = (short)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_sistemades_N_SetNull( )
      {
         gxTv_SdtTabela_Tabela_sistemades_N = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_sistemades_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_ModuloCod_N" )]
      [  XmlElement( ElementName = "Tabela_ModuloCod_N"   )]
      public short gxTpr_Tabela_modulocod_N
      {
         get {
            return gxTv_SdtTabela_Tabela_modulocod_N ;
         }

         set {
            gxTv_SdtTabela_Tabela_modulocod_N = (short)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_modulocod_N_SetNull( )
      {
         gxTv_SdtTabela_Tabela_modulocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_modulocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_ModuloDes_N" )]
      [  XmlElement( ElementName = "Tabela_ModuloDes_N"   )]
      public short gxTpr_Tabela_modulodes_N
      {
         get {
            return gxTv_SdtTabela_Tabela_modulodes_N ;
         }

         set {
            gxTv_SdtTabela_Tabela_modulodes_N = (short)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_modulodes_N_SetNull( )
      {
         gxTv_SdtTabela_Tabela_modulodes_N = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_modulodes_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_PaiCod_N" )]
      [  XmlElement( ElementName = "Tabela_PaiCod_N"   )]
      public short gxTpr_Tabela_paicod_N
      {
         get {
            return gxTv_SdtTabela_Tabela_paicod_N ;
         }

         set {
            gxTv_SdtTabela_Tabela_paicod_N = (short)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_paicod_N_SetNull( )
      {
         gxTv_SdtTabela_Tabela_paicod_N = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_paicod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_PaiNom_N" )]
      [  XmlElement( ElementName = "Tabela_PaiNom_N"   )]
      public short gxTpr_Tabela_painom_N
      {
         get {
            return gxTv_SdtTabela_Tabela_painom_N ;
         }

         set {
            gxTv_SdtTabela_Tabela_painom_N = (short)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_painom_N_SetNull( )
      {
         gxTv_SdtTabela_Tabela_painom_N = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_painom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tabela_MelhoraCod_N" )]
      [  XmlElement( ElementName = "Tabela_MelhoraCod_N"   )]
      public short gxTpr_Tabela_melhoracod_N
      {
         get {
            return gxTv_SdtTabela_Tabela_melhoracod_N ;
         }

         set {
            gxTv_SdtTabela_Tabela_melhoracod_N = (short)(value);
         }

      }

      public void gxTv_SdtTabela_Tabela_melhoracod_N_SetNull( )
      {
         gxTv_SdtTabela_Tabela_melhoracod_N = 0;
         return  ;
      }

      public bool gxTv_SdtTabela_Tabela_melhoracod_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtTabela_Tabela_nome = "";
         gxTv_SdtTabela_Tabela_descricao = "";
         gxTv_SdtTabela_Tabela_sistemades = "";
         gxTv_SdtTabela_Tabela_modulodes = "";
         gxTv_SdtTabela_Tabela_painom = "";
         gxTv_SdtTabela_Mode = "";
         gxTv_SdtTabela_Tabela_nome_Z = "";
         gxTv_SdtTabela_Tabela_sistemades_Z = "";
         gxTv_SdtTabela_Tabela_modulodes_Z = "";
         gxTv_SdtTabela_Tabela_painom_Z = "";
         gxTv_SdtTabela_Tabela_ativo = true;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "tabela", "GeneXus.Programs.tabela_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtTabela_Initialized ;
      private short gxTv_SdtTabela_Tabela_descricao_N ;
      private short gxTv_SdtTabela_Tabela_sistemades_N ;
      private short gxTv_SdtTabela_Tabela_modulocod_N ;
      private short gxTv_SdtTabela_Tabela_modulodes_N ;
      private short gxTv_SdtTabela_Tabela_paicod_N ;
      private short gxTv_SdtTabela_Tabela_painom_N ;
      private short gxTv_SdtTabela_Tabela_melhoracod_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtTabela_Tabela_codigo ;
      private int gxTv_SdtTabela_Tabela_sistemacod ;
      private int gxTv_SdtTabela_Tabela_modulocod ;
      private int gxTv_SdtTabela_Tabela_paicod ;
      private int gxTv_SdtTabela_Tabela_melhoracod ;
      private int gxTv_SdtTabela_Tabela_codigo_Z ;
      private int gxTv_SdtTabela_Tabela_sistemacod_Z ;
      private int gxTv_SdtTabela_Tabela_modulocod_Z ;
      private int gxTv_SdtTabela_Tabela_paicod_Z ;
      private int gxTv_SdtTabela_Tabela_melhoracod_Z ;
      private String gxTv_SdtTabela_Tabela_nome ;
      private String gxTv_SdtTabela_Tabela_modulodes ;
      private String gxTv_SdtTabela_Tabela_painom ;
      private String gxTv_SdtTabela_Mode ;
      private String gxTv_SdtTabela_Tabela_nome_Z ;
      private String gxTv_SdtTabela_Tabela_modulodes_Z ;
      private String gxTv_SdtTabela_Tabela_painom_Z ;
      private String sTagName ;
      private bool gxTv_SdtTabela_Tabela_ativo ;
      private bool gxTv_SdtTabela_Tabela_ativo_Z ;
      private String gxTv_SdtTabela_Tabela_descricao ;
      private String gxTv_SdtTabela_Tabela_sistemades ;
      private String gxTv_SdtTabela_Tabela_sistemades_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Tabela", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtTabela_RESTInterface : GxGenericCollectionItem<SdtTabela>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtTabela_RESTInterface( ) : base()
      {
      }

      public SdtTabela_RESTInterface( SdtTabela psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Tabela_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tabela_codigo
      {
         get {
            return sdt.gxTpr_Tabela_codigo ;
         }

         set {
            sdt.gxTpr_Tabela_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Tabela_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Tabela_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tabela_nome) ;
         }

         set {
            sdt.gxTpr_Tabela_nome = (String)(value);
         }

      }

      [DataMember( Name = "Tabela_Descricao" , Order = 2 )]
      public String gxTpr_Tabela_descricao
      {
         get {
            return sdt.gxTpr_Tabela_descricao ;
         }

         set {
            sdt.gxTpr_Tabela_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Tabela_SistemaCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tabela_sistemacod
      {
         get {
            return sdt.gxTpr_Tabela_sistemacod ;
         }

         set {
            sdt.gxTpr_Tabela_sistemacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Tabela_SistemaDes" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Tabela_sistemades
      {
         get {
            return sdt.gxTpr_Tabela_sistemades ;
         }

         set {
            sdt.gxTpr_Tabela_sistemades = (String)(value);
         }

      }

      [DataMember( Name = "Tabela_ModuloCod" , Order = 5 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tabela_modulocod
      {
         get {
            return sdt.gxTpr_Tabela_modulocod ;
         }

         set {
            sdt.gxTpr_Tabela_modulocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Tabela_ModuloDes" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Tabela_modulodes
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tabela_modulodes) ;
         }

         set {
            sdt.gxTpr_Tabela_modulodes = (String)(value);
         }

      }

      [DataMember( Name = "Tabela_PaiCod" , Order = 7 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tabela_paicod
      {
         get {
            return sdt.gxTpr_Tabela_paicod ;
         }

         set {
            sdt.gxTpr_Tabela_paicod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Tabela_PaiNom" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Tabela_painom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tabela_painom) ;
         }

         set {
            sdt.gxTpr_Tabela_painom = (String)(value);
         }

      }

      [DataMember( Name = "Tabela_MelhoraCod" , Order = 9 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tabela_melhoracod
      {
         get {
            return sdt.gxTpr_Tabela_melhoracod ;
         }

         set {
            sdt.gxTpr_Tabela_melhoracod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Tabela_Ativo" , Order = 10 )]
      [GxSeudo()]
      public bool gxTpr_Tabela_ativo
      {
         get {
            return sdt.gxTpr_Tabela_ativo ;
         }

         set {
            sdt.gxTpr_Tabela_ativo = value;
         }

      }

      public SdtTabela sdt
      {
         get {
            return (SdtTabela)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtTabela() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 30 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
