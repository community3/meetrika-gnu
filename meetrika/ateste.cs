/*
               File: Teste
        Description: Teste
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:50.71
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class ateste : GXProcedure
   {
      public ateste( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public ateste( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         ateste objateste;
         objateste = new ateste();
         objateste.context.SetSubmitInitialConfig(context);
         objateste.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objateste);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((ateste)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Usuarios.Add(1097, 0);
         new ws_enviaemail(context ).execute(  6,  AV8Usuarios,  "Teste WSDL",  "Texto do email",  new GxSimpleCollection(), ref  AV9Retorno) ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8Usuarios = new GxSimpleCollection();
         AV10Atachments = new GxSimpleCollection();
         AV9Retorno = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV9Retorno ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV8Usuarios ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV10Atachments ;
   }

}
