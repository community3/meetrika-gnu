/*
               File: PRC_UsuarioNotificaNoStatus
        Description: Usuario Notifica No Status
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:44.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_usuarionotificanostatus : GXProcedure
   {
      public prc_usuarionotificanostatus( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_usuarionotificanostatus( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_UsuarioNotifica_UsuarioCod ,
                           ref int aP1_UsuarioNotifica_AreaTrabalhoCod ,
                           String aP2_status ,
                           out bool aP3_Notificar )
      {
         this.A2076UsuarioNotifica_UsuarioCod = aP0_UsuarioNotifica_UsuarioCod;
         this.A2075UsuarioNotifica_AreaTrabalhoCod = aP1_UsuarioNotifica_AreaTrabalhoCod;
         this.AV9status = aP2_status;
         this.AV8Notificar = false ;
         initialize();
         executePrivate();
         aP0_UsuarioNotifica_UsuarioCod=this.A2076UsuarioNotifica_UsuarioCod;
         aP1_UsuarioNotifica_AreaTrabalhoCod=this.A2075UsuarioNotifica_AreaTrabalhoCod;
         aP3_Notificar=this.AV8Notificar;
      }

      public bool executeUdp( ref int aP0_UsuarioNotifica_UsuarioCod ,
                              ref int aP1_UsuarioNotifica_AreaTrabalhoCod ,
                              String aP2_status )
      {
         this.A2076UsuarioNotifica_UsuarioCod = aP0_UsuarioNotifica_UsuarioCod;
         this.A2075UsuarioNotifica_AreaTrabalhoCod = aP1_UsuarioNotifica_AreaTrabalhoCod;
         this.AV9status = aP2_status;
         this.AV8Notificar = false ;
         initialize();
         executePrivate();
         aP0_UsuarioNotifica_UsuarioCod=this.A2076UsuarioNotifica_UsuarioCod;
         aP1_UsuarioNotifica_AreaTrabalhoCod=this.A2075UsuarioNotifica_AreaTrabalhoCod;
         aP3_Notificar=this.AV8Notificar;
         return AV8Notificar ;
      }

      public void executeSubmit( ref int aP0_UsuarioNotifica_UsuarioCod ,
                                 ref int aP1_UsuarioNotifica_AreaTrabalhoCod ,
                                 String aP2_status ,
                                 out bool aP3_Notificar )
      {
         prc_usuarionotificanostatus objprc_usuarionotificanostatus;
         objprc_usuarionotificanostatus = new prc_usuarionotificanostatus();
         objprc_usuarionotificanostatus.A2076UsuarioNotifica_UsuarioCod = aP0_UsuarioNotifica_UsuarioCod;
         objprc_usuarionotificanostatus.A2075UsuarioNotifica_AreaTrabalhoCod = aP1_UsuarioNotifica_AreaTrabalhoCod;
         objprc_usuarionotificanostatus.AV9status = aP2_status;
         objprc_usuarionotificanostatus.AV8Notificar = false ;
         objprc_usuarionotificanostatus.context.SetSubmitInitialConfig(context);
         objprc_usuarionotificanostatus.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_usuarionotificanostatus);
         aP0_UsuarioNotifica_UsuarioCod=this.A2076UsuarioNotifica_UsuarioCod;
         aP1_UsuarioNotifica_AreaTrabalhoCod=this.A2075UsuarioNotifica_AreaTrabalhoCod;
         aP3_Notificar=this.AV8Notificar;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_usuarionotificanostatus)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         lV9status = StringUtil.PadR( StringUtil.RTrim( AV9status), 1, "%");
         /* Using cursor P00XA2 */
         pr_default.execute(0, new Object[] {A2075UsuarioNotifica_AreaTrabalhoCod, A2076UsuarioNotifica_UsuarioCod, lV9status});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2079UsuarioNotifica_NoStatus = P00XA2_A2079UsuarioNotifica_NoStatus[0];
            A1908Usuario_DeFerias = P00XA2_A1908Usuario_DeFerias[0];
            n1908Usuario_DeFerias = P00XA2_n1908Usuario_DeFerias[0];
            A54Usuario_Ativo = P00XA2_A54Usuario_Ativo[0];
            n54Usuario_Ativo = P00XA2_n54Usuario_Ativo[0];
            A2077UsuarioNotifica_Codigo = P00XA2_A2077UsuarioNotifica_Codigo[0];
            A1908Usuario_DeFerias = P00XA2_A1908Usuario_DeFerias[0];
            n1908Usuario_DeFerias = P00XA2_n1908Usuario_DeFerias[0];
            A54Usuario_Ativo = P00XA2_A54Usuario_Ativo[0];
            n54Usuario_Ativo = P00XA2_n54Usuario_Ativo[0];
            AV8Notificar = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         lV9status = "";
         scmdbuf = "";
         P00XA2_A2076UsuarioNotifica_UsuarioCod = new int[1] ;
         P00XA2_A2075UsuarioNotifica_AreaTrabalhoCod = new int[1] ;
         P00XA2_A2079UsuarioNotifica_NoStatus = new String[] {""} ;
         P00XA2_A1908Usuario_DeFerias = new bool[] {false} ;
         P00XA2_n1908Usuario_DeFerias = new bool[] {false} ;
         P00XA2_A54Usuario_Ativo = new bool[] {false} ;
         P00XA2_n54Usuario_Ativo = new bool[] {false} ;
         P00XA2_A2077UsuarioNotifica_Codigo = new int[1] ;
         A2079UsuarioNotifica_NoStatus = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_usuarionotificanostatus__default(),
            new Object[][] {
                new Object[] {
               P00XA2_A2076UsuarioNotifica_UsuarioCod, P00XA2_A2075UsuarioNotifica_AreaTrabalhoCod, P00XA2_A2079UsuarioNotifica_NoStatus, P00XA2_A1908Usuario_DeFerias, P00XA2_n1908Usuario_DeFerias, P00XA2_A54Usuario_Ativo, P00XA2_n54Usuario_Ativo, P00XA2_A2077UsuarioNotifica_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A2076UsuarioNotifica_UsuarioCod ;
      private int A2075UsuarioNotifica_AreaTrabalhoCod ;
      private int A2077UsuarioNotifica_Codigo ;
      private String AV9status ;
      private String lV9status ;
      private String scmdbuf ;
      private bool AV8Notificar ;
      private bool A1908Usuario_DeFerias ;
      private bool n1908Usuario_DeFerias ;
      private bool A54Usuario_Ativo ;
      private bool n54Usuario_Ativo ;
      private String A2079UsuarioNotifica_NoStatus ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_UsuarioNotifica_UsuarioCod ;
      private int aP1_UsuarioNotifica_AreaTrabalhoCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00XA2_A2076UsuarioNotifica_UsuarioCod ;
      private int[] P00XA2_A2075UsuarioNotifica_AreaTrabalhoCod ;
      private String[] P00XA2_A2079UsuarioNotifica_NoStatus ;
      private bool[] P00XA2_A1908Usuario_DeFerias ;
      private bool[] P00XA2_n1908Usuario_DeFerias ;
      private bool[] P00XA2_A54Usuario_Ativo ;
      private bool[] P00XA2_n54Usuario_Ativo ;
      private int[] P00XA2_A2077UsuarioNotifica_Codigo ;
      private bool aP3_Notificar ;
   }

   public class prc_usuarionotificanostatus__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XA2 ;
          prmP00XA2 = new Object[] {
          new Object[] {"@UsuarioNotifica_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioNotifica_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV9status",SqlDbType.Char,1,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XA2", "SELECT TOP 1 T1.[UsuarioNotifica_UsuarioCod] AS UsuarioNotifica_UsuarioCod, T1.[UsuarioNotifica_AreaTrabalhoCod], T1.[UsuarioNotifica_NoStatus], T2.[Usuario_DeFerias], T2.[Usuario_Ativo], T1.[UsuarioNotifica_Codigo] FROM ([UsuarioNotifica] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[UsuarioNotifica_UsuarioCod]) WHERE (T1.[UsuarioNotifica_AreaTrabalhoCod] = @UsuarioNotifica_AreaTrabalhoCod) AND (T1.[UsuarioNotifica_UsuarioCod] = @UsuarioNotifica_UsuarioCod) AND (T2.[Usuario_Ativo] = 1) AND (Not T2.[Usuario_DeFerias] = 1) AND (T1.[UsuarioNotifica_NoStatus] = '*' or T1.[UsuarioNotifica_NoStatus] like '%' + @lV9status + '%') ORDER BY T1.[UsuarioNotifica_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XA2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
       }
    }

 }

}
