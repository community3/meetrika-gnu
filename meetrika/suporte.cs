/*
               File: Suporte
        Description: Suporte
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:27:11.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class suporte : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbSuporte_Prioridade.Name = "SUPORTE_PRIORIDADE";
         cmbSuporte_Prioridade.WebTags = "";
         cmbSuporte_Prioridade.addItem("", "(Nenhuma)", 0);
         cmbSuporte_Prioridade.addItem("A", "Alta", 0);
         cmbSuporte_Prioridade.addItem("M", "M�dia", 0);
         cmbSuporte_Prioridade.addItem("B", "Baixa", 0);
         if ( cmbSuporte_Prioridade.ItemCount > 0 )
         {
            A1139Suporte_Prioridade = cmbSuporte_Prioridade.getValidValue(A1139Suporte_Prioridade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1139Suporte_Prioridade", A1139Suporte_Prioridade);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Suporte", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtSuporte_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public suporte( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public suporte( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbSuporte_Prioridade = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbSuporte_Prioridade.ItemCount > 0 )
         {
            A1139Suporte_Prioridade = cmbSuporte_Prioridade.getValidValue(A1139Suporte_Prioridade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1139Suporte_Prioridade", A1139Suporte_Prioridade);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_37135( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_37135e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_37135( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_37135( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_37135e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Suporte", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_Suporte.htm");
            wb_table3_28_37135( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_37135e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_37135e( true) ;
         }
         else
         {
            wb_table1_2_37135e( false) ;
         }
      }

      protected void wb_table3_28_37135( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_37135( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_37135e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Suporte.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Suporte.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_37135e( true) ;
         }
         else
         {
            wb_table3_28_37135e( false) ;
         }
      }

      protected void wb_table4_34_37135( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksuporte_codigo_Internalname, "Codigo", "", "", lblTextblocksuporte_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSuporte_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1138Suporte_Codigo), 6, 0, ",", "")), ((edtSuporte_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1138Suporte_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1138Suporte_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSuporte_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtSuporte_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksuporte_prioridade_Internalname, "Prioridade", "", "", lblTextblocksuporte_prioridade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbSuporte_Prioridade, cmbSuporte_Prioridade_Internalname, StringUtil.RTrim( A1139Suporte_Prioridade), 1, cmbSuporte_Prioridade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbSuporte_Prioridade.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_Suporte.htm");
            cmbSuporte_Prioridade.CurrentValue = StringUtil.RTrim( A1139Suporte_Prioridade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSuporte_Prioridade_Internalname, "Values", (String)(cmbSuporte_Prioridade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksuporte_assunto_Internalname, "Assunto", "", "", lblTextblocksuporte_assunto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSuporte_Assunto_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1140Suporte_Assunto), 6, 0, ",", "")), ((edtSuporte_Assunto_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1140Suporte_Assunto), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1140Suporte_Assunto), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSuporte_Assunto_Jsonclick, 0, "Attribute", "", "", "", 1, edtSuporte_Assunto_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksuporte_descricao_Internalname, "Descri��o", "", "", lblTextblocksuporte_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtSuporte_Descricao_Internalname, A1141Suporte_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, 1, edtSuporte_Descricao_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksuporte_status_Internalname, "Status", "", "", lblTextblocksuporte_status_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSuporte_Status_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1142Suporte_Status), 4, 0, ",", "")), ((edtSuporte_Status_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1142Suporte_Status), "ZZZ9")) : context.localUtil.Format( (decimal)(A1142Suporte_Status), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSuporte_Status_Jsonclick, 0, "Attribute", "", "", "", 1, edtSuporte_Status_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksuporte_responsavel_Internalname, "Respons�vel", "", "", lblTextblocksuporte_responsavel_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSuporte_Responsavel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1143Suporte_Responsavel), 4, 0, ",", "")), ((edtSuporte_Responsavel_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1143Suporte_Responsavel), "ZZZ9")) : context.localUtil.Format( (decimal)(A1143Suporte_Responsavel), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSuporte_Responsavel_Jsonclick, 0, "Attribute", "", "", "", 1, edtSuporte_Responsavel_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksuporte_owner_Internalname, "Usu�rio", "", "", lblTextblocksuporte_owner_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSuporte_Owner_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1144Suporte_Owner), 4, 0, ",", "")), ((edtSuporte_Owner_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1144Suporte_Owner), "ZZZ9")) : context.localUtil.Format( (decimal)(A1144Suporte_Owner), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSuporte_Owner_Jsonclick, 0, "Attribute", "", "", "", 1, edtSuporte_Owner_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Suporte.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_37135e( true) ;
         }
         else
         {
            wb_table4_34_37135e( false) ;
         }
      }

      protected void wb_table2_5_37135( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Suporte.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_37135e( true) ;
         }
         else
         {
            wb_table2_5_37135e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtSuporte_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSuporte_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SUPORTE_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSuporte_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1138Suporte_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1138Suporte_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1138Suporte_Codigo), 6, 0)));
               }
               else
               {
                  A1138Suporte_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSuporte_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1138Suporte_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1138Suporte_Codigo), 6, 0)));
               }
               cmbSuporte_Prioridade.CurrentValue = cgiGet( cmbSuporte_Prioridade_Internalname);
               A1139Suporte_Prioridade = cgiGet( cmbSuporte_Prioridade_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1139Suporte_Prioridade", A1139Suporte_Prioridade);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSuporte_Assunto_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSuporte_Assunto_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SUPORTE_ASSUNTO");
                  AnyError = 1;
                  GX_FocusControl = edtSuporte_Assunto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1140Suporte_Assunto = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1140Suporte_Assunto", StringUtil.LTrim( StringUtil.Str( (decimal)(A1140Suporte_Assunto), 6, 0)));
               }
               else
               {
                  A1140Suporte_Assunto = (int)(context.localUtil.CToN( cgiGet( edtSuporte_Assunto_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1140Suporte_Assunto", StringUtil.LTrim( StringUtil.Str( (decimal)(A1140Suporte_Assunto), 6, 0)));
               }
               A1141Suporte_Descricao = cgiGet( edtSuporte_Descricao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1141Suporte_Descricao", A1141Suporte_Descricao);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSuporte_Status_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSuporte_Status_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SUPORTE_STATUS");
                  AnyError = 1;
                  GX_FocusControl = edtSuporte_Status_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1142Suporte_Status = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1142Suporte_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1142Suporte_Status), 4, 0)));
               }
               else
               {
                  A1142Suporte_Status = (short)(context.localUtil.CToN( cgiGet( edtSuporte_Status_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1142Suporte_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1142Suporte_Status), 4, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtSuporte_Responsavel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSuporte_Responsavel_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SUPORTE_RESPONSAVEL");
                  AnyError = 1;
                  GX_FocusControl = edtSuporte_Responsavel_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1143Suporte_Responsavel = 0;
                  n1143Suporte_Responsavel = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1143Suporte_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1143Suporte_Responsavel), 4, 0)));
               }
               else
               {
                  A1143Suporte_Responsavel = (short)(context.localUtil.CToN( cgiGet( edtSuporte_Responsavel_Internalname), ",", "."));
                  n1143Suporte_Responsavel = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1143Suporte_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1143Suporte_Responsavel), 4, 0)));
               }
               n1143Suporte_Responsavel = ((0==A1143Suporte_Responsavel) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSuporte_Owner_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSuporte_Owner_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SUPORTE_OWNER");
                  AnyError = 1;
                  GX_FocusControl = edtSuporte_Owner_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1144Suporte_Owner = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1144Suporte_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1144Suporte_Owner), 4, 0)));
               }
               else
               {
                  A1144Suporte_Owner = (short)(context.localUtil.CToN( cgiGet( edtSuporte_Owner_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1144Suporte_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1144Suporte_Owner), 4, 0)));
               }
               /* Read saved values. */
               Z1138Suporte_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1138Suporte_Codigo"), ",", "."));
               Z1139Suporte_Prioridade = cgiGet( "Z1139Suporte_Prioridade");
               Z1140Suporte_Assunto = (int)(context.localUtil.CToN( cgiGet( "Z1140Suporte_Assunto"), ",", "."));
               Z1142Suporte_Status = (short)(context.localUtil.CToN( cgiGet( "Z1142Suporte_Status"), ",", "."));
               Z1143Suporte_Responsavel = (short)(context.localUtil.CToN( cgiGet( "Z1143Suporte_Responsavel"), ",", "."));
               n1143Suporte_Responsavel = ((0==A1143Suporte_Responsavel) ? true : false);
               Z1144Suporte_Owner = (short)(context.localUtil.CToN( cgiGet( "Z1144Suporte_Owner"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1138Suporte_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1138Suporte_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1138Suporte_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll37135( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes37135( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption370( )
      {
      }

      protected void ZM37135( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1139Suporte_Prioridade = T00373_A1139Suporte_Prioridade[0];
               Z1140Suporte_Assunto = T00373_A1140Suporte_Assunto[0];
               Z1142Suporte_Status = T00373_A1142Suporte_Status[0];
               Z1143Suporte_Responsavel = T00373_A1143Suporte_Responsavel[0];
               Z1144Suporte_Owner = T00373_A1144Suporte_Owner[0];
            }
            else
            {
               Z1139Suporte_Prioridade = A1139Suporte_Prioridade;
               Z1140Suporte_Assunto = A1140Suporte_Assunto;
               Z1142Suporte_Status = A1142Suporte_Status;
               Z1143Suporte_Responsavel = A1143Suporte_Responsavel;
               Z1144Suporte_Owner = A1144Suporte_Owner;
            }
         }
         if ( GX_JID == -1 )
         {
            Z1138Suporte_Codigo = A1138Suporte_Codigo;
            Z1139Suporte_Prioridade = A1139Suporte_Prioridade;
            Z1140Suporte_Assunto = A1140Suporte_Assunto;
            Z1141Suporte_Descricao = A1141Suporte_Descricao;
            Z1142Suporte_Status = A1142Suporte_Status;
            Z1143Suporte_Responsavel = A1143Suporte_Responsavel;
            Z1144Suporte_Owner = A1144Suporte_Owner;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load37135( )
      {
         /* Using cursor T00374 */
         pr_default.execute(2, new Object[] {A1138Suporte_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound135 = 1;
            A1139Suporte_Prioridade = T00374_A1139Suporte_Prioridade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1139Suporte_Prioridade", A1139Suporte_Prioridade);
            A1140Suporte_Assunto = T00374_A1140Suporte_Assunto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1140Suporte_Assunto", StringUtil.LTrim( StringUtil.Str( (decimal)(A1140Suporte_Assunto), 6, 0)));
            A1141Suporte_Descricao = T00374_A1141Suporte_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1141Suporte_Descricao", A1141Suporte_Descricao);
            A1142Suporte_Status = T00374_A1142Suporte_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1142Suporte_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1142Suporte_Status), 4, 0)));
            A1143Suporte_Responsavel = T00374_A1143Suporte_Responsavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1143Suporte_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1143Suporte_Responsavel), 4, 0)));
            n1143Suporte_Responsavel = T00374_n1143Suporte_Responsavel[0];
            A1144Suporte_Owner = T00374_A1144Suporte_Owner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1144Suporte_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1144Suporte_Owner), 4, 0)));
            ZM37135( -1) ;
         }
         pr_default.close(2);
         OnLoadActions37135( ) ;
      }

      protected void OnLoadActions37135( )
      {
      }

      protected void CheckExtendedTable37135( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors37135( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey37135( )
      {
         /* Using cursor T00375 */
         pr_default.execute(3, new Object[] {A1138Suporte_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound135 = 1;
         }
         else
         {
            RcdFound135 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00373 */
         pr_default.execute(1, new Object[] {A1138Suporte_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM37135( 1) ;
            RcdFound135 = 1;
            A1138Suporte_Codigo = T00373_A1138Suporte_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1138Suporte_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1138Suporte_Codigo), 6, 0)));
            A1139Suporte_Prioridade = T00373_A1139Suporte_Prioridade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1139Suporte_Prioridade", A1139Suporte_Prioridade);
            A1140Suporte_Assunto = T00373_A1140Suporte_Assunto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1140Suporte_Assunto", StringUtil.LTrim( StringUtil.Str( (decimal)(A1140Suporte_Assunto), 6, 0)));
            A1141Suporte_Descricao = T00373_A1141Suporte_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1141Suporte_Descricao", A1141Suporte_Descricao);
            A1142Suporte_Status = T00373_A1142Suporte_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1142Suporte_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1142Suporte_Status), 4, 0)));
            A1143Suporte_Responsavel = T00373_A1143Suporte_Responsavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1143Suporte_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1143Suporte_Responsavel), 4, 0)));
            n1143Suporte_Responsavel = T00373_n1143Suporte_Responsavel[0];
            A1144Suporte_Owner = T00373_A1144Suporte_Owner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1144Suporte_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1144Suporte_Owner), 4, 0)));
            Z1138Suporte_Codigo = A1138Suporte_Codigo;
            sMode135 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load37135( ) ;
            if ( AnyError == 1 )
            {
               RcdFound135 = 0;
               InitializeNonKey37135( ) ;
            }
            Gx_mode = sMode135;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound135 = 0;
            InitializeNonKey37135( ) ;
            sMode135 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode135;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey37135( ) ;
         if ( RcdFound135 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound135 = 0;
         /* Using cursor T00376 */
         pr_default.execute(4, new Object[] {A1138Suporte_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T00376_A1138Suporte_Codigo[0] < A1138Suporte_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T00376_A1138Suporte_Codigo[0] > A1138Suporte_Codigo ) ) )
            {
               A1138Suporte_Codigo = T00376_A1138Suporte_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1138Suporte_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1138Suporte_Codigo), 6, 0)));
               RcdFound135 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound135 = 0;
         /* Using cursor T00377 */
         pr_default.execute(5, new Object[] {A1138Suporte_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T00377_A1138Suporte_Codigo[0] > A1138Suporte_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T00377_A1138Suporte_Codigo[0] < A1138Suporte_Codigo ) ) )
            {
               A1138Suporte_Codigo = T00377_A1138Suporte_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1138Suporte_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1138Suporte_Codigo), 6, 0)));
               RcdFound135 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey37135( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtSuporte_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert37135( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound135 == 1 )
            {
               if ( A1138Suporte_Codigo != Z1138Suporte_Codigo )
               {
                  A1138Suporte_Codigo = Z1138Suporte_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1138Suporte_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1138Suporte_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SUPORTE_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSuporte_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtSuporte_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update37135( ) ;
                  GX_FocusControl = edtSuporte_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1138Suporte_Codigo != Z1138Suporte_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtSuporte_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert37135( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SUPORTE_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtSuporte_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtSuporte_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert37135( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1138Suporte_Codigo != Z1138Suporte_Codigo )
         {
            A1138Suporte_Codigo = Z1138Suporte_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1138Suporte_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1138Suporte_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SUPORTE_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSuporte_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtSuporte_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound135 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "SUPORTE_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSuporte_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = cmbSuporte_Prioridade_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart37135( ) ;
         if ( RcdFound135 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbSuporte_Prioridade_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd37135( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound135 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbSuporte_Prioridade_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound135 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbSuporte_Prioridade_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart37135( ) ;
         if ( RcdFound135 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound135 != 0 )
            {
               ScanNext37135( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbSuporte_Prioridade_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd37135( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency37135( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00372 */
            pr_default.execute(0, new Object[] {A1138Suporte_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Suporte"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1139Suporte_Prioridade, T00372_A1139Suporte_Prioridade[0]) != 0 ) || ( Z1140Suporte_Assunto != T00372_A1140Suporte_Assunto[0] ) || ( Z1142Suporte_Status != T00372_A1142Suporte_Status[0] ) || ( Z1143Suporte_Responsavel != T00372_A1143Suporte_Responsavel[0] ) || ( Z1144Suporte_Owner != T00372_A1144Suporte_Owner[0] ) )
            {
               if ( StringUtil.StrCmp(Z1139Suporte_Prioridade, T00372_A1139Suporte_Prioridade[0]) != 0 )
               {
                  GXUtil.WriteLog("suporte:[seudo value changed for attri]"+"Suporte_Prioridade");
                  GXUtil.WriteLogRaw("Old: ",Z1139Suporte_Prioridade);
                  GXUtil.WriteLogRaw("Current: ",T00372_A1139Suporte_Prioridade[0]);
               }
               if ( Z1140Suporte_Assunto != T00372_A1140Suporte_Assunto[0] )
               {
                  GXUtil.WriteLog("suporte:[seudo value changed for attri]"+"Suporte_Assunto");
                  GXUtil.WriteLogRaw("Old: ",Z1140Suporte_Assunto);
                  GXUtil.WriteLogRaw("Current: ",T00372_A1140Suporte_Assunto[0]);
               }
               if ( Z1142Suporte_Status != T00372_A1142Suporte_Status[0] )
               {
                  GXUtil.WriteLog("suporte:[seudo value changed for attri]"+"Suporte_Status");
                  GXUtil.WriteLogRaw("Old: ",Z1142Suporte_Status);
                  GXUtil.WriteLogRaw("Current: ",T00372_A1142Suporte_Status[0]);
               }
               if ( Z1143Suporte_Responsavel != T00372_A1143Suporte_Responsavel[0] )
               {
                  GXUtil.WriteLog("suporte:[seudo value changed for attri]"+"Suporte_Responsavel");
                  GXUtil.WriteLogRaw("Old: ",Z1143Suporte_Responsavel);
                  GXUtil.WriteLogRaw("Current: ",T00372_A1143Suporte_Responsavel[0]);
               }
               if ( Z1144Suporte_Owner != T00372_A1144Suporte_Owner[0] )
               {
                  GXUtil.WriteLog("suporte:[seudo value changed for attri]"+"Suporte_Owner");
                  GXUtil.WriteLogRaw("Old: ",Z1144Suporte_Owner);
                  GXUtil.WriteLogRaw("Current: ",T00372_A1144Suporte_Owner[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Suporte"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert37135( )
      {
         BeforeValidate37135( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable37135( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM37135( 0) ;
            CheckOptimisticConcurrency37135( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm37135( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert37135( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00378 */
                     pr_default.execute(6, new Object[] {A1139Suporte_Prioridade, A1140Suporte_Assunto, A1141Suporte_Descricao, A1142Suporte_Status, n1143Suporte_Responsavel, A1143Suporte_Responsavel, A1144Suporte_Owner});
                     A1138Suporte_Codigo = T00378_A1138Suporte_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1138Suporte_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1138Suporte_Codigo), 6, 0)));
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Suporte") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption370( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load37135( ) ;
            }
            EndLevel37135( ) ;
         }
         CloseExtendedTableCursors37135( ) ;
      }

      protected void Update37135( )
      {
         BeforeValidate37135( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable37135( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency37135( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm37135( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate37135( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00379 */
                     pr_default.execute(7, new Object[] {A1139Suporte_Prioridade, A1140Suporte_Assunto, A1141Suporte_Descricao, A1142Suporte_Status, n1143Suporte_Responsavel, A1143Suporte_Responsavel, A1144Suporte_Owner, A1138Suporte_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Suporte") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Suporte"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate37135( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption370( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel37135( ) ;
         }
         CloseExtendedTableCursors37135( ) ;
      }

      protected void DeferredUpdate37135( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate37135( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency37135( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls37135( ) ;
            AfterConfirm37135( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete37135( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003710 */
                  pr_default.execute(8, new Object[] {A1138Suporte_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Suporte") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound135 == 0 )
                        {
                           InitAll37135( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption370( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode135 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel37135( ) ;
         Gx_mode = sMode135;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls37135( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel37135( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete37135( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Suporte");
            if ( AnyError == 0 )
            {
               ConfirmValues370( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Suporte");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart37135( )
      {
         /* Using cursor T003711 */
         pr_default.execute(9);
         RcdFound135 = 0;
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound135 = 1;
            A1138Suporte_Codigo = T003711_A1138Suporte_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1138Suporte_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1138Suporte_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext37135( )
      {
         /* Scan next routine */
         pr_default.readNext(9);
         RcdFound135 = 0;
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound135 = 1;
            A1138Suporte_Codigo = T003711_A1138Suporte_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1138Suporte_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1138Suporte_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd37135( )
      {
         pr_default.close(9);
      }

      protected void AfterConfirm37135( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert37135( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate37135( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete37135( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete37135( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate37135( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes37135( )
      {
         edtSuporte_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSuporte_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSuporte_Codigo_Enabled), 5, 0)));
         cmbSuporte_Prioridade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSuporte_Prioridade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbSuporte_Prioridade.Enabled), 5, 0)));
         edtSuporte_Assunto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSuporte_Assunto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSuporte_Assunto_Enabled), 5, 0)));
         edtSuporte_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSuporte_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSuporte_Descricao_Enabled), 5, 0)));
         edtSuporte_Status_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSuporte_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSuporte_Status_Enabled), 5, 0)));
         edtSuporte_Responsavel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSuporte_Responsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSuporte_Responsavel_Enabled), 5, 0)));
         edtSuporte_Owner_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSuporte_Owner_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSuporte_Owner_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues370( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117271171");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("suporte.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1138Suporte_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1138Suporte_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1139Suporte_Prioridade", StringUtil.RTrim( Z1139Suporte_Prioridade));
         GxWebStd.gx_hidden_field( context, "Z1140Suporte_Assunto", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1140Suporte_Assunto), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1142Suporte_Status", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1142Suporte_Status), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1143Suporte_Responsavel", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1143Suporte_Responsavel), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1144Suporte_Owner", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1144Suporte_Owner), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("suporte.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Suporte" ;
      }

      public override String GetPgmdesc( )
      {
         return "Suporte" ;
      }

      protected void InitializeNonKey37135( )
      {
         A1139Suporte_Prioridade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1139Suporte_Prioridade", A1139Suporte_Prioridade);
         A1140Suporte_Assunto = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1140Suporte_Assunto", StringUtil.LTrim( StringUtil.Str( (decimal)(A1140Suporte_Assunto), 6, 0)));
         A1141Suporte_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1141Suporte_Descricao", A1141Suporte_Descricao);
         A1142Suporte_Status = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1142Suporte_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1142Suporte_Status), 4, 0)));
         A1143Suporte_Responsavel = 0;
         n1143Suporte_Responsavel = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1143Suporte_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1143Suporte_Responsavel), 4, 0)));
         n1143Suporte_Responsavel = ((0==A1143Suporte_Responsavel) ? true : false);
         A1144Suporte_Owner = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1144Suporte_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1144Suporte_Owner), 4, 0)));
         Z1139Suporte_Prioridade = "";
         Z1140Suporte_Assunto = 0;
         Z1142Suporte_Status = 0;
         Z1143Suporte_Responsavel = 0;
         Z1144Suporte_Owner = 0;
      }

      protected void InitAll37135( )
      {
         A1138Suporte_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1138Suporte_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1138Suporte_Codigo), 6, 0)));
         InitializeNonKey37135( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117271175");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("suporte.js", "?20203117271175");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblocksuporte_codigo_Internalname = "TEXTBLOCKSUPORTE_CODIGO";
         edtSuporte_Codigo_Internalname = "SUPORTE_CODIGO";
         lblTextblocksuporte_prioridade_Internalname = "TEXTBLOCKSUPORTE_PRIORIDADE";
         cmbSuporte_Prioridade_Internalname = "SUPORTE_PRIORIDADE";
         lblTextblocksuporte_assunto_Internalname = "TEXTBLOCKSUPORTE_ASSUNTO";
         edtSuporte_Assunto_Internalname = "SUPORTE_ASSUNTO";
         lblTextblocksuporte_descricao_Internalname = "TEXTBLOCKSUPORTE_DESCRICAO";
         edtSuporte_Descricao_Internalname = "SUPORTE_DESCRICAO";
         lblTextblocksuporte_status_Internalname = "TEXTBLOCKSUPORTE_STATUS";
         edtSuporte_Status_Internalname = "SUPORTE_STATUS";
         lblTextblocksuporte_responsavel_Internalname = "TEXTBLOCKSUPORTE_RESPONSAVEL";
         edtSuporte_Responsavel_Internalname = "SUPORTE_RESPONSAVEL";
         lblTextblocksuporte_owner_Internalname = "TEXTBLOCKSUPORTE_OWNER";
         edtSuporte_Owner_Internalname = "SUPORTE_OWNER";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Suporte";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtSuporte_Owner_Jsonclick = "";
         edtSuporte_Owner_Enabled = 1;
         edtSuporte_Responsavel_Jsonclick = "";
         edtSuporte_Responsavel_Enabled = 1;
         edtSuporte_Status_Jsonclick = "";
         edtSuporte_Status_Enabled = 1;
         edtSuporte_Descricao_Enabled = 1;
         edtSuporte_Assunto_Jsonclick = "";
         edtSuporte_Assunto_Enabled = 1;
         cmbSuporte_Prioridade_Jsonclick = "";
         cmbSuporte_Prioridade.Enabled = 1;
         edtSuporte_Codigo_Jsonclick = "";
         edtSuporte_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = cmbSuporte_Prioridade_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Suporte_codigo( int GX_Parm1 ,
                                        GXCombobox cmbGX_Parm2 ,
                                        int GX_Parm3 ,
                                        String GX_Parm4 ,
                                        short GX_Parm5 ,
                                        short GX_Parm6 ,
                                        short GX_Parm7 )
      {
         A1138Suporte_Codigo = GX_Parm1;
         cmbSuporte_Prioridade = cmbGX_Parm2;
         A1139Suporte_Prioridade = cmbSuporte_Prioridade.CurrentValue;
         cmbSuporte_Prioridade.CurrentValue = A1139Suporte_Prioridade;
         A1140Suporte_Assunto = GX_Parm3;
         A1141Suporte_Descricao = GX_Parm4;
         A1142Suporte_Status = GX_Parm5;
         A1143Suporte_Responsavel = GX_Parm6;
         n1143Suporte_Responsavel = false;
         A1144Suporte_Owner = GX_Parm7;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         cmbSuporte_Prioridade.CurrentValue = A1139Suporte_Prioridade;
         isValidOutput.Add(cmbSuporte_Prioridade);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1140Suporte_Assunto), 6, 0, ".", "")));
         isValidOutput.Add(A1141Suporte_Descricao);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1142Suporte_Status), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1143Suporte_Responsavel), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1144Suporte_Owner), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1138Suporte_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1139Suporte_Prioridade));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1140Suporte_Assunto), 6, 0, ",", "")));
         isValidOutput.Add(Z1141Suporte_Descricao);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1142Suporte_Status), 4, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1143Suporte_Responsavel), 4, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1144Suporte_Owner), 4, 0, ",", "")));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1139Suporte_Prioridade = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A1139Suporte_Prioridade = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblocksuporte_codigo_Jsonclick = "";
         lblTextblocksuporte_prioridade_Jsonclick = "";
         lblTextblocksuporte_assunto_Jsonclick = "";
         lblTextblocksuporte_descricao_Jsonclick = "";
         A1141Suporte_Descricao = "";
         lblTextblocksuporte_status_Jsonclick = "";
         lblTextblocksuporte_responsavel_Jsonclick = "";
         lblTextblocksuporte_owner_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1141Suporte_Descricao = "";
         T00374_A1138Suporte_Codigo = new int[1] ;
         T00374_A1139Suporte_Prioridade = new String[] {""} ;
         T00374_A1140Suporte_Assunto = new int[1] ;
         T00374_A1141Suporte_Descricao = new String[] {""} ;
         T00374_A1142Suporte_Status = new short[1] ;
         T00374_A1143Suporte_Responsavel = new short[1] ;
         T00374_n1143Suporte_Responsavel = new bool[] {false} ;
         T00374_A1144Suporte_Owner = new short[1] ;
         T00375_A1138Suporte_Codigo = new int[1] ;
         T00373_A1138Suporte_Codigo = new int[1] ;
         T00373_A1139Suporte_Prioridade = new String[] {""} ;
         T00373_A1140Suporte_Assunto = new int[1] ;
         T00373_A1141Suporte_Descricao = new String[] {""} ;
         T00373_A1142Suporte_Status = new short[1] ;
         T00373_A1143Suporte_Responsavel = new short[1] ;
         T00373_n1143Suporte_Responsavel = new bool[] {false} ;
         T00373_A1144Suporte_Owner = new short[1] ;
         sMode135 = "";
         T00376_A1138Suporte_Codigo = new int[1] ;
         T00377_A1138Suporte_Codigo = new int[1] ;
         T00372_A1138Suporte_Codigo = new int[1] ;
         T00372_A1139Suporte_Prioridade = new String[] {""} ;
         T00372_A1140Suporte_Assunto = new int[1] ;
         T00372_A1141Suporte_Descricao = new String[] {""} ;
         T00372_A1142Suporte_Status = new short[1] ;
         T00372_A1143Suporte_Responsavel = new short[1] ;
         T00372_n1143Suporte_Responsavel = new bool[] {false} ;
         T00372_A1144Suporte_Owner = new short[1] ;
         T00378_A1138Suporte_Codigo = new int[1] ;
         T003711_A1138Suporte_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.suporte__default(),
            new Object[][] {
                new Object[] {
               T00372_A1138Suporte_Codigo, T00372_A1139Suporte_Prioridade, T00372_A1140Suporte_Assunto, T00372_A1141Suporte_Descricao, T00372_A1142Suporte_Status, T00372_A1143Suporte_Responsavel, T00372_n1143Suporte_Responsavel, T00372_A1144Suporte_Owner
               }
               , new Object[] {
               T00373_A1138Suporte_Codigo, T00373_A1139Suporte_Prioridade, T00373_A1140Suporte_Assunto, T00373_A1141Suporte_Descricao, T00373_A1142Suporte_Status, T00373_A1143Suporte_Responsavel, T00373_n1143Suporte_Responsavel, T00373_A1144Suporte_Owner
               }
               , new Object[] {
               T00374_A1138Suporte_Codigo, T00374_A1139Suporte_Prioridade, T00374_A1140Suporte_Assunto, T00374_A1141Suporte_Descricao, T00374_A1142Suporte_Status, T00374_A1143Suporte_Responsavel, T00374_n1143Suporte_Responsavel, T00374_A1144Suporte_Owner
               }
               , new Object[] {
               T00375_A1138Suporte_Codigo
               }
               , new Object[] {
               T00376_A1138Suporte_Codigo
               }
               , new Object[] {
               T00377_A1138Suporte_Codigo
               }
               , new Object[] {
               T00378_A1138Suporte_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003711_A1138Suporte_Codigo
               }
            }
         );
      }

      private short Z1142Suporte_Status ;
      private short Z1143Suporte_Responsavel ;
      private short Z1144Suporte_Owner ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1142Suporte_Status ;
      private short A1143Suporte_Responsavel ;
      private short A1144Suporte_Owner ;
      private short GX_JID ;
      private short RcdFound135 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1138Suporte_Codigo ;
      private int Z1140Suporte_Assunto ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A1138Suporte_Codigo ;
      private int edtSuporte_Codigo_Enabled ;
      private int A1140Suporte_Assunto ;
      private int edtSuporte_Assunto_Enabled ;
      private int edtSuporte_Descricao_Enabled ;
      private int edtSuporte_Status_Enabled ;
      private int edtSuporte_Responsavel_Enabled ;
      private int edtSuporte_Owner_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String Z1139Suporte_Prioridade ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String A1139Suporte_Prioridade ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtSuporte_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblocksuporte_codigo_Internalname ;
      private String lblTextblocksuporte_codigo_Jsonclick ;
      private String edtSuporte_Codigo_Jsonclick ;
      private String lblTextblocksuporte_prioridade_Internalname ;
      private String lblTextblocksuporte_prioridade_Jsonclick ;
      private String cmbSuporte_Prioridade_Internalname ;
      private String cmbSuporte_Prioridade_Jsonclick ;
      private String lblTextblocksuporte_assunto_Internalname ;
      private String lblTextblocksuporte_assunto_Jsonclick ;
      private String edtSuporte_Assunto_Internalname ;
      private String edtSuporte_Assunto_Jsonclick ;
      private String lblTextblocksuporte_descricao_Internalname ;
      private String lblTextblocksuporte_descricao_Jsonclick ;
      private String edtSuporte_Descricao_Internalname ;
      private String lblTextblocksuporte_status_Internalname ;
      private String lblTextblocksuporte_status_Jsonclick ;
      private String edtSuporte_Status_Internalname ;
      private String edtSuporte_Status_Jsonclick ;
      private String lblTextblocksuporte_responsavel_Internalname ;
      private String lblTextblocksuporte_responsavel_Jsonclick ;
      private String edtSuporte_Responsavel_Internalname ;
      private String edtSuporte_Responsavel_Jsonclick ;
      private String lblTextblocksuporte_owner_Internalname ;
      private String lblTextblocksuporte_owner_Jsonclick ;
      private String edtSuporte_Owner_Internalname ;
      private String edtSuporte_Owner_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode135 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1143Suporte_Responsavel ;
      private String A1141Suporte_Descricao ;
      private String Z1141Suporte_Descricao ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbSuporte_Prioridade ;
      private IDataStoreProvider pr_default ;
      private int[] T00374_A1138Suporte_Codigo ;
      private String[] T00374_A1139Suporte_Prioridade ;
      private int[] T00374_A1140Suporte_Assunto ;
      private String[] T00374_A1141Suporte_Descricao ;
      private short[] T00374_A1142Suporte_Status ;
      private short[] T00374_A1143Suporte_Responsavel ;
      private bool[] T00374_n1143Suporte_Responsavel ;
      private short[] T00374_A1144Suporte_Owner ;
      private int[] T00375_A1138Suporte_Codigo ;
      private int[] T00373_A1138Suporte_Codigo ;
      private String[] T00373_A1139Suporte_Prioridade ;
      private int[] T00373_A1140Suporte_Assunto ;
      private String[] T00373_A1141Suporte_Descricao ;
      private short[] T00373_A1142Suporte_Status ;
      private short[] T00373_A1143Suporte_Responsavel ;
      private bool[] T00373_n1143Suporte_Responsavel ;
      private short[] T00373_A1144Suporte_Owner ;
      private int[] T00376_A1138Suporte_Codigo ;
      private int[] T00377_A1138Suporte_Codigo ;
      private int[] T00372_A1138Suporte_Codigo ;
      private String[] T00372_A1139Suporte_Prioridade ;
      private int[] T00372_A1140Suporte_Assunto ;
      private String[] T00372_A1141Suporte_Descricao ;
      private short[] T00372_A1142Suporte_Status ;
      private short[] T00372_A1143Suporte_Responsavel ;
      private bool[] T00372_n1143Suporte_Responsavel ;
      private short[] T00372_A1144Suporte_Owner ;
      private int[] T00378_A1138Suporte_Codigo ;
      private int[] T003711_A1138Suporte_Codigo ;
      private GXWebForm Form ;
   }

   public class suporte__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00374 ;
          prmT00374 = new Object[] {
          new Object[] {"@Suporte_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00375 ;
          prmT00375 = new Object[] {
          new Object[] {"@Suporte_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00373 ;
          prmT00373 = new Object[] {
          new Object[] {"@Suporte_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00376 ;
          prmT00376 = new Object[] {
          new Object[] {"@Suporte_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00377 ;
          prmT00377 = new Object[] {
          new Object[] {"@Suporte_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00372 ;
          prmT00372 = new Object[] {
          new Object[] {"@Suporte_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00378 ;
          prmT00378 = new Object[] {
          new Object[] {"@Suporte_Prioridade",SqlDbType.Char,1,0} ,
          new Object[] {"@Suporte_Assunto",SqlDbType.Int,6,0} ,
          new Object[] {"@Suporte_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Suporte_Status",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Suporte_Responsavel",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Suporte_Owner",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00379 ;
          prmT00379 = new Object[] {
          new Object[] {"@Suporte_Prioridade",SqlDbType.Char,1,0} ,
          new Object[] {"@Suporte_Assunto",SqlDbType.Int,6,0} ,
          new Object[] {"@Suporte_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Suporte_Status",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Suporte_Responsavel",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Suporte_Owner",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Suporte_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003710 ;
          prmT003710 = new Object[] {
          new Object[] {"@Suporte_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003711 ;
          prmT003711 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T00372", "SELECT [Suporte_Codigo], [Suporte_Prioridade], [Suporte_Assunto], [Suporte_Descricao], [Suporte_Status], [Suporte_Responsavel], [Suporte_Owner] FROM [Suporte] WITH (UPDLOCK) WHERE [Suporte_Codigo] = @Suporte_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00372,1,0,true,false )
             ,new CursorDef("T00373", "SELECT [Suporte_Codigo], [Suporte_Prioridade], [Suporte_Assunto], [Suporte_Descricao], [Suporte_Status], [Suporte_Responsavel], [Suporte_Owner] FROM [Suporte] WITH (NOLOCK) WHERE [Suporte_Codigo] = @Suporte_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00373,1,0,true,false )
             ,new CursorDef("T00374", "SELECT TM1.[Suporte_Codigo], TM1.[Suporte_Prioridade], TM1.[Suporte_Assunto], TM1.[Suporte_Descricao], TM1.[Suporte_Status], TM1.[Suporte_Responsavel], TM1.[Suporte_Owner] FROM [Suporte] TM1 WITH (NOLOCK) WHERE TM1.[Suporte_Codigo] = @Suporte_Codigo ORDER BY TM1.[Suporte_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00374,100,0,true,false )
             ,new CursorDef("T00375", "SELECT [Suporte_Codigo] FROM [Suporte] WITH (NOLOCK) WHERE [Suporte_Codigo] = @Suporte_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00375,1,0,true,false )
             ,new CursorDef("T00376", "SELECT TOP 1 [Suporte_Codigo] FROM [Suporte] WITH (NOLOCK) WHERE ( [Suporte_Codigo] > @Suporte_Codigo) ORDER BY [Suporte_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00376,1,0,true,true )
             ,new CursorDef("T00377", "SELECT TOP 1 [Suporte_Codigo] FROM [Suporte] WITH (NOLOCK) WHERE ( [Suporte_Codigo] < @Suporte_Codigo) ORDER BY [Suporte_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00377,1,0,true,true )
             ,new CursorDef("T00378", "INSERT INTO [Suporte]([Suporte_Prioridade], [Suporte_Assunto], [Suporte_Descricao], [Suporte_Status], [Suporte_Responsavel], [Suporte_Owner]) VALUES(@Suporte_Prioridade, @Suporte_Assunto, @Suporte_Descricao, @Suporte_Status, @Suporte_Responsavel, @Suporte_Owner); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT00378)
             ,new CursorDef("T00379", "UPDATE [Suporte] SET [Suporte_Prioridade]=@Suporte_Prioridade, [Suporte_Assunto]=@Suporte_Assunto, [Suporte_Descricao]=@Suporte_Descricao, [Suporte_Status]=@Suporte_Status, [Suporte_Responsavel]=@Suporte_Responsavel, [Suporte_Owner]=@Suporte_Owner  WHERE [Suporte_Codigo] = @Suporte_Codigo", GxErrorMask.GX_NOMASK,prmT00379)
             ,new CursorDef("T003710", "DELETE FROM [Suporte]  WHERE [Suporte_Codigo] = @Suporte_Codigo", GxErrorMask.GX_NOMASK,prmT003710)
             ,new CursorDef("T003711", "SELECT [Suporte_Codigo] FROM [Suporte] WITH (NOLOCK) ORDER BY [Suporte_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003711,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((short[]) buf[5])[0] = rslt.getShort(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((short[]) buf[7])[0] = rslt.getShort(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((short[]) buf[5])[0] = rslt.getShort(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((short[]) buf[7])[0] = rslt.getShort(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((short[]) buf[5])[0] = rslt.getShort(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((short[]) buf[7])[0] = rslt.getShort(7) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[5]);
                }
                stmt.SetParameter(6, (short)parms[6]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[5]);
                }
                stmt.SetParameter(6, (short)parms[6]);
                stmt.SetParameter(7, (int)parms[7]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
