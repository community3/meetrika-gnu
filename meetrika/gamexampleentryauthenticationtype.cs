/*
               File: GAMExampleEntryAuthenticationType
        Description: Authentication type
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:32:21.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexampleentryauthenticationtype : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gamexampleentryauthenticationtype( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamexampleentryauthenticationtype( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Gx_mode ,
                           ref String aP1_Name ,
                           ref String aP2_TypeIdDsp )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV35Name = aP1_Name;
         this.AV40TypeIdDsp = aP2_TypeIdDsp;
         executePrivate();
         aP0_Gx_mode=this.Gx_mode;
         aP1_Name=this.AV35Name;
         aP2_TypeIdDsp=this.AV40TypeIdDsp;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavTypeid = new GXCombobox();
         cmbavFunctionid = new GXCombobox();
         chkavIsenable = new GXCheckbox();
         cmbavWsversion = new GXCombobox();
         cmbavWsserversecureprotocol = new GXCombobox();
         cmbavCusversion = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               Gx_mode = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV35Name = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
                  AV40TypeIdDsp = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TypeIdDsp", AV40TypeIdDsp);
               }
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("gammasterpagepopup", "GeneXus.Programs.gammasterpagepopup", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA2C2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START2C2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117322556");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamexampleentryauthenticationtype.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(StringUtil.RTrim(AV35Name)) + "," + UrlEncode(StringUtil.RTrim(AV40TypeIdDsp))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vTYPEIDDSP", StringUtil.RTrim( AV40TypeIdDsp));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE2C2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT2C2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gamexampleentryauthenticationtype.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(StringUtil.RTrim(AV35Name)) + "," + UrlEncode(StringUtil.RTrim(AV40TypeIdDsp)) ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleEntryAuthenticationType" ;
      }

      public override String GetPgmdesc( )
      {
         return "Authentication type" ;
      }

      protected void WB2C0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table1_3_2C2( true) ;
         }
         else
         {
            wb_table1_3_2C2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_2C2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table2_29_2C2( true) ;
         }
         else
         {
            wb_table2_29_2C2( false) ;
         }
         return  ;
      }

      protected void wb_table2_29_2C2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table3_35_2C2( true) ;
         }
         else
         {
            wb_table3_35_2C2( false) ;
         }
         return  ;
      }

      protected void wb_table3_35_2C2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table4_51_2C2( true) ;
         }
         else
         {
            wb_table4_51_2C2( false) ;
         }
         return  ;
      }

      protected void wb_table4_51_2C2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table5_67_2C2( true) ;
         }
         else
         {
            wb_table5_67_2C2( false) ;
         }
         return  ;
      }

      protected void wb_table5_67_2C2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table6_73_2C2( true) ;
         }
         else
         {
            wb_table6_73_2C2( false) ;
         }
         return  ;
      }

      protected void wb_table6_73_2C2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table7_84_2C2( true) ;
         }
         else
         {
            wb_table7_84_2C2( false) ;
         }
         return  ;
      }

      protected void wb_table7_84_2C2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table8_136_2C2( true) ;
         }
         else
         {
            wb_table8_136_2C2( false) ;
         }
         return  ;
      }

      protected void wb_table8_136_2C2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table9_163_2C2( true) ;
         }
         else
         {
            wb_table9_163_2C2( false) ;
         }
         return  ;
      }

      protected void wb_table9_163_2C2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START2C2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Authentication type", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP2C0( ) ;
      }

      protected void WS2C2( )
      {
         START2C2( ) ;
         EVT2C2( ) ;
      }

      protected void EVT2C2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E112C2 */
                              E112C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E122C2 */
                              E122C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VTYPEID.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E132C2 */
                              E132C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E142C2 */
                                    E142C2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CLOSE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E152C2 */
                              E152C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'GENERATEKEY'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E162C2 */
                              E162C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'GENERATEKEYCUSTOM'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E172C2 */
                              E172C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E182C2 */
                              E182C2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE2C2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA2C2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            cmbavTypeid.Name = "vTYPEID";
            cmbavTypeid.WebTags = "";
            cmbavTypeid.addItem("Custom", "Custom", 0);
            cmbavTypeid.addItem("ExternalWebService", "External Web Service", 0);
            cmbavTypeid.addItem("Facebook", "Facebook", 0);
            cmbavTypeid.addItem("GAMLocal", "GAM Local", 0);
            cmbavTypeid.addItem("GAMRemote", "GAM Remote", 0);
            cmbavTypeid.addItem("Google", "Google", 0);
            cmbavTypeid.addItem("Twitter", "Twitter", 0);
            if ( cmbavTypeid.ItemCount > 0 )
            {
               AV39TypeId = cmbavTypeid.getValidValue(AV39TypeId);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TypeId", AV39TypeId);
            }
            cmbavFunctionid.Name = "vFUNCTIONID";
            cmbavFunctionid.WebTags = "";
            cmbavFunctionid.addItem("AuthenticationAndRoles", "Authentication and Roles", 0);
            cmbavFunctionid.addItem("OnlyAuthentication", "Only Authentication", 0);
            if ( cmbavFunctionid.ItemCount > 0 )
            {
               AV29FunctionId = cmbavFunctionid.getValidValue(AV29FunctionId);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FunctionId", AV29FunctionId);
            }
            chkavIsenable.Name = "vISENABLE";
            chkavIsenable.WebTags = "";
            chkavIsenable.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsenable_Internalname, "TitleCaption", chkavIsenable.Caption);
            chkavIsenable.CheckedValue = "false";
            cmbavWsversion.Name = "vWSVERSION";
            cmbavWsversion.WebTags = "";
            cmbavWsversion.addItem("GAM10", "Version 1.0", 0);
            cmbavWsversion.addItem("GAM20", "Version 2.0", 0);
            if ( cmbavWsversion.ItemCount > 0 )
            {
               AV53WSVersion = cmbavWsversion.getValidValue(AV53WSVersion);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53WSVersion", AV53WSVersion);
            }
            cmbavWsserversecureprotocol.Name = "vWSSERVERSECUREPROTOCOL";
            cmbavWsserversecureprotocol.WebTags = "";
            cmbavWsserversecureprotocol.addItem("0", "No", 0);
            cmbavWsserversecureprotocol.addItem("1", "Yes", 0);
            if ( cmbavWsserversecureprotocol.ItemCount > 0 )
            {
               AV50WSServerSecureProtocol = (short)(NumberUtil.Val( cmbavWsserversecureprotocol.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV50WSServerSecureProtocol), 1, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50WSServerSecureProtocol", StringUtil.Str( (decimal)(AV50WSServerSecureProtocol), 1, 0));
            }
            cmbavCusversion.Name = "vCUSVERSION";
            cmbavCusversion.WebTags = "";
            cmbavCusversion.addItem("GAM10", "Version 1.0", 0);
            cmbavCusversion.addItem("GAM20", "Version 2.0", 0);
            if ( cmbavCusversion.ItemCount > 0 )
            {
               AV25CusVersion = cmbavCusversion.getValidValue(AV25CusVersion);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25CusVersion", AV25CusVersion);
            }
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavTypeid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavTypeid.ItemCount > 0 )
         {
            AV39TypeId = cmbavTypeid.getValidValue(AV39TypeId);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TypeId", AV39TypeId);
         }
         if ( cmbavFunctionid.ItemCount > 0 )
         {
            AV29FunctionId = cmbavFunctionid.getValidValue(AV29FunctionId);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FunctionId", AV29FunctionId);
         }
         if ( cmbavWsversion.ItemCount > 0 )
         {
            AV53WSVersion = cmbavWsversion.getValidValue(AV53WSVersion);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53WSVersion", AV53WSVersion);
         }
         if ( cmbavWsserversecureprotocol.ItemCount > 0 )
         {
            AV50WSServerSecureProtocol = (short)(NumberUtil.Val( cmbavWsserversecureprotocol.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV50WSServerSecureProtocol), 1, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50WSServerSecureProtocol", StringUtil.Str( (decimal)(AV50WSServerSecureProtocol), 1, 0));
         }
         if ( cmbavCusversion.ItemCount > 0 )
         {
            AV25CusVersion = cmbavCusversion.getValidValue(AV25CusVersion);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25CusVersion", AV25CusVersion);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF2C2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF2C2( )
      {
         /* Execute user event: E122C2 */
         E122C2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E182C2 */
            E182C2 ();
            WB2C0( ) ;
         }
      }

      protected void STRUP2C0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112C2 */
         E112C2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavTypeid.CurrentValue = cgiGet( cmbavTypeid_Internalname);
            AV39TypeId = cgiGet( cmbavTypeid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TypeId", AV39TypeId);
            AV35Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
            cmbavFunctionid.CurrentValue = cgiGet( cmbavFunctionid_Internalname);
            AV29FunctionId = cgiGet( cmbavFunctionid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FunctionId", AV29FunctionId);
            AV34IsEnable = StringUtil.StrToBool( cgiGet( chkavIsenable_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34IsEnable", AV34IsEnable);
            AV26Dsc = cgiGet( edtavDsc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Dsc", AV26Dsc);
            AV33Impersonate = cgiGet( edtavImpersonate_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Impersonate", AV33Impersonate);
            AV17ClientId = cgiGet( edtavClientid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ClientId", AV17ClientId);
            AV18ClientSecret = cgiGet( edtavClientsecret_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ClientSecret", AV18ClientSecret);
            AV37SiteURL = cgiGet( edtavSiteurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37SiteURL", AV37SiteURL);
            AV19ConsumerKey = cgiGet( edtavConsumerkey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ConsumerKey", AV19ConsumerKey);
            AV20ConsumerSecret = cgiGet( edtavConsumersecret_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ConsumerSecret", AV20ConsumerSecret);
            AV16CallbackURL = cgiGet( edtavCallbackurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16CallbackURL", AV16CallbackURL);
            AV5AdditionalScope = cgiGet( edtavAdditionalscope_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AdditionalScope", AV5AdditionalScope);
            AV31GAMRServerURL = cgiGet( edtavGamrserverurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31GAMRServerURL", AV31GAMRServerURL);
            AV30GAMRPrivateEncryptKey = cgiGet( edtavGamrprivateencryptkey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30GAMRPrivateEncryptKey", AV30GAMRPrivateEncryptKey);
            cmbavWsversion.CurrentValue = cgiGet( cmbavWsversion_Internalname);
            AV53WSVersion = cgiGet( cmbavWsversion_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53WSVersion", AV53WSVersion);
            AV46WSPrivateEncryptKey = cgiGet( edtavWsprivateencryptkey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46WSPrivateEncryptKey", AV46WSPrivateEncryptKey);
            AV48WSServerName = cgiGet( edtavWsservername_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48WSServerName", AV48WSServerName);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavWsserverport_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavWsserverport_Internalname), ",", ".") > Convert.ToDecimal( 99999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vWSSERVERPORT");
               GX_FocusControl = edtavWsserverport_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49WSServerPort = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49WSServerPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49WSServerPort), 5, 0)));
            }
            else
            {
               AV49WSServerPort = (int)(context.localUtil.CToN( cgiGet( edtavWsserverport_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49WSServerPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49WSServerPort), 5, 0)));
            }
            AV47WSServerBaseURL = cgiGet( edtavWsserverbaseurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47WSServerBaseURL", AV47WSServerBaseURL);
            cmbavWsserversecureprotocol.CurrentValue = cgiGet( cmbavWsserversecureprotocol_Internalname);
            AV50WSServerSecureProtocol = (short)(NumberUtil.Val( cgiGet( cmbavWsserversecureprotocol_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50WSServerSecureProtocol", StringUtil.Str( (decimal)(AV50WSServerSecureProtocol), 1, 0));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavWstimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavWstimeout_Internalname), ",", ".") > Convert.ToDecimal( 99999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vWSTIMEOUT");
               GX_FocusControl = edtavWstimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52WSTimeout = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52WSTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52WSTimeout), 5, 0)));
            }
            else
            {
               AV52WSTimeout = (int)(context.localUtil.CToN( cgiGet( edtavWstimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52WSTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52WSTimeout), 5, 0)));
            }
            AV45WSPackage = cgiGet( edtavWspackage_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45WSPackage", AV45WSPackage);
            AV43WSName = cgiGet( edtavWsname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43WSName", AV43WSName);
            AV41WSExtension = cgiGet( edtavWsextension_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41WSExtension", AV41WSExtension);
            cmbavCusversion.CurrentValue = cgiGet( cmbavCusversion_Internalname);
            AV25CusVersion = cgiGet( cmbavCusversion_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25CusVersion", AV25CusVersion);
            AV24CusPrivateEncryptKey = cgiGet( edtavCusprivateencryptkey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24CusPrivateEncryptKey", AV24CusPrivateEncryptKey);
            AV22CusFileName = cgiGet( edtavCusfilename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22CusFileName", AV22CusFileName);
            AV23CusPackage = cgiGet( edtavCuspackage_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23CusPackage", AV23CusPackage);
            AV21CusClassName = cgiGet( edtavCusclassname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21CusClassName", AV21CusClassName);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E112C2 */
         E112C2 ();
         if (returnInSub) return;
      }

      protected void E112C2( )
      {
         /* Start Routine */
         AV39TypeId = AV40TypeIdDsp;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TypeId", AV39TypeId);
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            cmbavTypeid.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTypeid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTypeid.Enabled), 5, 0)));
            edtavName_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)));
         }
         else
         {
            cmbavTypeid.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTypeid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTypeid.Enabled), 5, 0)));
            edtavName_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)));
            AV29FunctionId = "OnlyAuthentication";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FunctionId", AV29FunctionId);
            if ( StringUtil.StrCmp(AV39TypeId, "GAMLocal") == 0 )
            {
               AV12AuthenticationTypeLocal.load( AV35Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV35Name = AV12AuthenticationTypeLocal.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV29FunctionId = AV12AuthenticationTypeLocal.gxTpr_Functionid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FunctionId", AV29FunctionId);
               AV34IsEnable = AV12AuthenticationTypeLocal.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34IsEnable", AV34IsEnable);
               AV26Dsc = AV12AuthenticationTypeLocal.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Dsc", AV26Dsc);
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "Facebook") == 0 )
            {
               cmbavFunctionid.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)));
               AV9AuthenticationTypeFacebook.load( AV35Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV35Name = AV9AuthenticationTypeFacebook.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV34IsEnable = AV9AuthenticationTypeFacebook.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34IsEnable", AV34IsEnable);
               AV26Dsc = AV9AuthenticationTypeFacebook.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Dsc", AV26Dsc);
               AV17ClientId = AV9AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Clientid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ClientId", AV17ClientId);
               AV18ClientSecret = AV9AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Clientsecret;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ClientSecret", AV18ClientSecret);
               AV37SiteURL = AV9AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Siteurl;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37SiteURL", AV37SiteURL);
               AV5AdditionalScope = AV9AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Additionalscope;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AdditionalScope", AV5AdditionalScope);
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "Google") == 0 )
            {
               cmbavFunctionid.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)));
               AV11AuthenticationTypeGoogle.load( AV35Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV35Name = AV11AuthenticationTypeGoogle.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV34IsEnable = AV11AuthenticationTypeGoogle.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34IsEnable", AV34IsEnable);
               AV26Dsc = AV11AuthenticationTypeGoogle.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Dsc", AV26Dsc);
               AV17ClientId = AV11AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Clientid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ClientId", AV17ClientId);
               AV18ClientSecret = AV11AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Clientsecret;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ClientSecret", AV18ClientSecret);
               AV37SiteURL = AV11AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Siteurl;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37SiteURL", AV37SiteURL);
               AV5AdditionalScope = AV11AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Additionalscope;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AdditionalScope", AV5AdditionalScope);
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "GAMRemote") == 0 )
            {
               cmbavFunctionid.Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)));
               AV10AuthenticationTypeGAMRemote.load( AV35Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV35Name = AV10AuthenticationTypeGAMRemote.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV29FunctionId = AV10AuthenticationTypeGAMRemote.gxTpr_Functionid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FunctionId", AV29FunctionId);
               AV34IsEnable = AV10AuthenticationTypeGAMRemote.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34IsEnable", AV34IsEnable);
               AV26Dsc = AV10AuthenticationTypeGAMRemote.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Dsc", AV26Dsc);
               AV33Impersonate = AV10AuthenticationTypeGAMRemote.gxTpr_Impersonate;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Impersonate", AV33Impersonate);
               AV17ClientId = AV10AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Clientid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ClientId", AV17ClientId);
               AV18ClientSecret = AV10AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Clientsecret;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ClientSecret", AV18ClientSecret);
               AV37SiteURL = AV10AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Siteurl;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37SiteURL", AV37SiteURL);
               AV5AdditionalScope = AV10AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Additionalscope;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AdditionalScope", AV5AdditionalScope);
               AV31GAMRServerURL = AV10AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Remoteserverurl;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31GAMRServerURL", AV31GAMRServerURL);
               AV30GAMRPrivateEncryptKey = AV10AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Remoteserverkey;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30GAMRPrivateEncryptKey", AV30GAMRPrivateEncryptKey);
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "Twitter") == 0 )
            {
               cmbavFunctionid.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)));
               AV14AuthenticationTypeTwitter.load( AV35Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV35Name = AV14AuthenticationTypeTwitter.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV34IsEnable = AV14AuthenticationTypeTwitter.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34IsEnable", AV34IsEnable);
               AV26Dsc = AV14AuthenticationTypeTwitter.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Dsc", AV26Dsc);
               AV19ConsumerKey = AV14AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Consumerkey;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ConsumerKey", AV19ConsumerKey);
               AV20ConsumerSecret = AV14AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Consumersecret;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ConsumerSecret", AV20ConsumerSecret);
               AV16CallbackURL = AV14AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Callbackurl;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16CallbackURL", AV16CallbackURL);
               AV5AdditionalScope = AV14AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Additionalscope;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AdditionalScope", AV5AdditionalScope);
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "ExternalWebService") == 0 )
            {
               cmbavFunctionid.Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)));
               AV15AuthenticationTypeWebService.load( AV35Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV35Name = AV15AuthenticationTypeWebService.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV29FunctionId = AV15AuthenticationTypeWebService.gxTpr_Functionid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FunctionId", AV29FunctionId);
               AV34IsEnable = AV15AuthenticationTypeWebService.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34IsEnable", AV34IsEnable);
               AV26Dsc = AV15AuthenticationTypeWebService.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Dsc", AV26Dsc);
               AV33Impersonate = AV15AuthenticationTypeWebService.gxTpr_Impersonate;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Impersonate", AV33Impersonate);
               AV53WSVersion = AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Version;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53WSVersion", AV53WSVersion);
               AV46WSPrivateEncryptKey = AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Privateencryptkey;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46WSPrivateEncryptKey", AV46WSPrivateEncryptKey);
               AV48WSServerName = AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48WSServerName", AV48WSServerName);
               AV49WSServerPort = AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Port;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49WSServerPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49WSServerPort), 5, 0)));
               AV47WSServerBaseURL = AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Baseurl;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47WSServerBaseURL", AV47WSServerBaseURL);
               AV50WSServerSecureProtocol = AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Secureprotocol;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50WSServerSecureProtocol", StringUtil.Str( (decimal)(AV50WSServerSecureProtocol), 1, 0));
               AV52WSTimeout = AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Timeout;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52WSTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52WSTimeout), 5, 0)));
               AV45WSPackage = AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Package;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45WSPackage", AV45WSPackage);
               AV43WSName = AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43WSName", AV43WSName);
               AV41WSExtension = AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Extension;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41WSExtension", AV41WSExtension);
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "Custom") == 0 )
            {
               cmbavFunctionid.Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)));
               AV8AuthenticationTypeCustom.load( AV35Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV35Name = AV8AuthenticationTypeCustom.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
               AV29FunctionId = AV8AuthenticationTypeCustom.gxTpr_Functionid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FunctionId", AV29FunctionId);
               AV34IsEnable = AV8AuthenticationTypeCustom.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34IsEnable", AV34IsEnable);
               AV26Dsc = AV8AuthenticationTypeCustom.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Dsc", AV26Dsc);
               AV33Impersonate = AV8AuthenticationTypeCustom.gxTpr_Impersonate;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Impersonate", AV33Impersonate);
               AV25CusVersion = AV8AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Version;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25CusVersion", AV25CusVersion);
               AV24CusPrivateEncryptKey = AV8AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Privateencryptkey;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24CusPrivateEncryptKey", AV24CusPrivateEncryptKey);
               AV22CusFileName = AV8AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Filename;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22CusFileName", AV22CusFileName);
               AV23CusPackage = AV8AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Package;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23CusPackage", AV23CusPackage);
               AV21CusClassName = AV8AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Classname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21CusClassName", AV21CusClassName);
            }
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtnconfirm_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirm_Visible), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtngenkey_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtngenkey_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtngenkey_Visible), 5, 0)));
            bttBtngenkeycustom_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtngenkeycustom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtngenkeycustom_Visible), 5, 0)));
            cmbavTypeid.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTypeid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTypeid.Enabled), 5, 0)));
            cmbavFunctionid.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)));
            chkavIsenable.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsenable_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavIsenable.Enabled), 5, 0)));
            edtavDsc_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDsc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDsc_Enabled), 5, 0)));
            edtavImpersonate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavImpersonate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavImpersonate_Enabled), 5, 0)));
            cmbavWsversion.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavWsversion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavWsversion.Enabled), 5, 0)));
            edtavWsprivateencryptkey_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWsprivateencryptkey_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWsprivateencryptkey_Enabled), 5, 0)));
            edtavWsservername_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWsservername_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWsservername_Enabled), 5, 0)));
            edtavWsserverport_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWsserverport_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWsserverport_Enabled), 5, 0)));
            edtavWsserverbaseurl_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWsserverbaseurl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWsserverbaseurl_Enabled), 5, 0)));
            cmbavWsserversecureprotocol.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavWsserversecureprotocol_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavWsserversecureprotocol.Enabled), 5, 0)));
            edtavWstimeout_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWstimeout_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWstimeout_Enabled), 5, 0)));
            edtavWspackage_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWspackage_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWspackage_Enabled), 5, 0)));
            edtavWsname_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWsname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWsname_Enabled), 5, 0)));
            edtavWsextension_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWsextension_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWsextension_Enabled), 5, 0)));
            edtavClientid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientid_Enabled), 5, 0)));
            edtavClientsecret_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientsecret_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientsecret_Enabled), 5, 0)));
            edtavSiteurl_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSiteurl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSiteurl_Enabled), 5, 0)));
            edtavAdditionalscope_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAdditionalscope_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAdditionalscope_Enabled), 5, 0)));
            edtavConsumerkey_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConsumerkey_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConsumerkey_Enabled), 5, 0)));
            edtavConsumersecret_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConsumersecret_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConsumersecret_Enabled), 5, 0)));
            edtavCallbackurl_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCallbackurl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCallbackurl_Enabled), 5, 0)));
            cmbavCusversion.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCusversion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCusversion.Enabled), 5, 0)));
            edtavCusprivateencryptkey_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCusprivateencryptkey_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCusprivateencryptkey_Enabled), 5, 0)));
            edtavCusfilename_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCusfilename_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCusfilename_Enabled), 5, 0)));
            edtavCuspackage_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCuspackage_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCuspackage_Enabled), 5, 0)));
            edtavCusclassname_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCusclassname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCusclassname_Enabled), 5, 0)));
            bttBtnconfirm_Caption = "Delete";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Caption", bttBtnconfirm_Caption);
         }
      }

      protected void E122C2( )
      {
         /* Refresh Routine */
         tblTblimpersonate_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblimpersonate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblimpersonate_Visible), 5, 0)));
         tblTblfacebook_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblfacebook_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblfacebook_Visible), 5, 0)));
         tblTblcommonadditional_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcommonadditional_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcommonadditional_Visible), 5, 0)));
         tblTblserverhost_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblserverhost_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblserverhost_Visible), 5, 0)));
         tblTbltwitter_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbltwitter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbltwitter_Visible), 5, 0)));
         tblTblwebservice_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblwebservice_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblwebservice_Visible), 5, 0)));
         tblTblexternal_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblexternal_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblexternal_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV39TypeId, "GAMLocal") == 0 )
         {
         }
         else if ( ( StringUtil.StrCmp(AV39TypeId, "Facebook") == 0 ) || ( StringUtil.StrCmp(AV39TypeId, "Google") == 0 ) || ( StringUtil.StrCmp(AV39TypeId, "GAMRemote") == 0 ) )
         {
            tblTblfacebook_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblfacebook_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblfacebook_Visible), 5, 0)));
            tblTblcommonadditional_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcommonadditional_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcommonadditional_Visible), 5, 0)));
            if ( StringUtil.StrCmp(AV39TypeId, "GAMRemote") == 0 )
            {
               tblTblimpersonate_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblimpersonate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblimpersonate_Visible), 5, 0)));
               tblTblserverhost_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblserverhost_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblserverhost_Visible), 5, 0)));
            }
         }
         else if ( StringUtil.StrCmp(AV39TypeId, "Twitter") == 0 )
         {
            tblTbltwitter_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbltwitter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbltwitter_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV39TypeId, "ExternalWebService") == 0 )
         {
            tblTblimpersonate_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblimpersonate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblimpersonate_Visible), 5, 0)));
            tblTblwebservice_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblwebservice_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblwebservice_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV39TypeId, "Custom") == 0 )
         {
            tblTblimpersonate_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblimpersonate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblimpersonate_Visible), 5, 0)));
            tblTblexternal_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblexternal_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblexternal_Visible), 5, 0)));
         }
      }

      protected void E132C2( )
      {
         /* Typeid_Click Routine */
         context.DoAjaxRefresh();
      }

      public void GXEnter( )
      {
         /* Execute user event: E142C2 */
         E142C2 ();
         if (returnInSub) return;
      }

      protected void E142C2( )
      {
         /* Enter Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) )
         {
            if ( StringUtil.StrCmp(AV39TypeId, "GAMLocal") == 0 )
            {
               AV12AuthenticationTypeLocal.gxTpr_Name = AV35Name;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12AuthenticationTypeLocal", AV12AuthenticationTypeLocal);
               AV12AuthenticationTypeLocal.gxTpr_Functionid = AV29FunctionId;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12AuthenticationTypeLocal", AV12AuthenticationTypeLocal);
               AV12AuthenticationTypeLocal.gxTpr_Isenable = AV34IsEnable;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12AuthenticationTypeLocal", AV12AuthenticationTypeLocal);
               AV12AuthenticationTypeLocal.gxTpr_Description = AV26Dsc;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12AuthenticationTypeLocal", AV12AuthenticationTypeLocal);
               AV12AuthenticationTypeLocal.save();
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "Facebook") == 0 )
            {
               AV9AuthenticationTypeFacebook.gxTpr_Name = AV35Name;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9AuthenticationTypeFacebook", AV9AuthenticationTypeFacebook);
               AV9AuthenticationTypeFacebook.gxTpr_Isenable = AV34IsEnable;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9AuthenticationTypeFacebook", AV9AuthenticationTypeFacebook);
               AV9AuthenticationTypeFacebook.gxTpr_Description = AV26Dsc;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9AuthenticationTypeFacebook", AV9AuthenticationTypeFacebook);
               AV9AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Clientid = AV17ClientId;
               AV9AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Clientsecret = AV18ClientSecret;
               AV9AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Siteurl = AV37SiteURL;
               AV9AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Additionalscope = AV5AdditionalScope;
               AV9AuthenticationTypeFacebook.save();
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "Google") == 0 )
            {
               AV11AuthenticationTypeGoogle.gxTpr_Name = AV35Name;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11AuthenticationTypeGoogle", AV11AuthenticationTypeGoogle);
               AV11AuthenticationTypeGoogle.gxTpr_Isenable = AV34IsEnable;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11AuthenticationTypeGoogle", AV11AuthenticationTypeGoogle);
               AV11AuthenticationTypeGoogle.gxTpr_Description = AV26Dsc;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11AuthenticationTypeGoogle", AV11AuthenticationTypeGoogle);
               AV11AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Clientid = AV17ClientId;
               AV11AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Clientsecret = AV18ClientSecret;
               AV11AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Siteurl = AV37SiteURL;
               AV11AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Additionalscope = AV5AdditionalScope;
               AV11AuthenticationTypeGoogle.save();
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "GAMRemote") == 0 )
            {
               AV10AuthenticationTypeGAMRemote.gxTpr_Name = AV35Name;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10AuthenticationTypeGAMRemote", AV10AuthenticationTypeGAMRemote);
               AV10AuthenticationTypeGAMRemote.gxTpr_Functionid = AV29FunctionId;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10AuthenticationTypeGAMRemote", AV10AuthenticationTypeGAMRemote);
               AV10AuthenticationTypeGAMRemote.gxTpr_Isenable = AV34IsEnable;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10AuthenticationTypeGAMRemote", AV10AuthenticationTypeGAMRemote);
               AV10AuthenticationTypeGAMRemote.gxTpr_Description = AV26Dsc;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10AuthenticationTypeGAMRemote", AV10AuthenticationTypeGAMRemote);
               AV10AuthenticationTypeGAMRemote.gxTpr_Impersonate = AV33Impersonate;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10AuthenticationTypeGAMRemote", AV10AuthenticationTypeGAMRemote);
               AV10AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Clientid = AV17ClientId;
               AV10AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Clientsecret = AV18ClientSecret;
               AV10AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Siteurl = AV37SiteURL;
               AV10AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Additionalscope = AV5AdditionalScope;
               AV10AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Remoteserverurl = AV31GAMRServerURL;
               AV10AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Remoteserverkey = AV30GAMRPrivateEncryptKey;
               AV10AuthenticationTypeGAMRemote.save();
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "Twitter") == 0 )
            {
               AV14AuthenticationTypeTwitter.gxTpr_Name = AV35Name;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14AuthenticationTypeTwitter", AV14AuthenticationTypeTwitter);
               AV14AuthenticationTypeTwitter.gxTpr_Isenable = AV34IsEnable;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14AuthenticationTypeTwitter", AV14AuthenticationTypeTwitter);
               AV14AuthenticationTypeTwitter.gxTpr_Description = AV26Dsc;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14AuthenticationTypeTwitter", AV14AuthenticationTypeTwitter);
               AV14AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Consumerkey = AV19ConsumerKey;
               AV14AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Consumersecret = AV20ConsumerSecret;
               AV14AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Callbackurl = AV16CallbackURL;
               AV14AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Additionalscope = AV5AdditionalScope;
               AV14AuthenticationTypeTwitter.save();
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "ExternalWebService") == 0 )
            {
               cmbavFunctionid.Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)));
               AV15AuthenticationTypeWebService.gxTpr_Name = AV35Name;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15AuthenticationTypeWebService", AV15AuthenticationTypeWebService);
               AV15AuthenticationTypeWebService.gxTpr_Functionid = AV29FunctionId;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15AuthenticationTypeWebService", AV15AuthenticationTypeWebService);
               AV15AuthenticationTypeWebService.gxTpr_Isenable = AV34IsEnable;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15AuthenticationTypeWebService", AV15AuthenticationTypeWebService);
               AV15AuthenticationTypeWebService.gxTpr_Description = AV26Dsc;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15AuthenticationTypeWebService", AV15AuthenticationTypeWebService);
               AV15AuthenticationTypeWebService.gxTpr_Impersonate = AV33Impersonate;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15AuthenticationTypeWebService", AV15AuthenticationTypeWebService);
               AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Version = AV53WSVersion;
               AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Privateencryptkey = AV46WSPrivateEncryptKey;
               AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Timeout = AV52WSTimeout;
               AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Package = AV45WSPackage;
               AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Name = AV43WSName;
               AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Extension = AV41WSExtension;
               AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Name = AV48WSServerName;
               AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Port = AV49WSServerPort;
               AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Baseurl = AV47WSServerBaseURL;
               AV15AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Secureprotocol = AV50WSServerSecureProtocol;
               AV15AuthenticationTypeWebService.save();
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "Custom") == 0 )
            {
               cmbavFunctionid.Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)));
               AV8AuthenticationTypeCustom.gxTpr_Name = AV35Name;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV8AuthenticationTypeCustom", AV8AuthenticationTypeCustom);
               AV8AuthenticationTypeCustom.gxTpr_Functionid = AV29FunctionId;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV8AuthenticationTypeCustom", AV8AuthenticationTypeCustom);
               AV8AuthenticationTypeCustom.gxTpr_Isenable = AV34IsEnable;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV8AuthenticationTypeCustom", AV8AuthenticationTypeCustom);
               AV8AuthenticationTypeCustom.gxTpr_Description = AV26Dsc;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV8AuthenticationTypeCustom", AV8AuthenticationTypeCustom);
               AV8AuthenticationTypeCustom.gxTpr_Impersonate = AV33Impersonate;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV8AuthenticationTypeCustom", AV8AuthenticationTypeCustom);
               AV8AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Version = AV25CusVersion;
               AV8AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Privateencryptkey = AV24CusPrivateEncryptKey;
               AV8AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Filename = AV22CusFileName;
               AV8AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Package = AV23CusPackage;
               AV8AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Classname = AV21CusClassName;
               AV8AuthenticationTypeCustom.save();
            }
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            if ( StringUtil.StrCmp(AV39TypeId, "GAMLocal") == 0 )
            {
               AV12AuthenticationTypeLocal.delete();
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "Facebook") == 0 )
            {
               AV9AuthenticationTypeFacebook.delete();
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "Google") == 0 )
            {
               AV11AuthenticationTypeGoogle.delete();
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "GAMRemote") == 0 )
            {
               AV10AuthenticationTypeGAMRemote.delete();
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "Twitter") == 0 )
            {
               AV14AuthenticationTypeTwitter.delete();
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "ExternalWebService") == 0 )
            {
               AV15AuthenticationTypeWebService.delete();
            }
            else if ( StringUtil.StrCmp(AV39TypeId, "Custom") == 0 )
            {
               AV8AuthenticationTypeCustom.delete();
            }
         }
         if ( StringUtil.StrCmp(AV39TypeId, "GAMLocal") == 0 )
         {
            if ( AV12AuthenticationTypeLocal.success() )
            {
               context.CommitDataStores( "GAMExampleEntryAuthenticationType");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV35Name,(String)AV40TypeIdDsp});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV39TypeId, "Facebook") == 0 )
         {
            if ( AV9AuthenticationTypeFacebook.success() )
            {
               context.CommitDataStores( "GAMExampleEntryAuthenticationType");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV35Name,(String)AV40TypeIdDsp});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV39TypeId, "Google") == 0 )
         {
            if ( AV11AuthenticationTypeGoogle.success() )
            {
               context.CommitDataStores( "GAMExampleEntryAuthenticationType");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV35Name,(String)AV40TypeIdDsp});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV39TypeId, "GAMRemote") == 0 )
         {
            if ( AV10AuthenticationTypeGAMRemote.success() )
            {
               context.CommitDataStores( "GAMExampleEntryAuthenticationType");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV35Name,(String)AV40TypeIdDsp});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV39TypeId, "Twitter") == 0 )
         {
            if ( AV14AuthenticationTypeTwitter.success() )
            {
               context.CommitDataStores( "GAMExampleEntryAuthenticationType");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV35Name,(String)AV40TypeIdDsp});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV39TypeId, "ExternalWebService") == 0 )
         {
            if ( AV15AuthenticationTypeWebService.success() )
            {
               context.CommitDataStores( "GAMExampleEntryAuthenticationType");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV35Name,(String)AV40TypeIdDsp});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV39TypeId, "Custom") == 0 )
         {
            if ( AV8AuthenticationTypeCustom.success() )
            {
               context.CommitDataStores( "GAMExampleEntryAuthenticationType");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV35Name,(String)AV40TypeIdDsp});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
            else
            {
               AV28Errors = AV8AuthenticationTypeCustom.geterrors();
            }
         }
         AV28Errors = new SdtGAMRepository(context).getlasterrors();
         AV57GXV1 = 1;
         while ( AV57GXV1 <= AV28Errors.Count )
         {
            AV27Error = ((SdtGAMError)AV28Errors.Item(AV57GXV1));
            GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV27Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
            AV57GXV1 = (int)(AV57GXV1+1);
         }
      }

      protected void E152C2( )
      {
         /* 'Close' Routine */
         context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV35Name,(String)AV40TypeIdDsp});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E162C2( )
      {
         /* 'GenerateKey' Routine */
         AV46WSPrivateEncryptKey = Crypto.GetEncryptionKey( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46WSPrivateEncryptKey", AV46WSPrivateEncryptKey);
      }

      protected void E172C2( )
      {
         /* 'GenerateKeyCustom' Routine */
         AV24CusPrivateEncryptKey = Crypto.GetEncryptionKey( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24CusPrivateEncryptKey", AV24CusPrivateEncryptKey);
      }

      protected void nextLoad( )
      {
      }

      protected void E182C2( )
      {
         /* Load Routine */
      }

      protected void wb_table9_163_2C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(600), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblbuttons_Internalname, tblTblbuttons_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:30px;width:150px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:29px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 170,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", bttBtnconfirm_Caption, bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnconfirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryAuthenticationType.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 171,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclose_Internalname, "", "Close", bttBtnclose_Jsonclick, 5, "Close", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CLOSE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_163_2C2e( true) ;
         }
         else
         {
            wb_table9_163_2C2e( false) ;
         }
      }

      protected void wb_table8_136_2C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblexternal_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(700), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblexternal_Internalname, tblTblexternal_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:25px;width:160px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbextversion_Internalname, "JSON version", "", "", lblTbextversion_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavCusversion, cmbavCusversion_Internalname, StringUtil.RTrim( AV25CusVersion), 1, cmbavCusversion_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavCusversion.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,141);\"", "", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            cmbavCusversion.CurrentValue = StringUtil.RTrim( AV25CusVersion);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCusversion_Internalname, "Values", (String)(cmbavCusversion.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:33px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbextprivenckey_Internalname, "Private encription key", "", "", lblTbextprivenckey_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCusprivateencryptkey_Internalname, StringUtil.RTrim( AV24CusPrivateEncryptKey), StringUtil.RTrim( context.localUtil.Format( AV24CusPrivateEncryptKey, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,146);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCusprivateencryptkey_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCusprivateencryptkey_Enabled, 1, "text", "", 45, "chr", 1, "row", 32, 0, 0, 0, 1, -1, 0, true, "GAMEncryptionKey", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtngenkeycustom_Internalname, "", "Generate Key", bttBtngenkeycustom_Jsonclick, 5, "Generate Key", "", StyleString, ClassString, bttBtngenkeycustom_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'GENERATEKEYCUSTOM\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbextfilename_Internalname, "File name", "", "", lblTbextfilename_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 152,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCusfilename_Internalname, StringUtil.RTrim( AV22CusFileName), StringUtil.RTrim( context.localUtil.Format( AV22CusFileName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,152);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCusfilename_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCusfilename_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbextpackage_Internalname, "Package", "", "", lblTbextpackage_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 157,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCuspackage_Internalname, StringUtil.RTrim( AV23CusPackage), StringUtil.RTrim( context.localUtil.Format( AV23CusPackage, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,157);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCuspackage_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCuspackage_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbextclassname_Internalname, "Class name", "", "", lblTbextclassname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 162,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCusclassname_Internalname, StringUtil.RTrim( AV21CusClassName), StringUtil.RTrim( context.localUtil.Format( AV21CusClassName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,162);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCusclassname_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCusclassname_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_136_2C2e( true) ;
         }
         else
         {
            wb_table8_136_2C2e( false) ;
         }
      }

      protected void wb_table7_84_2C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblwebservice_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(700), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblwebservice_Internalname, tblTblwebservice_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:25px;width:160px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbwsversion_Internalname, "Web service version", "", "", lblTbwsversion_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavWsversion, cmbavWsversion_Internalname, StringUtil.RTrim( AV53WSVersion), 1, cmbavWsversion_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavWsversion.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"", "", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            cmbavWsversion.CurrentValue = StringUtil.RTrim( AV53WSVersion);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavWsversion_Internalname, "Values", (String)(cmbavWsversion.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:33px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbwsprivenckey_Internalname, "Private encription key", "", "", lblTbwsprivenckey_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWsprivateencryptkey_Internalname, StringUtil.RTrim( AV46WSPrivateEncryptKey), StringUtil.RTrim( context.localUtil.Format( AV46WSPrivateEncryptKey, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWsprivateencryptkey_Jsonclick, 0, "Attribute", "", "", "", 1, edtavWsprivateencryptkey_Enabled, 1, "text", "", 45, "chr", 1, "row", 32, 0, 0, 0, 1, -1, 0, true, "GAMEncryptionKey", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtngenkey_Internalname, "", "Generate Key", bttBtngenkey_Jsonclick, 5, "Generate Key", "", StyleString, ClassString, bttBtngenkey_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'GENERATEKEY\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbwsservername_Internalname, "Server name", "", "", lblTbwsservername_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWsservername_Internalname, StringUtil.RTrim( AV48WSServerName), StringUtil.RTrim( context.localUtil.Format( AV48WSServerName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWsservername_Jsonclick, 0, "Attribute", "", "", "", 1, edtavWsservername_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbwsserverport_Internalname, "Server port", "", "", lblTbwsserverport_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWsserverport_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49WSServerPort), 5, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV49WSServerPort), "ZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWsserverport_Jsonclick, 0, "Attribute", "", "", "", 1, edtavWsserverport_Enabled, 1, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbwsbaseurl_Internalname, "Base URL", "", "", lblTbwsbaseurl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWsserverbaseurl_Internalname, StringUtil.RTrim( AV47WSServerBaseURL), StringUtil.RTrim( context.localUtil.Format( AV47WSServerBaseURL, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWsserverbaseurl_Jsonclick, 0, "Attribute", "", "", "", 1, edtavWsserverbaseurl_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbwssecureprotocol_Internalname, "Secure protocol", "", "", lblTbwssecureprotocol_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavWsserversecureprotocol, cmbavWsserversecureprotocol_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV50WSServerSecureProtocol), 1, 0)), 1, cmbavWsserversecureprotocol_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavWsserversecureprotocol.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", "", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            cmbavWsserversecureprotocol.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV50WSServerSecureProtocol), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavWsserversecureprotocol_Internalname, "Values", (String)(cmbavWsserversecureprotocol.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbwsversion3_Internalname, "Timeout", "", "", lblTbwsversion3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWstimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52WSTimeout), 5, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV52WSTimeout), "ZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWstimeout_Jsonclick, 0, "Attribute", "", "", "", 1, edtavWstimeout_Enabled, 1, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:25px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbwsversion4_Internalname, "Web service package", "", "", lblTbwsversion4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWspackage_Internalname, StringUtil.RTrim( AV45WSPackage), StringUtil.RTrim( context.localUtil.Format( AV45WSPackage, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWspackage_Jsonclick, 0, "Attribute", "", "", "", 1, edtavWspackage_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:26px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbwsversion5_Internalname, "Web service name", "", "", lblTbwsversion5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWsname_Internalname, StringUtil.RTrim( AV43WSName), StringUtil.RTrim( context.localUtil.Format( AV43WSName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWsname_Jsonclick, 0, "Attribute", "", "", "", 1, edtavWsname_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbwsextension_Internalname, "Web service extension", "", "", lblTbwsextension_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWsextension_Internalname, StringUtil.RTrim( AV41WSExtension), StringUtil.RTrim( context.localUtil.Format( AV41WSExtension, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWsextension_Jsonclick, 0, "Attribute", "", "", "", 1, edtavWsextension_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_84_2C2e( true) ;
         }
         else
         {
            wb_table7_84_2C2e( false) ;
         }
      }

      protected void wb_table6_73_2C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblserverhost_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(700), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblserverhost_Internalname, tblTblserverhost_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:160px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbserverurl_Internalname, "Remote server URL", "", "", lblTbserverurl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGamrserverurl_Internalname, AV31GAMRServerURL, StringUtil.RTrim( context.localUtil.Format( AV31GAMRServerURL, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGamrserverurl_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:33px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbgamrprivenckey_Internalname, "Private encription key", "", "", lblTbgamrprivenckey_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGamrprivateencryptkey_Internalname, StringUtil.RTrim( AV30GAMRPrivateEncryptKey), StringUtil.RTrim( context.localUtil.Format( AV30GAMRPrivateEncryptKey, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGamrprivateencryptkey_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 40, "chr", 1, "row", 32, 0, 0, 0, 1, -1, 0, true, "GAMEncryptionKey", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_73_2C2e( true) ;
         }
         else
         {
            wb_table6_73_2C2e( false) ;
         }
      }

      protected void wb_table5_67_2C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblcommonadditional_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(700), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblcommonadditional_Internalname, tblTblcommonadditional_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:160px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbadditionalscope2_Internalname, "Additional Scope", "", "", lblTbadditionalscope2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAdditionalscope_Internalname, AV5AdditionalScope, StringUtil.RTrim( context.localUtil.Format( AV5AdditionalScope, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAdditionalscope_Jsonclick, 0, "Attribute", "", "", "", 1, edtavAdditionalscope_Enabled, 1, "text", "", 80, "chr", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_67_2C2e( true) ;
         }
         else
         {
            wb_table5_67_2C2e( false) ;
         }
      }

      protected void wb_table4_51_2C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTbltwitter_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(700), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTbltwitter_Internalname, tblTbltwitter_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:160px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbconkey_Internalname, "Consumer key", "", "", lblTbconkey_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavConsumerkey_Internalname, StringUtil.RTrim( AV19ConsumerKey), StringUtil.RTrim( context.localUtil.Format( AV19ConsumerKey, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavConsumerkey_Jsonclick, 0, "Attribute", "", "", "", 1, edtavConsumerkey_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbconsecret_Internalname, "Consumer secret", "", "", lblTbconsecret_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavConsumersecret_Internalname, StringUtil.RTrim( AV20ConsumerSecret), StringUtil.RTrim( context.localUtil.Format( AV20ConsumerSecret, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavConsumersecret_Jsonclick, 0, "Attribute", "", "", "", 1, edtavConsumersecret_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbclisecret2_Internalname, "Callback URL", "", "", lblTbclisecret2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCallbackurl_Internalname, AV16CallbackURL, StringUtil.RTrim( context.localUtil.Format( AV16CallbackURL, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCallbackurl_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCallbackurl_Enabled, 1, "text", "", 80, "chr", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_51_2C2e( true) ;
         }
         else
         {
            wb_table4_51_2C2e( false) ;
         }
      }

      protected void wb_table3_35_2C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblfacebook_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(700), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblfacebook_Internalname, tblTblfacebook_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:160px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcliid_Internalname, "Client Id.", "", "", lblTbcliid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientid_Internalname, AV17ClientId, StringUtil.RTrim( context.localUtil.Format( AV17ClientId, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientid_Jsonclick, 0, "Attribute", "", "", "", 1, edtavClientid_Enabled, 1, "text", "", 80, "chr", 1, "row", 400, 0, 0, 0, 1, -1, 0, true, "GAMPropertyValue", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbclisecret_Internalname, "Client secret", "", "", lblTbclisecret_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientsecret_Internalname, AV18ClientSecret, StringUtil.RTrim( context.localUtil.Format( AV18ClientSecret, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientsecret_Jsonclick, 0, "Attribute", "", "", "", 1, edtavClientsecret_Enabled, 1, "text", "", 80, "chr", 1, "row", 400, 0, 0, 0, 1, -1, 0, true, "GAMPropertyValue", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbsiteurl_Internalname, "Local site URL", "", "", lblTbsiteurl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSiteurl_Internalname, AV37SiteURL, StringUtil.RTrim( context.localUtil.Format( AV37SiteURL, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSiteurl_Jsonclick, 0, "Attribute", "", "", "", 1, edtavSiteurl_Enabled, 1, "text", "", 80, "chr", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_35_2C2e( true) ;
         }
         else
         {
            wb_table3_35_2C2e( false) ;
         }
      }

      protected void wb_table2_29_2C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblimpersonate_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(700), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblimpersonate_Internalname, tblTblimpersonate_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:160px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbextimpersonate_Internalname, "Impersonate", "", "", lblTbextimpersonate_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavImpersonate_Internalname, StringUtil.RTrim( AV33Impersonate), StringUtil.RTrim( context.localUtil.Format( AV33Impersonate, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavImpersonate_Jsonclick, 0, "Attribute", "", "", "", 1, edtavImpersonate_Enabled, 1, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMAuthenticationTypeName", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_29_2C2e( true) ;
         }
         else
         {
            wb_table2_29_2C2e( false) ;
         }
      }

      protected void wb_table1_3_2C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(700), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTbldata_Internalname, tblTbldata_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:160px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbtype_Internalname, "Type", "", "", lblTbtype_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavTypeid, cmbavTypeid_Internalname, StringUtil.RTrim( AV39TypeId), 1, cmbavTypeid_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVTYPEID.CLICK."+"'", "char", "", 1, cmbavTypeid.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,8);\"", "", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            cmbavTypeid.CurrentValue = StringUtil.RTrim( AV39TypeId);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTypeid_Internalname, "Values", (String)(cmbavTypeid.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbname_Internalname, "Name", "", "", lblTbname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, StringUtil.RTrim( AV35Name), StringUtil.RTrim( context.localUtil.Format( AV35Name, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "Attribute", "", "", "", 1, edtavName_Enabled, 1, "text", "", 80, "chr", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbfunction_Internalname, "Function", "", "", lblTbfunction_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFunctionid, cmbavFunctionid_Internalname, StringUtil.RTrim( AV29FunctionId), 1, cmbavFunctionid_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavFunctionid.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            cmbavFunctionid.CurrentValue = StringUtil.RTrim( AV29FunctionId);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Values", (String)(cmbavFunctionid.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbenabled_Internalname, "Enabled ?", "", "", lblTbenabled_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavIsenable_Internalname, StringUtil.BoolToStr( AV34IsEnable), "", "", 1, chkavIsenable.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(23, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdsc_Internalname, "Description", "", "", lblTbdsc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDsc_Internalname, StringUtil.RTrim( AV26Dsc), StringUtil.RTrim( context.localUtil.Format( AV26Dsc, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDsc_Jsonclick, 0, "Attribute", "", "", "", 1, edtavDsc_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleEntryAuthenticationType.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_2C2e( true) ;
         }
         else
         {
            wb_table1_3_2C2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         Gx_mode = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         AV35Name = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Name", AV35Name);
         AV40TypeIdDsp = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TypeIdDsp", AV40TypeIdDsp);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA2C2( ) ;
         WS2C2( ) ;
         WE2C2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176033");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117324113");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamexampleentryauthenticationtype.js", "?20203117324115");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbtype_Internalname = "TBTYPE";
         cmbavTypeid_Internalname = "vTYPEID";
         lblTbname_Internalname = "TBNAME";
         edtavName_Internalname = "vNAME";
         lblTbfunction_Internalname = "TBFUNCTION";
         cmbavFunctionid_Internalname = "vFUNCTIONID";
         lblTbenabled_Internalname = "TBENABLED";
         chkavIsenable_Internalname = "vISENABLE";
         lblTbdsc_Internalname = "TBDSC";
         edtavDsc_Internalname = "vDSC";
         tblTbldata_Internalname = "TBLDATA";
         lblTbextimpersonate_Internalname = "TBEXTIMPERSONATE";
         edtavImpersonate_Internalname = "vIMPERSONATE";
         tblTblimpersonate_Internalname = "TBLIMPERSONATE";
         lblTbcliid_Internalname = "TBCLIID";
         edtavClientid_Internalname = "vCLIENTID";
         lblTbclisecret_Internalname = "TBCLISECRET";
         edtavClientsecret_Internalname = "vCLIENTSECRET";
         lblTbsiteurl_Internalname = "TBSITEURL";
         edtavSiteurl_Internalname = "vSITEURL";
         tblTblfacebook_Internalname = "TBLFACEBOOK";
         lblTbconkey_Internalname = "TBCONKEY";
         edtavConsumerkey_Internalname = "vCONSUMERKEY";
         lblTbconsecret_Internalname = "TBCONSECRET";
         edtavConsumersecret_Internalname = "vCONSUMERSECRET";
         lblTbclisecret2_Internalname = "TBCLISECRET2";
         edtavCallbackurl_Internalname = "vCALLBACKURL";
         tblTbltwitter_Internalname = "TBLTWITTER";
         lblTbadditionalscope2_Internalname = "TBADDITIONALSCOPE2";
         edtavAdditionalscope_Internalname = "vADDITIONALSCOPE";
         tblTblcommonadditional_Internalname = "TBLCOMMONADDITIONAL";
         lblTbserverurl_Internalname = "TBSERVERURL";
         edtavGamrserverurl_Internalname = "vGAMRSERVERURL";
         lblTbgamrprivenckey_Internalname = "TBGAMRPRIVENCKEY";
         edtavGamrprivateencryptkey_Internalname = "vGAMRPRIVATEENCRYPTKEY";
         tblTblserverhost_Internalname = "TBLSERVERHOST";
         lblTbwsversion_Internalname = "TBWSVERSION";
         cmbavWsversion_Internalname = "vWSVERSION";
         lblTbwsprivenckey_Internalname = "TBWSPRIVENCKEY";
         edtavWsprivateencryptkey_Internalname = "vWSPRIVATEENCRYPTKEY";
         bttBtngenkey_Internalname = "BTNGENKEY";
         lblTbwsservername_Internalname = "TBWSSERVERNAME";
         edtavWsservername_Internalname = "vWSSERVERNAME";
         lblTbwsserverport_Internalname = "TBWSSERVERPORT";
         edtavWsserverport_Internalname = "vWSSERVERPORT";
         lblTbwsbaseurl_Internalname = "TBWSBASEURL";
         edtavWsserverbaseurl_Internalname = "vWSSERVERBASEURL";
         lblTbwssecureprotocol_Internalname = "TBWSSECUREPROTOCOL";
         cmbavWsserversecureprotocol_Internalname = "vWSSERVERSECUREPROTOCOL";
         lblTbwsversion3_Internalname = "TBWSVERSION3";
         edtavWstimeout_Internalname = "vWSTIMEOUT";
         lblTbwsversion4_Internalname = "TBWSVERSION4";
         edtavWspackage_Internalname = "vWSPACKAGE";
         lblTbwsversion5_Internalname = "TBWSVERSION5";
         edtavWsname_Internalname = "vWSNAME";
         lblTbwsextension_Internalname = "TBWSEXTENSION";
         edtavWsextension_Internalname = "vWSEXTENSION";
         tblTblwebservice_Internalname = "TBLWEBSERVICE";
         lblTbextversion_Internalname = "TBEXTVERSION";
         cmbavCusversion_Internalname = "vCUSVERSION";
         lblTbextprivenckey_Internalname = "TBEXTPRIVENCKEY";
         edtavCusprivateencryptkey_Internalname = "vCUSPRIVATEENCRYPTKEY";
         bttBtngenkeycustom_Internalname = "BTNGENKEYCUSTOM";
         lblTbextfilename_Internalname = "TBEXTFILENAME";
         edtavCusfilename_Internalname = "vCUSFILENAME";
         lblTbextpackage_Internalname = "TBEXTPACKAGE";
         edtavCuspackage_Internalname = "vCUSPACKAGE";
         lblTbextclassname_Internalname = "TBEXTCLASSNAME";
         edtavCusclassname_Internalname = "vCUSCLASSNAME";
         tblTblexternal_Internalname = "TBLEXTERNAL";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         bttBtnclose_Internalname = "BTNCLOSE";
         tblTblbuttons_Internalname = "TBLBUTTONS";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         edtavDsc_Jsonclick = "";
         cmbavFunctionid_Jsonclick = "";
         edtavName_Jsonclick = "";
         cmbavTypeid_Jsonclick = "";
         edtavImpersonate_Jsonclick = "";
         edtavSiteurl_Jsonclick = "";
         edtavClientsecret_Jsonclick = "";
         edtavClientid_Jsonclick = "";
         edtavCallbackurl_Jsonclick = "";
         edtavConsumersecret_Jsonclick = "";
         edtavConsumerkey_Jsonclick = "";
         edtavAdditionalscope_Jsonclick = "";
         edtavGamrprivateencryptkey_Jsonclick = "";
         edtavGamrserverurl_Jsonclick = "";
         edtavWsextension_Jsonclick = "";
         edtavWsname_Jsonclick = "";
         edtavWspackage_Jsonclick = "";
         edtavWstimeout_Jsonclick = "";
         cmbavWsserversecureprotocol_Jsonclick = "";
         edtavWsserverbaseurl_Jsonclick = "";
         edtavWsserverport_Jsonclick = "";
         edtavWsservername_Jsonclick = "";
         bttBtngenkey_Visible = 1;
         edtavWsprivateencryptkey_Jsonclick = "";
         cmbavWsversion_Jsonclick = "";
         edtavCusclassname_Jsonclick = "";
         edtavCuspackage_Jsonclick = "";
         edtavCusfilename_Jsonclick = "";
         bttBtngenkeycustom_Visible = 1;
         edtavCusprivateencryptkey_Jsonclick = "";
         cmbavCusversion_Jsonclick = "";
         bttBtnconfirm_Visible = 1;
         tblTblexternal_Visible = 1;
         tblTblwebservice_Visible = 1;
         tblTbltwitter_Visible = 1;
         tblTblserverhost_Visible = 1;
         tblTblcommonadditional_Visible = 1;
         tblTblfacebook_Visible = 1;
         tblTblimpersonate_Visible = 1;
         bttBtnconfirm_Caption = "Confirmar";
         edtavCusclassname_Enabled = 1;
         edtavCuspackage_Enabled = 1;
         edtavCusfilename_Enabled = 1;
         edtavCusprivateencryptkey_Enabled = 1;
         cmbavCusversion.Enabled = 1;
         edtavCallbackurl_Enabled = 1;
         edtavConsumersecret_Enabled = 1;
         edtavConsumerkey_Enabled = 1;
         edtavAdditionalscope_Enabled = 1;
         edtavSiteurl_Enabled = 1;
         edtavClientsecret_Enabled = 1;
         edtavClientid_Enabled = 1;
         edtavWsextension_Enabled = 1;
         edtavWsname_Enabled = 1;
         edtavWspackage_Enabled = 1;
         edtavWstimeout_Enabled = 1;
         cmbavWsserversecureprotocol.Enabled = 1;
         edtavWsserverbaseurl_Enabled = 1;
         edtavWsserverport_Enabled = 1;
         edtavWsservername_Enabled = 1;
         edtavWsprivateencryptkey_Enabled = 1;
         cmbavWsversion.Enabled = 1;
         edtavImpersonate_Enabled = 1;
         edtavDsc_Enabled = 1;
         chkavIsenable.Enabled = 1;
         cmbavFunctionid.Enabled = 1;
         edtavName_Enabled = 0;
         cmbavTypeid.Enabled = 1;
         chkavIsenable.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Authentication type";
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOGx_mode = "";
         wcpOAV35Name = "";
         wcpOAV40TypeIdDsp = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV39TypeId = "";
         AV29FunctionId = "";
         AV53WSVersion = "";
         AV50WSServerSecureProtocol = 0;
         AV25CusVersion = "";
         AV26Dsc = "";
         AV33Impersonate = "";
         AV17ClientId = "";
         AV18ClientSecret = "";
         AV37SiteURL = "";
         AV19ConsumerKey = "";
         AV20ConsumerSecret = "";
         AV16CallbackURL = "";
         AV5AdditionalScope = "";
         AV31GAMRServerURL = "";
         AV30GAMRPrivateEncryptKey = "";
         AV46WSPrivateEncryptKey = "";
         AV48WSServerName = "";
         AV47WSServerBaseURL = "";
         AV45WSPackage = "";
         AV43WSName = "";
         AV41WSExtension = "";
         AV24CusPrivateEncryptKey = "";
         AV22CusFileName = "";
         AV23CusPackage = "";
         AV21CusClassName = "";
         AV12AuthenticationTypeLocal = new SdtGAMAuthenticationTypeLocal(context);
         AV9AuthenticationTypeFacebook = new SdtGAMAuthenticationTypeFacebook(context);
         AV11AuthenticationTypeGoogle = new SdtGAMAuthenticationTypeGoogle(context);
         AV10AuthenticationTypeGAMRemote = new SdtGAMAuthenticationTypeGAMRemote(context);
         AV14AuthenticationTypeTwitter = new SdtGAMAuthenticationTypeTwitter(context);
         AV15AuthenticationTypeWebService = new SdtGAMAuthenticationTypeWebService(context);
         AV8AuthenticationTypeCustom = new SdtGAMAuthenticationTypeCustom(context);
         AV28Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV27Error = new SdtGAMError(context);
         sStyleString = "";
         TempTags = "";
         bttBtnconfirm_Jsonclick = "";
         bttBtnclose_Jsonclick = "";
         lblTbextversion_Jsonclick = "";
         lblTbextprivenckey_Jsonclick = "";
         bttBtngenkeycustom_Jsonclick = "";
         lblTbextfilename_Jsonclick = "";
         lblTbextpackage_Jsonclick = "";
         lblTbextclassname_Jsonclick = "";
         lblTbwsversion_Jsonclick = "";
         lblTbwsprivenckey_Jsonclick = "";
         bttBtngenkey_Jsonclick = "";
         lblTbwsservername_Jsonclick = "";
         lblTbwsserverport_Jsonclick = "";
         lblTbwsbaseurl_Jsonclick = "";
         lblTbwssecureprotocol_Jsonclick = "";
         lblTbwsversion3_Jsonclick = "";
         lblTbwsversion4_Jsonclick = "";
         lblTbwsversion5_Jsonclick = "";
         lblTbwsextension_Jsonclick = "";
         lblTbserverurl_Jsonclick = "";
         lblTbgamrprivenckey_Jsonclick = "";
         lblTbadditionalscope2_Jsonclick = "";
         lblTbconkey_Jsonclick = "";
         lblTbconsecret_Jsonclick = "";
         lblTbclisecret2_Jsonclick = "";
         lblTbcliid_Jsonclick = "";
         lblTbclisecret_Jsonclick = "";
         lblTbsiteurl_Jsonclick = "";
         lblTbextimpersonate_Jsonclick = "";
         lblTbtype_Jsonclick = "";
         lblTbname_Jsonclick = "";
         lblTbfunction_Jsonclick = "";
         lblTbenabled_Jsonclick = "";
         lblTbdsc_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexampleentryauthenticationtype__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV50WSServerSecureProtocol ;
      private short nGXWrapped ;
      private int AV49WSServerPort ;
      private int AV52WSTimeout ;
      private int edtavName_Enabled ;
      private int bttBtnconfirm_Visible ;
      private int bttBtngenkey_Visible ;
      private int bttBtngenkeycustom_Visible ;
      private int edtavDsc_Enabled ;
      private int edtavImpersonate_Enabled ;
      private int edtavWsprivateencryptkey_Enabled ;
      private int edtavWsservername_Enabled ;
      private int edtavWsserverport_Enabled ;
      private int edtavWsserverbaseurl_Enabled ;
      private int edtavWstimeout_Enabled ;
      private int edtavWspackage_Enabled ;
      private int edtavWsname_Enabled ;
      private int edtavWsextension_Enabled ;
      private int edtavClientid_Enabled ;
      private int edtavClientsecret_Enabled ;
      private int edtavSiteurl_Enabled ;
      private int edtavAdditionalscope_Enabled ;
      private int edtavConsumerkey_Enabled ;
      private int edtavConsumersecret_Enabled ;
      private int edtavCallbackurl_Enabled ;
      private int edtavCusprivateencryptkey_Enabled ;
      private int edtavCusfilename_Enabled ;
      private int edtavCuspackage_Enabled ;
      private int edtavCusclassname_Enabled ;
      private int tblTblimpersonate_Visible ;
      private int tblTblfacebook_Visible ;
      private int tblTblcommonadditional_Visible ;
      private int tblTblserverhost_Visible ;
      private int tblTbltwitter_Visible ;
      private int tblTblwebservice_Visible ;
      private int tblTblexternal_Visible ;
      private int AV57GXV1 ;
      private int idxLst ;
      private String Gx_mode ;
      private String AV35Name ;
      private String AV40TypeIdDsp ;
      private String wcpOGx_mode ;
      private String wcpOAV35Name ;
      private String wcpOAV40TypeIdDsp ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV39TypeId ;
      private String AV29FunctionId ;
      private String chkavIsenable_Internalname ;
      private String AV53WSVersion ;
      private String AV25CusVersion ;
      private String cmbavTypeid_Internalname ;
      private String edtavName_Internalname ;
      private String cmbavFunctionid_Internalname ;
      private String AV26Dsc ;
      private String edtavDsc_Internalname ;
      private String AV33Impersonate ;
      private String edtavImpersonate_Internalname ;
      private String edtavClientid_Internalname ;
      private String edtavClientsecret_Internalname ;
      private String edtavSiteurl_Internalname ;
      private String AV19ConsumerKey ;
      private String edtavConsumerkey_Internalname ;
      private String AV20ConsumerSecret ;
      private String edtavConsumersecret_Internalname ;
      private String edtavCallbackurl_Internalname ;
      private String edtavAdditionalscope_Internalname ;
      private String edtavGamrserverurl_Internalname ;
      private String AV30GAMRPrivateEncryptKey ;
      private String edtavGamrprivateencryptkey_Internalname ;
      private String cmbavWsversion_Internalname ;
      private String AV46WSPrivateEncryptKey ;
      private String edtavWsprivateencryptkey_Internalname ;
      private String AV48WSServerName ;
      private String edtavWsservername_Internalname ;
      private String edtavWsserverport_Internalname ;
      private String AV47WSServerBaseURL ;
      private String edtavWsserverbaseurl_Internalname ;
      private String cmbavWsserversecureprotocol_Internalname ;
      private String edtavWstimeout_Internalname ;
      private String AV45WSPackage ;
      private String edtavWspackage_Internalname ;
      private String AV43WSName ;
      private String edtavWsname_Internalname ;
      private String AV41WSExtension ;
      private String edtavWsextension_Internalname ;
      private String cmbavCusversion_Internalname ;
      private String AV24CusPrivateEncryptKey ;
      private String edtavCusprivateencryptkey_Internalname ;
      private String AV22CusFileName ;
      private String edtavCusfilename_Internalname ;
      private String AV23CusPackage ;
      private String edtavCuspackage_Internalname ;
      private String AV21CusClassName ;
      private String edtavCusclassname_Internalname ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtngenkey_Internalname ;
      private String bttBtngenkeycustom_Internalname ;
      private String bttBtnconfirm_Caption ;
      private String tblTblimpersonate_Internalname ;
      private String tblTblfacebook_Internalname ;
      private String tblTblcommonadditional_Internalname ;
      private String tblTblserverhost_Internalname ;
      private String tblTbltwitter_Internalname ;
      private String tblTblwebservice_Internalname ;
      private String tblTblexternal_Internalname ;
      private String sStyleString ;
      private String tblTblbuttons_Internalname ;
      private String TempTags ;
      private String bttBtnconfirm_Jsonclick ;
      private String bttBtnclose_Internalname ;
      private String bttBtnclose_Jsonclick ;
      private String lblTbextversion_Internalname ;
      private String lblTbextversion_Jsonclick ;
      private String cmbavCusversion_Jsonclick ;
      private String lblTbextprivenckey_Internalname ;
      private String lblTbextprivenckey_Jsonclick ;
      private String edtavCusprivateencryptkey_Jsonclick ;
      private String bttBtngenkeycustom_Jsonclick ;
      private String lblTbextfilename_Internalname ;
      private String lblTbextfilename_Jsonclick ;
      private String edtavCusfilename_Jsonclick ;
      private String lblTbextpackage_Internalname ;
      private String lblTbextpackage_Jsonclick ;
      private String edtavCuspackage_Jsonclick ;
      private String lblTbextclassname_Internalname ;
      private String lblTbextclassname_Jsonclick ;
      private String edtavCusclassname_Jsonclick ;
      private String lblTbwsversion_Internalname ;
      private String lblTbwsversion_Jsonclick ;
      private String cmbavWsversion_Jsonclick ;
      private String lblTbwsprivenckey_Internalname ;
      private String lblTbwsprivenckey_Jsonclick ;
      private String edtavWsprivateencryptkey_Jsonclick ;
      private String bttBtngenkey_Jsonclick ;
      private String lblTbwsservername_Internalname ;
      private String lblTbwsservername_Jsonclick ;
      private String edtavWsservername_Jsonclick ;
      private String lblTbwsserverport_Internalname ;
      private String lblTbwsserverport_Jsonclick ;
      private String edtavWsserverport_Jsonclick ;
      private String lblTbwsbaseurl_Internalname ;
      private String lblTbwsbaseurl_Jsonclick ;
      private String edtavWsserverbaseurl_Jsonclick ;
      private String lblTbwssecureprotocol_Internalname ;
      private String lblTbwssecureprotocol_Jsonclick ;
      private String cmbavWsserversecureprotocol_Jsonclick ;
      private String lblTbwsversion3_Internalname ;
      private String lblTbwsversion3_Jsonclick ;
      private String edtavWstimeout_Jsonclick ;
      private String lblTbwsversion4_Internalname ;
      private String lblTbwsversion4_Jsonclick ;
      private String edtavWspackage_Jsonclick ;
      private String lblTbwsversion5_Internalname ;
      private String lblTbwsversion5_Jsonclick ;
      private String edtavWsname_Jsonclick ;
      private String lblTbwsextension_Internalname ;
      private String lblTbwsextension_Jsonclick ;
      private String edtavWsextension_Jsonclick ;
      private String lblTbserverurl_Internalname ;
      private String lblTbserverurl_Jsonclick ;
      private String edtavGamrserverurl_Jsonclick ;
      private String lblTbgamrprivenckey_Internalname ;
      private String lblTbgamrprivenckey_Jsonclick ;
      private String edtavGamrprivateencryptkey_Jsonclick ;
      private String lblTbadditionalscope2_Internalname ;
      private String lblTbadditionalscope2_Jsonclick ;
      private String edtavAdditionalscope_Jsonclick ;
      private String lblTbconkey_Internalname ;
      private String lblTbconkey_Jsonclick ;
      private String edtavConsumerkey_Jsonclick ;
      private String lblTbconsecret_Internalname ;
      private String lblTbconsecret_Jsonclick ;
      private String edtavConsumersecret_Jsonclick ;
      private String lblTbclisecret2_Internalname ;
      private String lblTbclisecret2_Jsonclick ;
      private String edtavCallbackurl_Jsonclick ;
      private String lblTbcliid_Internalname ;
      private String lblTbcliid_Jsonclick ;
      private String edtavClientid_Jsonclick ;
      private String lblTbclisecret_Internalname ;
      private String lblTbclisecret_Jsonclick ;
      private String edtavClientsecret_Jsonclick ;
      private String lblTbsiteurl_Internalname ;
      private String lblTbsiteurl_Jsonclick ;
      private String edtavSiteurl_Jsonclick ;
      private String lblTbextimpersonate_Internalname ;
      private String lblTbextimpersonate_Jsonclick ;
      private String edtavImpersonate_Jsonclick ;
      private String tblTbldata_Internalname ;
      private String lblTbtype_Internalname ;
      private String lblTbtype_Jsonclick ;
      private String cmbavTypeid_Jsonclick ;
      private String lblTbname_Internalname ;
      private String lblTbname_Jsonclick ;
      private String edtavName_Jsonclick ;
      private String lblTbfunction_Internalname ;
      private String lblTbfunction_Jsonclick ;
      private String cmbavFunctionid_Jsonclick ;
      private String lblTbenabled_Internalname ;
      private String lblTbenabled_Jsonclick ;
      private String lblTbdsc_Internalname ;
      private String lblTbdsc_Jsonclick ;
      private String edtavDsc_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV34IsEnable ;
      private bool returnInSub ;
      private String AV17ClientId ;
      private String AV18ClientSecret ;
      private String AV37SiteURL ;
      private String AV16CallbackURL ;
      private String AV5AdditionalScope ;
      private String AV31GAMRServerURL ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Gx_mode ;
      private String aP1_Name ;
      private String aP2_TypeIdDsp ;
      private GXCombobox cmbavTypeid ;
      private GXCombobox cmbavFunctionid ;
      private GXCheckbox chkavIsenable ;
      private GXCombobox cmbavWsversion ;
      private GXCombobox cmbavWsserversecureprotocol ;
      private GXCombobox cmbavCusversion ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV28Errors ;
      private GXWebForm Form ;
      private SdtGAMError AV27Error ;
      private SdtGAMAuthenticationTypeCustom AV8AuthenticationTypeCustom ;
      private SdtGAMAuthenticationTypeFacebook AV9AuthenticationTypeFacebook ;
      private SdtGAMAuthenticationTypeGAMRemote AV10AuthenticationTypeGAMRemote ;
      private SdtGAMAuthenticationTypeGoogle AV11AuthenticationTypeGoogle ;
      private SdtGAMAuthenticationTypeLocal AV12AuthenticationTypeLocal ;
      private SdtGAMAuthenticationTypeTwitter AV14AuthenticationTypeTwitter ;
      private SdtGAMAuthenticationTypeWebService AV15AuthenticationTypeWebService ;
   }

   public class gamexampleentryauthenticationtype__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
