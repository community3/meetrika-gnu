/*
               File: PRC_UPDContagemResultadoContagens
        Description: Update Contagem Resultado Contagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:14.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updcontagemresultadocontagens : GXProcedure
   {
      public prc_updcontagemresultadocontagens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updcontagemresultadocontagens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           decimal aP1_ContagemResultado_PFBFS ,
                           decimal aP2_ContagemResultado_PFLFS ,
                           decimal aP3_ContagemResultado_PFBFM ,
                           decimal aP4_ContagemResultado_PFLFM ,
                           short aP5_ContagemResultado_StatusCnt ,
                           short aP6_ContagemResultadoContagens_Esforco ,
                           String aP7_ContagemResultado_ParecerTcn ,
                           int aP8_ContagemResultado_NaoCnfCntCod ,
                           String aP9_ContagemResultado_StatusDmn )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8ContagemResultado_PFBFS = aP1_ContagemResultado_PFBFS;
         this.AV9ContagemResultado_PFLFS = aP2_ContagemResultado_PFLFS;
         this.AV10ContagemResultado_PFBFM = aP3_ContagemResultado_PFBFM;
         this.AV11ContagemResultado_PFLFM = aP4_ContagemResultado_PFLFM;
         this.AV17ContagemResultado_StatusCnt = aP5_ContagemResultado_StatusCnt;
         this.AV15ContagemResultadoContagens_Esforco = aP6_ContagemResultadoContagens_Esforco;
         this.AV14ContagemResultado_ParecerTcn = aP7_ContagemResultado_ParecerTcn;
         this.AV16ContagemResultado_NaoCnfCntCod = aP8_ContagemResultado_NaoCnfCntCod;
         this.AV18ContagemResultado_StatusDmn = aP9_ContagemResultado_StatusDmn;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 decimal aP1_ContagemResultado_PFBFS ,
                                 decimal aP2_ContagemResultado_PFLFS ,
                                 decimal aP3_ContagemResultado_PFBFM ,
                                 decimal aP4_ContagemResultado_PFLFM ,
                                 short aP5_ContagemResultado_StatusCnt ,
                                 short aP6_ContagemResultadoContagens_Esforco ,
                                 String aP7_ContagemResultado_ParecerTcn ,
                                 int aP8_ContagemResultado_NaoCnfCntCod ,
                                 String aP9_ContagemResultado_StatusDmn )
      {
         prc_updcontagemresultadocontagens objprc_updcontagemresultadocontagens;
         objprc_updcontagemresultadocontagens = new prc_updcontagemresultadocontagens();
         objprc_updcontagemresultadocontagens.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_updcontagemresultadocontagens.AV8ContagemResultado_PFBFS = aP1_ContagemResultado_PFBFS;
         objprc_updcontagemresultadocontagens.AV9ContagemResultado_PFLFS = aP2_ContagemResultado_PFLFS;
         objprc_updcontagemresultadocontagens.AV10ContagemResultado_PFBFM = aP3_ContagemResultado_PFBFM;
         objprc_updcontagemresultadocontagens.AV11ContagemResultado_PFLFM = aP4_ContagemResultado_PFLFM;
         objprc_updcontagemresultadocontagens.AV17ContagemResultado_StatusCnt = aP5_ContagemResultado_StatusCnt;
         objprc_updcontagemresultadocontagens.AV15ContagemResultadoContagens_Esforco = aP6_ContagemResultadoContagens_Esforco;
         objprc_updcontagemresultadocontagens.AV14ContagemResultado_ParecerTcn = aP7_ContagemResultado_ParecerTcn;
         objprc_updcontagemresultadocontagens.AV16ContagemResultado_NaoCnfCntCod = aP8_ContagemResultado_NaoCnfCntCod;
         objprc_updcontagemresultadocontagens.AV18ContagemResultado_StatusDmn = aP9_ContagemResultado_StatusDmn;
         objprc_updcontagemresultadocontagens.context.SetSubmitInitialConfig(context);
         objprc_updcontagemresultadocontagens.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updcontagemresultadocontagens);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updcontagemresultadocontagens)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P004K2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A458ContagemResultado_PFBFS = P004K2_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = P004K2_n458ContagemResultado_PFBFS[0];
            A459ContagemResultado_PFLFS = P004K2_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = P004K2_n459ContagemResultado_PFLFS[0];
            A460ContagemResultado_PFBFM = P004K2_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = P004K2_n460ContagemResultado_PFBFM[0];
            A461ContagemResultado_PFLFM = P004K2_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = P004K2_n461ContagemResultado_PFLFM[0];
            A470ContagemResultado_ContadorFMCod = P004K2_A470ContagemResultado_ContadorFMCod[0];
            A463ContagemResultado_ParecerTcn = P004K2_A463ContagemResultado_ParecerTcn[0];
            n463ContagemResultado_ParecerTcn = P004K2_n463ContagemResultado_ParecerTcn[0];
            A482ContagemResultadoContagens_Esforco = P004K2_A482ContagemResultadoContagens_Esforco[0];
            A483ContagemResultado_StatusCnt = P004K2_A483ContagemResultado_StatusCnt[0];
            A469ContagemResultado_NaoCnfCntCod = P004K2_A469ContagemResultado_NaoCnfCntCod[0];
            n469ContagemResultado_NaoCnfCntCod = P004K2_n469ContagemResultado_NaoCnfCntCod[0];
            A473ContagemResultado_DataCnt = P004K2_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = P004K2_A511ContagemResultado_HoraCnt[0];
            A458ContagemResultado_PFBFS = AV8ContagemResultado_PFBFS;
            n458ContagemResultado_PFBFS = false;
            A459ContagemResultado_PFLFS = AV9ContagemResultado_PFLFS;
            n459ContagemResultado_PFLFS = false;
            A460ContagemResultado_PFBFM = AV10ContagemResultado_PFBFM;
            n460ContagemResultado_PFBFM = false;
            A461ContagemResultado_PFLFM = AV11ContagemResultado_PFLFM;
            n461ContagemResultado_PFLFM = false;
            A470ContagemResultado_ContadorFMCod = AV13ContagemResultado_ContadorFMCod;
            A463ContagemResultado_ParecerTcn = AV14ContagemResultado_ParecerTcn;
            n463ContagemResultado_ParecerTcn = false;
            A482ContagemResultadoContagens_Esforco = AV15ContagemResultadoContagens_Esforco;
            A483ContagemResultado_StatusCnt = AV17ContagemResultado_StatusCnt;
            if ( ! (0==AV16ContagemResultado_NaoCnfCntCod) )
            {
               A469ContagemResultado_NaoCnfCntCod = AV16ContagemResultado_NaoCnfCntCod;
               n469ContagemResultado_NaoCnfCntCod = false;
            }
            /* Optimized UPDATE. */
            /* Using cursor P004K3 */
            pr_default.execute(1, new Object[] {n484ContagemResultado_StatusDmn, AV18ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* End optimized UPDATE. */
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P004K4 */
            pr_default.execute(2, new Object[] {n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A470ContagemResultado_ContadorFMCod, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A482ContagemResultadoContagens_Esforco, A483ContagemResultado_StatusCnt, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if (true) break;
            /* Using cursor P004K5 */
            pr_default.execute(3, new Object[] {n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A470ContagemResultado_ContadorFMCod, n463ContagemResultado_ParecerTcn, A463ContagemResultado_ParecerTcn, A482ContagemResultadoContagens_Esforco, A483ContagemResultado_StatusCnt, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UPDContagemResultadoContagens");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P004K2_A456ContagemResultado_Codigo = new int[1] ;
         P004K2_A458ContagemResultado_PFBFS = new decimal[1] ;
         P004K2_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P004K2_A459ContagemResultado_PFLFS = new decimal[1] ;
         P004K2_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P004K2_A460ContagemResultado_PFBFM = new decimal[1] ;
         P004K2_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P004K2_A461ContagemResultado_PFLFM = new decimal[1] ;
         P004K2_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P004K2_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P004K2_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         P004K2_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         P004K2_A482ContagemResultadoContagens_Esforco = new short[1] ;
         P004K2_A483ContagemResultado_StatusCnt = new short[1] ;
         P004K2_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         P004K2_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         P004K2_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P004K2_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A463ContagemResultado_ParecerTcn = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         A484ContagemResultado_StatusDmn = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updcontagemresultadocontagens__default(),
            new Object[][] {
                new Object[] {
               P004K2_A456ContagemResultado_Codigo, P004K2_A458ContagemResultado_PFBFS, P004K2_n458ContagemResultado_PFBFS, P004K2_A459ContagemResultado_PFLFS, P004K2_n459ContagemResultado_PFLFS, P004K2_A460ContagemResultado_PFBFM, P004K2_n460ContagemResultado_PFBFM, P004K2_A461ContagemResultado_PFLFM, P004K2_n461ContagemResultado_PFLFM, P004K2_A470ContagemResultado_ContadorFMCod,
               P004K2_A463ContagemResultado_ParecerTcn, P004K2_n463ContagemResultado_ParecerTcn, P004K2_A482ContagemResultadoContagens_Esforco, P004K2_A483ContagemResultado_StatusCnt, P004K2_A469ContagemResultado_NaoCnfCntCod, P004K2_n469ContagemResultado_NaoCnfCntCod, P004K2_A473ContagemResultado_DataCnt, P004K2_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV17ContagemResultado_StatusCnt ;
      private short AV15ContagemResultadoContagens_Esforco ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short A483ContagemResultado_StatusCnt ;
      private int A456ContagemResultado_Codigo ;
      private int AV16ContagemResultado_NaoCnfCntCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int AV13ContagemResultado_ContadorFMCod ;
      private decimal AV8ContagemResultado_PFBFS ;
      private decimal AV9ContagemResultado_PFLFS ;
      private decimal AV10ContagemResultado_PFBFM ;
      private decimal AV11ContagemResultado_PFLFM ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private String AV18ContagemResultado_StatusDmn ;
      private String scmdbuf ;
      private String A511ContagemResultado_HoraCnt ;
      private String A484ContagemResultado_StatusDmn ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private String AV14ContagemResultado_ParecerTcn ;
      private String A463ContagemResultado_ParecerTcn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P004K2_A456ContagemResultado_Codigo ;
      private decimal[] P004K2_A458ContagemResultado_PFBFS ;
      private bool[] P004K2_n458ContagemResultado_PFBFS ;
      private decimal[] P004K2_A459ContagemResultado_PFLFS ;
      private bool[] P004K2_n459ContagemResultado_PFLFS ;
      private decimal[] P004K2_A460ContagemResultado_PFBFM ;
      private bool[] P004K2_n460ContagemResultado_PFBFM ;
      private decimal[] P004K2_A461ContagemResultado_PFLFM ;
      private bool[] P004K2_n461ContagemResultado_PFLFM ;
      private int[] P004K2_A470ContagemResultado_ContadorFMCod ;
      private String[] P004K2_A463ContagemResultado_ParecerTcn ;
      private bool[] P004K2_n463ContagemResultado_ParecerTcn ;
      private short[] P004K2_A482ContagemResultadoContagens_Esforco ;
      private short[] P004K2_A483ContagemResultado_StatusCnt ;
      private int[] P004K2_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] P004K2_n469ContagemResultado_NaoCnfCntCod ;
      private DateTime[] P004K2_A473ContagemResultado_DataCnt ;
      private String[] P004K2_A511ContagemResultado_HoraCnt ;
   }

   public class prc_updcontagemresultadocontagens__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004K2 ;
          prmP004K2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004K3 ;
          prmP004K3 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004K4 ;
          prmP004K4 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP004K5 ;
          prmP004K5 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ParecerTcn",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004K2", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_ContadorFMCod], [ContagemResultado_ParecerTcn], [ContagemResultadoContagens_Esforco], [ContagemResultado_StatusCnt], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004K2,1,0,true,true )
             ,new CursorDef("P004K3", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004K3)
             ,new CursorDef("P004K4", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_PFBFM]=@ContagemResultado_PFBFM, [ContagemResultado_PFLFM]=@ContagemResultado_PFLFM, [ContagemResultado_ContadorFMCod]=@ContagemResultado_ContadorFMCod, [ContagemResultado_ParecerTcn]=@ContagemResultado_ParecerTcn, [ContagemResultadoContagens_Esforco]=@ContagemResultadoContagens_Esforco, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_NaoCnfCntCod]=@ContagemResultado_NaoCnfCntCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004K4)
             ,new CursorDef("P004K5", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_PFBFM]=@ContagemResultado_PFBFM, [ContagemResultado_PFLFM]=@ContagemResultado_PFLFM, [ContagemResultado_ContadorFMCod]=@ContagemResultado_ContadorFMCod, [ContagemResultado_ParecerTcn]=@ContagemResultado_ParecerTcn, [ContagemResultadoContagens_Esforco]=@ContagemResultadoContagens_Esforco, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_NaoCnfCntCod]=@ContagemResultado_NaoCnfCntCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004K5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((short[]) buf[12])[0] = rslt.getShort(8) ;
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(11) ;
                ((String[]) buf[17])[0] = rslt.getString(12, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                stmt.SetParameter(7, (short)parms[11]);
                stmt.SetParameter(8, (short)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[14]);
                }
                stmt.SetParameter(10, (int)parms[15]);
                stmt.SetParameter(11, (DateTime)parms[16]);
                stmt.SetParameter(12, (String)parms[17]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                stmt.SetParameter(7, (short)parms[11]);
                stmt.SetParameter(8, (short)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[14]);
                }
                stmt.SetParameter(10, (int)parms[15]);
                stmt.SetParameter(11, (DateTime)parms[16]);
                stmt.SetParameter(12, (String)parms[17]);
                return;
       }
    }

 }

}
