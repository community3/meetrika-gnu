/*
               File: WWContratoServicosPrioridade
        Description:  Prioridade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:58:34.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratoservicosprioridade : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratoservicosprioridade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratoservicosprioridade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_72 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_72_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_72_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV17ContratoServicosPrioridade_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosPrioridade_Nome1", AV17ContratoServicosPrioridade_Nome1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV21ContratoServicosPrioridade_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosPrioridade_Nome2", AV21ContratoServicosPrioridade_Nome2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV25ContratoServicosPrioridade_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosPrioridade_Nome3", AV25ContratoServicosPrioridade_Nome3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV50TFContratoServicosPrioridade_Ordem = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFContratoServicosPrioridade_Ordem), 3, 0)));
               AV51TFContratoServicosPrioridade_Ordem_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoServicosPrioridade_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFContratoServicosPrioridade_Ordem_To), 3, 0)));
               AV34TFContratoServicosPrioridade_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratoServicosPrioridade_Nome", AV34TFContratoServicosPrioridade_Nome);
               AV35TFContratoServicosPrioridade_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratoServicosPrioridade_Nome_Sel", AV35TFContratoServicosPrioridade_Nome_Sel);
               AV38TFContratoServicosPrioridade_PercValorB = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.Str( AV38TFContratoServicosPrioridade_PercValorB, 6, 2)));
               AV39TFContratoServicosPrioridade_PercValorB_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosPrioridade_PercValorB_To", StringUtil.LTrim( StringUtil.Str( AV39TFContratoServicosPrioridade_PercValorB_To, 6, 2)));
               AV42TFContratoServicosPrioridade_PercPrazo = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.Str( AV42TFContratoServicosPrioridade_PercPrazo, 6, 2)));
               AV43TFContratoServicosPrioridade_PercPrazo_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratoServicosPrioridade_PercPrazo_To", StringUtil.LTrim( StringUtil.Str( AV43TFContratoServicosPrioridade_PercPrazo_To, 6, 2)));
               AV54TFContratoServicosPrioridade_Peso = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFContratoServicosPrioridade_Peso), 4, 0)));
               AV55TFContratoServicosPrioridade_Peso_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoServicosPrioridade_Peso_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFContratoServicosPrioridade_Peso_To), 4, 0)));
               AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace", AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace);
               AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace", AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace);
               AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace", AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace);
               AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace", AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace);
               AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace", AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace);
               AV79Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A1336ContratoServicosPrioridade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratoServicosPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicosPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ContratoServicosPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFContratoServicosPrioridade_Ordem, AV51TFContratoServicosPrioridade_Ordem_To, AV34TFContratoServicosPrioridade_Nome, AV35TFContratoServicosPrioridade_Nome_Sel, AV38TFContratoServicosPrioridade_PercValorB, AV39TFContratoServicosPrioridade_PercValorB_To, AV42TFContratoServicosPrioridade_PercPrazo, AV43TFContratoServicosPrioridade_PercPrazo_To, AV54TFContratoServicosPrioridade_Peso, AV55TFContratoServicosPrioridade_Peso_To, AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace, AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace, AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace, AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace, AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1336ContratoServicosPrioridade_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAK12( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTK12( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203118583493");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratoservicosprioridade.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSPRIORIDADE_NOME1", StringUtil.RTrim( AV17ContratoServicosPrioridade_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSPRIORIDADE_NOME2", StringUtil.RTrim( AV21ContratoServicosPrioridade_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSPRIORIDADE_NOME3", StringUtil.RTrim( AV25ContratoServicosPrioridade_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRIORIDADE_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50TFContratoServicosPrioridade_Ordem), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFContratoServicosPrioridade_Ordem_To), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRIORIDADE_NOME", StringUtil.RTrim( AV34TFContratoServicosPrioridade_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL", StringUtil.RTrim( AV35TFContratoServicosPrioridade_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB", StringUtil.LTrim( StringUtil.NToC( AV38TFContratoServicosPrioridade_PercValorB, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO", StringUtil.LTrim( StringUtil.NToC( AV39TFContratoServicosPrioridade_PercValorB_To, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO", StringUtil.LTrim( StringUtil.NToC( AV42TFContratoServicosPrioridade_PercPrazo, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO", StringUtil.LTrim( StringUtil.NToC( AV43TFContratoServicosPrioridade_PercPrazo_To, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PESO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54TFContratoServicosPrioridade_Peso), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFContratoServicosPrioridade_Peso_To), 4, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_72", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_72), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV45DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV45DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSPRIORIDADE_ORDEMTITLEFILTERDATA", AV49ContratoServicosPrioridade_OrdemTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSPRIORIDADE_ORDEMTITLEFILTERDATA", AV49ContratoServicosPrioridade_OrdemTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSPRIORIDADE_NOMETITLEFILTERDATA", AV33ContratoServicosPrioridade_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSPRIORIDADE_NOMETITLEFILTERDATA", AV33ContratoServicosPrioridade_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLEFILTERDATA", AV37ContratoServicosPrioridade_PercValorBTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLEFILTERDATA", AV37ContratoServicosPrioridade_PercValorBTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLEFILTERDATA", AV41ContratoServicosPrioridade_PercPrazoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLEFILTERDATA", AV41ContratoServicosPrioridade_PercPrazoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSPRIORIDADE_PESOTITLEFILTERDATA", AV53ContratoServicosPrioridade_PesoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSPRIORIDADE_PESOTITLEFILTERDATA", AV53ContratoServicosPrioridade_PesoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV79Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Caption", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Cls", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_ordem_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_ordem_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_ordem_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_ordem_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_ordem_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Caption", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Cls", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Datalisttype", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Datalistproc", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicosprioridade_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Loadingdata", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Caption", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Cls", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_percvalorb_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_percvalorb_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_percvalorb_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_percvalorb_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_percvalorb_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Caption", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Cls", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_percprazo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_percprazo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_percprazo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_percprazo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_percprazo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Caption", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Cls", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_peso_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_peso_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_peso_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_peso_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprioridade_peso_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprioridade_ordem_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicosprioridade_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprioridade_percprazo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprioridade_peso_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEK12( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTK12( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratoservicosprioridade.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratoServicosPrioridade" ;
      }

      public override String GetPgmdesc( )
      {
         return " Prioridade" ;
      }

      protected void WBK10( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_K12( true) ;
         }
         else
         {
            wb_table1_2_K12( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_K12e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_72_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(86, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_72_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(87, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,87);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprioridade_ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50TFContratoServicosPrioridade_Ordem), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV50TFContratoServicosPrioridade_Ordem), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprioridade_ordem_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprioridade_ordem_Visible, 1, 0, "text", "", 40, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosPrioridade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprioridade_ordem_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFContratoServicosPrioridade_Ordem_To), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV51TFContratoServicosPrioridade_Ordem_To), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprioridade_ordem_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprioridade_ordem_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosPrioridade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprioridade_nome_Internalname, StringUtil.RTrim( AV34TFContratoServicosPrioridade_Nome), StringUtil.RTrim( context.localUtil.Format( AV34TFContratoServicosPrioridade_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprioridade_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprioridade_nome_Visible, 1, 0, "text", "", 0, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosPrioridade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprioridade_nome_sel_Internalname, StringUtil.RTrim( AV35TFContratoServicosPrioridade_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV35TFContratoServicosPrioridade_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprioridade_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprioridade_nome_sel_Visible, 1, 0, "text", "", 0, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosPrioridade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprioridade_percvalorb_Internalname, StringUtil.LTrim( StringUtil.NToC( AV38TFContratoServicosPrioridade_PercValorB, 8, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV38TFContratoServicosPrioridade_PercValorB, "ZZ9.99 %")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprioridade_percvalorb_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprioridade_percvalorb_Visible, 1, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosPrioridade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprioridade_percvalorb_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV39TFContratoServicosPrioridade_PercValorB_To, 8, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV39TFContratoServicosPrioridade_PercValorB_To, "ZZ9.99 %")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprioridade_percvalorb_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprioridade_percvalorb_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosPrioridade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprioridade_percprazo_Internalname, StringUtil.LTrim( StringUtil.NToC( AV42TFContratoServicosPrioridade_PercPrazo, 8, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV42TFContratoServicosPrioridade_PercPrazo, "ZZ9.99 %")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprioridade_percprazo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprioridade_percprazo_Visible, 1, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosPrioridade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprioridade_percprazo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV43TFContratoServicosPrioridade_PercPrazo_To, 8, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV43TFContratoServicosPrioridade_PercPrazo_To, "ZZ9.99 %")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprioridade_percprazo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprioridade_percprazo_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosPrioridade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprioridade_peso_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54TFContratoServicosPrioridade_Peso), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV54TFContratoServicosPrioridade_Peso), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprioridade_peso_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprioridade_peso_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosPrioridade.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprioridade_peso_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFContratoServicosPrioridade_Peso_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55TFContratoServicosPrioridade_Peso_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprioridade_peso_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprioridade_peso_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosPrioridade.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSPRIORIDADE_ORDEMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_72_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprioridade_ordemtitlecontrolidtoreplace_Internalname, AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", 0, edtavDdo_contratoservicosprioridade_ordemtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosPrioridade.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSPRIORIDADE_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_72_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprioridade_nometitlecontrolidtoreplace_Internalname, AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", 0, edtavDdo_contratoservicosprioridade_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosPrioridade.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_72_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprioridade_percvalorbtitlecontrolidtoreplace_Internalname, AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", 0, edtavDdo_contratoservicosprioridade_percvalorbtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosPrioridade.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_72_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprioridade_percprazotitlecontrolidtoreplace_Internalname, AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", 0, edtavDdo_contratoservicosprioridade_percprazotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosPrioridade.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSPRIORIDADE_PESOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_72_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprioridade_pesotitlecontrolidtoreplace_Internalname, AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", 0, edtavDdo_contratoservicosprioridade_pesotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoServicosPrioridade.htm");
         }
         wbLoad = true;
      }

      protected void STARTK12( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Prioridade", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPK10( ) ;
      }

      protected void WSK12( )
      {
         STARTK12( ) ;
         EVTK12( ) ;
      }

      protected void EVTK12( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11K12 */
                              E11K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12K12 */
                              E12K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRIORIDADE_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13K12 */
                              E13K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14K12 */
                              E14K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15K12 */
                              E15K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRIORIDADE_PESO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16K12 */
                              E16K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17K12 */
                              E17K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18K12 */
                              E18K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19K12 */
                              E19K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20K12 */
                              E20K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21K12 */
                              E21K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22K12 */
                              E22K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23K12 */
                              E23K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24K12 */
                              E24K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25K12 */
                              E25K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26K12 */
                              E26K12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_72_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_72_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_72_idx), 4, 0)), 4, "0");
                              SubsflControlProps_722( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV77Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV78Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A1336ContratoServicosPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_Codigo_Internalname), ",", "."));
                              A1335ContratoServicosPrioridade_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_CntSrvCod_Internalname), ",", "."));
                              A2066ContratoServicosPrioridade_Ordem = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_Ordem_Internalname), ",", "."));
                              n2066ContratoServicosPrioridade_Ordem = false;
                              A1337ContratoServicosPrioridade_Nome = StringUtil.Upper( cgiGet( edtContratoServicosPrioridade_Nome_Internalname));
                              A1338ContratoServicosPrioridade_PercValorB = context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_PercValorB_Internalname), ",", ".");
                              n1338ContratoServicosPrioridade_PercValorB = false;
                              A1339ContratoServicosPrioridade_PercPrazo = context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_PercPrazo_Internalname), ",", ".");
                              n1339ContratoServicosPrioridade_PercPrazo = false;
                              A2067ContratoServicosPrioridade_Peso = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrioridade_Peso_Internalname), ",", "."));
                              n2067ContratoServicosPrioridade_Peso = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27K12 */
                                    E27K12 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28K12 */
                                    E28K12 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29K12 */
                                    E29K12 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoservicosprioridade_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSPRIORIDADE_NOME1"), AV17ContratoServicosPrioridade_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoservicosprioridade_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSPRIORIDADE_NOME2"), AV21ContratoServicosPrioridade_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoservicosprioridade_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSPRIORIDADE_NOME3"), AV25ContratoServicosPrioridade_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprioridade_ordem Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_ORDEM"), ",", ".") != Convert.ToDecimal( AV50TFContratoServicosPrioridade_Ordem )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprioridade_ordem_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO"), ",", ".") != Convert.ToDecimal( AV51TFContratoServicosPrioridade_Ordem_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprioridade_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_NOME"), AV34TFContratoServicosPrioridade_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprioridade_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL"), AV35TFContratoServicosPrioridade_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprioridade_percvalorb Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB"), ",", ".") != AV38TFContratoServicosPrioridade_PercValorB )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprioridade_percvalorb_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO"), ",", ".") != AV39TFContratoServicosPrioridade_PercValorB_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprioridade_percprazo Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO"), ",", ".") != AV42TFContratoServicosPrioridade_PercPrazo )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprioridade_percprazo_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO"), ",", ".") != AV43TFContratoServicosPrioridade_PercPrazo_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprioridade_peso Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PESO"), ",", ".") != Convert.ToDecimal( AV54TFContratoServicosPrioridade_Peso )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoservicosprioridade_peso_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO"), ",", ".") != Convert.ToDecimal( AV55TFContratoServicosPrioridade_Peso_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEK12( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAK12( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOSERVICOSPRIORIDADE_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOSERVICOSPRIORIDADE_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOSERVICOSPRIORIDADE_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_722( ) ;
         while ( nGXsfl_72_idx <= nRC_GXsfl_72 )
         {
            sendrow_722( ) ;
            nGXsfl_72_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_72_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_72_idx+1));
            sGXsfl_72_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_72_idx), 4, 0)), 4, "0");
            SubsflControlProps_722( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV17ContratoServicosPrioridade_Nome1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV21ContratoServicosPrioridade_Nome2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV25ContratoServicosPrioridade_Nome3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       short AV50TFContratoServicosPrioridade_Ordem ,
                                       short AV51TFContratoServicosPrioridade_Ordem_To ,
                                       String AV34TFContratoServicosPrioridade_Nome ,
                                       String AV35TFContratoServicosPrioridade_Nome_Sel ,
                                       decimal AV38TFContratoServicosPrioridade_PercValorB ,
                                       decimal AV39TFContratoServicosPrioridade_PercValorB_To ,
                                       decimal AV42TFContratoServicosPrioridade_PercPrazo ,
                                       decimal AV43TFContratoServicosPrioridade_PercPrazo_To ,
                                       short AV54TFContratoServicosPrioridade_Peso ,
                                       short AV55TFContratoServicosPrioridade_Peso_To ,
                                       String AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace ,
                                       String AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace ,
                                       String AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace ,
                                       String AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace ,
                                       String AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace ,
                                       String AV79Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A1336ContratoServicosPrioridade_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFK12( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1336ContratoServicosPrioridade_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRIORIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_ORDEM", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2066ContratoServicosPrioridade_Ordem), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRIORIDADE_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1337ContratoServicosPrioridade_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRIORIDADE_NOME", StringUtil.RTrim( A1337ContratoServicosPrioridade_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_PERCVALORB", GetSecureSignedToken( "", context.localUtil.Format( A1338ContratoServicosPrioridade_PercValorB, "ZZ9.99 %")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRIORIDADE_PERCVALORB", StringUtil.LTrim( StringUtil.NToC( A1338ContratoServicosPrioridade_PercValorB, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO", GetSecureSignedToken( "", context.localUtil.Format( A1339ContratoServicosPrioridade_PercPrazo, "ZZ9.99 %")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRIORIDADE_PERCPRAZO", StringUtil.LTrim( StringUtil.NToC( A1339ContratoServicosPrioridade_PercPrazo, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_PESO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2067ContratoServicosPrioridade_Peso), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRIORIDADE_PESO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2067ContratoServicosPrioridade_Peso), 4, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFK12( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV79Pgmname = "WWContratoServicosPrioridade";
         context.Gx_err = 0;
      }

      protected void RFK12( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 72;
         /* Execute user event: E28K12 */
         E28K12 ();
         nGXsfl_72_idx = 1;
         sGXsfl_72_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_72_idx), 4, 0)), 4, "0");
         SubsflControlProps_722( ) ;
         nGXsfl_72_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_722( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 ,
                                                 AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 ,
                                                 AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 ,
                                                 AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 ,
                                                 AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 ,
                                                 AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 ,
                                                 AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 ,
                                                 AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 ,
                                                 AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem ,
                                                 AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to ,
                                                 AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel ,
                                                 AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome ,
                                                 AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb ,
                                                 AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to ,
                                                 AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo ,
                                                 AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to ,
                                                 AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso ,
                                                 AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to ,
                                                 A1337ContratoServicosPrioridade_Nome ,
                                                 A2066ContratoServicosPrioridade_Ordem ,
                                                 A1338ContratoServicosPrioridade_PercValorB ,
                                                 A1339ContratoServicosPrioridade_PercPrazo ,
                                                 A2067ContratoServicosPrioridade_Peso ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1), 50, "%");
            lV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = StringUtil.PadR( StringUtil.RTrim( AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2), 50, "%");
            lV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = StringUtil.PadR( StringUtil.RTrim( AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3), 50, "%");
            lV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = StringUtil.PadR( StringUtil.RTrim( AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome), 50, "%");
            /* Using cursor H00K12 */
            pr_default.execute(0, new Object[] {lV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1, lV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2, lV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3, AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem, AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to, lV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome, AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel, AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb, AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to, AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo, AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to, AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso, AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_72_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2067ContratoServicosPrioridade_Peso = H00K12_A2067ContratoServicosPrioridade_Peso[0];
               n2067ContratoServicosPrioridade_Peso = H00K12_n2067ContratoServicosPrioridade_Peso[0];
               A1339ContratoServicosPrioridade_PercPrazo = H00K12_A1339ContratoServicosPrioridade_PercPrazo[0];
               n1339ContratoServicosPrioridade_PercPrazo = H00K12_n1339ContratoServicosPrioridade_PercPrazo[0];
               A1338ContratoServicosPrioridade_PercValorB = H00K12_A1338ContratoServicosPrioridade_PercValorB[0];
               n1338ContratoServicosPrioridade_PercValorB = H00K12_n1338ContratoServicosPrioridade_PercValorB[0];
               A1337ContratoServicosPrioridade_Nome = H00K12_A1337ContratoServicosPrioridade_Nome[0];
               A2066ContratoServicosPrioridade_Ordem = H00K12_A2066ContratoServicosPrioridade_Ordem[0];
               n2066ContratoServicosPrioridade_Ordem = H00K12_n2066ContratoServicosPrioridade_Ordem[0];
               A1335ContratoServicosPrioridade_CntSrvCod = H00K12_A1335ContratoServicosPrioridade_CntSrvCod[0];
               A1336ContratoServicosPrioridade_Codigo = H00K12_A1336ContratoServicosPrioridade_Codigo[0];
               /* Execute user event: E29K12 */
               E29K12 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 72;
            WBK10( ) ;
         }
         nGXsfl_72_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = AV17ContratoServicosPrioridade_Nome1;
         AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = AV21ContratoServicosPrioridade_Nome2;
         AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = AV25ContratoServicosPrioridade_Nome3;
         AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem = AV50TFContratoServicosPrioridade_Ordem;
         AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to = AV51TFContratoServicosPrioridade_Ordem_To;
         AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = AV34TFContratoServicosPrioridade_Nome;
         AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel = AV35TFContratoServicosPrioridade_Nome_Sel;
         AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb = AV38TFContratoServicosPrioridade_PercValorB;
         AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to = AV39TFContratoServicosPrioridade_PercValorB_To;
         AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo = AV42TFContratoServicosPrioridade_PercPrazo;
         AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to = AV43TFContratoServicosPrioridade_PercPrazo_To;
         AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso = AV54TFContratoServicosPrioridade_Peso;
         AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to = AV55TFContratoServicosPrioridade_Peso_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 ,
                                              AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 ,
                                              AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 ,
                                              AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 ,
                                              AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 ,
                                              AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 ,
                                              AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 ,
                                              AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 ,
                                              AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem ,
                                              AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to ,
                                              AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel ,
                                              AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome ,
                                              AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb ,
                                              AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to ,
                                              AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo ,
                                              AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to ,
                                              AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso ,
                                              AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to ,
                                              A1337ContratoServicosPrioridade_Nome ,
                                              A2066ContratoServicosPrioridade_Ordem ,
                                              A1338ContratoServicosPrioridade_PercValorB ,
                                              A1339ContratoServicosPrioridade_PercPrazo ,
                                              A2067ContratoServicosPrioridade_Peso ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1), 50, "%");
         lV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = StringUtil.PadR( StringUtil.RTrim( AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2), 50, "%");
         lV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = StringUtil.PadR( StringUtil.RTrim( AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3), 50, "%");
         lV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = StringUtil.PadR( StringUtil.RTrim( AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome), 50, "%");
         /* Using cursor H00K13 */
         pr_default.execute(1, new Object[] {lV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1, lV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2, lV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3, AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem, AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to, lV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome, AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel, AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb, AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to, AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo, AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to, AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso, AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to});
         GRID_nRecordCount = H00K13_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = AV17ContratoServicosPrioridade_Nome1;
         AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = AV21ContratoServicosPrioridade_Nome2;
         AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = AV25ContratoServicosPrioridade_Nome3;
         AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem = AV50TFContratoServicosPrioridade_Ordem;
         AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to = AV51TFContratoServicosPrioridade_Ordem_To;
         AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = AV34TFContratoServicosPrioridade_Nome;
         AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel = AV35TFContratoServicosPrioridade_Nome_Sel;
         AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb = AV38TFContratoServicosPrioridade_PercValorB;
         AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to = AV39TFContratoServicosPrioridade_PercValorB_To;
         AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo = AV42TFContratoServicosPrioridade_PercPrazo;
         AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to = AV43TFContratoServicosPrioridade_PercPrazo_To;
         AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso = AV54TFContratoServicosPrioridade_Peso;
         AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to = AV55TFContratoServicosPrioridade_Peso_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratoServicosPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicosPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ContratoServicosPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFContratoServicosPrioridade_Ordem, AV51TFContratoServicosPrioridade_Ordem_To, AV34TFContratoServicosPrioridade_Nome, AV35TFContratoServicosPrioridade_Nome_Sel, AV38TFContratoServicosPrioridade_PercValorB, AV39TFContratoServicosPrioridade_PercValorB_To, AV42TFContratoServicosPrioridade_PercPrazo, AV43TFContratoServicosPrioridade_PercPrazo_To, AV54TFContratoServicosPrioridade_Peso, AV55TFContratoServicosPrioridade_Peso_To, AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace, AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace, AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace, AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace, AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1336ContratoServicosPrioridade_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = AV17ContratoServicosPrioridade_Nome1;
         AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = AV21ContratoServicosPrioridade_Nome2;
         AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = AV25ContratoServicosPrioridade_Nome3;
         AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem = AV50TFContratoServicosPrioridade_Ordem;
         AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to = AV51TFContratoServicosPrioridade_Ordem_To;
         AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = AV34TFContratoServicosPrioridade_Nome;
         AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel = AV35TFContratoServicosPrioridade_Nome_Sel;
         AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb = AV38TFContratoServicosPrioridade_PercValorB;
         AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to = AV39TFContratoServicosPrioridade_PercValorB_To;
         AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo = AV42TFContratoServicosPrioridade_PercPrazo;
         AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to = AV43TFContratoServicosPrioridade_PercPrazo_To;
         AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso = AV54TFContratoServicosPrioridade_Peso;
         AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to = AV55TFContratoServicosPrioridade_Peso_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratoServicosPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicosPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ContratoServicosPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFContratoServicosPrioridade_Ordem, AV51TFContratoServicosPrioridade_Ordem_To, AV34TFContratoServicosPrioridade_Nome, AV35TFContratoServicosPrioridade_Nome_Sel, AV38TFContratoServicosPrioridade_PercValorB, AV39TFContratoServicosPrioridade_PercValorB_To, AV42TFContratoServicosPrioridade_PercPrazo, AV43TFContratoServicosPrioridade_PercPrazo_To, AV54TFContratoServicosPrioridade_Peso, AV55TFContratoServicosPrioridade_Peso_To, AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace, AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace, AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace, AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace, AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1336ContratoServicosPrioridade_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = AV17ContratoServicosPrioridade_Nome1;
         AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = AV21ContratoServicosPrioridade_Nome2;
         AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = AV25ContratoServicosPrioridade_Nome3;
         AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem = AV50TFContratoServicosPrioridade_Ordem;
         AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to = AV51TFContratoServicosPrioridade_Ordem_To;
         AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = AV34TFContratoServicosPrioridade_Nome;
         AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel = AV35TFContratoServicosPrioridade_Nome_Sel;
         AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb = AV38TFContratoServicosPrioridade_PercValorB;
         AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to = AV39TFContratoServicosPrioridade_PercValorB_To;
         AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo = AV42TFContratoServicosPrioridade_PercPrazo;
         AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to = AV43TFContratoServicosPrioridade_PercPrazo_To;
         AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso = AV54TFContratoServicosPrioridade_Peso;
         AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to = AV55TFContratoServicosPrioridade_Peso_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratoServicosPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicosPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ContratoServicosPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFContratoServicosPrioridade_Ordem, AV51TFContratoServicosPrioridade_Ordem_To, AV34TFContratoServicosPrioridade_Nome, AV35TFContratoServicosPrioridade_Nome_Sel, AV38TFContratoServicosPrioridade_PercValorB, AV39TFContratoServicosPrioridade_PercValorB_To, AV42TFContratoServicosPrioridade_PercPrazo, AV43TFContratoServicosPrioridade_PercPrazo_To, AV54TFContratoServicosPrioridade_Peso, AV55TFContratoServicosPrioridade_Peso_To, AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace, AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace, AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace, AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace, AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1336ContratoServicosPrioridade_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = AV17ContratoServicosPrioridade_Nome1;
         AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = AV21ContratoServicosPrioridade_Nome2;
         AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = AV25ContratoServicosPrioridade_Nome3;
         AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem = AV50TFContratoServicosPrioridade_Ordem;
         AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to = AV51TFContratoServicosPrioridade_Ordem_To;
         AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = AV34TFContratoServicosPrioridade_Nome;
         AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel = AV35TFContratoServicosPrioridade_Nome_Sel;
         AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb = AV38TFContratoServicosPrioridade_PercValorB;
         AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to = AV39TFContratoServicosPrioridade_PercValorB_To;
         AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo = AV42TFContratoServicosPrioridade_PercPrazo;
         AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to = AV43TFContratoServicosPrioridade_PercPrazo_To;
         AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso = AV54TFContratoServicosPrioridade_Peso;
         AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to = AV55TFContratoServicosPrioridade_Peso_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratoServicosPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicosPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ContratoServicosPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFContratoServicosPrioridade_Ordem, AV51TFContratoServicosPrioridade_Ordem_To, AV34TFContratoServicosPrioridade_Nome, AV35TFContratoServicosPrioridade_Nome_Sel, AV38TFContratoServicosPrioridade_PercValorB, AV39TFContratoServicosPrioridade_PercValorB_To, AV42TFContratoServicosPrioridade_PercPrazo, AV43TFContratoServicosPrioridade_PercPrazo_To, AV54TFContratoServicosPrioridade_Peso, AV55TFContratoServicosPrioridade_Peso_To, AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace, AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace, AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace, AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace, AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1336ContratoServicosPrioridade_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = AV17ContratoServicosPrioridade_Nome1;
         AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = AV21ContratoServicosPrioridade_Nome2;
         AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = AV25ContratoServicosPrioridade_Nome3;
         AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem = AV50TFContratoServicosPrioridade_Ordem;
         AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to = AV51TFContratoServicosPrioridade_Ordem_To;
         AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = AV34TFContratoServicosPrioridade_Nome;
         AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel = AV35TFContratoServicosPrioridade_Nome_Sel;
         AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb = AV38TFContratoServicosPrioridade_PercValorB;
         AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to = AV39TFContratoServicosPrioridade_PercValorB_To;
         AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo = AV42TFContratoServicosPrioridade_PercPrazo;
         AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to = AV43TFContratoServicosPrioridade_PercPrazo_To;
         AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso = AV54TFContratoServicosPrioridade_Peso;
         AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to = AV55TFContratoServicosPrioridade_Peso_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratoServicosPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicosPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ContratoServicosPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFContratoServicosPrioridade_Ordem, AV51TFContratoServicosPrioridade_Ordem_To, AV34TFContratoServicosPrioridade_Nome, AV35TFContratoServicosPrioridade_Nome_Sel, AV38TFContratoServicosPrioridade_PercValorB, AV39TFContratoServicosPrioridade_PercValorB_To, AV42TFContratoServicosPrioridade_PercPrazo, AV43TFContratoServicosPrioridade_PercPrazo_To, AV54TFContratoServicosPrioridade_Peso, AV55TFContratoServicosPrioridade_Peso_To, AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace, AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace, AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace, AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace, AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1336ContratoServicosPrioridade_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPK10( )
      {
         /* Before Start, stand alone formulas. */
         AV79Pgmname = "WWContratoServicosPrioridade";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E27K12 */
         E27K12 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV45DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSPRIORIDADE_ORDEMTITLEFILTERDATA"), AV49ContratoServicosPrioridade_OrdemTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSPRIORIDADE_NOMETITLEFILTERDATA"), AV33ContratoServicosPrioridade_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLEFILTERDATA"), AV37ContratoServicosPrioridade_PercValorBTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLEFILTERDATA"), AV41ContratoServicosPrioridade_PercPrazoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSPRIORIDADE_PESOTITLEFILTERDATA"), AV53ContratoServicosPrioridade_PesoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV17ContratoServicosPrioridade_Nome1 = StringUtil.Upper( cgiGet( edtavContratoservicosprioridade_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosPrioridade_Nome1", AV17ContratoServicosPrioridade_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            AV21ContratoServicosPrioridade_Nome2 = StringUtil.Upper( cgiGet( edtavContratoservicosprioridade_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosPrioridade_Nome2", AV21ContratoServicosPrioridade_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            AV25ContratoServicosPrioridade_Nome3 = StringUtil.Upper( cgiGet( edtavContratoservicosprioridade_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosPrioridade_Nome3", AV25ContratoServicosPrioridade_Nome3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_ordem_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_ordem_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRIORIDADE_ORDEM");
               GX_FocusControl = edtavTfcontratoservicosprioridade_ordem_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50TFContratoServicosPrioridade_Ordem = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFContratoServicosPrioridade_Ordem), 3, 0)));
            }
            else
            {
               AV50TFContratoServicosPrioridade_Ordem = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_ordem_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFContratoServicosPrioridade_Ordem), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_ordem_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_ordem_to_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO");
               GX_FocusControl = edtavTfcontratoservicosprioridade_ordem_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51TFContratoServicosPrioridade_Ordem_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoServicosPrioridade_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFContratoServicosPrioridade_Ordem_To), 3, 0)));
            }
            else
            {
               AV51TFContratoServicosPrioridade_Ordem_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_ordem_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoServicosPrioridade_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFContratoServicosPrioridade_Ordem_To), 3, 0)));
            }
            AV34TFContratoServicosPrioridade_Nome = StringUtil.Upper( cgiGet( edtavTfcontratoservicosprioridade_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratoServicosPrioridade_Nome", AV34TFContratoServicosPrioridade_Nome);
            AV35TFContratoServicosPrioridade_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoservicosprioridade_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratoServicosPrioridade_Nome_Sel", AV35TFContratoServicosPrioridade_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_percvalorb_Internalname), ",", ".") < -99.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_percvalorb_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB");
               GX_FocusControl = edtavTfcontratoservicosprioridade_percvalorb_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFContratoServicosPrioridade_PercValorB = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.Str( AV38TFContratoServicosPrioridade_PercValorB, 6, 2)));
            }
            else
            {
               AV38TFContratoServicosPrioridade_PercValorB = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_percvalorb_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.Str( AV38TFContratoServicosPrioridade_PercValorB, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_percvalorb_to_Internalname), ",", ".") < -99.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_percvalorb_to_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO");
               GX_FocusControl = edtavTfcontratoservicosprioridade_percvalorb_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFContratoServicosPrioridade_PercValorB_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosPrioridade_PercValorB_To", StringUtil.LTrim( StringUtil.Str( AV39TFContratoServicosPrioridade_PercValorB_To, 6, 2)));
            }
            else
            {
               AV39TFContratoServicosPrioridade_PercValorB_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_percvalorb_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosPrioridade_PercValorB_To", StringUtil.LTrim( StringUtil.Str( AV39TFContratoServicosPrioridade_PercValorB_To, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_percprazo_Internalname), ",", ".") < -99.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_percprazo_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO");
               GX_FocusControl = edtavTfcontratoservicosprioridade_percprazo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42TFContratoServicosPrioridade_PercPrazo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.Str( AV42TFContratoServicosPrioridade_PercPrazo, 6, 2)));
            }
            else
            {
               AV42TFContratoServicosPrioridade_PercPrazo = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_percprazo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.Str( AV42TFContratoServicosPrioridade_PercPrazo, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_percprazo_to_Internalname), ",", ".") < -99.99m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_percprazo_to_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO");
               GX_FocusControl = edtavTfcontratoservicosprioridade_percprazo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFContratoServicosPrioridade_PercPrazo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratoServicosPrioridade_PercPrazo_To", StringUtil.LTrim( StringUtil.Str( AV43TFContratoServicosPrioridade_PercPrazo_To, 6, 2)));
            }
            else
            {
               AV43TFContratoServicosPrioridade_PercPrazo_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_percprazo_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratoServicosPrioridade_PercPrazo_To", StringUtil.LTrim( StringUtil.Str( AV43TFContratoServicosPrioridade_PercPrazo_To, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_peso_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_peso_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRIORIDADE_PESO");
               GX_FocusControl = edtavTfcontratoservicosprioridade_peso_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFContratoServicosPrioridade_Peso = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFContratoServicosPrioridade_Peso), 4, 0)));
            }
            else
            {
               AV54TFContratoServicosPrioridade_Peso = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_peso_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFContratoServicosPrioridade_Peso), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_peso_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_peso_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO");
               GX_FocusControl = edtavTfcontratoservicosprioridade_peso_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFContratoServicosPrioridade_Peso_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoServicosPrioridade_Peso_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFContratoServicosPrioridade_Peso_To), 4, 0)));
            }
            else
            {
               AV55TFContratoServicosPrioridade_Peso_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprioridade_peso_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoServicosPrioridade_Peso_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFContratoServicosPrioridade_Peso_To), 4, 0)));
            }
            AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprioridade_ordemtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace", AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace);
            AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprioridade_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace", AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace);
            AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprioridade_percvalorbtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace", AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace);
            AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprioridade_percprazotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace", AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace);
            AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprioridade_pesotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace", AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_72 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_72"), ",", "."));
            AV47GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV48GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoservicosprioridade_ordem_Caption = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Caption");
            Ddo_contratoservicosprioridade_ordem_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Tooltip");
            Ddo_contratoservicosprioridade_ordem_Cls = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Cls");
            Ddo_contratoservicosprioridade_ordem_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Filteredtext_set");
            Ddo_contratoservicosprioridade_ordem_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Filteredtextto_set");
            Ddo_contratoservicosprioridade_ordem_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Dropdownoptionstype");
            Ddo_contratoservicosprioridade_ordem_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Titlecontrolidtoreplace");
            Ddo_contratoservicosprioridade_ordem_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Includesortasc"));
            Ddo_contratoservicosprioridade_ordem_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Includesortdsc"));
            Ddo_contratoservicosprioridade_ordem_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Sortedstatus");
            Ddo_contratoservicosprioridade_ordem_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Includefilter"));
            Ddo_contratoservicosprioridade_ordem_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Filtertype");
            Ddo_contratoservicosprioridade_ordem_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Filterisrange"));
            Ddo_contratoservicosprioridade_ordem_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Includedatalist"));
            Ddo_contratoservicosprioridade_ordem_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Sortasc");
            Ddo_contratoservicosprioridade_ordem_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Sortdsc");
            Ddo_contratoservicosprioridade_ordem_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Cleanfilter");
            Ddo_contratoservicosprioridade_ordem_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Rangefilterfrom");
            Ddo_contratoservicosprioridade_ordem_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Rangefilterto");
            Ddo_contratoservicosprioridade_ordem_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Searchbuttontext");
            Ddo_contratoservicosprioridade_nome_Caption = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Caption");
            Ddo_contratoservicosprioridade_nome_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Tooltip");
            Ddo_contratoservicosprioridade_nome_Cls = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Cls");
            Ddo_contratoservicosprioridade_nome_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Filteredtext_set");
            Ddo_contratoservicosprioridade_nome_Selectedvalue_set = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Selectedvalue_set");
            Ddo_contratoservicosprioridade_nome_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Dropdownoptionstype");
            Ddo_contratoservicosprioridade_nome_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Titlecontrolidtoreplace");
            Ddo_contratoservicosprioridade_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Includesortasc"));
            Ddo_contratoservicosprioridade_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Includesortdsc"));
            Ddo_contratoservicosprioridade_nome_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Sortedstatus");
            Ddo_contratoservicosprioridade_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Includefilter"));
            Ddo_contratoservicosprioridade_nome_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Filtertype");
            Ddo_contratoservicosprioridade_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Filterisrange"));
            Ddo_contratoservicosprioridade_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Includedatalist"));
            Ddo_contratoservicosprioridade_nome_Datalisttype = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Datalisttype");
            Ddo_contratoservicosprioridade_nome_Datalistproc = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Datalistproc");
            Ddo_contratoservicosprioridade_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicosprioridade_nome_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Sortasc");
            Ddo_contratoservicosprioridade_nome_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Sortdsc");
            Ddo_contratoservicosprioridade_nome_Loadingdata = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Loadingdata");
            Ddo_contratoservicosprioridade_nome_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Cleanfilter");
            Ddo_contratoservicosprioridade_nome_Noresultsfound = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Noresultsfound");
            Ddo_contratoservicosprioridade_nome_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Searchbuttontext");
            Ddo_contratoservicosprioridade_percvalorb_Caption = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Caption");
            Ddo_contratoservicosprioridade_percvalorb_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Tooltip");
            Ddo_contratoservicosprioridade_percvalorb_Cls = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Cls");
            Ddo_contratoservicosprioridade_percvalorb_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Filteredtext_set");
            Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Filteredtextto_set");
            Ddo_contratoservicosprioridade_percvalorb_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Dropdownoptionstype");
            Ddo_contratoservicosprioridade_percvalorb_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Titlecontrolidtoreplace");
            Ddo_contratoservicosprioridade_percvalorb_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Includesortasc"));
            Ddo_contratoservicosprioridade_percvalorb_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Includesortdsc"));
            Ddo_contratoservicosprioridade_percvalorb_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Sortedstatus");
            Ddo_contratoservicosprioridade_percvalorb_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Includefilter"));
            Ddo_contratoservicosprioridade_percvalorb_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Filtertype");
            Ddo_contratoservicosprioridade_percvalorb_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Filterisrange"));
            Ddo_contratoservicosprioridade_percvalorb_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Includedatalist"));
            Ddo_contratoservicosprioridade_percvalorb_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Sortasc");
            Ddo_contratoservicosprioridade_percvalorb_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Sortdsc");
            Ddo_contratoservicosprioridade_percvalorb_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Cleanfilter");
            Ddo_contratoservicosprioridade_percvalorb_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Rangefilterfrom");
            Ddo_contratoservicosprioridade_percvalorb_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Rangefilterto");
            Ddo_contratoservicosprioridade_percvalorb_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Searchbuttontext");
            Ddo_contratoservicosprioridade_percprazo_Caption = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Caption");
            Ddo_contratoservicosprioridade_percprazo_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Tooltip");
            Ddo_contratoservicosprioridade_percprazo_Cls = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Cls");
            Ddo_contratoservicosprioridade_percprazo_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Filteredtext_set");
            Ddo_contratoservicosprioridade_percprazo_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Filteredtextto_set");
            Ddo_contratoservicosprioridade_percprazo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Dropdownoptionstype");
            Ddo_contratoservicosprioridade_percprazo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Titlecontrolidtoreplace");
            Ddo_contratoservicosprioridade_percprazo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Includesortasc"));
            Ddo_contratoservicosprioridade_percprazo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Includesortdsc"));
            Ddo_contratoservicosprioridade_percprazo_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Sortedstatus");
            Ddo_contratoservicosprioridade_percprazo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Includefilter"));
            Ddo_contratoservicosprioridade_percprazo_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Filtertype");
            Ddo_contratoservicosprioridade_percprazo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Filterisrange"));
            Ddo_contratoservicosprioridade_percprazo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Includedatalist"));
            Ddo_contratoservicosprioridade_percprazo_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Sortasc");
            Ddo_contratoservicosprioridade_percprazo_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Sortdsc");
            Ddo_contratoservicosprioridade_percprazo_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Cleanfilter");
            Ddo_contratoservicosprioridade_percprazo_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Rangefilterfrom");
            Ddo_contratoservicosprioridade_percprazo_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Rangefilterto");
            Ddo_contratoservicosprioridade_percprazo_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Searchbuttontext");
            Ddo_contratoservicosprioridade_peso_Caption = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Caption");
            Ddo_contratoservicosprioridade_peso_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Tooltip");
            Ddo_contratoservicosprioridade_peso_Cls = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Cls");
            Ddo_contratoservicosprioridade_peso_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Filteredtext_set");
            Ddo_contratoservicosprioridade_peso_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Filteredtextto_set");
            Ddo_contratoservicosprioridade_peso_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Dropdownoptionstype");
            Ddo_contratoservicosprioridade_peso_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Titlecontrolidtoreplace");
            Ddo_contratoservicosprioridade_peso_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Includesortasc"));
            Ddo_contratoservicosprioridade_peso_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Includesortdsc"));
            Ddo_contratoservicosprioridade_peso_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Sortedstatus");
            Ddo_contratoservicosprioridade_peso_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Includefilter"));
            Ddo_contratoservicosprioridade_peso_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Filtertype");
            Ddo_contratoservicosprioridade_peso_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Filterisrange"));
            Ddo_contratoservicosprioridade_peso_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Includedatalist"));
            Ddo_contratoservicosprioridade_peso_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Sortasc");
            Ddo_contratoservicosprioridade_peso_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Sortdsc");
            Ddo_contratoservicosprioridade_peso_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Cleanfilter");
            Ddo_contratoservicosprioridade_peso_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Rangefilterfrom");
            Ddo_contratoservicosprioridade_peso_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Rangefilterto");
            Ddo_contratoservicosprioridade_peso_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoservicosprioridade_ordem_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Activeeventkey");
            Ddo_contratoservicosprioridade_ordem_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Filteredtext_get");
            Ddo_contratoservicosprioridade_ordem_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM_Filteredtextto_get");
            Ddo_contratoservicosprioridade_nome_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Activeeventkey");
            Ddo_contratoservicosprioridade_nome_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Filteredtext_get");
            Ddo_contratoservicosprioridade_nome_Selectedvalue_get = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_NOME_Selectedvalue_get");
            Ddo_contratoservicosprioridade_percvalorb_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Activeeventkey");
            Ddo_contratoservicosprioridade_percvalorb_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Filteredtext_get");
            Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB_Filteredtextto_get");
            Ddo_contratoservicosprioridade_percprazo_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Activeeventkey");
            Ddo_contratoservicosprioridade_percprazo_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Filteredtext_get");
            Ddo_contratoservicosprioridade_percprazo_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_Filteredtextto_get");
            Ddo_contratoservicosprioridade_peso_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Activeeventkey");
            Ddo_contratoservicosprioridade_peso_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Filteredtext_get");
            Ddo_contratoservicosprioridade_peso_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSPRIORIDADE_PESO_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSPRIORIDADE_NOME1"), AV17ContratoServicosPrioridade_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSPRIORIDADE_NOME2"), AV21ContratoServicosPrioridade_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOSERVICOSPRIORIDADE_NOME3"), AV25ContratoServicosPrioridade_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_ORDEM"), ",", ".") != Convert.ToDecimal( AV50TFContratoServicosPrioridade_Ordem )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO"), ",", ".") != Convert.ToDecimal( AV51TFContratoServicosPrioridade_Ordem_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_NOME"), AV34TFContratoServicosPrioridade_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL"), AV35TFContratoServicosPrioridade_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB"), ",", ".") != AV38TFContratoServicosPrioridade_PercValorB )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO"), ",", ".") != AV39TFContratoServicosPrioridade_PercValorB_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO"), ",", ".") != AV42TFContratoServicosPrioridade_PercPrazo )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO"), ",", ".") != AV43TFContratoServicosPrioridade_PercPrazo_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PESO"), ",", ".") != Convert.ToDecimal( AV54TFContratoServicosPrioridade_Peso )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO"), ",", ".") != Convert.ToDecimal( AV55TFContratoServicosPrioridade_Peso_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E27K12 */
         E27K12 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27K12( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATOSERVICOSPRIORIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTRATOSERVICOSPRIORIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "CONTRATOSERVICOSPRIORIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontratoservicosprioridade_ordem_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprioridade_ordem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprioridade_ordem_Visible), 5, 0)));
         edtavTfcontratoservicosprioridade_ordem_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprioridade_ordem_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprioridade_ordem_to_Visible), 5, 0)));
         edtavTfcontratoservicosprioridade_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprioridade_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprioridade_nome_Visible), 5, 0)));
         edtavTfcontratoservicosprioridade_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprioridade_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprioridade_nome_sel_Visible), 5, 0)));
         edtavTfcontratoservicosprioridade_percvalorb_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprioridade_percvalorb_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprioridade_percvalorb_Visible), 5, 0)));
         edtavTfcontratoservicosprioridade_percvalorb_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprioridade_percvalorb_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprioridade_percvalorb_to_Visible), 5, 0)));
         edtavTfcontratoservicosprioridade_percprazo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprioridade_percprazo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprioridade_percprazo_Visible), 5, 0)));
         edtavTfcontratoservicosprioridade_percprazo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprioridade_percprazo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprioridade_percprazo_to_Visible), 5, 0)));
         edtavTfcontratoservicosprioridade_peso_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprioridade_peso_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprioridade_peso_Visible), 5, 0)));
         edtavTfcontratoservicosprioridade_peso_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprioridade_peso_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprioridade_peso_to_Visible), 5, 0)));
         Ddo_contratoservicosprioridade_ordem_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrioridade_Ordem";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_ordem_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprioridade_ordem_Titlecontrolidtoreplace);
         AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace = Ddo_contratoservicosprioridade_ordem_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace", AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace);
         edtavDdo_contratoservicosprioridade_ordemtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosprioridade_ordemtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprioridade_ordemtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosprioridade_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrioridade_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_nome_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprioridade_nome_Titlecontrolidtoreplace);
         AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace = Ddo_contratoservicosprioridade_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace", AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace);
         edtavDdo_contratoservicosprioridade_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosprioridade_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprioridade_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosprioridade_percvalorb_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrioridade_PercValorB";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percvalorb_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprioridade_percvalorb_Titlecontrolidtoreplace);
         AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace = Ddo_contratoservicosprioridade_percvalorb_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace", AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace);
         edtavDdo_contratoservicosprioridade_percvalorbtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosprioridade_percvalorbtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprioridade_percvalorbtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosprioridade_percprazo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrioridade_PercPrazo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percprazo_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprioridade_percprazo_Titlecontrolidtoreplace);
         AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace = Ddo_contratoservicosprioridade_percprazo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace", AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace);
         edtavDdo_contratoservicosprioridade_percprazotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosprioridade_percprazotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprioridade_percprazotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosprioridade_peso_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrioridade_Peso";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_peso_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprioridade_peso_Titlecontrolidtoreplace);
         AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace = Ddo_contratoservicosprioridade_peso_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace", AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace);
         edtavDdo_contratoservicosprioridade_pesotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosprioridade_pesotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprioridade_pesotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Prioridade";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "", 0);
         cmbavOrderedby.addItem("3", "% do Valor B", 0);
         cmbavOrderedby.addItem("4", "% do Prazo", 0);
         cmbavOrderedby.addItem("5", "Peso", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV45DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV45DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E28K12( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV49ContratoServicosPrioridade_OrdemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV33ContratoServicosPrioridade_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37ContratoServicosPrioridade_PercValorBTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41ContratoServicosPrioridade_PercPrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53ContratoServicosPrioridade_PesoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoServicosPrioridade_Ordem_Titleformat = 2;
         edtContratoServicosPrioridade_Ordem_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "", AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_Ordem_Internalname, "Title", edtContratoServicosPrioridade_Ordem_Title);
         edtContratoServicosPrioridade_Nome_Titleformat = 2;
         edtContratoServicosPrioridade_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_Nome_Internalname, "Title", edtContratoServicosPrioridade_Nome_Title);
         edtContratoServicosPrioridade_PercValorB_Titleformat = 2;
         edtContratoServicosPrioridade_PercValorB_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "% do Valor B", AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_PercValorB_Internalname, "Title", edtContratoServicosPrioridade_PercValorB_Title);
         edtContratoServicosPrioridade_PercPrazo_Titleformat = 2;
         edtContratoServicosPrioridade_PercPrazo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "% do Prazo", AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_PercPrazo_Internalname, "Title", edtContratoServicosPrioridade_PercPrazo_Title);
         edtContratoServicosPrioridade_Peso_Titleformat = 2;
         edtContratoServicosPrioridade_Peso_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Peso", AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrioridade_Peso_Internalname, "Title", edtContratoServicosPrioridade_Peso_Title);
         AV47GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47GridCurrentPage), 10, 0)));
         AV48GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48GridPageCount), 10, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = AV17ContratoServicosPrioridade_Nome1;
         AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = AV21ContratoServicosPrioridade_Nome2;
         AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = AV25ContratoServicosPrioridade_Nome3;
         AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem = AV50TFContratoServicosPrioridade_Ordem;
         AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to = AV51TFContratoServicosPrioridade_Ordem_To;
         AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = AV34TFContratoServicosPrioridade_Nome;
         AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel = AV35TFContratoServicosPrioridade_Nome_Sel;
         AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb = AV38TFContratoServicosPrioridade_PercValorB;
         AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to = AV39TFContratoServicosPrioridade_PercValorB_To;
         AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo = AV42TFContratoServicosPrioridade_PercPrazo;
         AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to = AV43TFContratoServicosPrioridade_PercPrazo_To;
         AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso = AV54TFContratoServicosPrioridade_Peso;
         AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to = AV55TFContratoServicosPrioridade_Peso_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49ContratoServicosPrioridade_OrdemTitleFilterData", AV49ContratoServicosPrioridade_OrdemTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33ContratoServicosPrioridade_NomeTitleFilterData", AV33ContratoServicosPrioridade_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37ContratoServicosPrioridade_PercValorBTitleFilterData", AV37ContratoServicosPrioridade_PercValorBTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV41ContratoServicosPrioridade_PercPrazoTitleFilterData", AV41ContratoServicosPrioridade_PercPrazoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV53ContratoServicosPrioridade_PesoTitleFilterData", AV53ContratoServicosPrioridade_PesoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11K12( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV46PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV46PageToGo) ;
         }
      }

      protected void E12K12( )
      {
         /* Ddo_contratoservicosprioridade_ordem_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_ordem_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprioridade_ordem_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_ordem_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_ordem_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_ordem_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprioridade_ordem_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_ordem_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_ordem_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_ordem_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFContratoServicosPrioridade_Ordem = (short)(NumberUtil.Val( Ddo_contratoservicosprioridade_ordem_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFContratoServicosPrioridade_Ordem), 3, 0)));
            AV51TFContratoServicosPrioridade_Ordem_To = (short)(NumberUtil.Val( Ddo_contratoservicosprioridade_ordem_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoServicosPrioridade_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFContratoServicosPrioridade_Ordem_To), 3, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13K12( )
      {
         /* Ddo_contratoservicosprioridade_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprioridade_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_nome_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprioridade_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_nome_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFContratoServicosPrioridade_Nome = Ddo_contratoservicosprioridade_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratoServicosPrioridade_Nome", AV34TFContratoServicosPrioridade_Nome);
            AV35TFContratoServicosPrioridade_Nome_Sel = Ddo_contratoservicosprioridade_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratoServicosPrioridade_Nome_Sel", AV35TFContratoServicosPrioridade_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14K12( )
      {
         /* Ddo_contratoservicosprioridade_percvalorb_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_percvalorb_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprioridade_percvalorb_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percvalorb_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_percvalorb_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_percvalorb_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprioridade_percvalorb_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percvalorb_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_percvalorb_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_percvalorb_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFContratoServicosPrioridade_PercValorB = NumberUtil.Val( Ddo_contratoservicosprioridade_percvalorb_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.Str( AV38TFContratoServicosPrioridade_PercValorB, 6, 2)));
            AV39TFContratoServicosPrioridade_PercValorB_To = NumberUtil.Val( Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosPrioridade_PercValorB_To", StringUtil.LTrim( StringUtil.Str( AV39TFContratoServicosPrioridade_PercValorB_To, 6, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15K12( )
      {
         /* Ddo_contratoservicosprioridade_percprazo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_percprazo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprioridade_percprazo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percprazo_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_percprazo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_percprazo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprioridade_percprazo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percprazo_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_percprazo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_percprazo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV42TFContratoServicosPrioridade_PercPrazo = NumberUtil.Val( Ddo_contratoservicosprioridade_percprazo_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.Str( AV42TFContratoServicosPrioridade_PercPrazo, 6, 2)));
            AV43TFContratoServicosPrioridade_PercPrazo_To = NumberUtil.Val( Ddo_contratoservicosprioridade_percprazo_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratoServicosPrioridade_PercPrazo_To", StringUtil.LTrim( StringUtil.Str( AV43TFContratoServicosPrioridade_PercPrazo_To, 6, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16K12( )
      {
         /* Ddo_contratoservicosprioridade_peso_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_peso_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprioridade_peso_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_peso_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_peso_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_peso_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosprioridade_peso_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_peso_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_peso_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprioridade_peso_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV54TFContratoServicosPrioridade_Peso = (short)(NumberUtil.Val( Ddo_contratoservicosprioridade_peso_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFContratoServicosPrioridade_Peso), 4, 0)));
            AV55TFContratoServicosPrioridade_Peso_To = (short)(NumberUtil.Val( Ddo_contratoservicosprioridade_peso_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoServicosPrioridade_Peso_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFContratoServicosPrioridade_Peso_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E29K12( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV77Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratoservicosprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1336ContratoServicosPrioridade_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV78Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratoservicosprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1336ContratoServicosPrioridade_Codigo);
         edtContratoServicosPrioridade_Nome_Link = formatLink("viewcontratoservicosprioridade.aspx") + "?" + UrlEncode("" +A1336ContratoServicosPrioridade_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 72;
         }
         sendrow_722( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_72_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(72, GridRow);
         }
      }

      protected void E17K12( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22K12( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E18K12( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratoServicosPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicosPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ContratoServicosPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFContratoServicosPrioridade_Ordem, AV51TFContratoServicosPrioridade_Ordem_To, AV34TFContratoServicosPrioridade_Nome, AV35TFContratoServicosPrioridade_Nome_Sel, AV38TFContratoServicosPrioridade_PercValorB, AV39TFContratoServicosPrioridade_PercValorB_To, AV42TFContratoServicosPrioridade_PercPrazo, AV43TFContratoServicosPrioridade_PercPrazo_To, AV54TFContratoServicosPrioridade_Peso, AV55TFContratoServicosPrioridade_Peso_To, AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace, AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace, AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace, AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace, AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1336ContratoServicosPrioridade_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23K12( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24K12( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E19K12( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratoServicosPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicosPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ContratoServicosPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFContratoServicosPrioridade_Ordem, AV51TFContratoServicosPrioridade_Ordem_To, AV34TFContratoServicosPrioridade_Nome, AV35TFContratoServicosPrioridade_Nome_Sel, AV38TFContratoServicosPrioridade_PercValorB, AV39TFContratoServicosPrioridade_PercValorB_To, AV42TFContratoServicosPrioridade_PercPrazo, AV43TFContratoServicosPrioridade_PercPrazo_To, AV54TFContratoServicosPrioridade_Peso, AV55TFContratoServicosPrioridade_Peso_To, AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace, AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace, AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace, AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace, AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1336ContratoServicosPrioridade_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E25K12( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20K12( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ContratoServicosPrioridade_Nome1, AV19DynamicFiltersSelector2, AV21ContratoServicosPrioridade_Nome2, AV23DynamicFiltersSelector3, AV25ContratoServicosPrioridade_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFContratoServicosPrioridade_Ordem, AV51TFContratoServicosPrioridade_Ordem_To, AV34TFContratoServicosPrioridade_Nome, AV35TFContratoServicosPrioridade_Nome_Sel, AV38TFContratoServicosPrioridade_PercValorB, AV39TFContratoServicosPrioridade_PercValorB_To, AV42TFContratoServicosPrioridade_PercPrazo, AV43TFContratoServicosPrioridade_PercPrazo_To, AV54TFContratoServicosPrioridade_Peso, AV55TFContratoServicosPrioridade_Peso_To, AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace, AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace, AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace, AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace, AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1336ContratoServicosPrioridade_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E26K12( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21K12( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoservicosprioridade_ordem_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_ordem_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_ordem_Sortedstatus);
         Ddo_contratoservicosprioridade_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_nome_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_nome_Sortedstatus);
         Ddo_contratoservicosprioridade_percvalorb_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percvalorb_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_percvalorb_Sortedstatus);
         Ddo_contratoservicosprioridade_percprazo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percprazo_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_percprazo_Sortedstatus);
         Ddo_contratoservicosprioridade_peso_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_peso_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_peso_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contratoservicosprioridade_ordem_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_ordem_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_ordem_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoservicosprioridade_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_nome_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoservicosprioridade_percvalorb_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percvalorb_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_percvalorb_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratoservicosprioridade_percprazo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percprazo_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_percprazo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratoservicosprioridade_peso_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_peso_Internalname, "SortedStatus", Ddo_contratoservicosprioridade_peso_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratoservicosprioridade_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosprioridade_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosprioridade_nome1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 )
         {
            edtavContratoservicosprioridade_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosprioridade_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosprioridade_nome1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContratoservicosprioridade_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosprioridade_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosprioridade_nome2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 )
         {
            edtavContratoservicosprioridade_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosprioridade_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosprioridade_nome2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContratoservicosprioridade_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosprioridade_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosprioridade_nome3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 )
         {
            edtavContratoservicosprioridade_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosprioridade_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosprioridade_nome3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATOSERVICOSPRIORIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV21ContratoServicosPrioridade_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosPrioridade_Nome2", AV21ContratoServicosPrioridade_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CONTRATOSERVICOSPRIORIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV25ContratoServicosPrioridade_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosPrioridade_Nome3", AV25ContratoServicosPrioridade_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV50TFContratoServicosPrioridade_Ordem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFContratoServicosPrioridade_Ordem), 3, 0)));
         Ddo_contratoservicosprioridade_ordem_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_ordem_Internalname, "FilteredText_set", Ddo_contratoservicosprioridade_ordem_Filteredtext_set);
         AV51TFContratoServicosPrioridade_Ordem_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoServicosPrioridade_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFContratoServicosPrioridade_Ordem_To), 3, 0)));
         Ddo_contratoservicosprioridade_ordem_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_ordem_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprioridade_ordem_Filteredtextto_set);
         AV34TFContratoServicosPrioridade_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratoServicosPrioridade_Nome", AV34TFContratoServicosPrioridade_Nome);
         Ddo_contratoservicosprioridade_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_nome_Internalname, "FilteredText_set", Ddo_contratoservicosprioridade_nome_Filteredtext_set);
         AV35TFContratoServicosPrioridade_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratoServicosPrioridade_Nome_Sel", AV35TFContratoServicosPrioridade_Nome_Sel);
         Ddo_contratoservicosprioridade_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_nome_Internalname, "SelectedValue_set", Ddo_contratoservicosprioridade_nome_Selectedvalue_set);
         AV38TFContratoServicosPrioridade_PercValorB = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.Str( AV38TFContratoServicosPrioridade_PercValorB, 6, 2)));
         Ddo_contratoservicosprioridade_percvalorb_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percvalorb_Internalname, "FilteredText_set", Ddo_contratoservicosprioridade_percvalorb_Filteredtext_set);
         AV39TFContratoServicosPrioridade_PercValorB_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosPrioridade_PercValorB_To", StringUtil.LTrim( StringUtil.Str( AV39TFContratoServicosPrioridade_PercValorB_To, 6, 2)));
         Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percvalorb_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_set);
         AV42TFContratoServicosPrioridade_PercPrazo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.Str( AV42TFContratoServicosPrioridade_PercPrazo, 6, 2)));
         Ddo_contratoservicosprioridade_percprazo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percprazo_Internalname, "FilteredText_set", Ddo_contratoservicosprioridade_percprazo_Filteredtext_set);
         AV43TFContratoServicosPrioridade_PercPrazo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratoServicosPrioridade_PercPrazo_To", StringUtil.LTrim( StringUtil.Str( AV43TFContratoServicosPrioridade_PercPrazo_To, 6, 2)));
         Ddo_contratoservicosprioridade_percprazo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percprazo_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprioridade_percprazo_Filteredtextto_set);
         AV54TFContratoServicosPrioridade_Peso = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFContratoServicosPrioridade_Peso), 4, 0)));
         Ddo_contratoservicosprioridade_peso_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_peso_Internalname, "FilteredText_set", Ddo_contratoservicosprioridade_peso_Filteredtext_set);
         AV55TFContratoServicosPrioridade_Peso_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoServicosPrioridade_Peso_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFContratoServicosPrioridade_Peso_To), 4, 0)));
         Ddo_contratoservicosprioridade_peso_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_peso_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprioridade_peso_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "CONTRATOSERVICOSPRIORIDADE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17ContratoServicosPrioridade_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosPrioridade_Nome1", AV17ContratoServicosPrioridade_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV79Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV79Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV79Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV80GXV1 = 1;
         while ( AV80GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV80GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRIORIDADE_ORDEM") == 0 )
            {
               AV50TFContratoServicosPrioridade_Ordem = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoServicosPrioridade_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50TFContratoServicosPrioridade_Ordem), 3, 0)));
               AV51TFContratoServicosPrioridade_Ordem_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoServicosPrioridade_Ordem_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFContratoServicosPrioridade_Ordem_To), 3, 0)));
               if ( ! (0==AV50TFContratoServicosPrioridade_Ordem) )
               {
                  Ddo_contratoservicosprioridade_ordem_Filteredtext_set = StringUtil.Str( (decimal)(AV50TFContratoServicosPrioridade_Ordem), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_ordem_Internalname, "FilteredText_set", Ddo_contratoservicosprioridade_ordem_Filteredtext_set);
               }
               if ( ! (0==AV51TFContratoServicosPrioridade_Ordem_To) )
               {
                  Ddo_contratoservicosprioridade_ordem_Filteredtextto_set = StringUtil.Str( (decimal)(AV51TFContratoServicosPrioridade_Ordem_To), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_ordem_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprioridade_ordem_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRIORIDADE_NOME") == 0 )
            {
               AV34TFContratoServicosPrioridade_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFContratoServicosPrioridade_Nome", AV34TFContratoServicosPrioridade_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratoServicosPrioridade_Nome)) )
               {
                  Ddo_contratoservicosprioridade_nome_Filteredtext_set = AV34TFContratoServicosPrioridade_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_nome_Internalname, "FilteredText_set", Ddo_contratoservicosprioridade_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRIORIDADE_NOME_SEL") == 0 )
            {
               AV35TFContratoServicosPrioridade_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratoServicosPrioridade_Nome_Sel", AV35TFContratoServicosPrioridade_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFContratoServicosPrioridade_Nome_Sel)) )
               {
                  Ddo_contratoservicosprioridade_nome_Selectedvalue_set = AV35TFContratoServicosPrioridade_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_nome_Internalname, "SelectedValue_set", Ddo_contratoservicosprioridade_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRIORIDADE_PERCVALORB") == 0 )
            {
               AV38TFContratoServicosPrioridade_PercValorB = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoServicosPrioridade_PercValorB", StringUtil.LTrim( StringUtil.Str( AV38TFContratoServicosPrioridade_PercValorB, 6, 2)));
               AV39TFContratoServicosPrioridade_PercValorB_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContratoServicosPrioridade_PercValorB_To", StringUtil.LTrim( StringUtil.Str( AV39TFContratoServicosPrioridade_PercValorB_To, 6, 2)));
               if ( ! (Convert.ToDecimal(0)==AV38TFContratoServicosPrioridade_PercValorB) )
               {
                  Ddo_contratoservicosprioridade_percvalorb_Filteredtext_set = StringUtil.Str( AV38TFContratoServicosPrioridade_PercValorB, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percvalorb_Internalname, "FilteredText_set", Ddo_contratoservicosprioridade_percvalorb_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV39TFContratoServicosPrioridade_PercValorB_To) )
               {
                  Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_set = StringUtil.Str( AV39TFContratoServicosPrioridade_PercValorB_To, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percvalorb_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO") == 0 )
            {
               AV42TFContratoServicosPrioridade_PercPrazo = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoServicosPrioridade_PercPrazo", StringUtil.LTrim( StringUtil.Str( AV42TFContratoServicosPrioridade_PercPrazo, 6, 2)));
               AV43TFContratoServicosPrioridade_PercPrazo_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratoServicosPrioridade_PercPrazo_To", StringUtil.LTrim( StringUtil.Str( AV43TFContratoServicosPrioridade_PercPrazo_To, 6, 2)));
               if ( ! (Convert.ToDecimal(0)==AV42TFContratoServicosPrioridade_PercPrazo) )
               {
                  Ddo_contratoservicosprioridade_percprazo_Filteredtext_set = StringUtil.Str( AV42TFContratoServicosPrioridade_PercPrazo, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percprazo_Internalname, "FilteredText_set", Ddo_contratoservicosprioridade_percprazo_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV43TFContratoServicosPrioridade_PercPrazo_To) )
               {
                  Ddo_contratoservicosprioridade_percprazo_Filteredtextto_set = StringUtil.Str( AV43TFContratoServicosPrioridade_PercPrazo_To, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_percprazo_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprioridade_percprazo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSPRIORIDADE_PESO") == 0 )
            {
               AV54TFContratoServicosPrioridade_Peso = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoServicosPrioridade_Peso", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFContratoServicosPrioridade_Peso), 4, 0)));
               AV55TFContratoServicosPrioridade_Peso_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratoServicosPrioridade_Peso_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFContratoServicosPrioridade_Peso_To), 4, 0)));
               if ( ! (0==AV54TFContratoServicosPrioridade_Peso) )
               {
                  Ddo_contratoservicosprioridade_peso_Filteredtext_set = StringUtil.Str( (decimal)(AV54TFContratoServicosPrioridade_Peso), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_peso_Internalname, "FilteredText_set", Ddo_contratoservicosprioridade_peso_Filteredtext_set);
               }
               if ( ! (0==AV55TFContratoServicosPrioridade_Peso_To) )
               {
                  Ddo_contratoservicosprioridade_peso_Filteredtextto_set = StringUtil.Str( (decimal)(AV55TFContratoServicosPrioridade_Peso_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprioridade_peso_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprioridade_peso_Filteredtextto_set);
               }
            }
            AV80GXV1 = (int)(AV80GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 )
            {
               AV17ContratoServicosPrioridade_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosPrioridade_Nome1", AV17ContratoServicosPrioridade_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 )
               {
                  AV21ContratoServicosPrioridade_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosPrioridade_Nome2", AV21ContratoServicosPrioridade_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 )
                  {
                     AV25ContratoServicosPrioridade_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosPrioridade_Nome3", AV25ContratoServicosPrioridade_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV79Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV50TFContratoServicosPrioridade_Ordem) && (0==AV51TFContratoServicosPrioridade_Ordem_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSPRIORIDADE_ORDEM";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV50TFContratoServicosPrioridade_Ordem), 3, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV51TFContratoServicosPrioridade_Ordem_To), 3, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFContratoServicosPrioridade_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSPRIORIDADE_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV34TFContratoServicosPrioridade_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFContratoServicosPrioridade_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSPRIORIDADE_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV35TFContratoServicosPrioridade_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV38TFContratoServicosPrioridade_PercValorB) && (Convert.ToDecimal(0)==AV39TFContratoServicosPrioridade_PercValorB_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSPRIORIDADE_PERCVALORB";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV38TFContratoServicosPrioridade_PercValorB, 6, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV39TFContratoServicosPrioridade_PercValorB_To, 6, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV42TFContratoServicosPrioridade_PercPrazo) && (Convert.ToDecimal(0)==AV43TFContratoServicosPrioridade_PercPrazo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV42TFContratoServicosPrioridade_PercPrazo, 6, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV43TFContratoServicosPrioridade_PercPrazo_To, 6, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV54TFContratoServicosPrioridade_Peso) && (0==AV55TFContratoServicosPrioridade_Peso_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSPRIORIDADE_PESO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV54TFContratoServicosPrioridade_Peso), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV55TFContratoServicosPrioridade_Peso_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV79Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratoServicosPrioridade_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17ContratoServicosPrioridade_Nome1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContratoServicosPrioridade_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21ContratoServicosPrioridade_Nome2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoServicosPrioridade_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ContratoServicosPrioridade_Nome3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV79Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoServicosPrioridade";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_K12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_K12( true) ;
         }
         else
         {
            wb_table2_8_K12( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_K12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_66_K12( true) ;
         }
         else
         {
            wb_table3_66_K12( false) ;
         }
         return  ;
      }

      protected void wb_table3_66_K12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_K12e( true) ;
         }
         else
         {
            wb_table1_2_K12e( false) ;
         }
      }

      protected void wb_table3_66_K12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_69_K12( true) ;
         }
         else
         {
            wb_table4_69_K12( false) ;
         }
         return  ;
      }

      protected void wb_table4_69_K12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_66_K12e( true) ;
         }
         else
         {
            wb_table3_66_K12e( false) ;
         }
      }

      protected void wb_table4_69_K12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"72\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Servico") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(28), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrioridade_Ordem_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrioridade_Ordem_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrioridade_Ordem_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(260), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrioridade_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrioridade_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrioridade_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrioridade_PercValorB_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrioridade_PercValorB_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrioridade_PercValorB_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrioridade_PercPrazo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrioridade_PercPrazo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrioridade_PercPrazo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(34), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrioridade_Peso_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrioridade_Peso_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrioridade_Peso_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrioridade_Ordem_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrioridade_Ordem_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1337ContratoServicosPrioridade_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrioridade_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrioridade_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoServicosPrioridade_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1338ContratoServicosPrioridade_PercValorB, 8, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrioridade_PercValorB_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrioridade_PercValorB_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1339ContratoServicosPrioridade_PercPrazo, 8, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrioridade_PercPrazo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrioridade_PercPrazo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2067ContratoServicosPrioridade_Peso), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrioridade_Peso_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrioridade_Peso_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 72 )
         {
            wbEnd = 0;
            nRC_GXsfl_72 = (short)(nGXsfl_72_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_69_K12e( true) ;
         }
         else
         {
            wb_table4_69_K12e( false) ;
         }
      }

      protected void wb_table2_8_K12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicosprioridadetitle_Internalname, "Prioridades", "", "", lblContratoservicosprioridadetitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_K12( true) ;
         }
         else
         {
            wb_table5_13_K12( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_K12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_72_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_WWContratoServicosPrioridade.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_22_K12( true) ;
         }
         else
         {
            wb_table6_22_K12( false) ;
         }
         return  ;
      }

      protected void wb_table6_22_K12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_K12e( true) ;
         }
         else
         {
            wb_table2_8_K12e( false) ;
         }
      }

      protected void wb_table6_22_K12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_27_K12( true) ;
         }
         else
         {
            wb_table7_27_K12( false) ;
         }
         return  ;
      }

      protected void wb_table7_27_K12e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_22_K12e( true) ;
         }
         else
         {
            wb_table6_22_K12e( false) ;
         }
      }

      protected void wb_table7_27_K12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_72_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "", true, "HLP_WWContratoServicosPrioridade.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicosprioridade_nome1_Internalname, StringUtil.RTrim( AV17ContratoServicosPrioridade_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17ContratoServicosPrioridade_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicosprioridade_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicosprioridade_nome1_Visible, 1, 0, "text", "", 0, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosPrioridade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_72_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_WWContratoServicosPrioridade.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicosprioridade_nome2_Internalname, StringUtil.RTrim( AV21ContratoServicosPrioridade_Nome2), StringUtil.RTrim( context.localUtil.Format( AV21ContratoServicosPrioridade_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicosprioridade_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicosprioridade_nome2_Visible, 1, 0, "text", "", 0, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosPrioridade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_72_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_WWContratoServicosPrioridade.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicosprioridade_nome3_Internalname, StringUtil.RTrim( AV25ContratoServicosPrioridade_Nome3), StringUtil.RTrim( context.localUtil.Format( AV25ContratoServicosPrioridade_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicosprioridade_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicosprioridade_nome3_Visible, 1, 0, "text", "", 0, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_27_K12e( true) ;
         }
         else
         {
            wb_table7_27_K12e( false) ;
         }
      }

      protected void wb_table5_13_K12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_K12e( true) ;
         }
         else
         {
            wb_table5_13_K12e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAK12( ) ;
         WSK12( ) ;
         WEK12( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203118584124");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratoservicosprioridade.js", "?20203118584124");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_722( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_72_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_72_idx;
         edtContratoServicosPrioridade_Codigo_Internalname = "CONTRATOSERVICOSPRIORIDADE_CODIGO_"+sGXsfl_72_idx;
         edtContratoServicosPrioridade_CntSrvCod_Internalname = "CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD_"+sGXsfl_72_idx;
         edtContratoServicosPrioridade_Ordem_Internalname = "CONTRATOSERVICOSPRIORIDADE_ORDEM_"+sGXsfl_72_idx;
         edtContratoServicosPrioridade_Nome_Internalname = "CONTRATOSERVICOSPRIORIDADE_NOME_"+sGXsfl_72_idx;
         edtContratoServicosPrioridade_PercValorB_Internalname = "CONTRATOSERVICOSPRIORIDADE_PERCVALORB_"+sGXsfl_72_idx;
         edtContratoServicosPrioridade_PercPrazo_Internalname = "CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_"+sGXsfl_72_idx;
         edtContratoServicosPrioridade_Peso_Internalname = "CONTRATOSERVICOSPRIORIDADE_PESO_"+sGXsfl_72_idx;
      }

      protected void SubsflControlProps_fel_722( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_72_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_72_fel_idx;
         edtContratoServicosPrioridade_Codigo_Internalname = "CONTRATOSERVICOSPRIORIDADE_CODIGO_"+sGXsfl_72_fel_idx;
         edtContratoServicosPrioridade_CntSrvCod_Internalname = "CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD_"+sGXsfl_72_fel_idx;
         edtContratoServicosPrioridade_Ordem_Internalname = "CONTRATOSERVICOSPRIORIDADE_ORDEM_"+sGXsfl_72_fel_idx;
         edtContratoServicosPrioridade_Nome_Internalname = "CONTRATOSERVICOSPRIORIDADE_NOME_"+sGXsfl_72_fel_idx;
         edtContratoServicosPrioridade_PercValorB_Internalname = "CONTRATOSERVICOSPRIORIDADE_PERCVALORB_"+sGXsfl_72_fel_idx;
         edtContratoServicosPrioridade_PercPrazo_Internalname = "CONTRATOSERVICOSPRIORIDADE_PERCPRAZO_"+sGXsfl_72_fel_idx;
         edtContratoServicosPrioridade_Peso_Internalname = "CONTRATOSERVICOSPRIORIDADE_PESO_"+sGXsfl_72_fel_idx;
      }

      protected void sendrow_722( )
      {
         SubsflControlProps_722( ) ;
         WBK10( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_72_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_72_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_72_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV77Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV77Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV78Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV78Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrioridade_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1336ContratoServicosPrioridade_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrioridade_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)72,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrioridade_CntSrvCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrioridade_CntSrvCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)72,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrioridade_Ordem_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2066ContratoServicosPrioridade_Ordem), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A2066ContratoServicosPrioridade_Ordem), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrioridade_Ordem_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)28,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)72,(short)1,(short)-1,(short)0,(bool)true,(String)"Ordem",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrioridade_Nome_Internalname,StringUtil.RTrim( A1337ContratoServicosPrioridade_Nome),StringUtil.RTrim( context.localUtil.Format( A1337ContratoServicosPrioridade_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratoServicosPrioridade_Nome_Link,(String)"",(String)"",(String)"",(String)edtContratoServicosPrioridade_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)260,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)72,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrioridade_PercValorB_Internalname,StringUtil.LTrim( StringUtil.NToC( A1338ContratoServicosPrioridade_PercValorB, 8, 2, ",", "")),context.localUtil.Format( A1338ContratoServicosPrioridade_PercValorB, "ZZ9.99 %"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrioridade_PercValorB_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)72,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrioridade_PercPrazo_Internalname,StringUtil.LTrim( StringUtil.NToC( A1339ContratoServicosPrioridade_PercPrazo, 8, 2, ",", "")),context.localUtil.Format( A1339ContratoServicosPrioridade_PercPrazo, "ZZ9.99 %"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrioridade_PercPrazo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)72,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrioridade_Peso_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2067ContratoServicosPrioridade_Peso), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A2067ContratoServicosPrioridade_Peso), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrioridade_Peso_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)34,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)72,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_CODIGO"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sGXsfl_72_idx, context.localUtil.Format( (decimal)(A1336ContratoServicosPrioridade_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sGXsfl_72_idx, context.localUtil.Format( (decimal)(A1335ContratoServicosPrioridade_CntSrvCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_ORDEM"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sGXsfl_72_idx, context.localUtil.Format( (decimal)(A2066ContratoServicosPrioridade_Ordem), "ZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_NOME"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sGXsfl_72_idx, StringUtil.RTrim( context.localUtil.Format( A1337ContratoServicosPrioridade_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_PERCVALORB"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sGXsfl_72_idx, context.localUtil.Format( A1338ContratoServicosPrioridade_PercValorB, "ZZ9.99 %")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sGXsfl_72_idx, context.localUtil.Format( A1339ContratoServicosPrioridade_PercPrazo, "ZZ9.99 %")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRIORIDADE_PESO"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sGXsfl_72_idx, context.localUtil.Format( (decimal)(A2067ContratoServicosPrioridade_Peso), "ZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_72_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_72_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_72_idx+1));
            sGXsfl_72_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_72_idx), 4, 0)), 4, "0");
            SubsflControlProps_722( ) ;
         }
         /* End function sendrow_722 */
      }

      protected void init_default_properties( )
      {
         lblContratoservicosprioridadetitle_Internalname = "CONTRATOSERVICOSPRIORIDADETITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavContratoservicosprioridade_nome1_Internalname = "vCONTRATOSERVICOSPRIORIDADE_NOME1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavContratoservicosprioridade_nome2_Internalname = "vCONTRATOSERVICOSPRIORIDADE_NOME2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavContratoservicosprioridade_nome3_Internalname = "vCONTRATOSERVICOSPRIORIDADE_NOME3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContratoServicosPrioridade_Codigo_Internalname = "CONTRATOSERVICOSPRIORIDADE_CODIGO";
         edtContratoServicosPrioridade_CntSrvCod_Internalname = "CONTRATOSERVICOSPRIORIDADE_CNTSRVCOD";
         edtContratoServicosPrioridade_Ordem_Internalname = "CONTRATOSERVICOSPRIORIDADE_ORDEM";
         edtContratoServicosPrioridade_Nome_Internalname = "CONTRATOSERVICOSPRIORIDADE_NOME";
         edtContratoServicosPrioridade_PercValorB_Internalname = "CONTRATOSERVICOSPRIORIDADE_PERCVALORB";
         edtContratoServicosPrioridade_PercPrazo_Internalname = "CONTRATOSERVICOSPRIORIDADE_PERCPRAZO";
         edtContratoServicosPrioridade_Peso_Internalname = "CONTRATOSERVICOSPRIORIDADE_PESO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontratoservicosprioridade_ordem_Internalname = "vTFCONTRATOSERVICOSPRIORIDADE_ORDEM";
         edtavTfcontratoservicosprioridade_ordem_to_Internalname = "vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO";
         edtavTfcontratoservicosprioridade_nome_Internalname = "vTFCONTRATOSERVICOSPRIORIDADE_NOME";
         edtavTfcontratoservicosprioridade_nome_sel_Internalname = "vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL";
         edtavTfcontratoservicosprioridade_percvalorb_Internalname = "vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB";
         edtavTfcontratoservicosprioridade_percvalorb_to_Internalname = "vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO";
         edtavTfcontratoservicosprioridade_percprazo_Internalname = "vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO";
         edtavTfcontratoservicosprioridade_percprazo_to_Internalname = "vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO";
         edtavTfcontratoservicosprioridade_peso_Internalname = "vTFCONTRATOSERVICOSPRIORIDADE_PESO";
         edtavTfcontratoservicosprioridade_peso_to_Internalname = "vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO";
         Ddo_contratoservicosprioridade_ordem_Internalname = "DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM";
         edtavDdo_contratoservicosprioridade_ordemtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSPRIORIDADE_ORDEMTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosprioridade_nome_Internalname = "DDO_CONTRATOSERVICOSPRIORIDADE_NOME";
         edtavDdo_contratoservicosprioridade_nometitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSPRIORIDADE_NOMETITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosprioridade_percvalorb_Internalname = "DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB";
         edtavDdo_contratoservicosprioridade_percvalorbtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosprioridade_percprazo_Internalname = "DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO";
         edtavDdo_contratoservicosprioridade_percprazotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosprioridade_peso_Internalname = "DDO_CONTRATOSERVICOSPRIORIDADE_PESO";
         edtavDdo_contratoservicosprioridade_pesotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSPRIORIDADE_PESOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoServicosPrioridade_Peso_Jsonclick = "";
         edtContratoServicosPrioridade_PercPrazo_Jsonclick = "";
         edtContratoServicosPrioridade_PercValorB_Jsonclick = "";
         edtContratoServicosPrioridade_Nome_Jsonclick = "";
         edtContratoServicosPrioridade_Ordem_Jsonclick = "";
         edtContratoServicosPrioridade_CntSrvCod_Jsonclick = "";
         edtContratoServicosPrioridade_Codigo_Jsonclick = "";
         edtavContratoservicosprioridade_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavContratoservicosprioridade_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavContratoservicosprioridade_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoServicosPrioridade_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContratoServicosPrioridade_Peso_Titleformat = 0;
         edtContratoServicosPrioridade_PercPrazo_Titleformat = 0;
         edtContratoServicosPrioridade_PercValorB_Titleformat = 0;
         edtContratoServicosPrioridade_Nome_Titleformat = 0;
         edtContratoServicosPrioridade_Ordem_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavContratoservicosprioridade_nome3_Visible = 1;
         edtavContratoservicosprioridade_nome2_Visible = 1;
         edtavContratoservicosprioridade_nome1_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtContratoServicosPrioridade_Peso_Title = "Peso";
         edtContratoServicosPrioridade_PercPrazo_Title = "% do Prazo";
         edtContratoServicosPrioridade_PercValorB_Title = "% do Valor B";
         edtContratoServicosPrioridade_Nome_Title = "Nome";
         edtContratoServicosPrioridade_Ordem_Title = "";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratoservicosprioridade_pesotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosprioridade_percprazotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosprioridade_percvalorbtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosprioridade_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosprioridade_ordemtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicosprioridade_peso_to_Jsonclick = "";
         edtavTfcontratoservicosprioridade_peso_to_Visible = 1;
         edtavTfcontratoservicosprioridade_peso_Jsonclick = "";
         edtavTfcontratoservicosprioridade_peso_Visible = 1;
         edtavTfcontratoservicosprioridade_percprazo_to_Jsonclick = "";
         edtavTfcontratoservicosprioridade_percprazo_to_Visible = 1;
         edtavTfcontratoservicosprioridade_percprazo_Jsonclick = "";
         edtavTfcontratoservicosprioridade_percprazo_Visible = 1;
         edtavTfcontratoservicosprioridade_percvalorb_to_Jsonclick = "";
         edtavTfcontratoservicosprioridade_percvalorb_to_Visible = 1;
         edtavTfcontratoservicosprioridade_percvalorb_Jsonclick = "";
         edtavTfcontratoservicosprioridade_percvalorb_Visible = 1;
         edtavTfcontratoservicosprioridade_nome_sel_Jsonclick = "";
         edtavTfcontratoservicosprioridade_nome_sel_Visible = 1;
         edtavTfcontratoservicosprioridade_nome_Jsonclick = "";
         edtavTfcontratoservicosprioridade_nome_Visible = 1;
         edtavTfcontratoservicosprioridade_ordem_to_Jsonclick = "";
         edtavTfcontratoservicosprioridade_ordem_to_Visible = 1;
         edtavTfcontratoservicosprioridade_ordem_Jsonclick = "";
         edtavTfcontratoservicosprioridade_ordem_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratoservicosprioridade_peso_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprioridade_peso_Rangefilterto = "At�";
         Ddo_contratoservicosprioridade_peso_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprioridade_peso_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprioridade_peso_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprioridade_peso_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprioridade_peso_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprioridade_peso_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_peso_Filtertype = "Numeric";
         Ddo_contratoservicosprioridade_peso_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_peso_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_peso_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_peso_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprioridade_peso_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprioridade_peso_Cls = "ColumnSettings";
         Ddo_contratoservicosprioridade_peso_Tooltip = "Op��es";
         Ddo_contratoservicosprioridade_peso_Caption = "";
         Ddo_contratoservicosprioridade_percprazo_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprioridade_percprazo_Rangefilterto = "At�";
         Ddo_contratoservicosprioridade_percprazo_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprioridade_percprazo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprioridade_percprazo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprioridade_percprazo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprioridade_percprazo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprioridade_percprazo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_percprazo_Filtertype = "Numeric";
         Ddo_contratoservicosprioridade_percprazo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_percprazo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_percprazo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_percprazo_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprioridade_percprazo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprioridade_percprazo_Cls = "ColumnSettings";
         Ddo_contratoservicosprioridade_percprazo_Tooltip = "Op��es";
         Ddo_contratoservicosprioridade_percprazo_Caption = "";
         Ddo_contratoservicosprioridade_percvalorb_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprioridade_percvalorb_Rangefilterto = "At�";
         Ddo_contratoservicosprioridade_percvalorb_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprioridade_percvalorb_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprioridade_percvalorb_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprioridade_percvalorb_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprioridade_percvalorb_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprioridade_percvalorb_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_percvalorb_Filtertype = "Numeric";
         Ddo_contratoservicosprioridade_percvalorb_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_percvalorb_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_percvalorb_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_percvalorb_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprioridade_percvalorb_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprioridade_percvalorb_Cls = "ColumnSettings";
         Ddo_contratoservicosprioridade_percvalorb_Tooltip = "Op��es";
         Ddo_contratoservicosprioridade_percvalorb_Caption = "";
         Ddo_contratoservicosprioridade_nome_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprioridade_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicosprioridade_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprioridade_nome_Loadingdata = "Carregando dados...";
         Ddo_contratoservicosprioridade_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprioridade_nome_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprioridade_nome_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicosprioridade_nome_Datalistproc = "GetWWContratoServicosPrioridadeFilterData";
         Ddo_contratoservicosprioridade_nome_Datalisttype = "Dynamic";
         Ddo_contratoservicosprioridade_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicosprioridade_nome_Filtertype = "Character";
         Ddo_contratoservicosprioridade_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_nome_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprioridade_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprioridade_nome_Cls = "ColumnSettings";
         Ddo_contratoservicosprioridade_nome_Tooltip = "Op��es";
         Ddo_contratoservicosprioridade_nome_Caption = "";
         Ddo_contratoservicosprioridade_ordem_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprioridade_ordem_Rangefilterto = "At�";
         Ddo_contratoservicosprioridade_ordem_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprioridade_ordem_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprioridade_ordem_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprioridade_ordem_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprioridade_ordem_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprioridade_ordem_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_ordem_Filtertype = "Numeric";
         Ddo_contratoservicosprioridade_ordem_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_ordem_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_ordem_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprioridade_ordem_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprioridade_ordem_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprioridade_ordem_Cls = "ColumnSettings";
         Ddo_contratoservicosprioridade_ordem_Tooltip = "Op��es";
         Ddo_contratoservicosprioridade_ordem_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Prioridade";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PESOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV49ContratoServicosPrioridade_OrdemTitleFilterData',fld:'vCONTRATOSERVICOSPRIORIDADE_ORDEMTITLEFILTERDATA',pic:'',nv:null},{av:'AV33ContratoServicosPrioridade_NomeTitleFilterData',fld:'vCONTRATOSERVICOSPRIORIDADE_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV37ContratoServicosPrioridade_PercValorBTitleFilterData',fld:'vCONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLEFILTERDATA',pic:'',nv:null},{av:'AV41ContratoServicosPrioridade_PercPrazoTitleFilterData',fld:'vCONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV53ContratoServicosPrioridade_PesoTitleFilterData',fld:'vCONTRATOSERVICOSPRIORIDADE_PESOTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicosPrioridade_Ordem_Titleformat',ctrl:'CONTRATOSERVICOSPRIORIDADE_ORDEM',prop:'Titleformat'},{av:'edtContratoServicosPrioridade_Ordem_Title',ctrl:'CONTRATOSERVICOSPRIORIDADE_ORDEM',prop:'Title'},{av:'edtContratoServicosPrioridade_Nome_Titleformat',ctrl:'CONTRATOSERVICOSPRIORIDADE_NOME',prop:'Titleformat'},{av:'edtContratoServicosPrioridade_Nome_Title',ctrl:'CONTRATOSERVICOSPRIORIDADE_NOME',prop:'Title'},{av:'edtContratoServicosPrioridade_PercValorB_Titleformat',ctrl:'CONTRATOSERVICOSPRIORIDADE_PERCVALORB',prop:'Titleformat'},{av:'edtContratoServicosPrioridade_PercValorB_Title',ctrl:'CONTRATOSERVICOSPRIORIDADE_PERCVALORB',prop:'Title'},{av:'edtContratoServicosPrioridade_PercPrazo_Titleformat',ctrl:'CONTRATOSERVICOSPRIORIDADE_PERCPRAZO',prop:'Titleformat'},{av:'edtContratoServicosPrioridade_PercPrazo_Title',ctrl:'CONTRATOSERVICOSPRIORIDADE_PERCPRAZO',prop:'Title'},{av:'edtContratoServicosPrioridade_Peso_Titleformat',ctrl:'CONTRATOSERVICOSPRIORIDADE_PESO',prop:'Titleformat'},{av:'edtContratoServicosPrioridade_Peso_Title',ctrl:'CONTRATOSERVICOSPRIORIDADE_PESO',prop:'Title'},{av:'AV47GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV48GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11K12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PESOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM.ONOPTIONCLICKED","{handler:'E12K12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PESOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoservicosprioridade_ordem_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprioridade_ordem_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprioridade_ordem_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprioridade_ordem_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM',prop:'SortedStatus'},{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'Ddo_contratoservicosprioridade_nome_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_NOME',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_percvalorb_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_percprazo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_peso_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PESO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRIORIDADE_NOME.ONOPTIONCLICKED","{handler:'E13K12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PESOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoservicosprioridade_nome_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_NOME',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprioridade_nome_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_NOME',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprioridade_nome_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprioridade_nome_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_NOME',prop:'SortedStatus'},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_contratoservicosprioridade_ordem_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_percvalorb_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_percprazo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_peso_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PESO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB.ONOPTIONCLICKED","{handler:'E14K12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PESOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoservicosprioridade_percvalorb_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprioridade_percvalorb_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprioridade_percvalorb_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB',prop:'SortedStatus'},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'Ddo_contratoservicosprioridade_ordem_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_nome_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_NOME',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_percprazo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_peso_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PESO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO.ONOPTIONCLICKED","{handler:'E15K12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PESOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoservicosprioridade_percprazo_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprioridade_percprazo_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprioridade_percprazo_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprioridade_percprazo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO',prop:'SortedStatus'},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'Ddo_contratoservicosprioridade_ordem_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_nome_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_NOME',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_percvalorb_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_peso_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PESO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRIORIDADE_PESO.ONOPTIONCLICKED","{handler:'E16K12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PESOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoservicosprioridade_peso_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PESO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprioridade_peso_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PESO',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprioridade_peso_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PESO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprioridade_peso_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PESO',prop:'SortedStatus'},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosprioridade_ordem_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_nome_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_NOME',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_percvalorb_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB',prop:'SortedStatus'},{av:'Ddo_contratoservicosprioridade_percprazo_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E29K12',iparms:[{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtContratoServicosPrioridade_Nome_Link',ctrl:'CONTRATOSERVICOSPRIORIDADE_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E17K12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PESOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E22K12',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E18K12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PESOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosprioridade_nome2_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME2',prop:'Visible'},{av:'edtavContratoservicosprioridade_nome3_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME3',prop:'Visible'},{av:'edtavContratoservicosprioridade_nome1_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E23K12',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosprioridade_nome1_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E24K12',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E19K12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PESOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosprioridade_nome2_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME2',prop:'Visible'},{av:'edtavContratoservicosprioridade_nome3_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME3',prop:'Visible'},{av:'edtavContratoservicosprioridade_nome1_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E25K12',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosprioridade_nome2_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E20K12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PESOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosprioridade_nome2_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME2',prop:'Visible'},{av:'edtavContratoservicosprioridade_nome3_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME3',prop:'Visible'},{av:'edtavContratoservicosprioridade_nome1_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E26K12',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosprioridade_nome3_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21K12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_ORDEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORBTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRIORIDADE_PESOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV50TFContratoServicosPrioridade_Ordem',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM',pic:'ZZ9',nv:0},{av:'Ddo_contratoservicosprioridade_ordem_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM',prop:'FilteredText_set'},{av:'AV51TFContratoServicosPrioridade_Ordem_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_ORDEM_TO',pic:'ZZ9',nv:0},{av:'Ddo_contratoservicosprioridade_ordem_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_ORDEM',prop:'FilteredTextTo_set'},{av:'AV34TFContratoServicosPrioridade_Nome',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'Ddo_contratoservicosprioridade_nome_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_NOME',prop:'FilteredText_set'},{av:'AV35TFContratoServicosPrioridade_Nome_Sel',fld:'vTFCONTRATOSERVICOSPRIORIDADE_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_contratoservicosprioridade_nome_Selectedvalue_set',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_NOME',prop:'SelectedValue_set'},{av:'AV38TFContratoServicosPrioridade_PercValorB',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB',pic:'ZZ9.99 %',nv:0.0},{av:'Ddo_contratoservicosprioridade_percvalorb_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB',prop:'FilteredText_set'},{av:'AV39TFContratoServicosPrioridade_PercValorB_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCVALORB_TO',pic:'ZZ9.99 %',nv:0.0},{av:'Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCVALORB',prop:'FilteredTextTo_set'},{av:'AV42TFContratoServicosPrioridade_PercPrazo',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO',pic:'ZZ9.99 %',nv:0.0},{av:'Ddo_contratoservicosprioridade_percprazo_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO',prop:'FilteredText_set'},{av:'AV43TFContratoServicosPrioridade_PercPrazo_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PERCPRAZO_TO',pic:'ZZ9.99 %',nv:0.0},{av:'Ddo_contratoservicosprioridade_percprazo_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PERCPRAZO',prop:'FilteredTextTo_set'},{av:'AV54TFContratoServicosPrioridade_Peso',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosprioridade_peso_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PESO',prop:'FilteredText_set'},{av:'AV55TFContratoServicosPrioridade_Peso_To',fld:'vTFCONTRATOSERVICOSPRIORIDADE_PESO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosprioridade_peso_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSPRIORIDADE_PESO',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoServicosPrioridade_Nome1',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContratoservicosprioridade_nome1_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoServicosPrioridade_Nome2',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoServicosPrioridade_Nome3',fld:'vCONTRATOSERVICOSPRIORIDADE_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosprioridade_nome2_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME2',prop:'Visible'},{av:'edtavContratoservicosprioridade_nome3_Visible',ctrl:'vCONTRATOSERVICOSPRIORIDADE_NOME3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoservicosprioridade_ordem_Activeeventkey = "";
         Ddo_contratoservicosprioridade_ordem_Filteredtext_get = "";
         Ddo_contratoservicosprioridade_ordem_Filteredtextto_get = "";
         Ddo_contratoservicosprioridade_nome_Activeeventkey = "";
         Ddo_contratoservicosprioridade_nome_Filteredtext_get = "";
         Ddo_contratoservicosprioridade_nome_Selectedvalue_get = "";
         Ddo_contratoservicosprioridade_percvalorb_Activeeventkey = "";
         Ddo_contratoservicosprioridade_percvalorb_Filteredtext_get = "";
         Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_get = "";
         Ddo_contratoservicosprioridade_percprazo_Activeeventkey = "";
         Ddo_contratoservicosprioridade_percprazo_Filteredtext_get = "";
         Ddo_contratoservicosprioridade_percprazo_Filteredtextto_get = "";
         Ddo_contratoservicosprioridade_peso_Activeeventkey = "";
         Ddo_contratoservicosprioridade_peso_Filteredtext_get = "";
         Ddo_contratoservicosprioridade_peso_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ContratoServicosPrioridade_Nome1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21ContratoServicosPrioridade_Nome2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25ContratoServicosPrioridade_Nome3 = "";
         AV34TFContratoServicosPrioridade_Nome = "";
         AV35TFContratoServicosPrioridade_Nome_Sel = "";
         AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace = "";
         AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace = "";
         AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace = "";
         AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace = "";
         AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace = "";
         AV79Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV45DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV49ContratoServicosPrioridade_OrdemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV33ContratoServicosPrioridade_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37ContratoServicosPrioridade_PercValorBTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41ContratoServicosPrioridade_PercPrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53ContratoServicosPrioridade_PesoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoservicosprioridade_ordem_Filteredtext_set = "";
         Ddo_contratoservicosprioridade_ordem_Filteredtextto_set = "";
         Ddo_contratoservicosprioridade_ordem_Sortedstatus = "";
         Ddo_contratoservicosprioridade_nome_Filteredtext_set = "";
         Ddo_contratoservicosprioridade_nome_Selectedvalue_set = "";
         Ddo_contratoservicosprioridade_nome_Sortedstatus = "";
         Ddo_contratoservicosprioridade_percvalorb_Filteredtext_set = "";
         Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_set = "";
         Ddo_contratoservicosprioridade_percvalorb_Sortedstatus = "";
         Ddo_contratoservicosprioridade_percprazo_Filteredtext_set = "";
         Ddo_contratoservicosprioridade_percprazo_Filteredtextto_set = "";
         Ddo_contratoservicosprioridade_percprazo_Sortedstatus = "";
         Ddo_contratoservicosprioridade_peso_Filteredtext_set = "";
         Ddo_contratoservicosprioridade_peso_Filteredtextto_set = "";
         Ddo_contratoservicosprioridade_peso_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV77Update_GXI = "";
         AV29Delete = "";
         AV78Delete_GXI = "";
         A1337ContratoServicosPrioridade_Nome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = "";
         lV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = "";
         lV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = "";
         lV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = "";
         AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 = "";
         AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 = "";
         AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 = "";
         AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 = "";
         AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 = "";
         AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 = "";
         AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel = "";
         AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome = "";
         H00K12_A2067ContratoServicosPrioridade_Peso = new short[1] ;
         H00K12_n2067ContratoServicosPrioridade_Peso = new bool[] {false} ;
         H00K12_A1339ContratoServicosPrioridade_PercPrazo = new decimal[1] ;
         H00K12_n1339ContratoServicosPrioridade_PercPrazo = new bool[] {false} ;
         H00K12_A1338ContratoServicosPrioridade_PercValorB = new decimal[1] ;
         H00K12_n1338ContratoServicosPrioridade_PercValorB = new bool[] {false} ;
         H00K12_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         H00K12_A2066ContratoServicosPrioridade_Ordem = new short[1] ;
         H00K12_n2066ContratoServicosPrioridade_Ordem = new bool[] {false} ;
         H00K12_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         H00K12_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         H00K13_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratoservicosprioridadetitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratoservicosprioridade__default(),
            new Object[][] {
                new Object[] {
               H00K12_A2067ContratoServicosPrioridade_Peso, H00K12_n2067ContratoServicosPrioridade_Peso, H00K12_A1339ContratoServicosPrioridade_PercPrazo, H00K12_n1339ContratoServicosPrioridade_PercPrazo, H00K12_A1338ContratoServicosPrioridade_PercValorB, H00K12_n1338ContratoServicosPrioridade_PercValorB, H00K12_A1337ContratoServicosPrioridade_Nome, H00K12_A2066ContratoServicosPrioridade_Ordem, H00K12_n2066ContratoServicosPrioridade_Ordem, H00K12_A1335ContratoServicosPrioridade_CntSrvCod,
               H00K12_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               H00K13_AGRID_nRecordCount
               }
            }
         );
         AV79Pgmname = "WWContratoServicosPrioridade";
         /* GeneXus formulas. */
         AV79Pgmname = "WWContratoServicosPrioridade";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_72 ;
      private short nGXsfl_72_idx=1 ;
      private short AV13OrderedBy ;
      private short AV50TFContratoServicosPrioridade_Ordem ;
      private short AV51TFContratoServicosPrioridade_Ordem_To ;
      private short AV54TFContratoServicosPrioridade_Peso ;
      private short AV55TFContratoServicosPrioridade_Peso_To ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A2066ContratoServicosPrioridade_Ordem ;
      private short A2067ContratoServicosPrioridade_Peso ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_72_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem ;
      private short AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to ;
      private short AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso ;
      private short AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to ;
      private short edtContratoServicosPrioridade_Ordem_Titleformat ;
      private short edtContratoServicosPrioridade_Nome_Titleformat ;
      private short edtContratoServicosPrioridade_PercValorB_Titleformat ;
      private short edtContratoServicosPrioridade_PercPrazo_Titleformat ;
      private short edtContratoServicosPrioridade_Peso_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A1336ContratoServicosPrioridade_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratoservicosprioridade_nome_Datalistupdateminimumcharacters ;
      private int edtavTfcontratoservicosprioridade_ordem_Visible ;
      private int edtavTfcontratoservicosprioridade_ordem_to_Visible ;
      private int edtavTfcontratoservicosprioridade_nome_Visible ;
      private int edtavTfcontratoservicosprioridade_nome_sel_Visible ;
      private int edtavTfcontratoservicosprioridade_percvalorb_Visible ;
      private int edtavTfcontratoservicosprioridade_percvalorb_to_Visible ;
      private int edtavTfcontratoservicosprioridade_percprazo_Visible ;
      private int edtavTfcontratoservicosprioridade_percprazo_to_Visible ;
      private int edtavTfcontratoservicosprioridade_peso_Visible ;
      private int edtavTfcontratoservicosprioridade_peso_to_Visible ;
      private int edtavDdo_contratoservicosprioridade_ordemtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosprioridade_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosprioridade_percvalorbtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosprioridade_percprazotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosprioridade_pesotitlecontrolidtoreplace_Visible ;
      private int A1335ContratoServicosPrioridade_CntSrvCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int AV46PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContratoservicosprioridade_nome1_Visible ;
      private int edtavContratoservicosprioridade_nome2_Visible ;
      private int edtavContratoservicosprioridade_nome3_Visible ;
      private int AV80GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV47GridCurrentPage ;
      private long AV48GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV38TFContratoServicosPrioridade_PercValorB ;
      private decimal AV39TFContratoServicosPrioridade_PercValorB_To ;
      private decimal AV42TFContratoServicosPrioridade_PercPrazo ;
      private decimal AV43TFContratoServicosPrioridade_PercPrazo_To ;
      private decimal A1338ContratoServicosPrioridade_PercValorB ;
      private decimal A1339ContratoServicosPrioridade_PercPrazo ;
      private decimal AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb ;
      private decimal AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to ;
      private decimal AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo ;
      private decimal AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoservicosprioridade_ordem_Activeeventkey ;
      private String Ddo_contratoservicosprioridade_ordem_Filteredtext_get ;
      private String Ddo_contratoservicosprioridade_ordem_Filteredtextto_get ;
      private String Ddo_contratoservicosprioridade_nome_Activeeventkey ;
      private String Ddo_contratoservicosprioridade_nome_Filteredtext_get ;
      private String Ddo_contratoservicosprioridade_nome_Selectedvalue_get ;
      private String Ddo_contratoservicosprioridade_percvalorb_Activeeventkey ;
      private String Ddo_contratoservicosprioridade_percvalorb_Filteredtext_get ;
      private String Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_get ;
      private String Ddo_contratoservicosprioridade_percprazo_Activeeventkey ;
      private String Ddo_contratoservicosprioridade_percprazo_Filteredtext_get ;
      private String Ddo_contratoservicosprioridade_percprazo_Filteredtextto_get ;
      private String Ddo_contratoservicosprioridade_peso_Activeeventkey ;
      private String Ddo_contratoservicosprioridade_peso_Filteredtext_get ;
      private String Ddo_contratoservicosprioridade_peso_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_72_idx="0001" ;
      private String AV17ContratoServicosPrioridade_Nome1 ;
      private String AV21ContratoServicosPrioridade_Nome2 ;
      private String AV25ContratoServicosPrioridade_Nome3 ;
      private String AV34TFContratoServicosPrioridade_Nome ;
      private String AV35TFContratoServicosPrioridade_Nome_Sel ;
      private String AV79Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoservicosprioridade_ordem_Caption ;
      private String Ddo_contratoservicosprioridade_ordem_Tooltip ;
      private String Ddo_contratoservicosprioridade_ordem_Cls ;
      private String Ddo_contratoservicosprioridade_ordem_Filteredtext_set ;
      private String Ddo_contratoservicosprioridade_ordem_Filteredtextto_set ;
      private String Ddo_contratoservicosprioridade_ordem_Dropdownoptionstype ;
      private String Ddo_contratoservicosprioridade_ordem_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprioridade_ordem_Sortedstatus ;
      private String Ddo_contratoservicosprioridade_ordem_Filtertype ;
      private String Ddo_contratoservicosprioridade_ordem_Sortasc ;
      private String Ddo_contratoservicosprioridade_ordem_Sortdsc ;
      private String Ddo_contratoservicosprioridade_ordem_Cleanfilter ;
      private String Ddo_contratoservicosprioridade_ordem_Rangefilterfrom ;
      private String Ddo_contratoservicosprioridade_ordem_Rangefilterto ;
      private String Ddo_contratoservicosprioridade_ordem_Searchbuttontext ;
      private String Ddo_contratoservicosprioridade_nome_Caption ;
      private String Ddo_contratoservicosprioridade_nome_Tooltip ;
      private String Ddo_contratoservicosprioridade_nome_Cls ;
      private String Ddo_contratoservicosprioridade_nome_Filteredtext_set ;
      private String Ddo_contratoservicosprioridade_nome_Selectedvalue_set ;
      private String Ddo_contratoservicosprioridade_nome_Dropdownoptionstype ;
      private String Ddo_contratoservicosprioridade_nome_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprioridade_nome_Sortedstatus ;
      private String Ddo_contratoservicosprioridade_nome_Filtertype ;
      private String Ddo_contratoservicosprioridade_nome_Datalisttype ;
      private String Ddo_contratoservicosprioridade_nome_Datalistproc ;
      private String Ddo_contratoservicosprioridade_nome_Sortasc ;
      private String Ddo_contratoservicosprioridade_nome_Sortdsc ;
      private String Ddo_contratoservicosprioridade_nome_Loadingdata ;
      private String Ddo_contratoservicosprioridade_nome_Cleanfilter ;
      private String Ddo_contratoservicosprioridade_nome_Noresultsfound ;
      private String Ddo_contratoservicosprioridade_nome_Searchbuttontext ;
      private String Ddo_contratoservicosprioridade_percvalorb_Caption ;
      private String Ddo_contratoservicosprioridade_percvalorb_Tooltip ;
      private String Ddo_contratoservicosprioridade_percvalorb_Cls ;
      private String Ddo_contratoservicosprioridade_percvalorb_Filteredtext_set ;
      private String Ddo_contratoservicosprioridade_percvalorb_Filteredtextto_set ;
      private String Ddo_contratoservicosprioridade_percvalorb_Dropdownoptionstype ;
      private String Ddo_contratoservicosprioridade_percvalorb_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprioridade_percvalorb_Sortedstatus ;
      private String Ddo_contratoservicosprioridade_percvalorb_Filtertype ;
      private String Ddo_contratoservicosprioridade_percvalorb_Sortasc ;
      private String Ddo_contratoservicosprioridade_percvalorb_Sortdsc ;
      private String Ddo_contratoservicosprioridade_percvalorb_Cleanfilter ;
      private String Ddo_contratoservicosprioridade_percvalorb_Rangefilterfrom ;
      private String Ddo_contratoservicosprioridade_percvalorb_Rangefilterto ;
      private String Ddo_contratoservicosprioridade_percvalorb_Searchbuttontext ;
      private String Ddo_contratoservicosprioridade_percprazo_Caption ;
      private String Ddo_contratoservicosprioridade_percprazo_Tooltip ;
      private String Ddo_contratoservicosprioridade_percprazo_Cls ;
      private String Ddo_contratoservicosprioridade_percprazo_Filteredtext_set ;
      private String Ddo_contratoservicosprioridade_percprazo_Filteredtextto_set ;
      private String Ddo_contratoservicosprioridade_percprazo_Dropdownoptionstype ;
      private String Ddo_contratoservicosprioridade_percprazo_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprioridade_percprazo_Sortedstatus ;
      private String Ddo_contratoservicosprioridade_percprazo_Filtertype ;
      private String Ddo_contratoservicosprioridade_percprazo_Sortasc ;
      private String Ddo_contratoservicosprioridade_percprazo_Sortdsc ;
      private String Ddo_contratoservicosprioridade_percprazo_Cleanfilter ;
      private String Ddo_contratoservicosprioridade_percprazo_Rangefilterfrom ;
      private String Ddo_contratoservicosprioridade_percprazo_Rangefilterto ;
      private String Ddo_contratoservicosprioridade_percprazo_Searchbuttontext ;
      private String Ddo_contratoservicosprioridade_peso_Caption ;
      private String Ddo_contratoservicosprioridade_peso_Tooltip ;
      private String Ddo_contratoservicosprioridade_peso_Cls ;
      private String Ddo_contratoservicosprioridade_peso_Filteredtext_set ;
      private String Ddo_contratoservicosprioridade_peso_Filteredtextto_set ;
      private String Ddo_contratoservicosprioridade_peso_Dropdownoptionstype ;
      private String Ddo_contratoservicosprioridade_peso_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprioridade_peso_Sortedstatus ;
      private String Ddo_contratoservicosprioridade_peso_Filtertype ;
      private String Ddo_contratoservicosprioridade_peso_Sortasc ;
      private String Ddo_contratoservicosprioridade_peso_Sortdsc ;
      private String Ddo_contratoservicosprioridade_peso_Cleanfilter ;
      private String Ddo_contratoservicosprioridade_peso_Rangefilterfrom ;
      private String Ddo_contratoservicosprioridade_peso_Rangefilterto ;
      private String Ddo_contratoservicosprioridade_peso_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontratoservicosprioridade_ordem_Internalname ;
      private String edtavTfcontratoservicosprioridade_ordem_Jsonclick ;
      private String edtavTfcontratoservicosprioridade_ordem_to_Internalname ;
      private String edtavTfcontratoservicosprioridade_ordem_to_Jsonclick ;
      private String edtavTfcontratoservicosprioridade_nome_Internalname ;
      private String edtavTfcontratoservicosprioridade_nome_Jsonclick ;
      private String edtavTfcontratoservicosprioridade_nome_sel_Internalname ;
      private String edtavTfcontratoservicosprioridade_nome_sel_Jsonclick ;
      private String edtavTfcontratoservicosprioridade_percvalorb_Internalname ;
      private String edtavTfcontratoservicosprioridade_percvalorb_Jsonclick ;
      private String edtavTfcontratoservicosprioridade_percvalorb_to_Internalname ;
      private String edtavTfcontratoservicosprioridade_percvalorb_to_Jsonclick ;
      private String edtavTfcontratoservicosprioridade_percprazo_Internalname ;
      private String edtavTfcontratoservicosprioridade_percprazo_Jsonclick ;
      private String edtavTfcontratoservicosprioridade_percprazo_to_Internalname ;
      private String edtavTfcontratoservicosprioridade_percprazo_to_Jsonclick ;
      private String edtavTfcontratoservicosprioridade_peso_Internalname ;
      private String edtavTfcontratoservicosprioridade_peso_Jsonclick ;
      private String edtavTfcontratoservicosprioridade_peso_to_Internalname ;
      private String edtavTfcontratoservicosprioridade_peso_to_Jsonclick ;
      private String edtavDdo_contratoservicosprioridade_ordemtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosprioridade_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosprioridade_percvalorbtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosprioridade_percprazotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosprioridade_pesotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContratoServicosPrioridade_Codigo_Internalname ;
      private String edtContratoServicosPrioridade_CntSrvCod_Internalname ;
      private String edtContratoServicosPrioridade_Ordem_Internalname ;
      private String A1337ContratoServicosPrioridade_Nome ;
      private String edtContratoServicosPrioridade_Nome_Internalname ;
      private String edtContratoServicosPrioridade_PercValorB_Internalname ;
      private String edtContratoServicosPrioridade_PercPrazo_Internalname ;
      private String edtContratoServicosPrioridade_Peso_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 ;
      private String lV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 ;
      private String lV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 ;
      private String lV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome ;
      private String AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 ;
      private String AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 ;
      private String AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 ;
      private String AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel ;
      private String AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContratoservicosprioridade_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavContratoservicosprioridade_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavContratoservicosprioridade_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratoservicosprioridade_ordem_Internalname ;
      private String Ddo_contratoservicosprioridade_nome_Internalname ;
      private String Ddo_contratoservicosprioridade_percvalorb_Internalname ;
      private String Ddo_contratoservicosprioridade_percprazo_Internalname ;
      private String Ddo_contratoservicosprioridade_peso_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratoServicosPrioridade_Ordem_Title ;
      private String edtContratoServicosPrioridade_Nome_Title ;
      private String edtContratoServicosPrioridade_PercValorB_Title ;
      private String edtContratoServicosPrioridade_PercPrazo_Title ;
      private String edtContratoServicosPrioridade_Peso_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContratoServicosPrioridade_Nome_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContratoservicosprioridadetitle_Internalname ;
      private String lblContratoservicosprioridadetitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavContratoservicosprioridade_nome1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavContratoservicosprioridade_nome2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavContratoservicosprioridade_nome3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_72_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoServicosPrioridade_Codigo_Jsonclick ;
      private String edtContratoServicosPrioridade_CntSrvCod_Jsonclick ;
      private String edtContratoServicosPrioridade_Ordem_Jsonclick ;
      private String edtContratoServicosPrioridade_Nome_Jsonclick ;
      private String edtContratoServicosPrioridade_PercValorB_Jsonclick ;
      private String edtContratoServicosPrioridade_PercPrazo_Jsonclick ;
      private String edtContratoServicosPrioridade_Peso_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoservicosprioridade_ordem_Includesortasc ;
      private bool Ddo_contratoservicosprioridade_ordem_Includesortdsc ;
      private bool Ddo_contratoservicosprioridade_ordem_Includefilter ;
      private bool Ddo_contratoservicosprioridade_ordem_Filterisrange ;
      private bool Ddo_contratoservicosprioridade_ordem_Includedatalist ;
      private bool Ddo_contratoservicosprioridade_nome_Includesortasc ;
      private bool Ddo_contratoservicosprioridade_nome_Includesortdsc ;
      private bool Ddo_contratoservicosprioridade_nome_Includefilter ;
      private bool Ddo_contratoservicosprioridade_nome_Filterisrange ;
      private bool Ddo_contratoservicosprioridade_nome_Includedatalist ;
      private bool Ddo_contratoservicosprioridade_percvalorb_Includesortasc ;
      private bool Ddo_contratoservicosprioridade_percvalorb_Includesortdsc ;
      private bool Ddo_contratoservicosprioridade_percvalorb_Includefilter ;
      private bool Ddo_contratoservicosprioridade_percvalorb_Filterisrange ;
      private bool Ddo_contratoservicosprioridade_percvalorb_Includedatalist ;
      private bool Ddo_contratoservicosprioridade_percprazo_Includesortasc ;
      private bool Ddo_contratoservicosprioridade_percprazo_Includesortdsc ;
      private bool Ddo_contratoservicosprioridade_percprazo_Includefilter ;
      private bool Ddo_contratoservicosprioridade_percprazo_Filterisrange ;
      private bool Ddo_contratoservicosprioridade_percprazo_Includedatalist ;
      private bool Ddo_contratoservicosprioridade_peso_Includesortasc ;
      private bool Ddo_contratoservicosprioridade_peso_Includesortdsc ;
      private bool Ddo_contratoservicosprioridade_peso_Includefilter ;
      private bool Ddo_contratoservicosprioridade_peso_Filterisrange ;
      private bool Ddo_contratoservicosprioridade_peso_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2066ContratoServicosPrioridade_Ordem ;
      private bool n1338ContratoServicosPrioridade_PercValorB ;
      private bool n1339ContratoServicosPrioridade_PercPrazo ;
      private bool n2067ContratoServicosPrioridade_Peso ;
      private bool AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 ;
      private bool AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV52ddo_ContratoServicosPrioridade_OrdemTitleControlIdToReplace ;
      private String AV36ddo_ContratoServicosPrioridade_NomeTitleControlIdToReplace ;
      private String AV40ddo_ContratoServicosPrioridade_PercValorBTitleControlIdToReplace ;
      private String AV44ddo_ContratoServicosPrioridade_PercPrazoTitleControlIdToReplace ;
      private String AV56ddo_ContratoServicosPrioridade_PesoTitleControlIdToReplace ;
      private String AV77Update_GXI ;
      private String AV78Delete_GXI ;
      private String AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 ;
      private String AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 ;
      private String AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private short[] H00K12_A2067ContratoServicosPrioridade_Peso ;
      private bool[] H00K12_n2067ContratoServicosPrioridade_Peso ;
      private decimal[] H00K12_A1339ContratoServicosPrioridade_PercPrazo ;
      private bool[] H00K12_n1339ContratoServicosPrioridade_PercPrazo ;
      private decimal[] H00K12_A1338ContratoServicosPrioridade_PercValorB ;
      private bool[] H00K12_n1338ContratoServicosPrioridade_PercValorB ;
      private String[] H00K12_A1337ContratoServicosPrioridade_Nome ;
      private short[] H00K12_A2066ContratoServicosPrioridade_Ordem ;
      private bool[] H00K12_n2066ContratoServicosPrioridade_Ordem ;
      private int[] H00K12_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] H00K12_A1336ContratoServicosPrioridade_Codigo ;
      private long[] H00K13_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49ContratoServicosPrioridade_OrdemTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33ContratoServicosPrioridade_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37ContratoServicosPrioridade_PercValorBTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV41ContratoServicosPrioridade_PercPrazoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV53ContratoServicosPrioridade_PesoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV45DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontratoservicosprioridade__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00K12( IGxContext context ,
                                             String AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 ,
                                             String AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 ,
                                             bool AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 ,
                                             String AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 ,
                                             String AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 ,
                                             bool AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 ,
                                             String AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 ,
                                             String AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 ,
                                             short AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem ,
                                             short AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to ,
                                             String AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel ,
                                             String AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome ,
                                             decimal AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb ,
                                             decimal AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to ,
                                             decimal AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo ,
                                             decimal AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to ,
                                             short AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso ,
                                             short AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to ,
                                             String A1337ContratoServicosPrioridade_Nome ,
                                             short A2066ContratoServicosPrioridade_Ordem ,
                                             decimal A1338ContratoServicosPrioridade_PercValorB ,
                                             decimal A1339ContratoServicosPrioridade_PercPrazo ,
                                             short A2067ContratoServicosPrioridade_Peso ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [18] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ContratoServicosPrioridade_Peso], [ContratoServicosPrioridade_PercPrazo], [ContratoServicosPrioridade_PercValorB], [ContratoServicosPrioridade_Nome], [ContratoServicosPrioridade_Ordem], [ContratoServicosPrioridade_CntSrvCod], [ContratoServicosPrioridade_Codigo]";
         sFromString = " FROM [ContratoServicosPrioridade] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] like '%' + @lV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] like '%' + @lV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] like '%' + @lV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] like '%' + @lV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] like '%' + @lV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] like '%' + @lV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 + '%')";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (0==AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Ordem] >= @AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Ordem] >= @AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (0==AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Ordem] <= @AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Ordem] <= @AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] like @lV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] like @lV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] = @AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] = @AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_PercValorB] >= @AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_PercValorB] >= @AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_PercValorB] <= @AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_PercValorB] <= @AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_PercPrazo] >= @AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_PercPrazo] >= @AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_PercPrazo] <= @AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_PercPrazo] <= @AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Peso] >= @AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Peso] >= @AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Peso] <= @AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Peso] <= @AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrioridade_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrioridade_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrioridade_Ordem]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrioridade_Ordem] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrioridade_PercValorB]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrioridade_PercValorB] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrioridade_PercPrazo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrioridade_PercPrazo] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrioridade_Peso]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrioridade_Peso] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosPrioridade_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00K13( IGxContext context ,
                                             String AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1 ,
                                             String AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 ,
                                             bool AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 ,
                                             String AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2 ,
                                             String AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 ,
                                             bool AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 ,
                                             String AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3 ,
                                             String AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 ,
                                             short AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem ,
                                             short AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to ,
                                             String AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel ,
                                             String AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome ,
                                             decimal AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb ,
                                             decimal AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to ,
                                             decimal AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo ,
                                             decimal AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to ,
                                             short AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso ,
                                             short AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to ,
                                             String A1337ContratoServicosPrioridade_Nome ,
                                             short A2066ContratoServicosPrioridade_Ordem ,
                                             decimal A1338ContratoServicosPrioridade_PercValorB ,
                                             decimal A1339ContratoServicosPrioridade_PercPrazo ,
                                             short A2067ContratoServicosPrioridade_Peso ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [13] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ContratoServicosPrioridade] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV59WWContratoServicosPrioridadeDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] like '%' + @lV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] like '%' + @lV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( AV61WWContratoServicosPrioridadeDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV62WWContratoServicosPrioridadeDS_4_Dynamicfiltersselector2, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] like '%' + @lV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] like '%' + @lV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV64WWContratoServicosPrioridadeDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWContratoServicosPrioridadeDS_7_Dynamicfiltersselector3, "CONTRATOSERVICOSPRIORIDADE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] like '%' + @lV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] like '%' + @lV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (0==AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Ordem] >= @AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Ordem] >= @AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (0==AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Ordem] <= @AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Ordem] <= @AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] like @lV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] like @lV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Nome] = @AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Nome] = @AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_PercValorB] >= @AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_PercValorB] >= @AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_PercValorB] <= @AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_PercValorB] <= @AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_PercPrazo] >= @AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_PercPrazo] >= @AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_PercPrazo] <= @AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_PercPrazo] <= @AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Peso] >= @AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Peso] >= @AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosPrioridade_Peso] <= @AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosPrioridade_Peso] <= @AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00K12(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (short)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (short)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
               case 1 :
                     return conditional_H00K13(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (short)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (short)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00K12 ;
          prmH00K12 = new Object[] {
          new Object[] {"@lV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@lV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00K13 ;
          prmH00K13 = new Object[] {
          new Object[] {"@lV60WWContratoServicosPrioridadeDS_2_Contratoservicosprioridade_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWContratoServicosPrioridadeDS_5_Contratoservicosprioridade_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWContratoServicosPrioridadeDS_8_Contratoservicosprioridade_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV67WWContratoServicosPrioridadeDS_9_Tfcontratoservicosprioridade_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV68WWContratoServicosPrioridadeDS_10_Tfcontratoservicosprioridade_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@lV69WWContratoServicosPrioridadeDS_11_Tfcontratoservicosprioridade_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV70WWContratoServicosPrioridadeDS_12_Tfcontratoservicosprioridade_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWContratoServicosPrioridadeDS_13_Tfcontratoservicosprioridade_percvalorb",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV72WWContratoServicosPrioridadeDS_14_Tfcontratoservicosprioridade_percvalorb_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV73WWContratoServicosPrioridadeDS_15_Tfcontratoservicosprioridade_percprazo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV74WWContratoServicosPrioridadeDS_16_Tfcontratoservicosprioridade_percprazo_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV75WWContratoServicosPrioridadeDS_17_Tfcontratoservicosprioridade_peso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV76WWContratoServicosPrioridadeDS_18_Tfcontratoservicosprioridade_peso_to",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00K12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K12,11,0,true,false )
             ,new CursorDef("H00K13", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K13,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 50) ;
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[25]);
                }
                return;
       }
    }

 }

}
