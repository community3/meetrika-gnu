/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:13:46.35
*/
gx.evt.autoSkip = false;
gx.define('wc_contadoresfs', true, function (CmpContext) {
   this.ServerClass =  "wc_contadoresfs" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV6Contratada_Codigo=gx.fn.getIntegerValue("vCONTRATADA_CODIGO",'.') ;
      this.AV8ContagemResultado_Codigo=gx.fn.getIntegerValue("vCONTAGEMRESULTADO_CODIGO",'.') ;
   };
   this.e12bc2_client=function()
   {
      this.executeServerEvent("VUSUARIO_CODIGO.CLICK", true, null, false, true);
   };
   this.e13bc2_client=function()
   {
      this.executeServerEvent("VUSUARIO_CODIGO.ISVALID", true, null, false, true);
   };
   this.e15bc2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e16bc2_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2];
   this.GXLastCtrlId =2;
   GXValidFnc[2]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:'e13bc2_client',rgrid:[],fld:"vUSUARIO_CODIGO",gxz:"ZV5Usuario_Codigo",gxold:"OV5Usuario_Codigo",gxvar:"AV5Usuario_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV5Usuario_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV5Usuario_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vUSUARIO_CODIGO",gx.O.AV5Usuario_Codigo)},c2v:function(){if(this.val()!==undefined)gx.O.AV5Usuario_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vUSUARIO_CODIGO",'.')},nac:gx.falseFn};
   this.AV5Usuario_Codigo = 0 ;
   this.AV6Contratada_Codigo = 0 ;
   this.AV8ContagemResultado_Codigo = 0 ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.A454ContagemResultado_ContadorFSCod = 0 ;
   this.A805ContagemResultado_ContratadaOrigemCod = 0 ;
   this.Events = {"e12bc2_client": ["VUSUARIO_CODIGO.CLICK", true] ,"e13bc2_client": ["VUSUARIO_CODIGO.ISVALID", true] ,"e15bc2_client": ["ENTER", true] ,"e16bc2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["VUSUARIO_CODIGO.CLICK"] = [[{av:'AV5Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],[]];
   this.EvtParms["VUSUARIO_CODIGO.ISVALID"] = [[{av:'AV5Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0}],[]];
   this.setVCMap("AV6Contratada_Codigo", "vCONTRATADA_CODIGO", 0, "int");
   this.setVCMap("AV8ContagemResultado_Codigo", "vCONTAGEMRESULTADO_CODIGO", 0, "int");
   this.InitStandaloneVars( );
});
