/*
               File: PRC_IsContratoServicosUnidConversao
        Description: Verifica se J� existe Unidade de Convers�o Cadastrada para o Servi�o do Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:52.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_iscontratoservicosunidconversao : GXProcedure
   {
      public prc_iscontratoservicosunidconversao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_iscontratoservicosunidconversao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo ,
                           out bool aP1_IsContratoServicosUnidConversao )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8IsContratoServicosUnidConversao = false ;
         initialize();
         executePrivate();
         aP1_IsContratoServicosUnidConversao=this.AV8IsContratoServicosUnidConversao;
      }

      public bool executeUdp( int aP0_ContratoServicos_Codigo )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8IsContratoServicosUnidConversao = false ;
         initialize();
         executePrivate();
         aP1_IsContratoServicosUnidConversao=this.AV8IsContratoServicosUnidConversao;
         return AV8IsContratoServicosUnidConversao ;
      }

      public void executeSubmit( int aP0_ContratoServicos_Codigo ,
                                 out bool aP1_IsContratoServicosUnidConversao )
      {
         prc_iscontratoservicosunidconversao objprc_iscontratoservicosunidconversao;
         objprc_iscontratoservicosunidconversao = new prc_iscontratoservicosunidconversao();
         objprc_iscontratoservicosunidconversao.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_iscontratoservicosunidconversao.AV8IsContratoServicosUnidConversao = false ;
         objprc_iscontratoservicosunidconversao.context.SetSubmitInitialConfig(context);
         objprc_iscontratoservicosunidconversao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_iscontratoservicosunidconversao);
         aP1_IsContratoServicosUnidConversao=this.AV8IsContratoServicosUnidConversao;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_iscontratoservicosunidconversao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8IsContratoServicosUnidConversao = false;
         /* Using cursor P00Y92 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2110ContratoServicosUnidConversao_Codigo = P00Y92_A2110ContratoServicosUnidConversao_Codigo[0];
            AV8IsContratoServicosUnidConversao = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00Y92_A160ContratoServicos_Codigo = new int[1] ;
         P00Y92_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_iscontratoservicosunidconversao__default(),
            new Object[][] {
                new Object[] {
               P00Y92_A160ContratoServicos_Codigo, P00Y92_A2110ContratoServicosUnidConversao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A160ContratoServicos_Codigo ;
      private int A2110ContratoServicosUnidConversao_Codigo ;
      private String scmdbuf ;
      private bool AV8IsContratoServicosUnidConversao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00Y92_A160ContratoServicos_Codigo ;
      private int[] P00Y92_A2110ContratoServicosUnidConversao_Codigo ;
      private bool aP1_IsContratoServicosUnidConversao ;
   }

   public class prc_iscontratoservicosunidconversao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00Y92 ;
          prmP00Y92 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00Y92", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosUnidConversao_Codigo] FROM [ContratoServicosUnidConversao] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Y92,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
