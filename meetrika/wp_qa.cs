/*
               File: WP_QA
        Description: QA
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:50:50.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_qa : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_qa( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_qa( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           int aP1_UserId ,
                           bool aP2_EhContratante ,
                           int aP3_RespostaDe )
      {
         this.AV16Codigo = aP0_Codigo;
         this.AV11UserId = aP1_UserId;
         this.AV15EhContratante = aP2_EhContratante;
         this.AV17RespostaDe = aP3_RespostaDe;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavPara = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV16Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV11UserId = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11UserId), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11UserId), "ZZZZZ9")));
                  AV15EhContratante = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15EhContratante", AV15EhContratante);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vEHCONTRATANTE", GetSecureSignedToken( "", AV15EhContratante));
                  AV17RespostaDe = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17RespostaDe", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17RespostaDe), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vRESPOSTADE", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV17RespostaDe), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAR32( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTR32( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423505067");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_qa.aspx") + "?" + UrlEncode("" +AV16Codigo) + "," + UrlEncode("" +AV11UserId) + "," + UrlEncode(StringUtil.BoolToStr(AV15EhContratante)) + "," + UrlEncode("" +AV17RespostaDe)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11UserId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vRESPOSTADE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17RespostaDe), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIOS", AV20Usuarios);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIOS", AV20Usuarios);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOQA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1984ContagemResultadoQA_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOQA_DATAHORA", context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOQA_TEXTO", A1987ContagemResultadoQA_Texto);
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_DEMANDA", AV39ContagemResultado_Demanda);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV30WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV30WWPContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV38Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV38Codigos);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vATTACHMENTS", AV33Attachments);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vATTACHMENTS", AV33Attachments);
         }
         GxWebStd.gx_hidden_field( context, "vRESULTADO", StringUtil.RTrim( AV34Resultado));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOQA", AV6ContagemResultadoQA);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOQA", AV6ContagemResultadoQA);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vEHCONTRATANTE", AV15EhContratante);
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOANOM", StringUtil.RTrim( A40000Usuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOANOM", StringUtil.RTrim( A40001Usuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "gxhash_vCODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11UserId), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vEHCONTRATANTE", GetSecureSignedToken( "", AV15EhContratante));
         GxWebStd.gx_hidden_field( context, "gxhash_vRESPOSTADE", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV17RespostaDe), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11UserId), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vEHCONTRATANTE", GetSecureSignedToken( "", AV15EhContratante));
         GxWebStd.gx_hidden_field( context, "gxhash_vRESPOSTADE", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV17RespostaDe), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WER32( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTR32( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_qa.aspx") + "?" + UrlEncode("" +AV16Codigo) + "," + UrlEncode("" +AV11UserId) + "," + UrlEncode(StringUtil.BoolToStr(AV15EhContratante)) + "," + UrlEncode("" +AV17RespostaDe) ;
      }

      public override String GetPgmname( )
      {
         return "WP_QA" ;
      }

      public override String GetPgmdesc( )
      {
         return "QA" ;
      }

      protected void WBR30( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_R32( true) ;
         }
         else
         {
            wb_table1_2_R32( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_R32e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTR32( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "QA", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPR30( ) ;
      }

      protected void WSR32( )
      {
         STARTR32( ) ;
         EVTR32( ) ;
      }

      protected void EVTR32( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11R32 */
                              E11R32 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12R32 */
                                    E12R32 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13R32 */
                              E13R32 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WER32( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAR32( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavPara.Name = "vPARA";
            cmbavPara.WebTags = "";
            cmbavPara.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavPara.ItemCount > 0 )
            {
               AV10Para = (int)(NumberUtil.Val( cmbavPara.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Para), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Para", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Para), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavPara_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavPara.ItemCount > 0 )
         {
            AV10Para = (int)(NumberUtil.Val( cmbavPara.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Para), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Para", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Para), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFR32( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFR32( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E13R32 */
            E13R32 ();
            WBR30( ) ;
         }
      }

      protected void STRUPR30( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Using cursor H00R33 */
         pr_default.execute(0, new Object[] {AV18Owner});
         if ( (pr_default.getStatus(0) != 101) )
         {
            A40000Usuario_PessoaNom = H00R33_A40000Usuario_PessoaNom[0];
         }
         else
         {
            A40000Usuario_PessoaNom = "";
         }
         pr_default.close(0);
         /* Using cursor H00R35 */
         pr_default.execute(1, new Object[] {AV19Responsavel});
         if ( (pr_default.getStatus(1) != 101) )
         {
            A40001Usuario_PessoaNom = H00R35_A40001Usuario_PessoaNom[0];
         }
         else
         {
            A40001Usuario_PessoaNom = "";
         }
         pr_default.close(1);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11R32 */
         E11R32 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavPara.CurrentValue = cgiGet( cmbavPara_Internalname);
            AV10Para = (int)(NumberUtil.Val( cgiGet( cmbavPara_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Para", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Para), 6, 0)));
            AV7ContagemResultadoQA_Texto = cgiGet( edtavContagemresultadoqa_texto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultadoQA_Texto", AV7ContagemResultadoQA_Texto);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11R32 */
         E11R32 ();
         if (returnInSub) return;
      }

      protected void E11R32( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV30WWPContext) ;
         /* Execute user subroutine: 'CARREGAPARA' */
         S112 ();
         if (returnInSub) return;
         if ( AV17RespostaDe > 0 )
         {
            Form.Caption = "Resposta";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         }
         else
         {
            Form.Caption = "Pergunta";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E12R32 */
         E12R32 ();
         if (returnInSub) return;
      }

      protected void E12R32( )
      {
         /* Enter Routine */
         if ( (0==AV10Para) )
         {
            GX_msglist.addItem("Selecione o destinat�rio da sua consulta!");
            GX_FocusControl = cmbavPara_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV7ContagemResultadoQA_Texto)) )
         {
            GX_msglist.addItem("Informe a consulta que deseja realizar!");
            GX_FocusControl = edtavContagemresultadoqa_texto_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else
         {
            AV6ContagemResultadoQA = new SdtContagemResultadoQA(context);
            AV6ContagemResultadoQA.gxTpr_Contagemresultadoqa_oscod = AV16Codigo;
            AV6ContagemResultadoQA.gxTpr_Contagemresultadoqa_texto = AV7ContagemResultadoQA_Texto;
            AV6ContagemResultadoQA.gxTpr_Contagemresultadoqa_usercod = AV11UserId;
            AV6ContagemResultadoQA.gxTpr_Contagemresultadoqa_paracod = AV10Para;
            AV6ContagemResultadoQA.gxTpr_Contagemresultadoqa_respostade = AV17RespostaDe;
            AV6ContagemResultadoQA.Save();
            if ( AV6ContagemResultadoQA.Success() )
            {
               context.CommitDataStores( "WP_QA");
               /* Execute user subroutine: 'NOTIFICAR' */
               S122 ();
               if (returnInSub) return;
               context.setWebReturnParms(new Object[] {});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
            else
            {
               context.RollbackDataStores( "WP_QA");
               /* Execute user subroutine: 'MESSAGES' */
               S132 ();
               if (returnInSub) return;
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6ContagemResultadoQA", AV6ContagemResultadoQA);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20Usuarios", AV20Usuarios);
      }

      protected void S112( )
      {
         /* 'CARREGAPARA' Routine */
         if ( AV17RespostaDe > 0 )
         {
            /* Using cursor H00R36 */
            pr_default.execute(2, new Object[] {AV17RespostaDe});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1985ContagemResultadoQA_OSCod = H00R36_A1985ContagemResultadoQA_OSCod[0];
               A1989ContagemResultadoQA_UserPesCod = H00R36_A1989ContagemResultadoQA_UserPesCod[0];
               n1989ContagemResultadoQA_UserPesCod = H00R36_n1989ContagemResultadoQA_UserPesCod[0];
               A1984ContagemResultadoQA_Codigo = H00R36_A1984ContagemResultadoQA_Codigo[0];
               A1990ContagemResultadoQA_UserPesNom = H00R36_A1990ContagemResultadoQA_UserPesNom[0];
               n1990ContagemResultadoQA_UserPesNom = H00R36_n1990ContagemResultadoQA_UserPesNom[0];
               A1988ContagemResultadoQA_UserCod = H00R36_A1988ContagemResultadoQA_UserCod[0];
               A493ContagemResultado_DemandaFM = H00R36_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00R36_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00R36_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00R36_n457ContagemResultado_Demanda[0];
               A493ContagemResultado_DemandaFM = H00R36_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00R36_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00R36_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00R36_n457ContagemResultado_Demanda[0];
               A1989ContagemResultadoQA_UserPesCod = H00R36_A1989ContagemResultadoQA_UserPesCod[0];
               n1989ContagemResultadoQA_UserPesCod = H00R36_n1989ContagemResultadoQA_UserPesCod[0];
               A1990ContagemResultadoQA_UserPesNom = H00R36_A1990ContagemResultadoQA_UserPesNom[0];
               n1990ContagemResultadoQA_UserPesNom = H00R36_n1990ContagemResultadoQA_UserPesNom[0];
               cmbavPara.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0)), A1990ContagemResultadoQA_UserPesNom, 0);
               AV10Para = A1988ContagemResultadoQA_UserCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Para", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Para), 6, 0)));
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) )
               {
                  AV39ContagemResultado_Demanda = A457ContagemResultado_Demanda;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContagemResultado_Demanda", AV39ContagemResultado_Demanda);
               }
               else
               {
                  AV39ContagemResultado_Demanda = A493ContagemResultado_DemandaFM;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContagemResultado_Demanda", AV39ContagemResultado_Demanda);
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
         }
         else
         {
            /* Using cursor H00R37 */
            pr_default.execute(3, new Object[] {AV16Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A456ContagemResultado_Codigo = H00R37_A456ContagemResultado_Codigo[0];
               A493ContagemResultado_DemandaFM = H00R37_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00R37_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00R37_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00R37_n457ContagemResultado_Demanda[0];
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) )
               {
                  AV39ContagemResultado_Demanda = A457ContagemResultado_Demanda;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContagemResultado_Demanda", AV39ContagemResultado_Demanda);
               }
               else
               {
                  AV39ContagemResultado_Demanda = A493ContagemResultado_DemandaFM;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContagemResultado_Demanda", AV39ContagemResultado_Demanda);
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            if ( AV15EhContratante )
            {
               /* Using cursor H00R38 */
               pr_default.execute(4, new Object[] {AV16Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A1553ContagemResultado_CntSrvCod = H00R38_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = H00R38_n1553ContagemResultado_CntSrvCod[0];
                  A1603ContagemResultado_CntCod = H00R38_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = H00R38_n1603ContagemResultado_CntCod[0];
                  A1605ContagemResultado_CntPrpPesCod = H00R38_A1605ContagemResultado_CntPrpPesCod[0];
                  n1605ContagemResultado_CntPrpPesCod = H00R38_n1605ContagemResultado_CntPrpPesCod[0];
                  A456ContagemResultado_Codigo = H00R38_A456ContagemResultado_Codigo[0];
                  A508ContagemResultado_Owner = H00R38_A508ContagemResultado_Owner[0];
                  n508ContagemResultado_Owner = H00R38_n508ContagemResultado_Owner[0];
                  A1604ContagemResultado_CntPrpCod = H00R38_A1604ContagemResultado_CntPrpCod[0];
                  n1604ContagemResultado_CntPrpCod = H00R38_n1604ContagemResultado_CntPrpCod[0];
                  A890ContagemResultado_Responsavel = H00R38_A890ContagemResultado_Responsavel[0];
                  n890ContagemResultado_Responsavel = H00R38_n890ContagemResultado_Responsavel[0];
                  A1606ContagemResultado_CntPrpPesNom = H00R38_A1606ContagemResultado_CntPrpPesNom[0];
                  n1606ContagemResultado_CntPrpPesNom = H00R38_n1606ContagemResultado_CntPrpPesNom[0];
                  A1603ContagemResultado_CntCod = H00R38_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = H00R38_n1603ContagemResultado_CntCod[0];
                  A1604ContagemResultado_CntPrpCod = H00R38_A1604ContagemResultado_CntPrpCod[0];
                  n1604ContagemResultado_CntPrpCod = H00R38_n1604ContagemResultado_CntPrpCod[0];
                  A1605ContagemResultado_CntPrpPesCod = H00R38_A1605ContagemResultado_CntPrpPesCod[0];
                  n1605ContagemResultado_CntPrpPesCod = H00R38_n1605ContagemResultado_CntPrpPesCod[0];
                  A1606ContagemResultado_CntPrpPesNom = H00R38_A1606ContagemResultado_CntPrpPesNom[0];
                  n1606ContagemResultado_CntPrpPesNom = H00R38_n1606ContagemResultado_CntPrpPesNom[0];
                  if ( A1604ContagemResultado_CntPrpCod != A508ContagemResultado_Owner )
                  {
                     AV18Owner = A508ContagemResultado_Owner;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Owner), 6, 0)));
                  }
                  if ( A1604ContagemResultado_CntPrpCod != A890ContagemResultado_Responsavel )
                  {
                     AV19Responsavel = A890ContagemResultado_Responsavel;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Responsavel), 6, 0)));
                  }
                  /* Execute user subroutine: 'ENVOLVIDOS' */
                  S145 ();
                  if ( returnInSub )
                  {
                     pr_default.close(4);
                     returnInSub = true;
                     if (true) return;
                  }
                  if ( A1604ContagemResultado_CntPrpCod > 0 )
                  {
                     cmbavPara.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1604ContagemResultado_CntPrpCod), 6, 0)), A1606ContagemResultado_CntPrpPesNom, 0);
                     AV10Para = A1604ContagemResultado_CntPrpCod;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Para", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Para), 6, 0)));
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(4);
            }
            else
            {
               AV45GXLvl87 = 0;
               /* Using cursor H00R39 */
               pr_default.execute(5, new Object[] {AV16Codigo, AV11UserId});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A1219LogResponsavel_OwnerPessoaCod = H00R39_A1219LogResponsavel_OwnerPessoaCod[0];
                  n1219LogResponsavel_OwnerPessoaCod = H00R39_n1219LogResponsavel_OwnerPessoaCod[0];
                  A1553ContagemResultado_CntSrvCod = H00R39_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = H00R39_n1553ContagemResultado_CntSrvCod[0];
                  A896LogResponsavel_Owner = H00R39_A896LogResponsavel_Owner[0];
                  A892LogResponsavel_DemandaCod = H00R39_A892LogResponsavel_DemandaCod[0];
                  n892LogResponsavel_DemandaCod = H00R39_n892LogResponsavel_DemandaCod[0];
                  A1603ContagemResultado_CntCod = H00R39_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = H00R39_n1603ContagemResultado_CntCod[0];
                  A1222LogResponsavel_OwnerPessoaNom = H00R39_A1222LogResponsavel_OwnerPessoaNom[0];
                  n1222LogResponsavel_OwnerPessoaNom = H00R39_n1222LogResponsavel_OwnerPessoaNom[0];
                  A1219LogResponsavel_OwnerPessoaCod = H00R39_A1219LogResponsavel_OwnerPessoaCod[0];
                  n1219LogResponsavel_OwnerPessoaCod = H00R39_n1219LogResponsavel_OwnerPessoaCod[0];
                  A1222LogResponsavel_OwnerPessoaNom = H00R39_A1222LogResponsavel_OwnerPessoaNom[0];
                  n1222LogResponsavel_OwnerPessoaNom = H00R39_n1222LogResponsavel_OwnerPessoaNom[0];
                  A1553ContagemResultado_CntSrvCod = H00R39_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = H00R39_n1553ContagemResultado_CntSrvCod[0];
                  A1603ContagemResultado_CntCod = H00R39_A1603ContagemResultado_CntCod[0];
                  n1603ContagemResultado_CntCod = H00R39_n1603ContagemResultado_CntCod[0];
                  AV45GXLvl87 = 1;
                  cmbavPara.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)), A1222LogResponsavel_OwnerPessoaNom, 0);
                  AV10Para = A896LogResponsavel_Owner;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Para", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Para), 6, 0)));
                  AV8Contrato_Codigo = A1603ContagemResultado_CntCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Contrato_Codigo), 6, 0)));
                  pr_default.readNext(5);
               }
               pr_default.close(5);
               if ( AV45GXLvl87 == 0 )
               {
                  AV46GXLvl95 = 0;
                  /* Using cursor H00R310 */
                  pr_default.execute(6, new Object[] {AV16Codigo});
                  while ( (pr_default.getStatus(6) != 101) )
                  {
                     A1553ContagemResultado_CntSrvCod = H00R310_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = H00R310_n1553ContagemResultado_CntSrvCod[0];
                     A1989ContagemResultadoQA_UserPesCod = H00R310_A1989ContagemResultadoQA_UserPesCod[0];
                     n1989ContagemResultadoQA_UserPesCod = H00R310_n1989ContagemResultadoQA_UserPesCod[0];
                     A1985ContagemResultadoQA_OSCod = H00R310_A1985ContagemResultadoQA_OSCod[0];
                     A508ContagemResultado_Owner = H00R310_A508ContagemResultado_Owner[0];
                     n508ContagemResultado_Owner = H00R310_n508ContagemResultado_Owner[0];
                     A1988ContagemResultadoQA_UserCod = H00R310_A1988ContagemResultadoQA_UserCod[0];
                     A890ContagemResultado_Responsavel = H00R310_A890ContagemResultado_Responsavel[0];
                     n890ContagemResultado_Responsavel = H00R310_n890ContagemResultado_Responsavel[0];
                     A1990ContagemResultadoQA_UserPesNom = H00R310_A1990ContagemResultadoQA_UserPesNom[0];
                     n1990ContagemResultadoQA_UserPesNom = H00R310_n1990ContagemResultadoQA_UserPesNom[0];
                     A1603ContagemResultado_CntCod = H00R310_A1603ContagemResultado_CntCod[0];
                     n1603ContagemResultado_CntCod = H00R310_n1603ContagemResultado_CntCod[0];
                     A1984ContagemResultadoQA_Codigo = H00R310_A1984ContagemResultadoQA_Codigo[0];
                     A1553ContagemResultado_CntSrvCod = H00R310_A1553ContagemResultado_CntSrvCod[0];
                     n1553ContagemResultado_CntSrvCod = H00R310_n1553ContagemResultado_CntSrvCod[0];
                     A508ContagemResultado_Owner = H00R310_A508ContagemResultado_Owner[0];
                     n508ContagemResultado_Owner = H00R310_n508ContagemResultado_Owner[0];
                     A890ContagemResultado_Responsavel = H00R310_A890ContagemResultado_Responsavel[0];
                     n890ContagemResultado_Responsavel = H00R310_n890ContagemResultado_Responsavel[0];
                     A1603ContagemResultado_CntCod = H00R310_A1603ContagemResultado_CntCod[0];
                     n1603ContagemResultado_CntCod = H00R310_n1603ContagemResultado_CntCod[0];
                     A1989ContagemResultadoQA_UserPesCod = H00R310_A1989ContagemResultadoQA_UserPesCod[0];
                     n1989ContagemResultadoQA_UserPesCod = H00R310_n1989ContagemResultadoQA_UserPesCod[0];
                     A1990ContagemResultadoQA_UserPesNom = H00R310_A1990ContagemResultadoQA_UserPesNom[0];
                     n1990ContagemResultadoQA_UserPesNom = H00R310_n1990ContagemResultadoQA_UserPesNom[0];
                     AV46GXLvl95 = 1;
                     if ( A1988ContagemResultadoQA_UserCod != A508ContagemResultado_Owner )
                     {
                        AV18Owner = A508ContagemResultado_Owner;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Owner), 6, 0)));
                     }
                     if ( A1988ContagemResultadoQA_UserCod != A890ContagemResultado_Responsavel )
                     {
                        AV19Responsavel = A890ContagemResultado_Responsavel;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Responsavel), 6, 0)));
                     }
                     /* Execute user subroutine: 'ENVOLVIDOS' */
                     S145 ();
                     if ( returnInSub )
                     {
                        pr_default.close(6);
                        returnInSub = true;
                        if (true) return;
                     }
                     cmbavPara.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1988ContagemResultadoQA_UserCod), 6, 0)), A1990ContagemResultadoQA_UserPesNom, 0);
                     AV10Para = A1988ContagemResultadoQA_UserCod;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Para", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Para), 6, 0)));
                     AV8Contrato_Codigo = A1603ContagemResultado_CntCod;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Contrato_Codigo), 6, 0)));
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(6);
                  }
                  pr_default.close(6);
                  if ( AV46GXLvl95 == 0 )
                  {
                     /* Using cursor H00R311 */
                     pr_default.execute(7, new Object[] {AV16Codigo});
                     while ( (pr_default.getStatus(7) != 101) )
                     {
                        A456ContagemResultado_Codigo = H00R311_A456ContagemResultado_Codigo[0];
                        A508ContagemResultado_Owner = H00R311_A508ContagemResultado_Owner[0];
                        n508ContagemResultado_Owner = H00R311_n508ContagemResultado_Owner[0];
                        A890ContagemResultado_Responsavel = H00R311_A890ContagemResultado_Responsavel[0];
                        n890ContagemResultado_Responsavel = H00R311_n890ContagemResultado_Responsavel[0];
                        AV18Owner = A508ContagemResultado_Owner;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Owner), 6, 0)));
                        if ( A508ContagemResultado_Owner != A890ContagemResultado_Responsavel )
                        {
                           AV19Responsavel = A890ContagemResultado_Responsavel;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Responsavel), 6, 0)));
                        }
                        /* Execute user subroutine: 'ENVOLVIDOS' */
                        S145 ();
                        if ( returnInSub )
                        {
                           pr_default.close(7);
                           returnInSub = true;
                           if (true) return;
                        }
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                     pr_default.close(7);
                  }
               }
               if ( (0==AV10Para) )
               {
                  /* Using cursor H00R312 */
                  pr_default.execute(8, new Object[] {AV8Contrato_Codigo});
                  while ( (pr_default.getStatus(8) != 101) )
                  {
                     A1080ContratoGestor_UsuarioPesCod = H00R312_A1080ContratoGestor_UsuarioPesCod[0];
                     n1080ContratoGestor_UsuarioPesCod = H00R312_n1080ContratoGestor_UsuarioPesCod[0];
                     A1078ContratoGestor_ContratoCod = H00R312_A1078ContratoGestor_ContratoCod[0];
                     A1081ContratoGestor_UsuarioPesNom = H00R312_A1081ContratoGestor_UsuarioPesNom[0];
                     n1081ContratoGestor_UsuarioPesNom = H00R312_n1081ContratoGestor_UsuarioPesNom[0];
                     A1079ContratoGestor_UsuarioCod = H00R312_A1079ContratoGestor_UsuarioCod[0];
                     A1446ContratoGestor_ContratadaAreaCod = H00R312_A1446ContratoGestor_ContratadaAreaCod[0];
                     n1446ContratoGestor_ContratadaAreaCod = H00R312_n1446ContratoGestor_ContratadaAreaCod[0];
                     A1446ContratoGestor_ContratadaAreaCod = H00R312_A1446ContratoGestor_ContratadaAreaCod[0];
                     n1446ContratoGestor_ContratadaAreaCod = H00R312_n1446ContratoGestor_ContratadaAreaCod[0];
                     A1080ContratoGestor_UsuarioPesCod = H00R312_A1080ContratoGestor_UsuarioPesCod[0];
                     n1080ContratoGestor_UsuarioPesCod = H00R312_n1080ContratoGestor_UsuarioPesCod[0];
                     A1081ContratoGestor_UsuarioPesNom = H00R312_A1081ContratoGestor_UsuarioPesNom[0];
                     n1081ContratoGestor_UsuarioPesNom = H00R312_n1081ContratoGestor_UsuarioPesNom[0];
                     GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
                     new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                     A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                     if ( AV15EhContratante || ( A1135ContratoGestor_UsuarioEhContratante ) )
                     {
                        if ( ! ( AV15EhContratante ) || ( ! A1135ContratoGestor_UsuarioEhContratante ) )
                        {
                           cmbavPara.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)), A1081ContratoGestor_UsuarioPesNom, 0);
                           AV10Para = A1079ContratoGestor_UsuarioCod;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Para", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Para), 6, 0)));
                        }
                     }
                     pr_default.readNext(8);
                  }
                  pr_default.close(8);
               }
            }
         }
         if ( cmbavPara.ItemCount > 2 )
         {
            cmbavPara.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPara_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavPara.Enabled), 5, 0)));
            AV10Para = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Para", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Para), 6, 0)));
         }
         else
         {
            cmbavPara.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPara_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavPara.Enabled), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'MESSAGES' Routine */
         AV50GXV2 = 1;
         AV49GXV1 = AV6ContagemResultadoQA.GetMessages();
         while ( AV50GXV2 <= AV49GXV1.Count )
         {
            AV9Message = ((SdtMessages_Message)AV49GXV1.Item(AV50GXV2));
            GX_msglist.addItem(AV9Message.gxTpr_Description);
            AV50GXV2 = (int)(AV50GXV2+1);
         }
      }

      protected void S145( )
      {
         /* 'ENVOLVIDOS' Routine */
         /* Using cursor H00R314 */
         pr_default.execute(9, new Object[] {AV18Owner});
         if ( (pr_default.getStatus(9) != 101) )
         {
            A40000Usuario_PessoaNom = H00R314_A40000Usuario_PessoaNom[0];
         }
         else
         {
            A40000Usuario_PessoaNom = "";
         }
         pr_default.close(9);
         /* Using cursor H00R316 */
         pr_default.execute(10, new Object[] {AV19Responsavel});
         if ( (pr_default.getStatus(10) != 101) )
         {
            A40001Usuario_PessoaNom = H00R316_A40001Usuario_PessoaNom[0];
         }
         else
         {
            A40001Usuario_PessoaNom = "";
         }
         pr_default.close(10);
         if ( AV18Owner > 0 )
         {
            if ( AV11UserId != AV18Owner )
            {
               cmbavPara.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV18Owner), 6, 0)), A40000Usuario_PessoaNom, 0);
               AV10Para = AV18Owner;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Para", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Para), 6, 0)));
            }
         }
         if ( AV19Responsavel > 0 )
         {
            if ( AV11UserId != AV19Responsavel )
            {
               cmbavPara.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV19Responsavel), 6, 0)), A40001Usuario_PessoaNom, 0);
               AV10Para = AV19Responsavel;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Para", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Para), 6, 0)));
            }
         }
      }

      protected void S122( )
      {
         /* 'NOTIFICAR' Routine */
         AV20Usuarios.Add(AV10Para, 0);
         AV32EmailText = StringUtil.NewLine( ) + "Prezado(a)," + StringUtil.NewLine( );
         if ( AV17RespostaDe > 0 )
         {
            /* Using cursor H00R317 */
            pr_default.execute(11, new Object[] {AV17RespostaDe});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A1984ContagemResultadoQA_Codigo = H00R317_A1984ContagemResultadoQA_Codigo[0];
               A1986ContagemResultadoQA_DataHora = H00R317_A1986ContagemResultadoQA_DataHora[0];
               A1987ContagemResultadoQA_Texto = H00R317_A1987ContagemResultadoQA_Texto[0];
               AV32EmailText = AV32EmailText + "Da consulta realizada (" + StringUtil.Trim( context.localUtil.TToC( A1986ContagemResultadoQA_DataHora, 8, 5, 0, 3, "/", ":", " ")) + "):" + StringUtil.NewLine( ) + StringUtil.NewLine( );
               AV32EmailText = AV32EmailText + A1987ContagemResultadoQA_Texto + StringUtil.NewLine( ) + StringUtil.NewLine( );
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(11);
            AV32EmailText = AV32EmailText + "Foi lhe enviada a seguinte resposta:" + StringUtil.NewLine( ) + StringUtil.NewLine( );
            AV31Subject = "Resposta na OS " + StringUtil.Trim( AV39ContagemResultado_Demanda) + " (No reply)";
         }
         else
         {
            AV32EmailText = AV32EmailText + "Foi lhe enviada a seguinte consulta:" + StringUtil.NewLine( ) + StringUtil.NewLine( );
            AV31Subject = "Consulta na OS " + StringUtil.Trim( AV39ContagemResultado_Demanda) + " (No reply)";
         }
         AV32EmailText = AV32EmailText + AV7ContagemResultadoQA_Texto + StringUtil.NewLine( ) + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + StringUtil.NewLine( ) + StringUtil.Trim( AV30WWPContext.gxTpr_Areatrabalho_descricao) + ", usu�rio " + StringUtil.Trim( AV30WWPContext.gxTpr_Username) + StringUtil.NewLine( );
         AV32EmailText = AV32EmailText + StringUtil.NewLine( ) + StringUtil.NewLine( ) + "Esta mensagem pode conter informa��o confidencial e/ou privilegiada. Se voc� n�o for o destinat�rio ou a pessoa autorizada a receber esta mensagem, n�o pode usar, copiar ou divulgar as informa��es nela contidas ou tomar qualquer a��o baseada nessas informa��es. Se voc� recebeu esta mensagem por engano, por favor avise imediatamente o remetente, respondendo o e-mail e em seguida apague-o.";
         AV35Demandante = AV30WWPContext.gxTpr_Areatrabalho_descricao;
         AV26NomeSistema = AV30WWPContext.gxTpr_Parametrossistema_nomesistema;
         AV37WebSession.Set("DemandaCodigo", AV38Codigos.ToXml(false, true, "Collection", ""));
         new prc_enviaremail(context ).execute(  AV30WWPContext.gxTpr_Areatrabalho_codigo,  AV20Usuarios,  AV31Subject,  AV32EmailText,  AV33Attachments, ref  AV34Resultado) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Resultado", AV34Resultado);
         AV37WebSession.Remove("DemandaCodigo");
      }

      protected void nextLoad( )
      {
      }

      protected void E13R32( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_R32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(208), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(433), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_R32( true) ;
         }
         else
         {
            wb_table2_8_R32( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_R32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_16_R32( true) ;
         }
         else
         {
            wb_table3_16_R32( false) ;
         }
         return  ;
      }

      protected void wb_table3_16_R32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Confirmar", bttButton1_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_QA.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "", "Fechar", bttButton2_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_QA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_R32e( true) ;
         }
         else
         {
            wb_table1_2_R32e( false) ;
         }
      }

      protected void wb_table3_16_R32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Texto", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_QA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContagemresultadoqa_texto_Internalname, AV7ContagemResultadoQA_Texto, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", 0, 1, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_QA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_16_R32e( true) ;
         }
         else
         {
            wb_table3_16_R32e( false) ;
         }
      }

      protected void wb_table2_8_R32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblpara_Internalname, tblTblpara_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Para", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_QA.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavPara, cmbavPara_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV10Para), 6, 0)), 1, cmbavPara_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavPara.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", "", true, "HLP_WP_QA.htm");
            cmbavPara.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Para), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPara_Internalname, "Values", (String)(cmbavPara.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_R32e( true) ;
         }
         else
         {
            wb_table2_8_R32e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV16Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16Codigo), "ZZZZZ9")));
         AV11UserId = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11UserId), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11UserId), "ZZZZZ9")));
         AV15EhContratante = (bool)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15EhContratante", AV15EhContratante);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vEHCONTRATANTE", GetSecureSignedToken( "", AV15EhContratante));
         AV17RespostaDe = Convert.ToInt32(getParm(obj,3));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17RespostaDe", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17RespostaDe), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vRESPOSTADE", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV17RespostaDe), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAR32( ) ;
         WSR32( ) ;
         WER32( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423505147");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_qa.js", "?202032423505147");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         cmbavPara_Internalname = "vPARA";
         tblTblpara_Internalname = "TBLPARA";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavContagemresultadoqa_texto_Internalname = "vCONTAGEMRESULTADOQA_TEXTO";
         tblTable2_Internalname = "TABLE2";
         bttButton1_Internalname = "BUTTON1";
         bttButton2_Internalname = "BUTTON2";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavPara_Jsonclick = "";
         cmbavPara.Enabled = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "QA";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12R32',iparms:[{av:'AV10Para',fld:'vPARA',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultadoQA_Texto',fld:'vCONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'AV16Codigo',fld:'vCODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV11UserId',fld:'vUSERID',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17RespostaDe',fld:'vRESPOSTADE',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV20Usuarios',fld:'vUSUARIOS',pic:'',nv:null},{av:'A1984ContagemResultadoQA_Codigo',fld:'CONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1986ContagemResultadoQA_DataHora',fld:'CONTAGEMRESULTADOQA_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'A1987ContagemResultadoQA_Texto',fld:'CONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'AV39ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV30WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV38Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV33Attachments',fld:'vATTACHMENTS',pic:'',nv:null},{av:'AV34Resultado',fld:'vRESULTADO',pic:'',nv:''},{av:'AV6ContagemResultadoQA',fld:'vCONTAGEMRESULTADOQA',pic:'',nv:null}],oparms:[{av:'AV6ContagemResultadoQA',fld:'vCONTAGEMRESULTADOQA',pic:'',nv:null},{av:'AV20Usuarios',fld:'vUSUARIOS',pic:'',nv:null},{av:'AV34Resultado',fld:'vRESULTADO',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV20Usuarios = new GxSimpleCollection();
         A1986ContagemResultadoQA_DataHora = (DateTime)(DateTime.MinValue);
         A1987ContagemResultadoQA_Texto = "";
         AV39ContagemResultado_Demanda = "";
         AV30WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV38Codigos = new GxSimpleCollection();
         AV33Attachments = new GxSimpleCollection();
         AV34Resultado = "";
         AV6ContagemResultadoQA = new SdtContagemResultadoQA(context);
         A40000Usuario_PessoaNom = "";
         A40001Usuario_PessoaNom = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00R33_A40000Usuario_PessoaNom = new String[] {""} ;
         H00R35_A40001Usuario_PessoaNom = new String[] {""} ;
         AV7ContagemResultadoQA_Texto = "";
         H00R36_A1985ContagemResultadoQA_OSCod = new int[1] ;
         H00R36_A1989ContagemResultadoQA_UserPesCod = new int[1] ;
         H00R36_n1989ContagemResultadoQA_UserPesCod = new bool[] {false} ;
         H00R36_A1984ContagemResultadoQA_Codigo = new int[1] ;
         H00R36_A1990ContagemResultadoQA_UserPesNom = new String[] {""} ;
         H00R36_n1990ContagemResultadoQA_UserPesNom = new bool[] {false} ;
         H00R36_A1988ContagemResultadoQA_UserCod = new int[1] ;
         H00R36_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00R36_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00R36_A457ContagemResultado_Demanda = new String[] {""} ;
         H00R36_n457ContagemResultado_Demanda = new bool[] {false} ;
         A1990ContagemResultadoQA_UserPesNom = "";
         A493ContagemResultado_DemandaFM = "";
         A457ContagemResultado_Demanda = "";
         H00R37_A456ContagemResultado_Codigo = new int[1] ;
         H00R37_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00R37_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00R37_A457ContagemResultado_Demanda = new String[] {""} ;
         H00R37_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00R38_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00R38_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00R38_A1603ContagemResultado_CntCod = new int[1] ;
         H00R38_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00R38_A1605ContagemResultado_CntPrpPesCod = new int[1] ;
         H00R38_n1605ContagemResultado_CntPrpPesCod = new bool[] {false} ;
         H00R38_A456ContagemResultado_Codigo = new int[1] ;
         H00R38_A508ContagemResultado_Owner = new int[1] ;
         H00R38_n508ContagemResultado_Owner = new bool[] {false} ;
         H00R38_A1604ContagemResultado_CntPrpCod = new int[1] ;
         H00R38_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         H00R38_A890ContagemResultado_Responsavel = new int[1] ;
         H00R38_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00R38_A1606ContagemResultado_CntPrpPesNom = new String[] {""} ;
         H00R38_n1606ContagemResultado_CntPrpPesNom = new bool[] {false} ;
         A1606ContagemResultado_CntPrpPesNom = "";
         H00R39_A1219LogResponsavel_OwnerPessoaCod = new int[1] ;
         H00R39_n1219LogResponsavel_OwnerPessoaCod = new bool[] {false} ;
         H00R39_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00R39_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00R39_A896LogResponsavel_Owner = new int[1] ;
         H00R39_A892LogResponsavel_DemandaCod = new int[1] ;
         H00R39_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00R39_A1603ContagemResultado_CntCod = new int[1] ;
         H00R39_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00R39_A1222LogResponsavel_OwnerPessoaNom = new String[] {""} ;
         H00R39_n1222LogResponsavel_OwnerPessoaNom = new bool[] {false} ;
         A1222LogResponsavel_OwnerPessoaNom = "";
         H00R310_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00R310_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00R310_A1989ContagemResultadoQA_UserPesCod = new int[1] ;
         H00R310_n1989ContagemResultadoQA_UserPesCod = new bool[] {false} ;
         H00R310_A1985ContagemResultadoQA_OSCod = new int[1] ;
         H00R310_A508ContagemResultado_Owner = new int[1] ;
         H00R310_n508ContagemResultado_Owner = new bool[] {false} ;
         H00R310_A1988ContagemResultadoQA_UserCod = new int[1] ;
         H00R310_A890ContagemResultado_Responsavel = new int[1] ;
         H00R310_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00R310_A1990ContagemResultadoQA_UserPesNom = new String[] {""} ;
         H00R310_n1990ContagemResultadoQA_UserPesNom = new bool[] {false} ;
         H00R310_A1603ContagemResultado_CntCod = new int[1] ;
         H00R310_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00R310_A1984ContagemResultadoQA_Codigo = new int[1] ;
         H00R311_A456ContagemResultado_Codigo = new int[1] ;
         H00R311_A508ContagemResultado_Owner = new int[1] ;
         H00R311_n508ContagemResultado_Owner = new bool[] {false} ;
         H00R311_A890ContagemResultado_Responsavel = new int[1] ;
         H00R311_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00R312_A1080ContratoGestor_UsuarioPesCod = new int[1] ;
         H00R312_n1080ContratoGestor_UsuarioPesCod = new bool[] {false} ;
         H00R312_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00R312_A1081ContratoGestor_UsuarioPesNom = new String[] {""} ;
         H00R312_n1081ContratoGestor_UsuarioPesNom = new bool[] {false} ;
         H00R312_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00R312_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00R312_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         A1081ContratoGestor_UsuarioPesNom = "";
         AV49GXV1 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV9Message = new SdtMessages_Message(context);
         H00R314_A40000Usuario_PessoaNom = new String[] {""} ;
         H00R316_A40001Usuario_PessoaNom = new String[] {""} ;
         AV32EmailText = "";
         H00R317_A1984ContagemResultadoQA_Codigo = new int[1] ;
         H00R317_A1986ContagemResultadoQA_DataHora = new DateTime[] {DateTime.MinValue} ;
         H00R317_A1987ContagemResultadoQA_Texto = new String[] {""} ;
         AV31Subject = "";
         AV35Demandante = "";
         AV26NomeSistema = "";
         AV37WebSession = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttButton1_Jsonclick = "";
         bttButton2_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_qa__default(),
            new Object[][] {
                new Object[] {
               H00R33_A40000Usuario_PessoaNom
               }
               , new Object[] {
               H00R35_A40001Usuario_PessoaNom
               }
               , new Object[] {
               H00R36_A1985ContagemResultadoQA_OSCod, H00R36_A1989ContagemResultadoQA_UserPesCod, H00R36_n1989ContagemResultadoQA_UserPesCod, H00R36_A1984ContagemResultadoQA_Codigo, H00R36_A1990ContagemResultadoQA_UserPesNom, H00R36_n1990ContagemResultadoQA_UserPesNom, H00R36_A1988ContagemResultadoQA_UserCod, H00R36_A493ContagemResultado_DemandaFM, H00R36_n493ContagemResultado_DemandaFM, H00R36_A457ContagemResultado_Demanda,
               H00R36_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00R37_A456ContagemResultado_Codigo, H00R37_A493ContagemResultado_DemandaFM, H00R37_n493ContagemResultado_DemandaFM, H00R37_A457ContagemResultado_Demanda, H00R37_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00R38_A1553ContagemResultado_CntSrvCod, H00R38_n1553ContagemResultado_CntSrvCod, H00R38_A1603ContagemResultado_CntCod, H00R38_n1603ContagemResultado_CntCod, H00R38_A1605ContagemResultado_CntPrpPesCod, H00R38_n1605ContagemResultado_CntPrpPesCod, H00R38_A456ContagemResultado_Codigo, H00R38_A508ContagemResultado_Owner, H00R38_A1604ContagemResultado_CntPrpCod, H00R38_n1604ContagemResultado_CntPrpCod,
               H00R38_A890ContagemResultado_Responsavel, H00R38_n890ContagemResultado_Responsavel, H00R38_A1606ContagemResultado_CntPrpPesNom, H00R38_n1606ContagemResultado_CntPrpPesNom
               }
               , new Object[] {
               H00R39_A1219LogResponsavel_OwnerPessoaCod, H00R39_n1219LogResponsavel_OwnerPessoaCod, H00R39_A1553ContagemResultado_CntSrvCod, H00R39_n1553ContagemResultado_CntSrvCod, H00R39_A896LogResponsavel_Owner, H00R39_A892LogResponsavel_DemandaCod, H00R39_n892LogResponsavel_DemandaCod, H00R39_A1603ContagemResultado_CntCod, H00R39_n1603ContagemResultado_CntCod, H00R39_A1222LogResponsavel_OwnerPessoaNom,
               H00R39_n1222LogResponsavel_OwnerPessoaNom
               }
               , new Object[] {
               H00R310_A1553ContagemResultado_CntSrvCod, H00R310_n1553ContagemResultado_CntSrvCod, H00R310_A1989ContagemResultadoQA_UserPesCod, H00R310_n1989ContagemResultadoQA_UserPesCod, H00R310_A1985ContagemResultadoQA_OSCod, H00R310_A508ContagemResultado_Owner, H00R310_n508ContagemResultado_Owner, H00R310_A1988ContagemResultadoQA_UserCod, H00R310_A890ContagemResultado_Responsavel, H00R310_n890ContagemResultado_Responsavel,
               H00R310_A1990ContagemResultadoQA_UserPesNom, H00R310_n1990ContagemResultadoQA_UserPesNom, H00R310_A1603ContagemResultado_CntCod, H00R310_n1603ContagemResultado_CntCod, H00R310_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               H00R311_A456ContagemResultado_Codigo, H00R311_A508ContagemResultado_Owner, H00R311_A890ContagemResultado_Responsavel, H00R311_n890ContagemResultado_Responsavel
               }
               , new Object[] {
               H00R312_A1080ContratoGestor_UsuarioPesCod, H00R312_n1080ContratoGestor_UsuarioPesCod, H00R312_A1078ContratoGestor_ContratoCod, H00R312_A1081ContratoGestor_UsuarioPesNom, H00R312_n1081ContratoGestor_UsuarioPesNom, H00R312_A1079ContratoGestor_UsuarioCod, H00R312_A1446ContratoGestor_ContratadaAreaCod, H00R312_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00R314_A40000Usuario_PessoaNom
               }
               , new Object[] {
               H00R316_A40001Usuario_PessoaNom
               }
               , new Object[] {
               H00R317_A1984ContagemResultadoQA_Codigo, H00R317_A1986ContagemResultadoQA_DataHora, H00R317_A1987ContagemResultadoQA_Texto
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV45GXLvl87 ;
      private short AV46GXLvl95 ;
      private short nGXWrapped ;
      private int AV16Codigo ;
      private int AV11UserId ;
      private int AV17RespostaDe ;
      private int wcpOAV16Codigo ;
      private int wcpOAV11UserId ;
      private int wcpOAV17RespostaDe ;
      private int A1984ContagemResultadoQA_Codigo ;
      private int AV10Para ;
      private int AV18Owner ;
      private int AV19Responsavel ;
      private int A1985ContagemResultadoQA_OSCod ;
      private int A1989ContagemResultadoQA_UserPesCod ;
      private int A1988ContagemResultadoQA_UserCod ;
      private int A456ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A1605ContagemResultado_CntPrpPesCod ;
      private int A508ContagemResultado_Owner ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A1219LogResponsavel_OwnerPessoaCod ;
      private int A896LogResponsavel_Owner ;
      private int A892LogResponsavel_DemandaCod ;
      private int AV8Contrato_Codigo ;
      private int A1080ContratoGestor_UsuarioPesCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int AV50GXV2 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV34Resultado ;
      private String A40000Usuario_PessoaNom ;
      private String A40001Usuario_PessoaNom ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavPara_Internalname ;
      private String scmdbuf ;
      private String edtavContagemresultadoqa_texto_Internalname ;
      private String A1990ContagemResultadoQA_UserPesNom ;
      private String A1606ContagemResultado_CntPrpPesNom ;
      private String A1222LogResponsavel_OwnerPessoaNom ;
      private String A1081ContratoGestor_UsuarioPesNom ;
      private String AV31Subject ;
      private String AV35Demandante ;
      private String AV26NomeSistema ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String tblTblpara_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String cmbavPara_Jsonclick ;
      private DateTime A1986ContagemResultadoQA_DataHora ;
      private bool AV15EhContratante ;
      private bool wcpOAV15EhContratante ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1989ContagemResultadoQA_UserPesCod ;
      private bool n1990ContagemResultadoQA_UserPesNom ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1605ContagemResultado_CntPrpPesCod ;
      private bool n508ContagemResultado_Owner ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n1606ContagemResultado_CntPrpPesNom ;
      private bool n1219LogResponsavel_OwnerPessoaCod ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1222LogResponsavel_OwnerPessoaNom ;
      private bool n1080ContratoGestor_UsuarioPesCod ;
      private bool n1081ContratoGestor_UsuarioPesNom ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool GXt_boolean1 ;
      private String A1987ContagemResultadoQA_Texto ;
      private String AV7ContagemResultadoQA_Texto ;
      private String AV32EmailText ;
      private String AV39ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavPara ;
      private IDataStoreProvider pr_default ;
      private String[] H00R33_A40000Usuario_PessoaNom ;
      private String[] H00R35_A40001Usuario_PessoaNom ;
      private int[] H00R36_A1985ContagemResultadoQA_OSCod ;
      private int[] H00R36_A1989ContagemResultadoQA_UserPesCod ;
      private bool[] H00R36_n1989ContagemResultadoQA_UserPesCod ;
      private int[] H00R36_A1984ContagemResultadoQA_Codigo ;
      private String[] H00R36_A1990ContagemResultadoQA_UserPesNom ;
      private bool[] H00R36_n1990ContagemResultadoQA_UserPesNom ;
      private int[] H00R36_A1988ContagemResultadoQA_UserCod ;
      private String[] H00R36_A493ContagemResultado_DemandaFM ;
      private bool[] H00R36_n493ContagemResultado_DemandaFM ;
      private String[] H00R36_A457ContagemResultado_Demanda ;
      private bool[] H00R36_n457ContagemResultado_Demanda ;
      private int[] H00R37_A456ContagemResultado_Codigo ;
      private String[] H00R37_A493ContagemResultado_DemandaFM ;
      private bool[] H00R37_n493ContagemResultado_DemandaFM ;
      private String[] H00R37_A457ContagemResultado_Demanda ;
      private bool[] H00R37_n457ContagemResultado_Demanda ;
      private int[] H00R38_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00R38_n1553ContagemResultado_CntSrvCod ;
      private int[] H00R38_A1603ContagemResultado_CntCod ;
      private bool[] H00R38_n1603ContagemResultado_CntCod ;
      private int[] H00R38_A1605ContagemResultado_CntPrpPesCod ;
      private bool[] H00R38_n1605ContagemResultado_CntPrpPesCod ;
      private int[] H00R38_A456ContagemResultado_Codigo ;
      private int[] H00R38_A508ContagemResultado_Owner ;
      private bool[] H00R38_n508ContagemResultado_Owner ;
      private int[] H00R38_A1604ContagemResultado_CntPrpCod ;
      private bool[] H00R38_n1604ContagemResultado_CntPrpCod ;
      private int[] H00R38_A890ContagemResultado_Responsavel ;
      private bool[] H00R38_n890ContagemResultado_Responsavel ;
      private String[] H00R38_A1606ContagemResultado_CntPrpPesNom ;
      private bool[] H00R38_n1606ContagemResultado_CntPrpPesNom ;
      private int[] H00R39_A1219LogResponsavel_OwnerPessoaCod ;
      private bool[] H00R39_n1219LogResponsavel_OwnerPessoaCod ;
      private int[] H00R39_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00R39_n1553ContagemResultado_CntSrvCod ;
      private int[] H00R39_A896LogResponsavel_Owner ;
      private int[] H00R39_A892LogResponsavel_DemandaCod ;
      private bool[] H00R39_n892LogResponsavel_DemandaCod ;
      private int[] H00R39_A1603ContagemResultado_CntCod ;
      private bool[] H00R39_n1603ContagemResultado_CntCod ;
      private String[] H00R39_A1222LogResponsavel_OwnerPessoaNom ;
      private bool[] H00R39_n1222LogResponsavel_OwnerPessoaNom ;
      private int[] H00R310_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00R310_n1553ContagemResultado_CntSrvCod ;
      private int[] H00R310_A1989ContagemResultadoQA_UserPesCod ;
      private bool[] H00R310_n1989ContagemResultadoQA_UserPesCod ;
      private int[] H00R310_A1985ContagemResultadoQA_OSCod ;
      private int[] H00R310_A508ContagemResultado_Owner ;
      private bool[] H00R310_n508ContagemResultado_Owner ;
      private int[] H00R310_A1988ContagemResultadoQA_UserCod ;
      private int[] H00R310_A890ContagemResultado_Responsavel ;
      private bool[] H00R310_n890ContagemResultado_Responsavel ;
      private String[] H00R310_A1990ContagemResultadoQA_UserPesNom ;
      private bool[] H00R310_n1990ContagemResultadoQA_UserPesNom ;
      private int[] H00R310_A1603ContagemResultado_CntCod ;
      private bool[] H00R310_n1603ContagemResultado_CntCod ;
      private int[] H00R310_A1984ContagemResultadoQA_Codigo ;
      private int[] H00R311_A456ContagemResultado_Codigo ;
      private int[] H00R311_A508ContagemResultado_Owner ;
      private bool[] H00R311_n508ContagemResultado_Owner ;
      private int[] H00R311_A890ContagemResultado_Responsavel ;
      private bool[] H00R311_n890ContagemResultado_Responsavel ;
      private int[] H00R312_A1080ContratoGestor_UsuarioPesCod ;
      private bool[] H00R312_n1080ContratoGestor_UsuarioPesCod ;
      private int[] H00R312_A1078ContratoGestor_ContratoCod ;
      private String[] H00R312_A1081ContratoGestor_UsuarioPesNom ;
      private bool[] H00R312_n1081ContratoGestor_UsuarioPesNom ;
      private int[] H00R312_A1079ContratoGestor_UsuarioCod ;
      private int[] H00R312_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00R312_n1446ContratoGestor_ContratadaAreaCod ;
      private String[] H00R314_A40000Usuario_PessoaNom ;
      private String[] H00R316_A40001Usuario_PessoaNom ;
      private int[] H00R317_A1984ContagemResultadoQA_Codigo ;
      private DateTime[] H00R317_A1986ContagemResultadoQA_DataHora ;
      private String[] H00R317_A1987ContagemResultadoQA_Texto ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV37WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV20Usuarios ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV38Codigos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV33Attachments ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV49GXV1 ;
      private GXWebForm Form ;
      private SdtContagemResultadoQA AV6ContagemResultadoQA ;
      private SdtMessages_Message AV9Message ;
      private wwpbaseobjects.SdtWWPContext AV30WWPContext ;
   }

   public class wp_qa__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00R33 ;
          prmH00R33 = new Object[] {
          new Object[] {"@AV18Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R35 ;
          prmH00R35 = new Object[] {
          new Object[] {"@AV19Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R36 ;
          prmH00R36 = new Object[] {
          new Object[] {"@AV17RespostaDe",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R37 ;
          prmH00R37 = new Object[] {
          new Object[] {"@AV16Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R38 ;
          prmH00R38 = new Object[] {
          new Object[] {"@AV16Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R39 ;
          prmH00R39 = new Object[] {
          new Object[] {"@AV16Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11UserId",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R310 ;
          prmH00R310 = new Object[] {
          new Object[] {"@AV16Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R311 ;
          prmH00R311 = new Object[] {
          new Object[] {"@AV16Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R312 ;
          prmH00R312 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R314 ;
          prmH00R314 = new Object[] {
          new Object[] {"@AV18Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R316 ;
          prmH00R316 = new Object[] {
          new Object[] {"@AV19Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R317 ;
          prmH00R317 = new Object[] {
          new Object[] {"@AV17RespostaDe",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00R33", "SELECT COALESCE( T1.[Usuario_PessoaNom], '') AS Usuario_PessoaNom FROM (SELECT MIN(T3.[Pessoa_Nome]) AS Usuario_PessoaNom FROM ([Usuario] T2 WITH (NOLOCK) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T2.[Usuario_Codigo] = @AV18Owner ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R33,1,0,true,true )
             ,new CursorDef("H00R35", "SELECT COALESCE( T1.[Usuario_PessoaNom], '') AS Usuario_PessoaNom FROM (SELECT MIN(T3.[Pessoa_Nome]) AS Usuario_PessoaNom FROM ([Usuario] T2 WITH (NOLOCK) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T2.[Usuario_Codigo] = @AV19Responsavel ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R35,1,0,true,true )
             ,new CursorDef("H00R36", "SELECT TOP 1 T1.[ContagemResultadoQA_OSCod] AS ContagemResultadoQA_OSCod, T3.[Usuario_PessoaCod] AS ContagemResultadoQA_UserPesCod, T1.[ContagemResultadoQA_Codigo], T4.[Pessoa_Nome] AS ContagemResultadoQA_UserPesNom, T1.[ContagemResultadoQA_UserCod] AS ContagemResultadoQA_UserCod, T2.[ContagemResultado_DemandaFM], T2.[ContagemResultado_Demanda] FROM ((([ContagemResultadoQA] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoQA_OSCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContagemResultadoQA_UserCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE T1.[ContagemResultadoQA_Codigo] = @AV17RespostaDe ORDER BY T1.[ContagemResultadoQA_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R36,1,0,false,true )
             ,new CursorDef("H00R37", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DemandaFM], [ContagemResultado_Demanda] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV16Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R37,1,0,false,true )
             ,new CursorDef("H00R38", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T4.[Usuario_PessoaCod] AS ContagemResultado_CntPrpPesCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Owner], T3.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod, T1.[ContagemResultado_Responsavel], T5.[Pessoa_Nome] AS ContagemResultado_CntPrpPesNom FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T3.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE T1.[ContagemResultado_Codigo] = @AV16Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R38,1,0,true,true )
             ,new CursorDef("H00R39", "SELECT DISTINCT NULL AS [LogResponsavel_OwnerPessoaCod], NULL AS [ContagemResultado_CntSrvCod], [LogResponsavel_Owner], NULL AS [LogResponsavel_DemandaCod], [ContagemResultado_CntCod], [LogResponsavel_OwnerPessoaNom] FROM ( SELECT TOP(100) PERCENT T2.[Usuario_PessoaCod] AS LogResponsavel_OwnerPessoaCod, T4.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[LogResponsavel_Owner] AS LogResponsavel_Owner, T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T5.[Contrato_Codigo] AS ContagemResultado_CntCod, T3.[Pessoa_Nome] AS LogResponsavel_OwnerPessoaNom FROM (((([LogResponsavel] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[LogResponsavel_Owner]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T4.[ContagemResultado_CntSrvCod]) WHERE (T1.[LogResponsavel_DemandaCod] = @AV16Codigo) AND (T1.[LogResponsavel_Owner] <> @AV11UserId) ORDER BY T1.[LogResponsavel_DemandaCod]) DistinctT ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R39,100,0,false,false )
             ,new CursorDef("H00R310", "SELECT TOP 1 T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Usuario_PessoaCod] AS ContagemResultadoQA_UserPesCod, T1.[ContagemResultadoQA_OSCod] AS ContagemResultadoQA_OSCod, T2.[ContagemResultado_Owner], T1.[ContagemResultadoQA_UserCod] AS ContagemResultadoQA_UserCod, T2.[ContagemResultado_Responsavel], T5.[Pessoa_Nome] AS ContagemResultadoQA_UserPesNom, T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultadoQA_Codigo] FROM (((([ContagemResultadoQA] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoQA_OSCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContagemResultadoQA_UserCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE T1.[ContagemResultadoQA_OSCod] = @AV16Codigo ORDER BY T1.[ContagemResultadoQA_OSCod], T1.[ContagemResultadoQA_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R310,1,0,true,true )
             ,new CursorDef("H00R311", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Owner], [ContagemResultado_Responsavel] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV16Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R311,1,0,true,true )
             ,new CursorDef("H00R312", "SELECT T3.[Usuario_PessoaCod] AS ContratoGestor_UsuarioPesCod, T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T4.[Pessoa_Nome] AS ContratoGestor_UsuarioPesNom, T1.[ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod, T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ((([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratoGestor_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE T1.[ContratoGestor_ContratoCod] = @AV8Contrato_Codigo ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R312,100,0,true,false )
             ,new CursorDef("H00R314", "SELECT COALESCE( T1.[Usuario_PessoaNom], '') AS Usuario_PessoaNom FROM (SELECT MIN(T3.[Pessoa_Nome]) AS Usuario_PessoaNom FROM ([Usuario] T2 WITH (NOLOCK) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T2.[Usuario_Codigo] = @AV18Owner ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R314,1,0,true,true )
             ,new CursorDef("H00R316", "SELECT COALESCE( T1.[Usuario_PessoaNom], '') AS Usuario_PessoaNom FROM (SELECT MIN(T3.[Pessoa_Nome]) AS Usuario_PessoaNom FROM ([Usuario] T2 WITH (NOLOCK) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T2.[Usuario_Codigo] = @AV19Responsavel ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R316,1,0,true,true )
             ,new CursorDef("H00R317", "SELECT TOP 1 [ContagemResultadoQA_Codigo], [ContagemResultadoQA_DataHora], [ContagemResultadoQA_Texto] FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_Codigo] = @AV17RespostaDe ORDER BY [ContagemResultadoQA_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R317,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
