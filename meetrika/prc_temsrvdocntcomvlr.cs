/*
               File: PRC_TemSrvDoCntComVlr
        Description: Tem Srv Do Cnt Com Vlr
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:37.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_temsrvdocntcomvlr : GXProcedure
   {
      public prc_temsrvdocntcomvlr( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_temsrvdocntcomvlr( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contrato_Codigo ,
                           out bool aP1_Flag )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV8Flag = false ;
         initialize();
         executePrivate();
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
         aP1_Flag=this.AV8Flag;
      }

      public bool executeUdp( ref int aP0_Contrato_Codigo )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV8Flag = false ;
         initialize();
         executePrivate();
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
         aP1_Flag=this.AV8Flag;
         return AV8Flag ;
      }

      public void executeSubmit( ref int aP0_Contrato_Codigo ,
                                 out bool aP1_Flag )
      {
         prc_temsrvdocntcomvlr objprc_temsrvdocntcomvlr;
         objprc_temsrvdocntcomvlr = new prc_temsrvdocntcomvlr();
         objprc_temsrvdocntcomvlr.A74Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_temsrvdocntcomvlr.AV8Flag = false ;
         objprc_temsrvdocntcomvlr.context.SetSubmitInitialConfig(context);
         objprc_temsrvdocntcomvlr.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_temsrvdocntcomvlr);
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
         aP1_Flag=this.AV8Flag;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_temsrvdocntcomvlr)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00BC2 */
         pr_default.execute(0, new Object[] {A74Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A557Servico_VlrUnidadeContratada = P00BC2_A557Servico_VlrUnidadeContratada[0];
            A160ContratoServicos_Codigo = P00BC2_A160ContratoServicos_Codigo[0];
            AV8Flag = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00BC2_A74Contrato_Codigo = new int[1] ;
         P00BC2_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         P00BC2_A160ContratoServicos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_temsrvdocntcomvlr__default(),
            new Object[][] {
                new Object[] {
               P00BC2_A74Contrato_Codigo, P00BC2_A557Servico_VlrUnidadeContratada, P00BC2_A160ContratoServicos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private String scmdbuf ;
      private bool AV8Flag ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contrato_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00BC2_A74Contrato_Codigo ;
      private decimal[] P00BC2_A557Servico_VlrUnidadeContratada ;
      private int[] P00BC2_A160ContratoServicos_Codigo ;
      private bool aP1_Flag ;
   }

   public class prc_temsrvdocntcomvlr__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BC2 ;
          prmP00BC2 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BC2", "SELECT TOP 1 [Contrato_Codigo], [Servico_VlrUnidadeContratada], [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @Contrato_Codigo) AND ([Servico_VlrUnidadeContratada] > 0) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BC2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
