/*
               File: WWAmbienteTecnologico
        Description:  Ambiente Tecnologico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:39:29.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwambientetecnologico : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwambientetecnologico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwambientetecnologico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         dynavAmbientetecnologico_areatrabalhocod = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkAmbienteTecnologico_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_77 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_77_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_77_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV17DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
               AV19AmbienteTecnologico_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19AmbienteTecnologico_Descricao1", AV19AmbienteTecnologico_Descricao1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV23AmbienteTecnologico_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23AmbienteTecnologico_Descricao2", AV23AmbienteTecnologico_Descricao2);
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV27AmbienteTecnologico_Descricao3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27AmbienteTecnologico_Descricao3", AV27AmbienteTecnologico_Descricao3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV64TFAmbienteTecnologico_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFAmbienteTecnologico_Descricao", AV64TFAmbienteTecnologico_Descricao);
               AV65TFAmbienteTecnologico_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFAmbienteTecnologico_Descricao_Sel", AV65TFAmbienteTecnologico_Descricao_Sel);
               AV68TFAmbienteTecnologico_TaxaEntregaDsnv = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFAmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68TFAmbienteTecnologico_TaxaEntregaDsnv), 4, 0)));
               AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To), 4, 0)));
               AV72TFAmbienteTecnologico_TaxaEntregaMlhr = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFAmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72TFAmbienteTecnologico_TaxaEntregaMlhr), 4, 0)));
               AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To), 4, 0)));
               AV76TFAmbienteTecnologico_LocPF = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFAmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFAmbienteTecnologico_LocPF), 4, 0)));
               AV77TFAmbienteTecnologico_LocPF_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFAmbienteTecnologico_LocPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFAmbienteTecnologico_LocPF_To), 4, 0)));
               AV80TFAmbienteTecnologico_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAmbienteTecnologico_Ativo_Sel", StringUtil.Str( (decimal)(AV80TFAmbienteTecnologico_Ativo_Sel), 1, 0));
               AV83TFAmbienteTecnologico_FatorAjuste = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFAmbienteTecnologico_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV83TFAmbienteTecnologico_FatorAjuste, 8, 4)));
               AV84TFAmbienteTecnologico_FatorAjuste_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFAmbienteTecnologico_FatorAjuste_To", StringUtil.LTrim( StringUtil.Str( AV84TFAmbienteTecnologico_FatorAjuste_To, 8, 4)));
               AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
               AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace", AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace);
               AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace", AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace);
               AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace", AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace);
               AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace", AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace);
               AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace", AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace);
               AV59AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
               AV117Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV29DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersIgnoreFirst", AV29DynamicFiltersIgnoreFirst);
               AV28DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
               A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV23AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV64TFAmbienteTecnologico_Descricao, AV65TFAmbienteTecnologico_Descricao_Sel, AV68TFAmbienteTecnologico_TaxaEntregaDsnv, AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To, AV72TFAmbienteTecnologico_TaxaEntregaMlhr, AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To, AV76TFAmbienteTecnologico_LocPF, AV77TFAmbienteTecnologico_LocPF_To, AV80TFAmbienteTecnologico_Ativo_Sel, AV83TFAmbienteTecnologico_FatorAjuste, AV84TFAmbienteTecnologico_FatorAjuste_To, AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace, AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace, AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace, AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace, AV59AmbienteTecnologico_AreaTrabalhoCod, AV117Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA9P2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START9P2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117392961");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwambientetecnologico.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV17DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vAMBIENTETECNOLOGICO_DESCRICAO1", AV19AmbienteTecnologico_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vAMBIENTETECNOLOGICO_DESCRICAO2", AV23AmbienteTecnologico_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vAMBIENTETECNOLOGICO_DESCRICAO3", AV27AmbienteTecnologico_Descricao3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO", AV64TFAmbienteTecnologico_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL", AV65TFAmbienteTecnologico_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68TFAmbienteTecnologico_TaxaEntregaDsnv), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV72TFAmbienteTecnologico_TaxaEntregaMlhr), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_LOCPF", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76TFAmbienteTecnologico_LocPF), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_LOCPF_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77TFAmbienteTecnologico_LocPF_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80TFAmbienteTecnologico_Ativo_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_FATORAJUSTE", StringUtil.LTrim( StringUtil.NToC( AV83TFAmbienteTecnologico_FatorAjuste, 8, 4, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO", StringUtil.LTrim( StringUtil.NToC( AV84TFAmbienteTecnologico_FatorAjuste_To, 8, 4, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_77", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_77), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV88GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV89GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV86DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV86DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA", AV63AmbienteTecnologico_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA", AV63AmbienteTecnologico_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLEFILTERDATA", AV67AmbienteTecnologico_TaxaEntregaDsnvTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLEFILTERDATA", AV67AmbienteTecnologico_TaxaEntregaDsnvTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLEFILTERDATA", AV71AmbienteTecnologico_TaxaEntregaMlhrTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLEFILTERDATA", AV71AmbienteTecnologico_TaxaEntregaMlhrTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAMBIENTETECNOLOGICO_LOCPFTITLEFILTERDATA", AV75AmbienteTecnologico_LocPFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAMBIENTETECNOLOGICO_LOCPFTITLEFILTERDATA", AV75AmbienteTecnologico_LocPFTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAMBIENTETECNOLOGICO_ATIVOTITLEFILTERDATA", AV79AmbienteTecnologico_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAMBIENTETECNOLOGICO_ATIVOTITLEFILTERDATA", AV79AmbienteTecnologico_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAMBIENTETECNOLOGICO_FATORAJUSTETITLEFILTERDATA", AV82AmbienteTecnologico_FatorAjusteTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAMBIENTETECNOLOGICO_FATORAJUSTETITLEFILTERDATA", AV82AmbienteTecnologico_FatorAjusteTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV117Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV29DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV28DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Caption", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Cls", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Filteredtext_set", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Filteredtextto_set", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologico_taxaentregadsnv_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologico_taxaentregadsnv_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologico_taxaentregadsnv_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Filtertype", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Filterisrange", StringUtil.BoolToStr( Ddo_ambientetecnologico_taxaentregadsnv_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologico_taxaentregadsnv_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Rangefilterfrom", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Rangefilterto", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Caption", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Cls", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Filteredtext_set", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Filteredtextto_set", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologico_taxaentregamlhr_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologico_taxaentregamlhr_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologico_taxaentregamlhr_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Filtertype", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Filterisrange", StringUtil.BoolToStr( Ddo_ambientetecnologico_taxaentregamlhr_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologico_taxaentregamlhr_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Rangefilterfrom", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Rangefilterto", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Caption", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Cls", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Filteredtext_set", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Filteredtextto_set", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologico_locpf_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologico_locpf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologico_locpf_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Filtertype", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Filterisrange", StringUtil.BoolToStr( Ddo_ambientetecnologico_locpf_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologico_locpf_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Rangefilterfrom", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Rangefilterto", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Caption", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Cls", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologico_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologico_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologico_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologico_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Caption", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Cls", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Filteredtext_set", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Filteredtextto_set", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologico_fatorajuste_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologico_fatorajuste_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologico_fatorajuste_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Filtertype", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Filterisrange", StringUtil.BoolToStr( Ddo_ambientetecnologico_fatorajuste_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologico_fatorajuste_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Rangefilterfrom", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Rangefilterto", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Filteredtext_get", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Filteredtextto_get", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Filteredtext_get", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Filteredtextto_get", StringUtil.RTrim( Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Filteredtext_get", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_LOCPF_Filteredtextto_get", StringUtil.RTrim( Ddo_ambientetecnologico_locpf_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_ambientetecnologico_ativo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Filteredtext_get", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Filteredtextto_get", StringUtil.RTrim( Ddo_ambientetecnologico_fatorajuste_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE9P2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT9P2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwambientetecnologico.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWAmbienteTecnologico" ;
      }

      public override String GetPgmdesc( )
      {
         return " Ambiente Tecnologico" ;
      }

      protected void WB9P0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_9P2( true) ;
         }
         else
         {
            wb_table1_2_9P2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9P2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(94, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(95, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_descricao_Internalname, AV64TFAmbienteTecnologico_Descricao, StringUtil.RTrim( context.localUtil.Format( AV64TFAmbienteTecnologico_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_descricao_sel_Internalname, AV65TFAmbienteTecnologico_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV65TFAmbienteTecnologico_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_taxaentregadsnv_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68TFAmbienteTecnologico_TaxaEntregaDsnv), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV68TFAmbienteTecnologico_TaxaEntregaDsnv), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_taxaentregadsnv_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_taxaentregadsnv_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_taxaentregadsnv_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_taxaentregadsnv_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_taxaentregadsnv_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_taxaentregamlhr_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV72TFAmbienteTecnologico_TaxaEntregaMlhr), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV72TFAmbienteTecnologico_TaxaEntregaMlhr), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_taxaentregamlhr_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_taxaentregamlhr_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_taxaentregamlhr_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_taxaentregamlhr_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_taxaentregamlhr_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_locpf_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76TFAmbienteTecnologico_LocPF), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV76TFAmbienteTecnologico_LocPF), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_locpf_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_locpf_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_locpf_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77TFAmbienteTecnologico_LocPF_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV77TFAmbienteTecnologico_LocPF_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_locpf_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_locpf_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80TFAmbienteTecnologico_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV80TFAmbienteTecnologico_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_fatorajuste_Internalname, StringUtil.LTrim( StringUtil.NToC( AV83TFAmbienteTecnologico_FatorAjuste, 8, 4, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV83TFAmbienteTecnologico_FatorAjuste, "ZZ9.9999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_fatorajuste_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_fatorajuste_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAmbienteTecnologico.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_fatorajuste_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV84TFAmbienteTecnologico_FatorAjuste_To, 8, 4, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV84TFAmbienteTecnologico_FatorAjuste_To, "ZZ9.9999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_fatorajuste_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_fatorajuste_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAmbienteTecnologico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AMBIENTETECNOLOGICO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname, AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAmbienteTecnologico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologico_taxaentregadsnvtitlecontrolidtoreplace_Internalname, AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavDdo_ambientetecnologico_taxaentregadsnvtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAmbienteTecnologico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologico_taxaentregamlhrtitlecontrolidtoreplace_Internalname, AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_ambientetecnologico_taxaentregamlhrtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAmbienteTecnologico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AMBIENTETECNOLOGICO_LOCPFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologico_locpftitlecontrolidtoreplace_Internalname, AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavDdo_ambientetecnologico_locpftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAmbienteTecnologico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AMBIENTETECNOLOGICO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Internalname, AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", 0, edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAmbienteTecnologico.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AMBIENTETECNOLOGICO_FATORAJUSTEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_77_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologico_fatorajustetitlecontrolidtoreplace_Internalname, AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", 0, edtavDdo_ambientetecnologico_fatorajustetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAmbienteTecnologico.htm");
         }
         wbLoad = true;
      }

      protected void START9P2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Ambiente Tecnologico", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP9P0( ) ;
      }

      protected void WS9P2( )
      {
         START9P2( ) ;
         EVT9P2( ) ;
      }

      protected void EVT9P2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E119P2 */
                              E119P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E129P2 */
                              E129P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E139P2 */
                              E139P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E149P2 */
                              E149P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICO_LOCPF.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E159P2 */
                              E159P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICO_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E169P2 */
                              E169P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E179P2 */
                              E179P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E189P2 */
                              E189P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E199P2 */
                              E199P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E209P2 */
                              E209P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E219P2 */
                              E219P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E229P2 */
                              E229P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E239P2 */
                              E239P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E249P2 */
                              E249P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E259P2 */
                              E259P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E269P2 */
                              E269P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E279P2 */
                              E279P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E289P2 */
                              E289P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "'DOBTNASSOCIATION'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 25), "'DOBTNASSOCIATIONSISTEMA'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "'DOBTNASSOCIATION'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 25), "'DOBTNASSOCIATIONSISTEMA'") == 0 ) )
                           {
                              nGXsfl_77_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_77_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_77_idx), 4, 0)), 4, "0");
                              SubsflControlProps_772( ) ;
                              AV30Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30Update)) ? AV112Update_GXI : context.convertURL( context.PathToRelativeUrl( AV30Update))));
                              AV31Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Delete)) ? AV113Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV31Delete))));
                              AV58Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV58Display)) ? AV114Display_GXI : context.convertURL( context.PathToRelativeUrl( AV58Display))));
                              A351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_Codigo_Internalname), ",", "."));
                              A352AmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtAmbienteTecnologico_Descricao_Internalname));
                              A976AmbienteTecnologico_TaxaEntregaDsnv = (short)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname), ",", "."));
                              n976AmbienteTecnologico_TaxaEntregaDsnv = false;
                              A977AmbienteTecnologico_TaxaEntregaMlhr = (short)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname), ",", "."));
                              n977AmbienteTecnologico_TaxaEntregaMlhr = false;
                              A978AmbienteTecnologico_LocPF = (short)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_LocPF_Internalname), ",", "."));
                              n978AmbienteTecnologico_LocPF = false;
                              A353AmbienteTecnologico_Ativo = StringUtil.StrToBool( cgiGet( chkAmbienteTecnologico_Ativo_Internalname));
                              A1423AmbienteTecnologico_FatorAjuste = context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_FatorAjuste_Internalname), ",", ".");
                              n1423AmbienteTecnologico_FatorAjuste = false;
                              AV57btnAssociation = cgiGet( edtavBtnassociation_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociation_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV57btnAssociation)) ? AV115Btnassociation_GXI : context.convertURL( context.PathToRelativeUrl( AV57btnAssociation))));
                              AV60btnAssociationSistema = cgiGet( edtavBtnassociationsistema_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociationsistema_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV60btnAssociationSistema)) ? AV116Btnassociationsistema_GXI : context.convertURL( context.PathToRelativeUrl( AV60btnAssociationSistema))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E299P2 */
                                    E299P2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E309P2 */
                                    E309P2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E319P2 */
                                    E319P2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOBTNASSOCIATION'") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E329P2 */
                                    E329P2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOBTNASSOCIATIONSISTEMA'") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E339P2 */
                                    E339P2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV17DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ambientetecnologico_descricao1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vAMBIENTETECNOLOGICO_DESCRICAO1"), AV19AmbienteTecnologico_Descricao1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ambientetecnologico_descricao2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vAMBIENTETECNOLOGICO_DESCRICAO2"), AV23AmbienteTecnologico_Descricao2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ambientetecnologico_descricao3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vAMBIENTETECNOLOGICO_DESCRICAO3"), AV27AmbienteTecnologico_Descricao3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfambientetecnologico_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO"), AV64TFAmbienteTecnologico_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfambientetecnologico_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL"), AV65TFAmbienteTecnologico_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfambientetecnologico_taxaentregadsnv Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV"), ",", ".") != Convert.ToDecimal( AV68TFAmbienteTecnologico_TaxaEntregaDsnv )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfambientetecnologico_taxaentregadsnv_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO"), ",", ".") != Convert.ToDecimal( AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfambientetecnologico_taxaentregamlhr Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR"), ",", ".") != Convert.ToDecimal( AV72TFAmbienteTecnologico_TaxaEntregaMlhr )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfambientetecnologico_taxaentregamlhr_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO"), ",", ".") != Convert.ToDecimal( AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfambientetecnologico_locpf Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_LOCPF"), ",", ".") != Convert.ToDecimal( AV76TFAmbienteTecnologico_LocPF )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfambientetecnologico_locpf_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_LOCPF_TO"), ",", ".") != Convert.ToDecimal( AV77TFAmbienteTecnologico_LocPF_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfambientetecnologico_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV80TFAmbienteTecnologico_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfambientetecnologico_fatorajuste Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_FATORAJUSTE"), ",", ".") != AV83TFAmbienteTecnologico_FatorAjuste )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfambientetecnologico_fatorajuste_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO"), ",", ".") != AV84TFAmbienteTecnologico_FatorAjuste_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE9P2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA9P2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            dynavAmbientetecnologico_areatrabalhocod.Name = "vAMBIENTETECNOLOGICO_AREATRABALHOCOD";
            dynavAmbientetecnologico_areatrabalhocod.WebTags = "";
            dynavAmbientetecnologico_areatrabalhocod.removeAllItems();
            /* Using cursor H009P2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavAmbientetecnologico_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H009P2_A5AreaTrabalho_Codigo[0]), 6, 0)), H009P2_A6AreaTrabalho_Descricao[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavAmbientetecnologico_areatrabalhocod.ItemCount > 0 )
            {
               AV59AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavAmbientetecnologico_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("AMBIENTETECNOLOGICO_DESCRICAO", "Tecnol�gico", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV17DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV17DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("AMBIENTETECNOLOGICO_DESCRICAO", "Tecnol�gico", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("AMBIENTETECNOLOGICO_DESCRICAO", "Tecnol�gico", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            GXCCtl = "AMBIENTETECNOLOGICO_ATIVO_" + sGXsfl_77_idx;
            chkAmbienteTecnologico_Ativo.Name = GXCCtl;
            chkAmbienteTecnologico_Ativo.WebTags = "";
            chkAmbienteTecnologico_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAmbienteTecnologico_Ativo_Internalname, "TitleCaption", chkAmbienteTecnologico_Ativo.Caption);
            chkAmbienteTecnologico_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvAMBIENTETECNOLOGICO_AREATRABALHOCOD9P1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvAMBIENTETECNOLOGICO_AREATRABALHOCOD_data9P1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvAMBIENTETECNOLOGICO_AREATRABALHOCOD_html9P1( )
      {
         int gxdynajaxvalue ;
         GXDLVvAMBIENTETECNOLOGICO_AREATRABALHOCOD_data9P1( ) ;
         gxdynajaxindex = 1;
         dynavAmbientetecnologico_areatrabalhocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavAmbientetecnologico_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavAmbientetecnologico_areatrabalhocod.ItemCount > 0 )
         {
            AV59AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavAmbientetecnologico_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void GXDLVvAMBIENTETECNOLOGICO_AREATRABALHOCOD_data9P1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H009P3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H009P3_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H009P3_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_772( ) ;
         while ( nGXsfl_77_idx <= nRC_GXsfl_77 )
         {
            sendrow_772( ) ;
            nGXsfl_77_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_77_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_77_idx+1));
            sGXsfl_77_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_77_idx), 4, 0)), 4, "0");
            SubsflControlProps_772( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV17DynamicFiltersSelector1 ,
                                       String AV19AmbienteTecnologico_Descricao1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       String AV23AmbienteTecnologico_Descricao2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       String AV27AmbienteTecnologico_Descricao3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       String AV64TFAmbienteTecnologico_Descricao ,
                                       String AV65TFAmbienteTecnologico_Descricao_Sel ,
                                       short AV68TFAmbienteTecnologico_TaxaEntregaDsnv ,
                                       short AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To ,
                                       short AV72TFAmbienteTecnologico_TaxaEntregaMlhr ,
                                       short AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To ,
                                       short AV76TFAmbienteTecnologico_LocPF ,
                                       short AV77TFAmbienteTecnologico_LocPF_To ,
                                       short AV80TFAmbienteTecnologico_Ativo_Sel ,
                                       decimal AV83TFAmbienteTecnologico_FatorAjuste ,
                                       decimal AV84TFAmbienteTecnologico_FatorAjuste_To ,
                                       String AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace ,
                                       String AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace ,
                                       String AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace ,
                                       String AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace ,
                                       String AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace ,
                                       String AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace ,
                                       int AV59AmbienteTecnologico_AreaTrabalhoCod ,
                                       String AV117Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV29DynamicFiltersIgnoreFirst ,
                                       bool AV28DynamicFiltersRemoving ,
                                       int A351AmbienteTecnologico_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF9P2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_DESCRICAO", A352AmbienteTecnologico_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_TAXAENTREGADSNV", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_TAXAENTREGADSNV", StringUtil.LTrim( StringUtil.NToC( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_TAXAENTREGAMLHR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_LOCPF", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A978AmbienteTecnologico_LocPF), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_LOCPF", StringUtil.LTrim( StringUtil.NToC( (decimal)(A978AmbienteTecnologico_LocPF), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_ATIVO", GetSecureSignedToken( "", A353AmbienteTecnologico_Ativo));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_ATIVO", StringUtil.BoolToStr( A353AmbienteTecnologico_Ativo));
         GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_FATORAJUSTE", GetSecureSignedToken( "", context.localUtil.Format( A1423AmbienteTecnologico_FatorAjuste, "ZZ9.9999")));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_FATORAJUSTE", StringUtil.LTrim( StringUtil.NToC( A1423AmbienteTecnologico_FatorAjuste, 8, 4, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( dynavAmbientetecnologico_areatrabalhocod.ItemCount > 0 )
         {
            AV59AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavAmbientetecnologico_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV17DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV17DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9P2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV117Pgmname = "WWAmbienteTecnologico";
         context.Gx_err = 0;
      }

      protected void RF9P2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 77;
         /* Execute user event: E309P2 */
         E309P2 ();
         nGXsfl_77_idx = 1;
         sGXsfl_77_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_77_idx), 4, 0)), 4, "0");
         SubsflControlProps_772( ) ;
         nGXsfl_77_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_772( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 ,
                                                 AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 ,
                                                 AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 ,
                                                 AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 ,
                                                 AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 ,
                                                 AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 ,
                                                 AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 ,
                                                 AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 ,
                                                 AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel ,
                                                 AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao ,
                                                 AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv ,
                                                 AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to ,
                                                 AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr ,
                                                 AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to ,
                                                 AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf ,
                                                 AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to ,
                                                 AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel ,
                                                 AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste ,
                                                 AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to ,
                                                 A352AmbienteTecnologico_Descricao ,
                                                 A976AmbienteTecnologico_TaxaEntregaDsnv ,
                                                 A977AmbienteTecnologico_TaxaEntregaMlhr ,
                                                 A978AmbienteTecnologico_LocPF ,
                                                 A353AmbienteTecnologico_Ativo ,
                                                 A1423AmbienteTecnologico_FatorAjuste ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A728AmbienteTecnologico_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1), "%", "");
            lV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2), "%", "");
            lV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3), "%", "");
            lV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = StringUtil.Concat( StringUtil.RTrim( AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao), "%", "");
            /* Using cursor H009P4 */
            pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1, lV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2, lV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3, lV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao, AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel, AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv, AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to, AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr, AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to, AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf, AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to, AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste, AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_77_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A728AmbienteTecnologico_AreaTrabalhoCod = H009P4_A728AmbienteTecnologico_AreaTrabalhoCod[0];
               A1423AmbienteTecnologico_FatorAjuste = H009P4_A1423AmbienteTecnologico_FatorAjuste[0];
               n1423AmbienteTecnologico_FatorAjuste = H009P4_n1423AmbienteTecnologico_FatorAjuste[0];
               A353AmbienteTecnologico_Ativo = H009P4_A353AmbienteTecnologico_Ativo[0];
               A978AmbienteTecnologico_LocPF = H009P4_A978AmbienteTecnologico_LocPF[0];
               n978AmbienteTecnologico_LocPF = H009P4_n978AmbienteTecnologico_LocPF[0];
               A977AmbienteTecnologico_TaxaEntregaMlhr = H009P4_A977AmbienteTecnologico_TaxaEntregaMlhr[0];
               n977AmbienteTecnologico_TaxaEntregaMlhr = H009P4_n977AmbienteTecnologico_TaxaEntregaMlhr[0];
               A976AmbienteTecnologico_TaxaEntregaDsnv = H009P4_A976AmbienteTecnologico_TaxaEntregaDsnv[0];
               n976AmbienteTecnologico_TaxaEntregaDsnv = H009P4_n976AmbienteTecnologico_TaxaEntregaDsnv[0];
               A352AmbienteTecnologico_Descricao = H009P4_A352AmbienteTecnologico_Descricao[0];
               A351AmbienteTecnologico_Codigo = H009P4_A351AmbienteTecnologico_Codigo[0];
               /* Execute user event: E319P2 */
               E319P2 ();
               pr_default.readNext(2);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 77;
            WB9P0( ) ;
         }
         nGXsfl_77_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV92WWAmbienteTecnologicoDS_1_Ambientetecnologico_areatrabalhocod = AV59AmbienteTecnologico_AreaTrabalhoCod;
         AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = AV19AmbienteTecnologico_Descricao1;
         AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = AV23AmbienteTecnologico_Descricao2;
         AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = AV27AmbienteTecnologico_Descricao3;
         AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = AV64TFAmbienteTecnologico_Descricao;
         AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel = AV65TFAmbienteTecnologico_Descricao_Sel;
         AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv = AV68TFAmbienteTecnologico_TaxaEntregaDsnv;
         AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to = AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To;
         AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr = AV72TFAmbienteTecnologico_TaxaEntregaMlhr;
         AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to = AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To;
         AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf = AV76TFAmbienteTecnologico_LocPF;
         AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to = AV77TFAmbienteTecnologico_LocPF_To;
         AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel = AV80TFAmbienteTecnologico_Ativo_Sel;
         AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste = AV83TFAmbienteTecnologico_FatorAjuste;
         AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to = AV84TFAmbienteTecnologico_FatorAjuste_To;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 ,
                                              AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 ,
                                              AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 ,
                                              AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 ,
                                              AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 ,
                                              AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 ,
                                              AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 ,
                                              AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 ,
                                              AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel ,
                                              AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao ,
                                              AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv ,
                                              AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to ,
                                              AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr ,
                                              AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to ,
                                              AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf ,
                                              AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to ,
                                              AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel ,
                                              AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste ,
                                              AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A976AmbienteTecnologico_TaxaEntregaDsnv ,
                                              A977AmbienteTecnologico_TaxaEntregaMlhr ,
                                              A978AmbienteTecnologico_LocPF ,
                                              A353AmbienteTecnologico_Ativo ,
                                              A1423AmbienteTecnologico_FatorAjuste ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A728AmbienteTecnologico_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1), "%", "");
         lV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2), "%", "");
         lV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3), "%", "");
         lV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = StringUtil.Concat( StringUtil.RTrim( AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao), "%", "");
         /* Using cursor H009P5 */
         pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1, lV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2, lV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3, lV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao, AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel, AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv, AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to, AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr, AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to, AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf, AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to, AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste, AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to});
         GRID_nRecordCount = H009P5_AGRID_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV92WWAmbienteTecnologicoDS_1_Ambientetecnologico_areatrabalhocod = AV59AmbienteTecnologico_AreaTrabalhoCod;
         AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = AV19AmbienteTecnologico_Descricao1;
         AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = AV23AmbienteTecnologico_Descricao2;
         AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = AV27AmbienteTecnologico_Descricao3;
         AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = AV64TFAmbienteTecnologico_Descricao;
         AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel = AV65TFAmbienteTecnologico_Descricao_Sel;
         AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv = AV68TFAmbienteTecnologico_TaxaEntregaDsnv;
         AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to = AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To;
         AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr = AV72TFAmbienteTecnologico_TaxaEntregaMlhr;
         AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to = AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To;
         AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf = AV76TFAmbienteTecnologico_LocPF;
         AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to = AV77TFAmbienteTecnologico_LocPF_To;
         AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel = AV80TFAmbienteTecnologico_Ativo_Sel;
         AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste = AV83TFAmbienteTecnologico_FatorAjuste;
         AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to = AV84TFAmbienteTecnologico_FatorAjuste_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV23AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV64TFAmbienteTecnologico_Descricao, AV65TFAmbienteTecnologico_Descricao_Sel, AV68TFAmbienteTecnologico_TaxaEntregaDsnv, AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To, AV72TFAmbienteTecnologico_TaxaEntregaMlhr, AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To, AV76TFAmbienteTecnologico_LocPF, AV77TFAmbienteTecnologico_LocPF_To, AV80TFAmbienteTecnologico_Ativo_Sel, AV83TFAmbienteTecnologico_FatorAjuste, AV84TFAmbienteTecnologico_FatorAjuste_To, AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace, AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace, AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace, AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace, AV59AmbienteTecnologico_AreaTrabalhoCod, AV117Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV92WWAmbienteTecnologicoDS_1_Ambientetecnologico_areatrabalhocod = AV59AmbienteTecnologico_AreaTrabalhoCod;
         AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = AV19AmbienteTecnologico_Descricao1;
         AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = AV23AmbienteTecnologico_Descricao2;
         AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = AV27AmbienteTecnologico_Descricao3;
         AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = AV64TFAmbienteTecnologico_Descricao;
         AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel = AV65TFAmbienteTecnologico_Descricao_Sel;
         AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv = AV68TFAmbienteTecnologico_TaxaEntregaDsnv;
         AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to = AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To;
         AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr = AV72TFAmbienteTecnologico_TaxaEntregaMlhr;
         AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to = AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To;
         AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf = AV76TFAmbienteTecnologico_LocPF;
         AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to = AV77TFAmbienteTecnologico_LocPF_To;
         AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel = AV80TFAmbienteTecnologico_Ativo_Sel;
         AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste = AV83TFAmbienteTecnologico_FatorAjuste;
         AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to = AV84TFAmbienteTecnologico_FatorAjuste_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV23AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV64TFAmbienteTecnologico_Descricao, AV65TFAmbienteTecnologico_Descricao_Sel, AV68TFAmbienteTecnologico_TaxaEntregaDsnv, AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To, AV72TFAmbienteTecnologico_TaxaEntregaMlhr, AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To, AV76TFAmbienteTecnologico_LocPF, AV77TFAmbienteTecnologico_LocPF_To, AV80TFAmbienteTecnologico_Ativo_Sel, AV83TFAmbienteTecnologico_FatorAjuste, AV84TFAmbienteTecnologico_FatorAjuste_To, AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace, AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace, AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace, AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace, AV59AmbienteTecnologico_AreaTrabalhoCod, AV117Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV92WWAmbienteTecnologicoDS_1_Ambientetecnologico_areatrabalhocod = AV59AmbienteTecnologico_AreaTrabalhoCod;
         AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = AV19AmbienteTecnologico_Descricao1;
         AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = AV23AmbienteTecnologico_Descricao2;
         AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = AV27AmbienteTecnologico_Descricao3;
         AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = AV64TFAmbienteTecnologico_Descricao;
         AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel = AV65TFAmbienteTecnologico_Descricao_Sel;
         AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv = AV68TFAmbienteTecnologico_TaxaEntregaDsnv;
         AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to = AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To;
         AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr = AV72TFAmbienteTecnologico_TaxaEntregaMlhr;
         AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to = AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To;
         AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf = AV76TFAmbienteTecnologico_LocPF;
         AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to = AV77TFAmbienteTecnologico_LocPF_To;
         AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel = AV80TFAmbienteTecnologico_Ativo_Sel;
         AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste = AV83TFAmbienteTecnologico_FatorAjuste;
         AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to = AV84TFAmbienteTecnologico_FatorAjuste_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV23AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV64TFAmbienteTecnologico_Descricao, AV65TFAmbienteTecnologico_Descricao_Sel, AV68TFAmbienteTecnologico_TaxaEntregaDsnv, AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To, AV72TFAmbienteTecnologico_TaxaEntregaMlhr, AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To, AV76TFAmbienteTecnologico_LocPF, AV77TFAmbienteTecnologico_LocPF_To, AV80TFAmbienteTecnologico_Ativo_Sel, AV83TFAmbienteTecnologico_FatorAjuste, AV84TFAmbienteTecnologico_FatorAjuste_To, AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace, AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace, AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace, AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace, AV59AmbienteTecnologico_AreaTrabalhoCod, AV117Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV92WWAmbienteTecnologicoDS_1_Ambientetecnologico_areatrabalhocod = AV59AmbienteTecnologico_AreaTrabalhoCod;
         AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = AV19AmbienteTecnologico_Descricao1;
         AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = AV23AmbienteTecnologico_Descricao2;
         AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = AV27AmbienteTecnologico_Descricao3;
         AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = AV64TFAmbienteTecnologico_Descricao;
         AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel = AV65TFAmbienteTecnologico_Descricao_Sel;
         AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv = AV68TFAmbienteTecnologico_TaxaEntregaDsnv;
         AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to = AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To;
         AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr = AV72TFAmbienteTecnologico_TaxaEntregaMlhr;
         AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to = AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To;
         AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf = AV76TFAmbienteTecnologico_LocPF;
         AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to = AV77TFAmbienteTecnologico_LocPF_To;
         AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel = AV80TFAmbienteTecnologico_Ativo_Sel;
         AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste = AV83TFAmbienteTecnologico_FatorAjuste;
         AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to = AV84TFAmbienteTecnologico_FatorAjuste_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV23AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV64TFAmbienteTecnologico_Descricao, AV65TFAmbienteTecnologico_Descricao_Sel, AV68TFAmbienteTecnologico_TaxaEntregaDsnv, AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To, AV72TFAmbienteTecnologico_TaxaEntregaMlhr, AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To, AV76TFAmbienteTecnologico_LocPF, AV77TFAmbienteTecnologico_LocPF_To, AV80TFAmbienteTecnologico_Ativo_Sel, AV83TFAmbienteTecnologico_FatorAjuste, AV84TFAmbienteTecnologico_FatorAjuste_To, AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace, AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace, AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace, AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace, AV59AmbienteTecnologico_AreaTrabalhoCod, AV117Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV92WWAmbienteTecnologicoDS_1_Ambientetecnologico_areatrabalhocod = AV59AmbienteTecnologico_AreaTrabalhoCod;
         AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = AV19AmbienteTecnologico_Descricao1;
         AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = AV23AmbienteTecnologico_Descricao2;
         AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = AV27AmbienteTecnologico_Descricao3;
         AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = AV64TFAmbienteTecnologico_Descricao;
         AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel = AV65TFAmbienteTecnologico_Descricao_Sel;
         AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv = AV68TFAmbienteTecnologico_TaxaEntregaDsnv;
         AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to = AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To;
         AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr = AV72TFAmbienteTecnologico_TaxaEntregaMlhr;
         AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to = AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To;
         AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf = AV76TFAmbienteTecnologico_LocPF;
         AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to = AV77TFAmbienteTecnologico_LocPF_To;
         AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel = AV80TFAmbienteTecnologico_Ativo_Sel;
         AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste = AV83TFAmbienteTecnologico_FatorAjuste;
         AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to = AV84TFAmbienteTecnologico_FatorAjuste_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV23AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV64TFAmbienteTecnologico_Descricao, AV65TFAmbienteTecnologico_Descricao_Sel, AV68TFAmbienteTecnologico_TaxaEntregaDsnv, AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To, AV72TFAmbienteTecnologico_TaxaEntregaMlhr, AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To, AV76TFAmbienteTecnologico_LocPF, AV77TFAmbienteTecnologico_LocPF_To, AV80TFAmbienteTecnologico_Ativo_Sel, AV83TFAmbienteTecnologico_FatorAjuste, AV84TFAmbienteTecnologico_FatorAjuste_To, AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace, AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace, AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace, AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace, AV59AmbienteTecnologico_AreaTrabalhoCod, AV117Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP9P0( )
      {
         /* Before Start, stand alone formulas. */
         AV117Pgmname = "WWAmbienteTecnologico";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E299P2 */
         E299P2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV86DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA"), AV63AmbienteTecnologico_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLEFILTERDATA"), AV67AmbienteTecnologico_TaxaEntregaDsnvTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLEFILTERDATA"), AV71AmbienteTecnologico_TaxaEntregaMlhrTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAMBIENTETECNOLOGICO_LOCPFTITLEFILTERDATA"), AV75AmbienteTecnologico_LocPFTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAMBIENTETECNOLOGICO_ATIVOTITLEFILTERDATA"), AV79AmbienteTecnologico_AtivoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAMBIENTETECNOLOGICO_FATORAJUSTETITLEFILTERDATA"), AV82AmbienteTecnologico_FatorAjusteTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            dynavAmbientetecnologico_areatrabalhocod.Name = dynavAmbientetecnologico_areatrabalhocod_Internalname;
            dynavAmbientetecnologico_areatrabalhocod.CurrentValue = cgiGet( dynavAmbientetecnologico_areatrabalhocod_Internalname);
            AV59AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynavAmbientetecnologico_areatrabalhocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV17DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            AV19AmbienteTecnologico_Descricao1 = StringUtil.Upper( cgiGet( edtavAmbientetecnologico_descricao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19AmbienteTecnologico_Descricao1", AV19AmbienteTecnologico_Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            AV23AmbienteTecnologico_Descricao2 = StringUtil.Upper( cgiGet( edtavAmbientetecnologico_descricao2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23AmbienteTecnologico_Descricao2", AV23AmbienteTecnologico_Descricao2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            AV27AmbienteTecnologico_Descricao3 = StringUtil.Upper( cgiGet( edtavAmbientetecnologico_descricao3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27AmbienteTecnologico_Descricao3", AV27AmbienteTecnologico_Descricao3);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            AV64TFAmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtavTfambientetecnologico_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFAmbienteTecnologico_Descricao", AV64TFAmbienteTecnologico_Descricao);
            AV65TFAmbienteTecnologico_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfambientetecnologico_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFAmbienteTecnologico_Descricao_Sel", AV65TFAmbienteTecnologico_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_taxaentregadsnv_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_taxaentregadsnv_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV");
               GX_FocusControl = edtavTfambientetecnologico_taxaentregadsnv_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68TFAmbienteTecnologico_TaxaEntregaDsnv = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFAmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68TFAmbienteTecnologico_TaxaEntregaDsnv), 4, 0)));
            }
            else
            {
               AV68TFAmbienteTecnologico_TaxaEntregaDsnv = (short)(context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_taxaentregadsnv_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFAmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68TFAmbienteTecnologico_TaxaEntregaDsnv), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_taxaentregadsnv_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_taxaentregadsnv_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO");
               GX_FocusControl = edtavTfambientetecnologico_taxaentregadsnv_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To), 4, 0)));
            }
            else
            {
               AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To = (short)(context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_taxaentregadsnv_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_taxaentregamlhr_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_taxaentregamlhr_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR");
               GX_FocusControl = edtavTfambientetecnologico_taxaentregamlhr_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV72TFAmbienteTecnologico_TaxaEntregaMlhr = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFAmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72TFAmbienteTecnologico_TaxaEntregaMlhr), 4, 0)));
            }
            else
            {
               AV72TFAmbienteTecnologico_TaxaEntregaMlhr = (short)(context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_taxaentregamlhr_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFAmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72TFAmbienteTecnologico_TaxaEntregaMlhr), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_taxaentregamlhr_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_taxaentregamlhr_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO");
               GX_FocusControl = edtavTfambientetecnologico_taxaentregamlhr_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To), 4, 0)));
            }
            else
            {
               AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To = (short)(context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_taxaentregamlhr_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_locpf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_locpf_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAMBIENTETECNOLOGICO_LOCPF");
               GX_FocusControl = edtavTfambientetecnologico_locpf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV76TFAmbienteTecnologico_LocPF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFAmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFAmbienteTecnologico_LocPF), 4, 0)));
            }
            else
            {
               AV76TFAmbienteTecnologico_LocPF = (short)(context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_locpf_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFAmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFAmbienteTecnologico_LocPF), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_locpf_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_locpf_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAMBIENTETECNOLOGICO_LOCPF_TO");
               GX_FocusControl = edtavTfambientetecnologico_locpf_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV77TFAmbienteTecnologico_LocPF_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFAmbienteTecnologico_LocPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFAmbienteTecnologico_LocPF_To), 4, 0)));
            }
            else
            {
               AV77TFAmbienteTecnologico_LocPF_To = (short)(context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_locpf_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFAmbienteTecnologico_LocPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFAmbienteTecnologico_LocPF_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAMBIENTETECNOLOGICO_ATIVO_SEL");
               GX_FocusControl = edtavTfambientetecnologico_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV80TFAmbienteTecnologico_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAmbienteTecnologico_Ativo_Sel", StringUtil.Str( (decimal)(AV80TFAmbienteTecnologico_Ativo_Sel), 1, 0));
            }
            else
            {
               AV80TFAmbienteTecnologico_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAmbienteTecnologico_Ativo_Sel", StringUtil.Str( (decimal)(AV80TFAmbienteTecnologico_Ativo_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_fatorajuste_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_fatorajuste_Internalname), ",", ".") > 999.9999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAMBIENTETECNOLOGICO_FATORAJUSTE");
               GX_FocusControl = edtavTfambientetecnologico_fatorajuste_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83TFAmbienteTecnologico_FatorAjuste = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFAmbienteTecnologico_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV83TFAmbienteTecnologico_FatorAjuste, 8, 4)));
            }
            else
            {
               AV83TFAmbienteTecnologico_FatorAjuste = context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_fatorajuste_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFAmbienteTecnologico_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV83TFAmbienteTecnologico_FatorAjuste, 8, 4)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_fatorajuste_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_fatorajuste_to_Internalname), ",", ".") > 999.9999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO");
               GX_FocusControl = edtavTfambientetecnologico_fatorajuste_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV84TFAmbienteTecnologico_FatorAjuste_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFAmbienteTecnologico_FatorAjuste_To", StringUtil.LTrim( StringUtil.Str( AV84TFAmbienteTecnologico_FatorAjuste_To, 8, 4)));
            }
            else
            {
               AV84TFAmbienteTecnologico_FatorAjuste_To = context.localUtil.CToN( cgiGet( edtavTfambientetecnologico_fatorajuste_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFAmbienteTecnologico_FatorAjuste_To", StringUtil.LTrim( StringUtil.Str( AV84TFAmbienteTecnologico_FatorAjuste_To, 8, 4)));
            }
            AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
            AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologico_taxaentregadsnvtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace", AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace);
            AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologico_taxaentregamlhrtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace", AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace);
            AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologico_locpftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace", AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace);
            AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace", AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace);
            AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologico_fatorajustetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace", AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_77 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_77"), ",", "."));
            AV88GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV89GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_ambientetecnologico_descricao_Caption = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Caption");
            Ddo_ambientetecnologico_descricao_Tooltip = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Tooltip");
            Ddo_ambientetecnologico_descricao_Cls = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cls");
            Ddo_ambientetecnologico_descricao_Filteredtext_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_set");
            Ddo_ambientetecnologico_descricao_Selectedvalue_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_set");
            Ddo_ambientetecnologico_descricao_Dropdownoptionstype = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Dropdownoptionstype");
            Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_ambientetecnologico_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortasc"));
            Ddo_ambientetecnologico_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortdsc"));
            Ddo_ambientetecnologico_descricao_Sortedstatus = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortedstatus");
            Ddo_ambientetecnologico_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includefilter"));
            Ddo_ambientetecnologico_descricao_Filtertype = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filtertype");
            Ddo_ambientetecnologico_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filterisrange"));
            Ddo_ambientetecnologico_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includedatalist"));
            Ddo_ambientetecnologico_descricao_Datalisttype = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalisttype");
            Ddo_ambientetecnologico_descricao_Datalistproc = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistproc");
            Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_ambientetecnologico_descricao_Sortasc = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortasc");
            Ddo_ambientetecnologico_descricao_Sortdsc = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortdsc");
            Ddo_ambientetecnologico_descricao_Loadingdata = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Loadingdata");
            Ddo_ambientetecnologico_descricao_Cleanfilter = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cleanfilter");
            Ddo_ambientetecnologico_descricao_Noresultsfound = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Noresultsfound");
            Ddo_ambientetecnologico_descricao_Searchbuttontext = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Searchbuttontext");
            Ddo_ambientetecnologico_taxaentregadsnv_Caption = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Caption");
            Ddo_ambientetecnologico_taxaentregadsnv_Tooltip = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Tooltip");
            Ddo_ambientetecnologico_taxaentregadsnv_Cls = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Cls");
            Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Filteredtext_set");
            Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Filteredtextto_set");
            Ddo_ambientetecnologico_taxaentregadsnv_Dropdownoptionstype = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Dropdownoptionstype");
            Ddo_ambientetecnologico_taxaentregadsnv_Titlecontrolidtoreplace = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Titlecontrolidtoreplace");
            Ddo_ambientetecnologico_taxaentregadsnv_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Includesortasc"));
            Ddo_ambientetecnologico_taxaentregadsnv_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Includesortdsc"));
            Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Sortedstatus");
            Ddo_ambientetecnologico_taxaentregadsnv_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Includefilter"));
            Ddo_ambientetecnologico_taxaentregadsnv_Filtertype = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Filtertype");
            Ddo_ambientetecnologico_taxaentregadsnv_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Filterisrange"));
            Ddo_ambientetecnologico_taxaentregadsnv_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Includedatalist"));
            Ddo_ambientetecnologico_taxaentregadsnv_Sortasc = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Sortasc");
            Ddo_ambientetecnologico_taxaentregadsnv_Sortdsc = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Sortdsc");
            Ddo_ambientetecnologico_taxaentregadsnv_Cleanfilter = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Cleanfilter");
            Ddo_ambientetecnologico_taxaentregadsnv_Rangefilterfrom = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Rangefilterfrom");
            Ddo_ambientetecnologico_taxaentregadsnv_Rangefilterto = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Rangefilterto");
            Ddo_ambientetecnologico_taxaentregadsnv_Searchbuttontext = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Searchbuttontext");
            Ddo_ambientetecnologico_taxaentregamlhr_Caption = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Caption");
            Ddo_ambientetecnologico_taxaentregamlhr_Tooltip = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Tooltip");
            Ddo_ambientetecnologico_taxaentregamlhr_Cls = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Cls");
            Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Filteredtext_set");
            Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Filteredtextto_set");
            Ddo_ambientetecnologico_taxaentregamlhr_Dropdownoptionstype = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Dropdownoptionstype");
            Ddo_ambientetecnologico_taxaentregamlhr_Titlecontrolidtoreplace = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Titlecontrolidtoreplace");
            Ddo_ambientetecnologico_taxaentregamlhr_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Includesortasc"));
            Ddo_ambientetecnologico_taxaentregamlhr_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Includesortdsc"));
            Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Sortedstatus");
            Ddo_ambientetecnologico_taxaentregamlhr_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Includefilter"));
            Ddo_ambientetecnologico_taxaentregamlhr_Filtertype = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Filtertype");
            Ddo_ambientetecnologico_taxaentregamlhr_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Filterisrange"));
            Ddo_ambientetecnologico_taxaentregamlhr_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Includedatalist"));
            Ddo_ambientetecnologico_taxaentregamlhr_Sortasc = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Sortasc");
            Ddo_ambientetecnologico_taxaentregamlhr_Sortdsc = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Sortdsc");
            Ddo_ambientetecnologico_taxaentregamlhr_Cleanfilter = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Cleanfilter");
            Ddo_ambientetecnologico_taxaentregamlhr_Rangefilterfrom = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Rangefilterfrom");
            Ddo_ambientetecnologico_taxaentregamlhr_Rangefilterto = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Rangefilterto");
            Ddo_ambientetecnologico_taxaentregamlhr_Searchbuttontext = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Searchbuttontext");
            Ddo_ambientetecnologico_locpf_Caption = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Caption");
            Ddo_ambientetecnologico_locpf_Tooltip = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Tooltip");
            Ddo_ambientetecnologico_locpf_Cls = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Cls");
            Ddo_ambientetecnologico_locpf_Filteredtext_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Filteredtext_set");
            Ddo_ambientetecnologico_locpf_Filteredtextto_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Filteredtextto_set");
            Ddo_ambientetecnologico_locpf_Dropdownoptionstype = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Dropdownoptionstype");
            Ddo_ambientetecnologico_locpf_Titlecontrolidtoreplace = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Titlecontrolidtoreplace");
            Ddo_ambientetecnologico_locpf_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Includesortasc"));
            Ddo_ambientetecnologico_locpf_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Includesortdsc"));
            Ddo_ambientetecnologico_locpf_Sortedstatus = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Sortedstatus");
            Ddo_ambientetecnologico_locpf_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Includefilter"));
            Ddo_ambientetecnologico_locpf_Filtertype = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Filtertype");
            Ddo_ambientetecnologico_locpf_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Filterisrange"));
            Ddo_ambientetecnologico_locpf_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Includedatalist"));
            Ddo_ambientetecnologico_locpf_Sortasc = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Sortasc");
            Ddo_ambientetecnologico_locpf_Sortdsc = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Sortdsc");
            Ddo_ambientetecnologico_locpf_Cleanfilter = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Cleanfilter");
            Ddo_ambientetecnologico_locpf_Rangefilterfrom = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Rangefilterfrom");
            Ddo_ambientetecnologico_locpf_Rangefilterto = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Rangefilterto");
            Ddo_ambientetecnologico_locpf_Searchbuttontext = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Searchbuttontext");
            Ddo_ambientetecnologico_ativo_Caption = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Caption");
            Ddo_ambientetecnologico_ativo_Tooltip = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Tooltip");
            Ddo_ambientetecnologico_ativo_Cls = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Cls");
            Ddo_ambientetecnologico_ativo_Selectedvalue_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Selectedvalue_set");
            Ddo_ambientetecnologico_ativo_Dropdownoptionstype = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Dropdownoptionstype");
            Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Titlecontrolidtoreplace");
            Ddo_ambientetecnologico_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Includesortasc"));
            Ddo_ambientetecnologico_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Includesortdsc"));
            Ddo_ambientetecnologico_ativo_Sortedstatus = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Sortedstatus");
            Ddo_ambientetecnologico_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Includefilter"));
            Ddo_ambientetecnologico_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Includedatalist"));
            Ddo_ambientetecnologico_ativo_Datalisttype = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Datalisttype");
            Ddo_ambientetecnologico_ativo_Datalistfixedvalues = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Datalistfixedvalues");
            Ddo_ambientetecnologico_ativo_Sortasc = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Sortasc");
            Ddo_ambientetecnologico_ativo_Sortdsc = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Sortdsc");
            Ddo_ambientetecnologico_ativo_Cleanfilter = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Cleanfilter");
            Ddo_ambientetecnologico_ativo_Searchbuttontext = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Searchbuttontext");
            Ddo_ambientetecnologico_fatorajuste_Caption = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Caption");
            Ddo_ambientetecnologico_fatorajuste_Tooltip = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Tooltip");
            Ddo_ambientetecnologico_fatorajuste_Cls = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Cls");
            Ddo_ambientetecnologico_fatorajuste_Filteredtext_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Filteredtext_set");
            Ddo_ambientetecnologico_fatorajuste_Filteredtextto_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Filteredtextto_set");
            Ddo_ambientetecnologico_fatorajuste_Dropdownoptionstype = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Dropdownoptionstype");
            Ddo_ambientetecnologico_fatorajuste_Titlecontrolidtoreplace = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Titlecontrolidtoreplace");
            Ddo_ambientetecnologico_fatorajuste_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Includesortasc"));
            Ddo_ambientetecnologico_fatorajuste_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Includesortdsc"));
            Ddo_ambientetecnologico_fatorajuste_Sortedstatus = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Sortedstatus");
            Ddo_ambientetecnologico_fatorajuste_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Includefilter"));
            Ddo_ambientetecnologico_fatorajuste_Filtertype = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Filtertype");
            Ddo_ambientetecnologico_fatorajuste_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Filterisrange"));
            Ddo_ambientetecnologico_fatorajuste_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Includedatalist"));
            Ddo_ambientetecnologico_fatorajuste_Sortasc = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Sortasc");
            Ddo_ambientetecnologico_fatorajuste_Sortdsc = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Sortdsc");
            Ddo_ambientetecnologico_fatorajuste_Cleanfilter = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Cleanfilter");
            Ddo_ambientetecnologico_fatorajuste_Rangefilterfrom = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Rangefilterfrom");
            Ddo_ambientetecnologico_fatorajuste_Rangefilterto = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Rangefilterto");
            Ddo_ambientetecnologico_fatorajuste_Searchbuttontext = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_ambientetecnologico_descricao_Activeeventkey = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Activeeventkey");
            Ddo_ambientetecnologico_descricao_Filteredtext_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_get");
            Ddo_ambientetecnologico_descricao_Selectedvalue_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_get");
            Ddo_ambientetecnologico_taxaentregadsnv_Activeeventkey = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Activeeventkey");
            Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Filteredtext_get");
            Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV_Filteredtextto_get");
            Ddo_ambientetecnologico_taxaentregamlhr_Activeeventkey = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Activeeventkey");
            Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Filteredtext_get");
            Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_Filteredtextto_get");
            Ddo_ambientetecnologico_locpf_Activeeventkey = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Activeeventkey");
            Ddo_ambientetecnologico_locpf_Filteredtext_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Filteredtext_get");
            Ddo_ambientetecnologico_locpf_Filteredtextto_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_LOCPF_Filteredtextto_get");
            Ddo_ambientetecnologico_ativo_Activeeventkey = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Activeeventkey");
            Ddo_ambientetecnologico_ativo_Selectedvalue_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_ATIVO_Selectedvalue_get");
            Ddo_ambientetecnologico_fatorajuste_Activeeventkey = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Activeeventkey");
            Ddo_ambientetecnologico_fatorajuste_Filteredtext_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Filteredtext_get");
            Ddo_ambientetecnologico_fatorajuste_Filteredtextto_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV17DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAMBIENTETECNOLOGICO_DESCRICAO1"), AV19AmbienteTecnologico_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAMBIENTETECNOLOGICO_DESCRICAO2"), AV23AmbienteTecnologico_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAMBIENTETECNOLOGICO_DESCRICAO3"), AV27AmbienteTecnologico_Descricao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO"), AV64TFAmbienteTecnologico_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL"), AV65TFAmbienteTecnologico_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV"), ",", ".") != Convert.ToDecimal( AV68TFAmbienteTecnologico_TaxaEntregaDsnv )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO"), ",", ".") != Convert.ToDecimal( AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR"), ",", ".") != Convert.ToDecimal( AV72TFAmbienteTecnologico_TaxaEntregaMlhr )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO"), ",", ".") != Convert.ToDecimal( AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_LOCPF"), ",", ".") != Convert.ToDecimal( AV76TFAmbienteTecnologico_LocPF )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_LOCPF_TO"), ",", ".") != Convert.ToDecimal( AV77TFAmbienteTecnologico_LocPF_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV80TFAmbienteTecnologico_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_FATORAJUSTE"), ",", ".") != AV83TFAmbienteTecnologico_FatorAjuste )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO"), ",", ".") != AV84TFAmbienteTecnologico_FatorAjuste_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E299P2 */
         E299P2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E299P2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV17DynamicFiltersSelector1 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfambientetecnologico_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_descricao_Visible), 5, 0)));
         edtavTfambientetecnologico_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_descricao_sel_Visible), 5, 0)));
         edtavTfambientetecnologico_taxaentregadsnv_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_taxaentregadsnv_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_taxaentregadsnv_Visible), 5, 0)));
         edtavTfambientetecnologico_taxaentregadsnv_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_taxaentregadsnv_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_taxaentregadsnv_to_Visible), 5, 0)));
         edtavTfambientetecnologico_taxaentregamlhr_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_taxaentregamlhr_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_taxaentregamlhr_Visible), 5, 0)));
         edtavTfambientetecnologico_taxaentregamlhr_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_taxaentregamlhr_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_taxaentregamlhr_to_Visible), 5, 0)));
         edtavTfambientetecnologico_locpf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_locpf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_locpf_Visible), 5, 0)));
         edtavTfambientetecnologico_locpf_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_locpf_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_locpf_to_Visible), 5, 0)));
         edtavTfambientetecnologico_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_ativo_sel_Visible), 5, 0)));
         edtavTfambientetecnologico_fatorajuste_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_fatorajuste_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_fatorajuste_Visible), 5, 0)));
         edtavTfambientetecnologico_fatorajuste_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_fatorajuste_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_fatorajuste_to_Visible), 5, 0)));
         Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologico_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace);
         AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_ambientetecnologico_taxaentregadsnv_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologico_TaxaEntregaDsnv";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregadsnv_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologico_taxaentregadsnv_Titlecontrolidtoreplace);
         AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace = Ddo_ambientetecnologico_taxaentregadsnv_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace", AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace);
         edtavDdo_ambientetecnologico_taxaentregadsnvtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_ambientetecnologico_taxaentregadsnvtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologico_taxaentregadsnvtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_ambientetecnologico_taxaentregamlhr_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologico_TaxaEntregaMlhr";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregamlhr_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologico_taxaentregamlhr_Titlecontrolidtoreplace);
         AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace = Ddo_ambientetecnologico_taxaentregamlhr_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace", AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace);
         edtavDdo_ambientetecnologico_taxaentregamlhrtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_ambientetecnologico_taxaentregamlhrtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologico_taxaentregamlhrtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_ambientetecnologico_locpf_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologico_LocPF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_locpf_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologico_locpf_Titlecontrolidtoreplace);
         AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace = Ddo_ambientetecnologico_locpf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace", AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace);
         edtavDdo_ambientetecnologico_locpftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_ambientetecnologico_locpftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologico_locpftitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologico_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_ativo_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace);
         AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace = Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace", AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace);
         edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_ambientetecnologico_fatorajuste_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologico_FatorAjuste";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_fatorajuste_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologico_fatorajuste_Titlecontrolidtoreplace);
         AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace = Ddo_ambientetecnologico_fatorajuste_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace", AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace);
         edtavDdo_ambientetecnologico_fatorajustetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_ambientetecnologico_fatorajustetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologico_fatorajustetitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Ambiente Tecnologico";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Tecnol�gico", 0);
         cmbavOrderedby.addItem("2", "Tx Ent Des", 0);
         cmbavOrderedby.addItem("3", "Tx Ent Mlh", 0);
         cmbavOrderedby.addItem("4", "LOC/PF", 0);
         cmbavOrderedby.addItem("5", "Ativo", 0);
         cmbavOrderedby.addItem("6", "Fator de Ajuste", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV86DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV86DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E309P2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV63AmbienteTecnologico_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67AmbienteTecnologico_TaxaEntregaDsnvTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV71AmbienteTecnologico_TaxaEntregaMlhrTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV75AmbienteTecnologico_LocPFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79AmbienteTecnologico_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82AmbienteTecnologico_FatorAjusteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtAmbienteTecnologico_Descricao_Titleformat = 2;
         edtAmbienteTecnologico_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_Descricao_Internalname, "Title", edtAmbienteTecnologico_Descricao_Title);
         edtAmbienteTecnologico_TaxaEntregaDsnv_Titleformat = 2;
         edtAmbienteTecnologico_TaxaEntregaDsnv_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tx Ent Des", AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname, "Title", edtAmbienteTecnologico_TaxaEntregaDsnv_Title);
         edtAmbienteTecnologico_TaxaEntregaMlhr_Titleformat = 2;
         edtAmbienteTecnologico_TaxaEntregaMlhr_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tx Ent Mlh", AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname, "Title", edtAmbienteTecnologico_TaxaEntregaMlhr_Title);
         edtAmbienteTecnologico_LocPF_Titleformat = 2;
         edtAmbienteTecnologico_LocPF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "LOC/PF", AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_LocPF_Internalname, "Title", edtAmbienteTecnologico_LocPF_Title);
         chkAmbienteTecnologico_Ativo_Titleformat = 2;
         chkAmbienteTecnologico_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAmbienteTecnologico_Ativo_Internalname, "Title", chkAmbienteTecnologico_Ativo.Title.Text);
         edtAmbienteTecnologico_FatorAjuste_Titleformat = 2;
         edtAmbienteTecnologico_FatorAjuste_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fator de Ajuste", AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_FatorAjuste_Internalname, "Title", edtAmbienteTecnologico_FatorAjuste_Title);
         AV88GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV88GridCurrentPage), 10, 0)));
         AV89GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV89GridPageCount), 10, 0)));
         AV16AreaTrabalho_Codigo = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         edtavBtnassociation_Title = "Tecnologias";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociation_Internalname, "Title", edtavBtnassociation_Title);
         edtavBtnassociationsistema_Title = "Sistemas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociationsistema_Internalname, "Title", edtavBtnassociationsistema_Title);
         imgInsert_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         AV92WWAmbienteTecnologicoDS_1_Ambientetecnologico_areatrabalhocod = AV59AmbienteTecnologico_AreaTrabalhoCod;
         AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = AV19AmbienteTecnologico_Descricao1;
         AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = AV23AmbienteTecnologico_Descricao2;
         AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = AV27AmbienteTecnologico_Descricao3;
         AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = AV64TFAmbienteTecnologico_Descricao;
         AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel = AV65TFAmbienteTecnologico_Descricao_Sel;
         AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv = AV68TFAmbienteTecnologico_TaxaEntregaDsnv;
         AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to = AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To;
         AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr = AV72TFAmbienteTecnologico_TaxaEntregaMlhr;
         AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to = AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To;
         AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf = AV76TFAmbienteTecnologico_LocPF;
         AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to = AV77TFAmbienteTecnologico_LocPF_To;
         AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel = AV80TFAmbienteTecnologico_Ativo_Sel;
         AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste = AV83TFAmbienteTecnologico_FatorAjuste;
         AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to = AV84TFAmbienteTecnologico_FatorAjuste_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV63AmbienteTecnologico_DescricaoTitleFilterData", AV63AmbienteTecnologico_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV67AmbienteTecnologico_TaxaEntregaDsnvTitleFilterData", AV67AmbienteTecnologico_TaxaEntregaDsnvTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV71AmbienteTecnologico_TaxaEntregaMlhrTitleFilterData", AV71AmbienteTecnologico_TaxaEntregaMlhrTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV75AmbienteTecnologico_LocPFTitleFilterData", AV75AmbienteTecnologico_LocPFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV79AmbienteTecnologico_AtivoTitleFilterData", AV79AmbienteTecnologico_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV82AmbienteTecnologico_FatorAjusteTitleFilterData", AV82AmbienteTecnologico_FatorAjusteTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E119P2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV87PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV87PageToGo) ;
         }
      }

      protected void E129P2( )
      {
         /* Ddo_ambientetecnologico_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV64TFAmbienteTecnologico_Descricao = Ddo_ambientetecnologico_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFAmbienteTecnologico_Descricao", AV64TFAmbienteTecnologico_Descricao);
            AV65TFAmbienteTecnologico_Descricao_Sel = Ddo_ambientetecnologico_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFAmbienteTecnologico_Descricao_Sel", AV65TFAmbienteTecnologico_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E139P2( )
      {
         /* Ddo_ambientetecnologico_taxaentregadsnv_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologico_taxaentregadsnv_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregadsnv_Internalname, "SortedStatus", Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_taxaentregadsnv_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregadsnv_Internalname, "SortedStatus", Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_taxaentregadsnv_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV68TFAmbienteTecnologico_TaxaEntregaDsnv = (short)(NumberUtil.Val( Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFAmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68TFAmbienteTecnologico_TaxaEntregaDsnv), 4, 0)));
            AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To = (short)(NumberUtil.Val( Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E149P2( )
      {
         /* Ddo_ambientetecnologico_taxaentregamlhr_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologico_taxaentregamlhr_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregamlhr_Internalname, "SortedStatus", Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_taxaentregamlhr_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregamlhr_Internalname, "SortedStatus", Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_taxaentregamlhr_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV72TFAmbienteTecnologico_TaxaEntregaMlhr = (short)(NumberUtil.Val( Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFAmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72TFAmbienteTecnologico_TaxaEntregaMlhr), 4, 0)));
            AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To = (short)(NumberUtil.Val( Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E159P2( )
      {
         /* Ddo_ambientetecnologico_locpf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologico_locpf_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_locpf_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_locpf_Internalname, "SortedStatus", Ddo_ambientetecnologico_locpf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_locpf_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_locpf_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_locpf_Internalname, "SortedStatus", Ddo_ambientetecnologico_locpf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_locpf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV76TFAmbienteTecnologico_LocPF = (short)(NumberUtil.Val( Ddo_ambientetecnologico_locpf_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFAmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFAmbienteTecnologico_LocPF), 4, 0)));
            AV77TFAmbienteTecnologico_LocPF_To = (short)(NumberUtil.Val( Ddo_ambientetecnologico_locpf_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFAmbienteTecnologico_LocPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFAmbienteTecnologico_LocPF_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E169P2( )
      {
         /* Ddo_ambientetecnologico_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologico_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_ativo_Internalname, "SortedStatus", Ddo_ambientetecnologico_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_ativo_Internalname, "SortedStatus", Ddo_ambientetecnologico_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV80TFAmbienteTecnologico_Ativo_Sel = (short)(NumberUtil.Val( Ddo_ambientetecnologico_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAmbienteTecnologico_Ativo_Sel", StringUtil.Str( (decimal)(AV80TFAmbienteTecnologico_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E179P2( )
      {
         /* Ddo_ambientetecnologico_fatorajuste_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologico_fatorajuste_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_fatorajuste_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_fatorajuste_Internalname, "SortedStatus", Ddo_ambientetecnologico_fatorajuste_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_fatorajuste_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_fatorajuste_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_fatorajuste_Internalname, "SortedStatus", Ddo_ambientetecnologico_fatorajuste_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_fatorajuste_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV83TFAmbienteTecnologico_FatorAjuste = NumberUtil.Val( Ddo_ambientetecnologico_fatorajuste_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFAmbienteTecnologico_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV83TFAmbienteTecnologico_FatorAjuste, 8, 4)));
            AV84TFAmbienteTecnologico_FatorAjuste_To = NumberUtil.Val( Ddo_ambientetecnologico_fatorajuste_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFAmbienteTecnologico_FatorAjuste_To", StringUtil.LTrim( StringUtil.Str( AV84TFAmbienteTecnologico_FatorAjuste_To, 8, 4)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E319P2( )
      {
         /* Grid_Load Routine */
         AV30Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV30Update);
         AV112Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("ambientetecnologico.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A351AmbienteTecnologico_Codigo);
         AV31Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV31Delete);
         AV113Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("ambientetecnologico.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A351AmbienteTecnologico_Codigo);
         AV58Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV58Display);
         AV114Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewambientetecnologico.aspx") + "?" + UrlEncode("" +A351AmbienteTecnologico_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         AV57btnAssociation = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavBtnassociation_Internalname, AV57btnAssociation);
         AV115Btnassociation_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavBtnassociation_Tooltiptext = "Clique para associar as Tecnologias deste Ambiente Operacinal";
         AV60btnAssociationSistema = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavBtnassociationsistema_Internalname, AV60btnAssociationSistema);
         AV116Btnassociationsistema_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavBtnassociationsistema_Tooltiptext = "Clique para associar este Ambiente Operacinal � v�rios Sistemas";
         edtAmbienteTecnologico_Descricao_Link = formatLink("viewambientetecnologico.aspx") + "?" + UrlEncode("" +A351AmbienteTecnologico_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         AV30Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV30Update);
         AV112Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         AV31Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV31Delete);
         AV113Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         AV58Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV58Display);
         AV114Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 77;
         }
         sendrow_772( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_77_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(77, GridRow);
         }
      }

      protected void E189P2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E249P2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E199P2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV28DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV29DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersIgnoreFirst", AV29DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV29DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersIgnoreFirst", AV29DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV23AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV64TFAmbienteTecnologico_Descricao, AV65TFAmbienteTecnologico_Descricao_Sel, AV68TFAmbienteTecnologico_TaxaEntregaDsnv, AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To, AV72TFAmbienteTecnologico_TaxaEntregaMlhr, AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To, AV76TFAmbienteTecnologico_LocPF, AV77TFAmbienteTecnologico_LocPF_To, AV80TFAmbienteTecnologico_Ativo_Sel, AV83TFAmbienteTecnologico_FatorAjuste, AV84TFAmbienteTecnologico_FatorAjuste_To, AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace, AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace, AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace, AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace, AV59AmbienteTecnologico_AreaTrabalhoCod, AV117Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E259P2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E269P2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E209P2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV28DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV23AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV64TFAmbienteTecnologico_Descricao, AV65TFAmbienteTecnologico_Descricao_Sel, AV68TFAmbienteTecnologico_TaxaEntregaDsnv, AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To, AV72TFAmbienteTecnologico_TaxaEntregaMlhr, AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To, AV76TFAmbienteTecnologico_LocPF, AV77TFAmbienteTecnologico_LocPF_To, AV80TFAmbienteTecnologico_Ativo_Sel, AV83TFAmbienteTecnologico_FatorAjuste, AV84TFAmbienteTecnologico_FatorAjuste_To, AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace, AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace, AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace, AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace, AV59AmbienteTecnologico_AreaTrabalhoCod, AV117Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E279P2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E219P2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV28DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17DynamicFiltersSelector1, AV19AmbienteTecnologico_Descricao1, AV21DynamicFiltersSelector2, AV23AmbienteTecnologico_Descricao2, AV25DynamicFiltersSelector3, AV27AmbienteTecnologico_Descricao3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV64TFAmbienteTecnologico_Descricao, AV65TFAmbienteTecnologico_Descricao_Sel, AV68TFAmbienteTecnologico_TaxaEntregaDsnv, AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To, AV72TFAmbienteTecnologico_TaxaEntregaMlhr, AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To, AV76TFAmbienteTecnologico_LocPF, AV77TFAmbienteTecnologico_LocPF_To, AV80TFAmbienteTecnologico_Ativo_Sel, AV83TFAmbienteTecnologico_FatorAjuste, AV84TFAmbienteTecnologico_FatorAjuste_To, AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace, AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace, AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace, AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace, AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace, AV59AmbienteTecnologico_AreaTrabalhoCod, AV117Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A351AmbienteTecnologico_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E289P2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E229P2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavAmbientetecnologico_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAmbientetecnologico_areatrabalhocod_Internalname, "Values", dynavAmbientetecnologico_areatrabalhocod.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E329P2( )
      {
         /* 'DobtnAssociation' Routine */
         context.PopUp(formatLink("wp_associationambientetecnologicotecnologia.aspx") + "?" + UrlEncode("" +A351AmbienteTecnologico_Codigo), new Object[] {});
         context.DoAjaxRefresh();
      }

      protected void E339P2( )
      {
         /* 'DobtnAssociationSistema' Routine */
         context.PopUp(formatLink("wp_associationambientetecnologicosistema.aspx") + "?" + UrlEncode("" +A351AmbienteTecnologico_Codigo), new Object[] {});
         context.DoAjaxRefresh();
      }

      protected void E239P2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("ambientetecnologico.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_ambientetecnologico_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
         Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregadsnv_Internalname, "SortedStatus", Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus);
         Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregamlhr_Internalname, "SortedStatus", Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus);
         Ddo_ambientetecnologico_locpf_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_locpf_Internalname, "SortedStatus", Ddo_ambientetecnologico_locpf_Sortedstatus);
         Ddo_ambientetecnologico_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_ativo_Internalname, "SortedStatus", Ddo_ambientetecnologico_ativo_Sortedstatus);
         Ddo_ambientetecnologico_fatorajuste_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_fatorajuste_Internalname, "SortedStatus", Ddo_ambientetecnologico_fatorajuste_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_ambientetecnologico_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregadsnv_Internalname, "SortedStatus", Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregamlhr_Internalname, "SortedStatus", Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_ambientetecnologico_locpf_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_locpf_Internalname, "SortedStatus", Ddo_ambientetecnologico_locpf_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_ambientetecnologico_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_ativo_Internalname, "SortedStatus", Ddo_ambientetecnologico_ativo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_ambientetecnologico_fatorajuste_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_fatorajuste_Internalname, "SortedStatus", Ddo_ambientetecnologico_fatorajuste_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavAmbientetecnologico_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAmbientetecnologico_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
         {
            edtavAmbientetecnologico_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAmbientetecnologico_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavAmbientetecnologico_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAmbientetecnologico_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
         {
            edtavAmbientetecnologico_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAmbientetecnologico_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavAmbientetecnologico_descricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAmbientetecnologico_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
         {
            edtavAmbientetecnologico_descricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAmbientetecnologico_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAmbientetecnologico_descricao3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV23AmbienteTecnologico_Descricao2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23AmbienteTecnologico_Descricao2", AV23AmbienteTecnologico_Descricao2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV27AmbienteTecnologico_Descricao3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27AmbienteTecnologico_Descricao3", AV27AmbienteTecnologico_Descricao3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV59AmbienteTecnologico_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         AV64TFAmbienteTecnologico_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFAmbienteTecnologico_Descricao", AV64TFAmbienteTecnologico_Descricao);
         Ddo_ambientetecnologico_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "FilteredText_set", Ddo_ambientetecnologico_descricao_Filteredtext_set);
         AV65TFAmbienteTecnologico_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFAmbienteTecnologico_Descricao_Sel", AV65TFAmbienteTecnologico_Descricao_Sel);
         Ddo_ambientetecnologico_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SelectedValue_set", Ddo_ambientetecnologico_descricao_Selectedvalue_set);
         AV68TFAmbienteTecnologico_TaxaEntregaDsnv = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFAmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68TFAmbienteTecnologico_TaxaEntregaDsnv), 4, 0)));
         Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregadsnv_Internalname, "FilteredText_set", Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_set);
         AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To), 4, 0)));
         Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregadsnv_Internalname, "FilteredTextTo_set", Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_set);
         AV72TFAmbienteTecnologico_TaxaEntregaMlhr = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFAmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72TFAmbienteTecnologico_TaxaEntregaMlhr), 4, 0)));
         Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregamlhr_Internalname, "FilteredText_set", Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_set);
         AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To), 4, 0)));
         Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregamlhr_Internalname, "FilteredTextTo_set", Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_set);
         AV76TFAmbienteTecnologico_LocPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFAmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFAmbienteTecnologico_LocPF), 4, 0)));
         Ddo_ambientetecnologico_locpf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_locpf_Internalname, "FilteredText_set", Ddo_ambientetecnologico_locpf_Filteredtext_set);
         AV77TFAmbienteTecnologico_LocPF_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFAmbienteTecnologico_LocPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFAmbienteTecnologico_LocPF_To), 4, 0)));
         Ddo_ambientetecnologico_locpf_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_locpf_Internalname, "FilteredTextTo_set", Ddo_ambientetecnologico_locpf_Filteredtextto_set);
         AV80TFAmbienteTecnologico_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAmbienteTecnologico_Ativo_Sel", StringUtil.Str( (decimal)(AV80TFAmbienteTecnologico_Ativo_Sel), 1, 0));
         Ddo_ambientetecnologico_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_ativo_Internalname, "SelectedValue_set", Ddo_ambientetecnologico_ativo_Selectedvalue_set);
         AV83TFAmbienteTecnologico_FatorAjuste = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFAmbienteTecnologico_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV83TFAmbienteTecnologico_FatorAjuste, 8, 4)));
         Ddo_ambientetecnologico_fatorajuste_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_fatorajuste_Internalname, "FilteredText_set", Ddo_ambientetecnologico_fatorajuste_Filteredtext_set);
         AV84TFAmbienteTecnologico_FatorAjuste_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFAmbienteTecnologico_FatorAjuste_To", StringUtil.LTrim( StringUtil.Str( AV84TFAmbienteTecnologico_FatorAjuste_To, 8, 4)));
         Ddo_ambientetecnologico_fatorajuste_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_fatorajuste_Internalname, "FilteredTextTo_set", Ddo_ambientetecnologico_fatorajuste_Filteredtextto_set);
         AV17DynamicFiltersSelector1 = "AMBIENTETECNOLOGICO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         AV19AmbienteTecnologico_Descricao1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19AmbienteTecnologico_Descricao1", AV19AmbienteTecnologico_Descricao1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.DoAjaxRefresh();
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV52Session.Get(AV117Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV117Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV52Session.Get(AV117Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV118GXV1 = 1;
         while ( AV118GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV118GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "AMBIENTETECNOLOGICO_AREATRABALHOCOD") == 0 )
            {
               AV59AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV64TFAmbienteTecnologico_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFAmbienteTecnologico_Descricao", AV64TFAmbienteTecnologico_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFAmbienteTecnologico_Descricao)) )
               {
                  Ddo_ambientetecnologico_descricao_Filteredtext_set = AV64TFAmbienteTecnologico_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "FilteredText_set", Ddo_ambientetecnologico_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO_SEL") == 0 )
            {
               AV65TFAmbienteTecnologico_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFAmbienteTecnologico_Descricao_Sel", AV65TFAmbienteTecnologico_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFAmbienteTecnologico_Descricao_Sel)) )
               {
                  Ddo_ambientetecnologico_descricao_Selectedvalue_set = AV65TFAmbienteTecnologico_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SelectedValue_set", Ddo_ambientetecnologico_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_TAXAENTREGADSNV") == 0 )
            {
               AV68TFAmbienteTecnologico_TaxaEntregaDsnv = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFAmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68TFAmbienteTecnologico_TaxaEntregaDsnv), 4, 0)));
               AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To), 4, 0)));
               if ( ! (0==AV68TFAmbienteTecnologico_TaxaEntregaDsnv) )
               {
                  Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_set = StringUtil.Str( (decimal)(AV68TFAmbienteTecnologico_TaxaEntregaDsnv), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregadsnv_Internalname, "FilteredText_set", Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_set);
               }
               if ( ! (0==AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To) )
               {
                  Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_set = StringUtil.Str( (decimal)(AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregadsnv_Internalname, "FilteredTextTo_set", Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR") == 0 )
            {
               AV72TFAmbienteTecnologico_TaxaEntregaMlhr = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72TFAmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72TFAmbienteTecnologico_TaxaEntregaMlhr), 4, 0)));
               AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To), 4, 0)));
               if ( ! (0==AV72TFAmbienteTecnologico_TaxaEntregaMlhr) )
               {
                  Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_set = StringUtil.Str( (decimal)(AV72TFAmbienteTecnologico_TaxaEntregaMlhr), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregamlhr_Internalname, "FilteredText_set", Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_set);
               }
               if ( ! (0==AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To) )
               {
                  Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_set = StringUtil.Str( (decimal)(AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_taxaentregamlhr_Internalname, "FilteredTextTo_set", Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_LOCPF") == 0 )
            {
               AV76TFAmbienteTecnologico_LocPF = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFAmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76TFAmbienteTecnologico_LocPF), 4, 0)));
               AV77TFAmbienteTecnologico_LocPF_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFAmbienteTecnologico_LocPF_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFAmbienteTecnologico_LocPF_To), 4, 0)));
               if ( ! (0==AV76TFAmbienteTecnologico_LocPF) )
               {
                  Ddo_ambientetecnologico_locpf_Filteredtext_set = StringUtil.Str( (decimal)(AV76TFAmbienteTecnologico_LocPF), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_locpf_Internalname, "FilteredText_set", Ddo_ambientetecnologico_locpf_Filteredtext_set);
               }
               if ( ! (0==AV77TFAmbienteTecnologico_LocPF_To) )
               {
                  Ddo_ambientetecnologico_locpf_Filteredtextto_set = StringUtil.Str( (decimal)(AV77TFAmbienteTecnologico_LocPF_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_locpf_Internalname, "FilteredTextTo_set", Ddo_ambientetecnologico_locpf_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_ATIVO_SEL") == 0 )
            {
               AV80TFAmbienteTecnologico_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAmbienteTecnologico_Ativo_Sel", StringUtil.Str( (decimal)(AV80TFAmbienteTecnologico_Ativo_Sel), 1, 0));
               if ( ! (0==AV80TFAmbienteTecnologico_Ativo_Sel) )
               {
                  Ddo_ambientetecnologico_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV80TFAmbienteTecnologico_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_ativo_Internalname, "SelectedValue_set", Ddo_ambientetecnologico_ativo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_FATORAJUSTE") == 0 )
            {
               AV83TFAmbienteTecnologico_FatorAjuste = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFAmbienteTecnologico_FatorAjuste", StringUtil.LTrim( StringUtil.Str( AV83TFAmbienteTecnologico_FatorAjuste, 8, 4)));
               AV84TFAmbienteTecnologico_FatorAjuste_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFAmbienteTecnologico_FatorAjuste_To", StringUtil.LTrim( StringUtil.Str( AV84TFAmbienteTecnologico_FatorAjuste_To, 8, 4)));
               if ( ! (Convert.ToDecimal(0)==AV83TFAmbienteTecnologico_FatorAjuste) )
               {
                  Ddo_ambientetecnologico_fatorajuste_Filteredtext_set = StringUtil.Str( AV83TFAmbienteTecnologico_FatorAjuste, 8, 4);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_fatorajuste_Internalname, "FilteredText_set", Ddo_ambientetecnologico_fatorajuste_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV84TFAmbienteTecnologico_FatorAjuste_To) )
               {
                  Ddo_ambientetecnologico_fatorajuste_Filteredtextto_set = StringUtil.Str( AV84TFAmbienteTecnologico_FatorAjuste_To, 8, 4);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_fatorajuste_Internalname, "FilteredTextTo_set", Ddo_ambientetecnologico_fatorajuste_Filteredtextto_set);
               }
            }
            AV118GXV1 = (int)(AV118GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV17DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV19AmbienteTecnologico_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19AmbienteTecnologico_Descricao1", AV19AmbienteTecnologico_Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
               {
                  AV23AmbienteTecnologico_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23AmbienteTecnologico_Descricao2", AV23AmbienteTecnologico_Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
                  {
                     AV27AmbienteTecnologico_Descricao3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27AmbienteTecnologico_Descricao3", AV27AmbienteTecnologico_Descricao3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV28DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV52Session.Get(AV117Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV59AmbienteTecnologico_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "AMBIENTETECNOLOGICO_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFAmbienteTecnologico_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV64TFAmbienteTecnologico_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFAmbienteTecnologico_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV65TFAmbienteTecnologico_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV68TFAmbienteTecnologico_TaxaEntregaDsnv) && (0==AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_TAXAENTREGADSNV";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV68TFAmbienteTecnologico_TaxaEntregaDsnv), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV72TFAmbienteTecnologico_TaxaEntregaMlhr) && (0==AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV72TFAmbienteTecnologico_TaxaEntregaMlhr), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV76TFAmbienteTecnologico_LocPF) && (0==AV77TFAmbienteTecnologico_LocPF_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_LOCPF";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV76TFAmbienteTecnologico_LocPF), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV77TFAmbienteTecnologico_LocPF_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV80TFAmbienteTecnologico_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV80TFAmbienteTecnologico_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV83TFAmbienteTecnologico_FatorAjuste) && (Convert.ToDecimal(0)==AV84TFAmbienteTecnologico_FatorAjuste_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_FATORAJUSTE";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV83TFAmbienteTecnologico_FatorAjuste, 8, 4);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV84TFAmbienteTecnologico_FatorAjuste_To, 8, 4);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV117Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV29DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV17DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19AmbienteTecnologico_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19AmbienteTecnologico_Descricao1;
            }
            if ( AV28DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23AmbienteTecnologico_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23AmbienteTecnologico_Descricao2;
            }
            if ( AV28DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27AmbienteTecnologico_Descricao3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV27AmbienteTecnologico_Descricao3;
            }
            if ( AV28DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV117Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "AmbienteTecnologico";
         AV52Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_9P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_9P2( true) ;
         }
         else
         {
            wb_table2_8_9P2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_9P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_71_9P2( true) ;
         }
         else
         {
            wb_table3_71_9P2( false) ;
         }
         return  ;
      }

      protected void wb_table3_71_9P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9P2e( true) ;
         }
         else
         {
            wb_table1_2_9P2e( false) ;
         }
      }

      protected void wb_table3_71_9P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table4_74_9P2( true) ;
         }
         else
         {
            wb_table4_74_9P2( false) ;
         }
         return  ;
      }

      protected void wb_table4_74_9P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_71_9P2e( true) ;
         }
         else
         {
            wb_table3_71_9P2e( false) ;
         }
      }

      protected void wb_table4_74_9P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"77\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Tecnol�gico") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAmbienteTecnologico_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtAmbienteTecnologico_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAmbienteTecnologico_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(62), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAmbienteTecnologico_TaxaEntregaDsnv_Titleformat == 0 )
               {
                  context.SendWebValue( edtAmbienteTecnologico_TaxaEntregaDsnv_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAmbienteTecnologico_TaxaEntregaDsnv_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(64), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAmbienteTecnologico_TaxaEntregaMlhr_Titleformat == 0 )
               {
                  context.SendWebValue( edtAmbienteTecnologico_TaxaEntregaMlhr_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAmbienteTecnologico_TaxaEntregaMlhr_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(45), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAmbienteTecnologico_LocPF_Titleformat == 0 )
               {
                  context.SendWebValue( edtAmbienteTecnologico_LocPF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAmbienteTecnologico_LocPF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkAmbienteTecnologico_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkAmbienteTecnologico_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkAmbienteTecnologico_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAmbienteTecnologico_FatorAjuste_Titleformat == 0 )
               {
                  context.SendWebValue( edtAmbienteTecnologico_FatorAjuste_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAmbienteTecnologico_FatorAjuste_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavBtnassociation_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavBtnassociationsistema_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV30Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV58Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A352AmbienteTecnologico_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAmbienteTecnologico_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmbienteTecnologico_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtAmbienteTecnologico_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAmbienteTecnologico_TaxaEntregaDsnv_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmbienteTecnologico_TaxaEntregaDsnv_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAmbienteTecnologico_TaxaEntregaMlhr_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmbienteTecnologico_TaxaEntregaMlhr_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A978AmbienteTecnologico_LocPF), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAmbienteTecnologico_LocPF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmbienteTecnologico_LocPF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A353AmbienteTecnologico_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkAmbienteTecnologico_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkAmbienteTecnologico_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1423AmbienteTecnologico_FatorAjuste, 8, 4, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAmbienteTecnologico_FatorAjuste_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmbienteTecnologico_FatorAjuste_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV57btnAssociation));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavBtnassociation_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBtnassociation_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV60btnAssociationSistema));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavBtnassociationsistema_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBtnassociationsistema_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 77 )
         {
            wbEnd = 0;
            nRC_GXsfl_77 = (short)(nGXsfl_77_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_74_9P2e( true) ;
         }
         else
         {
            wb_table4_74_9P2e( false) ;
         }
      }

      protected void wb_table2_8_9P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAmbientetecnologicotitle_Internalname, "Ambientes Operacionais", "", "", lblAmbientetecnologicotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_13_9P2( true) ;
         }
         else
         {
            wb_table5_13_9P2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_9P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWAmbienteTecnologico.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_9P2( true) ;
         }
         else
         {
            wb_table6_23_9P2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_9P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_9P2e( true) ;
         }
         else
         {
            wb_table2_8_9P2e( false) ;
         }
      }

      protected void wb_table6_23_9P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextambientetecnologico_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblFiltertextambientetecnologico_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavAmbientetecnologico_areatrabalhocod, dynavAmbientetecnologico_areatrabalhocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0)), 1, dynavAmbientetecnologico_areatrabalhocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "", true, "HLP_WWAmbienteTecnologico.htm");
            dynavAmbientetecnologico_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59AmbienteTecnologico_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAmbientetecnologico_areatrabalhocod_Internalname, "Values", (String)(dynavAmbientetecnologico_areatrabalhocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_32_9P2( true) ;
         }
         else
         {
            wb_table7_32_9P2( false) ;
         }
         return  ;
      }

      protected void wb_table7_32_9P2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_9P2e( true) ;
         }
         else
         {
            wb_table6_23_9P2e( false) ;
         }
      }

      protected void wb_table7_32_9P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV17DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_WWAmbienteTecnologico.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAmbientetecnologico_descricao1_Internalname, AV19AmbienteTecnologico_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV19AmbienteTecnologico_Descricao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAmbientetecnologico_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAmbientetecnologico_descricao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAmbienteTecnologico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_WWAmbienteTecnologico.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAmbientetecnologico_descricao2_Internalname, AV23AmbienteTecnologico_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV23AmbienteTecnologico_Descricao2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAmbientetecnologico_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAmbientetecnologico_descricao2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAmbienteTecnologico.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_77_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_WWAmbienteTecnologico.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_77_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAmbientetecnologico_descricao3_Internalname, AV27AmbienteTecnologico_Descricao3, StringUtil.RTrim( context.localUtil.Format( AV27AmbienteTecnologico_Descricao3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAmbientetecnologico_descricao3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAmbientetecnologico_descricao3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_32_9P2e( true) ;
         }
         else
         {
            wb_table7_32_9P2e( false) ;
         }
      }

      protected void wb_table5_13_9P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_9P2e( true) ;
         }
         else
         {
            wb_table5_13_9P2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9P2( ) ;
         WS9P2( ) ;
         WE9P2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117393911");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwambientetecnologico.js", "?20203117393911");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_772( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_77_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_77_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_77_idx;
         edtAmbienteTecnologico_Codigo_Internalname = "AMBIENTETECNOLOGICO_CODIGO_"+sGXsfl_77_idx;
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO_"+sGXsfl_77_idx;
         edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname = "AMBIENTETECNOLOGICO_TAXAENTREGADSNV_"+sGXsfl_77_idx;
         edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname = "AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_"+sGXsfl_77_idx;
         edtAmbienteTecnologico_LocPF_Internalname = "AMBIENTETECNOLOGICO_LOCPF_"+sGXsfl_77_idx;
         chkAmbienteTecnologico_Ativo_Internalname = "AMBIENTETECNOLOGICO_ATIVO_"+sGXsfl_77_idx;
         edtAmbienteTecnologico_FatorAjuste_Internalname = "AMBIENTETECNOLOGICO_FATORAJUSTE_"+sGXsfl_77_idx;
         edtavBtnassociation_Internalname = "vBTNASSOCIATION_"+sGXsfl_77_idx;
         edtavBtnassociationsistema_Internalname = "vBTNASSOCIATIONSISTEMA_"+sGXsfl_77_idx;
      }

      protected void SubsflControlProps_fel_772( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_77_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_77_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_77_fel_idx;
         edtAmbienteTecnologico_Codigo_Internalname = "AMBIENTETECNOLOGICO_CODIGO_"+sGXsfl_77_fel_idx;
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO_"+sGXsfl_77_fel_idx;
         edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname = "AMBIENTETECNOLOGICO_TAXAENTREGADSNV_"+sGXsfl_77_fel_idx;
         edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname = "AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_"+sGXsfl_77_fel_idx;
         edtAmbienteTecnologico_LocPF_Internalname = "AMBIENTETECNOLOGICO_LOCPF_"+sGXsfl_77_fel_idx;
         chkAmbienteTecnologico_Ativo_Internalname = "AMBIENTETECNOLOGICO_ATIVO_"+sGXsfl_77_fel_idx;
         edtAmbienteTecnologico_FatorAjuste_Internalname = "AMBIENTETECNOLOGICO_FATORAJUSTE_"+sGXsfl_77_fel_idx;
         edtavBtnassociation_Internalname = "vBTNASSOCIATION_"+sGXsfl_77_fel_idx;
         edtavBtnassociationsistema_Internalname = "vBTNASSOCIATIONSISTEMA_"+sGXsfl_77_fel_idx;
      }

      protected void sendrow_772( )
      {
         SubsflControlProps_772( ) ;
         WB9P0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_77_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_77_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_77_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV30Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV112Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV30Update)) ? AV112Update_GXI : context.PathToRelativeUrl( AV30Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV30Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV113Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Delete)) ? AV113Delete_GXI : context.PathToRelativeUrl( AV31Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV58Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV58Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV114Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV58Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV58Display)) ? AV114Display_GXI : context.PathToRelativeUrl( AV58Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV58Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_Descricao_Internalname,(String)A352AmbienteTecnologico_Descricao,StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtAmbienteTecnologico_Descricao_Link,(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_TaxaEntregaDsnv_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)62,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_TaxaEntregaMlhr_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)64,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_LocPF_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A978AmbienteTecnologico_LocPF), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A978AmbienteTecnologico_LocPF), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_LocPF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)45,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAmbienteTecnologico_Ativo_Internalname,StringUtil.BoolToStr( A353AmbienteTecnologico_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_FatorAjuste_Internalname,StringUtil.LTrim( StringUtil.NToC( A1423AmbienteTecnologico_FatorAjuste, 8, 4, ",", "")),context.localUtil.Format( A1423AmbienteTecnologico_FatorAjuste, "ZZ9.9999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_FatorAjuste_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)77,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBtnassociation_Enabled!=0)&&(edtavBtnassociation_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 88,'',false,'',77)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV57btnAssociation_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV57btnAssociation))&&String.IsNullOrEmpty(StringUtil.RTrim( AV115Btnassociation_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV57btnAssociation)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnassociation_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV57btnAssociation)) ? AV115Btnassociation_GXI : context.PathToRelativeUrl( AV57btnAssociation)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavBtnassociation_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavBtnassociation_Jsonclick,"'"+""+"'"+",false,"+"'"+"E\\'DOBTNASSOCIATION\\'."+sGXsfl_77_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV57btnAssociation_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBtnassociationsistema_Enabled!=0)&&(edtavBtnassociationsistema_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 89,'',false,'',77)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV60btnAssociationSistema_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV60btnAssociationSistema))&&String.IsNullOrEmpty(StringUtil.RTrim( AV116Btnassociationsistema_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV60btnAssociationSistema)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnassociationsistema_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV60btnAssociationSistema)) ? AV116Btnassociationsistema_GXI : context.PathToRelativeUrl( AV60btnAssociationSistema)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavBtnassociationsistema_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavBtnassociationsistema_Jsonclick,"'"+""+"'"+",false,"+"'"+"E\\'DOBTNASSOCIATIONSISTEMA\\'."+sGXsfl_77_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV60btnAssociationSistema_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_CODIGO"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_DESCRICAO"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_TAXAENTREGADSNV"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_LOCPF"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( (decimal)(A978AmbienteTecnologico_LocPF), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_ATIVO"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, A353AmbienteTecnologico_Ativo));
            GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_FATORAJUSTE"+"_"+sGXsfl_77_idx, GetSecureSignedToken( sGXsfl_77_idx, context.localUtil.Format( A1423AmbienteTecnologico_FatorAjuste, "ZZ9.9999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_77_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_77_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_77_idx+1));
            sGXsfl_77_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_77_idx), 4, 0)), 4, "0");
            SubsflControlProps_772( ) ;
         }
         /* End function sendrow_772 */
      }

      protected void init_default_properties( )
      {
         lblAmbientetecnologicotitle_Internalname = "AMBIENTETECNOLOGICOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextambientetecnologico_areatrabalhocod_Internalname = "FILTERTEXTAMBIENTETECNOLOGICO_AREATRABALHOCOD";
         dynavAmbientetecnologico_areatrabalhocod_Internalname = "vAMBIENTETECNOLOGICO_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavAmbientetecnologico_descricao1_Internalname = "vAMBIENTETECNOLOGICO_DESCRICAO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavAmbientetecnologico_descricao2_Internalname = "vAMBIENTETECNOLOGICO_DESCRICAO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavAmbientetecnologico_descricao3_Internalname = "vAMBIENTETECNOLOGICO_DESCRICAO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtAmbienteTecnologico_Codigo_Internalname = "AMBIENTETECNOLOGICO_CODIGO";
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO";
         edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname = "AMBIENTETECNOLOGICO_TAXAENTREGADSNV";
         edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname = "AMBIENTETECNOLOGICO_TAXAENTREGAMLHR";
         edtAmbienteTecnologico_LocPF_Internalname = "AMBIENTETECNOLOGICO_LOCPF";
         chkAmbienteTecnologico_Ativo_Internalname = "AMBIENTETECNOLOGICO_ATIVO";
         edtAmbienteTecnologico_FatorAjuste_Internalname = "AMBIENTETECNOLOGICO_FATORAJUSTE";
         edtavBtnassociation_Internalname = "vBTNASSOCIATION";
         edtavBtnassociationsistema_Internalname = "vBTNASSOCIATIONSISTEMA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfambientetecnologico_descricao_Internalname = "vTFAMBIENTETECNOLOGICO_DESCRICAO";
         edtavTfambientetecnologico_descricao_sel_Internalname = "vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL";
         edtavTfambientetecnologico_taxaentregadsnv_Internalname = "vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV";
         edtavTfambientetecnologico_taxaentregadsnv_to_Internalname = "vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO";
         edtavTfambientetecnologico_taxaentregamlhr_Internalname = "vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR";
         edtavTfambientetecnologico_taxaentregamlhr_to_Internalname = "vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO";
         edtavTfambientetecnologico_locpf_Internalname = "vTFAMBIENTETECNOLOGICO_LOCPF";
         edtavTfambientetecnologico_locpf_to_Internalname = "vTFAMBIENTETECNOLOGICO_LOCPF_TO";
         edtavTfambientetecnologico_ativo_sel_Internalname = "vTFAMBIENTETECNOLOGICO_ATIVO_SEL";
         edtavTfambientetecnologico_fatorajuste_Internalname = "vTFAMBIENTETECNOLOGICO_FATORAJUSTE";
         edtavTfambientetecnologico_fatorajuste_to_Internalname = "vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO";
         Ddo_ambientetecnologico_descricao_Internalname = "DDO_AMBIENTETECNOLOGICO_DESCRICAO";
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname = "vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_ambientetecnologico_taxaentregadsnv_Internalname = "DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV";
         edtavDdo_ambientetecnologico_taxaentregadsnvtitlecontrolidtoreplace_Internalname = "vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE";
         Ddo_ambientetecnologico_taxaentregamlhr_Internalname = "DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR";
         edtavDdo_ambientetecnologico_taxaentregamlhrtitlecontrolidtoreplace_Internalname = "vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE";
         Ddo_ambientetecnologico_locpf_Internalname = "DDO_AMBIENTETECNOLOGICO_LOCPF";
         edtavDdo_ambientetecnologico_locpftitlecontrolidtoreplace_Internalname = "vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE";
         Ddo_ambientetecnologico_ativo_Internalname = "DDO_AMBIENTETECNOLOGICO_ATIVO";
         edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Internalname = "vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE";
         Ddo_ambientetecnologico_fatorajuste_Internalname = "DDO_AMBIENTETECNOLOGICO_FATORAJUSTE";
         edtavDdo_ambientetecnologico_fatorajustetitlecontrolidtoreplace_Internalname = "vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavBtnassociationsistema_Jsonclick = "";
         edtavBtnassociationsistema_Visible = -1;
         edtavBtnassociationsistema_Enabled = 1;
         edtavBtnassociation_Jsonclick = "";
         edtavBtnassociation_Visible = -1;
         edtavBtnassociation_Enabled = 1;
         edtAmbienteTecnologico_FatorAjuste_Jsonclick = "";
         edtAmbienteTecnologico_LocPF_Jsonclick = "";
         edtAmbienteTecnologico_TaxaEntregaMlhr_Jsonclick = "";
         edtAmbienteTecnologico_TaxaEntregaDsnv_Jsonclick = "";
         edtAmbienteTecnologico_Descricao_Jsonclick = "";
         edtAmbienteTecnologico_Codigo_Jsonclick = "";
         imgInsert_Visible = 1;
         edtavAmbientetecnologico_descricao3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavAmbientetecnologico_descricao2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavAmbientetecnologico_descricao1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavAmbientetecnologico_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavBtnassociationsistema_Tooltiptext = "Clique para associar este Ambiente Operacinal � v�rios Sistemas";
         edtavBtnassociation_Tooltiptext = "Clique para associar as Tecnologias deste Ambiente Operacinal";
         edtAmbienteTecnologico_Descricao_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtAmbienteTecnologico_FatorAjuste_Titleformat = 0;
         chkAmbienteTecnologico_Ativo_Titleformat = 0;
         edtAmbienteTecnologico_LocPF_Titleformat = 0;
         edtAmbienteTecnologico_TaxaEntregaMlhr_Titleformat = 0;
         edtAmbienteTecnologico_TaxaEntregaDsnv_Titleformat = 0;
         edtAmbienteTecnologico_Descricao_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavAmbientetecnologico_descricao3_Visible = 1;
         edtavAmbientetecnologico_descricao2_Visible = 1;
         edtavAmbientetecnologico_descricao1_Visible = 1;
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtavBtnassociationsistema_Title = "";
         edtavBtnassociation_Title = "";
         edtAmbienteTecnologico_FatorAjuste_Title = "Fator de Ajuste";
         chkAmbienteTecnologico_Ativo.Title.Text = "Ativo";
         edtAmbienteTecnologico_LocPF_Title = "LOC/PF";
         edtAmbienteTecnologico_TaxaEntregaMlhr_Title = "Tx Ent Mlh";
         edtAmbienteTecnologico_TaxaEntregaDsnv_Title = "Tx Ent Des";
         edtAmbienteTecnologico_Descricao_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkAmbienteTecnologico_Ativo.Caption = "";
         edtavDdo_ambientetecnologico_fatorajustetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_ambientetecnologico_locpftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_ambientetecnologico_taxaentregamlhrtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_ambientetecnologico_taxaentregadsnvtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavTfambientetecnologico_fatorajuste_to_Jsonclick = "";
         edtavTfambientetecnologico_fatorajuste_to_Visible = 1;
         edtavTfambientetecnologico_fatorajuste_Jsonclick = "";
         edtavTfambientetecnologico_fatorajuste_Visible = 1;
         edtavTfambientetecnologico_ativo_sel_Jsonclick = "";
         edtavTfambientetecnologico_ativo_sel_Visible = 1;
         edtavTfambientetecnologico_locpf_to_Jsonclick = "";
         edtavTfambientetecnologico_locpf_to_Visible = 1;
         edtavTfambientetecnologico_locpf_Jsonclick = "";
         edtavTfambientetecnologico_locpf_Visible = 1;
         edtavTfambientetecnologico_taxaentregamlhr_to_Jsonclick = "";
         edtavTfambientetecnologico_taxaentregamlhr_to_Visible = 1;
         edtavTfambientetecnologico_taxaentregamlhr_Jsonclick = "";
         edtavTfambientetecnologico_taxaentregamlhr_Visible = 1;
         edtavTfambientetecnologico_taxaentregadsnv_to_Jsonclick = "";
         edtavTfambientetecnologico_taxaentregadsnv_to_Visible = 1;
         edtavTfambientetecnologico_taxaentregadsnv_Jsonclick = "";
         edtavTfambientetecnologico_taxaentregadsnv_Visible = 1;
         edtavTfambientetecnologico_descricao_sel_Jsonclick = "";
         edtavTfambientetecnologico_descricao_sel_Visible = 1;
         edtavTfambientetecnologico_descricao_Jsonclick = "";
         edtavTfambientetecnologico_descricao_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_ambientetecnologico_fatorajuste_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologico_fatorajuste_Rangefilterto = "At�";
         Ddo_ambientetecnologico_fatorajuste_Rangefilterfrom = "Desde";
         Ddo_ambientetecnologico_fatorajuste_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologico_fatorajuste_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologico_fatorajuste_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologico_fatorajuste_Includedatalist = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_fatorajuste_Filterisrange = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_fatorajuste_Filtertype = "Numeric";
         Ddo_ambientetecnologico_fatorajuste_Includefilter = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_fatorajuste_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_fatorajuste_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_fatorajuste_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologico_fatorajuste_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologico_fatorajuste_Cls = "ColumnSettings";
         Ddo_ambientetecnologico_fatorajuste_Tooltip = "Op��es";
         Ddo_ambientetecnologico_fatorajuste_Caption = "";
         Ddo_ambientetecnologico_ativo_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologico_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologico_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologico_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologico_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_ambientetecnologico_ativo_Datalisttype = "FixedValues";
         Ddo_ambientetecnologico_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologico_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologico_ativo_Cls = "ColumnSettings";
         Ddo_ambientetecnologico_ativo_Tooltip = "Op��es";
         Ddo_ambientetecnologico_ativo_Caption = "";
         Ddo_ambientetecnologico_locpf_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologico_locpf_Rangefilterto = "At�";
         Ddo_ambientetecnologico_locpf_Rangefilterfrom = "Desde";
         Ddo_ambientetecnologico_locpf_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologico_locpf_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologico_locpf_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologico_locpf_Includedatalist = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_locpf_Filterisrange = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_locpf_Filtertype = "Numeric";
         Ddo_ambientetecnologico_locpf_Includefilter = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_locpf_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_locpf_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_locpf_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologico_locpf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologico_locpf_Cls = "ColumnSettings";
         Ddo_ambientetecnologico_locpf_Tooltip = "Op��es";
         Ddo_ambientetecnologico_locpf_Caption = "";
         Ddo_ambientetecnologico_taxaentregamlhr_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologico_taxaentregamlhr_Rangefilterto = "At�";
         Ddo_ambientetecnologico_taxaentregamlhr_Rangefilterfrom = "Desde";
         Ddo_ambientetecnologico_taxaentregamlhr_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologico_taxaentregamlhr_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologico_taxaentregamlhr_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologico_taxaentregamlhr_Includedatalist = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_taxaentregamlhr_Filterisrange = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_taxaentregamlhr_Filtertype = "Numeric";
         Ddo_ambientetecnologico_taxaentregamlhr_Includefilter = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_taxaentregamlhr_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_taxaentregamlhr_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_taxaentregamlhr_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologico_taxaentregamlhr_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologico_taxaentregamlhr_Cls = "ColumnSettings";
         Ddo_ambientetecnologico_taxaentregamlhr_Tooltip = "Op��es";
         Ddo_ambientetecnologico_taxaentregamlhr_Caption = "";
         Ddo_ambientetecnologico_taxaentregadsnv_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologico_taxaentregadsnv_Rangefilterto = "At�";
         Ddo_ambientetecnologico_taxaentregadsnv_Rangefilterfrom = "Desde";
         Ddo_ambientetecnologico_taxaentregadsnv_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologico_taxaentregadsnv_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologico_taxaentregadsnv_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologico_taxaentregadsnv_Includedatalist = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_taxaentregadsnv_Filterisrange = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_taxaentregadsnv_Filtertype = "Numeric";
         Ddo_ambientetecnologico_taxaentregadsnv_Includefilter = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_taxaentregadsnv_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_taxaentregadsnv_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_taxaentregadsnv_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologico_taxaentregadsnv_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologico_taxaentregadsnv_Cls = "ColumnSettings";
         Ddo_ambientetecnologico_taxaentregadsnv_Tooltip = "Op��es";
         Ddo_ambientetecnologico_taxaentregadsnv_Caption = "";
         Ddo_ambientetecnologico_descricao_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologico_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_ambientetecnologico_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologico_descricao_Loadingdata = "Carregando dados...";
         Ddo_ambientetecnologico_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologico_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_ambientetecnologico_descricao_Datalistproc = "GetWWAmbienteTecnologicoFilterData";
         Ddo_ambientetecnologico_descricao_Datalisttype = "Dynamic";
         Ddo_ambientetecnologico_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_descricao_Filtertype = "Character";
         Ddo_ambientetecnologico_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologico_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologico_descricao_Cls = "ColumnSettings";
         Ddo_ambientetecnologico_descricao_Tooltip = "Op��es";
         Ddo_ambientetecnologico_descricao_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Ambiente Tecnologico";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV63AmbienteTecnologico_DescricaoTitleFilterData',fld:'vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV67AmbienteTecnologico_TaxaEntregaDsnvTitleFilterData',fld:'vAMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLEFILTERDATA',pic:'',nv:null},{av:'AV71AmbienteTecnologico_TaxaEntregaMlhrTitleFilterData',fld:'vAMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLEFILTERDATA',pic:'',nv:null},{av:'AV75AmbienteTecnologico_LocPFTitleFilterData',fld:'vAMBIENTETECNOLOGICO_LOCPFTITLEFILTERDATA',pic:'',nv:null},{av:'AV79AmbienteTecnologico_AtivoTitleFilterData',fld:'vAMBIENTETECNOLOGICO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV82AmbienteTecnologico_FatorAjusteTitleFilterData',fld:'vAMBIENTETECNOLOGICO_FATORAJUSTETITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtAmbienteTecnologico_Descricao_Titleformat',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Titleformat'},{av:'edtAmbienteTecnologico_Descricao_Title',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Title'},{av:'edtAmbienteTecnologico_TaxaEntregaDsnv_Titleformat',ctrl:'AMBIENTETECNOLOGICO_TAXAENTREGADSNV',prop:'Titleformat'},{av:'edtAmbienteTecnologico_TaxaEntregaDsnv_Title',ctrl:'AMBIENTETECNOLOGICO_TAXAENTREGADSNV',prop:'Title'},{av:'edtAmbienteTecnologico_TaxaEntregaMlhr_Titleformat',ctrl:'AMBIENTETECNOLOGICO_TAXAENTREGAMLHR',prop:'Titleformat'},{av:'edtAmbienteTecnologico_TaxaEntregaMlhr_Title',ctrl:'AMBIENTETECNOLOGICO_TAXAENTREGAMLHR',prop:'Title'},{av:'edtAmbienteTecnologico_LocPF_Titleformat',ctrl:'AMBIENTETECNOLOGICO_LOCPF',prop:'Titleformat'},{av:'edtAmbienteTecnologico_LocPF_Title',ctrl:'AMBIENTETECNOLOGICO_LOCPF',prop:'Title'},{av:'chkAmbienteTecnologico_Ativo_Titleformat',ctrl:'AMBIENTETECNOLOGICO_ATIVO',prop:'Titleformat'},{av:'chkAmbienteTecnologico_Ativo.Title.Text',ctrl:'AMBIENTETECNOLOGICO_ATIVO',prop:'Title'},{av:'edtAmbienteTecnologico_FatorAjuste_Titleformat',ctrl:'AMBIENTETECNOLOGICO_FATORAJUSTE',prop:'Titleformat'},{av:'edtAmbienteTecnologico_FatorAjuste_Title',ctrl:'AMBIENTETECNOLOGICO_FATORAJUSTE',prop:'Title'},{av:'AV88GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV89GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavBtnassociation_Title',ctrl:'vBTNASSOCIATION',prop:'Title'},{av:'edtavBtnassociationsistema_Title',ctrl:'vBTNASSOCIATIONSISTEMA',prop:'Title'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E119P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICO_DESCRICAO.ONOPTIONCLICKED","{handler:'E129P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_ambientetecnologico_descricao_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologico_descricao_Filteredtext_get',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_ambientetecnologico_descricao_Selectedvalue_get',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_locpf_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_LOCPF',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_ativo_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_fatorajuste_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_FATORAJUSTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV.ONOPTIONCLICKED","{handler:'E139P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_ambientetecnologico_taxaentregadsnv_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_get',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV',prop:'FilteredText_get'},{av:'Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_get',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV',prop:'SortedStatus'},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_locpf_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_LOCPF',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_ativo_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_fatorajuste_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_FATORAJUSTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR.ONOPTIONCLICKED","{handler:'E149P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_ambientetecnologico_taxaentregamlhr_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_get',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR',prop:'FilteredText_get'},{av:'Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_get',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR',prop:'SortedStatus'},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_locpf_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_LOCPF',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_ativo_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_fatorajuste_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_FATORAJUSTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICO_LOCPF.ONOPTIONCLICKED","{handler:'E159P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_ambientetecnologico_locpf_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICO_LOCPF',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologico_locpf_Filteredtext_get',ctrl:'DDO_AMBIENTETECNOLOGICO_LOCPF',prop:'FilteredText_get'},{av:'Ddo_ambientetecnologico_locpf_Filteredtextto_get',ctrl:'DDO_AMBIENTETECNOLOGICO_LOCPF',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologico_locpf_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_LOCPF',prop:'SortedStatus'},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_ativo_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_fatorajuste_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_FATORAJUSTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICO_ATIVO.ONOPTIONCLICKED","{handler:'E169P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_ambientetecnologico_ativo_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologico_ativo_Selectedvalue_get',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologico_ativo_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SortedStatus'},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_locpf_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_LOCPF',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_fatorajuste_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_FATORAJUSTE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICO_FATORAJUSTE.ONOPTIONCLICKED","{handler:'E179P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_ambientetecnologico_fatorajuste_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICO_FATORAJUSTE',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologico_fatorajuste_Filteredtext_get',ctrl:'DDO_AMBIENTETECNOLOGICO_FATORAJUSTE',prop:'FilteredText_get'},{av:'Ddo_ambientetecnologico_fatorajuste_Filteredtextto_get',ctrl:'DDO_AMBIENTETECNOLOGICO_FATORAJUSTE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologico_fatorajuste_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_FATORAJUSTE',prop:'SortedStatus'},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_locpf_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_LOCPF',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_ativo_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E319P2',iparms:[{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV30Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV31Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV58Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV57btnAssociation',fld:'vBTNASSOCIATION',pic:'',nv:''},{av:'edtavBtnassociation_Tooltiptext',ctrl:'vBTNASSOCIATION',prop:'Tooltiptext'},{av:'AV60btnAssociationSistema',fld:'vBTNASSOCIATIONSISTEMA',pic:'',nv:''},{av:'edtavBtnassociationsistema_Tooltiptext',ctrl:'vBTNASSOCIATIONSISTEMA',prop:'Tooltiptext'},{av:'edtAmbienteTecnologico_Descricao_Link',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E189P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E249P2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E199P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao3_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO3',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E259P2',iparms:[{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E269P2',iparms:[],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E209P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao3_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO3',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E279P2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E219P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao3_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO3',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E289P2',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavAmbientetecnologico_descricao3_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E229P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_descricao_Filteredtext_set',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'FilteredText_set'},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_descricao_Selectedvalue_set',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_set',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV',prop:'FilteredText_set'},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_set',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNV',prop:'FilteredTextTo_set'},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_set',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR',prop:'FilteredText_set'},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_set',ctrl:'DDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHR',prop:'FilteredTextTo_set'},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'Ddo_ambientetecnologico_locpf_Filteredtext_set',ctrl:'DDO_AMBIENTETECNOLOGICO_LOCPF',prop:'FilteredText_set'},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'Ddo_ambientetecnologico_locpf_Filteredtextto_set',ctrl:'DDO_AMBIENTETECNOLOGICO_LOCPF',prop:'FilteredTextTo_set'},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_ambientetecnologico_ativo_Selectedvalue_set',ctrl:'DDO_AMBIENTETECNOLOGICO_ATIVO',prop:'SelectedValue_set'},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'Ddo_ambientetecnologico_fatorajuste_Filteredtext_set',ctrl:'DDO_AMBIENTETECNOLOGICO_FATORAJUSTE',prop:'FilteredText_set'},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'Ddo_ambientetecnologico_fatorajuste_Filteredtextto_set',ctrl:'DDO_AMBIENTETECNOLOGICO_FATORAJUSTE',prop:'FilteredTextTo_set'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavAmbientetecnologico_descricao1_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO1',prop:'Visible'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavAmbientetecnologico_descricao2_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO2',prop:'Visible'},{av:'edtavAmbientetecnologico_descricao3_Visible',ctrl:'vAMBIENTETECNOLOGICO_DESCRICAO3',prop:'Visible'}]}");
         setEventMetadata("'DOBTNASSOCIATION'","{handler:'E329P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOBTNASSOCIATIONSISTEMA'","{handler:'E339P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19AmbienteTecnologico_Descricao1',fld:'vAMBIENTETECNOLOGICO_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23AmbienteTecnologico_Descricao2',fld:'vAMBIENTETECNOLOGICO_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27AmbienteTecnologico_Descricao3',fld:'vAMBIENTETECNOLOGICO_DESCRICAO3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV64TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV65TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV68TFAmbienteTecnologico_TaxaEntregaDsnv',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV',pic:'ZZZ9',nv:0},{av:'AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGADSNV_TO',pic:'ZZZ9',nv:0},{av:'AV72TFAmbienteTecnologico_TaxaEntregaMlhr',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR',pic:'ZZZ9',nv:0},{av:'AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To',fld:'vTFAMBIENTETECNOLOGICO_TAXAENTREGAMLHR_TO',pic:'ZZZ9',nv:0},{av:'AV76TFAmbienteTecnologico_LocPF',fld:'vTFAMBIENTETECNOLOGICO_LOCPF',pic:'ZZZ9',nv:0},{av:'AV77TFAmbienteTecnologico_LocPF_To',fld:'vTFAMBIENTETECNOLOGICO_LOCPF_TO',pic:'ZZZ9',nv:0},{av:'AV80TFAmbienteTecnologico_Ativo_Sel',fld:'vTFAMBIENTETECNOLOGICO_ATIVO_SEL',pic:'9',nv:0},{av:'AV83TFAmbienteTecnologico_FatorAjuste',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE',pic:'ZZ9.9999',nv:0.0},{av:'AV84TFAmbienteTecnologico_FatorAjuste_To',fld:'vTFAMBIENTETECNOLOGICO_FATORAJUSTE_TO',pic:'ZZ9.9999',nv:0.0},{av:'AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGADSNVTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_TAXAENTREGAMLHRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_LOCPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_FATORAJUSTETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59AmbienteTecnologico_AreaTrabalhoCod',fld:'vAMBIENTETECNOLOGICO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV117Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E239P2',iparms:[{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_ambientetecnologico_descricao_Activeeventkey = "";
         Ddo_ambientetecnologico_descricao_Filteredtext_get = "";
         Ddo_ambientetecnologico_descricao_Selectedvalue_get = "";
         Ddo_ambientetecnologico_taxaentregadsnv_Activeeventkey = "";
         Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_get = "";
         Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_get = "";
         Ddo_ambientetecnologico_taxaentregamlhr_Activeeventkey = "";
         Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_get = "";
         Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_get = "";
         Ddo_ambientetecnologico_locpf_Activeeventkey = "";
         Ddo_ambientetecnologico_locpf_Filteredtext_get = "";
         Ddo_ambientetecnologico_locpf_Filteredtextto_get = "";
         Ddo_ambientetecnologico_ativo_Activeeventkey = "";
         Ddo_ambientetecnologico_ativo_Selectedvalue_get = "";
         Ddo_ambientetecnologico_fatorajuste_Activeeventkey = "";
         Ddo_ambientetecnologico_fatorajuste_Filteredtext_get = "";
         Ddo_ambientetecnologico_fatorajuste_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV17DynamicFiltersSelector1 = "";
         AV19AmbienteTecnologico_Descricao1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23AmbienteTecnologico_Descricao2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV27AmbienteTecnologico_Descricao3 = "";
         AV64TFAmbienteTecnologico_Descricao = "";
         AV65TFAmbienteTecnologico_Descricao_Sel = "";
         AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = "";
         AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace = "";
         AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace = "";
         AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace = "";
         AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace = "";
         AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace = "";
         AV117Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV86DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV63AmbienteTecnologico_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67AmbienteTecnologico_TaxaEntregaDsnvTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV71AmbienteTecnologico_TaxaEntregaMlhrTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV75AmbienteTecnologico_LocPFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79AmbienteTecnologico_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV82AmbienteTecnologico_FatorAjusteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_ambientetecnologico_descricao_Filteredtext_set = "";
         Ddo_ambientetecnologico_descricao_Selectedvalue_set = "";
         Ddo_ambientetecnologico_descricao_Sortedstatus = "";
         Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_set = "";
         Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_set = "";
         Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus = "";
         Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_set = "";
         Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_set = "";
         Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus = "";
         Ddo_ambientetecnologico_locpf_Filteredtext_set = "";
         Ddo_ambientetecnologico_locpf_Filteredtextto_set = "";
         Ddo_ambientetecnologico_locpf_Sortedstatus = "";
         Ddo_ambientetecnologico_ativo_Selectedvalue_set = "";
         Ddo_ambientetecnologico_ativo_Sortedstatus = "";
         Ddo_ambientetecnologico_fatorajuste_Filteredtext_set = "";
         Ddo_ambientetecnologico_fatorajuste_Filteredtextto_set = "";
         Ddo_ambientetecnologico_fatorajuste_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV30Update = "";
         AV112Update_GXI = "";
         AV31Delete = "";
         AV113Delete_GXI = "";
         AV58Display = "";
         AV114Display_GXI = "";
         A352AmbienteTecnologico_Descricao = "";
         AV57btnAssociation = "";
         AV115Btnassociation_GXI = "";
         AV60btnAssociationSistema = "";
         AV116Btnassociationsistema_GXI = "";
         scmdbuf = "";
         H009P2_A5AreaTrabalho_Codigo = new int[1] ;
         H009P2_A6AreaTrabalho_Descricao = new String[] {""} ;
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H009P3_A5AreaTrabalho_Codigo = new int[1] ;
         H009P3_A6AreaTrabalho_Descricao = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         lV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = "";
         lV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = "";
         lV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = "";
         lV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = "";
         AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 = "";
         AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 = "";
         AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 = "";
         AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 = "";
         AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 = "";
         AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 = "";
         AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel = "";
         AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao = "";
         H009P4_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         H009P4_A1423AmbienteTecnologico_FatorAjuste = new decimal[1] ;
         H009P4_n1423AmbienteTecnologico_FatorAjuste = new bool[] {false} ;
         H009P4_A353AmbienteTecnologico_Ativo = new bool[] {false} ;
         H009P4_A978AmbienteTecnologico_LocPF = new short[1] ;
         H009P4_n978AmbienteTecnologico_LocPF = new bool[] {false} ;
         H009P4_A977AmbienteTecnologico_TaxaEntregaMlhr = new short[1] ;
         H009P4_n977AmbienteTecnologico_TaxaEntregaMlhr = new bool[] {false} ;
         H009P4_A976AmbienteTecnologico_TaxaEntregaDsnv = new short[1] ;
         H009P4_n976AmbienteTecnologico_TaxaEntregaDsnv = new bool[] {false} ;
         H009P4_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         H009P4_A351AmbienteTecnologico_Codigo = new int[1] ;
         H009P5_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV52Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblAmbientetecnologicotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblFiltertextambientetecnologico_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwambientetecnologico__default(),
            new Object[][] {
                new Object[] {
               H009P2_A5AreaTrabalho_Codigo, H009P2_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H009P3_A5AreaTrabalho_Codigo, H009P3_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H009P4_A728AmbienteTecnologico_AreaTrabalhoCod, H009P4_A1423AmbienteTecnologico_FatorAjuste, H009P4_n1423AmbienteTecnologico_FatorAjuste, H009P4_A353AmbienteTecnologico_Ativo, H009P4_A978AmbienteTecnologico_LocPF, H009P4_n978AmbienteTecnologico_LocPF, H009P4_A977AmbienteTecnologico_TaxaEntregaMlhr, H009P4_n977AmbienteTecnologico_TaxaEntregaMlhr, H009P4_A976AmbienteTecnologico_TaxaEntregaDsnv, H009P4_n976AmbienteTecnologico_TaxaEntregaDsnv,
               H009P4_A352AmbienteTecnologico_Descricao, H009P4_A351AmbienteTecnologico_Codigo
               }
               , new Object[] {
               H009P5_AGRID_nRecordCount
               }
            }
         );
         AV117Pgmname = "WWAmbienteTecnologico";
         /* GeneXus formulas. */
         AV117Pgmname = "WWAmbienteTecnologico";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_77 ;
      private short nGXsfl_77_idx=1 ;
      private short AV13OrderedBy ;
      private short AV68TFAmbienteTecnologico_TaxaEntregaDsnv ;
      private short AV69TFAmbienteTecnologico_TaxaEntregaDsnv_To ;
      private short AV72TFAmbienteTecnologico_TaxaEntregaMlhr ;
      private short AV73TFAmbienteTecnologico_TaxaEntregaMlhr_To ;
      private short AV76TFAmbienteTecnologico_LocPF ;
      private short AV77TFAmbienteTecnologico_LocPF_To ;
      private short AV80TFAmbienteTecnologico_Ativo_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A976AmbienteTecnologico_TaxaEntregaDsnv ;
      private short A977AmbienteTecnologico_TaxaEntregaMlhr ;
      private short A978AmbienteTecnologico_LocPF ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_77_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv ;
      private short AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to ;
      private short AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr ;
      private short AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to ;
      private short AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf ;
      private short AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to ;
      private short AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel ;
      private short edtAmbienteTecnologico_Descricao_Titleformat ;
      private short edtAmbienteTecnologico_TaxaEntregaDsnv_Titleformat ;
      private short edtAmbienteTecnologico_TaxaEntregaMlhr_Titleformat ;
      private short edtAmbienteTecnologico_LocPF_Titleformat ;
      private short chkAmbienteTecnologico_Ativo_Titleformat ;
      private short edtAmbienteTecnologico_FatorAjuste_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV59AmbienteTecnologico_AreaTrabalhoCod ;
      private int A351AmbienteTecnologico_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters ;
      private int edtavTfambientetecnologico_descricao_Visible ;
      private int edtavTfambientetecnologico_descricao_sel_Visible ;
      private int edtavTfambientetecnologico_taxaentregadsnv_Visible ;
      private int edtavTfambientetecnologico_taxaentregadsnv_to_Visible ;
      private int edtavTfambientetecnologico_taxaentregamlhr_Visible ;
      private int edtavTfambientetecnologico_taxaentregamlhr_to_Visible ;
      private int edtavTfambientetecnologico_locpf_Visible ;
      private int edtavTfambientetecnologico_locpf_to_Visible ;
      private int edtavTfambientetecnologico_ativo_sel_Visible ;
      private int edtavTfambientetecnologico_fatorajuste_Visible ;
      private int edtavTfambientetecnologico_fatorajuste_to_Visible ;
      private int edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_ambientetecnologico_taxaentregadsnvtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_ambientetecnologico_taxaentregamlhrtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_ambientetecnologico_locpftitlecontrolidtoreplace_Visible ;
      private int edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_ambientetecnologico_fatorajustetitlecontrolidtoreplace_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A728AmbienteTecnologico_AreaTrabalhoCod ;
      private int AV92WWAmbienteTecnologicoDS_1_Ambientetecnologico_areatrabalhocod ;
      private int edtavOrdereddsc_Visible ;
      private int AV16AreaTrabalho_Codigo ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int AV87PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavAmbientetecnologico_descricao1_Visible ;
      private int edtavAmbientetecnologico_descricao2_Visible ;
      private int edtavAmbientetecnologico_descricao3_Visible ;
      private int AV118GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavBtnassociation_Enabled ;
      private int edtavBtnassociation_Visible ;
      private int edtavBtnassociationsistema_Enabled ;
      private int edtavBtnassociationsistema_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV88GridCurrentPage ;
      private long AV89GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV83TFAmbienteTecnologico_FatorAjuste ;
      private decimal AV84TFAmbienteTecnologico_FatorAjuste_To ;
      private decimal A1423AmbienteTecnologico_FatorAjuste ;
      private decimal AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste ;
      private decimal AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_ambientetecnologico_descricao_Activeeventkey ;
      private String Ddo_ambientetecnologico_descricao_Filteredtext_get ;
      private String Ddo_ambientetecnologico_descricao_Selectedvalue_get ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Activeeventkey ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_get ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_get ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Activeeventkey ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_get ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_get ;
      private String Ddo_ambientetecnologico_locpf_Activeeventkey ;
      private String Ddo_ambientetecnologico_locpf_Filteredtext_get ;
      private String Ddo_ambientetecnologico_locpf_Filteredtextto_get ;
      private String Ddo_ambientetecnologico_ativo_Activeeventkey ;
      private String Ddo_ambientetecnologico_ativo_Selectedvalue_get ;
      private String Ddo_ambientetecnologico_fatorajuste_Activeeventkey ;
      private String Ddo_ambientetecnologico_fatorajuste_Filteredtext_get ;
      private String Ddo_ambientetecnologico_fatorajuste_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_77_idx="0001" ;
      private String AV117Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_ambientetecnologico_descricao_Caption ;
      private String Ddo_ambientetecnologico_descricao_Tooltip ;
      private String Ddo_ambientetecnologico_descricao_Cls ;
      private String Ddo_ambientetecnologico_descricao_Filteredtext_set ;
      private String Ddo_ambientetecnologico_descricao_Selectedvalue_set ;
      private String Ddo_ambientetecnologico_descricao_Dropdownoptionstype ;
      private String Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologico_descricao_Sortedstatus ;
      private String Ddo_ambientetecnologico_descricao_Filtertype ;
      private String Ddo_ambientetecnologico_descricao_Datalisttype ;
      private String Ddo_ambientetecnologico_descricao_Datalistproc ;
      private String Ddo_ambientetecnologico_descricao_Sortasc ;
      private String Ddo_ambientetecnologico_descricao_Sortdsc ;
      private String Ddo_ambientetecnologico_descricao_Loadingdata ;
      private String Ddo_ambientetecnologico_descricao_Cleanfilter ;
      private String Ddo_ambientetecnologico_descricao_Noresultsfound ;
      private String Ddo_ambientetecnologico_descricao_Searchbuttontext ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Caption ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Tooltip ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Cls ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Filteredtext_set ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Filteredtextto_set ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Dropdownoptionstype ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Sortedstatus ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Filtertype ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Sortasc ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Sortdsc ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Cleanfilter ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Rangefilterfrom ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Rangefilterto ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Searchbuttontext ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Caption ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Tooltip ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Cls ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Filteredtext_set ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Filteredtextto_set ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Dropdownoptionstype ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Sortedstatus ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Filtertype ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Sortasc ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Sortdsc ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Cleanfilter ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Rangefilterfrom ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Rangefilterto ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Searchbuttontext ;
      private String Ddo_ambientetecnologico_locpf_Caption ;
      private String Ddo_ambientetecnologico_locpf_Tooltip ;
      private String Ddo_ambientetecnologico_locpf_Cls ;
      private String Ddo_ambientetecnologico_locpf_Filteredtext_set ;
      private String Ddo_ambientetecnologico_locpf_Filteredtextto_set ;
      private String Ddo_ambientetecnologico_locpf_Dropdownoptionstype ;
      private String Ddo_ambientetecnologico_locpf_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologico_locpf_Sortedstatus ;
      private String Ddo_ambientetecnologico_locpf_Filtertype ;
      private String Ddo_ambientetecnologico_locpf_Sortasc ;
      private String Ddo_ambientetecnologico_locpf_Sortdsc ;
      private String Ddo_ambientetecnologico_locpf_Cleanfilter ;
      private String Ddo_ambientetecnologico_locpf_Rangefilterfrom ;
      private String Ddo_ambientetecnologico_locpf_Rangefilterto ;
      private String Ddo_ambientetecnologico_locpf_Searchbuttontext ;
      private String Ddo_ambientetecnologico_ativo_Caption ;
      private String Ddo_ambientetecnologico_ativo_Tooltip ;
      private String Ddo_ambientetecnologico_ativo_Cls ;
      private String Ddo_ambientetecnologico_ativo_Selectedvalue_set ;
      private String Ddo_ambientetecnologico_ativo_Dropdownoptionstype ;
      private String Ddo_ambientetecnologico_ativo_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologico_ativo_Sortedstatus ;
      private String Ddo_ambientetecnologico_ativo_Datalisttype ;
      private String Ddo_ambientetecnologico_ativo_Datalistfixedvalues ;
      private String Ddo_ambientetecnologico_ativo_Sortasc ;
      private String Ddo_ambientetecnologico_ativo_Sortdsc ;
      private String Ddo_ambientetecnologico_ativo_Cleanfilter ;
      private String Ddo_ambientetecnologico_ativo_Searchbuttontext ;
      private String Ddo_ambientetecnologico_fatorajuste_Caption ;
      private String Ddo_ambientetecnologico_fatorajuste_Tooltip ;
      private String Ddo_ambientetecnologico_fatorajuste_Cls ;
      private String Ddo_ambientetecnologico_fatorajuste_Filteredtext_set ;
      private String Ddo_ambientetecnologico_fatorajuste_Filteredtextto_set ;
      private String Ddo_ambientetecnologico_fatorajuste_Dropdownoptionstype ;
      private String Ddo_ambientetecnologico_fatorajuste_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologico_fatorajuste_Sortedstatus ;
      private String Ddo_ambientetecnologico_fatorajuste_Filtertype ;
      private String Ddo_ambientetecnologico_fatorajuste_Sortasc ;
      private String Ddo_ambientetecnologico_fatorajuste_Sortdsc ;
      private String Ddo_ambientetecnologico_fatorajuste_Cleanfilter ;
      private String Ddo_ambientetecnologico_fatorajuste_Rangefilterfrom ;
      private String Ddo_ambientetecnologico_fatorajuste_Rangefilterto ;
      private String Ddo_ambientetecnologico_fatorajuste_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfambientetecnologico_descricao_Internalname ;
      private String edtavTfambientetecnologico_descricao_Jsonclick ;
      private String edtavTfambientetecnologico_descricao_sel_Internalname ;
      private String edtavTfambientetecnologico_descricao_sel_Jsonclick ;
      private String edtavTfambientetecnologico_taxaentregadsnv_Internalname ;
      private String edtavTfambientetecnologico_taxaentregadsnv_Jsonclick ;
      private String edtavTfambientetecnologico_taxaentregadsnv_to_Internalname ;
      private String edtavTfambientetecnologico_taxaentregadsnv_to_Jsonclick ;
      private String edtavTfambientetecnologico_taxaentregamlhr_Internalname ;
      private String edtavTfambientetecnologico_taxaentregamlhr_Jsonclick ;
      private String edtavTfambientetecnologico_taxaentregamlhr_to_Internalname ;
      private String edtavTfambientetecnologico_taxaentregamlhr_to_Jsonclick ;
      private String edtavTfambientetecnologico_locpf_Internalname ;
      private String edtavTfambientetecnologico_locpf_Jsonclick ;
      private String edtavTfambientetecnologico_locpf_to_Internalname ;
      private String edtavTfambientetecnologico_locpf_to_Jsonclick ;
      private String edtavTfambientetecnologico_ativo_sel_Internalname ;
      private String edtavTfambientetecnologico_ativo_sel_Jsonclick ;
      private String edtavTfambientetecnologico_fatorajuste_Internalname ;
      private String edtavTfambientetecnologico_fatorajuste_Jsonclick ;
      private String edtavTfambientetecnologico_fatorajuste_to_Internalname ;
      private String edtavTfambientetecnologico_fatorajuste_to_Jsonclick ;
      private String edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_ambientetecnologico_taxaentregadsnvtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_ambientetecnologico_taxaentregamlhrtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_ambientetecnologico_locpftitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_ambientetecnologico_ativotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_ambientetecnologico_fatorajustetitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtAmbienteTecnologico_Codigo_Internalname ;
      private String edtAmbienteTecnologico_Descricao_Internalname ;
      private String edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname ;
      private String edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname ;
      private String edtAmbienteTecnologico_LocPF_Internalname ;
      private String chkAmbienteTecnologico_Ativo_Internalname ;
      private String edtAmbienteTecnologico_FatorAjuste_Internalname ;
      private String edtavBtnassociation_Internalname ;
      private String edtavBtnassociationsistema_Internalname ;
      private String scmdbuf ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String edtavOrdereddsc_Internalname ;
      private String dynavAmbientetecnologico_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavAmbientetecnologico_descricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavAmbientetecnologico_descricao2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavAmbientetecnologico_descricao3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_ambientetecnologico_descricao_Internalname ;
      private String Ddo_ambientetecnologico_taxaentregadsnv_Internalname ;
      private String Ddo_ambientetecnologico_taxaentregamlhr_Internalname ;
      private String Ddo_ambientetecnologico_locpf_Internalname ;
      private String Ddo_ambientetecnologico_ativo_Internalname ;
      private String Ddo_ambientetecnologico_fatorajuste_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtAmbienteTecnologico_Descricao_Title ;
      private String edtAmbienteTecnologico_TaxaEntregaDsnv_Title ;
      private String edtAmbienteTecnologico_TaxaEntregaMlhr_Title ;
      private String edtAmbienteTecnologico_LocPF_Title ;
      private String edtAmbienteTecnologico_FatorAjuste_Title ;
      private String edtavBtnassociation_Title ;
      private String edtavBtnassociationsistema_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtavBtnassociation_Tooltiptext ;
      private String edtavBtnassociationsistema_Tooltiptext ;
      private String edtAmbienteTecnologico_Descricao_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblAmbientetecnologicotitle_Internalname ;
      private String lblAmbientetecnologicotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextambientetecnologico_areatrabalhocod_Internalname ;
      private String lblFiltertextambientetecnologico_areatrabalhocod_Jsonclick ;
      private String dynavAmbientetecnologico_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavAmbientetecnologico_descricao1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavAmbientetecnologico_descricao2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavAmbientetecnologico_descricao3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_77_fel_idx="0001" ;
      private String ROClassString ;
      private String edtAmbienteTecnologico_Codigo_Jsonclick ;
      private String edtAmbienteTecnologico_Descricao_Jsonclick ;
      private String edtAmbienteTecnologico_TaxaEntregaDsnv_Jsonclick ;
      private String edtAmbienteTecnologico_TaxaEntregaMlhr_Jsonclick ;
      private String edtAmbienteTecnologico_LocPF_Jsonclick ;
      private String edtAmbienteTecnologico_FatorAjuste_Jsonclick ;
      private String edtavBtnassociation_Jsonclick ;
      private String edtavBtnassociationsistema_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV29DynamicFiltersIgnoreFirst ;
      private bool AV28DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_ambientetecnologico_descricao_Includesortasc ;
      private bool Ddo_ambientetecnologico_descricao_Includesortdsc ;
      private bool Ddo_ambientetecnologico_descricao_Includefilter ;
      private bool Ddo_ambientetecnologico_descricao_Filterisrange ;
      private bool Ddo_ambientetecnologico_descricao_Includedatalist ;
      private bool Ddo_ambientetecnologico_taxaentregadsnv_Includesortasc ;
      private bool Ddo_ambientetecnologico_taxaentregadsnv_Includesortdsc ;
      private bool Ddo_ambientetecnologico_taxaentregadsnv_Includefilter ;
      private bool Ddo_ambientetecnologico_taxaentregadsnv_Filterisrange ;
      private bool Ddo_ambientetecnologico_taxaentregadsnv_Includedatalist ;
      private bool Ddo_ambientetecnologico_taxaentregamlhr_Includesortasc ;
      private bool Ddo_ambientetecnologico_taxaentregamlhr_Includesortdsc ;
      private bool Ddo_ambientetecnologico_taxaentregamlhr_Includefilter ;
      private bool Ddo_ambientetecnologico_taxaentregamlhr_Filterisrange ;
      private bool Ddo_ambientetecnologico_taxaentregamlhr_Includedatalist ;
      private bool Ddo_ambientetecnologico_locpf_Includesortasc ;
      private bool Ddo_ambientetecnologico_locpf_Includesortdsc ;
      private bool Ddo_ambientetecnologico_locpf_Includefilter ;
      private bool Ddo_ambientetecnologico_locpf_Filterisrange ;
      private bool Ddo_ambientetecnologico_locpf_Includedatalist ;
      private bool Ddo_ambientetecnologico_ativo_Includesortasc ;
      private bool Ddo_ambientetecnologico_ativo_Includesortdsc ;
      private bool Ddo_ambientetecnologico_ativo_Includefilter ;
      private bool Ddo_ambientetecnologico_ativo_Includedatalist ;
      private bool Ddo_ambientetecnologico_fatorajuste_Includesortasc ;
      private bool Ddo_ambientetecnologico_fatorajuste_Includesortdsc ;
      private bool Ddo_ambientetecnologico_fatorajuste_Includefilter ;
      private bool Ddo_ambientetecnologico_fatorajuste_Filterisrange ;
      private bool Ddo_ambientetecnologico_fatorajuste_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n976AmbienteTecnologico_TaxaEntregaDsnv ;
      private bool n977AmbienteTecnologico_TaxaEntregaMlhr ;
      private bool n978AmbienteTecnologico_LocPF ;
      private bool A353AmbienteTecnologico_Ativo ;
      private bool n1423AmbienteTecnologico_FatorAjuste ;
      private bool AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 ;
      private bool AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV30Update_IsBlob ;
      private bool AV31Delete_IsBlob ;
      private bool AV58Display_IsBlob ;
      private bool AV57btnAssociation_IsBlob ;
      private bool AV60btnAssociationSistema_IsBlob ;
      private String AV17DynamicFiltersSelector1 ;
      private String AV19AmbienteTecnologico_Descricao1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV23AmbienteTecnologico_Descricao2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV27AmbienteTecnologico_Descricao3 ;
      private String AV64TFAmbienteTecnologico_Descricao ;
      private String AV65TFAmbienteTecnologico_Descricao_Sel ;
      private String AV66ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace ;
      private String AV70ddo_AmbienteTecnologico_TaxaEntregaDsnvTitleControlIdToReplace ;
      private String AV74ddo_AmbienteTecnologico_TaxaEntregaMlhrTitleControlIdToReplace ;
      private String AV78ddo_AmbienteTecnologico_LocPFTitleControlIdToReplace ;
      private String AV81ddo_AmbienteTecnologico_AtivoTitleControlIdToReplace ;
      private String AV85ddo_AmbienteTecnologico_FatorAjusteTitleControlIdToReplace ;
      private String AV112Update_GXI ;
      private String AV113Delete_GXI ;
      private String AV114Display_GXI ;
      private String A352AmbienteTecnologico_Descricao ;
      private String AV115Btnassociation_GXI ;
      private String AV116Btnassociationsistema_GXI ;
      private String lV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 ;
      private String lV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 ;
      private String lV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 ;
      private String lV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao ;
      private String AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 ;
      private String AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 ;
      private String AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 ;
      private String AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 ;
      private String AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 ;
      private String AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 ;
      private String AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel ;
      private String AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao ;
      private String AV30Update ;
      private String AV31Delete ;
      private String AV58Display ;
      private String AV57btnAssociation ;
      private String AV60btnAssociationSistema ;
      private IGxSession AV52Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox dynavAmbientetecnologico_areatrabalhocod ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkAmbienteTecnologico_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H009P2_A5AreaTrabalho_Codigo ;
      private String[] H009P2_A6AreaTrabalho_Descricao ;
      private int[] H009P3_A5AreaTrabalho_Codigo ;
      private String[] H009P3_A6AreaTrabalho_Descricao ;
      private int[] H009P4_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private decimal[] H009P4_A1423AmbienteTecnologico_FatorAjuste ;
      private bool[] H009P4_n1423AmbienteTecnologico_FatorAjuste ;
      private bool[] H009P4_A353AmbienteTecnologico_Ativo ;
      private short[] H009P4_A978AmbienteTecnologico_LocPF ;
      private bool[] H009P4_n978AmbienteTecnologico_LocPF ;
      private short[] H009P4_A977AmbienteTecnologico_TaxaEntregaMlhr ;
      private bool[] H009P4_n977AmbienteTecnologico_TaxaEntregaMlhr ;
      private short[] H009P4_A976AmbienteTecnologico_TaxaEntregaDsnv ;
      private bool[] H009P4_n976AmbienteTecnologico_TaxaEntregaDsnv ;
      private String[] H009P4_A352AmbienteTecnologico_Descricao ;
      private int[] H009P4_A351AmbienteTecnologico_Codigo ;
      private long[] H009P5_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV63AmbienteTecnologico_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV67AmbienteTecnologico_TaxaEntregaDsnvTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV71AmbienteTecnologico_TaxaEntregaMlhrTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV75AmbienteTecnologico_LocPFTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV79AmbienteTecnologico_AtivoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV82AmbienteTecnologico_FatorAjusteTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV86DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwambientetecnologico__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H009P4( IGxContext context ,
                                             String AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 ,
                                             String AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 ,
                                             bool AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 ,
                                             String AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 ,
                                             String AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 ,
                                             bool AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 ,
                                             String AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 ,
                                             String AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 ,
                                             String AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel ,
                                             String AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao ,
                                             short AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv ,
                                             short AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to ,
                                             short AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr ,
                                             short AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to ,
                                             short AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf ,
                                             short AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to ,
                                             short AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel ,
                                             decimal AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste ,
                                             decimal AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             short A976AmbienteTecnologico_TaxaEntregaDsnv ,
                                             short A977AmbienteTecnologico_TaxaEntregaMlhr ,
                                             short A978AmbienteTecnologico_LocPF ,
                                             bool A353AmbienteTecnologico_Ativo ,
                                             decimal A1423AmbienteTecnologico_FatorAjuste ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A728AmbienteTecnologico_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [19] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [AmbienteTecnologico_AreaTrabalhoCod], [AmbienteTecnologico_FatorAjuste], [AmbienteTecnologico_Ativo], [AmbienteTecnologico_LocPF], [AmbienteTecnologico_TaxaEntregaMlhr], [AmbienteTecnologico_TaxaEntregaDsnv], [AmbienteTecnologico_Descricao], [AmbienteTecnologico_Codigo]";
         sFromString = " FROM [AmbienteTecnologico] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([AmbienteTecnologico_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] like '%' + @lV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] like '%' + @lV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] like '%' + @lV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao)) ) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] like @lV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel)) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] = @AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_TaxaEntregaDsnv] >= @AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_TaxaEntregaDsnv] <= @AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_TaxaEntregaMlhr] >= @AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_TaxaEntregaMlhr] <= @AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (0==AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_LocPF] >= @AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_LocPF] <= @AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Ativo] = 1)";
         }
         if ( AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Ativo] = 0)";
         }
         if ( ! (Convert.ToDecimal(0)==AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_FatorAjuste] >= @AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_FatorAjuste] <= @AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_TaxaEntregaDsnv]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_TaxaEntregaDsnv] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_TaxaEntregaMlhr]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_TaxaEntregaMlhr] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_LocPF]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_LocPF] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_Ativo]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_Ativo] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_FatorAjuste]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_FatorAjuste] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [AmbienteTecnologico_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H009P5( IGxContext context ,
                                             String AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1 ,
                                             String AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1 ,
                                             bool AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 ,
                                             String AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2 ,
                                             String AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2 ,
                                             bool AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 ,
                                             String AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3 ,
                                             String AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3 ,
                                             String AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel ,
                                             String AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao ,
                                             short AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv ,
                                             short AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to ,
                                             short AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr ,
                                             short AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to ,
                                             short AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf ,
                                             short AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to ,
                                             short AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel ,
                                             decimal AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste ,
                                             decimal AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             short A976AmbienteTecnologico_TaxaEntregaDsnv ,
                                             short A977AmbienteTecnologico_TaxaEntregaMlhr ,
                                             short A978AmbienteTecnologico_LocPF ,
                                             bool A353AmbienteTecnologico_Ativo ,
                                             decimal A1423AmbienteTecnologico_FatorAjuste ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A728AmbienteTecnologico_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [14] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [AmbienteTecnologico] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([AmbienteTecnologico_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV93WWAmbienteTecnologicoDS_2_Dynamicfiltersselector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1)) ) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] like '%' + @lV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV95WWAmbienteTecnologicoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV96WWAmbienteTecnologicoDS_5_Dynamicfiltersselector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2)) ) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] like '%' + @lV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV98WWAmbienteTecnologicoDS_7_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWAmbienteTecnologicoDS_8_Dynamicfiltersselector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3)) ) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] like '%' + @lV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao)) ) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] like @lV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel)) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Descricao] = @AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_TaxaEntregaDsnv] >= @AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_TaxaEntregaDsnv] <= @AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_TaxaEntregaMlhr] >= @AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_TaxaEntregaMlhr] <= @AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (0==AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_LocPF] >= @AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_LocPF] <= @AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Ativo] = 1)";
         }
         if ( AV109WWAmbienteTecnologicoDS_18_Tfambientetecnologico_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_Ativo] = 0)";
         }
         if ( ! (Convert.ToDecimal(0)==AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_FatorAjuste] >= @AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to) )
         {
            sWhereString = sWhereString + " and ([AmbienteTecnologico_FatorAjuste] <= @AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H009P4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (decimal)dynConstraints[17] , (decimal)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] , (decimal)dynConstraints[24] , (short)dynConstraints[25] , (bool)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] );
               case 3 :
                     return conditional_H009P5(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (decimal)dynConstraints[17] , (decimal)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] , (decimal)dynConstraints[24] , (short)dynConstraints[25] , (bool)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009P2 ;
          prmH009P2 = new Object[] {
          } ;
          Object[] prmH009P3 ;
          prmH009P3 = new Object[] {
          } ;
          Object[] prmH009P4 ;
          prmH009P4 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste",SqlDbType.Decimal,8,4} ,
          new Object[] {"@AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to",SqlDbType.Decimal,8,4} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH009P5 ;
          prmH009P5 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV94WWAmbienteTecnologicoDS_3_Ambientetecnologico_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV97WWAmbienteTecnologicoDS_6_Ambientetecnologico_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV100WWAmbienteTecnologicoDS_9_Ambientetecnologico_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV101WWAmbienteTecnologicoDS_10_Tfambientetecnologico_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV102WWAmbienteTecnologicoDS_11_Tfambientetecnologico_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV103WWAmbienteTecnologicoDS_12_Tfambientetecnologico_taxaentregadsnv",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV104WWAmbienteTecnologicoDS_13_Tfambientetecnologico_taxaentregadsnv_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV105WWAmbienteTecnologicoDS_14_Tfambientetecnologico_taxaentregamlhr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV106WWAmbienteTecnologicoDS_15_Tfambientetecnologico_taxaentregamlhr_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV107WWAmbienteTecnologicoDS_16_Tfambientetecnologico_locpf",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV108WWAmbienteTecnologicoDS_17_Tfambientetecnologico_locpf_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV110WWAmbienteTecnologicoDS_19_Tfambientetecnologico_fatorajuste",SqlDbType.Decimal,8,4} ,
          new Object[] {"@AV111WWAmbienteTecnologicoDS_20_Tfambientetecnologico_fatorajuste_to",SqlDbType.Decimal,8,4}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009P2", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009P2,0,0,true,false )
             ,new CursorDef("H009P3", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009P3,0,0,true,false )
             ,new CursorDef("H009P4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009P4,11,0,true,false )
             ,new CursorDef("H009P5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009P5,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                return;
       }
    }

 }

}
