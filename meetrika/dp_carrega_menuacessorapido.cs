/*
               File: DP_Carrega_MenuAcessoRapido
        Description: DP_Carrega_Menu Acesso Rapido
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:37.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_carrega_menuacessorapido : GXProcedure
   {
      public dp_carrega_menuacessorapido( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_carrega_menuacessorapido( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo ,
                           out IGxCollection aP1_Gxm2rootcol )
      {
         this.AV5Usuario_Codigo = aP0_Usuario_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_MenuAcessoRapido.SDT_MenuAcessoRapidoItem", "GxEv3Up14_Meetrika", "SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( int aP0_Usuario_Codigo )
      {
         this.AV5Usuario_Codigo = aP0_Usuario_Codigo;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_MenuAcessoRapido.SDT_MenuAcessoRapidoItem", "GxEv3Up14_Meetrika", "SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( int aP0_Usuario_Codigo ,
                                 out IGxCollection aP1_Gxm2rootcol )
      {
         dp_carrega_menuacessorapido objdp_carrega_menuacessorapido;
         objdp_carrega_menuacessorapido = new dp_carrega_menuacessorapido();
         objdp_carrega_menuacessorapido.AV5Usuario_Codigo = aP0_Usuario_Codigo;
         objdp_carrega_menuacessorapido.Gxm2rootcol = new GxObjectCollection( context, "SDT_MenuAcessoRapido.SDT_MenuAcessoRapidoItem", "GxEv3Up14_Meetrika", "SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem", "GeneXus.Programs") ;
         objdp_carrega_menuacessorapido.context.SetSubmitInitialConfig(context);
         objdp_carrega_menuacessorapido.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_carrega_menuacessorapido);
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_carrega_menuacessorapido)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12DS_UsuarioPerfil_3_Usuario_codigo = AV5Usuario_Codigo;
         /* Using cursor P00032 */
         pr_default.execute(0, new Object[] {AV12DS_UsuarioPerfil_3_Usuario_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A3Perfil_Codigo = P00032_A3Perfil_Codigo[0];
            A280Menu_Tipo = P00032_A280Menu_Tipo[0];
            A284Menu_Ativo = P00032_A284Menu_Ativo[0];
            A283Menu_Ordem = P00032_A283Menu_Ordem[0];
            A279Menu_Descricao = P00032_A279Menu_Descricao[0];
            n279Menu_Descricao = P00032_n279Menu_Descricao[0];
            A281Menu_Link = P00032_A281Menu_Link[0];
            n281Menu_Link = P00032_n281Menu_Link[0];
            A278Menu_Nome = P00032_A278Menu_Nome[0];
            A277Menu_Codigo = P00032_A277Menu_Codigo[0];
            A280Menu_Tipo = P00032_A280Menu_Tipo[0];
            A284Menu_Ativo = P00032_A284Menu_Ativo[0];
            A283Menu_Ordem = P00032_A283Menu_Ordem[0];
            A279Menu_Descricao = P00032_A279Menu_Descricao[0];
            n279Menu_Descricao = P00032_n279Menu_Descricao[0];
            A281Menu_Link = P00032_A281Menu_Link[0];
            n281Menu_Link = P00032_n281Menu_Link[0];
            A278Menu_Nome = P00032_A278Menu_Nome[0];
            Gxm1sdt_menuacessorapido = new SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem(context);
            Gxm2rootcol.Add(Gxm1sdt_menuacessorapido, 0);
            Gxm1sdt_menuacessorapido.gxTpr_Menu_codigo = A277Menu_Codigo;
            Gxm1sdt_menuacessorapido.gxTpr_Menu_nome = A278Menu_Nome;
            Gxm1sdt_menuacessorapido.gxTpr_Menu_link = A281Menu_Link;
            Gxm1sdt_menuacessorapido.gxTpr_Menu_descricao = A279Menu_Descricao;
            Gxm1sdt_menuacessorapido.gxTpr_Menu_ordem = A283Menu_Ordem;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00032_A3Perfil_Codigo = new int[1] ;
         P00032_A280Menu_Tipo = new short[1] ;
         P00032_A284Menu_Ativo = new bool[] {false} ;
         P00032_A283Menu_Ordem = new short[1] ;
         P00032_A279Menu_Descricao = new String[] {""} ;
         P00032_n279Menu_Descricao = new bool[] {false} ;
         P00032_A281Menu_Link = new String[] {""} ;
         P00032_n281Menu_Link = new bool[] {false} ;
         P00032_A278Menu_Nome = new String[] {""} ;
         P00032_A277Menu_Codigo = new int[1] ;
         A279Menu_Descricao = "";
         A281Menu_Link = "";
         A278Menu_Nome = "";
         Gxm1sdt_menuacessorapido = new SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_carrega_menuacessorapido__default(),
            new Object[][] {
                new Object[] {
               P00032_A3Perfil_Codigo, P00032_A280Menu_Tipo, P00032_A284Menu_Ativo, P00032_A283Menu_Ordem, P00032_A279Menu_Descricao, P00032_n279Menu_Descricao, P00032_A281Menu_Link, P00032_n281Menu_Link, P00032_A278Menu_Nome, P00032_A277Menu_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A280Menu_Tipo ;
      private short A283Menu_Ordem ;
      private int AV5Usuario_Codigo ;
      private int AV12DS_UsuarioPerfil_3_Usuario_codigo ;
      private int A3Perfil_Codigo ;
      private int A277Menu_Codigo ;
      private String scmdbuf ;
      private String A278Menu_Nome ;
      private bool A284Menu_Ativo ;
      private bool n279Menu_Descricao ;
      private bool n281Menu_Link ;
      private String A279Menu_Descricao ;
      private String A281Menu_Link ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00032_A3Perfil_Codigo ;
      private short[] P00032_A280Menu_Tipo ;
      private bool[] P00032_A284Menu_Ativo ;
      private short[] P00032_A283Menu_Ordem ;
      private String[] P00032_A279Menu_Descricao ;
      private bool[] P00032_n279Menu_Descricao ;
      private String[] P00032_A281Menu_Link ;
      private bool[] P00032_n281Menu_Link ;
      private String[] P00032_A278Menu_Nome ;
      private int[] P00032_A277Menu_Codigo ;
      private IGxCollection aP1_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem Gxm1sdt_menuacessorapido ;
   }

   public class dp_carrega_menuacessorapido__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00032 ;
          prmP00032 = new Object[] {
          new Object[] {"@AV12DS_UsuarioPerfil_3_Usuario_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00032", "SELECT DISTINCT NULL AS [Perfil_Codigo], NULL AS [Menu_Tipo], NULL AS [Menu_Ativo], [Menu_Ordem], [Menu_Descricao], [Menu_Link], [Menu_Nome], [Menu_Codigo] FROM ( SELECT TOP(100) PERCENT T1.[Perfil_Codigo], T2.[Menu_Tipo], T2.[Menu_Ativo], T2.[Menu_Ordem], T2.[Menu_Descricao], T2.[Menu_Link], T2.[Menu_Nome], T1.[Menu_Codigo] FROM ([MenuPerfil] T1 WITH (NOLOCK) INNER JOIN [Menu] T2 WITH (NOLOCK) ON T2.[Menu_Codigo] = T1.[Menu_Codigo]) WHERE (T1.[Perfil_Codigo] IN ( SELECT V2000.[Perfil_Codigo] FROM (SELECT [Perfil_Codigo], [Usuario_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV12DS_UsuarioPerfil_3_Usuario_codigo)V2000)) AND (T2.[Menu_Ativo] = 1) AND (T2.[Menu_Tipo] = 2) ORDER BY T1.[Menu_Codigo]) DistinctT ORDER BY [Menu_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00032,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 30) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
