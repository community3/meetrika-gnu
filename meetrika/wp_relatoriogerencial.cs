/*
               File: WP_RelatorioGerencial
        Description: Relat�rio Gerencial
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:24:43.54
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_relatoriogerencial : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_relatoriogerencial( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_relatoriogerencial( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContratadaorigem = new GXCombobox();
         dynavServico_codigo = new GXCombobox();
         cmbavStatusdemanda = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADAORIGEM") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADAORIGEMS52( AV10WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSERVICO_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSERVICO_CODIGOS52( AV10WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAS52( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTS52( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221244356");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_relatoriogerencial.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_boolean_hidden_field( context, "vCHECKREQUIREDFIELDSRESULT", AV15CheckRequiredFieldsResult);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV10WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV10WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Width", StringUtil.RTrim( Dvpanel_unnamedtable1_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Cls", StringUtil.RTrim( Dvpanel_unnamedtable1_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Title", StringUtil.RTrim( Dvpanel_unnamedtable1_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Collapsible", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Collapsed", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Autowidth", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Autoheight", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Iconposition", StringUtil.RTrim( Dvpanel_unnamedtable1_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Autoscroll", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WES52( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTS52( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_relatoriogerencial.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_RelatorioGerencial" ;
      }

      public override String GetPgmdesc( )
      {
         return "Relat�rio Gerencial" ;
      }

      protected void WBS50( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_S52( true) ;
         }
         else
         {
            wb_table1_2_S52( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_S52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavData_contagem_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavData_contagem_Internalname, context.localUtil.Format(AV7Data_Contagem, "99/99/99"), context.localUtil.Format( AV7Data_Contagem, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavData_contagem_Jsonclick, 0, "Attribute", "", "", "", edtavData_contagem_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioGerencial.htm");
            GxWebStd.gx_bitmap( context, edtavData_contagem_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavData_contagem_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioGerencial.htm");
            context.WriteHtmlTextNl( "</div>") ;
         }
         wbLoad = true;
      }

      protected void STARTS52( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Relat�rio Gerencial", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPS50( ) ;
      }

      protected void WSS52( )
      {
         STARTS52( ) ;
         EVTS52( ) ;
      }

      protected void EVTS52( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11S52 */
                              E11S52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOBNTPDF'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12S52 */
                              E12S52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13S52 */
                              E13S52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WES52( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAS52( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContratadaorigem.Name = "vCONTRATADAORIGEM";
            dynavContratadaorigem.WebTags = "";
            dynavServico_codigo.Name = "vSERVICO_CODIGO";
            dynavServico_codigo.WebTags = "";
            cmbavStatusdemanda.Name = "vSTATUSDEMANDA";
            cmbavStatusdemanda.WebTags = "";
            cmbavStatusdemanda.addItem("", "Selecione", 0);
            cmbavStatusdemanda.addItem("B", "Stand by", 0);
            cmbavStatusdemanda.addItem("S", "Solicitada", 0);
            cmbavStatusdemanda.addItem("E", "Em An�lise", 0);
            cmbavStatusdemanda.addItem("A", "Em execu��o", 0);
            cmbavStatusdemanda.addItem("R", "Resolvida", 0);
            cmbavStatusdemanda.addItem("C", "Conferida", 0);
            cmbavStatusdemanda.addItem("D", "Rejeitada", 0);
            cmbavStatusdemanda.addItem("H", "Homologada", 0);
            cmbavStatusdemanda.addItem("O", "Aceite", 0);
            cmbavStatusdemanda.addItem("P", "A Pagar", 0);
            cmbavStatusdemanda.addItem("L", "Liquidada", 0);
            cmbavStatusdemanda.addItem("X", "Cancelada", 0);
            cmbavStatusdemanda.addItem("N", "N�o Faturada", 0);
            cmbavStatusdemanda.addItem("J", "Planejamento", 0);
            cmbavStatusdemanda.addItem("I", "An�lise Planejamento", 0);
            cmbavStatusdemanda.addItem("T", "Validacao T�cnica", 0);
            cmbavStatusdemanda.addItem("Q", "Validacao Qualidade", 0);
            cmbavStatusdemanda.addItem("G", "Em Homologa��o", 0);
            cmbavStatusdemanda.addItem("M", "Valida��o Mensura��o", 0);
            cmbavStatusdemanda.addItem("U", "Rascunho", 0);
            if ( cmbavStatusdemanda.ItemCount > 0 )
            {
               AV9StatusDemanda = cmbavStatusdemanda.getValidValue(AV9StatusDemanda);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9StatusDemanda", AV9StatusDemanda);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavContagemresultado_datadmnin_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADAORIGEMS52( wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADAORIGEM_dataS52( AV10WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADAORIGEM_htmlS52( wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADAORIGEM_dataS52( AV10WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratadaorigem.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratadaorigem.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratadaorigem.ItemCount > 0 )
         {
            AV6ContratadaOrigem = (int)(NumberUtil.Val( dynavContratadaorigem.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContratadaOrigem), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContratadaOrigem), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADAORIGEM_dataS52( wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H00S52 */
         pr_default.execute(0, new Object[] {AV10WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00S52_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00S52_A41Contratada_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvSERVICO_CODIGOS52( wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICO_CODIGO_dataS52( AV10WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICO_CODIGO_htmlS52( wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICO_CODIGO_dataS52( AV10WWPContext) ;
         gxdynajaxindex = 1;
         dynavServico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV8Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Servico_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSERVICO_CODIGO_dataS52( wwpbaseobjects.SdtWWPContext AV10WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00S53 */
         pr_default.execute(1, new Object[] {AV10WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00S53_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00S53_A1858ContratoServicos_Alias[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContratadaorigem.ItemCount > 0 )
         {
            AV6ContratadaOrigem = (int)(NumberUtil.Val( dynavContratadaorigem.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContratadaOrigem), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContratadaOrigem), 6, 0)));
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV8Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Servico_Codigo), 6, 0)));
         }
         if ( cmbavStatusdemanda.ItemCount > 0 )
         {
            AV9StatusDemanda = cmbavStatusdemanda.getValidValue(AV9StatusDemanda);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9StatusDemanda", AV9StatusDemanda);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFS52( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFS52( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E13S52 */
            E13S52 ();
            WBS50( ) ;
         }
      }

      protected void STRUPS50( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11S52 */
         E11S52 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTRATADAORIGEM_htmlS52( AV10WWPContext) ;
         GXVvSERVICO_CODIGO_htmlS52( AV10WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmnin_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn In"}), 1, "vCONTAGEMRESULTADO_DATADMNIN");
               GX_FocusControl = edtavContagemresultado_datadmnin_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13ContagemResultado_DataDmnIn = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_DataDmnIn", context.localUtil.Format(AV13ContagemResultado_DataDmnIn, "99/99/9999"));
            }
            else
            {
               AV13ContagemResultado_DataDmnIn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmnin_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_DataDmnIn", context.localUtil.Format(AV13ContagemResultado_DataDmnIn, "99/99/9999"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmnfim_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn Fim"}), 1, "vCONTAGEMRESULTADO_DATADMNFIM");
               GX_FocusControl = edtavContagemresultado_datadmnfim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14ContagemResultado_DataDmnFim = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_DataDmnFim", context.localUtil.Format(AV14ContagemResultado_DataDmnFim, "99/99/9999"));
            }
            else
            {
               AV14ContagemResultado_DataDmnFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmnfim_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_DataDmnFim", context.localUtil.Format(AV14ContagemResultado_DataDmnFim, "99/99/9999"));
            }
            dynavContratadaorigem.CurrentValue = cgiGet( dynavContratadaorigem_Internalname);
            AV6ContratadaOrigem = (int)(NumberUtil.Val( cgiGet( dynavContratadaorigem_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContratadaOrigem), 6, 0)));
            dynavServico_codigo.CurrentValue = cgiGet( dynavServico_codigo_Internalname);
            AV8Servico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Servico_Codigo), 6, 0)));
            cmbavStatusdemanda.CurrentValue = cgiGet( cmbavStatusdemanda_Internalname);
            AV9StatusDemanda = cgiGet( cmbavStatusdemanda_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9StatusDemanda", AV9StatusDemanda);
            if ( context.localUtil.VCDateTime( cgiGet( edtavData_contagem_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data_Contagem"}), 1, "vDATA_CONTAGEM");
               GX_FocusControl = edtavData_contagem_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7Data_Contagem = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Data_Contagem", context.localUtil.Format(AV7Data_Contagem, "99/99/99"));
            }
            else
            {
               AV7Data_Contagem = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavData_contagem_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Data_Contagem", context.localUtil.Format(AV7Data_Contagem, "99/99/99"));
            }
            /* Read saved values. */
            Dvpanel_unnamedtable1_Width = cgiGet( "DVPANEL_UNNAMEDTABLE1_Width");
            Dvpanel_unnamedtable1_Cls = cgiGet( "DVPANEL_UNNAMEDTABLE1_Cls");
            Dvpanel_unnamedtable1_Title = cgiGet( "DVPANEL_UNNAMEDTABLE1_Title");
            Dvpanel_unnamedtable1_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Collapsible"));
            Dvpanel_unnamedtable1_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Collapsed"));
            Dvpanel_unnamedtable1_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Autowidth"));
            Dvpanel_unnamedtable1_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Autoheight"));
            Dvpanel_unnamedtable1_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Showcollapseicon"));
            Dvpanel_unnamedtable1_Iconposition = cgiGet( "DVPANEL_UNNAMEDTABLE1_Iconposition");
            Dvpanel_unnamedtable1_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Autoscroll"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvCONTRATADAORIGEM_htmlS52( AV10WWPContext) ;
            GXVvSERVICO_CODIGO_htmlS52( AV10WWPContext) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11S52 */
         E11S52 ();
         if (returnInSub) return;
      }

      protected void E11S52( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV10WWPContext) ;
         AV13ContagemResultado_DataDmnIn = context.localUtil.YMDToD( 2017, 1, 1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_DataDmnIn", context.localUtil.Format(AV13ContagemResultado_DataDmnIn, "99/99/9999"));
         AV14ContagemResultado_DataDmnFim = context.localUtil.YMDToD( 2019, 12, 1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_DataDmnFim", context.localUtil.Format(AV14ContagemResultado_DataDmnFim, "99/99/9999"));
         edtavData_contagem_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavData_contagem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavData_contagem_Visible), 5, 0)));
         bttBtnbntexcel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnbntexcel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnbntexcel_Visible), 5, 0)));
         bttBtnbntword_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnbntword_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnbntword_Visible), 5, 0)));
      }

      protected void E12S52( )
      {
         /* 'DoBntPDF' Routine */
         /* Execute user subroutine: 'CHECKREQUIREDFIELDS' */
         S112 ();
         if (returnInSub) return;
         if ( AV15CheckRequiredFieldsResult )
         {
            context.wjLoc = formatLink("arel_relatoriogerencialcontagempdf.aspx") + "?" + UrlEncode(DateTimeUtil.FormatDateParm(AV13ContagemResultado_DataDmnIn)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV14ContagemResultado_DataDmnFim)) + "," + UrlEncode("" +AV6ContratadaOrigem) + "," + UrlEncode("" +AV8Servico_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV9StatusDemanda));
            context.wjLocDisableFrm = 0;
         }
      }

      protected void S112( )
      {
         /* 'CHECKREQUIREDFIELDS' Routine */
         AV15CheckRequiredFieldsResult = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15CheckRequiredFieldsResult", AV15CheckRequiredFieldsResult);
         if ( ! (DateTime.MinValue==AV13ContagemResultado_DataDmnIn) && ! (DateTime.MinValue==AV14ContagemResultado_DataDmnFim) )
         {
            if ( AV14ContagemResultado_DataDmnFim < AV13ContagemResultado_DataDmnIn )
            {
               GX_msglist.addItem("O per�odo informado para a Data da Demanda n�o � v�lido. Verifique!");
               AV15CheckRequiredFieldsResult = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15CheckRequiredFieldsResult", AV15CheckRequiredFieldsResult);
            }
         }
         else
         {
            GX_msglist.addItem("Data da Demanda � obrigat�rio. Verifique!");
            AV15CheckRequiredFieldsResult = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15CheckRequiredFieldsResult", AV15CheckRequiredFieldsResult);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E13S52( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_S52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_S52( true) ;
         }
         else
         {
            wb_table2_8_S52( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_S52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_S52e( true) ;
         }
         else
         {
            wb_table1_2_S52e( false) ;
         }
      }

      protected void wb_table2_8_S52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_UNNAMEDTABLE1Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_UNNAMEDTABLE1Container"+"Body"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayout_unnamedtable1_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtable1_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table3_18_S52( true) ;
         }
         else
         {
            wb_table3_18_S52( false) ;
         }
         return  ;
      }

      protected void wb_table3_18_S52e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 DataContent", "left", "top", "", "", "div");
            wb_table4_65_S52( true) ;
         }
         else
         {
            wb_table4_65_S52( false) ;
         }
         return  ;
      }

      protected void wb_table4_65_S52e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_S52e( true) ;
         }
         else
         {
            wb_table2_8_S52e( false) ;
         }
      }

      protected void wb_table4_65_S52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableaction_Internalname, tblTableaction_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "ActionButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbntpdf_Internalname, "", "PDF", bttBtnbntpdf_Jsonclick, 5, "PDF", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOBNTPDF\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_RelatorioGerencial.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            ClassString = "ActionButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbntexcel_Internalname, "", "Excel", bttBtnbntexcel_Jsonclick, 7, "Excel", "", StyleString, ClassString, bttBtnbntexcel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"e14s51_client"+"'", TempTags, "", 2, "HLP_WP_RelatorioGerencial.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "ActionButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbntword_Internalname, "", "Word", bttBtnbntword_Jsonclick, 7, "Word", "", StyleString, ClassString, bttBtnbntword_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"e15s51_client"+"'", TempTags, "", 2, "HLP_WP_RelatorioGerencial.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_65_S52e( true) ;
         }
         else
         {
            wb_table4_65_S52e( false) ;
         }
      }

      protected void wb_table3_18_S52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divTablesplittedcontagemresultado_datadmnin_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-4 MergeLabelCell", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_datadmnin_Internalname, "Data da Demanda", "", "", lblTextblockcontagemresultado_datadmnin_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_RelatorioGerencial.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8", "left", "top", "", "", "div");
            wb_table5_26_S52( true) ;
         }
         else
         {
            wb_table5_26_S52( false) ;
         }
         return  ;
      }

      protected void wb_table5_26_S52e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtablecontratadaorigem_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-4 MergeLabelCell", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratadaorigem_Internalname, "Origem", "", "", lblTextblockcontratadaorigem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_RelatorioGerencial.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, dynavContratadaorigem_Internalname, "Contratada Origem", "col-sm-3 BootstrapAttributeLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratadaorigem, dynavContratadaorigem_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContratadaOrigem), 6, 0)), 1, dynavContratadaorigem_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavContratadaorigem.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_WP_RelatorioGerencial.htm");
            dynavContratadaorigem.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContratadaOrigem), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Values", (String)(dynavContratadaorigem.ToJavascriptSource()));
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtableservico_codigo_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-4 MergeLabelCell", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_codigo_Internalname, "Servi�o (Alias)", "", "", lblTextblockservico_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_RelatorioGerencial.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, dynavServico_codigo_Internalname, "Servico_Codigo", "col-sm-3 BootstrapAttributeLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServico_codigo, dynavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV8Servico_Codigo), 6, 0)), 1, dynavServico_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavServico_codigo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_WP_RelatorioGerencial.htm");
            dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_codigo_Internalname, "Values", (String)(dynavServico_codigo.ToJavascriptSource()));
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divUnnamedtablestatusdemanda_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-4 MergeLabelCell", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockstatusdemanda_Internalname, "Situa��o", "", "", lblTextblockstatusdemanda_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_RelatorioGerencial.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavStatusdemanda_Internalname, "Status Demanda", "col-sm-3 BootstrapAttributeLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavStatusdemanda, cmbavStatusdemanda_Internalname, StringUtil.RTrim( AV9StatusDemanda), 1, cmbavStatusdemanda_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavStatusdemanda.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_WP_RelatorioGerencial.htm");
            cmbavStatusdemanda.CurrentValue = StringUtil.RTrim( AV9StatusDemanda);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusdemanda_Internalname, "Values", (String)(cmbavStatusdemanda.ToJavascriptSource()));
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_18_S52e( true) ;
         }
         else
         {
            wb_table3_18_S52e( false) ;
         }
      }

      protected void wb_table5_26_S52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_datadmnin_Internalname, tblTablemergedcontagemresultado_datadmnin_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='MergeDataCell'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavContagemresultado_datadmnin_Internalname, "Contagem Resultado_Data Dmn In", "col-sm-3 BootstrapAttributeDateLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmnin_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmnin_Internalname, context.localUtil.Format(AV13ContagemResultado_DataDmnIn, "99/99/9999"), context.localUtil.Format( AV13ContagemResultado_DataDmnIn, "99/99/9999"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmnin_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtavContagemresultado_datadmnin_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioGerencial.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmnin_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_datadmnin_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioGerencial.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTxta_Internalname, " �", "", "", lblTxta_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_RelatorioGerencial.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavContagemresultado_datadmnfim_Internalname, "Contagem Resultado_Data Dmn Fim", "col-sm-3 BootstrapAttributeDateLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmnfim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmnfim_Internalname, context.localUtil.Format(AV14ContagemResultado_DataDmnFim, "99/99/9999"), context.localUtil.Format( AV14ContagemResultado_DataDmnFim, "99/99/9999"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmnfim_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtavContagemresultado_datadmnfim_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioGerencial.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmnfim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_datadmnfim_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RelatorioGerencial.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_26_S52e( true) ;
         }
         else
         {
            wb_table5_26_S52e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAS52( ) ;
         WSS52( ) ;
         WES52( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221244422");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_relatoriogerencial.js", "?202031221244422");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultado_datadmnin_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DATADMNIN";
         div_Internalname = "";
         edtavContagemresultado_datadmnin_Internalname = "vCONTAGEMRESULTADO_DATADMNIN";
         div_Internalname = "";
         lblTxta_Internalname = "TXTA";
         edtavContagemresultado_datadmnfim_Internalname = "vCONTAGEMRESULTADO_DATADMNFIM";
         div_Internalname = "";
         tblTablemergedcontagemresultado_datadmnin_Internalname = "TABLEMERGEDCONTAGEMRESULTADO_DATADMNIN";
         div_Internalname = "";
         div_Internalname = "";
         divTablesplittedcontagemresultado_datadmnin_Internalname = "TABLESPLITTEDCONTAGEMRESULTADO_DATADMNIN";
         lblTextblockcontratadaorigem_Internalname = "TEXTBLOCKCONTRATADAORIGEM";
         div_Internalname = "";
         dynavContratadaorigem_Internalname = "vCONTRATADAORIGEM";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtablecontratadaorigem_Internalname = "UNNAMEDTABLECONTRATADAORIGEM";
         lblTextblockservico_codigo_Internalname = "TEXTBLOCKSERVICO_CODIGO";
         div_Internalname = "";
         dynavServico_codigo_Internalname = "vSERVICO_CODIGO";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtableservico_codigo_Internalname = "UNNAMEDTABLESERVICO_CODIGO";
         lblTextblockstatusdemanda_Internalname = "TEXTBLOCKSTATUSDEMANDA";
         div_Internalname = "";
         cmbavStatusdemanda_Internalname = "vSTATUSDEMANDA";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtablestatusdemanda_Internalname = "UNNAMEDTABLESTATUSDEMANDA";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         div_Internalname = "";
         div_Internalname = "";
         bttBtnbntpdf_Internalname = "BTNBNTPDF";
         bttBtnbntexcel_Internalname = "BTNBNTEXCEL";
         bttBtnbntword_Internalname = "BTNBNTWORD";
         tblTableaction_Internalname = "TABLEACTION";
         div_Internalname = "";
         div_Internalname = "";
         divUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         div_Internalname = "";
         divLayout_unnamedtable1_Internalname = "LAYOUT_UNNAMEDTABLE1";
         Dvpanel_unnamedtable1_Internalname = "DVPANEL_UNNAMEDTABLE1";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavData_contagem_Internalname = "vDATA_CONTAGEM";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavContagemresultado_datadmnfim_Jsonclick = "";
         edtavContagemresultado_datadmnfim_Enabled = 1;
         edtavContagemresultado_datadmnin_Jsonclick = "";
         edtavContagemresultado_datadmnin_Enabled = 1;
         cmbavStatusdemanda_Jsonclick = "";
         cmbavStatusdemanda.Enabled = 1;
         dynavServico_codigo_Jsonclick = "";
         dynavServico_codigo.Enabled = 1;
         dynavContratadaorigem_Jsonclick = "";
         dynavContratadaorigem.Enabled = 1;
         bttBtnbntword_Visible = 1;
         bttBtnbntexcel_Visible = 1;
         edtavData_contagem_Jsonclick = "";
         edtavData_contagem_Visible = 1;
         Dvpanel_unnamedtable1_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Iconposition = "left";
         Dvpanel_unnamedtable1_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_unnamedtable1_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_unnamedtable1_Title = "Relat�rio Gerencial";
         Dvpanel_unnamedtable1_Cls = "GXUI-DVelop-Panel";
         Dvpanel_unnamedtable1_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Relat�rio Gerencial";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOBNTPDF'","{handler:'E12S52',iparms:[{av:'AV15CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV13ContagemResultado_DataDmnIn',fld:'vCONTAGEMRESULTADO_DATADMNIN',pic:'',nv:''},{av:'AV14ContagemResultado_DataDmnFim',fld:'vCONTAGEMRESULTADO_DATADMNFIM',pic:'',nv:''},{av:'AV6ContratadaOrigem',fld:'vCONTRATADAORIGEM',pic:'ZZZZZ9',nv:0},{av:'AV8Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9StatusDemanda',fld:'vSTATUSDEMANDA',pic:'',nv:''}],oparms:[{av:'AV15CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false}]}");
         setEventMetadata("'DOBNTEXCEL'","{handler:'E14S51',iparms:[],oparms:[]}");
         setEventMetadata("'DOBNTWORD'","{handler:'E15S51',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV10WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         AV7Data_Contagem = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9StatusDemanda = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00S52_A40Contratada_PessoaCod = new int[1] ;
         H00S52_A39Contratada_Codigo = new int[1] ;
         H00S52_A41Contratada_PessoaNom = new String[] {""} ;
         H00S52_n41Contratada_PessoaNom = new bool[] {false} ;
         H00S52_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00S52_A43Contratada_Ativo = new bool[] {false} ;
         H00S53_A74Contrato_Codigo = new int[1] ;
         H00S53_A160ContratoServicos_Codigo = new int[1] ;
         H00S53_A155Servico_Codigo = new int[1] ;
         H00S53_A1858ContratoServicos_Alias = new String[] {""} ;
         H00S53_n1858ContratoServicos_Alias = new bool[] {false} ;
         H00S53_A75Contrato_AreaTrabalhoCod = new int[1] ;
         AV13ContagemResultado_DataDmnIn = DateTime.MinValue;
         AV14ContagemResultado_DataDmnFim = DateTime.MinValue;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtnbntpdf_Jsonclick = "";
         bttBtnbntexcel_Jsonclick = "";
         bttBtnbntword_Jsonclick = "";
         lblTextblockcontagemresultado_datadmnin_Jsonclick = "";
         lblTextblockcontratadaorigem_Jsonclick = "";
         lblTextblockservico_codigo_Jsonclick = "";
         lblTextblockstatusdemanda_Jsonclick = "";
         lblTxta_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_relatoriogerencial__default(),
            new Object[][] {
                new Object[] {
               H00S52_A40Contratada_PessoaCod, H00S52_A39Contratada_Codigo, H00S52_A41Contratada_PessoaNom, H00S52_n41Contratada_PessoaNom, H00S52_A52Contratada_AreaTrabalhoCod, H00S52_A43Contratada_Ativo
               }
               , new Object[] {
               H00S53_A74Contrato_Codigo, H00S53_A160ContratoServicos_Codigo, H00S53_A155Servico_Codigo, H00S53_A1858ContratoServicos_Alias, H00S53_n1858ContratoServicos_Alias, H00S53_A75Contrato_AreaTrabalhoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int edtavData_contagem_Visible ;
      private int gxdynajaxindex ;
      private int AV6ContratadaOrigem ;
      private int AV8Servico_Codigo ;
      private int bttBtnbntexcel_Visible ;
      private int bttBtnbntword_Visible ;
      private int edtavContagemresultado_datadmnin_Enabled ;
      private int edtavContagemresultado_datadmnfim_Enabled ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_unnamedtable1_Width ;
      private String Dvpanel_unnamedtable1_Cls ;
      private String Dvpanel_unnamedtable1_Title ;
      private String Dvpanel_unnamedtable1_Iconposition ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavData_contagem_Internalname ;
      private String edtavData_contagem_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV9StatusDemanda ;
      private String edtavContagemresultado_datadmnin_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavContagemresultado_datadmnfim_Internalname ;
      private String dynavContratadaorigem_Internalname ;
      private String dynavServico_codigo_Internalname ;
      private String cmbavStatusdemanda_Internalname ;
      private String bttBtnbntexcel_Internalname ;
      private String bttBtnbntword_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablecontent_Internalname ;
      private String divLayout_unnamedtable1_Internalname ;
      private String divUnnamedtable1_Internalname ;
      private String tblTableaction_Internalname ;
      private String bttBtnbntpdf_Internalname ;
      private String bttBtnbntpdf_Jsonclick ;
      private String bttBtnbntexcel_Jsonclick ;
      private String bttBtnbntword_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String divTablesplittedcontagemresultado_datadmnin_Internalname ;
      private String lblTextblockcontagemresultado_datadmnin_Internalname ;
      private String lblTextblockcontagemresultado_datadmnin_Jsonclick ;
      private String divUnnamedtablecontratadaorigem_Internalname ;
      private String lblTextblockcontratadaorigem_Internalname ;
      private String lblTextblockcontratadaorigem_Jsonclick ;
      private String dynavContratadaorigem_Jsonclick ;
      private String divUnnamedtableservico_codigo_Internalname ;
      private String lblTextblockservico_codigo_Internalname ;
      private String lblTextblockservico_codigo_Jsonclick ;
      private String dynavServico_codigo_Jsonclick ;
      private String divUnnamedtablestatusdemanda_Internalname ;
      private String lblTextblockstatusdemanda_Internalname ;
      private String lblTextblockstatusdemanda_Jsonclick ;
      private String cmbavStatusdemanda_Jsonclick ;
      private String tblTablemergedcontagemresultado_datadmnin_Internalname ;
      private String edtavContagemresultado_datadmnin_Jsonclick ;
      private String lblTxta_Internalname ;
      private String lblTxta_Jsonclick ;
      private String edtavContagemresultado_datadmnfim_Jsonclick ;
      private String div_Internalname ;
      private String Dvpanel_unnamedtable1_Internalname ;
      private DateTime AV7Data_Contagem ;
      private DateTime AV13ContagemResultado_DataDmnIn ;
      private DateTime AV14ContagemResultado_DataDmnFim ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV15CheckRequiredFieldsResult ;
      private bool Dvpanel_unnamedtable1_Collapsible ;
      private bool Dvpanel_unnamedtable1_Collapsed ;
      private bool Dvpanel_unnamedtable1_Autowidth ;
      private bool Dvpanel_unnamedtable1_Autoheight ;
      private bool Dvpanel_unnamedtable1_Showcollapseicon ;
      private bool Dvpanel_unnamedtable1_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavContratadaorigem ;
      private GXCombobox dynavServico_codigo ;
      private GXCombobox cmbavStatusdemanda ;
      private IDataStoreProvider pr_default ;
      private int[] H00S52_A40Contratada_PessoaCod ;
      private int[] H00S52_A39Contratada_Codigo ;
      private String[] H00S52_A41Contratada_PessoaNom ;
      private bool[] H00S52_n41Contratada_PessoaNom ;
      private int[] H00S52_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00S52_A43Contratada_Ativo ;
      private int[] H00S53_A74Contrato_Codigo ;
      private int[] H00S53_A160ContratoServicos_Codigo ;
      private int[] H00S53_A155Servico_Codigo ;
      private String[] H00S53_A1858ContratoServicos_Alias ;
      private bool[] H00S53_n1858ContratoServicos_Alias ;
      private int[] H00S53_A75Contrato_AreaTrabalhoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV10WWPContext ;
   }

   public class wp_relatoriogerencial__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00S52 ;
          prmH00S52 = new Object[] {
          new Object[] {"@AV10WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00S53 ;
          prmH00S53 = new Object[] {
          new Object[] {"@AV10WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00S52", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod], T1.[Contratada_Ativo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE (T1.[Contratada_Ativo] = 1) AND (T1.[Contratada_AreaTrabalhoCod] = @AV10WWPC_1Areatrabalho_codigo) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00S52,0,0,true,false )
             ,new CursorDef("H00S53", "SELECT T2.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[Servico_Codigo], T1.[ContratoServicos_Alias], T2.[Contrato_AreaTrabalhoCod] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE T2.[Contrato_AreaTrabalhoCod] = @AV10WWPC_1Areatrabalho_codigo ORDER BY T1.[ContratoServicos_Alias] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00S53,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
