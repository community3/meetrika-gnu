/*
               File: GetPromptContratoObrigacaoFilterData
        Description: Get Prompt Contrato Obrigacao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:52.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratoobrigacaofilterdata : GXProcedure
   {
      public getpromptcontratoobrigacaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratoobrigacaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratoobrigacaofilterdata objgetpromptcontratoobrigacaofilterdata;
         objgetpromptcontratoobrigacaofilterdata = new getpromptcontratoobrigacaofilterdata();
         objgetpromptcontratoobrigacaofilterdata.AV16DDOName = aP0_DDOName;
         objgetpromptcontratoobrigacaofilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetpromptcontratoobrigacaofilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratoobrigacaofilterdata.AV20OptionsJson = "" ;
         objgetpromptcontratoobrigacaofilterdata.AV23OptionsDescJson = "" ;
         objgetpromptcontratoobrigacaofilterdata.AV25OptionIndexesJson = "" ;
         objgetpromptcontratoobrigacaofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratoobrigacaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratoobrigacaofilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratoobrigacaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_CONTRATOOBRIGACAO_ITEM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOBRIGACAO_ITEMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("PromptContratoObrigacaoGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoObrigacaoGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("PromptContratoObrigacaoGridState"), "");
         }
         AV45GXV1 = 1;
         while ( AV45GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV45GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATOOBRIGACAO_ITEM") == 0 )
            {
               AV12TFContratoObrigacao_Item = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATOOBRIGACAO_ITEM_SEL") == 0 )
            {
               AV13TFContratoObrigacao_Item_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            AV45GXV1 = (int)(AV45GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "CONTRATOOBRIGACAO_ITEM") == 0 )
            {
               AV33DynamicFiltersOperator1 = AV31GridStateDynamicFilter.gxTpr_Operator;
               AV34ContratoObrigacao_Item1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV35DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV36DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "CONTRATOOBRIGACAO_ITEM") == 0 )
               {
                  AV37DynamicFiltersOperator2 = AV31GridStateDynamicFilter.gxTpr_Operator;
                  AV38ContratoObrigacao_Item2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV39DynamicFiltersEnabled3 = true;
                  AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(3));
                  AV40DynamicFiltersSelector3 = AV31GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTRATOOBRIGACAO_ITEM") == 0 )
                  {
                     AV41DynamicFiltersOperator3 = AV31GridStateDynamicFilter.gxTpr_Operator;
                     AV42ContratoObrigacao_Item3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV14SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV32DynamicFiltersSelector1 ,
                                              AV33DynamicFiltersOperator1 ,
                                              AV34ContratoObrigacao_Item1 ,
                                              AV35DynamicFiltersEnabled2 ,
                                              AV36DynamicFiltersSelector2 ,
                                              AV37DynamicFiltersOperator2 ,
                                              AV38ContratoObrigacao_Item2 ,
                                              AV39DynamicFiltersEnabled3 ,
                                              AV40DynamicFiltersSelector3 ,
                                              AV41DynamicFiltersOperator3 ,
                                              AV42ContratoObrigacao_Item3 ,
                                              AV11TFContrato_Numero_Sel ,
                                              AV10TFContrato_Numero ,
                                              AV13TFContratoObrigacao_Item_Sel ,
                                              AV12TFContratoObrigacao_Item ,
                                              A322ContratoObrigacao_Item ,
                                              A77Contrato_Numero },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV10TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFContrato_Numero), 20, "%");
         lV12TFContratoObrigacao_Item = StringUtil.Concat( StringUtil.RTrim( AV12TFContratoObrigacao_Item), "%", "");
         /* Using cursor P00KQ2 */
         pr_default.execute(0, new Object[] {AV34ContratoObrigacao_Item1, AV34ContratoObrigacao_Item1, AV34ContratoObrigacao_Item1, AV38ContratoObrigacao_Item2, AV38ContratoObrigacao_Item2, AV38ContratoObrigacao_Item2, AV42ContratoObrigacao_Item3, AV42ContratoObrigacao_Item3, AV42ContratoObrigacao_Item3, lV10TFContrato_Numero, AV11TFContrato_Numero_Sel, lV12TFContratoObrigacao_Item, AV13TFContratoObrigacao_Item_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKKQ2 = false;
            A74Contrato_Codigo = P00KQ2_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00KQ2_A77Contrato_Numero[0];
            A322ContratoObrigacao_Item = P00KQ2_A322ContratoObrigacao_Item[0];
            A321ContratoObrigacao_Codigo = P00KQ2_A321ContratoObrigacao_Codigo[0];
            A77Contrato_Numero = P00KQ2_A77Contrato_Numero[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00KQ2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKKQ2 = false;
               A74Contrato_Codigo = P00KQ2_A74Contrato_Codigo[0];
               A321ContratoObrigacao_Codigo = P00KQ2_A321ContratoObrigacao_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKKQ2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV18Option = A77Contrato_Numero;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKQ2 )
            {
               BRKKQ2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOOBRIGACAO_ITEMOPTIONS' Routine */
         AV12TFContratoObrigacao_Item = AV14SearchTxt;
         AV13TFContratoObrigacao_Item_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV32DynamicFiltersSelector1 ,
                                              AV33DynamicFiltersOperator1 ,
                                              AV34ContratoObrigacao_Item1 ,
                                              AV35DynamicFiltersEnabled2 ,
                                              AV36DynamicFiltersSelector2 ,
                                              AV37DynamicFiltersOperator2 ,
                                              AV38ContratoObrigacao_Item2 ,
                                              AV39DynamicFiltersEnabled3 ,
                                              AV40DynamicFiltersSelector3 ,
                                              AV41DynamicFiltersOperator3 ,
                                              AV42ContratoObrigacao_Item3 ,
                                              AV11TFContrato_Numero_Sel ,
                                              AV10TFContrato_Numero ,
                                              AV13TFContratoObrigacao_Item_Sel ,
                                              AV12TFContratoObrigacao_Item ,
                                              A322ContratoObrigacao_Item ,
                                              A77Contrato_Numero },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV10TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFContrato_Numero), 20, "%");
         lV12TFContratoObrigacao_Item = StringUtil.Concat( StringUtil.RTrim( AV12TFContratoObrigacao_Item), "%", "");
         /* Using cursor P00KQ3 */
         pr_default.execute(1, new Object[] {AV34ContratoObrigacao_Item1, AV34ContratoObrigacao_Item1, AV34ContratoObrigacao_Item1, AV38ContratoObrigacao_Item2, AV38ContratoObrigacao_Item2, AV38ContratoObrigacao_Item2, AV42ContratoObrigacao_Item3, AV42ContratoObrigacao_Item3, AV42ContratoObrigacao_Item3, lV10TFContrato_Numero, AV11TFContrato_Numero_Sel, lV12TFContratoObrigacao_Item, AV13TFContratoObrigacao_Item_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKKQ4 = false;
            A74Contrato_Codigo = P00KQ3_A74Contrato_Codigo[0];
            A322ContratoObrigacao_Item = P00KQ3_A322ContratoObrigacao_Item[0];
            A77Contrato_Numero = P00KQ3_A77Contrato_Numero[0];
            A321ContratoObrigacao_Codigo = P00KQ3_A321ContratoObrigacao_Codigo[0];
            A77Contrato_Numero = P00KQ3_A77Contrato_Numero[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00KQ3_A322ContratoObrigacao_Item[0], A322ContratoObrigacao_Item) == 0 ) )
            {
               BRKKQ4 = false;
               A321ContratoObrigacao_Codigo = P00KQ3_A321ContratoObrigacao_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKKQ4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A322ContratoObrigacao_Item)) )
            {
               AV18Option = A322ContratoObrigacao_Item;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKQ4 )
            {
               BRKKQ4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContratoObrigacao_Item = "";
         AV13TFContratoObrigacao_Item_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV34ContratoObrigacao_Item1 = "";
         AV36DynamicFiltersSelector2 = "";
         AV38ContratoObrigacao_Item2 = "";
         AV40DynamicFiltersSelector3 = "";
         AV42ContratoObrigacao_Item3 = "";
         scmdbuf = "";
         lV10TFContrato_Numero = "";
         lV12TFContratoObrigacao_Item = "";
         A322ContratoObrigacao_Item = "";
         A77Contrato_Numero = "";
         P00KQ2_A74Contrato_Codigo = new int[1] ;
         P00KQ2_A77Contrato_Numero = new String[] {""} ;
         P00KQ2_A322ContratoObrigacao_Item = new String[] {""} ;
         P00KQ2_A321ContratoObrigacao_Codigo = new int[1] ;
         AV18Option = "";
         P00KQ3_A74Contrato_Codigo = new int[1] ;
         P00KQ3_A322ContratoObrigacao_Item = new String[] {""} ;
         P00KQ3_A77Contrato_Numero = new String[] {""} ;
         P00KQ3_A321ContratoObrigacao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratoobrigacaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00KQ2_A74Contrato_Codigo, P00KQ2_A77Contrato_Numero, P00KQ2_A322ContratoObrigacao_Item, P00KQ2_A321ContratoObrigacao_Codigo
               }
               , new Object[] {
               P00KQ3_A74Contrato_Codigo, P00KQ3_A322ContratoObrigacao_Item, P00KQ3_A77Contrato_Numero, P00KQ3_A321ContratoObrigacao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV33DynamicFiltersOperator1 ;
      private short AV37DynamicFiltersOperator2 ;
      private short AV41DynamicFiltersOperator3 ;
      private int AV45GXV1 ;
      private int A74Contrato_Codigo ;
      private int A321ContratoObrigacao_Codigo ;
      private long AV26count ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String scmdbuf ;
      private String lV10TFContrato_Numero ;
      private String A77Contrato_Numero ;
      private bool returnInSub ;
      private bool AV35DynamicFiltersEnabled2 ;
      private bool AV39DynamicFiltersEnabled3 ;
      private bool BRKKQ2 ;
      private bool BRKKQ4 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV12TFContratoObrigacao_Item ;
      private String AV13TFContratoObrigacao_Item_Sel ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV34ContratoObrigacao_Item1 ;
      private String AV36DynamicFiltersSelector2 ;
      private String AV38ContratoObrigacao_Item2 ;
      private String AV40DynamicFiltersSelector3 ;
      private String AV42ContratoObrigacao_Item3 ;
      private String lV12TFContratoObrigacao_Item ;
      private String A322ContratoObrigacao_Item ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00KQ2_A74Contrato_Codigo ;
      private String[] P00KQ2_A77Contrato_Numero ;
      private String[] P00KQ2_A322ContratoObrigacao_Item ;
      private int[] P00KQ2_A321ContratoObrigacao_Codigo ;
      private int[] P00KQ3_A74Contrato_Codigo ;
      private String[] P00KQ3_A322ContratoObrigacao_Item ;
      private String[] P00KQ3_A77Contrato_Numero ;
      private int[] P00KQ3_A321ContratoObrigacao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getpromptcontratoobrigacaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00KQ2( IGxContext context ,
                                             String AV32DynamicFiltersSelector1 ,
                                             short AV33DynamicFiltersOperator1 ,
                                             String AV34ContratoObrigacao_Item1 ,
                                             bool AV35DynamicFiltersEnabled2 ,
                                             String AV36DynamicFiltersSelector2 ,
                                             short AV37DynamicFiltersOperator2 ,
                                             String AV38ContratoObrigacao_Item2 ,
                                             bool AV39DynamicFiltersEnabled3 ,
                                             String AV40DynamicFiltersSelector3 ,
                                             short AV41DynamicFiltersOperator3 ,
                                             String AV42ContratoObrigacao_Item3 ,
                                             String AV11TFContrato_Numero_Sel ,
                                             String AV10TFContrato_Numero ,
                                             String AV13TFContratoObrigacao_Item_Sel ,
                                             String AV12TFContratoObrigacao_Item ,
                                             String A322ContratoObrigacao_Item ,
                                             String A77Contrato_Numero )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [13] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contrato_Numero], T1.[ContratoObrigacao_Item], T1.[ContratoObrigacao_Codigo] FROM ([ContratoObrigacao] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV33DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContratoObrigacao_Item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] < @AV34ContratoObrigacao_Item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] < @AV34ContratoObrigacao_Item1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV33DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContratoObrigacao_Item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV34ContratoObrigacao_Item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV34ContratoObrigacao_Item1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV33DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContratoObrigacao_Item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] > @AV34ContratoObrigacao_Item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] > @AV34ContratoObrigacao_Item1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV35DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV37DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratoObrigacao_Item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] < @AV38ContratoObrigacao_Item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] < @AV38ContratoObrigacao_Item2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV35DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV37DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratoObrigacao_Item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV38ContratoObrigacao_Item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV38ContratoObrigacao_Item2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV35DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV37DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratoObrigacao_Item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] > @AV38ContratoObrigacao_Item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] > @AV38ContratoObrigacao_Item2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV41DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContratoObrigacao_Item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] < @AV42ContratoObrigacao_Item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] < @AV42ContratoObrigacao_Item3)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV39DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV41DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContratoObrigacao_Item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV42ContratoObrigacao_Item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV42ContratoObrigacao_Item3)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV39DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV41DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContratoObrigacao_Item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] > @AV42ContratoObrigacao_Item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] > @AV42ContratoObrigacao_Item3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoObrigacao_Item_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoObrigacao_Item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] like @lV12TFContratoObrigacao_Item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] like @lV12TFContratoObrigacao_Item)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoObrigacao_Item_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV13TFContratoObrigacao_Item_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV13TFContratoObrigacao_Item_Sel)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00KQ3( IGxContext context ,
                                             String AV32DynamicFiltersSelector1 ,
                                             short AV33DynamicFiltersOperator1 ,
                                             String AV34ContratoObrigacao_Item1 ,
                                             bool AV35DynamicFiltersEnabled2 ,
                                             String AV36DynamicFiltersSelector2 ,
                                             short AV37DynamicFiltersOperator2 ,
                                             String AV38ContratoObrigacao_Item2 ,
                                             bool AV39DynamicFiltersEnabled3 ,
                                             String AV40DynamicFiltersSelector3 ,
                                             short AV41DynamicFiltersOperator3 ,
                                             String AV42ContratoObrigacao_Item3 ,
                                             String AV11TFContrato_Numero_Sel ,
                                             String AV10TFContrato_Numero ,
                                             String AV13TFContratoObrigacao_Item_Sel ,
                                             String AV12TFContratoObrigacao_Item ,
                                             String A322ContratoObrigacao_Item ,
                                             String A77Contrato_Numero )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[ContratoObrigacao_Item], T2.[Contrato_Numero], T1.[ContratoObrigacao_Codigo] FROM ([ContratoObrigacao] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV33DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContratoObrigacao_Item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] < @AV34ContratoObrigacao_Item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] < @AV34ContratoObrigacao_Item1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV33DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContratoObrigacao_Item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV34ContratoObrigacao_Item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV34ContratoObrigacao_Item1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV33DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContratoObrigacao_Item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] > @AV34ContratoObrigacao_Item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] > @AV34ContratoObrigacao_Item1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV35DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV37DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratoObrigacao_Item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] < @AV38ContratoObrigacao_Item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] < @AV38ContratoObrigacao_Item2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV35DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV37DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratoObrigacao_Item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV38ContratoObrigacao_Item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV38ContratoObrigacao_Item2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV35DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV37DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContratoObrigacao_Item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] > @AV38ContratoObrigacao_Item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] > @AV38ContratoObrigacao_Item2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV41DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContratoObrigacao_Item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] < @AV42ContratoObrigacao_Item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] < @AV42ContratoObrigacao_Item3)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV39DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV41DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContratoObrigacao_Item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV42ContratoObrigacao_Item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV42ContratoObrigacao_Item3)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV39DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTRATOOBRIGACAO_ITEM") == 0 ) && ( AV41DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContratoObrigacao_Item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] > @AV42ContratoObrigacao_Item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] > @AV42ContratoObrigacao_Item3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoObrigacao_Item_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoObrigacao_Item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] like @lV12TFContratoObrigacao_Item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] like @lV12TFContratoObrigacao_Item)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoObrigacao_Item_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoObrigacao_Item] = @AV13TFContratoObrigacao_Item_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoObrigacao_Item] = @AV13TFContratoObrigacao_Item_Sel)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoObrigacao_Item]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00KQ2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] );
               case 1 :
                     return conditional_P00KQ3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00KQ2 ;
          prmP00KQ2 = new Object[] {
          new Object[] {"@AV34ContratoObrigacao_Item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV34ContratoObrigacao_Item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV34ContratoObrigacao_Item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV38ContratoObrigacao_Item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV38ContratoObrigacao_Item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV38ContratoObrigacao_Item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV42ContratoObrigacao_Item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV42ContratoObrigacao_Item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV42ContratoObrigacao_Item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV10TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFContratoObrigacao_Item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV13TFContratoObrigacao_Item_Sel",SqlDbType.VarChar,40,0}
          } ;
          Object[] prmP00KQ3 ;
          prmP00KQ3 = new Object[] {
          new Object[] {"@AV34ContratoObrigacao_Item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV34ContratoObrigacao_Item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV34ContratoObrigacao_Item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV38ContratoObrigacao_Item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV38ContratoObrigacao_Item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV38ContratoObrigacao_Item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV42ContratoObrigacao_Item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV42ContratoObrigacao_Item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV42ContratoObrigacao_Item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV10TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV12TFContratoObrigacao_Item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV13TFContratoObrigacao_Item_Sel",SqlDbType.VarChar,40,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00KQ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KQ2,100,0,true,false )
             ,new CursorDef("P00KQ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KQ3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratoobrigacaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratoobrigacaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratoobrigacaofilterdata") )
          {
             return  ;
          }
          getpromptcontratoobrigacaofilterdata worker = new getpromptcontratoobrigacaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
