/*
               File: ContratoGarantia
        Description: Contrato Garantia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:17:16.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratogarantia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoGarantia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoGarantia_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOGARANTIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoGarantia_Codigo), "ZZZZZ9")));
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contrato Garantia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoGarantia_DataPagtoGarantia_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratogarantia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratogarantia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoGarantia_Codigo ,
                           ref int aP2_Contrato_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoGarantia_Codigo = aP1_ContratoGarantia_Codigo;
         this.A74Contrato_Codigo = aP2_Contrato_Codigo;
         executePrivate();
         aP2_Contrato_Codigo=this.A74Contrato_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0K21( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0K21e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0K21( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0K21( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0K21e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_75_0K21( true) ;
         }
         return  ;
      }

      protected void wb_table3_75_0K21e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0K21e( true) ;
         }
         else
         {
            wb_table1_2_0K21e( false) ;
         }
      }

      protected void wb_table3_75_0K21( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_75_0K21e( true) ;
         }
         else
         {
            wb_table3_75_0K21e( false) ;
         }
      }

      protected void wb_table2_5_0K21( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_0K21( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_0K21e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0K21e( true) ;
         }
         else
         {
            wb_table2_5_0K21e( false) ;
         }
      }

      protected void wb_table4_13_0K21( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup1_Internalname, "do Contrato", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoGarantia.htm");
            wb_table5_17_0K21( true) ;
         }
         return  ;
      }

      protected void wb_table5_17_0K21e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogarantia_datapagtogarantia_Internalname, "Data de Pagamento", "", "", lblTextblockcontratogarantia_datapagtogarantia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoGarantia_DataPagtoGarantia_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoGarantia_DataPagtoGarantia_Internalname, context.localUtil.Format(A102ContratoGarantia_DataPagtoGarantia, "99/99/99"), context.localUtil.Format( A102ContratoGarantia_DataPagtoGarantia, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGarantia_DataPagtoGarantia_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoGarantia_DataPagtoGarantia_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtContratoGarantia_DataPagtoGarantia_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoGarantia_DataPagtoGarantia_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogarantia_percentual_Internalname, "Percentual", "", "", lblTextblockcontratogarantia_percentual_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoGarantia_Percentual_Internalname, StringUtil.LTrim( StringUtil.NToC( A103ContratoGarantia_Percentual, 6, 2, ",", "")), ((edtContratoGarantia_Percentual_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A103ContratoGarantia_Percentual, "ZZ9.99")) : context.localUtil.Format( A103ContratoGarantia_Percentual, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGarantia_Percentual_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoGarantia_Percentual_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogarantia_datacaucao_Internalname, "Data da Cau��o", "", "", lblTextblockcontratogarantia_datacaucao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoGarantia_DataCaucao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoGarantia_DataCaucao_Internalname, context.localUtil.Format(A104ContratoGarantia_DataCaucao, "99/99/99"), context.localUtil.Format( A104ContratoGarantia_DataCaucao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGarantia_DataCaucao_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoGarantia_DataCaucao_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtContratoGarantia_DataCaucao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoGarantia_DataCaucao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogarantia_valorgarantia_Internalname, "Valor da Garantia", "", "", lblTextblockcontratogarantia_valorgarantia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoGarantia_ValorGarantia_Internalname, StringUtil.LTrim( StringUtil.NToC( A105ContratoGarantia_ValorGarantia, 18, 5, ",", "")), ((edtContratoGarantia_ValorGarantia_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A105ContratoGarantia_ValorGarantia, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A105ContratoGarantia_ValorGarantia, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGarantia_ValorGarantia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoGarantia_ValorGarantia_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogarantia_dataencerramento_Internalname, "Data de Encerramento", "", "", lblTextblockcontratogarantia_dataencerramento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoGarantia_DataEncerramento_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoGarantia_DataEncerramento_Internalname, context.localUtil.Format(A106ContratoGarantia_DataEncerramento, "99/99/99"), context.localUtil.Format( A106ContratoGarantia_DataEncerramento, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGarantia_DataEncerramento_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoGarantia_DataEncerramento_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtContratoGarantia_DataEncerramento_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoGarantia_DataEncerramento_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogarantia_valorencerramento_Internalname, "Valor do Encerramento", "", "", lblTextblockcontratogarantia_valorencerramento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoGarantia_ValorEncerramento_Internalname, StringUtil.LTrim( StringUtil.NToC( A107ContratoGarantia_ValorEncerramento, 18, 5, ",", "")), ((edtContratoGarantia_ValorEncerramento_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A107ContratoGarantia_ValorEncerramento, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A107ContratoGarantia_ValorEncerramento, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGarantia_ValorEncerramento_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoGarantia_ValorEncerramento_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_0K21e( true) ;
         }
         else
         {
            wb_table4_13_0K21e( false) ;
         }
      }

      protected void wb_table5_17_0K21( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblContrato_Internalname, tblContrato_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N� do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_22_0K21( true) ;
         }
         return  ;
      }

      protected void wb_table6_22_0K21e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_38_0K21( true) ;
         }
         return  ;
      }

      protected void wb_table7_38_0K21e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_17_0K21e( true) ;
         }
         else
         {
            wb_table5_17_0K21e( false) ;
         }
      }

      protected void wb_table7_38_0K21( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratada_pessoanom_Internalname, tblTablemergedcontratada_pessoanom_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoacnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_pessoacnpj_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCNPJ_Internalname, A42Contratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( A42Contratada_PessoaCNPJ, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCNPJ_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaCNPJ_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_38_0K21e( true) ;
         }
         else
         {
            wb_table7_38_0K21e( false) ;
         }
      }

      protected void wb_table6_22_0K21( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_numero_Internalname, tblTablemergedcontrato_numero_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Numero_Enabled, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numeroata_Internalname, "N� da Ata", "", "", lblTextblockcontrato_numeroata_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_NumeroAta_Internalname, StringUtil.RTrim( A78Contrato_NumeroAta), StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_NumeroAta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_NumeroAta_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "NumeroAta", "left", true, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ano_Internalname, "Ano", "", "", lblTextblockcontrato_ano_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")), ((edtContrato_Ano_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")) : context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Ano_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Ano_Enabled, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_ContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_22_0K21e( true) ;
         }
         else
         {
            wb_table6_22_0K21e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110K2 */
         E110K2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
               n78Contrato_NumeroAta = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
               n41Contratada_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
               n42Contratada_PessoaCNPJ = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               if ( context.localUtil.VCDate( cgiGet( edtContratoGarantia_DataPagtoGarantia_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"D. do Pagamento da Garantia"}), 1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoGarantia_DataPagtoGarantia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A102ContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A102ContratoGarantia_DataPagtoGarantia", context.localUtil.Format(A102ContratoGarantia_DataPagtoGarantia, "99/99/99"));
               }
               else
               {
                  A102ContratoGarantia_DataPagtoGarantia = context.localUtil.CToD( cgiGet( edtContratoGarantia_DataPagtoGarantia_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A102ContratoGarantia_DataPagtoGarantia", context.localUtil.Format(A102ContratoGarantia_DataPagtoGarantia, "99/99/99"));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoGarantia_Percentual_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoGarantia_Percentual_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOGARANTIA_PERCENTUAL");
                  AnyError = 1;
                  GX_FocusControl = edtContratoGarantia_Percentual_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A103ContratoGarantia_Percentual = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A103ContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( A103ContratoGarantia_Percentual, 6, 2)));
               }
               else
               {
                  A103ContratoGarantia_Percentual = context.localUtil.CToN( cgiGet( edtContratoGarantia_Percentual_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A103ContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( A103ContratoGarantia_Percentual, 6, 2)));
               }
               if ( context.localUtil.VCDate( cgiGet( edtContratoGarantia_DataCaucao_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"D. da Cau��o"}), 1, "CONTRATOGARANTIA_DATACAUCAO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoGarantia_DataCaucao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A104ContratoGarantia_DataCaucao = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A104ContratoGarantia_DataCaucao", context.localUtil.Format(A104ContratoGarantia_DataCaucao, "99/99/99"));
               }
               else
               {
                  A104ContratoGarantia_DataCaucao = context.localUtil.CToD( cgiGet( edtContratoGarantia_DataCaucao_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A104ContratoGarantia_DataCaucao", context.localUtil.Format(A104ContratoGarantia_DataCaucao, "99/99/99"));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoGarantia_ValorGarantia_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoGarantia_ValorGarantia_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOGARANTIA_VALORGARANTIA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoGarantia_ValorGarantia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A105ContratoGarantia_ValorGarantia = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A105ContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( A105ContratoGarantia_ValorGarantia, 18, 5)));
               }
               else
               {
                  A105ContratoGarantia_ValorGarantia = context.localUtil.CToN( cgiGet( edtContratoGarantia_ValorGarantia_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A105ContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( A105ContratoGarantia_ValorGarantia, 18, 5)));
               }
               if ( context.localUtil.VCDate( cgiGet( edtContratoGarantia_DataEncerramento_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"D. Encerramento da Garantia"}), 1, "CONTRATOGARANTIA_DATAENCERRAMENTO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoGarantia_DataEncerramento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A106ContratoGarantia_DataEncerramento = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A106ContratoGarantia_DataEncerramento", context.localUtil.Format(A106ContratoGarantia_DataEncerramento, "99/99/99"));
               }
               else
               {
                  A106ContratoGarantia_DataEncerramento = context.localUtil.CToD( cgiGet( edtContratoGarantia_DataEncerramento_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A106ContratoGarantia_DataEncerramento", context.localUtil.Format(A106ContratoGarantia_DataEncerramento, "99/99/99"));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoGarantia_ValorEncerramento_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoGarantia_ValorEncerramento_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOGARANTIA_VALORENCERRAMENTO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoGarantia_ValorEncerramento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A107ContratoGarantia_ValorEncerramento = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A107ContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( A107ContratoGarantia_ValorEncerramento, 18, 5)));
               }
               else
               {
                  A107ContratoGarantia_ValorEncerramento = context.localUtil.CToN( cgiGet( edtContratoGarantia_ValorEncerramento_Internalname), ",", ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A107ContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( A107ContratoGarantia_ValorEncerramento, 18, 5)));
               }
               /* Read saved values. */
               Z101ContratoGarantia_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z101ContratoGarantia_Codigo"), ",", "."));
               Z102ContratoGarantia_DataPagtoGarantia = context.localUtil.CToD( cgiGet( "Z102ContratoGarantia_DataPagtoGarantia"), 0);
               Z103ContratoGarantia_Percentual = context.localUtil.CToN( cgiGet( "Z103ContratoGarantia_Percentual"), ",", ".");
               Z104ContratoGarantia_DataCaucao = context.localUtil.CToD( cgiGet( "Z104ContratoGarantia_DataCaucao"), 0);
               Z105ContratoGarantia_ValorGarantia = context.localUtil.CToN( cgiGet( "Z105ContratoGarantia_ValorGarantia"), ",", ".");
               Z106ContratoGarantia_DataEncerramento = context.localUtil.CToD( cgiGet( "Z106ContratoGarantia_DataEncerramento"), 0);
               Z107ContratoGarantia_ValorEncerramento = context.localUtil.CToN( cgiGet( "Z107ContratoGarantia_ValorEncerramento"), ",", ".");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "N74Contrato_Codigo"), ",", "."));
               AV7ContratoGarantia_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOGARANTIA_CODIGO"), ",", "."));
               A101ContratoGarantia_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATOGARANTIA_CODIGO"), ",", "."));
               AV11Insert_Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATO_CODIGO"), ",", "."));
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATO_CODIGO"), ",", "."));
               A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_CODIGO"), ",", "."));
               A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_PESSOACOD"), ",", "."));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoGarantia";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A101ContratoGarantia_Codigo), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratogarantia:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratogarantia:[SecurityCheckFailed value for]"+"ContratoGarantia_Codigo:"+context.localUtil.Format( (decimal)(A101ContratoGarantia_Codigo), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A101ContratoGarantia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A101ContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A101ContratoGarantia_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode21 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode21;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound21 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0K0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110K2 */
                           E110K2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120K2 */
                           E120K2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120K2 */
            E120K2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0K21( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0K21( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0K0( )
      {
         BeforeValidate0K21( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0K21( ) ;
            }
            else
            {
               CheckExtendedTable0K21( ) ;
               CloseExtendedTableCursors0K21( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0K0( )
      {
      }

      protected void E110K2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Contrato_Codigo") == 0 )
               {
                  AV11Insert_Contrato_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contrato_Codigo), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
      }

      protected void E120K2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratogarantia.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A74Contrato_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0K21( short GX_JID )
      {
         if ( ( GX_JID == 15 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z102ContratoGarantia_DataPagtoGarantia = T000K3_A102ContratoGarantia_DataPagtoGarantia[0];
               Z103ContratoGarantia_Percentual = T000K3_A103ContratoGarantia_Percentual[0];
               Z104ContratoGarantia_DataCaucao = T000K3_A104ContratoGarantia_DataCaucao[0];
               Z105ContratoGarantia_ValorGarantia = T000K3_A105ContratoGarantia_ValorGarantia[0];
               Z106ContratoGarantia_DataEncerramento = T000K3_A106ContratoGarantia_DataEncerramento[0];
               Z107ContratoGarantia_ValorEncerramento = T000K3_A107ContratoGarantia_ValorEncerramento[0];
            }
            else
            {
               Z102ContratoGarantia_DataPagtoGarantia = A102ContratoGarantia_DataPagtoGarantia;
               Z103ContratoGarantia_Percentual = A103ContratoGarantia_Percentual;
               Z104ContratoGarantia_DataCaucao = A104ContratoGarantia_DataCaucao;
               Z105ContratoGarantia_ValorGarantia = A105ContratoGarantia_ValorGarantia;
               Z106ContratoGarantia_DataEncerramento = A106ContratoGarantia_DataEncerramento;
               Z107ContratoGarantia_ValorEncerramento = A107ContratoGarantia_ValorEncerramento;
            }
         }
         if ( GX_JID == -15 )
         {
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z101ContratoGarantia_Codigo = A101ContratoGarantia_Codigo;
            Z102ContratoGarantia_DataPagtoGarantia = A102ContratoGarantia_DataPagtoGarantia;
            Z103ContratoGarantia_Percentual = A103ContratoGarantia_Percentual;
            Z104ContratoGarantia_DataCaucao = A104ContratoGarantia_DataCaucao;
            Z105ContratoGarantia_ValorGarantia = A105ContratoGarantia_ValorGarantia;
            Z106ContratoGarantia_DataEncerramento = A106ContratoGarantia_DataEncerramento;
            Z107ContratoGarantia_ValorEncerramento = A107ContratoGarantia_ValorEncerramento;
            Z77Contrato_Numero = A77Contrato_Numero;
            Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
            Z79Contrato_Ano = A79Contrato_Ano;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
         }
      }

      protected void standaloneNotModal( )
      {
         AV13Pgmname = "ContratoGarantia";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoGarantia_Codigo) )
         {
            A101ContratoGarantia_Codigo = AV7ContratoGarantia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A101ContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A101ContratoGarantia_Codigo), 6, 0)));
         }
         /* Using cursor T000K4 */
         pr_default.execute(2, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A77Contrato_Numero = T000K4_A77Contrato_Numero[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A78Contrato_NumeroAta = T000K4_A78Contrato_NumeroAta[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
         n78Contrato_NumeroAta = T000K4_n78Contrato_NumeroAta[0];
         A79Contrato_Ano = T000K4_A79Contrato_Ano[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
         A39Contratada_Codigo = T000K4_A39Contratada_Codigo[0];
         pr_default.close(2);
         /* Using cursor T000K5 */
         pr_default.execute(3, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A40Contratada_PessoaCod = T000K5_A40Contratada_PessoaCod[0];
         pr_default.close(3);
         /* Using cursor T000K6 */
         pr_default.execute(4, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T000K6_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T000K6_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T000K6_A42Contratada_PessoaCNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         n42Contratada_PessoaCNPJ = T000K6_n42Contratada_PessoaCNPJ[0];
         pr_default.close(4);
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
            {
               A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            }
         }
      }

      protected void Load0K21( )
      {
         /* Using cursor T000K7 */
         pr_default.execute(5, new Object[] {A101ContratoGarantia_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound21 = 1;
            A77Contrato_Numero = T000K7_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = T000K7_A78Contrato_NumeroAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            n78Contrato_NumeroAta = T000K7_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = T000K7_A79Contrato_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A41Contratada_PessoaNom = T000K7_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T000K7_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T000K7_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T000K7_n42Contratada_PessoaCNPJ[0];
            A102ContratoGarantia_DataPagtoGarantia = T000K7_A102ContratoGarantia_DataPagtoGarantia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A102ContratoGarantia_DataPagtoGarantia", context.localUtil.Format(A102ContratoGarantia_DataPagtoGarantia, "99/99/99"));
            A103ContratoGarantia_Percentual = T000K7_A103ContratoGarantia_Percentual[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A103ContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( A103ContratoGarantia_Percentual, 6, 2)));
            A104ContratoGarantia_DataCaucao = T000K7_A104ContratoGarantia_DataCaucao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A104ContratoGarantia_DataCaucao", context.localUtil.Format(A104ContratoGarantia_DataCaucao, "99/99/99"));
            A105ContratoGarantia_ValorGarantia = T000K7_A105ContratoGarantia_ValorGarantia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A105ContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( A105ContratoGarantia_ValorGarantia, 18, 5)));
            A106ContratoGarantia_DataEncerramento = T000K7_A106ContratoGarantia_DataEncerramento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A106ContratoGarantia_DataEncerramento", context.localUtil.Format(A106ContratoGarantia_DataEncerramento, "99/99/99"));
            A107ContratoGarantia_ValorEncerramento = T000K7_A107ContratoGarantia_ValorEncerramento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A107ContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( A107ContratoGarantia_ValorEncerramento, 18, 5)));
            A39Contratada_Codigo = T000K7_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = T000K7_A40Contratada_PessoaCod[0];
            ZM0K21( -15) ;
         }
         pr_default.close(5);
         OnLoadActions0K21( ) ;
      }

      protected void OnLoadActions0K21( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
      }

      protected void CheckExtendedTable0K21( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         if ( ! ( (DateTime.MinValue==A102ContratoGarantia_DataPagtoGarantia) || ( A102ContratoGarantia_DataPagtoGarantia >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo D. do Pagamento da Garantia fora do intervalo", "OutOfRange", 1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA");
            AnyError = 1;
            GX_FocusControl = edtContratoGarantia_DataPagtoGarantia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (DateTime.MinValue==A102ContratoGarantia_DataPagtoGarantia) )
         {
            GX_msglist.addItem("D. do Pagamento da Garantia � obrigat�rio.", 1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA");
            AnyError = 1;
            GX_FocusControl = edtContratoGarantia_DataPagtoGarantia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (Convert.ToDecimal(0)==A103ContratoGarantia_Percentual) )
         {
            GX_msglist.addItem("Percentual � obrigat�rio.", 1, "CONTRATOGARANTIA_PERCENTUAL");
            AnyError = 1;
            GX_FocusControl = edtContratoGarantia_Percentual_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A104ContratoGarantia_DataCaucao) || ( A104ContratoGarantia_DataCaucao >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo D. da Cau��o fora do intervalo", "OutOfRange", 1, "CONTRATOGARANTIA_DATACAUCAO");
            AnyError = 1;
            GX_FocusControl = edtContratoGarantia_DataCaucao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (DateTime.MinValue==A104ContratoGarantia_DataCaucao) )
         {
            GX_msglist.addItem("D. da Cau��o � obrigat�rio.", 1, "CONTRATOGARANTIA_DATACAUCAO");
            AnyError = 1;
            GX_FocusControl = edtContratoGarantia_DataCaucao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (Convert.ToDecimal(0)==A105ContratoGarantia_ValorGarantia) )
         {
            GX_msglist.addItem("Valor Garantia � obrigat�rio.", 1, "CONTRATOGARANTIA_VALORGARANTIA");
            AnyError = 1;
            GX_FocusControl = edtContratoGarantia_ValorGarantia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A106ContratoGarantia_DataEncerramento) || ( A106ContratoGarantia_DataEncerramento >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo D. Encerramento da Garantia fora do intervalo", "OutOfRange", 1, "CONTRATOGARANTIA_DATAENCERRAMENTO");
            AnyError = 1;
            GX_FocusControl = edtContratoGarantia_DataEncerramento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (DateTime.MinValue==A106ContratoGarantia_DataEncerramento) )
         {
            GX_msglist.addItem("D. Encerramento da Garantia � obrigat�rio.", 1, "CONTRATOGARANTIA_DATAENCERRAMENTO");
            AnyError = 1;
            GX_FocusControl = edtContratoGarantia_DataEncerramento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (Convert.ToDecimal(0)==A107ContratoGarantia_ValorEncerramento) )
         {
            GX_msglist.addItem("Valor Encerramento � obrigat�rio.", 1, "CONTRATOGARANTIA_VALORENCERRAMENTO");
            AnyError = 1;
            GX_FocusControl = edtContratoGarantia_ValorEncerramento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors0K21( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0K21( )
      {
         /* Using cursor T000K8 */
         pr_default.execute(6, new Object[] {A101ContratoGarantia_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound21 = 1;
         }
         else
         {
            RcdFound21 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000K3 */
         pr_default.execute(1, new Object[] {A101ContratoGarantia_Codigo});
         if ( (pr_default.getStatus(1) != 101) && ( T000K3_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
         {
            ZM0K21( 15) ;
            RcdFound21 = 1;
            A101ContratoGarantia_Codigo = T000K3_A101ContratoGarantia_Codigo[0];
            A102ContratoGarantia_DataPagtoGarantia = T000K3_A102ContratoGarantia_DataPagtoGarantia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A102ContratoGarantia_DataPagtoGarantia", context.localUtil.Format(A102ContratoGarantia_DataPagtoGarantia, "99/99/99"));
            A103ContratoGarantia_Percentual = T000K3_A103ContratoGarantia_Percentual[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A103ContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( A103ContratoGarantia_Percentual, 6, 2)));
            A104ContratoGarantia_DataCaucao = T000K3_A104ContratoGarantia_DataCaucao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A104ContratoGarantia_DataCaucao", context.localUtil.Format(A104ContratoGarantia_DataCaucao, "99/99/99"));
            A105ContratoGarantia_ValorGarantia = T000K3_A105ContratoGarantia_ValorGarantia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A105ContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( A105ContratoGarantia_ValorGarantia, 18, 5)));
            A106ContratoGarantia_DataEncerramento = T000K3_A106ContratoGarantia_DataEncerramento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A106ContratoGarantia_DataEncerramento", context.localUtil.Format(A106ContratoGarantia_DataEncerramento, "99/99/99"));
            A107ContratoGarantia_ValorEncerramento = T000K3_A107ContratoGarantia_ValorEncerramento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A107ContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( A107ContratoGarantia_ValorEncerramento, 18, 5)));
            Z101ContratoGarantia_Codigo = A101ContratoGarantia_Codigo;
            sMode21 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0K21( ) ;
            if ( AnyError == 1 )
            {
               RcdFound21 = 0;
               InitializeNonKey0K21( ) ;
            }
            Gx_mode = sMode21;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound21 = 0;
            InitializeNonKey0K21( ) ;
            sMode21 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode21;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0K21( ) ;
         if ( RcdFound21 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound21 = 0;
         /* Using cursor T000K9 */
         pr_default.execute(7, new Object[] {A101ContratoGarantia_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T000K9_A101ContratoGarantia_Codigo[0] < A101ContratoGarantia_Codigo ) ) && ( T000K9_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T000K9_A101ContratoGarantia_Codigo[0] > A101ContratoGarantia_Codigo ) ) && ( T000K9_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               A101ContratoGarantia_Codigo = T000K9_A101ContratoGarantia_Codigo[0];
               RcdFound21 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void move_previous( )
      {
         RcdFound21 = 0;
         /* Using cursor T000K10 */
         pr_default.execute(8, new Object[] {A101ContratoGarantia_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T000K10_A101ContratoGarantia_Codigo[0] > A101ContratoGarantia_Codigo ) ) && ( T000K10_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T000K10_A101ContratoGarantia_Codigo[0] < A101ContratoGarantia_Codigo ) ) && ( T000K10_A74Contrato_Codigo[0] == A74Contrato_Codigo ) )
            {
               A101ContratoGarantia_Codigo = T000K10_A101ContratoGarantia_Codigo[0];
               RcdFound21 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0K21( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoGarantia_DataPagtoGarantia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0K21( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound21 == 1 )
            {
               if ( A101ContratoGarantia_Codigo != Z101ContratoGarantia_Codigo )
               {
                  A101ContratoGarantia_Codigo = Z101ContratoGarantia_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A101ContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A101ContratoGarantia_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoGarantia_DataPagtoGarantia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0K21( ) ;
                  GX_FocusControl = edtContratoGarantia_DataPagtoGarantia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A101ContratoGarantia_Codigo != Z101ContratoGarantia_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContratoGarantia_DataPagtoGarantia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0K21( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContratoGarantia_DataPagtoGarantia_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0K21( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A101ContratoGarantia_Codigo != Z101ContratoGarantia_Codigo )
         {
            A101ContratoGarantia_Codigo = Z101ContratoGarantia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A101ContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A101ContratoGarantia_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoGarantia_DataPagtoGarantia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0K21( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000K2 */
            pr_default.execute(0, new Object[] {A101ContratoGarantia_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoGarantia"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z102ContratoGarantia_DataPagtoGarantia != T000K2_A102ContratoGarantia_DataPagtoGarantia[0] ) || ( Z103ContratoGarantia_Percentual != T000K2_A103ContratoGarantia_Percentual[0] ) || ( Z104ContratoGarantia_DataCaucao != T000K2_A104ContratoGarantia_DataCaucao[0] ) || ( Z105ContratoGarantia_ValorGarantia != T000K2_A105ContratoGarantia_ValorGarantia[0] ) || ( Z106ContratoGarantia_DataEncerramento != T000K2_A106ContratoGarantia_DataEncerramento[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z107ContratoGarantia_ValorEncerramento != T000K2_A107ContratoGarantia_ValorEncerramento[0] ) )
            {
               if ( Z102ContratoGarantia_DataPagtoGarantia != T000K2_A102ContratoGarantia_DataPagtoGarantia[0] )
               {
                  GXUtil.WriteLog("contratogarantia:[seudo value changed for attri]"+"ContratoGarantia_DataPagtoGarantia");
                  GXUtil.WriteLogRaw("Old: ",Z102ContratoGarantia_DataPagtoGarantia);
                  GXUtil.WriteLogRaw("Current: ",T000K2_A102ContratoGarantia_DataPagtoGarantia[0]);
               }
               if ( Z103ContratoGarantia_Percentual != T000K2_A103ContratoGarantia_Percentual[0] )
               {
                  GXUtil.WriteLog("contratogarantia:[seudo value changed for attri]"+"ContratoGarantia_Percentual");
                  GXUtil.WriteLogRaw("Old: ",Z103ContratoGarantia_Percentual);
                  GXUtil.WriteLogRaw("Current: ",T000K2_A103ContratoGarantia_Percentual[0]);
               }
               if ( Z104ContratoGarantia_DataCaucao != T000K2_A104ContratoGarantia_DataCaucao[0] )
               {
                  GXUtil.WriteLog("contratogarantia:[seudo value changed for attri]"+"ContratoGarantia_DataCaucao");
                  GXUtil.WriteLogRaw("Old: ",Z104ContratoGarantia_DataCaucao);
                  GXUtil.WriteLogRaw("Current: ",T000K2_A104ContratoGarantia_DataCaucao[0]);
               }
               if ( Z105ContratoGarantia_ValorGarantia != T000K2_A105ContratoGarantia_ValorGarantia[0] )
               {
                  GXUtil.WriteLog("contratogarantia:[seudo value changed for attri]"+"ContratoGarantia_ValorGarantia");
                  GXUtil.WriteLogRaw("Old: ",Z105ContratoGarantia_ValorGarantia);
                  GXUtil.WriteLogRaw("Current: ",T000K2_A105ContratoGarantia_ValorGarantia[0]);
               }
               if ( Z106ContratoGarantia_DataEncerramento != T000K2_A106ContratoGarantia_DataEncerramento[0] )
               {
                  GXUtil.WriteLog("contratogarantia:[seudo value changed for attri]"+"ContratoGarantia_DataEncerramento");
                  GXUtil.WriteLogRaw("Old: ",Z106ContratoGarantia_DataEncerramento);
                  GXUtil.WriteLogRaw("Current: ",T000K2_A106ContratoGarantia_DataEncerramento[0]);
               }
               if ( Z107ContratoGarantia_ValorEncerramento != T000K2_A107ContratoGarantia_ValorEncerramento[0] )
               {
                  GXUtil.WriteLog("contratogarantia:[seudo value changed for attri]"+"ContratoGarantia_ValorEncerramento");
                  GXUtil.WriteLogRaw("Old: ",Z107ContratoGarantia_ValorEncerramento);
                  GXUtil.WriteLogRaw("Current: ",T000K2_A107ContratoGarantia_ValorEncerramento[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoGarantia"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0K21( )
      {
         BeforeValidate0K21( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0K21( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0K21( 0) ;
            CheckOptimisticConcurrency0K21( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0K21( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0K21( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000K11 */
                     pr_default.execute(9, new Object[] {A74Contrato_Codigo, A102ContratoGarantia_DataPagtoGarantia, A103ContratoGarantia_Percentual, A104ContratoGarantia_DataCaucao, A105ContratoGarantia_ValorGarantia, A106ContratoGarantia_DataEncerramento, A107ContratoGarantia_ValorEncerramento});
                     A101ContratoGarantia_Codigo = T000K11_A101ContratoGarantia_Codigo[0];
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoGarantia") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0K0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0K21( ) ;
            }
            EndLevel0K21( ) ;
         }
         CloseExtendedTableCursors0K21( ) ;
      }

      protected void Update0K21( )
      {
         BeforeValidate0K21( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0K21( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0K21( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0K21( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0K21( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000K12 */
                     pr_default.execute(10, new Object[] {A74Contrato_Codigo, A102ContratoGarantia_DataPagtoGarantia, A103ContratoGarantia_Percentual, A104ContratoGarantia_DataCaucao, A105ContratoGarantia_ValorGarantia, A106ContratoGarantia_DataEncerramento, A107ContratoGarantia_ValorEncerramento, A101ContratoGarantia_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoGarantia") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoGarantia"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0K21( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0K21( ) ;
         }
         CloseExtendedTableCursors0K21( ) ;
      }

      protected void DeferredUpdate0K21( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0K21( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0K21( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0K21( ) ;
            AfterConfirm0K21( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0K21( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000K13 */
                  pr_default.execute(11, new Object[] {A101ContratoGarantia_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoGarantia") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode21 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0K21( ) ;
         Gx_mode = sMode21;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0K21( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel0K21( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0K21( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ContratoGarantia");
            if ( AnyError == 0 )
            {
               ConfirmValues0K0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ContratoGarantia");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0K21( )
      {
         /* Scan By routine */
         /* Using cursor T000K14 */
         pr_default.execute(12, new Object[] {A74Contrato_Codigo});
         RcdFound21 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound21 = 1;
            A101ContratoGarantia_Codigo = T000K14_A101ContratoGarantia_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0K21( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound21 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound21 = 1;
            A101ContratoGarantia_Codigo = T000K14_A101ContratoGarantia_Codigo[0];
         }
      }

      protected void ScanEnd0K21( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm0K21( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0K21( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0K21( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0K21( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0K21( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0K21( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0K21( )
      {
         edtContrato_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Numero_Enabled), 5, 0)));
         edtContrato_NumeroAta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_NumeroAta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_NumeroAta_Enabled), 5, 0)));
         edtContrato_Ano_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Ano_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Ano_Enabled), 5, 0)));
         edtContratada_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaNom_Enabled), 5, 0)));
         edtContratada_PessoaCNPJ_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaCNPJ_Enabled), 5, 0)));
         edtContratoGarantia_DataPagtoGarantia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_DataPagtoGarantia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGarantia_DataPagtoGarantia_Enabled), 5, 0)));
         edtContratoGarantia_Percentual_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_Percentual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGarantia_Percentual_Enabled), 5, 0)));
         edtContratoGarantia_DataCaucao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_DataCaucao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGarantia_DataCaucao_Enabled), 5, 0)));
         edtContratoGarantia_ValorGarantia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_ValorGarantia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGarantia_ValorGarantia_Enabled), 5, 0)));
         edtContratoGarantia_DataEncerramento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_DataEncerramento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGarantia_DataEncerramento_Enabled), 5, 0)));
         edtContratoGarantia_ValorEncerramento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_ValorEncerramento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGarantia_ValorEncerramento_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0K0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117171814");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratogarantia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoGarantia_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z101ContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z101ContratoGarantia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z102ContratoGarantia_DataPagtoGarantia", context.localUtil.DToC( Z102ContratoGarantia_DataPagtoGarantia, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z103ContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.NToC( Z103ContratoGarantia_Percentual, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z104ContratoGarantia_DataCaucao", context.localUtil.DToC( Z104ContratoGarantia_DataCaucao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z105ContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.NToC( Z105ContratoGarantia_ValorGarantia, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z106ContratoGarantia_DataEncerramento", context.localUtil.DToC( Z106ContratoGarantia_DataEncerramento, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z107ContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.NToC( Z107ContratoGarantia_ValorEncerramento, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOGARANTIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoGarantia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A101ContratoGarantia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOGARANTIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoGarantia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoGarantia";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A101ContratoGarantia_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratogarantia:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratogarantia:[SendSecurityCheck value for]"+"ContratoGarantia_Codigo:"+context.localUtil.Format( (decimal)(A101ContratoGarantia_Codigo), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratogarantia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoGarantia_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoGarantia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Garantia" ;
      }

      protected void InitializeNonKey0K21( )
      {
         A102ContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A102ContratoGarantia_DataPagtoGarantia", context.localUtil.Format(A102ContratoGarantia_DataPagtoGarantia, "99/99/99"));
         A103ContratoGarantia_Percentual = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A103ContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( A103ContratoGarantia_Percentual, 6, 2)));
         A104ContratoGarantia_DataCaucao = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A104ContratoGarantia_DataCaucao", context.localUtil.Format(A104ContratoGarantia_DataCaucao, "99/99/99"));
         A105ContratoGarantia_ValorGarantia = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A105ContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( A105ContratoGarantia_ValorGarantia, 18, 5)));
         A106ContratoGarantia_DataEncerramento = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A106ContratoGarantia_DataEncerramento", context.localUtil.Format(A106ContratoGarantia_DataEncerramento, "99/99/99"));
         A107ContratoGarantia_ValorEncerramento = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A107ContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( A107ContratoGarantia_ValorEncerramento, 18, 5)));
         Z102ContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         Z103ContratoGarantia_Percentual = 0;
         Z104ContratoGarantia_DataCaucao = DateTime.MinValue;
         Z105ContratoGarantia_ValorGarantia = 0;
         Z106ContratoGarantia_DataEncerramento = DateTime.MinValue;
         Z107ContratoGarantia_ValorEncerramento = 0;
      }

      protected void InitAll0K21( )
      {
         A101ContratoGarantia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A101ContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A101ContratoGarantia_Codigo), 6, 0)));
         InitializeNonKey0K21( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117171840");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratogarantia.js", "?20203117171840");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontrato_numero_Internalname = "TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         lblTextblockcontrato_numeroata_Internalname = "TEXTBLOCKCONTRATO_NUMEROATA";
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA";
         lblTextblockcontrato_ano_Internalname = "TEXTBLOCKCONTRATO_ANO";
         edtContrato_Ano_Internalname = "CONTRATO_ANO";
         tblTablemergedcontrato_numero_Internalname = "TABLEMERGEDCONTRATO_NUMERO";
         lblTextblockcontratada_pessoanom_Internalname = "TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         lblTextblockcontratada_pessoacnpj_Internalname = "TEXTBLOCKCONTRATADA_PESSOACNPJ";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         tblTablemergedcontratada_pessoanom_Internalname = "TABLEMERGEDCONTRATADA_PESSOANOM";
         tblContrato_Internalname = "CONTRATO";
         grpUnnamedgroup1_Internalname = "UNNAMEDGROUP1";
         lblTextblockcontratogarantia_datapagtogarantia_Internalname = "TEXTBLOCKCONTRATOGARANTIA_DATAPAGTOGARANTIA";
         edtContratoGarantia_DataPagtoGarantia_Internalname = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         lblTextblockcontratogarantia_percentual_Internalname = "TEXTBLOCKCONTRATOGARANTIA_PERCENTUAL";
         edtContratoGarantia_Percentual_Internalname = "CONTRATOGARANTIA_PERCENTUAL";
         lblTextblockcontratogarantia_datacaucao_Internalname = "TEXTBLOCKCONTRATOGARANTIA_DATACAUCAO";
         edtContratoGarantia_DataCaucao_Internalname = "CONTRATOGARANTIA_DATACAUCAO";
         lblTextblockcontratogarantia_valorgarantia_Internalname = "TEXTBLOCKCONTRATOGARANTIA_VALORGARANTIA";
         edtContratoGarantia_ValorGarantia_Internalname = "CONTRATOGARANTIA_VALORGARANTIA";
         lblTextblockcontratogarantia_dataencerramento_Internalname = "TEXTBLOCKCONTRATOGARANTIA_DATAENCERRAMENTO";
         edtContratoGarantia_DataEncerramento_Internalname = "CONTRATOGARANTIA_DATAENCERRAMENTO";
         lblTextblockcontratogarantia_valorencerramento_Internalname = "TEXTBLOCKCONTRATOGARANTIA_VALORENCERRAMENTO";
         edtContratoGarantia_ValorEncerramento_Internalname = "CONTRATOGARANTIA_VALORENCERRAMENTO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Garantia";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contrato Garantia";
         edtContrato_Ano_Jsonclick = "";
         edtContrato_Ano_Enabled = 0;
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_NumeroAta_Enabled = 0;
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Numero_Enabled = 0;
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaCNPJ_Enabled = 0;
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaNom_Enabled = 0;
         edtContratoGarantia_ValorEncerramento_Jsonclick = "";
         edtContratoGarantia_ValorEncerramento_Enabled = 1;
         edtContratoGarantia_DataEncerramento_Jsonclick = "";
         edtContratoGarantia_DataEncerramento_Enabled = 1;
         edtContratoGarantia_ValorGarantia_Jsonclick = "";
         edtContratoGarantia_ValorGarantia_Enabled = 1;
         edtContratoGarantia_DataCaucao_Jsonclick = "";
         edtContratoGarantia_DataCaucao_Enabled = 1;
         edtContratoGarantia_Percentual_Jsonclick = "";
         edtContratoGarantia_Percentual_Enabled = 1;
         edtContratoGarantia_DataPagtoGarantia_Jsonclick = "";
         edtContratoGarantia_DataPagtoGarantia_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoGarantia_Codigo',fld:'vCONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120K2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z102ContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         Z104ContratoGarantia_DataCaucao = DateTime.MinValue;
         Z106ContratoGarantia_DataEncerramento = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontratogarantia_datapagtogarantia_Jsonclick = "";
         A102ContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         lblTextblockcontratogarantia_percentual_Jsonclick = "";
         lblTextblockcontratogarantia_datacaucao_Jsonclick = "";
         A104ContratoGarantia_DataCaucao = DateTime.MinValue;
         lblTextblockcontratogarantia_valorgarantia_Jsonclick = "";
         lblTextblockcontratogarantia_dataencerramento_Jsonclick = "";
         A106ContratoGarantia_DataEncerramento = DateTime.MinValue;
         lblTextblockcontratogarantia_valorencerramento_Jsonclick = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         A41Contratada_PessoaNom = "";
         lblTextblockcontratada_pessoacnpj_Jsonclick = "";
         A42Contratada_PessoaCNPJ = "";
         A77Contrato_Numero = "";
         lblTextblockcontrato_numeroata_Jsonclick = "";
         A78Contrato_NumeroAta = "";
         lblTextblockcontrato_ano_Jsonclick = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode21 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z77Contrato_Numero = "";
         Z78Contrato_NumeroAta = "";
         Z41Contratada_PessoaNom = "";
         Z42Contratada_PessoaCNPJ = "";
         T000K4_A77Contrato_Numero = new String[] {""} ;
         T000K4_A78Contrato_NumeroAta = new String[] {""} ;
         T000K4_n78Contrato_NumeroAta = new bool[] {false} ;
         T000K4_A79Contrato_Ano = new short[1] ;
         T000K4_A39Contratada_Codigo = new int[1] ;
         T000K5_A40Contratada_PessoaCod = new int[1] ;
         T000K6_A41Contratada_PessoaNom = new String[] {""} ;
         T000K6_n41Contratada_PessoaNom = new bool[] {false} ;
         T000K6_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000K6_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000K7_A74Contrato_Codigo = new int[1] ;
         T000K7_A101ContratoGarantia_Codigo = new int[1] ;
         T000K7_A77Contrato_Numero = new String[] {""} ;
         T000K7_A78Contrato_NumeroAta = new String[] {""} ;
         T000K7_n78Contrato_NumeroAta = new bool[] {false} ;
         T000K7_A79Contrato_Ano = new short[1] ;
         T000K7_A41Contratada_PessoaNom = new String[] {""} ;
         T000K7_n41Contratada_PessoaNom = new bool[] {false} ;
         T000K7_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T000K7_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T000K7_A102ContratoGarantia_DataPagtoGarantia = new DateTime[] {DateTime.MinValue} ;
         T000K7_A103ContratoGarantia_Percentual = new decimal[1] ;
         T000K7_A104ContratoGarantia_DataCaucao = new DateTime[] {DateTime.MinValue} ;
         T000K7_A105ContratoGarantia_ValorGarantia = new decimal[1] ;
         T000K7_A106ContratoGarantia_DataEncerramento = new DateTime[] {DateTime.MinValue} ;
         T000K7_A107ContratoGarantia_ValorEncerramento = new decimal[1] ;
         T000K7_A39Contratada_Codigo = new int[1] ;
         T000K7_A40Contratada_PessoaCod = new int[1] ;
         T000K8_A101ContratoGarantia_Codigo = new int[1] ;
         T000K3_A74Contrato_Codigo = new int[1] ;
         T000K3_A101ContratoGarantia_Codigo = new int[1] ;
         T000K3_A102ContratoGarantia_DataPagtoGarantia = new DateTime[] {DateTime.MinValue} ;
         T000K3_A103ContratoGarantia_Percentual = new decimal[1] ;
         T000K3_A104ContratoGarantia_DataCaucao = new DateTime[] {DateTime.MinValue} ;
         T000K3_A105ContratoGarantia_ValorGarantia = new decimal[1] ;
         T000K3_A106ContratoGarantia_DataEncerramento = new DateTime[] {DateTime.MinValue} ;
         T000K3_A107ContratoGarantia_ValorEncerramento = new decimal[1] ;
         T000K9_A101ContratoGarantia_Codigo = new int[1] ;
         T000K9_A74Contrato_Codigo = new int[1] ;
         T000K10_A101ContratoGarantia_Codigo = new int[1] ;
         T000K10_A74Contrato_Codigo = new int[1] ;
         T000K2_A74Contrato_Codigo = new int[1] ;
         T000K2_A101ContratoGarantia_Codigo = new int[1] ;
         T000K2_A102ContratoGarantia_DataPagtoGarantia = new DateTime[] {DateTime.MinValue} ;
         T000K2_A103ContratoGarantia_Percentual = new decimal[1] ;
         T000K2_A104ContratoGarantia_DataCaucao = new DateTime[] {DateTime.MinValue} ;
         T000K2_A105ContratoGarantia_ValorGarantia = new decimal[1] ;
         T000K2_A106ContratoGarantia_DataEncerramento = new DateTime[] {DateTime.MinValue} ;
         T000K2_A107ContratoGarantia_ValorEncerramento = new decimal[1] ;
         T000K11_A101ContratoGarantia_Codigo = new int[1] ;
         T000K14_A101ContratoGarantia_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratogarantia__default(),
            new Object[][] {
                new Object[] {
               T000K2_A74Contrato_Codigo, T000K2_A101ContratoGarantia_Codigo, T000K2_A102ContratoGarantia_DataPagtoGarantia, T000K2_A103ContratoGarantia_Percentual, T000K2_A104ContratoGarantia_DataCaucao, T000K2_A105ContratoGarantia_ValorGarantia, T000K2_A106ContratoGarantia_DataEncerramento, T000K2_A107ContratoGarantia_ValorEncerramento
               }
               , new Object[] {
               T000K3_A74Contrato_Codigo, T000K3_A101ContratoGarantia_Codigo, T000K3_A102ContratoGarantia_DataPagtoGarantia, T000K3_A103ContratoGarantia_Percentual, T000K3_A104ContratoGarantia_DataCaucao, T000K3_A105ContratoGarantia_ValorGarantia, T000K3_A106ContratoGarantia_DataEncerramento, T000K3_A107ContratoGarantia_ValorEncerramento
               }
               , new Object[] {
               T000K4_A77Contrato_Numero, T000K4_A78Contrato_NumeroAta, T000K4_n78Contrato_NumeroAta, T000K4_A79Contrato_Ano, T000K4_A39Contratada_Codigo
               }
               , new Object[] {
               T000K5_A40Contratada_PessoaCod
               }
               , new Object[] {
               T000K6_A41Contratada_PessoaNom, T000K6_n41Contratada_PessoaNom, T000K6_A42Contratada_PessoaCNPJ, T000K6_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T000K7_A74Contrato_Codigo, T000K7_A101ContratoGarantia_Codigo, T000K7_A77Contrato_Numero, T000K7_A78Contrato_NumeroAta, T000K7_n78Contrato_NumeroAta, T000K7_A79Contrato_Ano, T000K7_A41Contratada_PessoaNom, T000K7_n41Contratada_PessoaNom, T000K7_A42Contratada_PessoaCNPJ, T000K7_n42Contratada_PessoaCNPJ,
               T000K7_A102ContratoGarantia_DataPagtoGarantia, T000K7_A103ContratoGarantia_Percentual, T000K7_A104ContratoGarantia_DataCaucao, T000K7_A105ContratoGarantia_ValorGarantia, T000K7_A106ContratoGarantia_DataEncerramento, T000K7_A107ContratoGarantia_ValorEncerramento, T000K7_A39Contratada_Codigo, T000K7_A40Contratada_PessoaCod
               }
               , new Object[] {
               T000K8_A101ContratoGarantia_Codigo
               }
               , new Object[] {
               T000K9_A101ContratoGarantia_Codigo, T000K9_A74Contrato_Codigo
               }
               , new Object[] {
               T000K10_A101ContratoGarantia_Codigo, T000K10_A74Contrato_Codigo
               }
               , new Object[] {
               T000K11_A101ContratoGarantia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000K14_A101ContratoGarantia_Codigo
               }
            }
         );
         N74Contrato_Codigo = 0;
         Z74Contrato_Codigo = 0;
         A74Contrato_Codigo = 0;
         AV13Pgmname = "ContratoGarantia";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A79Contrato_Ano ;
      private short RcdFound21 ;
      private short GX_JID ;
      private short Z79Contrato_Ano ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int wcpOAV7ContratoGarantia_Codigo ;
      private int wcpOA74Contrato_Codigo ;
      private int Z101ContratoGarantia_Codigo ;
      private int N74Contrato_Codigo ;
      private int AV7ContratoGarantia_Codigo ;
      private int A74Contrato_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContratoGarantia_DataPagtoGarantia_Enabled ;
      private int edtContratoGarantia_Percentual_Enabled ;
      private int edtContratoGarantia_DataCaucao_Enabled ;
      private int edtContratoGarantia_ValorGarantia_Enabled ;
      private int edtContratoGarantia_DataEncerramento_Enabled ;
      private int edtContratoGarantia_ValorEncerramento_Enabled ;
      private int edtContratada_PessoaNom_Enabled ;
      private int edtContratada_PessoaCNPJ_Enabled ;
      private int edtContrato_Numero_Enabled ;
      private int edtContrato_NumeroAta_Enabled ;
      private int edtContrato_Ano_Enabled ;
      private int A101ContratoGarantia_Codigo ;
      private int AV11Insert_Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int AV14GXV1 ;
      private int Z74Contrato_Codigo ;
      private int Z39Contratada_Codigo ;
      private int Z40Contratada_PessoaCod ;
      private int idxLst ;
      private decimal Z103ContratoGarantia_Percentual ;
      private decimal Z105ContratoGarantia_ValorGarantia ;
      private decimal Z107ContratoGarantia_ValorEncerramento ;
      private decimal A103ContratoGarantia_Percentual ;
      private decimal A105ContratoGarantia_ValorGarantia ;
      private decimal A107ContratoGarantia_ValorEncerramento ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoGarantia_DataPagtoGarantia_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String grpUnnamedgroup1_Internalname ;
      private String lblTextblockcontratogarantia_datapagtogarantia_Internalname ;
      private String lblTextblockcontratogarantia_datapagtogarantia_Jsonclick ;
      private String edtContratoGarantia_DataPagtoGarantia_Jsonclick ;
      private String lblTextblockcontratogarantia_percentual_Internalname ;
      private String lblTextblockcontratogarantia_percentual_Jsonclick ;
      private String edtContratoGarantia_Percentual_Internalname ;
      private String edtContratoGarantia_Percentual_Jsonclick ;
      private String lblTextblockcontratogarantia_datacaucao_Internalname ;
      private String lblTextblockcontratogarantia_datacaucao_Jsonclick ;
      private String edtContratoGarantia_DataCaucao_Internalname ;
      private String edtContratoGarantia_DataCaucao_Jsonclick ;
      private String lblTextblockcontratogarantia_valorgarantia_Internalname ;
      private String lblTextblockcontratogarantia_valorgarantia_Jsonclick ;
      private String edtContratoGarantia_ValorGarantia_Internalname ;
      private String edtContratoGarantia_ValorGarantia_Jsonclick ;
      private String lblTextblockcontratogarantia_dataencerramento_Internalname ;
      private String lblTextblockcontratogarantia_dataencerramento_Jsonclick ;
      private String edtContratoGarantia_DataEncerramento_Internalname ;
      private String edtContratoGarantia_DataEncerramento_Jsonclick ;
      private String lblTextblockcontratogarantia_valorencerramento_Internalname ;
      private String lblTextblockcontratogarantia_valorencerramento_Jsonclick ;
      private String edtContratoGarantia_ValorEncerramento_Internalname ;
      private String edtContratoGarantia_ValorEncerramento_Jsonclick ;
      private String tblContrato_Internalname ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String tblTablemergedcontratada_pessoanom_Internalname ;
      private String edtContratada_PessoaNom_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratada_pessoacnpj_Internalname ;
      private String lblTextblockcontratada_pessoacnpj_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String tblTablemergedcontrato_numero_Internalname ;
      private String edtContrato_Numero_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontrato_numeroata_Internalname ;
      private String lblTextblockcontrato_numeroata_Jsonclick ;
      private String edtContrato_NumeroAta_Internalname ;
      private String A78Contrato_NumeroAta ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String lblTextblockcontrato_ano_Internalname ;
      private String lblTextblockcontrato_ano_Jsonclick ;
      private String edtContrato_Ano_Internalname ;
      private String edtContrato_Ano_Jsonclick ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode21 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z77Contrato_Numero ;
      private String Z78Contrato_NumeroAta ;
      private String Z41Contratada_PessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z102ContratoGarantia_DataPagtoGarantia ;
      private DateTime Z104ContratoGarantia_DataCaucao ;
      private DateTime Z106ContratoGarantia_DataEncerramento ;
      private DateTime A102ContratoGarantia_DataPagtoGarantia ;
      private DateTime A104ContratoGarantia_DataCaucao ;
      private DateTime A106ContratoGarantia_DataEncerramento ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n78Contrato_NumeroAta ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A42Contratada_PessoaCNPJ ;
      private String Z42Contratada_PessoaCNPJ ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Contrato_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] T000K4_A77Contrato_Numero ;
      private String[] T000K4_A78Contrato_NumeroAta ;
      private bool[] T000K4_n78Contrato_NumeroAta ;
      private short[] T000K4_A79Contrato_Ano ;
      private int[] T000K4_A39Contratada_Codigo ;
      private int[] T000K5_A40Contratada_PessoaCod ;
      private String[] T000K6_A41Contratada_PessoaNom ;
      private bool[] T000K6_n41Contratada_PessoaNom ;
      private String[] T000K6_A42Contratada_PessoaCNPJ ;
      private bool[] T000K6_n42Contratada_PessoaCNPJ ;
      private int[] T000K7_A74Contrato_Codigo ;
      private int[] T000K7_A101ContratoGarantia_Codigo ;
      private String[] T000K7_A77Contrato_Numero ;
      private String[] T000K7_A78Contrato_NumeroAta ;
      private bool[] T000K7_n78Contrato_NumeroAta ;
      private short[] T000K7_A79Contrato_Ano ;
      private String[] T000K7_A41Contratada_PessoaNom ;
      private bool[] T000K7_n41Contratada_PessoaNom ;
      private String[] T000K7_A42Contratada_PessoaCNPJ ;
      private bool[] T000K7_n42Contratada_PessoaCNPJ ;
      private DateTime[] T000K7_A102ContratoGarantia_DataPagtoGarantia ;
      private decimal[] T000K7_A103ContratoGarantia_Percentual ;
      private DateTime[] T000K7_A104ContratoGarantia_DataCaucao ;
      private decimal[] T000K7_A105ContratoGarantia_ValorGarantia ;
      private DateTime[] T000K7_A106ContratoGarantia_DataEncerramento ;
      private decimal[] T000K7_A107ContratoGarantia_ValorEncerramento ;
      private int[] T000K7_A39Contratada_Codigo ;
      private int[] T000K7_A40Contratada_PessoaCod ;
      private int[] T000K8_A101ContratoGarantia_Codigo ;
      private int[] T000K3_A74Contrato_Codigo ;
      private int[] T000K3_A101ContratoGarantia_Codigo ;
      private DateTime[] T000K3_A102ContratoGarantia_DataPagtoGarantia ;
      private decimal[] T000K3_A103ContratoGarantia_Percentual ;
      private DateTime[] T000K3_A104ContratoGarantia_DataCaucao ;
      private decimal[] T000K3_A105ContratoGarantia_ValorGarantia ;
      private DateTime[] T000K3_A106ContratoGarantia_DataEncerramento ;
      private decimal[] T000K3_A107ContratoGarantia_ValorEncerramento ;
      private int[] T000K9_A101ContratoGarantia_Codigo ;
      private int[] T000K9_A74Contrato_Codigo ;
      private int[] T000K10_A101ContratoGarantia_Codigo ;
      private int[] T000K10_A74Contrato_Codigo ;
      private int[] T000K2_A74Contrato_Codigo ;
      private int[] T000K2_A101ContratoGarantia_Codigo ;
      private DateTime[] T000K2_A102ContratoGarantia_DataPagtoGarantia ;
      private decimal[] T000K2_A103ContratoGarantia_Percentual ;
      private DateTime[] T000K2_A104ContratoGarantia_DataCaucao ;
      private decimal[] T000K2_A105ContratoGarantia_ValorGarantia ;
      private DateTime[] T000K2_A106ContratoGarantia_DataEncerramento ;
      private decimal[] T000K2_A107ContratoGarantia_ValorEncerramento ;
      private int[] T000K11_A101ContratoGarantia_Codigo ;
      private int[] T000K14_A101ContratoGarantia_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class contratogarantia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000K4 ;
          prmT000K4 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000K5 ;
          prmT000K5 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000K6 ;
          prmT000K6 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000K7 ;
          prmT000K7 = new Object[] {
          new Object[] {"@ContratoGarantia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000K8 ;
          prmT000K8 = new Object[] {
          new Object[] {"@ContratoGarantia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000K3 ;
          prmT000K3 = new Object[] {
          new Object[] {"@ContratoGarantia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000K9 ;
          prmT000K9 = new Object[] {
          new Object[] {"@ContratoGarantia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000K10 ;
          prmT000K10 = new Object[] {
          new Object[] {"@ContratoGarantia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000K2 ;
          prmT000K2 = new Object[] {
          new Object[] {"@ContratoGarantia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000K11 ;
          prmT000K11 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGarantia_DataPagtoGarantia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoGarantia_Percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoGarantia_DataCaucao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoGarantia_ValorGarantia",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratoGarantia_DataEncerramento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoGarantia_ValorEncerramento",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmT000K12 ;
          prmT000K12 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGarantia_DataPagtoGarantia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoGarantia_Percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoGarantia_DataCaucao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoGarantia_ValorGarantia",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratoGarantia_DataEncerramento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoGarantia_ValorEncerramento",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratoGarantia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000K13 ;
          prmT000K13 = new Object[] {
          new Object[] {"@ContratoGarantia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000K14 ;
          prmT000K14 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000K2", "SELECT [Contrato_Codigo], [ContratoGarantia_Codigo], [ContratoGarantia_DataPagtoGarantia], [ContratoGarantia_Percentual], [ContratoGarantia_DataCaucao], [ContratoGarantia_ValorGarantia], [ContratoGarantia_DataEncerramento], [ContratoGarantia_ValorEncerramento] FROM [ContratoGarantia] WITH (UPDLOCK) WHERE [ContratoGarantia_Codigo] = @ContratoGarantia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000K2,1,0,true,false )
             ,new CursorDef("T000K3", "SELECT [Contrato_Codigo], [ContratoGarantia_Codigo], [ContratoGarantia_DataPagtoGarantia], [ContratoGarantia_Percentual], [ContratoGarantia_DataCaucao], [ContratoGarantia_ValorGarantia], [ContratoGarantia_DataEncerramento], [ContratoGarantia_ValorEncerramento] FROM [ContratoGarantia] WITH (NOLOCK) WHERE [ContratoGarantia_Codigo] = @ContratoGarantia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000K3,1,0,true,false )
             ,new CursorDef("T000K4", "SELECT [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000K4,1,0,true,false )
             ,new CursorDef("T000K5", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000K5,1,0,true,false )
             ,new CursorDef("T000K6", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000K6,1,0,true,false )
             ,new CursorDef("T000K7", "SELECT TM1.[Contrato_Codigo], TM1.[ContratoGarantia_Codigo], T2.[Contrato_Numero], T2.[Contrato_NumeroAta], T2.[Contrato_Ano], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, TM1.[ContratoGarantia_DataPagtoGarantia], TM1.[ContratoGarantia_Percentual], TM1.[ContratoGarantia_DataCaucao], TM1.[ContratoGarantia_ValorGarantia], TM1.[ContratoGarantia_DataEncerramento], TM1.[ContratoGarantia_ValorEncerramento], T2.[Contratada_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod FROM ((([ContratoGarantia] TM1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE TM1.[ContratoGarantia_Codigo] = @ContratoGarantia_Codigo and TM1.[Contrato_Codigo] = @Contrato_Codigo ORDER BY TM1.[ContratoGarantia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000K7,100,0,true,false )
             ,new CursorDef("T000K8", "SELECT [ContratoGarantia_Codigo] FROM [ContratoGarantia] WITH (NOLOCK) WHERE [ContratoGarantia_Codigo] = @ContratoGarantia_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000K8,1,0,true,false )
             ,new CursorDef("T000K9", "SELECT TOP 1 [ContratoGarantia_Codigo], [Contrato_Codigo] FROM [ContratoGarantia] WITH (NOLOCK) WHERE ( [ContratoGarantia_Codigo] > @ContratoGarantia_Codigo) and [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoGarantia_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000K9,1,0,true,true )
             ,new CursorDef("T000K10", "SELECT TOP 1 [ContratoGarantia_Codigo], [Contrato_Codigo] FROM [ContratoGarantia] WITH (NOLOCK) WHERE ( [ContratoGarantia_Codigo] < @ContratoGarantia_Codigo) and [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoGarantia_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000K10,1,0,true,true )
             ,new CursorDef("T000K11", "INSERT INTO [ContratoGarantia]([Contrato_Codigo], [ContratoGarantia_DataPagtoGarantia], [ContratoGarantia_Percentual], [ContratoGarantia_DataCaucao], [ContratoGarantia_ValorGarantia], [ContratoGarantia_DataEncerramento], [ContratoGarantia_ValorEncerramento]) VALUES(@Contrato_Codigo, @ContratoGarantia_DataPagtoGarantia, @ContratoGarantia_Percentual, @ContratoGarantia_DataCaucao, @ContratoGarantia_ValorGarantia, @ContratoGarantia_DataEncerramento, @ContratoGarantia_ValorEncerramento); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000K11)
             ,new CursorDef("T000K12", "UPDATE [ContratoGarantia] SET [Contrato_Codigo]=@Contrato_Codigo, [ContratoGarantia_DataPagtoGarantia]=@ContratoGarantia_DataPagtoGarantia, [ContratoGarantia_Percentual]=@ContratoGarantia_Percentual, [ContratoGarantia_DataCaucao]=@ContratoGarantia_DataCaucao, [ContratoGarantia_ValorGarantia]=@ContratoGarantia_ValorGarantia, [ContratoGarantia_DataEncerramento]=@ContratoGarantia_DataEncerramento, [ContratoGarantia_ValorEncerramento]=@ContratoGarantia_ValorEncerramento  WHERE [ContratoGarantia_Codigo] = @ContratoGarantia_Codigo", GxErrorMask.GX_NOMASK,prmT000K12)
             ,new CursorDef("T000K13", "DELETE FROM [ContratoGarantia]  WHERE [ContratoGarantia_Codigo] = @ContratoGarantia_Codigo", GxErrorMask.GX_NOMASK,prmT000K13)
             ,new CursorDef("T000K14", "SELECT [ContratoGarantia_Codigo] FROM [ContratoGarantia] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoGarantia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000K14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(7) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(7) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(8) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(8) ;
                ((decimal[]) buf[11])[0] = rslt.getDecimal(9) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(10) ;
                ((decimal[]) buf[13])[0] = rslt.getDecimal(11) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(12) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(13) ;
                ((int[]) buf[16])[0] = rslt.getInt(14) ;
                ((int[]) buf[17])[0] = rslt.getInt(15) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                stmt.SetParameter(6, (DateTime)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                stmt.SetParameter(6, (DateTime)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                stmt.SetParameter(8, (int)parms[7]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
