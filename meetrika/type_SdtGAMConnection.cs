/*
               File: type_SdtGAMConnection
        Description: GAMConnection
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 3:44:30.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMConnection : GxUserType, IGxExternalObject
   {
      public SdtGAMConnection( )
      {
         initialize();
      }

      public SdtGAMConnection( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMConnection_externalReference == null )
         {
            GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
         }
         returntostring = "";
         returntostring = (String)(GAMConnection_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Name
      {
         get {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            return GAMConnection_externalReference.Name ;
         }

         set {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            GAMConnection_externalReference.Name = value;
         }

      }

      public String gxTpr_Repository
      {
         get {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            return GAMConnection_externalReference.Repository ;
         }

         set {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            GAMConnection_externalReference.Repository = value;
         }

      }

      public String gxTpr_Username
      {
         get {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            return GAMConnection_externalReference.UserName ;
         }

         set {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            GAMConnection_externalReference.UserName = value;
         }

      }

      public String gxTpr_Userpassword
      {
         get {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            return GAMConnection_externalReference.UserPassword ;
         }

         set {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            GAMConnection_externalReference.UserPassword = value;
         }

      }

      public String gxTpr_Type
      {
         get {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            return GAMConnection_externalReference.Type ;
         }

         set {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            GAMConnection_externalReference.Type = value;
         }

      }

      public String gxTpr_Language
      {
         get {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            return GAMConnection_externalReference.Language ;
         }

         set {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            GAMConnection_externalReference.Language = value;
         }

      }

      public IGxCollection gxTpr_Properties
      {
         get {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMConnectionProperties", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMConnectionProperties> externalParm0 ;
            externalParm0 = GAMConnection_externalReference.Properties;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMConnectionProperties>), externalParm0);
            return intValue ;
         }

         set {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMConnectionProperties> externalParm1 ;
            intValue = value;
            externalParm1 = (System.Collections.Generic.List<Artech.Security.GAMConnectionProperties>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMConnectionProperties>), intValue.ExternalInstance);
            GAMConnection_externalReference.Properties = externalParm1;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMConnection_externalReference == null )
            {
               GAMConnection_externalReference = new Artech.Security.GAMConnection(context);
            }
            return GAMConnection_externalReference ;
         }

         set {
            GAMConnection_externalReference = (Artech.Security.GAMConnection)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMConnection GAMConnection_externalReference=null ;
   }

}
