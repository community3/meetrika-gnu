/*
               File: WP_ConsultaSistemas
        Description: Consulta de Sistemas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:46:38.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_consultasistemas : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_consultasistemas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_consultasistemas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavServico_codigo = new GXCombobox();
         cmbavFiltro_ano = new GXCombobox();
         cmbavFiltro_mes = new GXCombobox();
         cmbavCtlmes = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSERVICO_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSERVICO_CODIGOFV2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_20 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_20_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_20_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n52Contratada_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               A489ContagemResultado_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n489ContagemResultado_SistemaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A489ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV42WWPContext);
               A566ContagemResultado_DataUltCnt = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A566ContagemResultado_DataUltCnt", context.localUtil.Format(A566ContagemResultado_DataUltCnt, "99/99/99"));
               AV19Filtro_Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Filtro_Ano), 4, 0)));
               AV21Filtro_Mes = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Filtro_Mes), 10, 0)));
               AV20Filtro_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Filtro_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Filtro_SistemaCod), 6, 0)));
               A601ContagemResultado_Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n601ContagemResultado_Servico = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A601ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0)));
               AV35Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Servico_Codigo), 6, 0)));
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               A509ContagemrResultado_SistemaSigla = GetNextPar( );
               n509ContagemrResultado_SistemaSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A509ContagemrResultado_SistemaSigla", A509ContagemrResultado_SistemaSigla);
               AV45tQtdSistemas = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tQtdSistemas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tQtdSistemas), 6, 0)));
               A684ContagemResultado_PFBFSUltima = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A684ContagemResultado_PFBFSUltima", StringUtil.LTrim( StringUtil.Str( A684ContagemResultado_PFBFSUltima, 14, 5)));
               A682ContagemResultado_PFBFMUltima = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A682ContagemResultado_PFBFMUltima", StringUtil.LTrim( StringUtil.Str( A682ContagemResultado_PFBFMUltima, 14, 5)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV47sdt_Sistema);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV46sdt_ConsultaSistemas);
               AV41tQtdeDmn = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41tQtdeDmn), 8, 0)));
               AV40tPFFinal = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40tPFFinal", StringUtil.LTrim( StringUtil.Str( AV40tPFFinal, 14, 5)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( A52Contratada_AreaTrabalhoCod, A489ContagemResultado_SistemaCod, AV42WWPContext, A566ContagemResultado_DataUltCnt, AV19Filtro_Ano, AV21Filtro_Mes, AV20Filtro_SistemaCod, A601ContagemResultado_Servico, AV35Servico_Codigo, A484ContagemResultado_StatusDmn, A509ContagemrResultado_SistemaSigla, AV45tQtdSistemas, A684ContagemResultado_PFBFSUltima, A682ContagemResultado_PFBFMUltima, AV47sdt_Sistema, AV46sdt_ConsultaSistemas, AV41tQtdeDmn, AV40tPFFinal) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "WP_ConsultaSistemas";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV45tQtdSistemas), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV41tQtdeDmn), "ZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV40tPFFinal, "ZZ,ZZZ,ZZ9.999");
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("wp_consultasistemas:[SendSecurityCheck value for]"+"tQtdSistemas:"+context.localUtil.Format( (decimal)(AV45tQtdSistemas), "ZZZZZ9"));
               GXUtil.WriteLog("wp_consultasistemas:[SendSecurityCheck value for]"+"tQtdeDmn:"+context.localUtil.Format( (decimal)(AV41tQtdeDmn), "ZZZZZZZ9"));
               GXUtil.WriteLog("wp_consultasistemas:[SendSecurityCheck value for]"+"tPFFinal:"+context.localUtil.Format( AV40tPFFinal, "ZZ,ZZZ,ZZ9.999"));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAFV2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTFV2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423463837");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_consultasistemas.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_consultasistemas", AV46sdt_ConsultaSistemas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_consultasistemas", AV46sdt_ConsultaSistemas);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_20", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_20), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A489ContagemResultado_SistemaCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV42WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV42WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATAULTCNT", context.localUtil.DToC( A566ContagemResultado_DataUltCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRRESULTADO_SISTEMASIGLA", StringUtil.RTrim( A509ContagemrResultado_SistemaSigla));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A684ContagemResultado_PFBFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFMULTIMA", StringUtil.LTrim( StringUtil.NToC( A682ContagemResultado_PFBFMUltima, 14, 5, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_SISTEMA", AV47sdt_Sistema);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_SISTEMA", AV47sdt_Sistema);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_CONSULTASISTEMAS", AV46sdt_ConsultaSistemas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_CONSULTASISTEMAS", AV46sdt_ConsultaSistemas);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_FILTROCONSCONTADORFM", AV34SDT_FiltroConsContadorFM);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_FILTROCONSCONTADORFM", AV34SDT_FiltroConsContadorFM);
         }
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Target", StringUtil.RTrim( Innewwindow_Target));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_ConsultaSistemas";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV45tQtdSistemas), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV41tQtdeDmn), "ZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV40tPFFinal, "ZZ,ZZZ,ZZ9.999");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_consultasistemas:[SendSecurityCheck value for]"+"tQtdSistemas:"+context.localUtil.Format( (decimal)(AV45tQtdSistemas), "ZZZZZ9"));
         GXUtil.WriteLog("wp_consultasistemas:[SendSecurityCheck value for]"+"tQtdeDmn:"+context.localUtil.Format( (decimal)(AV41tQtdeDmn), "ZZZZZZZ9"));
         GXUtil.WriteLog("wp_consultasistemas:[SendSecurityCheck value for]"+"tPFFinal:"+context.localUtil.Format( AV40tPFFinal, "ZZ,ZZZ,ZZ9.999"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEFV2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTFV2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_consultasistemas.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_ConsultaSistemas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Consulta de Sistemas" ;
      }

      protected void WBFV0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_FV2( true) ;
         }
         else
         {
            wb_table1_2_FV2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_FV2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOWContainer"+"\"></div>") ;
         }
         wbLoad = true;
      }

      protected void STARTFV2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Consulta de Sistemas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPFV0( ) ;
      }

      protected void WSFV2( )
      {
         STARTFV2( ) ;
         EVTFV2( ) ;
      }

      protected void EVTFV2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11FV2 */
                              E11FV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "CTLQTDDMN.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "CTLQTDDMN.CLICK") == 0 ) )
                           {
                              nGXsfl_20_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_20_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_20_idx), 4, 0)), 4, "0");
                              SubsflControlProps_202( ) ;
                              AV53GXV1 = nGXsfl_20_idx;
                              if ( ( AV46sdt_ConsultaSistemas.Count >= AV53GXV1 ) && ( AV53GXV1 > 0 ) )
                              {
                                 AV46sdt_ConsultaSistemas.CurrentItem = ((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12FV2 */
                                    E12FV2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CTLQTDDMN.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13FV2 */
                                    E13FV2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14FV2 */
                                    E14FV2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEFV2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAFV2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavServico_codigo.Name = "vSERVICO_CODIGO";
            dynavServico_codigo.WebTags = "";
            cmbavFiltro_ano.Name = "vFILTRO_ANO";
            cmbavFiltro_ano.WebTags = "";
            cmbavFiltro_ano.addItem("2014", "2014", 0);
            cmbavFiltro_ano.addItem("2015", "2015", 0);
            if ( cmbavFiltro_ano.ItemCount > 0 )
            {
               AV19Filtro_Ano = (short)(NumberUtil.Val( cmbavFiltro_ano.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV19Filtro_Ano), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Filtro_Ano), 4, 0)));
            }
            cmbavFiltro_mes.Name = "vFILTRO_MES";
            cmbavFiltro_mes.WebTags = "";
            cmbavFiltro_mes.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 10, 0)), "Todos", 0);
            cmbavFiltro_mes.addItem("1", "Janeiro", 0);
            cmbavFiltro_mes.addItem("2", "Fevereiro", 0);
            cmbavFiltro_mes.addItem("3", "Mar�o", 0);
            cmbavFiltro_mes.addItem("4", "Abril", 0);
            cmbavFiltro_mes.addItem("5", "Maio", 0);
            cmbavFiltro_mes.addItem("6", "Junho", 0);
            cmbavFiltro_mes.addItem("7", "Julho", 0);
            cmbavFiltro_mes.addItem("8", "Agosto", 0);
            cmbavFiltro_mes.addItem("9", "Setembro", 0);
            cmbavFiltro_mes.addItem("10", "Outubro", 0);
            cmbavFiltro_mes.addItem("11", "Novembro", 0);
            cmbavFiltro_mes.addItem("12", "Dezembro", 0);
            if ( cmbavFiltro_mes.ItemCount > 0 )
            {
               AV21Filtro_Mes = (long)(NumberUtil.Val( cmbavFiltro_mes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21Filtro_Mes), 10, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Filtro_Mes), 10, 0)));
            }
            GXCCtl = "CTLMES_" + sGXsfl_20_idx;
            cmbavCtlmes.Name = GXCCtl;
            cmbavCtlmes.WebTags = "";
            cmbavCtlmes.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 10, 0)), "Todos", 0);
            cmbavCtlmes.addItem("1", "Janeiro", 0);
            cmbavCtlmes.addItem("2", "Fevereiro", 0);
            cmbavCtlmes.addItem("3", "Mar�o", 0);
            cmbavCtlmes.addItem("4", "Abril", 0);
            cmbavCtlmes.addItem("5", "Maio", 0);
            cmbavCtlmes.addItem("6", "Junho", 0);
            cmbavCtlmes.addItem("7", "Julho", 0);
            cmbavCtlmes.addItem("8", "Agosto", 0);
            cmbavCtlmes.addItem("9", "Setembro", 0);
            cmbavCtlmes.addItem("10", "Outubro", 0);
            cmbavCtlmes.addItem("11", "Novembro", 0);
            cmbavCtlmes.addItem("12", "Dezembro", 0);
            if ( cmbavCtlmes.ItemCount > 0 )
            {
               if ( ( AV53GXV1 > 0 ) && ( AV46sdt_ConsultaSistemas.Count >= AV53GXV1 ) && (0==((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Mes) )
               {
                  ((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Mes = (long)(NumberUtil.Val( cmbavCtlmes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Mes), 10, 0))), "."));
               }
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavFiltro_sistemacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvSERVICO_CODIGOFV2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICO_CODIGO_dataFV2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICO_CODIGO_htmlFV2( )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICO_CODIGO_dataFV2( ) ;
         gxdynajaxindex = 1;
         dynavServico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV35Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Servico_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSERVICO_CODIGO_dataFV2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Todos");
         /* Using cursor H00FV2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00FV2_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00FV2_A605Servico_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_202( ) ;
         while ( nGXsfl_20_idx <= nRC_GXsfl_20 )
         {
            sendrow_202( ) ;
            nGXsfl_20_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_20_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_20_idx+1));
            sGXsfl_20_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_20_idx), 4, 0)), 4, "0");
            SubsflControlProps_202( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int A52Contratada_AreaTrabalhoCod ,
                                       int A489ContagemResultado_SistemaCod ,
                                       wwpbaseobjects.SdtWWPContext AV42WWPContext ,
                                       DateTime A566ContagemResultado_DataUltCnt ,
                                       short AV19Filtro_Ano ,
                                       long AV21Filtro_Mes ,
                                       int AV20Filtro_SistemaCod ,
                                       int A601ContagemResultado_Servico ,
                                       int AV35Servico_Codigo ,
                                       String A484ContagemResultado_StatusDmn ,
                                       String A509ContagemrResultado_SistemaSigla ,
                                       int AV45tQtdSistemas ,
                                       decimal A684ContagemResultado_PFBFSUltima ,
                                       decimal A682ContagemResultado_PFBFMUltima ,
                                       SdtSDT_ConsultaSistemas_Sistema AV47sdt_Sistema ,
                                       IGxCollection AV46sdt_ConsultaSistemas ,
                                       int AV41tQtdeDmn ,
                                       decimal AV40tPFFinal )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFFV2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV35Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Servico_Codigo), 6, 0)));
         }
         if ( cmbavFiltro_ano.ItemCount > 0 )
         {
            AV19Filtro_Ano = (short)(NumberUtil.Val( cmbavFiltro_ano.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV19Filtro_Ano), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Filtro_Ano), 4, 0)));
         }
         if ( cmbavFiltro_mes.ItemCount > 0 )
         {
            AV21Filtro_Mes = (long)(NumberUtil.Val( cmbavFiltro_mes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21Filtro_Mes), 10, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Filtro_Mes), 10, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFFV2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavCtlsigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlsigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlsigla_Enabled), 5, 0)));
         cmbavCtlmes.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlmes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlmes.Enabled), 5, 0)));
         edtavCtlano_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlano_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlano_Enabled), 5, 0)));
         edtavCtlqtddmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlqtddmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlqtddmn_Enabled), 5, 0)));
         edtavCtlpffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlpffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlpffinal_Enabled), 5, 0)));
         edtavTqtdsistemas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTqtdsistemas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTqtdsistemas_Enabled), 5, 0)));
         edtavTqtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTqtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTqtdedmn_Enabled), 5, 0)));
         edtavTpffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpffinal_Enabled), 5, 0)));
      }

      protected void RFFV2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 20;
         /* Execute user event: E11FV2 */
         E11FV2 ();
         nGXsfl_20_idx = 1;
         sGXsfl_20_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_20_idx), 4, 0)), 4, "0");
         SubsflControlProps_202( ) ;
         nGXsfl_20_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "Grid");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Width), 9, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_202( ) ;
            /* Execute user event: E14FV2 */
            E14FV2 ();
            wbEnd = 20;
            WBFV0( ) ;
         }
         nGXsfl_20_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPFV0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavCtlsigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlsigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlsigla_Enabled), 5, 0)));
         cmbavCtlmes.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlmes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCtlmes.Enabled), 5, 0)));
         edtavCtlano_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlano_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlano_Enabled), 5, 0)));
         edtavCtlqtddmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlqtddmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlqtddmn_Enabled), 5, 0)));
         edtavCtlpffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlpffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlpffinal_Enabled), 5, 0)));
         edtavTqtdsistemas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTqtdsistemas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTqtdsistemas_Enabled), 5, 0)));
         edtavTqtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTqtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTqtdedmn_Enabled), 5, 0)));
         edtavTpffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpffinal_Enabled), 5, 0)));
         GXVvSERVICO_CODIGO_htmlFV2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12FV2 */
         E12FV2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_consultasistemas"), AV46sdt_ConsultaSistemas);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFiltro_sistemacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFiltro_sistemacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFILTRO_SISTEMACOD");
               GX_FocusControl = edtavFiltro_sistemacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20Filtro_SistemaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Filtro_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Filtro_SistemaCod), 6, 0)));
            }
            else
            {
               AV20Filtro_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtavFiltro_sistemacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Filtro_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Filtro_SistemaCod), 6, 0)));
            }
            dynavServico_codigo.Name = dynavServico_codigo_Internalname;
            dynavServico_codigo.CurrentValue = cgiGet( dynavServico_codigo_Internalname);
            AV35Servico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Servico_Codigo), 6, 0)));
            cmbavFiltro_ano.Name = cmbavFiltro_ano_Internalname;
            cmbavFiltro_ano.CurrentValue = cgiGet( cmbavFiltro_ano_Internalname);
            AV19Filtro_Ano = (short)(NumberUtil.Val( cgiGet( cmbavFiltro_ano_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Filtro_Ano), 4, 0)));
            cmbavFiltro_mes.Name = cmbavFiltro_mes_Internalname;
            cmbavFiltro_mes.CurrentValue = cgiGet( cmbavFiltro_mes_Internalname);
            AV21Filtro_Mes = (long)(NumberUtil.Val( cgiGet( cmbavFiltro_mes_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Filtro_Mes), 10, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTqtdsistemas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTqtdsistemas_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTQTDSISTEMAS");
               GX_FocusControl = edtavTqtdsistemas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45tQtdSistemas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tQtdSistemas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tQtdSistemas), 6, 0)));
            }
            else
            {
               AV45tQtdSistemas = (int)(context.localUtil.CToN( cgiGet( edtavTqtdsistemas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tQtdSistemas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tQtdSistemas), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTQTDEDMN");
               GX_FocusControl = edtavTqtdedmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41tQtdeDmn = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41tQtdeDmn), 8, 0)));
            }
            else
            {
               AV41tQtdeDmn = (int)(context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41tQtdeDmn), 8, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTpffinal_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTpffinal_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTPFFINAL");
               GX_FocusControl = edtavTpffinal_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40tPFFinal = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40tPFFinal", StringUtil.LTrim( StringUtil.Str( AV40tPFFinal, 14, 5)));
            }
            else
            {
               AV40tPFFinal = context.localUtil.CToN( cgiGet( edtavTpffinal_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40tPFFinal", StringUtil.LTrim( StringUtil.Str( AV40tPFFinal, 14, 5)));
            }
            /* Read saved values. */
            nRC_GXsfl_20 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_20"), ",", "."));
            Innewwindow_Target = cgiGet( "INNEWWINDOW_Target");
            nRC_GXsfl_20 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_20"), ",", "."));
            nGXsfl_20_fel_idx = 0;
            while ( nGXsfl_20_fel_idx < nRC_GXsfl_20 )
            {
               nGXsfl_20_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_20_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_20_fel_idx+1));
               sGXsfl_20_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_20_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_202( ) ;
               AV53GXV1 = nGXsfl_20_fel_idx;
               if ( ( AV46sdt_ConsultaSistemas.Count >= AV53GXV1 ) && ( AV53GXV1 > 0 ) )
               {
                  AV46sdt_ConsultaSistemas.CurrentItem = ((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1));
               }
            }
            if ( nGXsfl_20_fel_idx == 0 )
            {
               nGXsfl_20_idx = 1;
               sGXsfl_20_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_20_idx), 4, 0)), 4, "0");
               SubsflControlProps_202( ) ;
            }
            nGXsfl_20_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_ConsultaSistemas";
            AV45tQtdSistemas = (int)(context.localUtil.CToN( cgiGet( edtavTqtdsistemas_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tQtdSistemas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tQtdSistemas), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV45tQtdSistemas), "ZZZZZ9");
            AV41tQtdeDmn = (int)(context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41tQtdeDmn), 8, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV41tQtdeDmn), "ZZZZZZZ9");
            AV40tPFFinal = context.localUtil.CToN( cgiGet( edtavTpffinal_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40tPFFinal", StringUtil.LTrim( StringUtil.Str( AV40tPFFinal, 14, 5)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV40tPFFinal, "ZZ,ZZZ,ZZ9.999");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_consultasistemas:[SecurityCheckFailed value for]"+"tQtdSistemas:"+context.localUtil.Format( (decimal)(AV45tQtdSistemas), "ZZZZZ9"));
               GXUtil.WriteLog("wp_consultasistemas:[SecurityCheckFailed value for]"+"tQtdeDmn:"+context.localUtil.Format( (decimal)(AV41tQtdeDmn), "ZZZZZZZ9"));
               GXUtil.WriteLog("wp_consultasistemas:[SecurityCheckFailed value for]"+"tPFFinal:"+context.localUtil.Format( AV40tPFFinal, "ZZ,ZZZ,ZZ9.999"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12FV2 */
         E12FV2 ();
         if (returnInSub) return;
      }

      protected void E12FV2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV42WWPContext) ;
         AV19Filtro_Ano = (short)(DateTimeUtil.Year( Gx_date));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Filtro_Ano), 4, 0)));
         AV21Filtro_Mes = DateTimeUtil.Month( Gx_date);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Filtro_Mes), 10, 0)));
         if ( DateTimeUtil.Year( Gx_date) > 2015 )
         {
            AV22i = 2016;
            while ( AV22i <= DateTimeUtil.Year( Gx_date) )
            {
               cmbavFiltro_ano.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV22i), 4, 0)), StringUtil.Str( (decimal)(AV22i), 4, 0), 0);
               AV22i = (short)(AV22i+1);
            }
         }
         edtavCtlqtddmn_Tooltiptext = "Visualizar demandas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlqtddmn_Internalname, "Tooltiptext", edtavCtlqtddmn_Tooltiptext);
      }

      protected void E11FV2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV45tQtdSistemas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tQtdSistemas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tQtdSistemas), 6, 0)));
         AV41tQtdeDmn = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41tQtdeDmn), 8, 0)));
         AV40tPFFinal = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40tPFFinal", StringUtil.LTrim( StringUtil.Str( AV40tPFFinal, 14, 5)));
         AV46sdt_ConsultaSistemas.Clear();
         gx_BV20 = true;
         /* Execute user subroutine: 'CARREGASDT' */
         S112 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46sdt_ConsultaSistemas", AV46sdt_ConsultaSistemas);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47sdt_Sistema", AV47sdt_Sistema);
      }

      protected void S112( )
      {
         /* 'CARREGASDT' Routine */
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV20Filtro_SistemaCod ,
                                              AV35Servico_Codigo ,
                                              A489ContagemResultado_SistemaCod ,
                                              A601ContagemResultado_Servico ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV19Filtro_Ano ,
                                              AV21Filtro_Mes ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV42WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A52Contratada_AreaTrabalhoCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.LONG, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00FV4 */
         pr_default.execute(1, new Object[] {AV42WWPContext.gxTpr_Areatrabalho_codigo, AV19Filtro_Ano, AV21Filtro_Mes, AV21Filtro_Mes, AV20Filtro_SistemaCod, AV35Servico_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKFV3 = false;
            A146Modulo_Codigo = H00FV4_A146Modulo_Codigo[0];
            n146Modulo_Codigo = H00FV4_n146Modulo_Codigo[0];
            A127Sistema_Codigo = H00FV4_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = H00FV4_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = H00FV4_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = H00FV4_n830AreaTrabalho_ServicoPadrao[0];
            A490ContagemResultado_ContratadaCod = H00FV4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00FV4_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = H00FV4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00FV4_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = H00FV4_A456ContagemResultado_Codigo[0];
            A52Contratada_AreaTrabalhoCod = H00FV4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00FV4_n52Contratada_AreaTrabalhoCod[0];
            A489ContagemResultado_SistemaCod = H00FV4_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00FV4_n489ContagemResultado_SistemaCod[0];
            A632Servico_Ativo = H00FV4_A632Servico_Ativo[0];
            n632Servico_Ativo = H00FV4_n632Servico_Ativo[0];
            A484ContagemResultado_StatusDmn = H00FV4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00FV4_n484ContagemResultado_StatusDmn[0];
            A601ContagemResultado_Servico = H00FV4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00FV4_n601ContagemResultado_Servico[0];
            A509ContagemrResultado_SistemaSigla = H00FV4_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00FV4_n509ContagemrResultado_SistemaSigla[0];
            A566ContagemResultado_DataUltCnt = H00FV4_A566ContagemResultado_DataUltCnt[0];
            A682ContagemResultado_PFBFMUltima = H00FV4_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = H00FV4_A684ContagemResultado_PFBFSUltima[0];
            A127Sistema_Codigo = H00FV4_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = H00FV4_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = H00FV4_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = H00FV4_n830AreaTrabalho_ServicoPadrao[0];
            A632Servico_Ativo = H00FV4_A632Servico_Ativo[0];
            n632Servico_Ativo = H00FV4_n632Servico_Ativo[0];
            A52Contratada_AreaTrabalhoCod = H00FV4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00FV4_n52Contratada_AreaTrabalhoCod[0];
            A601ContagemResultado_Servico = H00FV4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00FV4_n601ContagemResultado_Servico[0];
            A566ContagemResultado_DataUltCnt = H00FV4_A566ContagemResultado_DataUltCnt[0];
            A682ContagemResultado_PFBFMUltima = H00FV4_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = H00FV4_A684ContagemResultado_PFBFSUltima[0];
            A509ContagemrResultado_SistemaSigla = H00FV4_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00FV4_n509ContagemrResultado_SistemaSigla[0];
            AV49Sistema_Codigo = A489ContagemResultado_SistemaCod;
            AV50Sistema_Sigla = A509ContagemrResultado_SistemaSigla;
            AV45tQtdSistemas = (int)(AV45tQtdSistemas+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45tQtdSistemas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45tQtdSistemas), 6, 0)));
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV28mQtdeDmn[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV27mPFFinal[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            while ( (pr_default.getStatus(1) != 101) && ( H00FV4_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( H00FV4_A489ContagemResultado_SistemaCod[0] == A489ContagemResultado_SistemaCod ) )
            {
               BRKFV3 = false;
               A146Modulo_Codigo = H00FV4_A146Modulo_Codigo[0];
               n146Modulo_Codigo = H00FV4_n146Modulo_Codigo[0];
               A490ContagemResultado_ContratadaCod = H00FV4_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00FV4_n490ContagemResultado_ContratadaCod[0];
               A1553ContagemResultado_CntSrvCod = H00FV4_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00FV4_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = H00FV4_A456ContagemResultado_Codigo[0];
               A484ContagemResultado_StatusDmn = H00FV4_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00FV4_n484ContagemResultado_StatusDmn[0];
               /* Using cursor H00FV6 */
               pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
               if ( (pr_default.getStatus(2) != 101) )
               {
                  A566ContagemResultado_DataUltCnt = H00FV6_A566ContagemResultado_DataUltCnt[0];
                  A682ContagemResultado_PFBFMUltima = H00FV6_A682ContagemResultado_PFBFMUltima[0];
                  A684ContagemResultado_PFBFSUltima = H00FV6_A684ContagemResultado_PFBFSUltima[0];
               }
               else
               {
                  A566ContagemResultado_DataUltCnt = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A566ContagemResultado_DataUltCnt", context.localUtil.Format(A566ContagemResultado_DataUltCnt, "99/99/99"));
                  A682ContagemResultado_PFBFMUltima = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A682ContagemResultado_PFBFMUltima", StringUtil.LTrim( StringUtil.Str( A682ContagemResultado_PFBFMUltima, 14, 5)));
                  A684ContagemResultado_PFBFSUltima = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A684ContagemResultado_PFBFSUltima", StringUtil.LTrim( StringUtil.Str( A684ContagemResultado_PFBFSUltima, 14, 5)));
               }
               pr_default.close(2);
               AV22i = (short)(DateTimeUtil.Month( A566ContagemResultado_DataUltCnt));
               AV28mQtdeDmn[AV22i-1] = (int)(AV28mQtdeDmn[AV22i-1]+1);
               AV44Contagem_PFFinal = ((A684ContagemResultado_PFBFSUltima<A682ContagemResultado_PFBFMUltima) ? A684ContagemResultado_PFBFSUltima : A682ContagemResultado_PFBFMUltima);
               AV27mPFFinal[AV22i-1] = (decimal)(AV27mPFFinal[AV22i-1]+AV44Contagem_PFFinal);
               BRKFV3 = true;
               pr_default.readNext(1);
            }
            AV22i = 1;
            while ( AV22i <= 12 )
            {
               if ( AV28mQtdeDmn[AV22i-1] > 0 )
               {
                  AV47sdt_Sistema.gxTpr_Mes = AV22i;
                  AV47sdt_Sistema.gxTpr_Codigo = AV49Sistema_Codigo;
                  AV47sdt_Sistema.gxTpr_Sigla = AV50Sistema_Sigla;
                  AV47sdt_Sistema.gxTpr_Ano = AV19Filtro_Ano;
                  AV47sdt_Sistema.gxTpr_Qtddmn = AV28mQtdeDmn[AV22i-1];
                  AV47sdt_Sistema.gxTpr_Pffinal = AV27mPFFinal[AV22i-1];
                  AV46sdt_ConsultaSistemas.Add(AV47sdt_Sistema, 0);
                  gx_BV20 = true;
                  AV47sdt_Sistema = new SdtSDT_ConsultaSistemas_Sistema(context);
                  AV41tQtdeDmn = (int)(AV41tQtdeDmn+(AV28mQtdeDmn[AV22i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41tQtdeDmn), 8, 0)));
                  AV40tPFFinal = (decimal)(AV40tPFFinal+(AV27mPFFinal[AV22i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40tPFFinal", StringUtil.LTrim( StringUtil.Str( AV40tPFFinal, 14, 5)));
               }
               AV22i = (short)(AV22i+1);
            }
            if ( ! BRKFV3 )
            {
               BRKFV3 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
         AV46sdt_ConsultaSistemas.Sort("[PFFinal]");
         gx_BV20 = true;
      }

      protected void E13FV2( )
      {
         AV53GXV1 = nGXsfl_20_idx;
         if ( AV46sdt_ConsultaSistemas.Count >= AV53GXV1 )
         {
            AV46sdt_ConsultaSistemas.CurrentItem = ((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1));
         }
         /* Ctlqtddmn_Click Routine */
         AV34SDT_FiltroConsContadorFM.gxTpr_Areatrabalho_codigo = AV42WWPContext.gxTpr_Areatrabalho_codigo;
         AV34SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod = 0;
         AV34SDT_FiltroConsContadorFM.gxTpr_Mes = ((SdtSDT_ConsultaSistemas_Sistema)(AV46sdt_ConsultaSistemas.CurrentItem)).gxTpr_Mes;
         AV34SDT_FiltroConsContadorFM.gxTpr_Ano = AV19Filtro_Ano;
         AV34SDT_FiltroConsContadorFM.gxTpr_Comerro = false;
         AV34SDT_FiltroConsContadorFM.gxTpr_Abertas = false;
         AV34SDT_FiltroConsContadorFM.gxTpr_Solicitadas = false;
         AV34SDT_FiltroConsContadorFM.gxTpr_Soconfirmadas = true;
         AV34SDT_FiltroConsContadorFM.gxTpr_Servico = AV35Servico_Codigo;
         AV34SDT_FiltroConsContadorFM.gxTpr_Sistema = ((SdtSDT_ConsultaSistemas_Sistema)(AV46sdt_ConsultaSistemas.CurrentItem)).gxTpr_Codigo;
         AV34SDT_FiltroConsContadorFM.gxTpr_Nome = ((SdtSDT_ConsultaSistemas_Sistema)(AV46sdt_ConsultaSistemas.CurrentItem)).gxTpr_Sigla;
         AV5WebSession.Set("FiltroConsultaContador", AV34SDT_FiltroConsContadorFM.ToXml(false, true, "SDT_FiltroConsContadorFM", "GxEv3Up14_Meetrika"));
         Innewwindow_Target = formatLink("wwcontagemresultado.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34SDT_FiltroConsContadorFM", AV34SDT_FiltroConsContadorFM);
      }

      private void E14FV2( )
      {
         /* Grid_Load Routine */
         AV53GXV1 = 1;
         while ( AV53GXV1 <= AV46sdt_ConsultaSistemas.Count )
         {
            AV46sdt_ConsultaSistemas.CurrentItem = ((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 20;
            }
            sendrow_202( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_20_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(20, GridRow);
            }
            AV53GXV1 = (short)(AV53GXV1+1);
         }
      }

      protected void wb_table1_2_FV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable4_Internalname, tblTable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:220px")+"\" class='GridHeaderCell'>") ;
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadotitle_Internalname, "Consulta de Sistemas", "", "", lblContagemresultadotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_ConsultaSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            context.WriteHtmlText( "&nbsp;&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Sistema:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaSistemas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_20_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltro_sistemacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20Filtro_SistemaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV20Filtro_SistemaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,10);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltro_sistemacod_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ConsultaSistemas.htm");
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Servi�o:", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaSistemas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'" + sGXsfl_20_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServico_codigo, dynavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV35Servico_Codigo), 6, 0)), 1, dynavServico_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,12);\"", "", true, "HLP_WP_ConsultaSistemas.htm");
            dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_codigo_Internalname, "Values", (String)(dynavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Ano:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaSistemas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'" + sGXsfl_20_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltro_ano, cmbavFiltro_ano_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV19Filtro_Ano), 4, 0)), 1, cmbavFiltro_ano_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,14);\"", "", true, "HLP_WP_ConsultaSistemas.htm");
            cmbavFiltro_ano.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV19Filtro_Ano), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltro_ano_Internalname, "Values", (String)(cmbavFiltro_ano.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Mes:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaSistemas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_20_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltro_mes, cmbavFiltro_mes_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21Filtro_Mes), 10, 0)), 1, cmbavFiltro_mes_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "", true, "HLP_WP_ConsultaSistemas.htm");
            cmbavFiltro_mes.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21Filtro_Mes), 10, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltro_mes_Internalname, "Values", (String)(cmbavFiltro_mes.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(20), 2, 0)+","+"null"+");", "Atualizar", bttButton1_Jsonclick, 5, "Atualizar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EREFRESH."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ConsultaSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"20\">") ;
               sStyleString = "";
               sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(700), 10, 0)) + "px" + ";";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Sigla") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Mes") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Ano") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Qtd Dmn") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PFFinal") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "Grid");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Width), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlsigla_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavCtlmes.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlano_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlqtddmn_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavCtlqtddmn_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlpffinal_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 20 )
         {
            wbEnd = 0;
            nRC_GXsfl_20 = (short)(nGXsfl_20_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV53GXV1 = nGXsfl_20_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\" colspan=\"3\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table2_28_FV2( true) ;
         }
         else
         {
            wb_table2_28_FV2( false) ;
         }
         return  ;
      }

      protected void wb_table2_28_FV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_FV2e( true) ;
         }
         else
         {
            wb_table1_2_FV2e( false) ;
         }
      }

      protected void wb_table2_28_FV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfstotal_Internalname, "Sistemas: ", "", "", lblTextblockcontagemresultado_pflfstotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_20_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTqtdsistemas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45tQtdSistemas), 6, 0, ",", "")), ((edtavTqtdsistemas_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45tQtdSistemas), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV45tQtdSistemas), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTqtdsistemas_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTqtdsistemas_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ConsultaSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfmtotal2_Internalname, " - Demandas: ", "", "", lblTextblockcontagemresultado_pfbfmtotal2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_20_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTqtdedmn_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41tQtdeDmn), 8, 0, ",", "")), ((edtavTqtdedmn_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV41tQtdeDmn), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV41tQtdeDmn), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTqtdedmn_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTqtdedmn_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ConsultaSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal_Internalname, " - PF Final: ", "", "", lblTextblockcontagemresultado_pflfmtotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_20_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpffinal_Internalname, StringUtil.LTrim( StringUtil.NToC( AV40tPFFinal, 14, 5, ",", "")), ((edtavTpffinal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV40tPFFinal, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV40tPFFinal, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpffinal_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTpffinal_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_ConsultaSistemas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_28_FV2e( true) ;
         }
         else
         {
            wb_table2_28_FV2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAFV2( ) ;
         WSFV2( ) ;
         WEFV2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423463925");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_consultasistemas.js", "?202032423463926");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_202( )
      {
         edtavCtlsigla_Internalname = "CTLSIGLA_"+sGXsfl_20_idx;
         cmbavCtlmes_Internalname = "CTLMES_"+sGXsfl_20_idx;
         edtavCtlano_Internalname = "CTLANO_"+sGXsfl_20_idx;
         edtavCtlqtddmn_Internalname = "CTLQTDDMN_"+sGXsfl_20_idx;
         edtavCtlpffinal_Internalname = "CTLPFFINAL_"+sGXsfl_20_idx;
      }

      protected void SubsflControlProps_fel_202( )
      {
         edtavCtlsigla_Internalname = "CTLSIGLA_"+sGXsfl_20_fel_idx;
         cmbavCtlmes_Internalname = "CTLMES_"+sGXsfl_20_fel_idx;
         edtavCtlano_Internalname = "CTLANO_"+sGXsfl_20_fel_idx;
         edtavCtlqtddmn_Internalname = "CTLQTDDMN_"+sGXsfl_20_fel_idx;
         edtavCtlpffinal_Internalname = "CTLPFFINAL_"+sGXsfl_20_fel_idx;
      }

      protected void sendrow_202( )
      {
         SubsflControlProps_202( ) ;
         WBFV0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xF0F0F0);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_20_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xE5E5E5);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xF0F0F0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_20_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlsigla_Internalname,StringUtil.RTrim( ((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Sigla),StringUtil.RTrim( context.localUtil.Format( ((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlsigla_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlsigla_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)20,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         if ( ( nGXsfl_20_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "CTLMES_" + sGXsfl_20_idx;
            cmbavCtlmes.Name = GXCCtl;
            cmbavCtlmes.WebTags = "";
            cmbavCtlmes.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 10, 0)), "Todos", 0);
            cmbavCtlmes.addItem("1", "Janeiro", 0);
            cmbavCtlmes.addItem("2", "Fevereiro", 0);
            cmbavCtlmes.addItem("3", "Mar�o", 0);
            cmbavCtlmes.addItem("4", "Abril", 0);
            cmbavCtlmes.addItem("5", "Maio", 0);
            cmbavCtlmes.addItem("6", "Junho", 0);
            cmbavCtlmes.addItem("7", "Julho", 0);
            cmbavCtlmes.addItem("8", "Agosto", 0);
            cmbavCtlmes.addItem("9", "Setembro", 0);
            cmbavCtlmes.addItem("10", "Outubro", 0);
            cmbavCtlmes.addItem("11", "Novembro", 0);
            cmbavCtlmes.addItem("12", "Dezembro", 0);
            if ( cmbavCtlmes.ItemCount > 0 )
            {
               if ( ( AV53GXV1 > 0 ) && ( AV46sdt_ConsultaSistemas.Count >= AV53GXV1 ) && (0==((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Mes) )
               {
                  ((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Mes = (long)(NumberUtil.Val( cmbavCtlmes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Mes), 10, 0))), "."));
               }
            }
         }
         /* ComboBox */
         GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavCtlmes,(String)cmbavCtlmes_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Mes), 10, 0)),(short)1,(String)cmbavCtlmes_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavCtlmes.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
         cmbavCtlmes.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Mes), 10, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlmes_Internalname, "Values", (String)(cmbavCtlmes.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlano_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Ano), 4, 0, ",", "")),((edtavCtlano_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Ano), "ZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Ano), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlano_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlano_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)20,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlqtddmn_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Qtddmn), 6, 0, ",", "")),((edtavCtlqtddmn_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Qtddmn), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Qtddmn), "ZZZZZ9")),(String)"","'"+""+"'"+",false,"+"'"+"ECTLQTDDMN.CLICK."+sGXsfl_20_idx+"'",(String)"",(String)"",(String)edtavCtlqtddmn_Tooltiptext,(String)"",(String)edtavCtlqtddmn_Jsonclick,(short)5,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlqtddmn_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)20,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlpffinal_Internalname,StringUtil.LTrim( StringUtil.NToC( ((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Pffinal, 14, 5, ",", "")),((edtavCtlpffinal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( ((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Pffinal, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( ((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Pffinal, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlpffinal_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlpffinal_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)20,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         GxWebStd.gx_hidden_field( context, "gxhash_CTLSIGLA"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, StringUtil.RTrim( context.localUtil.Format( ((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_CTLMES"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Mes), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_CTLANO"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Ano), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_CTLQTDDMN"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Qtddmn), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_CTLPFFINAL"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( ((SdtSDT_ConsultaSistemas_Sistema)AV46sdt_ConsultaSistemas.Item(AV53GXV1)).gxTpr_Pffinal, "ZZ,ZZZ,ZZ9.999")));
         GridContainer.AddRow(GridRow);
         nGXsfl_20_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_20_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_20_idx+1));
         sGXsfl_20_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_20_idx), 4, 0)), 4, "0");
         SubsflControlProps_202( ) ;
         /* End function sendrow_202 */
      }

      protected void init_default_properties( )
      {
         lblContagemresultadotitle_Internalname = "CONTAGEMRESULTADOTITLE";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavFiltro_sistemacod_Internalname = "vFILTRO_SISTEMACOD";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         dynavServico_codigo_Internalname = "vSERVICO_CODIGO";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         cmbavFiltro_ano_Internalname = "vFILTRO_ANO";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         cmbavFiltro_mes_Internalname = "vFILTRO_MES";
         bttButton1_Internalname = "BUTTON1";
         edtavCtlsigla_Internalname = "CTLSIGLA";
         cmbavCtlmes_Internalname = "CTLMES";
         edtavCtlano_Internalname = "CTLANO";
         edtavCtlqtddmn_Internalname = "CTLQTDDMN";
         edtavCtlpffinal_Internalname = "CTLPFFINAL";
         lblTextblockcontagemresultado_pflfstotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFSTOTAL";
         edtavTqtdsistemas_Internalname = "vTQTDSISTEMAS";
         lblTextblockcontagemresultado_pfbfmtotal2_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFBFMTOTAL2";
         edtavTqtdedmn_Internalname = "vTQTDEDMN";
         lblTextblockcontagemresultado_pflfmtotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL";
         edtavTpffinal_Internalname = "vTPFFINAL";
         tblTable3_Internalname = "TABLE3";
         tblTable4_Internalname = "TABLE4";
         Innewwindow_Internalname = "INNEWWINDOW";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavCtlpffinal_Jsonclick = "";
         edtavCtlqtddmn_Jsonclick = "";
         edtavCtlano_Jsonclick = "";
         cmbavCtlmes_Jsonclick = "";
         edtavCtlsigla_Jsonclick = "";
         edtavTpffinal_Jsonclick = "";
         edtavTpffinal_Enabled = 1;
         edtavTqtdedmn_Jsonclick = "";
         edtavTqtdedmn_Enabled = 1;
         edtavTqtdsistemas_Jsonclick = "";
         edtavTqtdsistemas_Enabled = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavCtlpffinal_Enabled = 0;
         edtavCtlqtddmn_Tooltiptext = "";
         edtavCtlqtddmn_Enabled = 0;
         edtavCtlano_Enabled = 0;
         cmbavCtlmes.Enabled = 0;
         edtavCtlsigla_Enabled = 0;
         subGrid_Class = "Grid";
         cmbavFiltro_mes_Jsonclick = "";
         cmbavFiltro_ano_Jsonclick = "";
         dynavServico_codigo_Jsonclick = "";
         edtavFiltro_sistemacod_Jsonclick = "";
         edtavCtlqtddmn_Tooltiptext = "";
         subGrid_Width = 700;
         subGrid_Sortable = -1;
         subGrid_Titleforecolor = (int)(0x000000);
         subGrid_Backcolorstyle = 3;
         edtavCtlpffinal_Enabled = -1;
         edtavCtlqtddmn_Enabled = -1;
         edtavCtlano_Enabled = -1;
         cmbavCtlmes.Enabled = -1;
         edtavCtlsigla_Enabled = -1;
         Innewwindow_Target = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Consulta de Sistemas";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV42WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A566ContagemResultado_DataUltCnt',fld:'CONTAGEMRESULTADO_DATAULTCNT',pic:'',nv:''},{av:'AV19Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0},{av:'AV21Filtro_Mes',fld:'vFILTRO_MES',pic:'ZZZZZZZZZ9',nv:0},{av:'AV20Filtro_SistemaCod',fld:'vFILTRO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV35Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'AV45tQtdSistemas',fld:'vTQTDSISTEMAS',pic:'ZZZZZ9',nv:0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV47sdt_Sistema',fld:'vSDT_SISTEMA',pic:'',nv:null},{av:'AV46sdt_ConsultaSistemas',fld:'vSDT_CONSULTASISTEMAS',grid:20,pic:'',nv:null},{av:'AV41tQtdeDmn',fld:'vTQTDEDMN',pic:'ZZZZZZZ9',nv:0},{av:'AV40tPFFinal',fld:'vTPFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV45tQtdSistemas',fld:'vTQTDSISTEMAS',pic:'ZZZZZ9',nv:0},{av:'AV41tQtdeDmn',fld:'vTQTDEDMN',pic:'ZZZZZZZ9',nv:0},{av:'AV40tPFFinal',fld:'vTPFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV46sdt_ConsultaSistemas',fld:'vSDT_CONSULTASISTEMAS',grid:20,pic:'',nv:null},{av:'AV47sdt_Sistema',fld:'vSDT_SISTEMA',pic:'',nv:null}]}");
         setEventMetadata("CTLQTDDMN.CLICK","{handler:'E13FV2',iparms:[{av:'AV42WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV34SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'AV46sdt_ConsultaSistemas',fld:'vSDT_CONSULTASISTEMAS',grid:20,pic:'',nv:null},{av:'AV19Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0},{av:'AV35Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV34SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(2);
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV42WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         A509ContagemrResultado_SistemaSigla = "";
         AV47sdt_Sistema = new SdtSDT_ConsultaSistemas_Sistema(context);
         AV46sdt_ConsultaSistemas = new GxObjectCollection( context, "SDT_ConsultaSistemas.Sistema", "GxEv3Up14_Meetrika", "SdtSDT_ConsultaSistemas_Sistema", "GeneXus.Programs");
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV34SDT_FiltroConsContadorFM = new SdtSDT_FiltroConsContadorFM(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00FV2_A155Servico_Codigo = new int[1] ;
         H00FV2_A605Servico_Sigla = new String[] {""} ;
         H00FV2_A631Servico_Vinculado = new int[1] ;
         H00FV2_n631Servico_Vinculado = new bool[] {false} ;
         H00FV2_A632Servico_Ativo = new bool[] {false} ;
         H00FV2_n632Servico_Ativo = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         Gx_date = DateTime.MinValue;
         hsh = "";
         H00FV4_A146Modulo_Codigo = new int[1] ;
         H00FV4_n146Modulo_Codigo = new bool[] {false} ;
         H00FV4_A127Sistema_Codigo = new int[1] ;
         H00FV4_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00FV4_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         H00FV4_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         H00FV4_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00FV4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00FV4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00FV4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00FV4_A456ContagemResultado_Codigo = new int[1] ;
         H00FV4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00FV4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00FV4_A489ContagemResultado_SistemaCod = new int[1] ;
         H00FV4_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00FV4_A632Servico_Ativo = new bool[] {false} ;
         H00FV4_n632Servico_Ativo = new bool[] {false} ;
         H00FV4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00FV4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00FV4_A601ContagemResultado_Servico = new int[1] ;
         H00FV4_n601ContagemResultado_Servico = new bool[] {false} ;
         H00FV4_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00FV4_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00FV4_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         H00FV4_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00FV4_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         AV50Sistema_Sigla = "";
         AV28mQtdeDmn = new int [12] ;
         AV27mPFFinal = new decimal [12] ;
         H00FV6_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         H00FV6_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00FV6_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         AV5WebSession = context.GetSession();
         GridRow = new GXWebRow();
         sStyleString = "";
         lblContagemresultadotitle_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         TempTags = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttButton1_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTextblockcontagemresultado_pflfstotal_Jsonclick = "";
         lblTextblockcontagemresultado_pfbfmtotal2_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_consultasistemas__default(),
            new Object[][] {
                new Object[] {
               H00FV2_A155Servico_Codigo, H00FV2_A605Servico_Sigla, H00FV2_A631Servico_Vinculado, H00FV2_n631Servico_Vinculado, H00FV2_A632Servico_Ativo
               }
               , new Object[] {
               H00FV4_A146Modulo_Codigo, H00FV4_n146Modulo_Codigo, H00FV4_A127Sistema_Codigo, H00FV4_A135Sistema_AreaTrabalhoCod, H00FV4_A830AreaTrabalho_ServicoPadrao, H00FV4_n830AreaTrabalho_ServicoPadrao, H00FV4_A490ContagemResultado_ContratadaCod, H00FV4_n490ContagemResultado_ContratadaCod, H00FV4_A1553ContagemResultado_CntSrvCod, H00FV4_n1553ContagemResultado_CntSrvCod,
               H00FV4_A456ContagemResultado_Codigo, H00FV4_A52Contratada_AreaTrabalhoCod, H00FV4_n52Contratada_AreaTrabalhoCod, H00FV4_A489ContagemResultado_SistemaCod, H00FV4_n489ContagemResultado_SistemaCod, H00FV4_A632Servico_Ativo, H00FV4_n632Servico_Ativo, H00FV4_A484ContagemResultado_StatusDmn, H00FV4_n484ContagemResultado_StatusDmn, H00FV4_A601ContagemResultado_Servico,
               H00FV4_n601ContagemResultado_Servico, H00FV4_A509ContagemrResultado_SistemaSigla, H00FV4_n509ContagemrResultado_SistemaSigla, H00FV4_A566ContagemResultado_DataUltCnt, H00FV4_A682ContagemResultado_PFBFMUltima, H00FV4_A684ContagemResultado_PFBFSUltima
               }
               , new Object[] {
               H00FV6_A566ContagemResultado_DataUltCnt, H00FV6_A682ContagemResultado_PFBFMUltima, H00FV6_A684ContagemResultado_PFBFSUltima
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavCtlsigla_Enabled = 0;
         cmbavCtlmes.Enabled = 0;
         edtavCtlano_Enabled = 0;
         edtavCtlqtddmn_Enabled = 0;
         edtavCtlpffinal_Enabled = 0;
         edtavTqtdsistemas_Enabled = 0;
         edtavTqtdedmn_Enabled = 0;
         edtavTpffinal_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_20 ;
      private short nGXsfl_20_idx=1 ;
      private short AV19Filtro_Ano ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short AV53GXV1 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_20_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short nGXsfl_20_fel_idx=1 ;
      private short AV22i ;
      private short GRID_nEOF ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int AV20Filtro_SistemaCod ;
      private int A601ContagemResultado_Servico ;
      private int AV35Servico_Codigo ;
      private int AV45tQtdSistemas ;
      private int AV41tQtdeDmn ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int edtavCtlsigla_Enabled ;
      private int edtavCtlano_Enabled ;
      private int edtavCtlqtddmn_Enabled ;
      private int edtavCtlpffinal_Enabled ;
      private int edtavTqtdsistemas_Enabled ;
      private int edtavTqtdedmn_Enabled ;
      private int edtavTpffinal_Enabled ;
      private int subGrid_Titleforecolor ;
      private int subGrid_Width ;
      private int AV42WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A146Modulo_Codigo ;
      private int A127Sistema_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV49Sistema_Codigo ;
      private int GX_I ;
      private int [] AV28mQtdeDmn ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long AV21Filtro_Mes ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal AV40tPFFinal ;
      private decimal [] AV27mPFFinal ;
      private decimal AV44Contagem_PFFinal ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_20_idx="0001" ;
      private String A484ContagemResultado_StatusDmn ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Innewwindow_Target ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String edtavFiltro_sistemacod_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavCtlsigla_Internalname ;
      private String cmbavCtlmes_Internalname ;
      private String edtavCtlano_Internalname ;
      private String edtavCtlqtddmn_Internalname ;
      private String edtavCtlpffinal_Internalname ;
      private String edtavTqtdsistemas_Internalname ;
      private String edtavTqtdedmn_Internalname ;
      private String edtavTpffinal_Internalname ;
      private String dynavServico_codigo_Internalname ;
      private String cmbavFiltro_ano_Internalname ;
      private String cmbavFiltro_mes_Internalname ;
      private String sGXsfl_20_fel_idx="0001" ;
      private String hsh ;
      private String edtavCtlqtddmn_Tooltiptext ;
      private String AV50Sistema_Sigla ;
      private String Innewwindow_Internalname ;
      private String sStyleString ;
      private String tblTable4_Internalname ;
      private String lblContagemresultadotitle_Internalname ;
      private String lblContagemresultadotitle_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String TempTags ;
      private String edtavFiltro_sistemacod_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String dynavServico_codigo_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String cmbavFiltro_ano_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String cmbavFiltro_mes_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTable3_Internalname ;
      private String lblTextblockcontagemresultado_pflfstotal_Internalname ;
      private String lblTextblockcontagemresultado_pflfstotal_Jsonclick ;
      private String edtavTqtdsistemas_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfmtotal2_Internalname ;
      private String lblTextblockcontagemresultado_pfbfmtotal2_Jsonclick ;
      private String edtavTqtdedmn_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal_Jsonclick ;
      private String edtavTpffinal_Jsonclick ;
      private String ROClassString ;
      private String edtavCtlsigla_Jsonclick ;
      private String cmbavCtlmes_Jsonclick ;
      private String edtavCtlano_Jsonclick ;
      private String edtavCtlqtddmn_Jsonclick ;
      private String edtavCtlpffinal_Jsonclick ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool gx_BV20 ;
      private bool BRKFV3 ;
      private bool n146Modulo_Codigo ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool A632Servico_Ativo ;
      private bool n632Servico_Ativo ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavServico_codigo ;
      private GXCombobox cmbavFiltro_ano ;
      private GXCombobox cmbavFiltro_mes ;
      private GXCombobox cmbavCtlmes ;
      private IDataStoreProvider pr_default ;
      private int[] H00FV2_A155Servico_Codigo ;
      private String[] H00FV2_A605Servico_Sigla ;
      private int[] H00FV2_A631Servico_Vinculado ;
      private bool[] H00FV2_n631Servico_Vinculado ;
      private bool[] H00FV2_A632Servico_Ativo ;
      private bool[] H00FV2_n632Servico_Ativo ;
      private int[] H00FV4_A146Modulo_Codigo ;
      private bool[] H00FV4_n146Modulo_Codigo ;
      private int[] H00FV4_A127Sistema_Codigo ;
      private int[] H00FV4_A135Sistema_AreaTrabalhoCod ;
      private int[] H00FV4_A830AreaTrabalho_ServicoPadrao ;
      private bool[] H00FV4_n830AreaTrabalho_ServicoPadrao ;
      private int[] H00FV4_A490ContagemResultado_ContratadaCod ;
      private bool[] H00FV4_n490ContagemResultado_ContratadaCod ;
      private int[] H00FV4_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00FV4_n1553ContagemResultado_CntSrvCod ;
      private int[] H00FV4_A456ContagemResultado_Codigo ;
      private int[] H00FV4_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00FV4_n52Contratada_AreaTrabalhoCod ;
      private int[] H00FV4_A489ContagemResultado_SistemaCod ;
      private bool[] H00FV4_n489ContagemResultado_SistemaCod ;
      private bool[] H00FV4_A632Servico_Ativo ;
      private bool[] H00FV4_n632Servico_Ativo ;
      private String[] H00FV4_A484ContagemResultado_StatusDmn ;
      private bool[] H00FV4_n484ContagemResultado_StatusDmn ;
      private int[] H00FV4_A601ContagemResultado_Servico ;
      private bool[] H00FV4_n601ContagemResultado_Servico ;
      private String[] H00FV4_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00FV4_n509ContagemrResultado_SistemaSigla ;
      private DateTime[] H00FV4_A566ContagemResultado_DataUltCnt ;
      private decimal[] H00FV4_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00FV4_A684ContagemResultado_PFBFSUltima ;
      private DateTime[] H00FV6_A566ContagemResultado_DataUltCnt ;
      private decimal[] H00FV6_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00FV6_A684ContagemResultado_PFBFSUltima ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV5WebSession ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ConsultaSistemas_Sistema ))]
      private IGxCollection AV46sdt_ConsultaSistemas ;
      private GXWebForm Form ;
      private SdtSDT_ConsultaSistemas_Sistema AV47sdt_Sistema ;
      private SdtSDT_FiltroConsContadorFM AV34SDT_FiltroConsContadorFM ;
      private wwpbaseobjects.SdtWWPContext AV42WWPContext ;
   }

   public class wp_consultasistemas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00FV4( IGxContext context ,
                                             int AV20Filtro_SistemaCod ,
                                             int AV35Servico_Codigo ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A601ContagemResultado_Servico ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             short AV19Filtro_Ano ,
                                             long AV21Filtro_Mes ,
                                             String A484ContagemResultado_StatusDmn ,
                                             int AV42WWPContext_gxTpr_Areatrabalho_codigo ,
                                             int A52Contratada_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T6.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T5.[Servico_Ativo], T1.[ContagemResultado_StatusDmn], T7.[Servico_Codigo] AS ContagemResultado_Servico, T9.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T8.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T8.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima FROM (((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T7 WITH (NOLOCK) ON T7.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE";
         scmdbuf = scmdbuf + " [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Sistema] T9 WITH (NOLOCK) ON T9.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (T6.[Contratada_AreaTrabalhoCod] = @AV42WWPC_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (YEAR(COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) = @AV19Filtro_Ano)";
         scmdbuf = scmdbuf + " and ((@AV21Filtro_Mes = convert(int, 0)) or ( MONTH(COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) = @AV21Filtro_Mes))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] = 'R' or T1.[ContagemResultado_StatusDmn] = 'C' or T1.[ContagemResultado_StatusDmn] = 'H' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L')";
         if ( ! (0==AV20Filtro_SistemaCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV20Filtro_SistemaCod)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV35Servico_Codigo) )
         {
            sWhereString = sWhereString + " and (T7.[Servico_Codigo] = @AV35Servico_Codigo)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T6.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_SistemaCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00FV4(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (DateTime)dynConstraints[4] , (short)dynConstraints[5] , (long)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00FV2 ;
          prmH00FV2 = new Object[] {
          } ;
          Object[] prmH00FV6 ;
          prmH00FV6 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00FV4 ;
          prmH00FV4 = new Object[] {
          new Object[] {"@AV42WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV21Filtro_Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV21Filtro_Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV20Filtro_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00FV2", "SELECT [Servico_Codigo], [Servico_Sigla], [Servico_Vinculado], [Servico_Ativo] FROM [Servico] WITH (NOLOCK) WHERE (([Servico_Vinculado] = convert(int, 0))) AND ([Servico_Ativo] = 1) ORDER BY [Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FV2,0,0,true,false )
             ,new CursorDef("H00FV4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FV4,100,0,true,false )
             ,new CursorDef("H00FV6", "SELECT COALESCE( T1.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T1.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T1.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima FROM (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T1 WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FV6,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((bool[]) buf[15])[0] = rslt.getBool(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 25) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[23])[0] = rslt.getGXDate(14) ;
                ((decimal[]) buf[24])[0] = rslt.getDecimal(15) ;
                ((decimal[]) buf[25])[0] = rslt.getDecimal(16) ;
                return;
             case 2 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
