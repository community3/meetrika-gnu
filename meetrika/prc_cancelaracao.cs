/*
               File: PRC_CancelarAcao
        Description: Cancelar A��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:8:18.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_cancelaracao : GXProcedure
   {
      public prc_cancelaracao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_cancelaracao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           int aP1_UserId ,
                           String aP2_Observacao )
      {
         this.AV9Codigo = aP0_Codigo;
         this.AV12UserId = aP1_UserId;
         this.AV10Observacao = aP2_Observacao;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Codigo ,
                                 int aP1_UserId ,
                                 String aP2_Observacao )
      {
         prc_cancelaracao objprc_cancelaracao;
         objprc_cancelaracao = new prc_cancelaracao();
         objprc_cancelaracao.AV9Codigo = aP0_Codigo;
         objprc_cancelaracao.AV12UserId = aP1_UserId;
         objprc_cancelaracao.AV10Observacao = aP2_Observacao;
         objprc_cancelaracao.context.SetSubmitInitialConfig(context);
         objprc_cancelaracao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_cancelaracao);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_cancelaracao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00T73 */
         pr_default.execute(0, new Object[] {AV9Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1797LogResponsavel_Codigo = P00T73_A1797LogResponsavel_Codigo[0];
            A484ContagemResultado_StatusDmn = P00T73_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00T73_n484ContagemResultado_StatusDmn[0];
            A602ContagemResultado_OSVinculada = P00T73_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00T73_n602ContagemResultado_OSVinculada[0];
            A892LogResponsavel_DemandaCod = P00T73_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P00T73_n892LogResponsavel_DemandaCod[0];
            A1130LogResponsavel_Status = P00T73_A1130LogResponsavel_Status[0];
            n1130LogResponsavel_Status = P00T73_n1130LogResponsavel_Status[0];
            A1177LogResponsavel_Prazo = P00T73_A1177LogResponsavel_Prazo[0];
            n1177LogResponsavel_Prazo = P00T73_n1177LogResponsavel_Prazo[0];
            A896LogResponsavel_Owner = P00T73_A896LogResponsavel_Owner[0];
            A890ContagemResultado_Responsavel = P00T73_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00T73_n890ContagemResultado_Responsavel[0];
            A1351ContagemResultado_DataPrevista = P00T73_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00T73_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P00T73_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00T73_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P00T73_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00T73_n912ContagemResultado_HoraEntrega[0];
            A1521ContagemResultado_FimAnl = P00T73_A1521ContagemResultado_FimAnl[0];
            n1521ContagemResultado_FimAnl = P00T73_n1521ContagemResultado_FimAnl[0];
            A1510ContagemResultado_FimExc = P00T73_A1510ContagemResultado_FimExc[0];
            n1510ContagemResultado_FimExc = P00T73_n1510ContagemResultado_FimExc[0];
            A1349ContagemResultado_DataExecucao = P00T73_A1349ContagemResultado_DataExecucao[0];
            n1349ContagemResultado_DataExecucao = P00T73_n1349ContagemResultado_DataExecucao[0];
            A1348ContagemResultado_DataHomologacao = P00T73_A1348ContagemResultado_DataHomologacao[0];
            n1348ContagemResultado_DataHomologacao = P00T73_n1348ContagemResultado_DataHomologacao[0];
            A597ContagemResultado_LoteAceiteCod = P00T73_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00T73_n597ContagemResultado_LoteAceiteCod[0];
            A1854ContagemResultado_VlrCnc = P00T73_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P00T73_n1854ContagemResultado_VlrCnc[0];
            A1855ContagemResultado_PFCnc = P00T73_A1855ContagemResultado_PFCnc[0];
            n1855ContagemResultado_PFCnc = P00T73_n1855ContagemResultado_PFCnc[0];
            A40000GXC1 = P00T73_A40000GXC1[0];
            n40000GXC1 = P00T73_n40000GXC1[0];
            A484ContagemResultado_StatusDmn = P00T73_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00T73_n484ContagemResultado_StatusDmn[0];
            A602ContagemResultado_OSVinculada = P00T73_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00T73_n602ContagemResultado_OSVinculada[0];
            A890ContagemResultado_Responsavel = P00T73_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00T73_n890ContagemResultado_Responsavel[0];
            A1351ContagemResultado_DataPrevista = P00T73_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00T73_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P00T73_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00T73_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P00T73_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00T73_n912ContagemResultado_HoraEntrega[0];
            A1521ContagemResultado_FimAnl = P00T73_A1521ContagemResultado_FimAnl[0];
            n1521ContagemResultado_FimAnl = P00T73_n1521ContagemResultado_FimAnl[0];
            A1510ContagemResultado_FimExc = P00T73_A1510ContagemResultado_FimExc[0];
            n1510ContagemResultado_FimExc = P00T73_n1510ContagemResultado_FimExc[0];
            A1349ContagemResultado_DataExecucao = P00T73_A1349ContagemResultado_DataExecucao[0];
            n1349ContagemResultado_DataExecucao = P00T73_n1349ContagemResultado_DataExecucao[0];
            A1348ContagemResultado_DataHomologacao = P00T73_A1348ContagemResultado_DataHomologacao[0];
            n1348ContagemResultado_DataHomologacao = P00T73_n1348ContagemResultado_DataHomologacao[0];
            A597ContagemResultado_LoteAceiteCod = P00T73_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00T73_n597ContagemResultado_LoteAceiteCod[0];
            A1854ContagemResultado_VlrCnc = P00T73_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P00T73_n1854ContagemResultado_VlrCnc[0];
            A1855ContagemResultado_PFCnc = P00T73_A1855ContagemResultado_PFCnc[0];
            n1855ContagemResultado_PFCnc = P00T73_n1855ContagemResultado_PFCnc[0];
            A40000GXC1 = P00T73_A40000GXC1[0];
            n40000GXC1 = P00T73_n40000GXC1[0];
            AV8StatusDemanda = A484ContagemResultado_StatusDmn;
            AV13OsVinculada = A602ContagemResultado_OSVinculada;
            AV16DemandaCodigo = A892LogResponsavel_DemandaCod;
            AV14StatusAnterior = A1130LogResponsavel_Status;
            AV11PrazoEntrega = A1177LogResponsavel_Prazo;
            AV15ResponsavelAnterior = A896LogResponsavel_Owner;
            A484ContagemResultado_StatusDmn = AV14StatusAnterior;
            n484ContagemResultado_StatusDmn = false;
            A890ContagemResultado_Responsavel = AV15ResponsavelAnterior;
            n890ContagemResultado_Responsavel = false;
            if ( StringUtil.StrCmp(AV14StatusAnterior, "E") == 0 )
            {
               A1351ContagemResultado_DataPrevista = AV11PrazoEntrega;
               n1351ContagemResultado_DataPrevista = false;
               /* Execute user subroutine: 'PRAZOANTERIOR' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
            }
            else
            {
               /* Execute user subroutine: 'PRAZOANTERIOR' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
               A1351ContagemResultado_DataPrevista = AV11PrazoEntrega;
               n1351ContagemResultado_DataPrevista = false;
            }
            A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV11PrazoEntrega);
            n472ContagemResultado_DataEntrega = false;
            A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV11PrazoEntrega);
            n912ContagemResultado_HoraEntrega = false;
            if ( StringUtil.StrCmp(AV8StatusDemanda, "E") == 0 )
            {
            }
            else if ( StringUtil.StrCmp(AV8StatusDemanda, "A") == 0 )
            {
               A1521ContagemResultado_FimAnl = (DateTime)(DateTime.MinValue);
               n1521ContagemResultado_FimAnl = false;
               n1521ContagemResultado_FimAnl = true;
            }
            else if ( StringUtil.StrCmp(AV8StatusDemanda, "R") == 0 )
            {
               AV17ExecucaCodigo = A40000GXC1;
               A1510ContagemResultado_FimExc = (DateTime)(DateTime.MinValue);
               n1510ContagemResultado_FimExc = false;
               n1510ContagemResultado_FimExc = true;
               A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
               n1349ContagemResultado_DataExecucao = false;
               n1349ContagemResultado_DataExecucao = true;
               /* Execute user subroutine: 'REABREOS' */
               S121 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
            }
            else if ( StringUtil.StrCmp(AV8StatusDemanda, "H") == 0 )
            {
               A1348ContagemResultado_DataHomologacao = (DateTime)(DateTime.MinValue);
               n1348ContagemResultado_DataHomologacao = false;
               n1348ContagemResultado_DataHomologacao = true;
            }
            else if ( StringUtil.StrCmp(AV8StatusDemanda, "O") == 0 )
            {
               A597ContagemResultado_LoteAceiteCod = 0;
               n597ContagemResultado_LoteAceiteCod = false;
               n597ContagemResultado_LoteAceiteCod = true;
            }
            else if ( StringUtil.StrCmp(AV8StatusDemanda, "X") == 0 )
            {
               A1854ContagemResultado_VlrCnc = 0;
               n1854ContagemResultado_VlrCnc = false;
               n1854ContagemResultado_VlrCnc = true;
               A1855ContagemResultado_PFCnc = 0;
               n1855ContagemResultado_PFCnc = false;
               n1855ContagemResultado_PFCnc = true;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00T74 */
            pr_default.execute(1, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1521ContagemResultado_FimAnl, A1521ContagemResultado_FimAnl, n1510ContagemResultado_FimExc, A1510ContagemResultado_FimExc, n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n1854ContagemResultado_VlrCnc, A1854ContagemResultado_VlrCnc, n1855ContagemResultado_PFCnc, A1855ContagemResultado_PFCnc, n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00T75 */
            pr_default.execute(2, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1521ContagemResultado_FimAnl, A1521ContagemResultado_FimAnl, n1510ContagemResultado_FimExc, A1510ContagemResultado_FimExc, n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n1854ContagemResultado_VlrCnc, A1854ContagemResultado_VlrCnc, n1855ContagemResultado_PFCnc, A1855ContagemResultado_PFCnc, n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( AV13OsVinculada > 0 )
         {
            new prc_upddpnhmlg(context ).execute( ref  AV13OsVinculada) ;
         }
         new prc_inslogresponsavel(context ).execute( ref  AV16DemandaCodigo,  AV15ResponsavelAnterior,  "BK",  "D",  AV12UserId,  0,  AV8StatusDemanda,  AV14StatusAnterior,  AV10Observacao,  AV11PrazoEntrega,  true) ;
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRAZOANTERIOR' Routine */
         /* Using cursor P00T76 */
         pr_default.execute(3, new Object[] {AV16DemandaCodigo, AV9Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A1797LogResponsavel_Codigo = P00T76_A1797LogResponsavel_Codigo[0];
            A892LogResponsavel_DemandaCod = P00T76_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P00T76_n892LogResponsavel_DemandaCod[0];
            A1177LogResponsavel_Prazo = P00T76_A1177LogResponsavel_Prazo[0];
            n1177LogResponsavel_Prazo = P00T76_n1177LogResponsavel_Prazo[0];
            AV11PrazoEntrega = A1177LogResponsavel_Prazo;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void S121( )
      {
         /* 'REABREOS' Routine */
         /* Using cursor P00T77 */
         pr_default.execute(4, new Object[] {AV17ExecucaCodigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A1405ContagemResultadoExecucao_Codigo = P00T77_A1405ContagemResultadoExecucao_Codigo[0];
            A1407ContagemResultadoExecucao_Fim = P00T77_A1407ContagemResultadoExecucao_Fim[0];
            n1407ContagemResultadoExecucao_Fim = P00T77_n1407ContagemResultadoExecucao_Fim[0];
            A1410ContagemResultadoExecucao_Dias = P00T77_A1410ContagemResultadoExecucao_Dias[0];
            n1410ContagemResultadoExecucao_Dias = P00T77_n1410ContagemResultadoExecucao_Dias[0];
            A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
            n1407ContagemResultadoExecucao_Fim = false;
            n1407ContagemResultadoExecucao_Fim = true;
            A1410ContagemResultadoExecucao_Dias = 0;
            n1410ContagemResultadoExecucao_Dias = false;
            n1410ContagemResultadoExecucao_Dias = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00T78 */
            pr_default.execute(5, new Object[] {n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, A1405ContagemResultadoExecucao_Codigo});
            pr_default.close(5);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
            if (true) break;
            /* Using cursor P00T79 */
            pr_default.execute(6, new Object[] {n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, A1405ContagemResultadoExecucao_Codigo});
            pr_default.close(6);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(4);
         /* Using cursor P00T710 */
         pr_default.execute(7, new Object[] {AV9Codigo});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A517ContagemResultado_Ultima = P00T710_A517ContagemResultado_Ultima[0];
            A456ContagemResultado_Codigo = P00T710_A456ContagemResultado_Codigo[0];
            A473ContagemResultado_DataCnt = P00T710_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = P00T710_A511ContagemResultado_HoraCnt[0];
            /* Using cursor P00T711 */
            pr_default.execute(8, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(7);
         }
         pr_default.close(7);
         /* Using cursor P00T712 */
         pr_default.execute(9, new Object[] {AV9Codigo});
         while ( (pr_default.getStatus(9) != 101) )
         {
            A456ContagemResultado_Codigo = P00T712_A456ContagemResultado_Codigo[0];
            A517ContagemResultado_Ultima = P00T712_A517ContagemResultado_Ultima[0];
            A511ContagemResultado_HoraCnt = P00T712_A511ContagemResultado_HoraCnt[0];
            A473ContagemResultado_DataCnt = P00T712_A473ContagemResultado_DataCnt[0];
            A517ContagemResultado_Ultima = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00T713 */
            pr_default.execute(10, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(10);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            if (true) break;
            /* Using cursor P00T714 */
            pr_default.execute(11, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(11);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            pr_default.readNext(9);
         }
         pr_default.close(9);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_CancelarAcao");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00T73_A1797LogResponsavel_Codigo = new long[1] ;
         P00T73_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00T73_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00T73_A602ContagemResultado_OSVinculada = new int[1] ;
         P00T73_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00T73_A892LogResponsavel_DemandaCod = new int[1] ;
         P00T73_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00T73_A1130LogResponsavel_Status = new String[] {""} ;
         P00T73_n1130LogResponsavel_Status = new bool[] {false} ;
         P00T73_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         P00T73_n1177LogResponsavel_Prazo = new bool[] {false} ;
         P00T73_A896LogResponsavel_Owner = new int[1] ;
         P00T73_A890ContagemResultado_Responsavel = new int[1] ;
         P00T73_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00T73_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00T73_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00T73_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00T73_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00T73_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00T73_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00T73_A1521ContagemResultado_FimAnl = new DateTime[] {DateTime.MinValue} ;
         P00T73_n1521ContagemResultado_FimAnl = new bool[] {false} ;
         P00T73_A1510ContagemResultado_FimExc = new DateTime[] {DateTime.MinValue} ;
         P00T73_n1510ContagemResultado_FimExc = new bool[] {false} ;
         P00T73_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P00T73_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P00T73_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00T73_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P00T73_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00T73_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00T73_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P00T73_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P00T73_A1855ContagemResultado_PFCnc = new decimal[1] ;
         P00T73_n1855ContagemResultado_PFCnc = new bool[] {false} ;
         P00T73_A40000GXC1 = new int[1] ;
         P00T73_n40000GXC1 = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         A1130LogResponsavel_Status = "";
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A1521ContagemResultado_FimAnl = (DateTime)(DateTime.MinValue);
         A1510ContagemResultado_FimExc = (DateTime)(DateTime.MinValue);
         A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
         A1348ContagemResultado_DataHomologacao = (DateTime)(DateTime.MinValue);
         AV8StatusDemanda = "";
         AV14StatusAnterior = "";
         AV11PrazoEntrega = (DateTime)(DateTime.MinValue);
         P00T76_A1797LogResponsavel_Codigo = new long[1] ;
         P00T76_A892LogResponsavel_DemandaCod = new int[1] ;
         P00T76_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00T76_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         P00T76_n1177LogResponsavel_Prazo = new bool[] {false} ;
         P00T77_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         P00T77_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         P00T77_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         P00T77_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         P00T77_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         P00T710_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00T710_A456ContagemResultado_Codigo = new int[1] ;
         P00T710_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00T710_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         P00T712_A456ContagemResultado_Codigo = new int[1] ;
         P00T712_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00T712_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00T712_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_cancelaracao__default(),
            new Object[][] {
                new Object[] {
               P00T73_A1797LogResponsavel_Codigo, P00T73_A484ContagemResultado_StatusDmn, P00T73_n484ContagemResultado_StatusDmn, P00T73_A602ContagemResultado_OSVinculada, P00T73_n602ContagemResultado_OSVinculada, P00T73_A892LogResponsavel_DemandaCod, P00T73_n892LogResponsavel_DemandaCod, P00T73_A1130LogResponsavel_Status, P00T73_n1130LogResponsavel_Status, P00T73_A1177LogResponsavel_Prazo,
               P00T73_n1177LogResponsavel_Prazo, P00T73_A896LogResponsavel_Owner, P00T73_A890ContagemResultado_Responsavel, P00T73_n890ContagemResultado_Responsavel, P00T73_A1351ContagemResultado_DataPrevista, P00T73_n1351ContagemResultado_DataPrevista, P00T73_A472ContagemResultado_DataEntrega, P00T73_n472ContagemResultado_DataEntrega, P00T73_A912ContagemResultado_HoraEntrega, P00T73_n912ContagemResultado_HoraEntrega,
               P00T73_A1521ContagemResultado_FimAnl, P00T73_n1521ContagemResultado_FimAnl, P00T73_A1510ContagemResultado_FimExc, P00T73_n1510ContagemResultado_FimExc, P00T73_A1349ContagemResultado_DataExecucao, P00T73_n1349ContagemResultado_DataExecucao, P00T73_A1348ContagemResultado_DataHomologacao, P00T73_n1348ContagemResultado_DataHomologacao, P00T73_A597ContagemResultado_LoteAceiteCod, P00T73_n597ContagemResultado_LoteAceiteCod,
               P00T73_A1854ContagemResultado_VlrCnc, P00T73_n1854ContagemResultado_VlrCnc, P00T73_A1855ContagemResultado_PFCnc, P00T73_n1855ContagemResultado_PFCnc, P00T73_A40000GXC1, P00T73_n40000GXC1
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00T76_A1797LogResponsavel_Codigo, P00T76_A892LogResponsavel_DemandaCod, P00T76_n892LogResponsavel_DemandaCod, P00T76_A1177LogResponsavel_Prazo, P00T76_n1177LogResponsavel_Prazo
               }
               , new Object[] {
               P00T77_A1405ContagemResultadoExecucao_Codigo, P00T77_A1407ContagemResultadoExecucao_Fim, P00T77_n1407ContagemResultadoExecucao_Fim, P00T77_A1410ContagemResultadoExecucao_Dias, P00T77_n1410ContagemResultadoExecucao_Dias
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00T710_A517ContagemResultado_Ultima, P00T710_A456ContagemResultado_Codigo, P00T710_A473ContagemResultado_DataCnt, P00T710_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               P00T712_A456ContagemResultado_Codigo, P00T712_A517ContagemResultado_Ultima, P00T712_A511ContagemResultado_HoraCnt, P00T712_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1410ContagemResultadoExecucao_Dias ;
      private int AV9Codigo ;
      private int AV12UserId ;
      private int A602ContagemResultado_OSVinculada ;
      private int A892LogResponsavel_DemandaCod ;
      private int A896LogResponsavel_Owner ;
      private int A890ContagemResultado_Responsavel ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A40000GXC1 ;
      private int AV13OsVinculada ;
      private int AV16DemandaCodigo ;
      private int AV15ResponsavelAnterior ;
      private int AV17ExecucaCodigo ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private long A1797LogResponsavel_Codigo ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A1855ContagemResultado_PFCnc ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1130LogResponsavel_Status ;
      private String AV8StatusDemanda ;
      private String AV14StatusAnterior ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A1177LogResponsavel_Prazo ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A1521ContagemResultado_FimAnl ;
      private DateTime A1510ContagemResultado_FimExc ;
      private DateTime A1349ContagemResultado_DataExecucao ;
      private DateTime A1348ContagemResultado_DataHomologacao ;
      private DateTime AV11PrazoEntrega ;
      private DateTime A1407ContagemResultadoExecucao_Fim ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1130LogResponsavel_Status ;
      private bool n1177LogResponsavel_Prazo ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n1521ContagemResultado_FimAnl ;
      private bool n1510ContagemResultado_FimExc ;
      private bool n1349ContagemResultado_DataExecucao ;
      private bool n1348ContagemResultado_DataHomologacao ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool n1855ContagemResultado_PFCnc ;
      private bool n40000GXC1 ;
      private bool returnInSub ;
      private bool n1407ContagemResultadoExecucao_Fim ;
      private bool n1410ContagemResultadoExecucao_Dias ;
      private bool A517ContagemResultado_Ultima ;
      private String AV10Observacao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private long[] P00T73_A1797LogResponsavel_Codigo ;
      private String[] P00T73_A484ContagemResultado_StatusDmn ;
      private bool[] P00T73_n484ContagemResultado_StatusDmn ;
      private int[] P00T73_A602ContagemResultado_OSVinculada ;
      private bool[] P00T73_n602ContagemResultado_OSVinculada ;
      private int[] P00T73_A892LogResponsavel_DemandaCod ;
      private bool[] P00T73_n892LogResponsavel_DemandaCod ;
      private String[] P00T73_A1130LogResponsavel_Status ;
      private bool[] P00T73_n1130LogResponsavel_Status ;
      private DateTime[] P00T73_A1177LogResponsavel_Prazo ;
      private bool[] P00T73_n1177LogResponsavel_Prazo ;
      private int[] P00T73_A896LogResponsavel_Owner ;
      private int[] P00T73_A890ContagemResultado_Responsavel ;
      private bool[] P00T73_n890ContagemResultado_Responsavel ;
      private DateTime[] P00T73_A1351ContagemResultado_DataPrevista ;
      private bool[] P00T73_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00T73_A472ContagemResultado_DataEntrega ;
      private bool[] P00T73_n472ContagemResultado_DataEntrega ;
      private DateTime[] P00T73_A912ContagemResultado_HoraEntrega ;
      private bool[] P00T73_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P00T73_A1521ContagemResultado_FimAnl ;
      private bool[] P00T73_n1521ContagemResultado_FimAnl ;
      private DateTime[] P00T73_A1510ContagemResultado_FimExc ;
      private bool[] P00T73_n1510ContagemResultado_FimExc ;
      private DateTime[] P00T73_A1349ContagemResultado_DataExecucao ;
      private bool[] P00T73_n1349ContagemResultado_DataExecucao ;
      private DateTime[] P00T73_A1348ContagemResultado_DataHomologacao ;
      private bool[] P00T73_n1348ContagemResultado_DataHomologacao ;
      private int[] P00T73_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00T73_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] P00T73_A1854ContagemResultado_VlrCnc ;
      private bool[] P00T73_n1854ContagemResultado_VlrCnc ;
      private decimal[] P00T73_A1855ContagemResultado_PFCnc ;
      private bool[] P00T73_n1855ContagemResultado_PFCnc ;
      private int[] P00T73_A40000GXC1 ;
      private bool[] P00T73_n40000GXC1 ;
      private long[] P00T76_A1797LogResponsavel_Codigo ;
      private int[] P00T76_A892LogResponsavel_DemandaCod ;
      private bool[] P00T76_n892LogResponsavel_DemandaCod ;
      private DateTime[] P00T76_A1177LogResponsavel_Prazo ;
      private bool[] P00T76_n1177LogResponsavel_Prazo ;
      private int[] P00T77_A1405ContagemResultadoExecucao_Codigo ;
      private DateTime[] P00T77_A1407ContagemResultadoExecucao_Fim ;
      private bool[] P00T77_n1407ContagemResultadoExecucao_Fim ;
      private short[] P00T77_A1410ContagemResultadoExecucao_Dias ;
      private bool[] P00T77_n1410ContagemResultadoExecucao_Dias ;
      private bool[] P00T710_A517ContagemResultado_Ultima ;
      private int[] P00T710_A456ContagemResultado_Codigo ;
      private DateTime[] P00T710_A473ContagemResultado_DataCnt ;
      private String[] P00T710_A511ContagemResultado_HoraCnt ;
      private int[] P00T712_A456ContagemResultado_Codigo ;
      private bool[] P00T712_A517ContagemResultado_Ultima ;
      private String[] P00T712_A511ContagemResultado_HoraCnt ;
      private DateTime[] P00T712_A473ContagemResultado_DataCnt ;
   }

   public class prc_cancelaracao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00T73 ;
          prmP00T73 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00T74 ;
          prmP00T74 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_FimAnl",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_FimExc",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_VlrCnc",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_PFCnc",SqlDbType.Decimal,14,5} ,
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00T75 ;
          prmP00T75 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_FimAnl",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_FimExc",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_VlrCnc",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_PFCnc",SqlDbType.Decimal,14,5} ,
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00T76 ;
          prmP00T76 = new Object[] {
          new Object[] {"@AV16DemandaCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00T77 ;
          prmP00T77 = new Object[] {
          new Object[] {"@AV17ExecucaCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00T78 ;
          prmP00T78 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00T79 ;
          prmP00T79 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00T710 ;
          prmP00T710 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00T711 ;
          prmP00T711 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP00T712 ;
          prmP00T712 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00T713 ;
          prmP00T713 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP00T714 ;
          prmP00T714 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00T73", "SELECT TOP 1 T1.[LogResponsavel_Codigo], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_OSVinculada], T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T1.[LogResponsavel_Status], T1.[LogResponsavel_Prazo], T1.[LogResponsavel_Owner], T2.[ContagemResultado_Responsavel], T2.[ContagemResultado_DataPrevista], T2.[ContagemResultado_DataEntrega], T2.[ContagemResultado_HoraEntrega], T2.[ContagemResultado_FimAnl], T2.[ContagemResultado_FimExc], T2.[ContagemResultado_DataExecucao], T2.[ContagemResultado_DataHomologacao], T2.[ContagemResultado_LoteAceiteCod], T2.[ContagemResultado_VlrCnc], T2.[ContagemResultado_PFCnc], COALESCE( T3.[GXC1], 0) AS GXC1 FROM ([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (UPDLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]),  (SELECT MAX([ContagemResultadoExecucao_Codigo]) AS GXC1 FROM [ContagemResultadoExecucao] WITH (NOLOCK) ) T3 WHERE T1.[LogResponsavel_Codigo] = @AV9Codigo ORDER BY T1.[LogResponsavel_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T73,1,0,true,true )
             ,new CursorDef("P00T74", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_FimAnl]=@ContagemResultado_FimAnl, [ContagemResultado_FimExc]=@ContagemResultado_FimExc, [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao, [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao, [ContagemResultado_LoteAceiteCod]=@ContagemResultado_LoteAceiteCod, [ContagemResultado_VlrCnc]=@ContagemResultado_VlrCnc, [ContagemResultado_PFCnc]=@ContagemResultado_PFCnc  WHERE [ContagemResultado_Codigo] = @LogResponsavel_DemandaCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00T74)
             ,new CursorDef("P00T75", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_FimAnl]=@ContagemResultado_FimAnl, [ContagemResultado_FimExc]=@ContagemResultado_FimExc, [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao, [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao, [ContagemResultado_LoteAceiteCod]=@ContagemResultado_LoteAceiteCod, [ContagemResultado_VlrCnc]=@ContagemResultado_VlrCnc, [ContagemResultado_PFCnc]=@ContagemResultado_PFCnc  WHERE [ContagemResultado_Codigo] = @LogResponsavel_DemandaCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00T75)
             ,new CursorDef("P00T76", "SELECT TOP 1 [LogResponsavel_Codigo], [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, [LogResponsavel_Prazo] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @AV16DemandaCodigo and [LogResponsavel_Codigo] < @AV9Codigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T76,1,0,false,true )
             ,new CursorDef("P00T77", "SELECT TOP 1 [ContagemResultadoExecucao_Codigo], [ContagemResultadoExecucao_Fim], [ContagemResultadoExecucao_Dias] FROM [ContagemResultadoExecucao] WITH (UPDLOCK) WHERE [ContagemResultadoExecucao_Codigo] = @AV17ExecucaCodigo ORDER BY [ContagemResultadoExecucao_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T77,1,0,true,true )
             ,new CursorDef("P00T78", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Fim]=@ContagemResultadoExecucao_Fim, [ContagemResultadoExecucao_Dias]=@ContagemResultadoExecucao_Dias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00T78)
             ,new CursorDef("P00T79", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Fim]=@ContagemResultadoExecucao_Fim, [ContagemResultadoExecucao_Dias]=@ContagemResultadoExecucao_Dias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00T79)
             ,new CursorDef("P00T710", "SELECT TOP 1 [ContagemResultado_Ultima], [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE ([ContagemResultado_Codigo] = @AV9Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T710,1,0,true,true )
             ,new CursorDef("P00T711", "DELETE FROM [ContagemResultadoContagens]  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00T711)
             ,new CursorDef("P00T712", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV9Codigo ORDER BY [ContagemResultado_Codigo] DESC, [ContagemResultado_DataCnt] DESC, [ContagemResultado_HoraCnt] DESC ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T712,1,0,true,true )
             ,new CursorDef("P00T713", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00T713)
             ,new CursorDef("P00T714", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00T714)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[14])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[22])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[24])[0] = rslt.getGXDateTime(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[26])[0] = rslt.getGXDateTime(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((int[]) buf[28])[0] = rslt.getInt(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((decimal[]) buf[30])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 7 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 5) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(7, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(8, (DateTime)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 9 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(9, (DateTime)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[25]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(7, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(8, (DateTime)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 9 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(9, (DateTime)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[25]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 11 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
       }
    }

 }

}
