/*
               File: SistemaTabelaAtributoWC
        Description: Sistema Tabela Atributo WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:45.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistematabelaatributowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public sistematabelaatributowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public sistematabelaatributowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_Codigo )
      {
         this.A127Sistema_Codigo = aP0_Sistema_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbAtributos_TipoDados = new GXCombobox();
         chkAtributos_PK = new GXCheckbox();
         chkAtributos_FK = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n127Sistema_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A127Sistema_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridtabelas") == 0 )
               {
                  nRC_GXsfl_42 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_42_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_42_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGridtabelas_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridtabelas") == 0 )
               {
                  subGridtabelas_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13Tabela_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Tabela_Nome", AV13Tabela_Nome);
                  A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n127Sistema_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
                  A172Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV7Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0)));
                  AV15QtdTbl = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15QtdTbl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15QtdTbl), 4, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGridtabelas_refresh( subGridtabelas_Rows, AV13Tabela_Nome, A127Sistema_Codigo, A172Tabela_Codigo, AV7Tabela_Codigo, AV15QtdTbl, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridatributos") == 0 )
               {
                  nRC_GXsfl_55 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_55_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_55_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGridatributos_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridatributos") == 0 )
               {
                  subGridatributos_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13Tabela_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Tabela_Nome", AV13Tabela_Nome);
                  A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n127Sistema_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
                  A176Atributos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV16QtdAtr = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16QtdAtr", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16QtdAtr), 4, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGridatributos_refresh( subGridatributos_Rows, AV13Tabela_Nome, A127Sistema_Codigo, A176Atributos_Codigo, AV16QtdAtr, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA9H2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS9H2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Sistema Tabela Atributo WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117184583");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistematabelaatributowc.aspx") + "?" + UrlEncode("" +A127Sistema_Codigo)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_NOME", StringUtil.RTrim( AV13Tabela_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_NOME", StringUtil.RTrim( AV13Tabela_Nome));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_42", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_42), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_55", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_55), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDTABELASPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19GridTabelasPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA127Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Tabela_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vQTDTBL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15QtdTbl), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vQTDATR", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16QtdAtr), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Class", StringUtil.RTrim( Gridtabelaspaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_First", StringUtil.RTrim( Gridtabelaspaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Previous", StringUtil.RTrim( Gridtabelaspaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Next", StringUtil.RTrim( Gridtabelaspaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Last", StringUtil.RTrim( Gridtabelaspaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Caption", StringUtil.RTrim( Gridtabelaspaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridtabelaspaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridtabelaspaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridtabelaspaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridtabelaspaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridtabelaspaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridtabelaspaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridtabelaspaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridtabelaspaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridtabelaspaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE1_Width", StringUtil.RTrim( Dvpanel_unnamedtable1_Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE1_Cls", StringUtil.RTrim( Dvpanel_unnamedtable1_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE1_Title", StringUtil.RTrim( Dvpanel_unnamedtable1_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE1_Collapsible", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Collapsible));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE1_Collapsed", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Collapsed));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE1_Autowidth", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autowidth));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE1_Autoheight", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autoheight));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE1_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE1_Iconposition", StringUtil.RTrim( Dvpanel_unnamedtable1_Iconposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE1_Autoscroll", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autoscroll));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELASPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridtabelaspaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm9H2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("sistematabelaatributowc.js", "?20203117184631");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SistemaTabelaAtributoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistema Tabela Atributo WC" ;
      }

      protected void WB9H0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "sistematabelaatributowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
            }
            wb_table1_2_9H2( true) ;
         }
         else
         {
            wb_table1_2_9H2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9H2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START9H2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Sistema Tabela Atributo WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP9H0( ) ;
            }
         }
      }

      protected void WS9H2( )
      {
         START9H2( ) ;
         EVT9H2( ) ;
      }

      protected void EVT9H2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9H0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDTABELASPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9H0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E119H2 */
                                    E119H2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DONEWTABELA'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9H0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E129H2 */
                                    E129H2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DONEWATRIBUTO'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9H0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E139H2 */
                                    E139H2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9H0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavTabela_nome_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDATRIBUTOSPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9H0( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDATRIBUTOSPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgridatributos_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgridatributos_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgridatributos_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgridatributos_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "GRIDTABELAS.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 26), "GRIDTABELAS.ONLINEACTIVATE") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9H0( ) ;
                              }
                              nGXsfl_42_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
                              SubsflControlProps_422( ) ;
                              AV11UpdTabela = cgiGet( edtavUpdtabela_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdtabela_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV11UpdTabela)) ? AV22Updtabela_GXI : context.convertURL( context.PathToRelativeUrl( AV11UpdTabela))));
                              AV12DltTabela = cgiGet( edtavDlttabela_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDlttabela_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV12DltTabela)) ? AV23Dlttabela_GXI : context.convertURL( context.PathToRelativeUrl( AV12DltTabela))));
                              A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTabela_Codigo_Internalname), ",", "."));
                              A173Tabela_Nome = StringUtil.Upper( cgiGet( edtTabela_Nome_Internalname));
                              A189Tabela_ModuloDes = StringUtil.Upper( cgiGet( edtTabela_ModuloDes_Internalname));
                              n189Tabela_ModuloDes = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E149H2 */
                                          E149H2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E159H2 */
                                          E159H2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDTABELAS.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E169H2 */
                                          E169H2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDTABELAS.ONLINEACTIVATE") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E179H2 */
                                          E179H2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Tabela_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME"), AV13Tabela_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tabela_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME"), AV13Tabela_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP9H0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                           else if ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "GRIDATRIBUTOS.LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9H0( ) ;
                              }
                              nGXsfl_55_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
                              SubsflControlProps_553( ) ;
                              AV9UpdAtributo = cgiGet( edtavUpdatributo_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdatributo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV9UpdAtributo)) ? AV24Updatributo_GXI : context.convertURL( context.PathToRelativeUrl( AV9UpdAtributo))));
                              AV10DltAtributo = cgiGet( edtavDltatributo_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDltatributo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV10DltAtributo)) ? AV25Dltatributo_GXI : context.convertURL( context.PathToRelativeUrl( AV10DltAtributo))));
                              A356Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( edtAtributos_TabelaCod_Internalname), ",", "."));
                              A176Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAtributos_Codigo_Internalname), ",", "."));
                              A177Atributos_Nome = StringUtil.Upper( cgiGet( edtAtributos_Nome_Internalname));
                              cmbAtributos_TipoDados.Name = cmbAtributos_TipoDados_Internalname;
                              cmbAtributos_TipoDados.CurrentValue = cgiGet( cmbAtributos_TipoDados_Internalname);
                              A178Atributos_TipoDados = cgiGet( cmbAtributos_TipoDados_Internalname);
                              n178Atributos_TipoDados = false;
                              A400Atributos_PK = StringUtil.StrToBool( cgiGet( chkAtributos_PK_Internalname));
                              n400Atributos_PK = false;
                              A401Atributos_FK = StringUtil.StrToBool( cgiGet( chkAtributos_FK_Internalname));
                              n401Atributos_FK = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "GRIDATRIBUTOS.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E189H3 */
                                          E189H3 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP9H0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_nome_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE9H2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm9H2( ) ;
            }
         }
      }

      protected void PA9H2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "ATRIBUTOS_TIPODADOS_" + sGXsfl_55_idx;
            cmbAtributos_TipoDados.Name = GXCCtl;
            cmbAtributos_TipoDados.WebTags = "";
            cmbAtributos_TipoDados.addItem("", "Desconhecido", 0);
            cmbAtributos_TipoDados.addItem("N", "Numeric", 0);
            cmbAtributos_TipoDados.addItem("C", "Character", 0);
            cmbAtributos_TipoDados.addItem("VC", "Varchar", 0);
            cmbAtributos_TipoDados.addItem("D", "Date", 0);
            cmbAtributos_TipoDados.addItem("DT", "Date Time", 0);
            cmbAtributos_TipoDados.addItem("Bool", "Boolean", 0);
            cmbAtributos_TipoDados.addItem("Blob", "Blob", 0);
            cmbAtributos_TipoDados.addItem("Outr", "Outros", 0);
            if ( cmbAtributos_TipoDados.ItemCount > 0 )
            {
               A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
               n178Atributos_TipoDados = false;
            }
            GXCCtl = "ATRIBUTOS_PK_" + sGXsfl_55_idx;
            chkAtributos_PK.Name = GXCCtl;
            chkAtributos_PK.WebTags = "";
            chkAtributos_PK.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_PK_Internalname, "TitleCaption", chkAtributos_PK.Caption);
            chkAtributos_PK.CheckedValue = "false";
            GXCCtl = "ATRIBUTOS_FK_" + sGXsfl_55_idx;
            chkAtributos_FK.Name = GXCCtl;
            chkAtributos_FK.WebTags = "";
            chkAtributos_FK.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_FK_Internalname, "TitleCaption", chkAtributos_FK.Caption);
            chkAtributos_FK.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavTabela_nome_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridtabelas_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_422( ) ;
         while ( nGXsfl_42_idx <= nRC_GXsfl_42 )
         {
            sendrow_422( ) ;
            nGXsfl_42_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_42_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_42_idx+1));
            sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
            SubsflControlProps_422( ) ;
         }
         context.GX_webresponse.AddString(GridtabelasContainer.ToJavascriptSource());
         /* End function gxnrGridtabelas_newrow */
      }

      protected void gxnrGridatributos_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_553( ) ;
         while ( nGXsfl_55_idx <= nRC_GXsfl_55 )
         {
            sendrow_553( ) ;
            nGXsfl_55_idx = (short)(((subGridatributos_Islastpage==1)&&(nGXsfl_55_idx+1>subGridatributos_Recordsperpage( )) ? 1 : nGXsfl_55_idx+1));
            sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
            SubsflControlProps_553( ) ;
         }
         context.GX_webresponse.AddString(GridatributosContainer.ToJavascriptSource());
         /* End function gxnrGridatributos_newrow */
      }

      protected void gxgrGridtabelas_refresh( int subGridtabelas_Rows ,
                                              String AV13Tabela_Nome ,
                                              int A127Sistema_Codigo ,
                                              int A172Tabela_Codigo ,
                                              int AV7Tabela_Codigo ,
                                              short AV15QtdTbl ,
                                              String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Rows), 6, 0, ".", "")));
         GRIDTABELAS_nCurrentRecord = 0;
         RF9H2( ) ;
         /* End function gxgrGridtabelas_refresh */
      }

      protected void gxgrGridatributos_refresh( int subGridatributos_Rows ,
                                                String AV13Tabela_Nome ,
                                                int A127Sistema_Codigo ,
                                                int A176Atributos_Codigo ,
                                                short AV16QtdAtr ,
                                                String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         /* Execute user event: E159H2 */
         E159H2 ();
         GRIDATRIBUTOS_nCurrentRecord = 0;
         RF9H3( ) ;
         /* End function gxgrGridatributos_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_NOME", StringUtil.RTrim( A173Tabela_Nome));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9H2( ) ;
         RF9H3( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF9H2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridtabelasContainer.ClearRows();
         }
         wbStart = 42;
         /* Execute user event: E159H2 */
         E159H2 ();
         nGXsfl_42_idx = 1;
         sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
         SubsflControlProps_422( ) ;
         nGXsfl_42_Refreshing = 1;
         GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
         GridtabelasContainer.AddObjectProperty("CmpContext", sPrefix);
         GridtabelasContainer.AddObjectProperty("InMasterPage", "false");
         GridtabelasContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridtabelasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridtabelasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridtabelasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Backcolorstyle), 1, 0, ".", "")));
         GridtabelasContainer.PageSize = subGridtabelas_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_422( ) ;
            GXPagingFrom2 = (int)(((subGridtabelas_Rows==0) ? 1 : GRIDTABELAS_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGridtabelas_Rows==0) ? 10000 : GRIDTABELAS_nFirstRecordOnPage+subGridtabelas_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV13Tabela_Nome ,
                                                 A173Tabela_Nome ,
                                                 A190Tabela_SistemaCod ,
                                                 A127Sistema_Codigo ,
                                                 A174Tabela_Ativo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                 }
            });
            lV13Tabela_Nome = StringUtil.PadR( StringUtil.RTrim( AV13Tabela_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Tabela_Nome", AV13Tabela_Nome);
            /* Using cursor H009H2 */
            pr_default.execute(0, new Object[] {n127Sistema_Codigo, A127Sistema_Codigo, lV13Tabela_Nome, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_42_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGridtabelas_Rows == 0 ) || ( GRIDTABELAS_nCurrentRecord < subGridtabelas_Recordsperpage( ) ) ) ) )
            {
               A188Tabela_ModuloCod = H009H2_A188Tabela_ModuloCod[0];
               n188Tabela_ModuloCod = H009H2_n188Tabela_ModuloCod[0];
               A190Tabela_SistemaCod = H009H2_A190Tabela_SistemaCod[0];
               A174Tabela_Ativo = H009H2_A174Tabela_Ativo[0];
               n174Tabela_Ativo = H009H2_n174Tabela_Ativo[0];
               A189Tabela_ModuloDes = H009H2_A189Tabela_ModuloDes[0];
               n189Tabela_ModuloDes = H009H2_n189Tabela_ModuloDes[0];
               A173Tabela_Nome = H009H2_A173Tabela_Nome[0];
               A172Tabela_Codigo = H009H2_A172Tabela_Codigo[0];
               A189Tabela_ModuloDes = H009H2_A189Tabela_ModuloDes[0];
               n189Tabela_ModuloDes = H009H2_n189Tabela_ModuloDes[0];
               /* Execute user event: E169H2 */
               E169H2 ();
               pr_default.readNext(0);
            }
            GRIDTABELAS_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 42;
            WB9H0( ) ;
         }
         nGXsfl_42_Refreshing = 0;
      }

      protected void RF9H3( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridatributosContainer.ClearRows();
         }
         wbStart = 55;
         nGXsfl_55_idx = 1;
         sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
         SubsflControlProps_553( ) ;
         nGXsfl_55_Refreshing = 1;
         GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
         GridatributosContainer.AddObjectProperty("CmpContext", sPrefix);
         GridatributosContainer.AddObjectProperty("InMasterPage", "false");
         GridatributosContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridatributosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridatributosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridatributosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Backcolorstyle), 1, 0, ".", "")));
         GridatributosContainer.PageSize = subGridatributos_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_553( ) ;
            GXPagingFrom3 = (int)(((subGridatributos_Rows==0) ? 1 : GRIDATRIBUTOS_nFirstRecordOnPage+1));
            GXPagingTo3 = (int)(((subGridatributos_Rows==0) ? 10000 : GRIDATRIBUTOS_nFirstRecordOnPage+subGridatributos_Recordsperpage( )+1));
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV13Tabela_Nome ,
                                                 A173Tabela_Nome ,
                                                 A127Sistema_Codigo ,
                                                 A174Tabela_Ativo ,
                                                 A356Atributos_TabelaCod ,
                                                 AV7Tabela_Codigo ,
                                                 A180Atributos_Ativo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H009H3 */
            pr_default.execute(1, new Object[] {n127Sistema_Codigo, A127Sistema_Codigo, AV7Tabela_Codigo, GXPagingFrom3, GXPagingTo3, GXPagingTo3, GXPagingFrom3, GXPagingFrom3});
            nGXsfl_55_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGridatributos_Rows == 0 ) || ( GRIDATRIBUTOS_nCurrentRecord < subGridatributos_Recordsperpage( ) ) ) ) )
            {
               A188Tabela_ModuloCod = H009H3_A188Tabela_ModuloCod[0];
               n188Tabela_ModuloCod = H009H3_n188Tabela_ModuloCod[0];
               A174Tabela_Ativo = H009H3_A174Tabela_Ativo[0];
               n174Tabela_Ativo = H009H3_n174Tabela_Ativo[0];
               A356Atributos_TabelaCod = H009H3_A356Atributos_TabelaCod[0];
               A180Atributos_Ativo = H009H3_A180Atributos_Ativo[0];
               A401Atributos_FK = H009H3_A401Atributos_FK[0];
               n401Atributos_FK = H009H3_n401Atributos_FK[0];
               A400Atributos_PK = H009H3_A400Atributos_PK[0];
               n400Atributos_PK = H009H3_n400Atributos_PK[0];
               A178Atributos_TipoDados = H009H3_A178Atributos_TipoDados[0];
               n178Atributos_TipoDados = H009H3_n178Atributos_TipoDados[0];
               A177Atributos_Nome = H009H3_A177Atributos_Nome[0];
               A176Atributos_Codigo = H009H3_A176Atributos_Codigo[0];
               A188Tabela_ModuloCod = H009H3_A188Tabela_ModuloCod[0];
               n188Tabela_ModuloCod = H009H3_n188Tabela_ModuloCod[0];
               A174Tabela_Ativo = H009H3_A174Tabela_Ativo[0];
               n174Tabela_Ativo = H009H3_n174Tabela_Ativo[0];
               /* Execute user event: E189H3 */
               E189H3 ();
               pr_default.readNext(1);
            }
            GRIDATRIBUTOS_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 55;
            WB9H0( ) ;
         }
         nGXsfl_55_Refreshing = 0;
      }

      protected int subGridtabelas_Pagecount( )
      {
         GRIDTABELAS_nRecordCount = subGridtabelas_Recordcount( );
         if ( ((int)((GRIDTABELAS_nRecordCount) % (subGridtabelas_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDTABELAS_nRecordCount/ (decimal)(subGridtabelas_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDTABELAS_nRecordCount/ (decimal)(subGridtabelas_Recordsperpage( ))))+1) ;
      }

      protected int subGridtabelas_Recordcount( )
      {
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV13Tabela_Nome ,
                                              A173Tabela_Nome ,
                                              A190Tabela_SistemaCod ,
                                              A127Sistema_Codigo ,
                                              A174Tabela_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV13Tabela_Nome = StringUtil.PadR( StringUtil.RTrim( AV13Tabela_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Tabela_Nome", AV13Tabela_Nome);
         /* Using cursor H009H4 */
         pr_default.execute(2, new Object[] {n127Sistema_Codigo, A127Sistema_Codigo, lV13Tabela_Nome});
         GRIDTABELAS_nRecordCount = H009H4_AGRIDTABELAS_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRIDTABELAS_nRecordCount) ;
      }

      protected int subGridtabelas_Recordsperpage( )
      {
         if ( subGridtabelas_Rows > 0 )
         {
            return subGridtabelas_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridtabelas_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDTABELAS_nFirstRecordOnPage/ (decimal)(subGridtabelas_Recordsperpage( ))))+1) ;
      }

      protected short subgridtabelas_firstpage( )
      {
         GRIDTABELAS_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, AV13Tabela_Nome, A127Sistema_Codigo, A172Tabela_Codigo, AV7Tabela_Codigo, AV15QtdTbl, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridtabelas_nextpage( )
      {
         GRIDTABELAS_nRecordCount = subGridtabelas_Recordcount( );
         if ( ( GRIDTABELAS_nRecordCount >= subGridtabelas_Recordsperpage( ) ) && ( GRIDTABELAS_nEOF == 0 ) )
         {
            GRIDTABELAS_nFirstRecordOnPage = (long)(GRIDTABELAS_nFirstRecordOnPage+subGridtabelas_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, AV13Tabela_Nome, A127Sistema_Codigo, A172Tabela_Codigo, AV7Tabela_Codigo, AV15QtdTbl, sPrefix) ;
         }
         return (short)(((GRIDTABELAS_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridtabelas_previouspage( )
      {
         if ( GRIDTABELAS_nFirstRecordOnPage >= subGridtabelas_Recordsperpage( ) )
         {
            GRIDTABELAS_nFirstRecordOnPage = (long)(GRIDTABELAS_nFirstRecordOnPage-subGridtabelas_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, AV13Tabela_Nome, A127Sistema_Codigo, A172Tabela_Codigo, AV7Tabela_Codigo, AV15QtdTbl, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridtabelas_lastpage( )
      {
         GRIDTABELAS_nRecordCount = subGridtabelas_Recordcount( );
         if ( GRIDTABELAS_nRecordCount > subGridtabelas_Recordsperpage( ) )
         {
            if ( ((int)((GRIDTABELAS_nRecordCount) % (subGridtabelas_Recordsperpage( )))) == 0 )
            {
               GRIDTABELAS_nFirstRecordOnPage = (long)(GRIDTABELAS_nRecordCount-subGridtabelas_Recordsperpage( ));
            }
            else
            {
               GRIDTABELAS_nFirstRecordOnPage = (long)(GRIDTABELAS_nRecordCount-((int)((GRIDTABELAS_nRecordCount) % (subGridtabelas_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDTABELAS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, AV13Tabela_Nome, A127Sistema_Codigo, A172Tabela_Codigo, AV7Tabela_Codigo, AV15QtdTbl, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgridtabelas_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDTABELAS_nFirstRecordOnPage = (long)(subGridtabelas_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDTABELAS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDTABELAS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridtabelas_refresh( subGridtabelas_Rows, AV13Tabela_Nome, A127Sistema_Codigo, A172Tabela_Codigo, AV7Tabela_Codigo, AV15QtdTbl, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected int subGridatributos_Pagecount( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( ((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nRecordCount/ (decimal)(subGridatributos_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nRecordCount/ (decimal)(subGridatributos_Recordsperpage( ))))+1) ;
      }

      protected int subGridatributos_Recordcount( )
      {
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV13Tabela_Nome ,
                                              A173Tabela_Nome ,
                                              A127Sistema_Codigo ,
                                              A174Tabela_Ativo ,
                                              A356Atributos_TabelaCod ,
                                              AV7Tabela_Codigo ,
                                              A180Atributos_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H009H5 */
         pr_default.execute(3, new Object[] {n127Sistema_Codigo, A127Sistema_Codigo, AV7Tabela_Codigo});
         GRIDATRIBUTOS_nRecordCount = H009H5_AGRIDATRIBUTOS_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRIDATRIBUTOS_nRecordCount) ;
      }

      protected int subGridatributos_Recordsperpage( )
      {
         if ( subGridatributos_Rows > 0 )
         {
            return subGridatributos_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridatributos_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nFirstRecordOnPage/ (decimal)(subGridatributos_Recordsperpage( ))))+1) ;
      }

      protected short subgridatributos_firstpage( )
      {
         GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV13Tabela_Nome, A127Sistema_Codigo, A176Atributos_Codigo, AV16QtdAtr, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridatributos_nextpage( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( ( GRIDATRIBUTOS_nRecordCount >= subGridatributos_Recordsperpage( ) ) && ( GRIDATRIBUTOS_nEOF == 0 ) )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nFirstRecordOnPage+subGridatributos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV13Tabela_Nome, A127Sistema_Codigo, A176Atributos_Codigo, AV16QtdAtr, sPrefix) ;
         }
         return (short)(((GRIDATRIBUTOS_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridatributos_previouspage( )
      {
         if ( GRIDATRIBUTOS_nFirstRecordOnPage >= subGridatributos_Recordsperpage( ) )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nFirstRecordOnPage-subGridatributos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV13Tabela_Nome, A127Sistema_Codigo, A176Atributos_Codigo, AV16QtdAtr, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridatributos_lastpage( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( GRIDATRIBUTOS_nRecordCount > subGridatributos_Recordsperpage( ) )
         {
            if ( ((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))) == 0 )
            {
               GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nRecordCount-subGridatributos_Recordsperpage( ));
            }
            else
            {
               GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nRecordCount-((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV13Tabela_Nome, A127Sistema_Codigo, A176Atributos_Codigo, AV16QtdAtr, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgridatributos_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(subGridatributos_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV13Tabela_Nome, A127Sistema_Codigo, A176Atributos_Codigo, AV16QtdAtr, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP9H0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E149H2 */
         E149H2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV13Tabela_Nome = StringUtil.Upper( cgiGet( edtavTabela_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Tabela_Nome", AV13Tabela_Nome);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGridtabelascurrentpage_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGridtabelascurrentpage_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGRIDTABELASCURRENTPAGE");
               GX_FocusControl = edtavGridtabelascurrentpage_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18GridTabelasCurrentPage = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18GridTabelasCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GridTabelasCurrentPage), 10, 0)));
            }
            else
            {
               AV18GridTabelasCurrentPage = (long)(context.localUtil.CToN( cgiGet( edtavGridtabelascurrentpage_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18GridTabelasCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GridTabelasCurrentPage), 10, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_42 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_42"), ",", "."));
            nRC_GXsfl_55 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_55"), ",", "."));
            AV19GridTabelasPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDTABELASPAGECOUNT"), ",", "."));
            wcpOA127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA127Sistema_Codigo"), ",", "."));
            GRIDTABELAS_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDTABELAS_nFirstRecordOnPage"), ",", "."));
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage"), ",", "."));
            GRIDTABELAS_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDTABELAS_nEOF"), ",", "."));
            GRIDATRIBUTOS_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_nEOF"), ",", "."));
            subGridtabelas_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDTABELAS_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Rows), 6, 0, ".", "")));
            subGridatributos_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
            Gridtabelaspaginationbar_Class = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Class");
            Gridtabelaspaginationbar_First = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_First");
            Gridtabelaspaginationbar_Previous = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Previous");
            Gridtabelaspaginationbar_Next = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Next");
            Gridtabelaspaginationbar_Last = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Last");
            Gridtabelaspaginationbar_Caption = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Caption");
            Gridtabelaspaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Showfirst"));
            Gridtabelaspaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Showprevious"));
            Gridtabelaspaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Shownext"));
            Gridtabelaspaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Showlast"));
            Gridtabelaspaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridtabelaspaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Pagingbuttonsposition");
            Gridtabelaspaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Pagingcaptionposition");
            Gridtabelaspaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Emptygridclass");
            Gridtabelaspaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Emptygridcaption");
            Dvpanel_unnamedtable1_Width = cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE1_Width");
            Dvpanel_unnamedtable1_Cls = cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE1_Cls");
            Dvpanel_unnamedtable1_Title = cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE1_Title");
            Dvpanel_unnamedtable1_Collapsible = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE1_Collapsible"));
            Dvpanel_unnamedtable1_Collapsed = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE1_Collapsed"));
            Dvpanel_unnamedtable1_Autowidth = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE1_Autowidth"));
            Dvpanel_unnamedtable1_Autoheight = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE1_Autoheight"));
            Dvpanel_unnamedtable1_Showcollapseicon = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE1_Showcollapseicon"));
            Dvpanel_unnamedtable1_Iconposition = cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE1_Iconposition");
            Dvpanel_unnamedtable1_Autoscroll = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE1_Autoscroll"));
            Gridtabelaspaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDTABELASPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            nGXsfl_42_idx = (short)(context.localUtil.CToN( cgiGet( subGridtabelas_Internalname+"_ROW"), ",", "."));
            sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
            SubsflControlProps_422( ) ;
            if ( nGXsfl_42_idx > 0 )
            {
               AV11UpdTabela = cgiGet( edtavUpdtabela_Internalname);
               AV12DltTabela = cgiGet( edtavDlttabela_Internalname);
               A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTabela_Codigo_Internalname), ",", "."));
               A173Tabela_Nome = StringUtil.Upper( cgiGet( edtTabela_Nome_Internalname));
               A189Tabela_ModuloDes = StringUtil.Upper( cgiGet( edtTabela_ModuloDes_Internalname));
               n189Tabela_ModuloDes = false;
            }
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME"), AV13Tabela_Nome) != 0 )
            {
               GRIDTABELAS_nFirstRecordOnPage = 0;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTABELA_NOME"), AV13Tabela_Nome) != 0 )
            {
               GRIDATRIBUTOS_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E149H2 */
         E149H2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E149H2( )
      {
         /* Start Routine */
         subGridtabelas_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDTABELAS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Rows), 6, 0, ".", "")));
         AV18GridTabelasCurrentPage = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18GridTabelasCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GridTabelasCurrentPage), 10, 0)));
         edtavGridtabelascurrentpage_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavGridtabelascurrentpage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGridtabelascurrentpage_Visible), 5, 0)));
         AV19GridTabelasPageCount = -1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19GridTabelasPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19GridTabelasPageCount), 10, 0)));
         subGridatributos_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         AV13Tabela_Nome = AV8WebSession.Get("FiltroRecebido");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Tabela_Nome", AV13Tabela_Nome);
      }

      protected void E159H2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV5Context) ;
         AV15QtdTbl = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15QtdTbl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15QtdTbl), 4, 0)));
         AV16QtdAtr = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16QtdAtr", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16QtdAtr), 4, 0)));
      }

      private void E169H2( )
      {
         /* Gridtabelas_Load Routine */
         AV11UpdTabela = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdtabela_Internalname, AV11UpdTabela);
         AV22Updtabela_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdtabela_Tooltiptext = "";
         edtavUpdtabela_Link = formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode("" +A127Sistema_Codigo) + "," + UrlEncode("" +0);
         AV12DltTabela = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDlttabela_Internalname, AV12DltTabela);
         AV23Dlttabela_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDlttabela_Tooltiptext = "";
         edtavDlttabela_Link = formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A172Tabela_Codigo) + "," + UrlEncode("" +A127Sistema_Codigo) + "," + UrlEncode("" +0);
         AV11UpdTabela = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdtabela_Internalname, AV11UpdTabela);
         AV22Updtabela_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         AV12DltTabela = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDlttabela_Internalname, AV12DltTabela);
         AV23Dlttabela_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         if ( (0==AV7Tabela_Codigo) )
         {
            AV7Tabela_Codigo = A172Tabela_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0)));
            AV8WebSession.Set("Tabela_Codigo", StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0));
         }
         AV15QtdTbl = (short)(AV15QtdTbl+1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15QtdTbl", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15QtdTbl), 4, 0)));
         edtTabela_Nome_Title = "Tabelas ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV15QtdTbl), 4, 0))+")";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 42;
         }
         sendrow_422( ) ;
         GRIDTABELAS_nCurrentRecord = (long)(GRIDTABELAS_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_42_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(42, GridtabelasRow);
         }
      }

      protected void E119H2( )
      {
         /* Gridtabelaspaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridtabelaspaginationbar_Selectedpage, "Previous") == 0 )
         {
            AV18GridTabelasCurrentPage = (long)(AV18GridTabelasCurrentPage-1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18GridTabelasCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GridTabelasCurrentPage), 10, 0)));
            subgridtabelas_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridtabelaspaginationbar_Selectedpage, "Next") == 0 )
         {
            AV18GridTabelasCurrentPage = (long)(AV18GridTabelasCurrentPage+1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18GridTabelasCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GridTabelasCurrentPage), 10, 0)));
            subgridtabelas_nextpage( ) ;
         }
         else
         {
            AV17PageToGo = (int)(NumberUtil.Val( Gridtabelaspaginationbar_Selectedpage, "."));
            AV18GridTabelasCurrentPage = AV17PageToGo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18GridTabelasCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GridTabelasCurrentPage), 10, 0)));
            subgridtabelas_gotopage( AV17PageToGo) ;
         }
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E129H2( )
      {
         /* 'DoNewTabela' Routine */
         context.wjLoc = formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +A127Sistema_Codigo) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void E139H2( )
      {
         /* 'DoNewAtributo' Routine */
         new prc_newatributo(context ).execute(  AV7Tabela_Codigo,  A127Sistema_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
      }

      protected void E179H2( )
      {
         /* Gridtabelas_Onlineactivate Routine */
         if ( AV7Tabela_Codigo != A172Tabela_Codigo )
         {
            AV7Tabela_Codigo = A172Tabela_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0)));
            AV8WebSession.Set("Tabela_Codigo", StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0));
            AV16QtdAtr = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16QtdAtr", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16QtdAtr), 4, 0)));
            gxgrGridatributos_refresh( subGridatributos_Rows, AV13Tabela_Nome, A127Sistema_Codigo, A176Atributos_Codigo, AV16QtdAtr, sPrefix) ;
         }
      }

      private void E189H3( )
      {
         /* Gridatributos_Load Routine */
         AV9UpdAtributo = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdatributo_Internalname, AV9UpdAtributo);
         AV24Updatributo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdatributo_Tooltiptext = "";
         edtavUpdatributo_Link = formatLink("atributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A176Atributos_Codigo) + "," + UrlEncode("" +A127Sistema_Codigo);
         AV10DltAtributo = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDltatributo_Internalname, AV10DltAtributo);
         AV25Dltatributo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDltatributo_Tooltiptext = "";
         edtavDltatributo_Link = formatLink("atributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A176Atributos_Codigo) + "," + UrlEncode("" +A127Sistema_Codigo);
         AV9UpdAtributo = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdatributo_Internalname, AV9UpdAtributo);
         AV24Updatributo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         AV10DltAtributo = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDltatributo_Internalname, AV10DltAtributo);
         AV25Dltatributo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         AV16QtdAtr = (short)(AV16QtdAtr+1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16QtdAtr", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16QtdAtr), 4, 0)));
         edtAtributos_Nome_Title = "Atributos ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV16QtdAtr), 4, 0))+")";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 55;
         }
         sendrow_553( ) ;
         GRIDATRIBUTOS_nCurrentRecord = (long)(GRIDATRIBUTOS_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_55_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(55, GridatributosRow);
         }
      }

      protected void wb_table1_2_9H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_9H2( true) ;
         }
         else
         {
            wb_table2_8_9H2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_9H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_20_9H2( true) ;
         }
         else
         {
            wb_table3_20_9H2( false) ;
         }
         return  ;
      }

      protected void wb_table3_20_9H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9H2e( true) ;
         }
         else
         {
            wb_table1_2_9H2e( false) ;
         }
      }

      protected void wb_table3_20_9H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVPANEL_UNNAMEDTABLE1Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"DVPANEL_UNNAMEDTABLE1Container"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_25_9H2( true) ;
         }
         else
         {
            wb_table4_25_9H2( false) ;
         }
         return  ;
      }

      protected void wb_table4_25_9H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_20_9H2e( true) ;
         }
         else
         {
            wb_table3_20_9H2e( false) ;
         }
      }

      protected void wb_table4_25_9H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_28_9H2( true) ;
         }
         else
         {
            wb_table5_28_9H2( false) ;
         }
         return  ;
      }

      protected void wb_table5_28_9H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_25_9H2e( true) ;
         }
         else
         {
            wb_table4_25_9H2e( false) ;
         }
      }

      protected void wb_table5_28_9H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(50), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            wb_table6_31_9H2( true) ;
         }
         else
         {
            wb_table6_31_9H2( false) ;
         }
         return  ;
      }

      protected void wb_table6_31_9H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableContentNoMargin'>") ;
            wb_table7_39_9H2( true) ;
         }
         else
         {
            wb_table7_39_9H2( false) ;
         }
         return  ;
      }

      protected void wb_table7_39_9H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgNewtabela_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Nova Tabela", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgNewtabela_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DONEWTABELA\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaTabelaAtributoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableContentNoMargin'>") ;
            /*  Grid Control  */
            GridatributosContainer.SetWrapped(nGXWrapped);
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridatributosContainer"+"DivS\" data-gxgridid=\"55\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridatributos_Internalname, subGridatributos_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridatributos_Backcolorstyle == 0 )
               {
                  subGridatributos_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridatributos_Class) > 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Title";
                  }
               }
               else
               {
                  subGridatributos_Titlebackstyle = 1;
                  if ( subGridatributos_Backcolorstyle == 1 )
                  {
                     subGridatributos_Titlebackcolor = subGridatributos_Allbackcolor;
                     if ( StringUtil.Len( subGridatributos_Class) > 0 )
                     {
                        subGridatributos_Linesclass = subGridatributos_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridatributos_Class) > 0 )
                     {
                        subGridatributos_Linesclass = subGridatributos_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtAtributos_Nome_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo de Dados") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PK") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "FK") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridatributosContainer = new GXWebGrid( context);
               }
               else
               {
                  GridatributosContainer.Clear();
               }
               GridatributosContainer.SetWrapped(nGXWrapped);
               GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
               GridatributosContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridatributosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Backcolorstyle), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("CmpContext", sPrefix);
               GridatributosContainer.AddObjectProperty("InMasterPage", "false");
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", context.convertURL( AV9UpdAtributo));
               GridatributosColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdatributo_Link));
               GridatributosColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdatributo_Tooltiptext));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", context.convertURL( AV10DltAtributo));
               GridatributosColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDltatributo_Link));
               GridatributosColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDltatributo_Tooltiptext));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( A177Atributos_Nome));
               GridatributosColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAtributos_Nome_Title));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( A178Atributos_TipoDados));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A400Atributos_PK));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A401Atributos_FK));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowselection), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Selectioncolor), 9, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowhovering), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Hoveringcolor), 9, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowcollapsing), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 55 )
         {
            wbEnd = 0;
            nRC_GXsfl_55 = (short)(nGXsfl_55_idx-1);
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridatributosContainer.AddObjectProperty("GRIDATRIBUTOS_nEOF", GRIDATRIBUTOS_nEOF);
               GridatributosContainer.AddObjectProperty("GRIDATRIBUTOS_nFirstRecordOnPage", GRIDATRIBUTOS_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridatributosContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Gridatributos", GridatributosContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridatributosContainerData", GridatributosContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridatributosContainerData"+"V", GridatributosContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridatributosContainerData"+"V"+"\" value='"+GridatributosContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgNewatributo_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Novo Atributo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgNewatributo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DONEWATRIBUTO\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaTabelaAtributoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_28_9H2e( true) ;
         }
         else
         {
            wb_table5_28_9H2e( false) ;
         }
      }

      protected void wb_table7_39_9H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtabelastablewithpaginationbar_Internalname, tblGridtabelastablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridtabelasContainer.SetWrapped(nGXWrapped);
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridtabelasContainer"+"DivS\" data-gxgridid=\"42\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridtabelas_Internalname, subGridtabelas_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridtabelas_Backcolorstyle == 0 )
               {
                  subGridtabelas_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                  {
                     subGridtabelas_Linesclass = subGridtabelas_Class+"Title";
                  }
               }
               else
               {
                  subGridtabelas_Titlebackstyle = 1;
                  if ( subGridtabelas_Backcolorstyle == 1 )
                  {
                     subGridtabelas_Titlebackcolor = subGridtabelas_Allbackcolor;
                     if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                     {
                        subGridtabelas_Linesclass = subGridtabelas_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                     {
                        subGridtabelas_Linesclass = subGridtabelas_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtTabela_Nome_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "M�dulo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridtabelasContainer = new GXWebGrid( context);
               }
               else
               {
                  GridtabelasContainer.Clear();
               }
               GridtabelasContainer.SetWrapped(nGXWrapped);
               GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
               GridtabelasContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridtabelasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Backcolorstyle), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("CmpContext", sPrefix);
               GridtabelasContainer.AddObjectProperty("InMasterPage", "false");
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", context.convertURL( AV11UpdTabela));
               GridtabelasColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdtabela_Link));
               GridtabelasColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdtabela_Tooltiptext));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", context.convertURL( AV12DltTabela));
               GridtabelasColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDlttabela_Link));
               GridtabelasColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDlttabela_Tooltiptext));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ".", "")));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.RTrim( A173Tabela_Nome));
               GridtabelasColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTabela_Nome_Title));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.RTrim( A189Tabela_ModuloDes));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowselection), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Selectioncolor), 9, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowhovering), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Hoveringcolor), 9, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowcollapsing), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 42 )
         {
            wbEnd = 0;
            nRC_GXsfl_42 = (short)(nGXsfl_42_idx-1);
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridtabelasContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Gridtabelas", GridtabelasContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridtabelasContainerData", GridtabelasContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridtabelasContainerData"+"V", GridtabelasContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridtabelasContainerData"+"V"+"\" value='"+GridtabelasContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDTABELASPAGINATIONBARContainer"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGridtabelascurrentpage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18GridTabelasCurrentPage), 10, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV18GridTabelasCurrentPage), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,51);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGridtabelascurrentpage_Jsonclick, 0, "Attribute", "", "", "", edtavGridtabelascurrentpage_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaTabelaAtributoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_39_9H2e( true) ;
         }
         else
         {
            wb_table7_39_9H2e( false) ;
         }
      }

      protected void wb_table6_31_9H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_nome_Internalname, "Filtrar tabelas contendo:", "", "", lblTextblocktabela_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SistemaTabelaAtributoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_42_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_nome_Internalname, StringUtil.RTrim( AV13Tabela_Nome), StringUtil.RTrim( context.localUtil.Format( AV13Tabela_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,36);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaTabelaAtributoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_31_9H2e( true) ;
         }
         else
         {
            wb_table6_31_9H2e( false) ;
         }
      }

      protected void wb_table2_8_9H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_11_9H2( true) ;
         }
         else
         {
            wb_table8_11_9H2( false) ;
         }
         return  ;
      }

      protected void wb_table8_11_9H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table9_15_9H2( true) ;
         }
         else
         {
            wb_table9_15_9H2( false) ;
         }
         return  ;
      }

      protected void wb_table9_15_9H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_9H2e( true) ;
         }
         else
         {
            wb_table2_8_9H2e( false) ;
         }
      }

      protected void wb_table9_15_9H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_15_9H2e( true) ;
         }
         else
         {
            wb_table9_15_9H2e( false) ;
         }
      }

      protected void wb_table8_11_9H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_11_9H2e( true) ;
         }
         else
         {
            wb_table8_11_9H2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A127Sistema_Codigo = Convert.ToInt32(getParm(obj,0));
         n127Sistema_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9H2( ) ;
         WS9H2( ) ;
         WE9H2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA127Sistema_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA9H2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "sistematabelaatributowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA9H2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A127Sistema_Codigo = Convert.ToInt32(getParm(obj,2));
            n127Sistema_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
         wcpOA127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA127Sistema_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A127Sistema_Codigo != wcpOA127Sistema_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA127Sistema_Codigo = A127Sistema_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA127Sistema_Codigo = cgiGet( sPrefix+"A127Sistema_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA127Sistema_Codigo) > 0 )
         {
            A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA127Sistema_Codigo), ",", "."));
            n127Sistema_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
         else
         {
            A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A127Sistema_Codigo_PARM"), ",", "."));
            n127Sistema_Codigo = false;
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA9H2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS9H2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS9H2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A127Sistema_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA127Sistema_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A127Sistema_Codigo_CTRL", StringUtil.RTrim( sCtrlA127Sistema_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE9H2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117184833");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("sistematabelaatributowc.js", "?20203117184833");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_422( )
      {
         edtavUpdtabela_Internalname = sPrefix+"vUPDTABELA_"+sGXsfl_42_idx;
         edtavDlttabela_Internalname = sPrefix+"vDLTTABELA_"+sGXsfl_42_idx;
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO_"+sGXsfl_42_idx;
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME_"+sGXsfl_42_idx;
         edtTabela_ModuloDes_Internalname = sPrefix+"TABELA_MODULODES_"+sGXsfl_42_idx;
      }

      protected void SubsflControlProps_fel_422( )
      {
         edtavUpdtabela_Internalname = sPrefix+"vUPDTABELA_"+sGXsfl_42_fel_idx;
         edtavDlttabela_Internalname = sPrefix+"vDLTTABELA_"+sGXsfl_42_fel_idx;
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO_"+sGXsfl_42_fel_idx;
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME_"+sGXsfl_42_fel_idx;
         edtTabela_ModuloDes_Internalname = sPrefix+"TABELA_MODULODES_"+sGXsfl_42_fel_idx;
      }

      protected void sendrow_422( )
      {
         SubsflControlProps_422( ) ;
         WB9H0( ) ;
         if ( ( subGridtabelas_Rows * 1 == 0 ) || ( nGXsfl_42_idx <= subGridtabelas_Recordsperpage( ) * 1 ) )
         {
            GridtabelasRow = GXWebRow.GetNew(context,GridtabelasContainer);
            if ( subGridtabelas_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridtabelas_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
               }
            }
            else if ( subGridtabelas_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridtabelas_Backstyle = 0;
               subGridtabelas_Backcolor = subGridtabelas_Allbackcolor;
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Uniform";
               }
            }
            else if ( subGridtabelas_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridtabelas_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
               }
               subGridtabelas_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridtabelas_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridtabelas_Backstyle = 1;
               if ( ((int)((nGXsfl_42_idx) % (2))) == 0 )
               {
                  subGridtabelas_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
                  {
                     subGridtabelas_Linesclass = subGridtabelas_Class+"Even";
                  }
               }
               else
               {
                  subGridtabelas_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
                  {
                     subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
                  }
               }
            }
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridtabelas_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_42_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV11UpdTabela_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV11UpdTabela))&&String.IsNullOrEmpty(StringUtil.RTrim( AV22Updtabela_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV11UpdTabela)));
            GridtabelasRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdtabela_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV11UpdTabela)) ? AV22Updtabela_GXI : context.PathToRelativeUrl( AV11UpdTabela)),(String)edtavUpdtabela_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdtabela_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV11UpdTabela_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV12DltTabela_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV12DltTabela))&&String.IsNullOrEmpty(StringUtil.RTrim( AV23Dlttabela_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV12DltTabela)));
            GridtabelasRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDlttabela_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV12DltTabela)) ? AV23Dlttabela_GXI : context.PathToRelativeUrl( AV12DltTabela)),(String)edtavDlttabela_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDlttabela_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV12DltTabela_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Nome_Internalname,StringUtil.RTrim( A173Tabela_Nome),StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_ModuloDes_Internalname,StringUtil.RTrim( A189Tabela_ModuloDes),StringUtil.RTrim( context.localUtil.Format( A189Tabela_ModuloDes, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_ModuloDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)42,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_CODIGO"+"_"+sGXsfl_42_idx, GetSecureSignedToken( sPrefix+sGXsfl_42_idx, context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_TABELA_NOME"+"_"+sGXsfl_42_idx, GetSecureSignedToken( sPrefix+sGXsfl_42_idx, StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!"))));
            GridtabelasContainer.AddRow(GridtabelasRow);
            nGXsfl_42_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_42_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_42_idx+1));
            sGXsfl_42_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_42_idx), 4, 0)), 4, "0");
            SubsflControlProps_422( ) ;
         }
         /* End function sendrow_422 */
      }

      protected void SubsflControlProps_553( )
      {
         edtavUpdatributo_Internalname = sPrefix+"vUPDATRIBUTO_"+sGXsfl_55_idx;
         edtavDltatributo_Internalname = sPrefix+"vDLTATRIBUTO_"+sGXsfl_55_idx;
         edtAtributos_TabelaCod_Internalname = sPrefix+"ATRIBUTOS_TABELACOD_"+sGXsfl_55_idx;
         edtAtributos_Codigo_Internalname = sPrefix+"ATRIBUTOS_CODIGO_"+sGXsfl_55_idx;
         edtAtributos_Nome_Internalname = sPrefix+"ATRIBUTOS_NOME_"+sGXsfl_55_idx;
         cmbAtributos_TipoDados_Internalname = sPrefix+"ATRIBUTOS_TIPODADOS_"+sGXsfl_55_idx;
         chkAtributos_PK_Internalname = sPrefix+"ATRIBUTOS_PK_"+sGXsfl_55_idx;
         chkAtributos_FK_Internalname = sPrefix+"ATRIBUTOS_FK_"+sGXsfl_55_idx;
      }

      protected void SubsflControlProps_fel_553( )
      {
         edtavUpdatributo_Internalname = sPrefix+"vUPDATRIBUTO_"+sGXsfl_55_fel_idx;
         edtavDltatributo_Internalname = sPrefix+"vDLTATRIBUTO_"+sGXsfl_55_fel_idx;
         edtAtributos_TabelaCod_Internalname = sPrefix+"ATRIBUTOS_TABELACOD_"+sGXsfl_55_fel_idx;
         edtAtributos_Codigo_Internalname = sPrefix+"ATRIBUTOS_CODIGO_"+sGXsfl_55_fel_idx;
         edtAtributos_Nome_Internalname = sPrefix+"ATRIBUTOS_NOME_"+sGXsfl_55_fel_idx;
         cmbAtributos_TipoDados_Internalname = sPrefix+"ATRIBUTOS_TIPODADOS_"+sGXsfl_55_fel_idx;
         chkAtributos_PK_Internalname = sPrefix+"ATRIBUTOS_PK_"+sGXsfl_55_fel_idx;
         chkAtributos_FK_Internalname = sPrefix+"ATRIBUTOS_FK_"+sGXsfl_55_fel_idx;
      }

      protected void sendrow_553( )
      {
         SubsflControlProps_553( ) ;
         WB9H0( ) ;
         if ( ( subGridatributos_Rows * 1 == 0 ) || ( nGXsfl_55_idx <= subGridatributos_Recordsperpage( ) * 1 ) )
         {
            GridatributosRow = GXWebRow.GetNew(context,GridatributosContainer);
            if ( subGridatributos_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridatributos_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Odd";
               }
            }
            else if ( subGridatributos_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridatributos_Backstyle = 0;
               subGridatributos_Backcolor = subGridatributos_Allbackcolor;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Uniform";
               }
            }
            else if ( subGridatributos_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridatributos_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Odd";
               }
               subGridatributos_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridatributos_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridatributos_Backstyle = 1;
               if ( ((int)((nGXsfl_55_idx) % (2))) == 0 )
               {
                  subGridatributos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Even";
                  }
               }
               else
               {
                  subGridatributos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Odd";
                  }
               }
            }
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridatributos_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_55_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV9UpdAtributo_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV9UpdAtributo))&&String.IsNullOrEmpty(StringUtil.RTrim( AV24Updatributo_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV9UpdAtributo)));
            GridatributosRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdatributo_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV9UpdAtributo)) ? AV24Updatributo_GXI : context.PathToRelativeUrl( AV9UpdAtributo)),(String)edtavUpdatributo_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdatributo_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV9UpdAtributo_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV10DltAtributo_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV10DltAtributo))&&String.IsNullOrEmpty(StringUtil.RTrim( AV25Dltatributo_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV10DltAtributo)));
            GridatributosRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDltatributo_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV10DltAtributo)) ? AV25Dltatributo_GXI : context.PathToRelativeUrl( AV10DltAtributo)),(String)edtavDltatributo_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDltatributo_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV10DltAtributo_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_TabelaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_TabelaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)55,(short)1,(short)-1,(short)0,(bool)false,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)55,(short)1,(short)-1,(short)0,(bool)false,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_Nome_Internalname,StringUtil.RTrim( A177Atributos_Nome),StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)55,(short)1,(short)-1,(short)-1,(bool)false,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_55_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "ATRIBUTOS_TIPODADOS_" + sGXsfl_55_idx;
               cmbAtributos_TipoDados.Name = GXCCtl;
               cmbAtributos_TipoDados.WebTags = "";
               cmbAtributos_TipoDados.addItem("", "Desconhecido", 0);
               cmbAtributos_TipoDados.addItem("N", "Numeric", 0);
               cmbAtributos_TipoDados.addItem("C", "Character", 0);
               cmbAtributos_TipoDados.addItem("VC", "Varchar", 0);
               cmbAtributos_TipoDados.addItem("D", "Date", 0);
               cmbAtributos_TipoDados.addItem("DT", "Date Time", 0);
               cmbAtributos_TipoDados.addItem("Bool", "Boolean", 0);
               cmbAtributos_TipoDados.addItem("Blob", "Blob", 0);
               cmbAtributos_TipoDados.addItem("Outr", "Outros", 0);
               if ( cmbAtributos_TipoDados.ItemCount > 0 )
               {
                  A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
                  n178Atributos_TipoDados = false;
               }
            }
            /* ComboBox */
            GridatributosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbAtributos_TipoDados,(String)cmbAtributos_TipoDados_Internalname,StringUtil.RTrim( A178Atributos_TipoDados),(short)1,(String)cmbAtributos_TipoDados_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)false});
            cmbAtributos_TipoDados.CurrentValue = StringUtil.RTrim( A178Atributos_TipoDados);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbAtributos_TipoDados_Internalname, "Values", (String)(cmbAtributos_TipoDados.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridatributosRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAtributos_PK_Internalname,StringUtil.BoolToStr( A400Atributos_PK),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridatributosRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAtributos_FK_Internalname,StringUtil.BoolToStr( A401Atributos_FK),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GridatributosContainer.AddRow(GridatributosRow);
            nGXsfl_55_idx = (short)(((subGridatributos_Islastpage==1)&&(nGXsfl_55_idx+1>subGridatributos_Recordsperpage( )) ? 1 : nGXsfl_55_idx+1));
            sGXsfl_55_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_55_idx), 4, 0)), 4, "0");
            SubsflControlProps_553( ) ;
         }
         /* End function sendrow_553 */
      }

      protected void init_default_properties( )
      {
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTableheader_Internalname = sPrefix+"TABLEHEADER";
         lblTextblocktabela_nome_Internalname = sPrefix+"TEXTBLOCKTABELA_NOME";
         edtavTabela_nome_Internalname = sPrefix+"vTABELA_NOME";
         tblUnnamedtable3_Internalname = sPrefix+"UNNAMEDTABLE3";
         edtavUpdtabela_Internalname = sPrefix+"vUPDTABELA";
         edtavDlttabela_Internalname = sPrefix+"vDLTTABELA";
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO";
         edtTabela_Nome_Internalname = sPrefix+"TABELA_NOME";
         edtTabela_ModuloDes_Internalname = sPrefix+"TABELA_MODULODES";
         Gridtabelaspaginationbar_Internalname = sPrefix+"GRIDTABELASPAGINATIONBAR";
         edtavGridtabelascurrentpage_Internalname = sPrefix+"vGRIDTABELASCURRENTPAGE";
         tblGridtabelastablewithpaginationbar_Internalname = sPrefix+"GRIDTABELASTABLEWITHPAGINATIONBAR";
         imgNewtabela_Internalname = sPrefix+"NEWTABELA";
         edtavUpdatributo_Internalname = sPrefix+"vUPDATRIBUTO";
         edtavDltatributo_Internalname = sPrefix+"vDLTATRIBUTO";
         edtAtributos_TabelaCod_Internalname = sPrefix+"ATRIBUTOS_TABELACOD";
         edtAtributos_Codigo_Internalname = sPrefix+"ATRIBUTOS_CODIGO";
         edtAtributos_Nome_Internalname = sPrefix+"ATRIBUTOS_NOME";
         cmbAtributos_TipoDados_Internalname = sPrefix+"ATRIBUTOS_TIPODADOS";
         chkAtributos_PK_Internalname = sPrefix+"ATRIBUTOS_PK";
         chkAtributos_FK_Internalname = sPrefix+"ATRIBUTOS_FK";
         imgNewatributo_Internalname = sPrefix+"NEWATRIBUTO";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Dvpanel_unnamedtable1_Internalname = sPrefix+"DVPANEL_UNNAMEDTABLE1";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         tblTablemain_Internalname = sPrefix+"TABLEMAIN";
         Form.Internalname = sPrefix+"FORM";
         subGridtabelas_Internalname = sPrefix+"GRIDTABELAS";
         subGridatributos_Internalname = sPrefix+"GRIDATRIBUTOS";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbAtributos_TipoDados_Jsonclick = "";
         edtAtributos_Nome_Jsonclick = "";
         edtAtributos_Codigo_Jsonclick = "";
         edtAtributos_TabelaCod_Jsonclick = "";
         edtTabela_ModuloDes_Jsonclick = "";
         edtTabela_Nome_Jsonclick = "";
         edtTabela_Codigo_Jsonclick = "";
         edtavTabela_nome_Jsonclick = "";
         edtavGridtabelascurrentpage_Jsonclick = "";
         subGridtabelas_Allowcollapsing = 0;
         subGridtabelas_Hoveringcolor = (int)(0xEEFAEE);
         subGridtabelas_Allowhovering = -1;
         subGridtabelas_Selectioncolor = (int)(0xC4F0C4);
         subGridtabelas_Allowselection = 1;
         edtavDlttabela_Tooltiptext = "";
         edtavDlttabela_Link = "";
         edtavUpdtabela_Tooltiptext = "";
         edtavUpdtabela_Link = "";
         edtTabela_Nome_Title = "Tabela";
         subGridtabelas_Class = "WorkWithBorder WorkWith";
         subGridatributos_Allowcollapsing = 0;
         subGridatributos_Allowselection = 0;
         edtavDltatributo_Tooltiptext = "";
         edtavDltatributo_Link = "";
         edtavUpdatributo_Tooltiptext = "";
         edtavUpdatributo_Link = "";
         edtAtributos_Nome_Title = "Atributo";
         subGridatributos_Class = "WorkWithBorder WorkWith";
         edtavGridtabelascurrentpage_Visible = 1;
         subGridatributos_Backcolorstyle = 3;
         subGridtabelas_Backcolorstyle = 3;
         chkAtributos_FK.Caption = "";
         chkAtributos_PK.Caption = "";
         Dvpanel_unnamedtable1_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Iconposition = "left";
         Dvpanel_unnamedtable1_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_unnamedtable1_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Title = "Tabelas e Atributos";
         Dvpanel_unnamedtable1_Cls = "GXUI-DVelop-Panel";
         Dvpanel_unnamedtable1_Width = "100%";
         Gridtabelaspaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridtabelaspaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridtabelaspaginationbar_Pagingcaptionposition = "Left";
         Gridtabelaspaginationbar_Pagingbuttonsposition = "Right";
         Gridtabelaspaginationbar_Pagestoshow = 5;
         Gridtabelaspaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridtabelaspaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridtabelaspaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridtabelaspaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridtabelaspaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridtabelaspaginationbar_Last = "�|";
         Gridtabelaspaginationbar_Next = "�";
         Gridtabelaspaginationbar_Previous = "�";
         Gridtabelaspaginationbar_First = "|�";
         Gridtabelaspaginationbar_Class = "PaginationBar";
         subGridatributos_Rows = 0;
         subGridtabelas_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15QtdTbl',fld:'vQTDTBL',pic:'ZZZ9',nv:0},{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV13Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV15QtdTbl',fld:'vQTDTBL',pic:'ZZZ9',nv:0},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRIDTABELAS.LOAD","{handler:'E169H2',iparms:[{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15QtdTbl',fld:'vQTDTBL',pic:'ZZZ9',nv:0}],oparms:[{av:'AV11UpdTabela',fld:'vUPDTABELA',pic:'',nv:''},{av:'edtavUpdtabela_Tooltiptext',ctrl:'vUPDTABELA',prop:'Tooltiptext'},{av:'edtavUpdtabela_Link',ctrl:'vUPDTABELA',prop:'Link'},{av:'AV12DltTabela',fld:'vDLTTABELA',pic:'',nv:''},{av:'edtavDlttabela_Tooltiptext',ctrl:'vDLTTABELA',prop:'Tooltiptext'},{av:'edtavDlttabela_Link',ctrl:'vDLTTABELA',prop:'Link'},{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15QtdTbl',fld:'vQTDTBL',pic:'ZZZ9',nv:0},{av:'edtTabela_Nome_Title',ctrl:'TABELA_NOME',prop:'Title'}]}");
         setEventMetadata("GRIDATRIBUTOS.LOAD","{handler:'E189H3',iparms:[{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0}],oparms:[{av:'AV9UpdAtributo',fld:'vUPDATRIBUTO',pic:'',nv:''},{av:'edtavUpdatributo_Tooltiptext',ctrl:'vUPDATRIBUTO',prop:'Tooltiptext'},{av:'edtavUpdatributo_Link',ctrl:'vUPDATRIBUTO',prop:'Link'},{av:'AV10DltAtributo',fld:'vDLTATRIBUTO',pic:'',nv:''},{av:'edtavDltatributo_Tooltiptext',ctrl:'vDLTATRIBUTO',prop:'Tooltiptext'},{av:'edtavDltatributo_Link',ctrl:'vDLTATRIBUTO',prop:'Link'},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0},{av:'edtAtributos_Nome_Title',ctrl:'ATRIBUTOS_NOME',prop:'Title'}]}");
         setEventMetadata("GRIDTABELASPAGINATIONBAR.CHANGEPAGE","{handler:'E119H2',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'subGridtabelas_Rows',nv:0},{av:'AV13Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15QtdTbl',fld:'vQTDTBL',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridtabelaspaginationbar_Selectedpage',ctrl:'GRIDTABELASPAGINATIONBAR',prop:'SelectedPage'},{av:'AV18GridTabelasCurrentPage',fld:'vGRIDTABELASCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}],oparms:[{av:'AV18GridTabelasCurrentPage',fld:'vGRIDTABELASCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("'DONEWTABELA'","{handler:'E129H2',iparms:[{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DONEWATRIBUTO'","{handler:'E139H2',iparms:[{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("GRIDTABELAS.ONLINEACTIVATE","{handler:'E179H2',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV13Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRIDATRIBUTOS_FIRSTPAGE","{handler:'subgridatributos_firstpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV13Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV15QtdTbl',fld:'vQTDTBL',pic:'ZZZ9',nv:0},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRIDATRIBUTOS_PREVPAGE","{handler:'subgridatributos_previouspage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV13Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV15QtdTbl',fld:'vQTDTBL',pic:'ZZZ9',nv:0},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRIDATRIBUTOS_NEXTPAGE","{handler:'subgridatributos_nextpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV13Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV15QtdTbl',fld:'vQTDTBL',pic:'ZZZ9',nv:0},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRIDATRIBUTOS_LASTPAGE","{handler:'subgridatributos_lastpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV13Tabela_Nome',fld:'vTABELA_NOME',pic:'@!',nv:''},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV15QtdTbl',fld:'vQTDTBL',pic:'ZZZ9',nv:0},{av:'AV16QtdAtr',fld:'vQTDATR',pic:'ZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridtabelaspaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV13Tabela_Nome = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV11UpdTabela = "";
         AV22Updtabela_GXI = "";
         AV12DltTabela = "";
         AV23Dlttabela_GXI = "";
         A173Tabela_Nome = "";
         A189Tabela_ModuloDes = "";
         AV9UpdAtributo = "";
         AV24Updatributo_GXI = "";
         AV10DltAtributo = "";
         AV25Dltatributo_GXI = "";
         A177Atributos_Nome = "";
         A178Atributos_TipoDados = "";
         GXCCtl = "";
         GridtabelasContainer = new GXWebGrid( context);
         GridatributosContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV13Tabela_Nome = "";
         H009H2_A188Tabela_ModuloCod = new int[1] ;
         H009H2_n188Tabela_ModuloCod = new bool[] {false} ;
         H009H2_A190Tabela_SistemaCod = new int[1] ;
         H009H2_A174Tabela_Ativo = new bool[] {false} ;
         H009H2_n174Tabela_Ativo = new bool[] {false} ;
         H009H2_A189Tabela_ModuloDes = new String[] {""} ;
         H009H2_n189Tabela_ModuloDes = new bool[] {false} ;
         H009H2_A173Tabela_Nome = new String[] {""} ;
         H009H2_A172Tabela_Codigo = new int[1] ;
         H009H3_A188Tabela_ModuloCod = new int[1] ;
         H009H3_n188Tabela_ModuloCod = new bool[] {false} ;
         H009H3_A127Sistema_Codigo = new int[1] ;
         H009H3_n127Sistema_Codigo = new bool[] {false} ;
         H009H3_A174Tabela_Ativo = new bool[] {false} ;
         H009H3_n174Tabela_Ativo = new bool[] {false} ;
         H009H3_A356Atributos_TabelaCod = new int[1] ;
         H009H3_A180Atributos_Ativo = new bool[] {false} ;
         H009H3_A401Atributos_FK = new bool[] {false} ;
         H009H3_n401Atributos_FK = new bool[] {false} ;
         H009H3_A400Atributos_PK = new bool[] {false} ;
         H009H3_n400Atributos_PK = new bool[] {false} ;
         H009H3_A178Atributos_TipoDados = new String[] {""} ;
         H009H3_n178Atributos_TipoDados = new bool[] {false} ;
         H009H3_A177Atributos_Nome = new String[] {""} ;
         H009H3_A176Atributos_Codigo = new int[1] ;
         H009H4_AGRIDTABELAS_nRecordCount = new long[1] ;
         H009H5_AGRIDATRIBUTOS_nRecordCount = new long[1] ;
         AV8WebSession = context.GetSession();
         AV5Context = new wwpbaseobjects.SdtWWPContext(context);
         GridtabelasRow = new GXWebRow();
         GridatributosRow = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         imgNewtabela_Jsonclick = "";
         subGridatributos_Linesclass = "";
         GridatributosColumn = new GXWebColumn();
         imgNewatributo_Jsonclick = "";
         subGridtabelas_Linesclass = "";
         GridtabelasColumn = new GXWebColumn();
         lblTextblocktabela_nome_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA127Sistema_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistematabelaatributowc__default(),
            new Object[][] {
                new Object[] {
               H009H2_A188Tabela_ModuloCod, H009H2_n188Tabela_ModuloCod, H009H2_A190Tabela_SistemaCod, H009H2_A174Tabela_Ativo, H009H2_A189Tabela_ModuloDes, H009H2_n189Tabela_ModuloDes, H009H2_A173Tabela_Nome, H009H2_A172Tabela_Codigo
               }
               , new Object[] {
               H009H3_A188Tabela_ModuloCod, H009H3_n188Tabela_ModuloCod, H009H3_A127Sistema_Codigo, H009H3_n127Sistema_Codigo, H009H3_A174Tabela_Ativo, H009H3_n174Tabela_Ativo, H009H3_A356Atributos_TabelaCod, H009H3_A180Atributos_Ativo, H009H3_A401Atributos_FK, H009H3_n401Atributos_FK,
               H009H3_A400Atributos_PK, H009H3_n400Atributos_PK, H009H3_A178Atributos_TipoDados, H009H3_n178Atributos_TipoDados, H009H3_A177Atributos_Nome, H009H3_A176Atributos_Codigo
               }
               , new Object[] {
               H009H4_AGRIDTABELAS_nRecordCount
               }
               , new Object[] {
               H009H5_AGRIDATRIBUTOS_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_42 ;
      private short nGXsfl_42_idx=1 ;
      private short AV15QtdTbl ;
      private short nRC_GXsfl_55 ;
      private short nGXsfl_55_idx=1 ;
      private short AV16QtdAtr ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRIDTABELAS_nEOF ;
      private short GRIDATRIBUTOS_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_42_Refreshing=0 ;
      private short subGridtabelas_Backcolorstyle ;
      private short nGXsfl_55_Refreshing=0 ;
      private short subGridatributos_Backcolorstyle ;
      private short subGridatributos_Titlebackstyle ;
      private short subGridatributos_Allowselection ;
      private short subGridatributos_Allowhovering ;
      private short subGridatributos_Allowcollapsing ;
      private short subGridatributos_Collapsed ;
      private short subGridtabelas_Titlebackstyle ;
      private short subGridtabelas_Allowselection ;
      private short subGridtabelas_Allowhovering ;
      private short subGridtabelas_Allowcollapsing ;
      private short subGridtabelas_Collapsed ;
      private short subGridtabelas_Backstyle ;
      private short subGridatributos_Backstyle ;
      private int A127Sistema_Codigo ;
      private int wcpOA127Sistema_Codigo ;
      private int subGridtabelas_Rows ;
      private int A172Tabela_Codigo ;
      private int AV7Tabela_Codigo ;
      private int subGridatributos_Rows ;
      private int A176Atributos_Codigo ;
      private int Gridtabelaspaginationbar_Pagestoshow ;
      private int A356Atributos_TabelaCod ;
      private int subGridtabelas_Islastpage ;
      private int subGridatributos_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A190Tabela_SistemaCod ;
      private int A188Tabela_ModuloCod ;
      private int GXPagingFrom3 ;
      private int GXPagingTo3 ;
      private int edtavGridtabelascurrentpage_Visible ;
      private int AV17PageToGo ;
      private int subGridatributos_Titlebackcolor ;
      private int subGridatributos_Allbackcolor ;
      private int subGridatributos_Selectioncolor ;
      private int subGridatributos_Hoveringcolor ;
      private int subGridtabelas_Titlebackcolor ;
      private int subGridtabelas_Allbackcolor ;
      private int subGridtabelas_Selectioncolor ;
      private int subGridtabelas_Hoveringcolor ;
      private int idxLst ;
      private int subGridtabelas_Backcolor ;
      private int subGridatributos_Backcolor ;
      private long GRIDTABELAS_nFirstRecordOnPage ;
      private long GRIDATRIBUTOS_nFirstRecordOnPage ;
      private long AV19GridTabelasPageCount ;
      private long GRIDTABELAS_nCurrentRecord ;
      private long GRIDATRIBUTOS_nCurrentRecord ;
      private long GRIDTABELAS_nRecordCount ;
      private long GRIDATRIBUTOS_nRecordCount ;
      private long AV18GridTabelasCurrentPage ;
      private String Gridtabelaspaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_42_idx="0001" ;
      private String AV13Tabela_Nome ;
      private String GXKey ;
      private String sGXsfl_55_idx="0001" ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridtabelaspaginationbar_Class ;
      private String Gridtabelaspaginationbar_First ;
      private String Gridtabelaspaginationbar_Previous ;
      private String Gridtabelaspaginationbar_Next ;
      private String Gridtabelaspaginationbar_Last ;
      private String Gridtabelaspaginationbar_Caption ;
      private String Gridtabelaspaginationbar_Pagingbuttonsposition ;
      private String Gridtabelaspaginationbar_Pagingcaptionposition ;
      private String Gridtabelaspaginationbar_Emptygridclass ;
      private String Gridtabelaspaginationbar_Emptygridcaption ;
      private String Dvpanel_unnamedtable1_Width ;
      private String Dvpanel_unnamedtable1_Cls ;
      private String Dvpanel_unnamedtable1_Title ;
      private String Dvpanel_unnamedtable1_Iconposition ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavTabela_nome_Internalname ;
      private String edtavUpdtabela_Internalname ;
      private String edtavDlttabela_Internalname ;
      private String edtTabela_Codigo_Internalname ;
      private String A173Tabela_Nome ;
      private String edtTabela_Nome_Internalname ;
      private String A189Tabela_ModuloDes ;
      private String edtTabela_ModuloDes_Internalname ;
      private String edtavUpdatributo_Internalname ;
      private String edtavDltatributo_Internalname ;
      private String edtAtributos_TabelaCod_Internalname ;
      private String edtAtributos_Codigo_Internalname ;
      private String A177Atributos_Nome ;
      private String edtAtributos_Nome_Internalname ;
      private String cmbAtributos_TipoDados_Internalname ;
      private String A178Atributos_TipoDados ;
      private String chkAtributos_PK_Internalname ;
      private String chkAtributos_FK_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV13Tabela_Nome ;
      private String edtavGridtabelascurrentpage_Internalname ;
      private String subGridtabelas_Internalname ;
      private String edtavUpdtabela_Tooltiptext ;
      private String edtavUpdtabela_Link ;
      private String edtavDlttabela_Tooltiptext ;
      private String edtavDlttabela_Link ;
      private String edtTabela_Nome_Title ;
      private String edtavUpdatributo_Tooltiptext ;
      private String edtavUpdatributo_Link ;
      private String edtavDltatributo_Tooltiptext ;
      private String edtavDltatributo_Link ;
      private String edtAtributos_Nome_Title ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String TempTags ;
      private String imgNewtabela_Internalname ;
      private String imgNewtabela_Jsonclick ;
      private String subGridatributos_Internalname ;
      private String subGridatributos_Class ;
      private String subGridatributos_Linesclass ;
      private String imgNewatributo_Internalname ;
      private String imgNewatributo_Jsonclick ;
      private String tblGridtabelastablewithpaginationbar_Internalname ;
      private String subGridtabelas_Class ;
      private String subGridtabelas_Linesclass ;
      private String edtavGridtabelascurrentpage_Jsonclick ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTextblocktabela_nome_Internalname ;
      private String lblTextblocktabela_nome_Jsonclick ;
      private String edtavTabela_nome_Jsonclick ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String tblTableactions_Internalname ;
      private String sCtrlA127Sistema_Codigo ;
      private String sGXsfl_42_fel_idx="0001" ;
      private String ROClassString ;
      private String edtTabela_Codigo_Jsonclick ;
      private String edtTabela_Nome_Jsonclick ;
      private String edtTabela_ModuloDes_Jsonclick ;
      private String sGXsfl_55_fel_idx="0001" ;
      private String edtAtributos_TabelaCod_Jsonclick ;
      private String edtAtributos_Codigo_Jsonclick ;
      private String edtAtributos_Nome_Jsonclick ;
      private String cmbAtributos_TipoDados_Jsonclick ;
      private String Gridtabelaspaginationbar_Internalname ;
      private String Dvpanel_unnamedtable1_Internalname ;
      private bool entryPointCalled ;
      private bool n127Sistema_Codigo ;
      private bool toggleJsOutput ;
      private bool Gridtabelaspaginationbar_Showfirst ;
      private bool Gridtabelaspaginationbar_Showprevious ;
      private bool Gridtabelaspaginationbar_Shownext ;
      private bool Gridtabelaspaginationbar_Showlast ;
      private bool Dvpanel_unnamedtable1_Collapsible ;
      private bool Dvpanel_unnamedtable1_Collapsed ;
      private bool Dvpanel_unnamedtable1_Autowidth ;
      private bool Dvpanel_unnamedtable1_Autoheight ;
      private bool Dvpanel_unnamedtable1_Showcollapseicon ;
      private bool Dvpanel_unnamedtable1_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n189Tabela_ModuloDes ;
      private bool n178Atributos_TipoDados ;
      private bool A400Atributos_PK ;
      private bool n400Atributos_PK ;
      private bool A401Atributos_FK ;
      private bool n401Atributos_FK ;
      private bool A174Tabela_Ativo ;
      private bool n188Tabela_ModuloCod ;
      private bool n174Tabela_Ativo ;
      private bool A180Atributos_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV11UpdTabela_IsBlob ;
      private bool AV12DltTabela_IsBlob ;
      private bool AV9UpdAtributo_IsBlob ;
      private bool AV10DltAtributo_IsBlob ;
      private String AV22Updtabela_GXI ;
      private String AV23Dlttabela_GXI ;
      private String AV24Updatributo_GXI ;
      private String AV25Dltatributo_GXI ;
      private String AV11UpdTabela ;
      private String AV12DltTabela ;
      private String AV9UpdAtributo ;
      private String AV10DltAtributo ;
      private IGxSession AV8WebSession ;
      private GXWebGrid GridtabelasContainer ;
      private GXWebGrid GridatributosContainer ;
      private GXWebRow GridtabelasRow ;
      private GXWebRow GridatributosRow ;
      private GXWebColumn GridatributosColumn ;
      private GXWebColumn GridtabelasColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbAtributos_TipoDados ;
      private GXCheckbox chkAtributos_PK ;
      private GXCheckbox chkAtributos_FK ;
      private IDataStoreProvider pr_default ;
      private int[] H009H2_A188Tabela_ModuloCod ;
      private bool[] H009H2_n188Tabela_ModuloCod ;
      private int[] H009H2_A190Tabela_SistemaCod ;
      private bool[] H009H2_A174Tabela_Ativo ;
      private bool[] H009H2_n174Tabela_Ativo ;
      private String[] H009H2_A189Tabela_ModuloDes ;
      private bool[] H009H2_n189Tabela_ModuloDes ;
      private String[] H009H2_A173Tabela_Nome ;
      private int[] H009H2_A172Tabela_Codigo ;
      private int[] H009H3_A188Tabela_ModuloCod ;
      private bool[] H009H3_n188Tabela_ModuloCod ;
      private int[] H009H3_A127Sistema_Codigo ;
      private bool[] H009H3_n127Sistema_Codigo ;
      private bool[] H009H3_A174Tabela_Ativo ;
      private bool[] H009H3_n174Tabela_Ativo ;
      private int[] H009H3_A356Atributos_TabelaCod ;
      private bool[] H009H3_A180Atributos_Ativo ;
      private bool[] H009H3_A401Atributos_FK ;
      private bool[] H009H3_n401Atributos_FK ;
      private bool[] H009H3_A400Atributos_PK ;
      private bool[] H009H3_n400Atributos_PK ;
      private String[] H009H3_A178Atributos_TipoDados ;
      private bool[] H009H3_n178Atributos_TipoDados ;
      private String[] H009H3_A177Atributos_Nome ;
      private int[] H009H3_A176Atributos_Codigo ;
      private long[] H009H4_AGRIDTABELAS_nRecordCount ;
      private long[] H009H5_AGRIDATRIBUTOS_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV5Context ;
   }

   public class sistematabelaatributowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H009H2( IGxContext context ,
                                             String AV13Tabela_Nome ,
                                             String A173Tabela_Nome ,
                                             int A190Tabela_SistemaCod ,
                                             int A127Sistema_Codigo ,
                                             bool A174Tabela_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Tabela_ModuloCod] AS Tabela_ModuloCod, T1.[Tabela_SistemaCod], T1.[Tabela_Ativo], T2.[Modulo_Nome] AS Tabela_ModuloDes, T1.[Tabela_Nome], T1.[Tabela_Codigo]";
         sFromString = " FROM ([Tabela] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Tabela_ModuloCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Tabela_SistemaCod] = @Sistema_Codigo)";
         sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
         sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13Tabela_Nome)) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV13Tabela_Nome + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         sOrderString = sOrderString + " ORDER BY T1.[Tabela_Nome]";
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H009H3( IGxContext context ,
                                             String AV13Tabela_Nome ,
                                             String A173Tabela_Nome ,
                                             int A127Sistema_Codigo ,
                                             bool A174Tabela_Ativo ,
                                             int A356Atributos_TabelaCod ,
                                             int AV7Tabela_Codigo ,
                                             bool A180Atributos_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [7] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Tabela_ModuloCod] AS Tabela_ModuloCod, T3.[Sistema_Codigo], T2.[Tabela_Ativo], T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, T1.[Atributos_Ativo], T1.[Atributos_FK], T1.[Atributos_PK], T1.[Atributos_TipoDados], T1.[Atributos_Nome], T1.[Atributos_Codigo]";
         sFromString = " FROM (([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T2.[Tabela_ModuloCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T3.[Sistema_Codigo] = @Sistema_Codigo)";
         sWhereString = sWhereString + " and (T2.[Tabela_Ativo] = 1)";
         sWhereString = sWhereString + " and (T1.[Atributos_TabelaCod] = @AV7Tabela_Codigo)";
         sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 1)";
         sOrderString = sOrderString + " ORDER BY T1.[Atributos_Nome]";
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom3" + " AND " + "@GXPagingTo3" + " OR " + "@GXPagingTo3" + " < " + "@GXPagingFrom3" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom3";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H009H4( IGxContext context ,
                                             String AV13Tabela_Nome ,
                                             String A173Tabela_Nome ,
                                             int A190Tabela_SistemaCod ,
                                             int A127Sistema_Codigo ,
                                             bool A174Tabela_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [2] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Tabela] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Tabela_ModuloCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Tabela_SistemaCod] = @Sistema_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[Tabela_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (T1.[Tabela_Ativo] = 1)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13Tabela_Nome)) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV13Tabela_Nome + '%')";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_H009H5( IGxContext context ,
                                             String AV13Tabela_Nome ,
                                             String A173Tabela_Nome ,
                                             int A127Sistema_Codigo ,
                                             bool A174Tabela_Ativo ,
                                             int A356Atributos_TabelaCod ,
                                             int AV7Tabela_Codigo ,
                                             bool A180Atributos_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [2] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T2.[Tabela_ModuloCod])";
         scmdbuf = scmdbuf + " WHERE (T3.[Sistema_Codigo] = @Sistema_Codigo)";
         scmdbuf = scmdbuf + " and (T2.[Tabela_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (T1.[Atributos_TabelaCod] = @AV7Tabela_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[Atributos_Ativo] = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H009H2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] );
               case 1 :
                     return conditional_H009H3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (bool)dynConstraints[6] );
               case 2 :
                     return conditional_H009H4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] );
               case 3 :
                     return conditional_H009H5(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (bool)dynConstraints[6] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009H2 ;
          prmH009H2 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV13Tabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH009H3 ;
          prmH009H3 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7Tabela_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom3",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo3",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo3",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom3",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom3",SqlDbType.Int,9,0}
          } ;
          Object[] prmH009H4 ;
          prmH009H4 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV13Tabela_Nome",SqlDbType.Char,50,0}
          } ;
          Object[] prmH009H5 ;
          prmH009H5 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009H2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009H2,11,0,true,false )
             ,new CursorDef("H009H3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009H3,11,0,false,false )
             ,new CursorDef("H009H4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009H4,1,0,true,false )
             ,new CursorDef("H009H5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009H5,1,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 4) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 50) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[7] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Int );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (int)parms[8]);
                   }
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[7] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Int );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (int)parms[8]);
                   }
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[2] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Int );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (int)parms[3]);
                   }
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[2] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Int );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (int)parms[3]);
                   }
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                return;
       }
    }

 }

}
