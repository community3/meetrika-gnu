/*
               File: type_SdtGAMAuthenticationTypeCustom
        Description: GAMAuthenticationTypeCustom
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:43.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMAuthenticationTypeCustom : GxUserType, IGxExternalObject
   {
      public SdtGAMAuthenticationTypeCustom( )
      {
         initialize();
      }

      public SdtGAMAuthenticationTypeCustom( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void load( String gxTp_Name )
      {
         if ( GAMAuthenticationTypeCustom_externalReference == null )
         {
            GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
         }
         GAMAuthenticationTypeCustom_externalReference.Load(gxTp_Name);
         return  ;
      }

      public void save( )
      {
         if ( GAMAuthenticationTypeCustom_externalReference == null )
         {
            GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
         }
         GAMAuthenticationTypeCustom_externalReference.Save();
         return  ;
      }

      public void delete( )
      {
         if ( GAMAuthenticationTypeCustom_externalReference == null )
         {
            GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
         }
         GAMAuthenticationTypeCustom_externalReference.Delete();
         return  ;
      }

      public bool success( )
      {
         bool returnsuccess ;
         if ( GAMAuthenticationTypeCustom_externalReference == null )
         {
            GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
         }
         returnsuccess = false;
         returnsuccess = (bool)(GAMAuthenticationTypeCustom_externalReference.Success());
         return returnsuccess ;
      }

      public bool fail( )
      {
         bool returnfail ;
         if ( GAMAuthenticationTypeCustom_externalReference == null )
         {
            GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
         }
         returnfail = false;
         returnfail = (bool)(GAMAuthenticationTypeCustom_externalReference.Fail());
         return returnfail ;
      }

      public IGxCollection geterrors( )
      {
         IGxCollection returngeterrors ;
         if ( GAMAuthenticationTypeCustom_externalReference == null )
         {
            GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
         }
         returngeterrors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm0 ;
         externalParm0 = GAMAuthenticationTypeCustom_externalReference.GetErrors();
         returngeterrors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm0);
         return returngeterrors ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMAuthenticationTypeCustom_externalReference == null )
         {
            GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
         }
         returntostring = "";
         returntostring = (String)(GAMAuthenticationTypeCustom_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Name
      {
         get {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            return GAMAuthenticationTypeCustom_externalReference.Name ;
         }

         set {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            GAMAuthenticationTypeCustom_externalReference.Name = value;
         }

      }

      public String gxTpr_Functionid
      {
         get {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            return GAMAuthenticationTypeCustom_externalReference.FunctionId ;
         }

         set {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            GAMAuthenticationTypeCustom_externalReference.FunctionId = value;
         }

      }

      public bool gxTpr_Isenable
      {
         get {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            return GAMAuthenticationTypeCustom_externalReference.IsEnable ;
         }

         set {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            GAMAuthenticationTypeCustom_externalReference.IsEnable = value;
         }

      }

      public String gxTpr_Description
      {
         get {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            return GAMAuthenticationTypeCustom_externalReference.Description ;
         }

         set {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            GAMAuthenticationTypeCustom_externalReference.Description = value;
         }

      }

      public String gxTpr_Impersonate
      {
         get {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            return GAMAuthenticationTypeCustom_externalReference.Impersonate ;
         }

         set {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            GAMAuthenticationTypeCustom_externalReference.Impersonate = value;
         }

      }

      public SdtGAMAuthenticationCustom gxTpr_Custom
      {
         get {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            SdtGAMAuthenticationCustom intValue ;
            intValue = new SdtGAMAuthenticationCustom(context);
            Artech.Security.GAMAuthenticationCustom externalParm0 ;
            externalParm0 = GAMAuthenticationTypeCustom_externalReference.Custom;
            intValue.ExternalInstance = externalParm0;
            return intValue ;
         }

         set {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            SdtGAMAuthenticationCustom intValue ;
            Artech.Security.GAMAuthenticationCustom externalParm1 ;
            intValue = value;
            externalParm1 = (Artech.Security.GAMAuthenticationCustom)(intValue.ExternalInstance);
            GAMAuthenticationTypeCustom_externalReference.Custom = externalParm1;
         }

      }

      public DateTime gxTpr_Datecreated
      {
         get {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            return GAMAuthenticationTypeCustom_externalReference.DateCreated ;
         }

         set {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            GAMAuthenticationTypeCustom_externalReference.DateCreated = value;
         }

      }

      public String gxTpr_Usercreated
      {
         get {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            return GAMAuthenticationTypeCustom_externalReference.UserCreated ;
         }

         set {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            GAMAuthenticationTypeCustom_externalReference.UserCreated = value;
         }

      }

      public DateTime gxTpr_Dateupdated
      {
         get {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            return GAMAuthenticationTypeCustom_externalReference.DateUpdated ;
         }

         set {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            GAMAuthenticationTypeCustom_externalReference.DateUpdated = value;
         }

      }

      public String gxTpr_Userupdated
      {
         get {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            return GAMAuthenticationTypeCustom_externalReference.UserUpdated ;
         }

         set {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            GAMAuthenticationTypeCustom_externalReference.UserUpdated = value;
         }

      }

      public IGxCollection gxTpr_Descriptions
      {
         get {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMDescription", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMDescription> externalParm2 ;
            externalParm2 = GAMAuthenticationTypeCustom_externalReference.Descriptions;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMDescription>), externalParm2);
            return intValue ;
         }

         set {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMDescription> externalParm3 ;
            intValue = value;
            externalParm3 = (System.Collections.Generic.List<Artech.Security.GAMDescription>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMDescription>), intValue.ExternalInstance);
            GAMAuthenticationTypeCustom_externalReference.Descriptions = externalParm3;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMAuthenticationTypeCustom_externalReference == null )
            {
               GAMAuthenticationTypeCustom_externalReference = new Artech.Security.GAMAuthenticationTypeCustom(context);
            }
            return GAMAuthenticationTypeCustom_externalReference ;
         }

         set {
            GAMAuthenticationTypeCustom_externalReference = (Artech.Security.GAMAuthenticationTypeCustom)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMAuthenticationTypeCustom GAMAuthenticationTypeCustom_externalReference=null ;
   }

}
