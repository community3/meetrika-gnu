/*
               File: PRC_EnviarEmailCsharp
        Description: Enviar Email Csharp
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:49.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_enviaremailcsharp : GXProcedure
   {
      public prc_enviaremailcsharp( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_enviaremailcsharp( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_Host ,
                           short aP1_Porta ,
                           short aP2_Seguranca ,
                           String aP3_Email ,
                           String aP4_Password ,
                           String aP5_EmailTo ,
                           out short aP6_retorno )
      {
         this.AV10Host = aP0_Host;
         this.AV14Porta = aP1_Porta;
         this.AV17Seguranca = aP2_Seguranca;
         this.AV9Email = aP3_Email;
         this.AV8Password = aP4_Password;
         this.AV19EmailTo = aP5_EmailTo;
         this.AV13retorno = 0 ;
         initialize();
         executePrivate();
         aP6_retorno=this.AV13retorno;
      }

      public short executeUdp( String aP0_Host ,
                               short aP1_Porta ,
                               short aP2_Seguranca ,
                               String aP3_Email ,
                               String aP4_Password ,
                               String aP5_EmailTo )
      {
         this.AV10Host = aP0_Host;
         this.AV14Porta = aP1_Porta;
         this.AV17Seguranca = aP2_Seguranca;
         this.AV9Email = aP3_Email;
         this.AV8Password = aP4_Password;
         this.AV19EmailTo = aP5_EmailTo;
         this.AV13retorno = 0 ;
         initialize();
         executePrivate();
         aP6_retorno=this.AV13retorno;
         return AV13retorno ;
      }

      public void executeSubmit( String aP0_Host ,
                                 short aP1_Porta ,
                                 short aP2_Seguranca ,
                                 String aP3_Email ,
                                 String aP4_Password ,
                                 String aP5_EmailTo ,
                                 out short aP6_retorno )
      {
         prc_enviaremailcsharp objprc_enviaremailcsharp;
         objprc_enviaremailcsharp = new prc_enviaremailcsharp();
         objprc_enviaremailcsharp.AV10Host = aP0_Host;
         objprc_enviaremailcsharp.AV14Porta = aP1_Porta;
         objprc_enviaremailcsharp.AV17Seguranca = aP2_Seguranca;
         objprc_enviaremailcsharp.AV9Email = aP3_Email;
         objprc_enviaremailcsharp.AV8Password = aP4_Password;
         objprc_enviaremailcsharp.AV19EmailTo = aP5_EmailTo;
         objprc_enviaremailcsharp.AV13retorno = 0 ;
         objprc_enviaremailcsharp.context.SetSubmitInitialConfig(context);
         objprc_enviaremailcsharp.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_enviaremailcsharp);
         aP6_retorno=this.AV13retorno;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_enviaremailcsharp)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( (0==AV17Seguranca) )
         {
            AV18ssl = false;
         }
         else
         {
            AV18ssl = true;
         }
         /* User Code */
          try
         /* User Code */
          {
         /* User Code */
          string SMTPUserName = AV9Email;
         /* User Code */
          string EMailTo = AV19EmailTo;
         /* User Code */
          string EMailSubject = "Teste";
         /* User Code */
          string EMailMessage = "Teste";
         /* User Code */
          string SMTPHost = AV10Host;
         /* User Code */
          int SMTPPort = AV14Porta;
         /* User Code */
          string SMTPEmailPassword = AV8Password;
         /* User Code */
          System.Net.Mail.MailMessage mM = new System.Net.Mail.MailMessage();
         /* User Code */
          mM.From = new System.Net.Mail.MailAddress(SMTPUserName);
         /* User Code */
          mM.To.Add(EMailTo);
         /* User Code */
          mM.Subject = EMailSubject;
         /* User Code */
          mM.Body = EMailMessage;
         /* User Code */
          mM.IsBodyHtml = true;
         /* User Code */
          mM.Priority = System.Net.Mail.MailPriority.High;
         /* User Code */
          System.Net.Mail.SmtpClient sC = new System.Net.Mail.SmtpClient(SMTPHost, SMTPPort);
         /* User Code */
          sC.EnableSsl = AV18ssl;
         /* User Code */
          string strId;
         /* User Code */
          string strPassword;
         /* User Code */
          strId = SMTPUserName.Split('@')[0];
         /* User Code */
          strPassword = SMTPEmailPassword;
         /* User Code */
          sC.Credentials = new System.Net.NetworkCredential(strId, strPassword);
         /* User Code */
          sC.Send(mM);
         /* User Code */
          AV13retorno = 1;
         /* User Code */
          }
         /* User Code */
          catch (System.Exception ex)
         /* User Code */
          {
         /* User Code */
          AV13retorno = 0;
         /* User Code */
          }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14Porta ;
      private short AV17Seguranca ;
      private short AV13retorno ;
      private String AV10Host ;
      private String AV8Password ;
      private String AV19EmailTo ;
      private bool AV18ssl ;
      private String AV9Email ;
      private short aP6_retorno ;
   }

}
