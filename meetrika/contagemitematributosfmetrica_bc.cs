/*
               File: ContagemItemAtributosFMetrica_BC
        Description: Contagem Item Atributos FMetrica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:37.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemitematributosfmetrica_bc : GXHttpHandler, IGxSilentTrn
   {
      public contagemitematributosfmetrica_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemitematributosfmetrica_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1A47( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1A47( ) ;
         standaloneModal( ) ;
         AddRow1A47( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z261ContagemItemAtributosFMetrica_ItemContagemLan = A261ContagemItemAtributosFMetrica_ItemContagemLan;
               Z249ContagemItem_FMetricasAtributosCod = A249ContagemItem_FMetricasAtributosCod;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_1A0( )
      {
         BeforeValidate1A47( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1A47( ) ;
            }
            else
            {
               CheckExtendedTable1A47( ) ;
               if ( AnyError == 0 )
               {
                  ZM1A47( 2) ;
                  ZM1A47( 3) ;
                  ZM1A47( 4) ;
               }
               CloseExtendedTableCursors1A47( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM1A47( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z250ContagemItem_FMetricasAtributosNom = A250ContagemItem_FMetricasAtributosNom;
            Z253ContagemItem_FMetricasAtributosTabCod = A253ContagemItem_FMetricasAtributosTabCod;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z254ContagemItem_FMetricasAtributosTabNom = A254ContagemItem_FMetricasAtributosTabNom;
         }
         if ( GX_JID == -1 )
         {
            Z261ContagemItemAtributosFMetrica_ItemContagemLan = A261ContagemItemAtributosFMetrica_ItemContagemLan;
            Z249ContagemItem_FMetricasAtributosCod = A249ContagemItem_FMetricasAtributosCod;
            Z250ContagemItem_FMetricasAtributosNom = A250ContagemItem_FMetricasAtributosNom;
            Z253ContagemItem_FMetricasAtributosTabCod = A253ContagemItem_FMetricasAtributosTabCod;
            Z254ContagemItem_FMetricasAtributosTabNom = A254ContagemItem_FMetricasAtributosTabNom;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load1A47( )
      {
         /* Using cursor BC001A7 */
         pr_default.execute(5, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound47 = 1;
            A250ContagemItem_FMetricasAtributosNom = BC001A7_A250ContagemItem_FMetricasAtributosNom[0];
            n250ContagemItem_FMetricasAtributosNom = BC001A7_n250ContagemItem_FMetricasAtributosNom[0];
            A254ContagemItem_FMetricasAtributosTabNom = BC001A7_A254ContagemItem_FMetricasAtributosTabNom[0];
            n254ContagemItem_FMetricasAtributosTabNom = BC001A7_n254ContagemItem_FMetricasAtributosTabNom[0];
            A253ContagemItem_FMetricasAtributosTabCod = BC001A7_A253ContagemItem_FMetricasAtributosTabCod[0];
            n253ContagemItem_FMetricasAtributosTabCod = BC001A7_n253ContagemItem_FMetricasAtributosTabCod[0];
            ZM1A47( -1) ;
         }
         pr_default.close(5);
         OnLoadActions1A47( ) ;
      }

      protected void OnLoadActions1A47( )
      {
      }

      protected void CheckExtendedTable1A47( )
      {
         standaloneModal( ) ;
         /* Using cursor BC001A4 */
         pr_default.execute(2, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Item Atributos FMetrica_Item Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC001A5 */
         pr_default.execute(3, new Object[] {A249ContagemItem_FMetricasAtributosCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Item_FMetricas Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_FMETRICASATRIBUTOSCOD");
            AnyError = 1;
         }
         A250ContagemItem_FMetricasAtributosNom = BC001A5_A250ContagemItem_FMetricasAtributosNom[0];
         n250ContagemItem_FMetricasAtributosNom = BC001A5_n250ContagemItem_FMetricasAtributosNom[0];
         A253ContagemItem_FMetricasAtributosTabCod = BC001A5_A253ContagemItem_FMetricasAtributosTabCod[0];
         n253ContagemItem_FMetricasAtributosTabCod = BC001A5_n253ContagemItem_FMetricasAtributosTabCod[0];
         pr_default.close(3);
         /* Using cursor BC001A6 */
         pr_default.execute(4, new Object[] {n253ContagemItem_FMetricasAtributosTabCod, A253ContagemItem_FMetricasAtributosTabCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A254ContagemItem_FMetricasAtributosTabNom = BC001A6_A254ContagemItem_FMetricasAtributosTabNom[0];
         n254ContagemItem_FMetricasAtributosTabNom = BC001A6_n254ContagemItem_FMetricasAtributosTabNom[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors1A47( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1A47( )
      {
         /* Using cursor BC001A8 */
         pr_default.execute(6, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound47 = 1;
         }
         else
         {
            RcdFound47 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC001A3 */
         pr_default.execute(1, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1A47( 1) ;
            RcdFound47 = 1;
            A261ContagemItemAtributosFMetrica_ItemContagemLan = BC001A3_A261ContagemItemAtributosFMetrica_ItemContagemLan[0];
            A249ContagemItem_FMetricasAtributosCod = BC001A3_A249ContagemItem_FMetricasAtributosCod[0];
            Z261ContagemItemAtributosFMetrica_ItemContagemLan = A261ContagemItemAtributosFMetrica_ItemContagemLan;
            Z249ContagemItem_FMetricasAtributosCod = A249ContagemItem_FMetricasAtributosCod;
            sMode47 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1A47( ) ;
            if ( AnyError == 1 )
            {
               RcdFound47 = 0;
               InitializeNonKey1A47( ) ;
            }
            Gx_mode = sMode47;
         }
         else
         {
            RcdFound47 = 0;
            InitializeNonKey1A47( ) ;
            sMode47 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode47;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1A47( ) ;
         if ( RcdFound47 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_1A0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1A47( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC001A2 */
            pr_default.execute(0, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemItemAtributosFMetrica"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemItemAtributosFMetrica"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1A47( )
      {
         BeforeValidate1A47( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1A47( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1A47( 0) ;
            CheckOptimisticConcurrency1A47( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1A47( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1A47( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001A9 */
                     pr_default.execute(7, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemItemAtributosFMetrica") ;
                     if ( (pr_default.getStatus(7) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1A47( ) ;
            }
            EndLevel1A47( ) ;
         }
         CloseExtendedTableCursors1A47( ) ;
      }

      protected void Update1A47( )
      {
         BeforeValidate1A47( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1A47( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1A47( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1A47( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1A47( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ContagemItemAtributosFMetrica] */
                     DeferredUpdate1A47( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1A47( ) ;
         }
         CloseExtendedTableCursors1A47( ) ;
      }

      protected void DeferredUpdate1A47( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1A47( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1A47( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1A47( ) ;
            AfterConfirm1A47( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1A47( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001A10 */
                  pr_default.execute(8, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemItemAtributosFMetrica") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode47 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1A47( ) ;
         Gx_mode = sMode47;
      }

      protected void OnDeleteControls1A47( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC001A11 */
            pr_default.execute(9, new Object[] {A249ContagemItem_FMetricasAtributosCod});
            A250ContagemItem_FMetricasAtributosNom = BC001A11_A250ContagemItem_FMetricasAtributosNom[0];
            n250ContagemItem_FMetricasAtributosNom = BC001A11_n250ContagemItem_FMetricasAtributosNom[0];
            A253ContagemItem_FMetricasAtributosTabCod = BC001A11_A253ContagemItem_FMetricasAtributosTabCod[0];
            n253ContagemItem_FMetricasAtributosTabCod = BC001A11_n253ContagemItem_FMetricasAtributosTabCod[0];
            pr_default.close(9);
            /* Using cursor BC001A12 */
            pr_default.execute(10, new Object[] {n253ContagemItem_FMetricasAtributosTabCod, A253ContagemItem_FMetricasAtributosTabCod});
            A254ContagemItem_FMetricasAtributosTabNom = BC001A12_A254ContagemItem_FMetricasAtributosTabNom[0];
            n254ContagemItem_FMetricasAtributosTabNom = BC001A12_n254ContagemItem_FMetricasAtributosTabNom[0];
            pr_default.close(10);
         }
      }

      protected void EndLevel1A47( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1A47( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1A47( )
      {
         /* Using cursor BC001A13 */
         pr_default.execute(11, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
         RcdFound47 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound47 = 1;
            A250ContagemItem_FMetricasAtributosNom = BC001A13_A250ContagemItem_FMetricasAtributosNom[0];
            n250ContagemItem_FMetricasAtributosNom = BC001A13_n250ContagemItem_FMetricasAtributosNom[0];
            A254ContagemItem_FMetricasAtributosTabNom = BC001A13_A254ContagemItem_FMetricasAtributosTabNom[0];
            n254ContagemItem_FMetricasAtributosTabNom = BC001A13_n254ContagemItem_FMetricasAtributosTabNom[0];
            A261ContagemItemAtributosFMetrica_ItemContagemLan = BC001A13_A261ContagemItemAtributosFMetrica_ItemContagemLan[0];
            A249ContagemItem_FMetricasAtributosCod = BC001A13_A249ContagemItem_FMetricasAtributosCod[0];
            A253ContagemItem_FMetricasAtributosTabCod = BC001A13_A253ContagemItem_FMetricasAtributosTabCod[0];
            n253ContagemItem_FMetricasAtributosTabCod = BC001A13_n253ContagemItem_FMetricasAtributosTabCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1A47( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound47 = 0;
         ScanKeyLoad1A47( ) ;
      }

      protected void ScanKeyLoad1A47( )
      {
         sMode47 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound47 = 1;
            A250ContagemItem_FMetricasAtributosNom = BC001A13_A250ContagemItem_FMetricasAtributosNom[0];
            n250ContagemItem_FMetricasAtributosNom = BC001A13_n250ContagemItem_FMetricasAtributosNom[0];
            A254ContagemItem_FMetricasAtributosTabNom = BC001A13_A254ContagemItem_FMetricasAtributosTabNom[0];
            n254ContagemItem_FMetricasAtributosTabNom = BC001A13_n254ContagemItem_FMetricasAtributosTabNom[0];
            A261ContagemItemAtributosFMetrica_ItemContagemLan = BC001A13_A261ContagemItemAtributosFMetrica_ItemContagemLan[0];
            A249ContagemItem_FMetricasAtributosCod = BC001A13_A249ContagemItem_FMetricasAtributosCod[0];
            A253ContagemItem_FMetricasAtributosTabCod = BC001A13_A253ContagemItem_FMetricasAtributosTabCod[0];
            n253ContagemItem_FMetricasAtributosTabCod = BC001A13_n253ContagemItem_FMetricasAtributosTabCod[0];
         }
         Gx_mode = sMode47;
      }

      protected void ScanKeyEnd1A47( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm1A47( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1A47( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1A47( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1A47( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1A47( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1A47( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1A47( )
      {
      }

      protected void AddRow1A47( )
      {
         VarsToRow47( bcContagemItemAtributosFMetrica) ;
      }

      protected void ReadRow1A47( )
      {
         RowToVars47( bcContagemItemAtributosFMetrica, 1) ;
      }

      protected void InitializeNonKey1A47( )
      {
         A250ContagemItem_FMetricasAtributosNom = "";
         n250ContagemItem_FMetricasAtributosNom = false;
         A253ContagemItem_FMetricasAtributosTabCod = 0;
         n253ContagemItem_FMetricasAtributosTabCod = false;
         A254ContagemItem_FMetricasAtributosTabNom = "";
         n254ContagemItem_FMetricasAtributosTabNom = false;
      }

      protected void InitAll1A47( )
      {
         A261ContagemItemAtributosFMetrica_ItemContagemLan = 0;
         A249ContagemItem_FMetricasAtributosCod = 0;
         InitializeNonKey1A47( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow47( SdtContagemItemAtributosFMetrica obj47 )
      {
         obj47.gxTpr_Mode = Gx_mode;
         obj47.gxTpr_Contagemitem_fmetricasatributosnom = A250ContagemItem_FMetricasAtributosNom;
         obj47.gxTpr_Contagemitem_fmetricasatributostabcod = A253ContagemItem_FMetricasAtributosTabCod;
         obj47.gxTpr_Contagemitem_fmetricasatributostabnom = A254ContagemItem_FMetricasAtributosTabNom;
         obj47.gxTpr_Contagemitematributosfmetrica_itemcontagemlan = A261ContagemItemAtributosFMetrica_ItemContagemLan;
         obj47.gxTpr_Contagemitem_fmetricasatributoscod = A249ContagemItem_FMetricasAtributosCod;
         obj47.gxTpr_Contagemitematributosfmetrica_itemcontagemlan_Z = Z261ContagemItemAtributosFMetrica_ItemContagemLan;
         obj47.gxTpr_Contagemitem_fmetricasatributoscod_Z = Z249ContagemItem_FMetricasAtributosCod;
         obj47.gxTpr_Contagemitem_fmetricasatributosnom_Z = Z250ContagemItem_FMetricasAtributosNom;
         obj47.gxTpr_Contagemitem_fmetricasatributostabcod_Z = Z253ContagemItem_FMetricasAtributosTabCod;
         obj47.gxTpr_Contagemitem_fmetricasatributostabnom_Z = Z254ContagemItem_FMetricasAtributosTabNom;
         obj47.gxTpr_Contagemitem_fmetricasatributosnom_N = (short)(Convert.ToInt16(n250ContagemItem_FMetricasAtributosNom));
         obj47.gxTpr_Contagemitem_fmetricasatributostabcod_N = (short)(Convert.ToInt16(n253ContagemItem_FMetricasAtributosTabCod));
         obj47.gxTpr_Contagemitem_fmetricasatributostabnom_N = (short)(Convert.ToInt16(n254ContagemItem_FMetricasAtributosTabNom));
         obj47.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow47( SdtContagemItemAtributosFMetrica obj47 )
      {
         obj47.gxTpr_Contagemitematributosfmetrica_itemcontagemlan = A261ContagemItemAtributosFMetrica_ItemContagemLan;
         obj47.gxTpr_Contagemitem_fmetricasatributoscod = A249ContagemItem_FMetricasAtributosCod;
         return  ;
      }

      public void RowToVars47( SdtContagemItemAtributosFMetrica obj47 ,
                               int forceLoad )
      {
         Gx_mode = obj47.gxTpr_Mode;
         A250ContagemItem_FMetricasAtributosNom = obj47.gxTpr_Contagemitem_fmetricasatributosnom;
         n250ContagemItem_FMetricasAtributosNom = false;
         A253ContagemItem_FMetricasAtributosTabCod = obj47.gxTpr_Contagemitem_fmetricasatributostabcod;
         n253ContagemItem_FMetricasAtributosTabCod = false;
         A254ContagemItem_FMetricasAtributosTabNom = obj47.gxTpr_Contagemitem_fmetricasatributostabnom;
         n254ContagemItem_FMetricasAtributosTabNom = false;
         A261ContagemItemAtributosFMetrica_ItemContagemLan = obj47.gxTpr_Contagemitematributosfmetrica_itemcontagemlan;
         A249ContagemItem_FMetricasAtributosCod = obj47.gxTpr_Contagemitem_fmetricasatributoscod;
         Z261ContagemItemAtributosFMetrica_ItemContagemLan = obj47.gxTpr_Contagemitematributosfmetrica_itemcontagemlan_Z;
         Z249ContagemItem_FMetricasAtributosCod = obj47.gxTpr_Contagemitem_fmetricasatributoscod_Z;
         Z250ContagemItem_FMetricasAtributosNom = obj47.gxTpr_Contagemitem_fmetricasatributosnom_Z;
         Z253ContagemItem_FMetricasAtributosTabCod = obj47.gxTpr_Contagemitem_fmetricasatributostabcod_Z;
         Z254ContagemItem_FMetricasAtributosTabNom = obj47.gxTpr_Contagemitem_fmetricasatributostabnom_Z;
         n250ContagemItem_FMetricasAtributosNom = (bool)(Convert.ToBoolean(obj47.gxTpr_Contagemitem_fmetricasatributosnom_N));
         n253ContagemItem_FMetricasAtributosTabCod = (bool)(Convert.ToBoolean(obj47.gxTpr_Contagemitem_fmetricasatributostabcod_N));
         n254ContagemItem_FMetricasAtributosTabNom = (bool)(Convert.ToBoolean(obj47.gxTpr_Contagemitem_fmetricasatributostabnom_N));
         Gx_mode = obj47.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A261ContagemItemAtributosFMetrica_ItemContagemLan = (int)getParm(obj,0);
         A249ContagemItem_FMetricasAtributosCod = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1A47( ) ;
         ScanKeyStart1A47( ) ;
         if ( RcdFound47 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001A14 */
            pr_default.execute(12, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Item Atributos FMetrica_Item Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor BC001A11 */
            pr_default.execute(9, new Object[] {A249ContagemItem_FMetricasAtributosCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Item_FMetricas Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_FMETRICASATRIBUTOSCOD");
               AnyError = 1;
            }
            A250ContagemItem_FMetricasAtributosNom = BC001A11_A250ContagemItem_FMetricasAtributosNom[0];
            n250ContagemItem_FMetricasAtributosNom = BC001A11_n250ContagemItem_FMetricasAtributosNom[0];
            A253ContagemItem_FMetricasAtributosTabCod = BC001A11_A253ContagemItem_FMetricasAtributosTabCod[0];
            n253ContagemItem_FMetricasAtributosTabCod = BC001A11_n253ContagemItem_FMetricasAtributosTabCod[0];
            pr_default.close(9);
            /* Using cursor BC001A12 */
            pr_default.execute(10, new Object[] {n253ContagemItem_FMetricasAtributosTabCod, A253ContagemItem_FMetricasAtributosTabCod});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A254ContagemItem_FMetricasAtributosTabNom = BC001A12_A254ContagemItem_FMetricasAtributosTabNom[0];
            n254ContagemItem_FMetricasAtributosTabNom = BC001A12_n254ContagemItem_FMetricasAtributosTabNom[0];
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z261ContagemItemAtributosFMetrica_ItemContagemLan = A261ContagemItemAtributosFMetrica_ItemContagemLan;
            Z249ContagemItem_FMetricasAtributosCod = A249ContagemItem_FMetricasAtributosCod;
         }
         ZM1A47( -1) ;
         OnLoadActions1A47( ) ;
         AddRow1A47( ) ;
         ScanKeyEnd1A47( ) ;
         if ( RcdFound47 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars47( bcContagemItemAtributosFMetrica, 0) ;
         ScanKeyStart1A47( ) ;
         if ( RcdFound47 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001A14 */
            pr_default.execute(12, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Item Atributos FMetrica_Item Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor BC001A11 */
            pr_default.execute(9, new Object[] {A249ContagemItem_FMetricasAtributosCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Item_FMetricas Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_FMETRICASATRIBUTOSCOD");
               AnyError = 1;
            }
            A250ContagemItem_FMetricasAtributosNom = BC001A11_A250ContagemItem_FMetricasAtributosNom[0];
            n250ContagemItem_FMetricasAtributosNom = BC001A11_n250ContagemItem_FMetricasAtributosNom[0];
            A253ContagemItem_FMetricasAtributosTabCod = BC001A11_A253ContagemItem_FMetricasAtributosTabCod[0];
            n253ContagemItem_FMetricasAtributosTabCod = BC001A11_n253ContagemItem_FMetricasAtributosTabCod[0];
            pr_default.close(9);
            /* Using cursor BC001A12 */
            pr_default.execute(10, new Object[] {n253ContagemItem_FMetricasAtributosTabCod, A253ContagemItem_FMetricasAtributosTabCod});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A254ContagemItem_FMetricasAtributosTabNom = BC001A12_A254ContagemItem_FMetricasAtributosTabNom[0];
            n254ContagemItem_FMetricasAtributosTabNom = BC001A12_n254ContagemItem_FMetricasAtributosTabNom[0];
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z261ContagemItemAtributosFMetrica_ItemContagemLan = A261ContagemItemAtributosFMetrica_ItemContagemLan;
            Z249ContagemItem_FMetricasAtributosCod = A249ContagemItem_FMetricasAtributosCod;
         }
         ZM1A47( -1) ;
         OnLoadActions1A47( ) ;
         AddRow1A47( ) ;
         ScanKeyEnd1A47( ) ;
         if ( RcdFound47 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars47( bcContagemItemAtributosFMetrica, 0) ;
         nKeyPressed = 1;
         GetKey1A47( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1A47( ) ;
         }
         else
         {
            if ( RcdFound47 == 1 )
            {
               if ( ( A261ContagemItemAtributosFMetrica_ItemContagemLan != Z261ContagemItemAtributosFMetrica_ItemContagemLan ) || ( A249ContagemItem_FMetricasAtributosCod != Z249ContagemItem_FMetricasAtributosCod ) )
               {
                  A261ContagemItemAtributosFMetrica_ItemContagemLan = Z261ContagemItemAtributosFMetrica_ItemContagemLan;
                  A249ContagemItem_FMetricasAtributosCod = Z249ContagemItem_FMetricasAtributosCod;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1A47( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A261ContagemItemAtributosFMetrica_ItemContagemLan != Z261ContagemItemAtributosFMetrica_ItemContagemLan ) || ( A249ContagemItem_FMetricasAtributosCod != Z249ContagemItem_FMetricasAtributosCod ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1A47( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1A47( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow47( bcContagemItemAtributosFMetrica) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars47( bcContagemItemAtributosFMetrica, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1A47( ) ;
         if ( RcdFound47 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A261ContagemItemAtributosFMetrica_ItemContagemLan != Z261ContagemItemAtributosFMetrica_ItemContagemLan ) || ( A249ContagemItem_FMetricasAtributosCod != Z249ContagemItem_FMetricasAtributosCod ) )
            {
               A261ContagemItemAtributosFMetrica_ItemContagemLan = Z261ContagemItemAtributosFMetrica_ItemContagemLan;
               A249ContagemItem_FMetricasAtributosCod = Z249ContagemItem_FMetricasAtributosCod;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A261ContagemItemAtributosFMetrica_ItemContagemLan != Z261ContagemItemAtributosFMetrica_ItemContagemLan ) || ( A249ContagemItem_FMetricasAtributosCod != Z249ContagemItem_FMetricasAtributosCod ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(9);
         pr_default.close(10);
         context.RollbackDataStores( "ContagemItemAtributosFMetrica_BC");
         VarsToRow47( bcContagemItemAtributosFMetrica) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContagemItemAtributosFMetrica.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContagemItemAtributosFMetrica.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContagemItemAtributosFMetrica )
         {
            bcContagemItemAtributosFMetrica = (SdtContagemItemAtributosFMetrica)(sdt);
            if ( StringUtil.StrCmp(bcContagemItemAtributosFMetrica.gxTpr_Mode, "") == 0 )
            {
               bcContagemItemAtributosFMetrica.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow47( bcContagemItemAtributosFMetrica) ;
            }
            else
            {
               RowToVars47( bcContagemItemAtributosFMetrica, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContagemItemAtributosFMetrica.gxTpr_Mode, "") == 0 )
            {
               bcContagemItemAtributosFMetrica.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars47( bcContagemItemAtributosFMetrica, 1) ;
         return  ;
      }

      public SdtContagemItemAtributosFMetrica ContagemItemAtributosFMetrica_BC
      {
         get {
            return bcContagemItemAtributosFMetrica ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(9);
         pr_default.close(10);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z250ContagemItem_FMetricasAtributosNom = "";
         A250ContagemItem_FMetricasAtributosNom = "";
         Z254ContagemItem_FMetricasAtributosTabNom = "";
         A254ContagemItem_FMetricasAtributosTabNom = "";
         BC001A7_A250ContagemItem_FMetricasAtributosNom = new String[] {""} ;
         BC001A7_n250ContagemItem_FMetricasAtributosNom = new bool[] {false} ;
         BC001A7_A254ContagemItem_FMetricasAtributosTabNom = new String[] {""} ;
         BC001A7_n254ContagemItem_FMetricasAtributosTabNom = new bool[] {false} ;
         BC001A7_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         BC001A7_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         BC001A7_A253ContagemItem_FMetricasAtributosTabCod = new int[1] ;
         BC001A7_n253ContagemItem_FMetricasAtributosTabCod = new bool[] {false} ;
         BC001A4_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         BC001A5_A250ContagemItem_FMetricasAtributosNom = new String[] {""} ;
         BC001A5_n250ContagemItem_FMetricasAtributosNom = new bool[] {false} ;
         BC001A5_A253ContagemItem_FMetricasAtributosTabCod = new int[1] ;
         BC001A5_n253ContagemItem_FMetricasAtributosTabCod = new bool[] {false} ;
         BC001A6_A254ContagemItem_FMetricasAtributosTabNom = new String[] {""} ;
         BC001A6_n254ContagemItem_FMetricasAtributosTabNom = new bool[] {false} ;
         BC001A8_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         BC001A8_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         BC001A3_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         BC001A3_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         sMode47 = "";
         BC001A2_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         BC001A2_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         BC001A11_A250ContagemItem_FMetricasAtributosNom = new String[] {""} ;
         BC001A11_n250ContagemItem_FMetricasAtributosNom = new bool[] {false} ;
         BC001A11_A253ContagemItem_FMetricasAtributosTabCod = new int[1] ;
         BC001A11_n253ContagemItem_FMetricasAtributosTabCod = new bool[] {false} ;
         BC001A12_A254ContagemItem_FMetricasAtributosTabNom = new String[] {""} ;
         BC001A12_n254ContagemItem_FMetricasAtributosTabNom = new bool[] {false} ;
         BC001A13_A250ContagemItem_FMetricasAtributosNom = new String[] {""} ;
         BC001A13_n250ContagemItem_FMetricasAtributosNom = new bool[] {false} ;
         BC001A13_A254ContagemItem_FMetricasAtributosTabNom = new String[] {""} ;
         BC001A13_n254ContagemItem_FMetricasAtributosTabNom = new bool[] {false} ;
         BC001A13_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         BC001A13_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         BC001A13_A253ContagemItem_FMetricasAtributosTabCod = new int[1] ;
         BC001A13_n253ContagemItem_FMetricasAtributosTabCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC001A14_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemitematributosfmetrica_bc__default(),
            new Object[][] {
                new Object[] {
               BC001A2_A261ContagemItemAtributosFMetrica_ItemContagemLan, BC001A2_A249ContagemItem_FMetricasAtributosCod
               }
               , new Object[] {
               BC001A3_A261ContagemItemAtributosFMetrica_ItemContagemLan, BC001A3_A249ContagemItem_FMetricasAtributosCod
               }
               , new Object[] {
               BC001A4_A261ContagemItemAtributosFMetrica_ItemContagemLan
               }
               , new Object[] {
               BC001A5_A250ContagemItem_FMetricasAtributosNom, BC001A5_n250ContagemItem_FMetricasAtributosNom, BC001A5_A253ContagemItem_FMetricasAtributosTabCod, BC001A5_n253ContagemItem_FMetricasAtributosTabCod
               }
               , new Object[] {
               BC001A6_A254ContagemItem_FMetricasAtributosTabNom, BC001A6_n254ContagemItem_FMetricasAtributosTabNom
               }
               , new Object[] {
               BC001A7_A250ContagemItem_FMetricasAtributosNom, BC001A7_n250ContagemItem_FMetricasAtributosNom, BC001A7_A254ContagemItem_FMetricasAtributosTabNom, BC001A7_n254ContagemItem_FMetricasAtributosTabNom, BC001A7_A261ContagemItemAtributosFMetrica_ItemContagemLan, BC001A7_A249ContagemItem_FMetricasAtributosCod, BC001A7_A253ContagemItem_FMetricasAtributosTabCod, BC001A7_n253ContagemItem_FMetricasAtributosTabCod
               }
               , new Object[] {
               BC001A8_A261ContagemItemAtributosFMetrica_ItemContagemLan, BC001A8_A249ContagemItem_FMetricasAtributosCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001A11_A250ContagemItem_FMetricasAtributosNom, BC001A11_n250ContagemItem_FMetricasAtributosNom, BC001A11_A253ContagemItem_FMetricasAtributosTabCod, BC001A11_n253ContagemItem_FMetricasAtributosTabCod
               }
               , new Object[] {
               BC001A12_A254ContagemItem_FMetricasAtributosTabNom, BC001A12_n254ContagemItem_FMetricasAtributosTabNom
               }
               , new Object[] {
               BC001A13_A250ContagemItem_FMetricasAtributosNom, BC001A13_n250ContagemItem_FMetricasAtributosNom, BC001A13_A254ContagemItem_FMetricasAtributosTabNom, BC001A13_n254ContagemItem_FMetricasAtributosTabNom, BC001A13_A261ContagemItemAtributosFMetrica_ItemContagemLan, BC001A13_A249ContagemItem_FMetricasAtributosCod, BC001A13_A253ContagemItem_FMetricasAtributosTabCod, BC001A13_n253ContagemItem_FMetricasAtributosTabCod
               }
               , new Object[] {
               BC001A14_A261ContagemItemAtributosFMetrica_ItemContagemLan
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound47 ;
      private int trnEnded ;
      private int Z261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int Z249ContagemItem_FMetricasAtributosCod ;
      private int A249ContagemItem_FMetricasAtributosCod ;
      private int Z253ContagemItem_FMetricasAtributosTabCod ;
      private int A253ContagemItem_FMetricasAtributosTabCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z250ContagemItem_FMetricasAtributosNom ;
      private String A250ContagemItem_FMetricasAtributosNom ;
      private String Z254ContagemItem_FMetricasAtributosTabNom ;
      private String A254ContagemItem_FMetricasAtributosTabNom ;
      private String sMode47 ;
      private bool n250ContagemItem_FMetricasAtributosNom ;
      private bool n254ContagemItem_FMetricasAtributosTabNom ;
      private bool n253ContagemItem_FMetricasAtributosTabCod ;
      private SdtContagemItemAtributosFMetrica bcContagemItemAtributosFMetrica ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC001A7_A250ContagemItem_FMetricasAtributosNom ;
      private bool[] BC001A7_n250ContagemItem_FMetricasAtributosNom ;
      private String[] BC001A7_A254ContagemItem_FMetricasAtributosTabNom ;
      private bool[] BC001A7_n254ContagemItem_FMetricasAtributosTabNom ;
      private int[] BC001A7_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] BC001A7_A249ContagemItem_FMetricasAtributosCod ;
      private int[] BC001A7_A253ContagemItem_FMetricasAtributosTabCod ;
      private bool[] BC001A7_n253ContagemItem_FMetricasAtributosTabCod ;
      private int[] BC001A4_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private String[] BC001A5_A250ContagemItem_FMetricasAtributosNom ;
      private bool[] BC001A5_n250ContagemItem_FMetricasAtributosNom ;
      private int[] BC001A5_A253ContagemItem_FMetricasAtributosTabCod ;
      private bool[] BC001A5_n253ContagemItem_FMetricasAtributosTabCod ;
      private String[] BC001A6_A254ContagemItem_FMetricasAtributosTabNom ;
      private bool[] BC001A6_n254ContagemItem_FMetricasAtributosTabNom ;
      private int[] BC001A8_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] BC001A8_A249ContagemItem_FMetricasAtributosCod ;
      private int[] BC001A3_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] BC001A3_A249ContagemItem_FMetricasAtributosCod ;
      private int[] BC001A2_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] BC001A2_A249ContagemItem_FMetricasAtributosCod ;
      private String[] BC001A11_A250ContagemItem_FMetricasAtributosNom ;
      private bool[] BC001A11_n250ContagemItem_FMetricasAtributosNom ;
      private int[] BC001A11_A253ContagemItem_FMetricasAtributosTabCod ;
      private bool[] BC001A11_n253ContagemItem_FMetricasAtributosTabCod ;
      private String[] BC001A12_A254ContagemItem_FMetricasAtributosTabNom ;
      private bool[] BC001A12_n254ContagemItem_FMetricasAtributosTabNom ;
      private String[] BC001A13_A250ContagemItem_FMetricasAtributosNom ;
      private bool[] BC001A13_n250ContagemItem_FMetricasAtributosNom ;
      private String[] BC001A13_A254ContagemItem_FMetricasAtributosTabNom ;
      private bool[] BC001A13_n254ContagemItem_FMetricasAtributosTabNom ;
      private int[] BC001A13_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] BC001A13_A249ContagemItem_FMetricasAtributosCod ;
      private int[] BC001A13_A253ContagemItem_FMetricasAtributosTabCod ;
      private bool[] BC001A13_n253ContagemItem_FMetricasAtributosTabCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC001A14_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
   }

   public class contagemitematributosfmetrica_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC001A7 ;
          prmBC001A7 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001A4 ;
          prmBC001A4 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001A5 ;
          prmBC001A5 = new Object[] {
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001A6 ;
          prmBC001A6 = new Object[] {
          new Object[] {"@ContagemItem_FMetricasAtributosTabCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001A8 ;
          prmBC001A8 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001A3 ;
          prmBC001A3 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001A2 ;
          prmBC001A2 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001A9 ;
          prmBC001A9 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001A10 ;
          prmBC001A10 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001A13 ;
          prmBC001A13 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001A14 ;
          prmBC001A14 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001A11 ;
          prmBC001A11 = new Object[] {
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001A12 ;
          prmBC001A12 = new Object[] {
          new Object[] {"@ContagemItem_FMetricasAtributosTabCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC001A2", "SELECT [ContagemItemAtributosFMetrica_ItemContagemLan] AS ContagemItemAtributosFMetrica_ItemContagemLan, [ContagemItem_FMetricasAtributosCod] AS ContagemItem_FMetricasAtributosCod FROM [ContagemItemAtributosFMetrica] WITH (UPDLOCK) WHERE [ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItemAtributosFMetrica_ItemContagemLan AND [ContagemItem_FMetricasAtributosCod] = @ContagemItem_FMetricasAtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001A2,1,0,true,false )
             ,new CursorDef("BC001A3", "SELECT [ContagemItemAtributosFMetrica_ItemContagemLan] AS ContagemItemAtributosFMetrica_ItemContagemLan, [ContagemItem_FMetricasAtributosCod] AS ContagemItem_FMetricasAtributosCod FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) WHERE [ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItemAtributosFMetrica_ItemContagemLan AND [ContagemItem_FMetricasAtributosCod] = @ContagemItem_FMetricasAtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001A3,1,0,true,false )
             ,new CursorDef("BC001A4", "SELECT [ContagemItem_Lancamento] AS ContagemItemAtributosFMetrica_ItemContagemLan FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItemAtributosFMetrica_ItemContagemLan ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001A4,1,0,true,false )
             ,new CursorDef("BC001A5", "SELECT [Atributos_Nome] AS ContagemItem_FMetricasAtributosNom, [Atributos_TabelaCod] AS ContagemItem_FMetricasAtributosTabCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @ContagemItem_FMetricasAtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001A5,1,0,true,false )
             ,new CursorDef("BC001A6", "SELECT [Tabela_Nome] AS ContagemItem_FMetricasAtributosTabNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @ContagemItem_FMetricasAtributosTabCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001A6,1,0,true,false )
             ,new CursorDef("BC001A7", "SELECT T2.[Atributos_Nome] AS ContagemItem_FMetricasAtributosNom, T3.[Tabela_Nome] AS ContagemItem_FMetricasAtributosTabNom, TM1.[ContagemItemAtributosFMetrica_ItemContagemLan] AS ContagemItemAtributosFMetrica_ItemContagemLan, TM1.[ContagemItem_FMetricasAtributosCod] AS ContagemItem_FMetricasAtributosCod, T2.[Atributos_TabelaCod] AS ContagemItem_FMetricasAtributosTabCod FROM (([ContagemItemAtributosFMetrica] TM1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = TM1.[ContagemItem_FMetricasAtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) WHERE TM1.[ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItemAtributosFMetrica_ItemContagemLan and TM1.[ContagemItem_FMetricasAtributosCod] = @ContagemItem_FMetricasAtributosCod ORDER BY TM1.[ContagemItemAtributosFMetrica_ItemContagemLan], TM1.[ContagemItem_FMetricasAtributosCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001A7,100,0,true,false )
             ,new CursorDef("BC001A8", "SELECT [ContagemItemAtributosFMetrica_ItemContagemLan] AS ContagemItemAtributosFMetrica_ItemContagemLan, [ContagemItem_FMetricasAtributosCod] AS ContagemItem_FMetricasAtributosCod FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) WHERE [ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItemAtributosFMetrica_ItemContagemLan AND [ContagemItem_FMetricasAtributosCod] = @ContagemItem_FMetricasAtributosCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001A8,1,0,true,false )
             ,new CursorDef("BC001A9", "INSERT INTO [ContagemItemAtributosFMetrica]([ContagemItemAtributosFMetrica_ItemContagemLan], [ContagemItem_FMetricasAtributosCod]) VALUES(@ContagemItemAtributosFMetrica_ItemContagemLan, @ContagemItem_FMetricasAtributosCod)", GxErrorMask.GX_NOMASK,prmBC001A9)
             ,new CursorDef("BC001A10", "DELETE FROM [ContagemItemAtributosFMetrica]  WHERE [ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItemAtributosFMetrica_ItemContagemLan AND [ContagemItem_FMetricasAtributosCod] = @ContagemItem_FMetricasAtributosCod", GxErrorMask.GX_NOMASK,prmBC001A10)
             ,new CursorDef("BC001A11", "SELECT [Atributos_Nome] AS ContagemItem_FMetricasAtributosNom, [Atributos_TabelaCod] AS ContagemItem_FMetricasAtributosTabCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @ContagemItem_FMetricasAtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001A11,1,0,true,false )
             ,new CursorDef("BC001A12", "SELECT [Tabela_Nome] AS ContagemItem_FMetricasAtributosTabNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @ContagemItem_FMetricasAtributosTabCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001A12,1,0,true,false )
             ,new CursorDef("BC001A13", "SELECT T2.[Atributos_Nome] AS ContagemItem_FMetricasAtributosNom, T3.[Tabela_Nome] AS ContagemItem_FMetricasAtributosTabNom, TM1.[ContagemItemAtributosFMetrica_ItemContagemLan] AS ContagemItemAtributosFMetrica_ItemContagemLan, TM1.[ContagemItem_FMetricasAtributosCod] AS ContagemItem_FMetricasAtributosCod, T2.[Atributos_TabelaCod] AS ContagemItem_FMetricasAtributosTabCod FROM (([ContagemItemAtributosFMetrica] TM1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = TM1.[ContagemItem_FMetricasAtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) WHERE TM1.[ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItemAtributosFMetrica_ItemContagemLan and TM1.[ContagemItem_FMetricasAtributosCod] = @ContagemItem_FMetricasAtributosCod ORDER BY TM1.[ContagemItemAtributosFMetrica_ItemContagemLan], TM1.[ContagemItem_FMetricasAtributosCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001A13,100,0,true,false )
             ,new CursorDef("BC001A14", "SELECT [ContagemItem_Lancamento] AS ContagemItemAtributosFMetrica_ItemContagemLan FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItemAtributosFMetrica_ItemContagemLan ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001A14,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
