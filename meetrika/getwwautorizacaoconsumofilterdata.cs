/*
               File: GetWWAutorizacaoConsumoFilterData
        Description: Get WWAutorizacao Consumo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:16:30.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwautorizacaoconsumofilterdata : GXProcedure
   {
      public getwwautorizacaoconsumofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwautorizacaoconsumofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwautorizacaoconsumofilterdata objgetwwautorizacaoconsumofilterdata;
         objgetwwautorizacaoconsumofilterdata = new getwwautorizacaoconsumofilterdata();
         objgetwwautorizacaoconsumofilterdata.AV24DDOName = aP0_DDOName;
         objgetwwautorizacaoconsumofilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetwwautorizacaoconsumofilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetwwautorizacaoconsumofilterdata.AV28OptionsJson = "" ;
         objgetwwautorizacaoconsumofilterdata.AV31OptionsDescJson = "" ;
         objgetwwautorizacaoconsumofilterdata.AV33OptionIndexesJson = "" ;
         objgetwwautorizacaoconsumofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwautorizacaoconsumofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwautorizacaoconsumofilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwautorizacaoconsumofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM") == 0 )
         {
            /* Execute user subroutine: 'LOADAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("WWAutorizacaoConsumoGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWAutorizacaoConsumoGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("WWAutorizacaoConsumoGridState"), "");
         }
         AV53GXV1 = 1;
         while ( AV53GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV53GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_CODIGO") == 0 )
            {
               AV10TFAutorizacaoConsumo_Codigo = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV11TFAutorizacaoConsumo_Codigo_To = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATO_CODIGO") == 0 )
            {
               AV12TFContrato_Codigo = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContrato_Codigo_To = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
            {
               AV14TFAutorizacaoConsumo_VigenciaInicio = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Value, 2);
               AV15TFAutorizacaoConsumo_VigenciaInicio_To = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_VIGENCIAFIM") == 0 )
            {
               AV16TFAutorizacaoConsumo_VigenciaFim = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Value, 2);
               AV17TFAutorizacaoConsumo_VigenciaFim_To = context.localUtil.CToD( AV38GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM") == 0 )
            {
               AV18TFAutorizacaoConsumo_UnidadeMedicaoNom = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL") == 0 )
            {
               AV19TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_QUANTIDADE") == 0 )
            {
               AV20TFAutorizacaoConsumo_Quantidade = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV21TFAutorizacaoConsumo_Quantidade_To = (short)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            AV53GXV1 = (int)(AV53GXV1+1);
         }
         if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(1));
            AV40DynamicFiltersSelector1 = AV39GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
            {
               AV41AutorizacaoConsumo_VigenciaInicio1 = context.localUtil.CToD( AV39GridStateDynamicFilter.gxTpr_Value, 2);
               AV42AutorizacaoConsumo_VigenciaInicio_To1 = context.localUtil.CToD( AV39GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV43DynamicFiltersEnabled2 = true;
               AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(2));
               AV44DynamicFiltersSelector2 = AV39GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
               {
                  AV45AutorizacaoConsumo_VigenciaInicio2 = context.localUtil.CToD( AV39GridStateDynamicFilter.gxTpr_Value, 2);
                  AV46AutorizacaoConsumo_VigenciaInicio_To2 = context.localUtil.CToD( AV39GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV47DynamicFiltersEnabled3 = true;
                  AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(3));
                  AV48DynamicFiltersSelector3 = AV39GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
                  {
                     AV49AutorizacaoConsumo_VigenciaInicio3 = context.localUtil.CToD( AV39GridStateDynamicFilter.gxTpr_Value, 2);
                     AV50AutorizacaoConsumo_VigenciaInicio_To3 = context.localUtil.CToD( AV39GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMOPTIONS' Routine */
         AV18TFAutorizacaoConsumo_UnidadeMedicaoNom = AV22SearchTxt;
         AV19TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = "";
         AV55WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV56WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 = AV41AutorizacaoConsumo_VigenciaInicio1;
         AV57WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 = AV42AutorizacaoConsumo_VigenciaInicio_To1;
         AV58WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV59WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV60WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 = AV45AutorizacaoConsumo_VigenciaInicio2;
         AV61WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 = AV46AutorizacaoConsumo_VigenciaInicio_To2;
         AV62WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 = AV47DynamicFiltersEnabled3;
         AV63WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 = AV48DynamicFiltersSelector3;
         AV64WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 = AV49AutorizacaoConsumo_VigenciaInicio3;
         AV65WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 = AV50AutorizacaoConsumo_VigenciaInicio_To3;
         AV66WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo = AV10TFAutorizacaoConsumo_Codigo;
         AV67WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to = AV11TFAutorizacaoConsumo_Codigo_To;
         AV68WWAutorizacaoConsumoDS_14_Tfcontrato_codigo = AV12TFContrato_Codigo;
         AV69WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to = AV13TFContrato_Codigo_To;
         AV70WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio = AV14TFAutorizacaoConsumo_VigenciaInicio;
         AV71WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to = AV15TFAutorizacaoConsumo_VigenciaInicio_To;
         AV72WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim = AV16TFAutorizacaoConsumo_VigenciaFim;
         AV73WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to = AV17TFAutorizacaoConsumo_VigenciaFim_To;
         AV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = AV18TFAutorizacaoConsumo_UnidadeMedicaoNom;
         AV75WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel = AV19TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel;
         AV76WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade = AV20TFAutorizacaoConsumo_Quantidade;
         AV77WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to = AV21TFAutorizacaoConsumo_Quantidade_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV55WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 ,
                                              AV56WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 ,
                                              AV57WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 ,
                                              AV58WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 ,
                                              AV59WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 ,
                                              AV60WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 ,
                                              AV61WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 ,
                                              AV62WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 ,
                                              AV63WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 ,
                                              AV64WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 ,
                                              AV65WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 ,
                                              AV66WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo ,
                                              AV67WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to ,
                                              AV68WWAutorizacaoConsumoDS_14_Tfcontrato_codigo ,
                                              AV69WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to ,
                                              AV70WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio ,
                                              AV71WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to ,
                                              AV72WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim ,
                                              AV73WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to ,
                                              AV75WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel ,
                                              AV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom ,
                                              AV76WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade ,
                                              AV77WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to ,
                                              A1775AutorizacaoConsumo_VigenciaInicio ,
                                              A1774AutorizacaoConsumo_Codigo ,
                                              A74Contrato_Codigo ,
                                              A1776AutorizacaoConsumo_VigenciaFim ,
                                              A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                              A1779AutorizacaoConsumo_Quantidade },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         lV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = StringUtil.PadR( StringUtil.RTrim( AV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom), 50, "%");
         /* Using cursor P00RN2 */
         pr_default.execute(0, new Object[] {AV56WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1, AV57WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1, AV60WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2, AV61WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2, AV64WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3, AV65WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3, AV66WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo, AV67WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to, AV68WWAutorizacaoConsumoDS_14_Tfcontrato_codigo, AV69WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to, AV70WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio, AV71WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to, AV72WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim, AV73WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to, lV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom, AV75WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel, AV76WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade, AV77WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKRN2 = false;
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = P00RN2_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0];
            A1779AutorizacaoConsumo_Quantidade = P00RN2_A1779AutorizacaoConsumo_Quantidade[0];
            A1778AutorizacaoConsumo_UnidadeMedicaoNom = P00RN2_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            n1778AutorizacaoConsumo_UnidadeMedicaoNom = P00RN2_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            A1776AutorizacaoConsumo_VigenciaFim = P00RN2_A1776AutorizacaoConsumo_VigenciaFim[0];
            A74Contrato_Codigo = P00RN2_A74Contrato_Codigo[0];
            A1774AutorizacaoConsumo_Codigo = P00RN2_A1774AutorizacaoConsumo_Codigo[0];
            A1775AutorizacaoConsumo_VigenciaInicio = P00RN2_A1775AutorizacaoConsumo_VigenciaInicio[0];
            A1778AutorizacaoConsumo_UnidadeMedicaoNom = P00RN2_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            n1778AutorizacaoConsumo_UnidadeMedicaoNom = P00RN2_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
            AV34count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00RN2_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0] == A1777AutorizacaoConsumo_UnidadeMedicaoCod ) )
            {
               BRKRN2 = false;
               A1774AutorizacaoConsumo_Codigo = P00RN2_A1774AutorizacaoConsumo_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKRN2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1778AutorizacaoConsumo_UnidadeMedicaoNom)) )
            {
               AV26Option = A1778AutorizacaoConsumo_UnidadeMedicaoNom;
               AV25InsertIndex = 1;
               while ( ( AV25InsertIndex <= AV27Options.Count ) && ( StringUtil.StrCmp(((String)AV27Options.Item(AV25InsertIndex)), AV26Option) < 0 ) )
               {
                  AV25InsertIndex = (int)(AV25InsertIndex+1);
               }
               AV27Options.Add(AV26Option, AV25InsertIndex);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), AV25InsertIndex);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKRN2 )
            {
               BRKRN2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFAutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         AV15TFAutorizacaoConsumo_VigenciaInicio_To = DateTime.MinValue;
         AV16TFAutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         AV17TFAutorizacaoConsumo_VigenciaFim_To = DateTime.MinValue;
         AV18TFAutorizacaoConsumo_UnidadeMedicaoNom = "";
         AV19TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = "";
         AV39GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV40DynamicFiltersSelector1 = "";
         AV41AutorizacaoConsumo_VigenciaInicio1 = DateTime.MinValue;
         AV42AutorizacaoConsumo_VigenciaInicio_To1 = DateTime.MinValue;
         AV44DynamicFiltersSelector2 = "";
         AV45AutorizacaoConsumo_VigenciaInicio2 = DateTime.MinValue;
         AV46AutorizacaoConsumo_VigenciaInicio_To2 = DateTime.MinValue;
         AV48DynamicFiltersSelector3 = "";
         AV49AutorizacaoConsumo_VigenciaInicio3 = DateTime.MinValue;
         AV50AutorizacaoConsumo_VigenciaInicio_To3 = DateTime.MinValue;
         AV55WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 = "";
         AV56WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 = DateTime.MinValue;
         AV57WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 = DateTime.MinValue;
         AV59WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 = "";
         AV60WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 = DateTime.MinValue;
         AV61WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 = DateTime.MinValue;
         AV63WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 = "";
         AV64WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 = DateTime.MinValue;
         AV65WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 = DateTime.MinValue;
         AV70WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio = DateTime.MinValue;
         AV71WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to = DateTime.MinValue;
         AV72WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim = DateTime.MinValue;
         AV73WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to = DateTime.MinValue;
         AV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = "";
         AV75WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel = "";
         scmdbuf = "";
         lV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = "";
         A1775AutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         A1776AutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         A1778AutorizacaoConsumo_UnidadeMedicaoNom = "";
         P00RN2_A1777AutorizacaoConsumo_UnidadeMedicaoCod = new int[1] ;
         P00RN2_A1779AutorizacaoConsumo_Quantidade = new short[1] ;
         P00RN2_A1778AutorizacaoConsumo_UnidadeMedicaoNom = new String[] {""} ;
         P00RN2_n1778AutorizacaoConsumo_UnidadeMedicaoNom = new bool[] {false} ;
         P00RN2_A1776AutorizacaoConsumo_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         P00RN2_A74Contrato_Codigo = new int[1] ;
         P00RN2_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         P00RN2_A1775AutorizacaoConsumo_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         AV26Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwautorizacaoconsumofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00RN2_A1777AutorizacaoConsumo_UnidadeMedicaoCod, P00RN2_A1779AutorizacaoConsumo_Quantidade, P00RN2_A1778AutorizacaoConsumo_UnidadeMedicaoNom, P00RN2_n1778AutorizacaoConsumo_UnidadeMedicaoNom, P00RN2_A1776AutorizacaoConsumo_VigenciaFim, P00RN2_A74Contrato_Codigo, P00RN2_A1774AutorizacaoConsumo_Codigo, P00RN2_A1775AutorizacaoConsumo_VigenciaInicio
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV20TFAutorizacaoConsumo_Quantidade ;
      private short AV21TFAutorizacaoConsumo_Quantidade_To ;
      private short AV76WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade ;
      private short AV77WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to ;
      private short A1779AutorizacaoConsumo_Quantidade ;
      private int AV53GXV1 ;
      private int AV10TFAutorizacaoConsumo_Codigo ;
      private int AV11TFAutorizacaoConsumo_Codigo_To ;
      private int AV12TFContrato_Codigo ;
      private int AV13TFContrato_Codigo_To ;
      private int AV66WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo ;
      private int AV67WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to ;
      private int AV68WWAutorizacaoConsumoDS_14_Tfcontrato_codigo ;
      private int AV69WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to ;
      private int A1774AutorizacaoConsumo_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int AV25InsertIndex ;
      private long AV34count ;
      private String AV18TFAutorizacaoConsumo_UnidadeMedicaoNom ;
      private String AV19TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ;
      private String AV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom ;
      private String AV75WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel ;
      private String scmdbuf ;
      private String lV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom ;
      private String A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private DateTime AV14TFAutorizacaoConsumo_VigenciaInicio ;
      private DateTime AV15TFAutorizacaoConsumo_VigenciaInicio_To ;
      private DateTime AV16TFAutorizacaoConsumo_VigenciaFim ;
      private DateTime AV17TFAutorizacaoConsumo_VigenciaFim_To ;
      private DateTime AV41AutorizacaoConsumo_VigenciaInicio1 ;
      private DateTime AV42AutorizacaoConsumo_VigenciaInicio_To1 ;
      private DateTime AV45AutorizacaoConsumo_VigenciaInicio2 ;
      private DateTime AV46AutorizacaoConsumo_VigenciaInicio_To2 ;
      private DateTime AV49AutorizacaoConsumo_VigenciaInicio3 ;
      private DateTime AV50AutorizacaoConsumo_VigenciaInicio_To3 ;
      private DateTime AV56WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 ;
      private DateTime AV57WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 ;
      private DateTime AV60WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 ;
      private DateTime AV61WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 ;
      private DateTime AV64WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 ;
      private DateTime AV65WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 ;
      private DateTime AV70WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio ;
      private DateTime AV71WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to ;
      private DateTime AV72WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim ;
      private DateTime AV73WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to ;
      private DateTime A1775AutorizacaoConsumo_VigenciaInicio ;
      private DateTime A1776AutorizacaoConsumo_VigenciaFim ;
      private bool returnInSub ;
      private bool AV43DynamicFiltersEnabled2 ;
      private bool AV47DynamicFiltersEnabled3 ;
      private bool AV58WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 ;
      private bool AV62WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 ;
      private bool BRKRN2 ;
      private bool n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV40DynamicFiltersSelector1 ;
      private String AV44DynamicFiltersSelector2 ;
      private String AV48DynamicFiltersSelector3 ;
      private String AV55WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 ;
      private String AV59WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 ;
      private String AV63WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00RN2_A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private short[] P00RN2_A1779AutorizacaoConsumo_Quantidade ;
      private String[] P00RN2_A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool[] P00RN2_n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private DateTime[] P00RN2_A1776AutorizacaoConsumo_VigenciaFim ;
      private int[] P00RN2_A74Contrato_Codigo ;
      private int[] P00RN2_A1774AutorizacaoConsumo_Codigo ;
      private DateTime[] P00RN2_A1775AutorizacaoConsumo_VigenciaInicio ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV39GridStateDynamicFilter ;
   }

   public class getwwautorizacaoconsumofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00RN2( IGxContext context ,
                                             String AV55WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV56WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 ,
                                             DateTime AV57WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 ,
                                             bool AV58WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 ,
                                             String AV59WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 ,
                                             DateTime AV60WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 ,
                                             DateTime AV61WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 ,
                                             bool AV62WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 ,
                                             String AV63WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 ,
                                             DateTime AV64WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 ,
                                             DateTime AV65WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 ,
                                             int AV66WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo ,
                                             int AV67WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to ,
                                             int AV68WWAutorizacaoConsumoDS_14_Tfcontrato_codigo ,
                                             int AV69WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to ,
                                             DateTime AV70WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio ,
                                             DateTime AV71WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to ,
                                             DateTime AV72WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim ,
                                             DateTime AV73WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to ,
                                             String AV75WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel ,
                                             String AV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom ,
                                             short AV76WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade ,
                                             short AV77WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to ,
                                             DateTime A1775AutorizacaoConsumo_VigenciaInicio ,
                                             int A1774AutorizacaoConsumo_Codigo ,
                                             int A74Contrato_Codigo ,
                                             DateTime A1776AutorizacaoConsumo_VigenciaFim ,
                                             String A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                             short A1779AutorizacaoConsumo_Quantidade )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [18] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[AutorizacaoConsumo_UnidadeMedicaoCod] AS AutorizacaoConsumo_UnidadeMedicaoCod, T1.[AutorizacaoConsumo_Quantidade], T2.[UnidadeMedicao_Nome] AS AutorizacaoConsumo_UnidadeMedicaoNom, T1.[AutorizacaoConsumo_VigenciaFim], T1.[Contrato_Codigo], T1.[AutorizacaoConsumo_Codigo], T1.[AutorizacaoConsumo_VigenciaInicio] FROM ([AutorizacaoConsumo] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[AutorizacaoConsumo_UnidadeMedicaoCod])";
         if ( ( StringUtil.StrCmp(AV55WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV56WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV56WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV56WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV55WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV57WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV57WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV57WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV58WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV60WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV60WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV60WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV58WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV61WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV61WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV61WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV62WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV64WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV64WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV64WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV62WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV65WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV65WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV65WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV66WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] >= @AV66WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Codigo] >= @AV66WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV67WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] <= @AV67WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Codigo] <= @AV67WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV68WWAutorizacaoConsumoDS_14_Tfcontrato_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV68WWAutorizacaoConsumoDS_14_Tfcontrato_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV68WWAutorizacaoConsumoDS_14_Tfcontrato_codigo)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV69WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV69WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV69WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV70WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV70WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV70WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV71WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV71WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV71WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV72WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] >= @AV72WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaFim] >= @AV72WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV73WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] <= @AV73WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaFim] <= @AV73WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] like @lV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV75WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] = @AV75WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (0==AV76WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] >= @AV76WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Quantidade] >= @AV76WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (0==AV77WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] <= @AV77WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Quantidade] <= @AV77WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[AutorizacaoConsumo_UnidadeMedicaoCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00RN2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (DateTime)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00RN2 ;
          prmP00RN2 = new Object[] {
          new Object[] {"@AV56WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV57WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV60WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV61WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV64WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV65WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV66WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV67WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWAutorizacaoConsumoDS_14_Tfcontrato_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV69WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV70WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV71WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV72WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV73WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV74WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV75WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV76WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV77WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to",SqlDbType.SmallInt,3,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00RN2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00RN2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[35]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwautorizacaoconsumofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwautorizacaoconsumofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwautorizacaoconsumofilterdata") )
          {
             return  ;
          }
          getwwautorizacaoconsumofilterdata worker = new getwwautorizacaoconsumofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
