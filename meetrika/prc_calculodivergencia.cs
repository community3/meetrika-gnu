/*
               File: PRC_CalculoDivergencia
        Description: Calculo Divergencia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:48.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_calculodivergencia : GXProcedure
   {
      public prc_calculodivergencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_calculodivergencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo ,
                           out String aP1_Contrato_CalculoDivergencia )
      {
         this.AV9Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV10Contrato_CalculoDivergencia = "" ;
         initialize();
         executePrivate();
         aP1_Contrato_CalculoDivergencia=this.AV10Contrato_CalculoDivergencia;
      }

      public String executeUdp( int aP0_Contratada_Codigo )
      {
         this.AV9Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV10Contrato_CalculoDivergencia = "" ;
         initialize();
         executePrivate();
         aP1_Contrato_CalculoDivergencia=this.AV10Contrato_CalculoDivergencia;
         return AV10Contrato_CalculoDivergencia ;
      }

      public void executeSubmit( int aP0_Contratada_Codigo ,
                                 out String aP1_Contrato_CalculoDivergencia )
      {
         prc_calculodivergencia objprc_calculodivergencia;
         objprc_calculodivergencia = new prc_calculodivergencia();
         objprc_calculodivergencia.AV9Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_calculodivergencia.AV10Contrato_CalculoDivergencia = "" ;
         objprc_calculodivergencia.context.SetSubmitInitialConfig(context);
         objprc_calculodivergencia.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_calculodivergencia);
         aP1_Contrato_CalculoDivergencia=this.AV10Contrato_CalculoDivergencia;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_calculodivergencia)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P007H2 */
         pr_default.execute(0, new Object[] {AV9Contratada_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A92Contrato_Ativo = P007H2_A92Contrato_Ativo[0];
            A39Contratada_Codigo = P007H2_A39Contratada_Codigo[0];
            A452Contrato_CalculoDivergencia = P007H2_A452Contrato_CalculoDivergencia[0];
            A74Contrato_Codigo = P007H2_A74Contrato_Codigo[0];
            OV10Contrato_CalculoDivergencia = AV10Contrato_CalculoDivergencia;
            AV10Contrato_CalculoDivergencia = A452Contrato_CalculoDivergencia;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P007H2_A92Contrato_Ativo = new bool[] {false} ;
         P007H2_A39Contratada_Codigo = new int[1] ;
         P007H2_A452Contrato_CalculoDivergencia = new String[] {""} ;
         P007H2_A74Contrato_Codigo = new int[1] ;
         A452Contrato_CalculoDivergencia = "";
         OV10Contrato_CalculoDivergencia = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_calculodivergencia__default(),
            new Object[][] {
                new Object[] {
               P007H2_A92Contrato_Ativo, P007H2_A39Contratada_Codigo, P007H2_A452Contrato_CalculoDivergencia, P007H2_A74Contrato_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9Contratada_Codigo ;
      private int A39Contratada_Codigo ;
      private int A74Contrato_Codigo ;
      private String AV10Contrato_CalculoDivergencia ;
      private String scmdbuf ;
      private String A452Contrato_CalculoDivergencia ;
      private String OV10Contrato_CalculoDivergencia ;
      private bool A92Contrato_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private bool[] P007H2_A92Contrato_Ativo ;
      private int[] P007H2_A39Contratada_Codigo ;
      private String[] P007H2_A452Contrato_CalculoDivergencia ;
      private int[] P007H2_A74Contrato_Codigo ;
      private String aP1_Contrato_CalculoDivergencia ;
   }

   public class prc_calculodivergencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP007H2 ;
          prmP007H2 = new Object[] {
          new Object[] {"@AV9Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P007H2", "SELECT TOP 1 [Contrato_Ativo], [Contratada_Codigo], [Contrato_CalculoDivergencia], [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE ([Contratada_Codigo] = @AV9Contratada_Codigo) AND ([Contrato_Ativo] = 1) ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007H2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
