/*
               File: RegrasContagemAnexos
        Description: Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:26:8.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class regrascontagemanexos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Anexos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public regrascontagemanexos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public regrascontagemanexos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_31123( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_31123e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_31123( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_31123( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_31123e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_18_31123( true) ;
         }
         return  ;
      }

      protected void wb_table3_18_31123e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_31123e( true) ;
         }
         else
         {
            wb_table1_2_31123e( false) ;
         }
      }

      protected void wb_table3_18_31123( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_RegrasContagemAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_RegrasContagemAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_RegrasContagemAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_18_31123e( true) ;
         }
         else
         {
            wb_table3_18_31123e( false) ;
         }
      }

      protected void wb_table2_5_31123( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_31123( true) ;
         }
         return  ;
      }

      protected void wb_table4_11_31123e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_31123e( true) ;
         }
         else
         {
            wb_table2_5_31123e( false) ;
         }
      }

      protected void wb_table4_11_31123( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoevidencia_arquivo_Internalname, "Arquivo", "", "", lblTextblockcontagemresultadoevidencia_arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_RegrasContagemAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_31123e( true) ;
         }
         else
         {
            wb_table4_11_31123e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               /* Read saved values. */
               Z1005RegrasContagemAnexo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1005RegrasContagemAnexo_Codigo"), ",", "."));
               Z1008RegrasContagemAnexo_NomeArq = cgiGet( "Z1008RegrasContagemAnexo_NomeArq");
               n1008RegrasContagemAnexo_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1008RegrasContagemAnexo_NomeArq)) ? true : false);
               Z1009RegrasContagemAnexo_TipoArq = cgiGet( "Z1009RegrasContagemAnexo_TipoArq");
               n1009RegrasContagemAnexo_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1009RegrasContagemAnexo_TipoArq)) ? true : false);
               Z1010RegrasContagemAnexo_Data = context.localUtil.CToD( cgiGet( "Z1010RegrasContagemAnexo_Data"), 0);
               n1010RegrasContagemAnexo_Data = ((DateTime.MinValue==A1010RegrasContagemAnexo_Data) ? true : false);
               Z645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z645TipoDocumento_Codigo"), ",", "."));
               n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
               Z860RegrasContagem_Regra = cgiGet( "Z860RegrasContagem_Regra");
               A1008RegrasContagemAnexo_NomeArq = cgiGet( "Z1008RegrasContagemAnexo_NomeArq");
               n1008RegrasContagemAnexo_NomeArq = false;
               n1008RegrasContagemAnexo_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1008RegrasContagemAnexo_NomeArq)) ? true : false);
               A1009RegrasContagemAnexo_TipoArq = cgiGet( "Z1009RegrasContagemAnexo_TipoArq");
               n1009RegrasContagemAnexo_TipoArq = false;
               n1009RegrasContagemAnexo_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1009RegrasContagemAnexo_TipoArq)) ? true : false);
               A1010RegrasContagemAnexo_Data = context.localUtil.CToD( cgiGet( "Z1010RegrasContagemAnexo_Data"), 0);
               n1010RegrasContagemAnexo_Data = false;
               n1010RegrasContagemAnexo_Data = ((DateTime.MinValue==A1010RegrasContagemAnexo_Data) ? true : false);
               A645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z645TipoDocumento_Codigo"), ",", "."));
               n645TipoDocumento_Codigo = false;
               n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
               A860RegrasContagem_Regra = cgiGet( "Z860RegrasContagem_Regra");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               A1005RegrasContagemAnexo_Codigo = (int)(context.localUtil.CToN( cgiGet( "REGRASCONTAGEMANEXO_CODIGO"), ",", "."));
               A1007RegrasContagemAnexo_Arquivo = cgiGet( "REGRASCONTAGEMANEXO_ARQUIVO");
               n1007RegrasContagemAnexo_Arquivo = false;
               n1007RegrasContagemAnexo_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A1007RegrasContagemAnexo_Arquivo)) ? true : false);
               A1008RegrasContagemAnexo_NomeArq = cgiGet( "REGRASCONTAGEMANEXO_NOMEARQ");
               n1008RegrasContagemAnexo_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1008RegrasContagemAnexo_NomeArq)) ? true : false);
               A1009RegrasContagemAnexo_TipoArq = cgiGet( "REGRASCONTAGEMANEXO_TIPOARQ");
               n1009RegrasContagemAnexo_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1009RegrasContagemAnexo_TipoArq)) ? true : false);
               A1010RegrasContagemAnexo_Data = context.localUtil.CToD( cgiGet( "REGRASCONTAGEMANEXO_DATA"), 0);
               n1010RegrasContagemAnexo_Data = ((DateTime.MinValue==A1010RegrasContagemAnexo_Data) ? true : false);
               A1011RegrasContagemAnexo_Descricao = cgiGet( "REGRASCONTAGEMANEXO_DESCRICAO");
               n1011RegrasContagemAnexo_Descricao = false;
               n1011RegrasContagemAnexo_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A1011RegrasContagemAnexo_Descricao)) ? true : false);
               A645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "TIPODOCUMENTO_CODIGO"), ",", "."));
               n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
               A860RegrasContagem_Regra = cgiGet( "REGRASCONTAGEM_REGRA");
               A646TipoDocumento_Nome = cgiGet( "TIPODOCUMENTO_NOME");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "RegrasContagemAnexos";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1005RegrasContagemAnexo_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A860RegrasContagem_Regra, ""));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1008RegrasContagemAnexo_NomeArq, ""));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1009RegrasContagemAnexo_TipoArq, ""));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1010RegrasContagemAnexo_Data, "99/99/99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("regrascontagemanexos:[SecurityCheckFailed value for]"+"RegrasContagemAnexo_Codigo:"+context.localUtil.Format( (decimal)(A1005RegrasContagemAnexo_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("regrascontagemanexos:[SecurityCheckFailed value for]"+"RegrasContagem_Regra:"+StringUtil.RTrim( context.localUtil.Format( A860RegrasContagem_Regra, "")));
                  GXUtil.WriteLog("regrascontagemanexos:[SecurityCheckFailed value for]"+"RegrasContagemAnexo_NomeArq:"+StringUtil.RTrim( context.localUtil.Format( A1008RegrasContagemAnexo_NomeArq, "")));
                  GXUtil.WriteLog("regrascontagemanexos:[SecurityCheckFailed value for]"+"RegrasContagemAnexo_TipoArq:"+StringUtil.RTrim( context.localUtil.Format( A1009RegrasContagemAnexo_TipoArq, "")));
                  GXUtil.WriteLog("regrascontagemanexos:[SecurityCheckFailed value for]"+"RegrasContagemAnexo_Data:"+context.localUtil.Format(A1010RegrasContagemAnexo_Data, "99/99/99"));
                  GXUtil.WriteLog("regrascontagemanexos:[SecurityCheckFailed value for]"+"TipoDocumento_Codigo:"+context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1005RegrasContagemAnexo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1005RegrasContagemAnexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1005RegrasContagemAnexo_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll31123( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_trn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
         }
         DisableAttributes31123( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption310( )
      {
      }

      protected void ZM31123( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1008RegrasContagemAnexo_NomeArq = T00313_A1008RegrasContagemAnexo_NomeArq[0];
               Z1009RegrasContagemAnexo_TipoArq = T00313_A1009RegrasContagemAnexo_TipoArq[0];
               Z1010RegrasContagemAnexo_Data = T00313_A1010RegrasContagemAnexo_Data[0];
               Z645TipoDocumento_Codigo = T00313_A645TipoDocumento_Codigo[0];
               Z860RegrasContagem_Regra = T00313_A860RegrasContagem_Regra[0];
            }
            else
            {
               Z1008RegrasContagemAnexo_NomeArq = A1008RegrasContagemAnexo_NomeArq;
               Z1009RegrasContagemAnexo_TipoArq = A1009RegrasContagemAnexo_TipoArq;
               Z1010RegrasContagemAnexo_Data = A1010RegrasContagemAnexo_Data;
               Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
               Z860RegrasContagem_Regra = A860RegrasContagem_Regra;
            }
         }
         if ( GX_JID == -1 )
         {
            Z1005RegrasContagemAnexo_Codigo = A1005RegrasContagemAnexo_Codigo;
            Z1007RegrasContagemAnexo_Arquivo = A1007RegrasContagemAnexo_Arquivo;
            Z1008RegrasContagemAnexo_NomeArq = A1008RegrasContagemAnexo_NomeArq;
            Z1009RegrasContagemAnexo_TipoArq = A1009RegrasContagemAnexo_TipoArq;
            Z1010RegrasContagemAnexo_Data = A1010RegrasContagemAnexo_Data;
            Z1011RegrasContagemAnexo_Descricao = A1011RegrasContagemAnexo_Descricao;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z860RegrasContagem_Regra = A860RegrasContagem_Regra;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         /* Using cursor T00314 */
         pr_default.execute(2, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A646TipoDocumento_Nome = T00314_A646TipoDocumento_Nome[0];
         pr_default.close(2);
         /* Using cursor T00315 */
         pr_default.execute(3, new Object[] {A860RegrasContagem_Regra});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Regras de Contagem'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(3);
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_trn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load31123( )
      {
         /* Using cursor T00316 */
         pr_default.execute(4, new Object[] {A1005RegrasContagemAnexo_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound123 = 1;
            A1008RegrasContagemAnexo_NomeArq = T00316_A1008RegrasContagemAnexo_NomeArq[0];
            n1008RegrasContagemAnexo_NomeArq = T00316_n1008RegrasContagemAnexo_NomeArq[0];
            A1009RegrasContagemAnexo_TipoArq = T00316_A1009RegrasContagemAnexo_TipoArq[0];
            n1009RegrasContagemAnexo_TipoArq = T00316_n1009RegrasContagemAnexo_TipoArq[0];
            A1010RegrasContagemAnexo_Data = T00316_A1010RegrasContagemAnexo_Data[0];
            n1010RegrasContagemAnexo_Data = T00316_n1010RegrasContagemAnexo_Data[0];
            A1011RegrasContagemAnexo_Descricao = T00316_A1011RegrasContagemAnexo_Descricao[0];
            n1011RegrasContagemAnexo_Descricao = T00316_n1011RegrasContagemAnexo_Descricao[0];
            A646TipoDocumento_Nome = T00316_A646TipoDocumento_Nome[0];
            A645TipoDocumento_Codigo = T00316_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = T00316_n645TipoDocumento_Codigo[0];
            A860RegrasContagem_Regra = T00316_A860RegrasContagem_Regra[0];
            A1007RegrasContagemAnexo_Arquivo = T00316_A1007RegrasContagemAnexo_Arquivo[0];
            n1007RegrasContagemAnexo_Arquivo = T00316_n1007RegrasContagemAnexo_Arquivo[0];
            ZM31123( -1) ;
         }
         pr_default.close(4);
         OnLoadActions31123( ) ;
      }

      protected void OnLoadActions31123( )
      {
      }

      protected void CheckExtendedTable31123( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors31123( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey31123( )
      {
         /* Using cursor T00317 */
         pr_default.execute(5, new Object[] {A1005RegrasContagemAnexo_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound123 = 1;
         }
         else
         {
            RcdFound123 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00313 */
         pr_default.execute(1, new Object[] {A1005RegrasContagemAnexo_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM31123( 1) ;
            RcdFound123 = 1;
            A1005RegrasContagemAnexo_Codigo = T00313_A1005RegrasContagemAnexo_Codigo[0];
            A1008RegrasContagemAnexo_NomeArq = T00313_A1008RegrasContagemAnexo_NomeArq[0];
            n1008RegrasContagemAnexo_NomeArq = T00313_n1008RegrasContagemAnexo_NomeArq[0];
            A1009RegrasContagemAnexo_TipoArq = T00313_A1009RegrasContagemAnexo_TipoArq[0];
            n1009RegrasContagemAnexo_TipoArq = T00313_n1009RegrasContagemAnexo_TipoArq[0];
            A1010RegrasContagemAnexo_Data = T00313_A1010RegrasContagemAnexo_Data[0];
            n1010RegrasContagemAnexo_Data = T00313_n1010RegrasContagemAnexo_Data[0];
            A1011RegrasContagemAnexo_Descricao = T00313_A1011RegrasContagemAnexo_Descricao[0];
            n1011RegrasContagemAnexo_Descricao = T00313_n1011RegrasContagemAnexo_Descricao[0];
            A645TipoDocumento_Codigo = T00313_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = T00313_n645TipoDocumento_Codigo[0];
            A860RegrasContagem_Regra = T00313_A860RegrasContagem_Regra[0];
            A1007RegrasContagemAnexo_Arquivo = T00313_A1007RegrasContagemAnexo_Arquivo[0];
            n1007RegrasContagemAnexo_Arquivo = T00313_n1007RegrasContagemAnexo_Arquivo[0];
            Z1005RegrasContagemAnexo_Codigo = A1005RegrasContagemAnexo_Codigo;
            sMode123 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load31123( ) ;
            if ( AnyError == 1 )
            {
               RcdFound123 = 0;
               InitializeNonKey31123( ) ;
            }
            Gx_mode = sMode123;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound123 = 0;
            InitializeNonKey31123( ) ;
            sMode123 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode123;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey31123( ) ;
         if ( RcdFound123 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound123 = 0;
         /* Using cursor T00318 */
         pr_default.execute(6, new Object[] {A1005RegrasContagemAnexo_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T00318_A1005RegrasContagemAnexo_Codigo[0] < A1005RegrasContagemAnexo_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T00318_A1005RegrasContagemAnexo_Codigo[0] > A1005RegrasContagemAnexo_Codigo ) ) )
            {
               A1005RegrasContagemAnexo_Codigo = T00318_A1005RegrasContagemAnexo_Codigo[0];
               RcdFound123 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound123 = 0;
         /* Using cursor T00319 */
         pr_default.execute(7, new Object[] {A1005RegrasContagemAnexo_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T00319_A1005RegrasContagemAnexo_Codigo[0] > A1005RegrasContagemAnexo_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T00319_A1005RegrasContagemAnexo_Codigo[0] < A1005RegrasContagemAnexo_Codigo ) ) )
            {
               A1005RegrasContagemAnexo_Codigo = T00319_A1005RegrasContagemAnexo_Codigo[0];
               RcdFound123 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey31123( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert31123( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound123 == 1 )
            {
               if ( A1005RegrasContagemAnexo_Codigo != Z1005RegrasContagemAnexo_Codigo )
               {
                  A1005RegrasContagemAnexo_Codigo = Z1005RegrasContagemAnexo_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1005RegrasContagemAnexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1005RegrasContagemAnexo_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update31123( ) ;
               }
            }
            else
            {
               if ( A1005RegrasContagemAnexo_Codigo != Z1005RegrasContagemAnexo_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  Insert31123( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     Insert31123( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1005RegrasContagemAnexo_Codigo != Z1005RegrasContagemAnexo_Codigo )
         {
            A1005RegrasContagemAnexo_Codigo = Z1005RegrasContagemAnexo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1005RegrasContagemAnexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1005RegrasContagemAnexo_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound123 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart31123( ) ;
         if ( RcdFound123 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd31123( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound123 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound123 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart31123( ) ;
         if ( RcdFound123 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound123 != 0 )
            {
               ScanNext31123( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd31123( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency31123( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00312 */
            pr_default.execute(0, new Object[] {A1005RegrasContagemAnexo_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RegrasContagemAnexos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1008RegrasContagemAnexo_NomeArq, T00312_A1008RegrasContagemAnexo_NomeArq[0]) != 0 ) || ( StringUtil.StrCmp(Z1009RegrasContagemAnexo_TipoArq, T00312_A1009RegrasContagemAnexo_TipoArq[0]) != 0 ) || ( Z1010RegrasContagemAnexo_Data != T00312_A1010RegrasContagemAnexo_Data[0] ) || ( Z645TipoDocumento_Codigo != T00312_A645TipoDocumento_Codigo[0] ) || ( StringUtil.StrCmp(Z860RegrasContagem_Regra, T00312_A860RegrasContagem_Regra[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z1008RegrasContagemAnexo_NomeArq, T00312_A1008RegrasContagemAnexo_NomeArq[0]) != 0 )
               {
                  GXUtil.WriteLog("regrascontagemanexos:[seudo value changed for attri]"+"RegrasContagemAnexo_NomeArq");
                  GXUtil.WriteLogRaw("Old: ",Z1008RegrasContagemAnexo_NomeArq);
                  GXUtil.WriteLogRaw("Current: ",T00312_A1008RegrasContagemAnexo_NomeArq[0]);
               }
               if ( StringUtil.StrCmp(Z1009RegrasContagemAnexo_TipoArq, T00312_A1009RegrasContagemAnexo_TipoArq[0]) != 0 )
               {
                  GXUtil.WriteLog("regrascontagemanexos:[seudo value changed for attri]"+"RegrasContagemAnexo_TipoArq");
                  GXUtil.WriteLogRaw("Old: ",Z1009RegrasContagemAnexo_TipoArq);
                  GXUtil.WriteLogRaw("Current: ",T00312_A1009RegrasContagemAnexo_TipoArq[0]);
               }
               if ( Z1010RegrasContagemAnexo_Data != T00312_A1010RegrasContagemAnexo_Data[0] )
               {
                  GXUtil.WriteLog("regrascontagemanexos:[seudo value changed for attri]"+"RegrasContagemAnexo_Data");
                  GXUtil.WriteLogRaw("Old: ",Z1010RegrasContagemAnexo_Data);
                  GXUtil.WriteLogRaw("Current: ",T00312_A1010RegrasContagemAnexo_Data[0]);
               }
               if ( Z645TipoDocumento_Codigo != T00312_A645TipoDocumento_Codigo[0] )
               {
                  GXUtil.WriteLog("regrascontagemanexos:[seudo value changed for attri]"+"TipoDocumento_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z645TipoDocumento_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00312_A645TipoDocumento_Codigo[0]);
               }
               if ( StringUtil.StrCmp(Z860RegrasContagem_Regra, T00312_A860RegrasContagem_Regra[0]) != 0 )
               {
                  GXUtil.WriteLog("regrascontagemanexos:[seudo value changed for attri]"+"RegrasContagem_Regra");
                  GXUtil.WriteLogRaw("Old: ",Z860RegrasContagem_Regra);
                  GXUtil.WriteLogRaw("Current: ",T00312_A860RegrasContagem_Regra[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"RegrasContagemAnexos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert31123( )
      {
         BeforeValidate31123( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable31123( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM31123( 0) ;
            CheckOptimisticConcurrency31123( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm31123( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert31123( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003110 */
                     pr_default.execute(8, new Object[] {n1007RegrasContagemAnexo_Arquivo, A1007RegrasContagemAnexo_Arquivo, n1008RegrasContagemAnexo_NomeArq, A1008RegrasContagemAnexo_NomeArq, n1009RegrasContagemAnexo_TipoArq, A1009RegrasContagemAnexo_TipoArq, n1010RegrasContagemAnexo_Data, A1010RegrasContagemAnexo_Data, n1011RegrasContagemAnexo_Descricao, A1011RegrasContagemAnexo_Descricao, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A860RegrasContagem_Regra});
                     A1005RegrasContagemAnexo_Codigo = T003110_A1005RegrasContagemAnexo_Codigo[0];
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("RegrasContagemAnexos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption310( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load31123( ) ;
            }
            EndLevel31123( ) ;
         }
         CloseExtendedTableCursors31123( ) ;
      }

      protected void Update31123( )
      {
         BeforeValidate31123( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable31123( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency31123( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm31123( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate31123( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003111 */
                     pr_default.execute(9, new Object[] {n1008RegrasContagemAnexo_NomeArq, A1008RegrasContagemAnexo_NomeArq, n1009RegrasContagemAnexo_TipoArq, A1009RegrasContagemAnexo_TipoArq, n1010RegrasContagemAnexo_Data, A1010RegrasContagemAnexo_Data, n1011RegrasContagemAnexo_Descricao, A1011RegrasContagemAnexo_Descricao, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A860RegrasContagem_Regra, A1005RegrasContagemAnexo_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("RegrasContagemAnexos") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RegrasContagemAnexos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate31123( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption310( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel31123( ) ;
         }
         CloseExtendedTableCursors31123( ) ;
      }

      protected void DeferredUpdate31123( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T003112 */
            pr_default.execute(10, new Object[] {n1007RegrasContagemAnexo_Arquivo, A1007RegrasContagemAnexo_Arquivo, A1005RegrasContagemAnexo_Codigo});
            pr_default.close(10);
            dsDefault.SmartCacheProvider.SetUpdated("RegrasContagemAnexos") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate31123( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency31123( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls31123( ) ;
            AfterConfirm31123( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete31123( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003113 */
                  pr_default.execute(11, new Object[] {A1005RegrasContagemAnexo_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("RegrasContagemAnexos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound123 == 0 )
                        {
                           InitAll31123( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption310( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode123 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel31123( ) ;
         Gx_mode = sMode123;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls31123( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel31123( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete31123( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "RegrasContagemAnexos");
            if ( AnyError == 0 )
            {
               ConfirmValues310( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "RegrasContagemAnexos");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart31123( )
      {
         /* Using cursor T003114 */
         pr_default.execute(12);
         RcdFound123 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound123 = 1;
            A1005RegrasContagemAnexo_Codigo = T003114_A1005RegrasContagemAnexo_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext31123( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound123 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound123 = 1;
            A1005RegrasContagemAnexo_Codigo = T003114_A1005RegrasContagemAnexo_Codigo[0];
         }
      }

      protected void ScanEnd31123( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm31123( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert31123( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate31123( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete31123( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete31123( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate31123( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes31123( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues310( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311726937");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("regrascontagemanexos.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1005RegrasContagemAnexo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1005RegrasContagemAnexo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1008RegrasContagemAnexo_NomeArq", StringUtil.RTrim( Z1008RegrasContagemAnexo_NomeArq));
         GxWebStd.gx_hidden_field( context, "Z1009RegrasContagemAnexo_TipoArq", StringUtil.RTrim( Z1009RegrasContagemAnexo_TipoArq));
         GxWebStd.gx_hidden_field( context, "Z1010RegrasContagemAnexo_Data", context.localUtil.DToC( Z1010RegrasContagemAnexo_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z645TipoDocumento_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z860RegrasContagem_Regra", Z860RegrasContagem_Regra);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "REGRASCONTAGEMANEXO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1005RegrasContagemAnexo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REGRASCONTAGEMANEXO_ARQUIVO", A1007RegrasContagemAnexo_Arquivo);
         GxWebStd.gx_hidden_field( context, "REGRASCONTAGEMANEXO_NOMEARQ", StringUtil.RTrim( A1008RegrasContagemAnexo_NomeArq));
         GxWebStd.gx_hidden_field( context, "REGRASCONTAGEMANEXO_TIPOARQ", StringUtil.RTrim( A1009RegrasContagemAnexo_TipoArq));
         GxWebStd.gx_hidden_field( context, "REGRASCONTAGEMANEXO_DATA", context.localUtil.DToC( A1010RegrasContagemAnexo_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "REGRASCONTAGEMANEXO_DESCRICAO", A1011RegrasContagemAnexo_Descricao);
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REGRASCONTAGEM_REGRA", A860RegrasContagem_Regra);
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_NOME", StringUtil.RTrim( A646TipoDocumento_Nome));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "RegrasContagemAnexos";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1005RegrasContagemAnexo_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A860RegrasContagem_Regra, ""));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1008RegrasContagemAnexo_NomeArq, ""));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1009RegrasContagemAnexo_TipoArq, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1010RegrasContagemAnexo_Data, "99/99/99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("regrascontagemanexos:[SendSecurityCheck value for]"+"RegrasContagemAnexo_Codigo:"+context.localUtil.Format( (decimal)(A1005RegrasContagemAnexo_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("regrascontagemanexos:[SendSecurityCheck value for]"+"RegrasContagem_Regra:"+StringUtil.RTrim( context.localUtil.Format( A860RegrasContagem_Regra, "")));
         GXUtil.WriteLog("regrascontagemanexos:[SendSecurityCheck value for]"+"RegrasContagemAnexo_NomeArq:"+StringUtil.RTrim( context.localUtil.Format( A1008RegrasContagemAnexo_NomeArq, "")));
         GXUtil.WriteLog("regrascontagemanexos:[SendSecurityCheck value for]"+"RegrasContagemAnexo_TipoArq:"+StringUtil.RTrim( context.localUtil.Format( A1009RegrasContagemAnexo_TipoArq, "")));
         GXUtil.WriteLog("regrascontagemanexos:[SendSecurityCheck value for]"+"RegrasContagemAnexo_Data:"+context.localUtil.Format(A1010RegrasContagemAnexo_Data, "99/99/99"));
         GXUtil.WriteLog("regrascontagemanexos:[SendSecurityCheck value for]"+"TipoDocumento_Codigo:"+context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("regrascontagemanexos.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "RegrasContagemAnexos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Anexos" ;
      }

      protected void InitializeNonKey31123( )
      {
         A860RegrasContagem_Regra = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A860RegrasContagem_Regra", A860RegrasContagem_Regra);
         A1007RegrasContagemAnexo_Arquivo = "";
         n1007RegrasContagemAnexo_Arquivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1007RegrasContagemAnexo_Arquivo", A1007RegrasContagemAnexo_Arquivo);
         A1008RegrasContagemAnexo_NomeArq = "";
         n1008RegrasContagemAnexo_NomeArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1008RegrasContagemAnexo_NomeArq", A1008RegrasContagemAnexo_NomeArq);
         A1009RegrasContagemAnexo_TipoArq = "";
         n1009RegrasContagemAnexo_TipoArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1009RegrasContagemAnexo_TipoArq", A1009RegrasContagemAnexo_TipoArq);
         A1010RegrasContagemAnexo_Data = DateTime.MinValue;
         n1010RegrasContagemAnexo_Data = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1010RegrasContagemAnexo_Data", context.localUtil.Format(A1010RegrasContagemAnexo_Data, "99/99/99"));
         A1011RegrasContagemAnexo_Descricao = "";
         n1011RegrasContagemAnexo_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1011RegrasContagemAnexo_Descricao", A1011RegrasContagemAnexo_Descricao);
         A645TipoDocumento_Codigo = 0;
         n645TipoDocumento_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
         A646TipoDocumento_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A646TipoDocumento_Nome", A646TipoDocumento_Nome);
         Z1008RegrasContagemAnexo_NomeArq = "";
         Z1009RegrasContagemAnexo_TipoArq = "";
         Z1010RegrasContagemAnexo_Data = DateTime.MinValue;
         Z645TipoDocumento_Codigo = 0;
         Z860RegrasContagem_Regra = "";
      }

      protected void InitAll31123( )
      {
         A1005RegrasContagemAnexo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1005RegrasContagemAnexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1005RegrasContagemAnexo_Codigo), 6, 0)));
         InitializeNonKey31123( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311726942");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("regrascontagemanexos.js", "?2020311726942");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultadoevidencia_arquivo_Internalname = "TEXTBLOCKCONTAGEMRESULTADOEVIDENCIA_ARQUIVO";
         tblTable2_Internalname = "TABLE2";
         tblTable1_Internalname = "TABLE1";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTable3_Internalname = "TABLE3";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Anexos";
         bttBtn_trn_delete_Enabled = 1;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( AnyError == 0 )
         {
            GX_FocusControl = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1008RegrasContagemAnexo_NomeArq = "";
         Z1009RegrasContagemAnexo_TipoArq = "";
         Z1010RegrasContagemAnexo_Data = DateTime.MinValue;
         Z860RegrasContagem_Regra = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontagemresultadoevidencia_arquivo_Jsonclick = "";
         A1008RegrasContagemAnexo_NomeArq = "";
         A1009RegrasContagemAnexo_TipoArq = "";
         A1010RegrasContagemAnexo_Data = DateTime.MinValue;
         A860RegrasContagem_Regra = "";
         Gx_mode = "";
         A1007RegrasContagemAnexo_Arquivo = "";
         A1011RegrasContagemAnexo_Descricao = "";
         A646TipoDocumento_Nome = "";
         forbiddenHiddens = "";
         hsh = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1007RegrasContagemAnexo_Arquivo = "";
         Z1011RegrasContagemAnexo_Descricao = "";
         Z646TipoDocumento_Nome = "";
         T00314_A646TipoDocumento_Nome = new String[] {""} ;
         T00315_A860RegrasContagem_Regra = new String[] {""} ;
         T00316_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         T00316_A1008RegrasContagemAnexo_NomeArq = new String[] {""} ;
         T00316_n1008RegrasContagemAnexo_NomeArq = new bool[] {false} ;
         T00316_A1009RegrasContagemAnexo_TipoArq = new String[] {""} ;
         T00316_n1009RegrasContagemAnexo_TipoArq = new bool[] {false} ;
         T00316_A1010RegrasContagemAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         T00316_n1010RegrasContagemAnexo_Data = new bool[] {false} ;
         T00316_A1011RegrasContagemAnexo_Descricao = new String[] {""} ;
         T00316_n1011RegrasContagemAnexo_Descricao = new bool[] {false} ;
         T00316_A646TipoDocumento_Nome = new String[] {""} ;
         T00316_A645TipoDocumento_Codigo = new int[1] ;
         T00316_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00316_A860RegrasContagem_Regra = new String[] {""} ;
         T00316_A1007RegrasContagemAnexo_Arquivo = new String[] {""} ;
         T00316_n1007RegrasContagemAnexo_Arquivo = new bool[] {false} ;
         T00317_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         T00313_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         T00313_A1008RegrasContagemAnexo_NomeArq = new String[] {""} ;
         T00313_n1008RegrasContagemAnexo_NomeArq = new bool[] {false} ;
         T00313_A1009RegrasContagemAnexo_TipoArq = new String[] {""} ;
         T00313_n1009RegrasContagemAnexo_TipoArq = new bool[] {false} ;
         T00313_A1010RegrasContagemAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         T00313_n1010RegrasContagemAnexo_Data = new bool[] {false} ;
         T00313_A1011RegrasContagemAnexo_Descricao = new String[] {""} ;
         T00313_n1011RegrasContagemAnexo_Descricao = new bool[] {false} ;
         T00313_A645TipoDocumento_Codigo = new int[1] ;
         T00313_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00313_A860RegrasContagem_Regra = new String[] {""} ;
         T00313_A1007RegrasContagemAnexo_Arquivo = new String[] {""} ;
         T00313_n1007RegrasContagemAnexo_Arquivo = new bool[] {false} ;
         sMode123 = "";
         T00318_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         T00319_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         GX_FocusControl = "";
         T00312_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         T00312_A1008RegrasContagemAnexo_NomeArq = new String[] {""} ;
         T00312_n1008RegrasContagemAnexo_NomeArq = new bool[] {false} ;
         T00312_A1009RegrasContagemAnexo_TipoArq = new String[] {""} ;
         T00312_n1009RegrasContagemAnexo_TipoArq = new bool[] {false} ;
         T00312_A1010RegrasContagemAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         T00312_n1010RegrasContagemAnexo_Data = new bool[] {false} ;
         T00312_A1011RegrasContagemAnexo_Descricao = new String[] {""} ;
         T00312_n1011RegrasContagemAnexo_Descricao = new bool[] {false} ;
         T00312_A645TipoDocumento_Codigo = new int[1] ;
         T00312_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00312_A860RegrasContagem_Regra = new String[] {""} ;
         T00312_A1007RegrasContagemAnexo_Arquivo = new String[] {""} ;
         T00312_n1007RegrasContagemAnexo_Arquivo = new bool[] {false} ;
         T003110_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         T003114_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.regrascontagemanexos__default(),
            new Object[][] {
                new Object[] {
               T00312_A1005RegrasContagemAnexo_Codigo, T00312_A1008RegrasContagemAnexo_NomeArq, T00312_n1008RegrasContagemAnexo_NomeArq, T00312_A1009RegrasContagemAnexo_TipoArq, T00312_n1009RegrasContagemAnexo_TipoArq, T00312_A1010RegrasContagemAnexo_Data, T00312_n1010RegrasContagemAnexo_Data, T00312_A1011RegrasContagemAnexo_Descricao, T00312_n1011RegrasContagemAnexo_Descricao, T00312_A645TipoDocumento_Codigo,
               T00312_n645TipoDocumento_Codigo, T00312_A860RegrasContagem_Regra, T00312_A1007RegrasContagemAnexo_Arquivo, T00312_n1007RegrasContagemAnexo_Arquivo
               }
               , new Object[] {
               T00313_A1005RegrasContagemAnexo_Codigo, T00313_A1008RegrasContagemAnexo_NomeArq, T00313_n1008RegrasContagemAnexo_NomeArq, T00313_A1009RegrasContagemAnexo_TipoArq, T00313_n1009RegrasContagemAnexo_TipoArq, T00313_A1010RegrasContagemAnexo_Data, T00313_n1010RegrasContagemAnexo_Data, T00313_A1011RegrasContagemAnexo_Descricao, T00313_n1011RegrasContagemAnexo_Descricao, T00313_A645TipoDocumento_Codigo,
               T00313_n645TipoDocumento_Codigo, T00313_A860RegrasContagem_Regra, T00313_A1007RegrasContagemAnexo_Arquivo, T00313_n1007RegrasContagemAnexo_Arquivo
               }
               , new Object[] {
               T00314_A646TipoDocumento_Nome
               }
               , new Object[] {
               T00315_A860RegrasContagem_Regra
               }
               , new Object[] {
               T00316_A1005RegrasContagemAnexo_Codigo, T00316_A1008RegrasContagemAnexo_NomeArq, T00316_n1008RegrasContagemAnexo_NomeArq, T00316_A1009RegrasContagemAnexo_TipoArq, T00316_n1009RegrasContagemAnexo_TipoArq, T00316_A1010RegrasContagemAnexo_Data, T00316_n1010RegrasContagemAnexo_Data, T00316_A1011RegrasContagemAnexo_Descricao, T00316_n1011RegrasContagemAnexo_Descricao, T00316_A646TipoDocumento_Nome,
               T00316_A645TipoDocumento_Codigo, T00316_n645TipoDocumento_Codigo, T00316_A860RegrasContagem_Regra, T00316_A1007RegrasContagemAnexo_Arquivo, T00316_n1007RegrasContagemAnexo_Arquivo
               }
               , new Object[] {
               T00317_A1005RegrasContagemAnexo_Codigo
               }
               , new Object[] {
               T00318_A1005RegrasContagemAnexo_Codigo
               }
               , new Object[] {
               T00319_A1005RegrasContagemAnexo_Codigo
               }
               , new Object[] {
               T003110_A1005RegrasContagemAnexo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003114_A1005RegrasContagemAnexo_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound123 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int Z1005RegrasContagemAnexo_Codigo ;
      private int Z645TipoDocumento_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int A645TipoDocumento_Codigo ;
      private int A1005RegrasContagemAnexo_Codigo ;
      private int idxLst ;
      private String sPrefix ;
      private String Z1008RegrasContagemAnexo_NomeArq ;
      private String Z1009RegrasContagemAnexo_TipoArq ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTable3_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTable1_Internalname ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemresultadoevidencia_arquivo_Internalname ;
      private String lblTextblockcontagemresultadoevidencia_arquivo_Jsonclick ;
      private String A1008RegrasContagemAnexo_NomeArq ;
      private String A1009RegrasContagemAnexo_TipoArq ;
      private String Gx_mode ;
      private String A646TipoDocumento_Nome ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z646TipoDocumento_Nome ;
      private String sMode123 ;
      private String GX_FocusControl ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z1010RegrasContagemAnexo_Data ;
      private DateTime A1010RegrasContagemAnexo_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1008RegrasContagemAnexo_NomeArq ;
      private bool n1009RegrasContagemAnexo_TipoArq ;
      private bool n1010RegrasContagemAnexo_Data ;
      private bool n645TipoDocumento_Codigo ;
      private bool n1007RegrasContagemAnexo_Arquivo ;
      private bool n1011RegrasContagemAnexo_Descricao ;
      private String A1011RegrasContagemAnexo_Descricao ;
      private String Z1011RegrasContagemAnexo_Descricao ;
      private String Z860RegrasContagem_Regra ;
      private String A860RegrasContagem_Regra ;
      private String A1007RegrasContagemAnexo_Arquivo ;
      private String Z1007RegrasContagemAnexo_Arquivo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T00314_A646TipoDocumento_Nome ;
      private String[] T00315_A860RegrasContagem_Regra ;
      private int[] T00316_A1005RegrasContagemAnexo_Codigo ;
      private String[] T00316_A1008RegrasContagemAnexo_NomeArq ;
      private bool[] T00316_n1008RegrasContagemAnexo_NomeArq ;
      private String[] T00316_A1009RegrasContagemAnexo_TipoArq ;
      private bool[] T00316_n1009RegrasContagemAnexo_TipoArq ;
      private DateTime[] T00316_A1010RegrasContagemAnexo_Data ;
      private bool[] T00316_n1010RegrasContagemAnexo_Data ;
      private String[] T00316_A1011RegrasContagemAnexo_Descricao ;
      private bool[] T00316_n1011RegrasContagemAnexo_Descricao ;
      private String[] T00316_A646TipoDocumento_Nome ;
      private int[] T00316_A645TipoDocumento_Codigo ;
      private bool[] T00316_n645TipoDocumento_Codigo ;
      private String[] T00316_A860RegrasContagem_Regra ;
      private String[] T00316_A1007RegrasContagemAnexo_Arquivo ;
      private bool[] T00316_n1007RegrasContagemAnexo_Arquivo ;
      private int[] T00317_A1005RegrasContagemAnexo_Codigo ;
      private int[] T00313_A1005RegrasContagemAnexo_Codigo ;
      private String[] T00313_A1008RegrasContagemAnexo_NomeArq ;
      private bool[] T00313_n1008RegrasContagemAnexo_NomeArq ;
      private String[] T00313_A1009RegrasContagemAnexo_TipoArq ;
      private bool[] T00313_n1009RegrasContagemAnexo_TipoArq ;
      private DateTime[] T00313_A1010RegrasContagemAnexo_Data ;
      private bool[] T00313_n1010RegrasContagemAnexo_Data ;
      private String[] T00313_A1011RegrasContagemAnexo_Descricao ;
      private bool[] T00313_n1011RegrasContagemAnexo_Descricao ;
      private int[] T00313_A645TipoDocumento_Codigo ;
      private bool[] T00313_n645TipoDocumento_Codigo ;
      private String[] T00313_A860RegrasContagem_Regra ;
      private String[] T00313_A1007RegrasContagemAnexo_Arquivo ;
      private bool[] T00313_n1007RegrasContagemAnexo_Arquivo ;
      private int[] T00318_A1005RegrasContagemAnexo_Codigo ;
      private int[] T00319_A1005RegrasContagemAnexo_Codigo ;
      private int[] T00312_A1005RegrasContagemAnexo_Codigo ;
      private String[] T00312_A1008RegrasContagemAnexo_NomeArq ;
      private bool[] T00312_n1008RegrasContagemAnexo_NomeArq ;
      private String[] T00312_A1009RegrasContagemAnexo_TipoArq ;
      private bool[] T00312_n1009RegrasContagemAnexo_TipoArq ;
      private DateTime[] T00312_A1010RegrasContagemAnexo_Data ;
      private bool[] T00312_n1010RegrasContagemAnexo_Data ;
      private String[] T00312_A1011RegrasContagemAnexo_Descricao ;
      private bool[] T00312_n1011RegrasContagemAnexo_Descricao ;
      private int[] T00312_A645TipoDocumento_Codigo ;
      private bool[] T00312_n645TipoDocumento_Codigo ;
      private String[] T00312_A860RegrasContagem_Regra ;
      private String[] T00312_A1007RegrasContagemAnexo_Arquivo ;
      private bool[] T00312_n1007RegrasContagemAnexo_Arquivo ;
      private int[] T003110_A1005RegrasContagemAnexo_Codigo ;
      private int[] T003114_A1005RegrasContagemAnexo_Codigo ;
      private GXWebForm Form ;
   }

   public class regrascontagemanexos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00314 ;
          prmT00314 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00315 ;
          prmT00315 = new Object[] {
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmT00316 ;
          prmT00316 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00317 ;
          prmT00317 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00313 ;
          prmT00313 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00318 ;
          prmT00318 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00319 ;
          prmT00319 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00312 ;
          prmT00312 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003110 ;
          prmT003110 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@RegrasContagemAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@RegrasContagemAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@RegrasContagemAnexo_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@RegrasContagemAnexo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmT003111 ;
          prmT003111 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@RegrasContagemAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@RegrasContagemAnexo_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@RegrasContagemAnexo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0} ,
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003112 ;
          prmT003112 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003113 ;
          prmT003113 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003114 ;
          prmT003114 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T00312", "SELECT [RegrasContagemAnexo_Codigo], [RegrasContagemAnexo_NomeArq], [RegrasContagemAnexo_TipoArq], [RegrasContagemAnexo_Data], [RegrasContagemAnexo_Descricao], [TipoDocumento_Codigo], [RegrasContagem_Regra], [RegrasContagemAnexo_Arquivo] FROM [RegrasContagemAnexos] WITH (UPDLOCK) WHERE [RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00312,1,0,true,false )
             ,new CursorDef("T00313", "SELECT [RegrasContagemAnexo_Codigo], [RegrasContagemAnexo_NomeArq], [RegrasContagemAnexo_TipoArq], [RegrasContagemAnexo_Data], [RegrasContagemAnexo_Descricao], [TipoDocumento_Codigo], [RegrasContagem_Regra], [RegrasContagemAnexo_Arquivo] FROM [RegrasContagemAnexos] WITH (NOLOCK) WHERE [RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00313,1,0,true,false )
             ,new CursorDef("T00314", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00314,1,0,true,false )
             ,new CursorDef("T00315", "SELECT [RegrasContagem_Regra] FROM [RegrasContagem] WITH (NOLOCK) WHERE [RegrasContagem_Regra] = @RegrasContagem_Regra ",true, GxErrorMask.GX_NOMASK, false, this,prmT00315,1,0,true,false )
             ,new CursorDef("T00316", "SELECT TM1.[RegrasContagemAnexo_Codigo], TM1.[RegrasContagemAnexo_NomeArq], TM1.[RegrasContagemAnexo_TipoArq], TM1.[RegrasContagemAnexo_Data], TM1.[RegrasContagemAnexo_Descricao], T2.[TipoDocumento_Nome], TM1.[TipoDocumento_Codigo], TM1.[RegrasContagem_Regra], TM1.[RegrasContagemAnexo_Arquivo] FROM ([RegrasContagemAnexos] TM1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = TM1.[TipoDocumento_Codigo]) WHERE TM1.[RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo ORDER BY TM1.[RegrasContagemAnexo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00316,100,0,true,false )
             ,new CursorDef("T00317", "SELECT [RegrasContagemAnexo_Codigo] FROM [RegrasContagemAnexos] WITH (NOLOCK) WHERE [RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00317,1,0,true,false )
             ,new CursorDef("T00318", "SELECT TOP 1 [RegrasContagemAnexo_Codigo] FROM [RegrasContagemAnexos] WITH (NOLOCK) WHERE ( [RegrasContagemAnexo_Codigo] > @RegrasContagemAnexo_Codigo) ORDER BY [RegrasContagemAnexo_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00318,1,0,true,true )
             ,new CursorDef("T00319", "SELECT TOP 1 [RegrasContagemAnexo_Codigo] FROM [RegrasContagemAnexos] WITH (NOLOCK) WHERE ( [RegrasContagemAnexo_Codigo] < @RegrasContagemAnexo_Codigo) ORDER BY [RegrasContagemAnexo_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00319,1,0,true,true )
             ,new CursorDef("T003110", "INSERT INTO [RegrasContagemAnexos]([RegrasContagemAnexo_Arquivo], [RegrasContagemAnexo_NomeArq], [RegrasContagemAnexo_TipoArq], [RegrasContagemAnexo_Data], [RegrasContagemAnexo_Descricao], [TipoDocumento_Codigo], [RegrasContagem_Regra]) VALUES(@RegrasContagemAnexo_Arquivo, @RegrasContagemAnexo_NomeArq, @RegrasContagemAnexo_TipoArq, @RegrasContagemAnexo_Data, @RegrasContagemAnexo_Descricao, @TipoDocumento_Codigo, @RegrasContagem_Regra); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003110)
             ,new CursorDef("T003111", "UPDATE [RegrasContagemAnexos] SET [RegrasContagemAnexo_NomeArq]=@RegrasContagemAnexo_NomeArq, [RegrasContagemAnexo_TipoArq]=@RegrasContagemAnexo_TipoArq, [RegrasContagemAnexo_Data]=@RegrasContagemAnexo_Data, [RegrasContagemAnexo_Descricao]=@RegrasContagemAnexo_Descricao, [TipoDocumento_Codigo]=@TipoDocumento_Codigo, [RegrasContagem_Regra]=@RegrasContagem_Regra  WHERE [RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo", GxErrorMask.GX_NOMASK,prmT003111)
             ,new CursorDef("T003112", "UPDATE [RegrasContagemAnexos] SET [RegrasContagemAnexo_Arquivo]=@RegrasContagemAnexo_Arquivo  WHERE [RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo", GxErrorMask.GX_NOMASK,prmT003112)
             ,new CursorDef("T003113", "DELETE FROM [RegrasContagemAnexos]  WHERE [RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo", GxErrorMask.GX_NOMASK,prmT003113)
             ,new CursorDef("T003114", "SELECT [RegrasContagemAnexo_Codigo] FROM [RegrasContagemAnexos] WITH (NOLOCK) ORDER BY [RegrasContagemAnexo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003114,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[12])[0] = rslt.getBLOBFile(8, "tmp", "") ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[12])[0] = rslt.getBLOBFile(8, "tmp", "") ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, "tmp", "") ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[11]);
                }
                stmt.SetParameter(7, (String)parms[12]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                stmt.SetParameter(6, (String)parms[10]);
                stmt.SetParameter(7, (int)parms[11]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
