/*
               File: WP_RE
        Description: Registro de Esfor�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 22:6:2.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_re : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_re( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_re( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           String aP1_OS ,
                           ref String aP2_Nome ,
                           ref String aP3_TipodePrazo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV43OS = aP1_OS;
         this.AV42Nome = aP2_Nome;
         this.AV68TipodePrazo = aP3_TipodePrazo;
         executePrivate();
         aP2_Nome=this.AV42Nome;
         aP3_TipodePrazo=this.AV68TipodePrazo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContagemresultado_contadorfmcod = new GXCombobox();
         chkavContestacao = new GXCheckbox();
         cmbavProximaetapa = new GXCombobox();
         dynavContratoorigem_codigo = new GXCombobox();
         cmbavServicoaexecutar = new GXCombobox();
         dynavContador_codigo = new GXCombobox();
         chkavCriarproposta = new GXCheckbox();
         chkavIncluiproposta = new GXCheckbox();
         chkavCombinada = new GXCheckbox();
         chkavPfvisible = new GXCheckbox();
         cmbavStatusantdmn = new GXCombobox();
         chkavTemvnccmp = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTADORFMCOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV9WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTADORFMCODJV2( AV9WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATOORIGEM_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATOORIGEM_CODIGOJV2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTADOR_CODIGO") == 0 )
            {
               AV45Prestadora = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Prestadora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Prestadora), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTADOR_CODIGOJV2( AV45Prestadora) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV43OS = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43OS", AV43OS);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vOS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV43OS, "@!"))));
                  AV42Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Nome", AV42Nome);
                  AV68TipodePrazo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TipodePrazo", AV68TipodePrazo);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAJV2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               Gx_time = context.localUtil.Time( );
               context.Gx_err = 0;
               dynavContratoorigem_codigo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoorigem_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratoorigem_codigo.Enabled), 5, 0)));
               edtavValorpf_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValorpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValorpf_Enabled), 5, 0)));
               edtavValor_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValor_Enabled), 5, 0)));
               GXVvCONTRATOORIGEM_CODIGO_htmlJV2( ) ;
               WSJV2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEJV2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020326226495");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_re.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV43OS)) + "," + UrlEncode(StringUtil.RTrim(AV42Nome)) + "," + UrlEncode(StringUtil.RTrim(AV68TipodePrazo))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_boolean_hidden_field( context, "vPROPOSTARQRPRZ", AV76PropostaRqrPrz);
         GxWebStd.gx_boolean_hidden_field( context, "vPROPOSTARQRESF", AV75PropostaRqrEsf);
         GxWebStd.gx_hidden_field( context, "vTPVNC", StringUtil.RTrim( AV88TpVnc));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV51Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV51Codigos);
         }
         GxWebStd.gx_hidden_field( context, "vRESPONSAVEL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54Responsavel_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFATURAMENTO", AV85Faturamento);
         GxWebStd.gx_hidden_field( context, "vTIPOFABRICA", StringUtil.RTrim( AV26TipoFabrica));
         GxWebStd.gx_hidden_field( context, "vPROPOSTA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66Proposta_Codigo), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTIME", StringUtil.RTrim( Gx_time));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_TIPOFABRICA", StringUtil.RTrim( A516Contratada_TipoFabrica));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_PFBFS", StringUtil.LTrim( StringUtil.NToC( AV14ContagemResultado_PFBFS, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPFBFS", StringUtil.LTrim( StringUtil.NToC( AV94PFBFS, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPFLFS", StringUtil.LTrim( StringUtil.NToC( AV95PFLFS, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vQTDUNTCNS", StringUtil.LTrim( StringUtil.NToC( AV50QtdUntCns, 9, 4, ",", "")));
         GxWebStd.gx_hidden_field( context, "vDIVERGENCIA", StringUtil.LTrim( StringUtil.NToC( AV21Divergencia, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_PFBFM", StringUtil.LTrim( StringUtil.NToC( AV13ContagemResultado_PFBFM, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_PFLFM", StringUtil.LTrim( StringUtil.NToC( AV22ContagemResultado_PFLFM, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_PFLFS", StringUtil.LTrim( StringUtil.NToC( AV23ContagemResultado_PFLFS, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSTATUSCNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25StatusCnt), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSTATUSDMN", StringUtil.RTrim( AV18StatusDmn));
         GxWebStd.gx_hidden_field( context, "vPRAZOENTREGA", context.localUtil.DToC( AV40PrazoEntrega, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vNIVELCONTAGEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81NivelContagem), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTIPODIAS", StringUtil.RTrim( AV86TipoDias));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROPOSTA", AV65Proposta);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROPOSTA", AV65Proposta);
         }
         GxWebStd.gx_hidden_field( context, "vDIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV84Dias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_DEMANDACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A892LogResponsavel_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1797LogResponsavel_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "LOGRESPONSAVEL_OWNEREHCONTRATANTE", A1149LogResponsavel_OwnerEhContratante);
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A896LogResponsavel_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vDATAENTREGA", context.localUtil.DToC( AV59DataEntrega, 0, "/"));
         GxWebStd.gx_boolean_hidden_field( context, "vPROPOSTARQRCNT", AV74PropostaRqrCnt);
         GxWebStd.gx_hidden_field( context, "vTIPODEPRAZO", StringUtil.RTrim( AV68TipodePrazo));
         GxWebStd.gx_hidden_field( context, "vNOME", StringUtil.RTrim( AV42Nome));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICOSS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADATIPOFAB", StringUtil.RTrim( A1326ContagemResultado_ContratadaTipoFab));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A684ContagemResultado_PFBFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFLFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A685ContagemResultado_PFLFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFMULTIMA", StringUtil.LTrim( StringUtil.NToC( A682ContagemResultado_PFBFMUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFLFMULTIMA", StringUtil.LTrim( StringUtil.NToC( A683ContagemResultado_PFLFMUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCALCULODIVERGENCIA", StringUtil.RTrim( AV34CalculoDivergencia));
         GxWebStd.gx_hidden_field( context, "vINDICEDIVERGENCIA", StringUtil.LTrim( StringUtil.NToC( AV35IndiceDivergencia, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OSVINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTSRVTPVNC", StringUtil.RTrim( A1593ContagemResultado_CntSrvTpVnc));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_NOVOSTATUS", StringUtil.RTrim( A1234LogResponsavel_NovoStatus));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_VLRUNIDADECONTRATADA", StringUtil.LTrim( StringUtil.NToC( A557Servico_VlrUnidadeContratada, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_VALORUNIDADECONTRATACAO", StringUtil.LTrim( StringUtil.NToC( A116Contrato_ValorUnidadeContratacao, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vAFATURAR", StringUtil.LTrim( StringUtil.NToC( AV83Afaturar, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vOS", AV43OS);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV9WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV9WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOORIGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV53ContratoOrigem_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOORIGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV53ContratoOrigem_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOORIGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV53ContratoOrigem_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vOS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV43OS, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vOS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV43OS, "@!"))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_RE";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV53ContratoOrigem_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_re:[SendSecurityCheck value for]"+"ContratoOrigem_Codigo:"+context.localUtil.Format( (decimal)(AV53ContratoOrigem_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormJV2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Wccontagemresultadoevidencias == null ) )
         {
            WebComp_Wccontagemresultadoevidencias.componentjscripts();
         }
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_RE" ;
      }

      public override String GetPgmdesc( )
      {
         return "Registro de Esfor�o" ;
      }

      protected void WBJV0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_JV2( true) ;
         }
         else
         {
            wb_table1_2_JV2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_JV2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavCriarproposta_Internalname, StringUtil.BoolToStr( AV57CriarProposta), "", "", chkavCriarproposta.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(132, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,132);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavIncluiproposta_Internalname, StringUtil.BoolToStr( AV62IncluiProposta), "", "", chkavIncluiproposta.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(133, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavCombinada_Internalname, StringUtil.BoolToStr( AV55Combinada), "", "", chkavCombinada.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(134, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavPfvisible_Internalname, StringUtil.BoolToStr( AV87PFVisible), "", "", chkavPfvisible.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(135, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavStatusantdmn, cmbavStatusantdmn_Internalname, StringUtil.RTrim( AV89StatusAntDmn), 1, cmbavStatusantdmn_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavStatusantdmn.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", "", true, "HLP_WP_RE.htm");
            cmbavStatusantdmn.CurrentValue = StringUtil.RTrim( AV89StatusAntDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusantdmn_Internalname, "Values", (String)(cmbavStatusantdmn.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPrestadora_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45Prestadora), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45Prestadora), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPrestadora_Jsonclick, 0, "Attribute", "", "", "", edtavPrestadora_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_RE.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOsvinculada_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24OSVinculada), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV24OSVinculada), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,138);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOsvinculada_Jsonclick, 0, "Attribute", "", "", "", edtavOsvinculada_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_RE.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDemanda_Internalname, AV37Demanda, StringUtil.RTrim( context.localUtil.Format( AV37Demanda, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDemanda_Jsonclick, 0, "Attribute", "", "", "", edtavDemanda_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_RE.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20Servico), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV20Servico), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,140);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_Jsonclick, 0, "Attribute", "", "", "", edtavServico_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_RE.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39AreaTrabalho), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39AreaTrabalho), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_Jsonclick, 0, "Attribute", "", "", "", edtavAreatrabalho_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_RE.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratadaorigem_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV93ContratadaOrigem_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV93ContratadaOrigem_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,142);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadaorigem_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContratadaorigem_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_RE.htm");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavTemvnccmp_Internalname, StringUtil.BoolToStr( AV96TemVncCmp), "", "", chkavTemvnccmp.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(143, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,143);\"");
         }
         wbLoad = true;
      }

      protected void STARTJV2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Registro de Esfor�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPJV0( ) ;
      }

      protected void WSJV2( )
      {
         STARTJV2( ) ;
         EVTJV2( ) ;
      }

      protected void EVTJV2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11JV2 */
                           E11JV2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E12JV2 */
                                 E12JV2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13JV2 */
                           E13JV2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOACEITA'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14JV2 */
                           E14JV2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DORECUSA'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15JV2 */
                           E15JV2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16JV2 */
                           E16JV2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VSERVICOAEXECUTAR.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17JV2 */
                           E17JV2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18JV2 */
                           E18JV2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 4);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                     if ( nCmpId == 112 )
                     {
                        OldWccontagemresultadoevidencias = cgiGet( "W0112");
                        if ( ( StringUtil.Len( OldWccontagemresultadoevidencias) == 0 ) || ( StringUtil.StrCmp(OldWccontagemresultadoevidencias, WebComp_Wccontagemresultadoevidencias_Component) != 0 ) )
                        {
                           WebComp_Wccontagemresultadoevidencias = getWebComponent(GetType(), "GeneXus.Programs", OldWccontagemresultadoevidencias, new Object[] {context} );
                           WebComp_Wccontagemresultadoevidencias.ComponentInit();
                           WebComp_Wccontagemresultadoevidencias.Name = "OldWccontagemresultadoevidencias";
                           WebComp_Wccontagemresultadoevidencias_Component = OldWccontagemresultadoevidencias;
                        }
                        if ( StringUtil.Len( WebComp_Wccontagemresultadoevidencias_Component) != 0 )
                        {
                           WebComp_Wccontagemresultadoevidencias.componentprocess("W0112", "", sEvt);
                        }
                        WebComp_Wccontagemresultadoevidencias_Component = OldWccontagemresultadoevidencias;
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEJV2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormJV2( ) ;
            }
         }
      }

      protected void PAJV2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContagemresultado_contadorfmcod.Name = "vCONTAGEMRESULTADO_CONTADORFMCOD";
            dynavContagemresultado_contadorfmcod.WebTags = "";
            chkavContestacao.Name = "vCONTESTACAO";
            chkavContestacao.WebTags = "";
            chkavContestacao.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContestacao_Internalname, "TitleCaption", chkavContestacao.Caption);
            chkavContestacao.CheckedValue = "false";
            cmbavProximaetapa.Name = "vPROXIMAETAPA";
            cmbavProximaetapa.WebTags = "";
            cmbavProximaetapa.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavProximaetapa.ItemCount > 0 )
            {
               AV82ProximaEtapa = (int)(NumberUtil.Val( cmbavProximaetapa.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV82ProximaEtapa), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ProximaEtapa", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82ProximaEtapa), 6, 0)));
            }
            dynavContratoorigem_codigo.Name = "vCONTRATOORIGEM_CODIGO";
            dynavContratoorigem_codigo.WebTags = "";
            cmbavServicoaexecutar.Name = "vSERVICOAEXECUTAR";
            cmbavServicoaexecutar.WebTags = "";
            cmbavServicoaexecutar.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavServicoaexecutar.ItemCount > 0 )
            {
               AV67ServicoAExecutar = (int)(NumberUtil.Val( cmbavServicoaexecutar.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV67ServicoAExecutar), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ServicoAExecutar", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ServicoAExecutar), 6, 0)));
            }
            dynavContador_codigo.Name = "vCONTADOR_CODIGO";
            dynavContador_codigo.WebTags = "";
            chkavCriarproposta.Name = "vCRIARPROPOSTA";
            chkavCriarproposta.WebTags = "";
            chkavCriarproposta.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCriarproposta_Internalname, "TitleCaption", chkavCriarproposta.Caption);
            chkavCriarproposta.CheckedValue = "false";
            chkavIncluiproposta.Name = "vINCLUIPROPOSTA";
            chkavIncluiproposta.WebTags = "";
            chkavIncluiproposta.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIncluiproposta_Internalname, "TitleCaption", chkavIncluiproposta.Caption);
            chkavIncluiproposta.CheckedValue = "false";
            chkavCombinada.Name = "vCOMBINADA";
            chkavCombinada.WebTags = "";
            chkavCombinada.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCombinada_Internalname, "TitleCaption", chkavCombinada.Caption);
            chkavCombinada.CheckedValue = "false";
            chkavPfvisible.Name = "vPFVISIBLE";
            chkavPfvisible.WebTags = "";
            chkavPfvisible.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavPfvisible_Internalname, "TitleCaption", chkavPfvisible.Caption);
            chkavPfvisible.CheckedValue = "false";
            cmbavStatusantdmn.Name = "vSTATUSANTDMN";
            cmbavStatusantdmn.WebTags = "";
            cmbavStatusantdmn.addItem("B", "Stand by", 0);
            cmbavStatusantdmn.addItem("S", "Solicitada", 0);
            cmbavStatusantdmn.addItem("E", "Em An�lise", 0);
            cmbavStatusantdmn.addItem("A", "Em execu��o", 0);
            cmbavStatusantdmn.addItem("R", "Resolvida", 0);
            cmbavStatusantdmn.addItem("C", "Conferida", 0);
            cmbavStatusantdmn.addItem("D", "Rejeitada", 0);
            cmbavStatusantdmn.addItem("H", "Homologada", 0);
            cmbavStatusantdmn.addItem("O", "Aceite", 0);
            cmbavStatusantdmn.addItem("P", "A Pagar", 0);
            cmbavStatusantdmn.addItem("L", "Liquidada", 0);
            cmbavStatusantdmn.addItem("X", "Cancelada", 0);
            cmbavStatusantdmn.addItem("N", "N�o Faturada", 0);
            cmbavStatusantdmn.addItem("J", "Planejamento", 0);
            cmbavStatusantdmn.addItem("I", "An�lise Planejamento", 0);
            cmbavStatusantdmn.addItem("T", "Validacao T�cnica", 0);
            cmbavStatusantdmn.addItem("Q", "Validacao Qualidade", 0);
            cmbavStatusantdmn.addItem("G", "Em Homologa��o", 0);
            cmbavStatusantdmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbavStatusantdmn.addItem("U", "Rascunho", 0);
            if ( cmbavStatusantdmn.ItemCount > 0 )
            {
               AV89StatusAntDmn = cmbavStatusantdmn.getValidValue(AV89StatusAntDmn);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89StatusAntDmn", AV89StatusAntDmn);
            }
            chkavTemvnccmp.Name = "vTEMVNCCMP";
            chkavTemvnccmp.WebTags = "";
            chkavTemvnccmp.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavTemvnccmp_Internalname, "TitleCaption", chkavTemvnccmp.Caption);
            chkavTemvnccmp.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavContagemresultado_contadorfmcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvCONTADOR_CODIGO_htmlJV2( AV45Prestadora) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFMCODJV2( wwpbaseobjects.SdtWWPContext AV9WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTADORFMCOD_dataJV2( AV9WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlJV2( wwpbaseobjects.SdtWWPContext AV9WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTADORFMCOD_dataJV2( AV9WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contadorfmcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contadorfmcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contadorfmcod.ItemCount > 0 )
         {
            AV5ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFMCOD_dataJV2( wwpbaseobjects.SdtWWPContext AV9WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00JV2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Userehadministradorgam, AV9WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00JV2_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00JV2_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTRATOORIGEM_CODIGOJV2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATOORIGEM_CODIGO_dataJV2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATOORIGEM_CODIGO_htmlJV2( )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATOORIGEM_CODIGO_dataJV2( ) ;
         gxdynajaxindex = 1;
         dynavContratoorigem_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratoorigem_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratoorigem_codigo.ItemCount > 0 )
         {
            AV53ContratoOrigem_Codigo = (int)(NumberUtil.Val( dynavContratoorigem_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV53ContratoOrigem_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContratoOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53ContratoOrigem_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOORIGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV53ContratoOrigem_Codigo), "ZZZZZ9")));
         }
      }

      protected void GXDLVvCONTRATOORIGEM_CODIGO_dataJV2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00JV3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00JV3_A74Contrato_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00JV3_A77Contrato_Numero[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvCONTADOR_CODIGOJV2( int AV45Prestadora )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTADOR_CODIGO_dataJV2( AV45Prestadora) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTADOR_CODIGO_htmlJV2( int AV45Prestadora )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTADOR_CODIGO_dataJV2( AV45Prestadora) ;
         gxdynajaxindex = 1;
         dynavContador_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContador_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContador_codigo.ItemCount > 0 )
         {
            AV72Contador_Codigo = (int)(NumberUtil.Val( dynavContador_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV72Contador_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72Contador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72Contador_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTADOR_CODIGO_dataJV2( int AV45Prestadora )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00JV4 */
         pr_default.execute(2, new Object[] {AV45Prestadora});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00JV4_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00JV4_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContagemresultado_contadorfmcod.ItemCount > 0 )
         {
            AV5ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( dynavContagemresultado_contadorfmcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0)));
         }
         if ( cmbavProximaetapa.ItemCount > 0 )
         {
            AV82ProximaEtapa = (int)(NumberUtil.Val( cmbavProximaetapa.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV82ProximaEtapa), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ProximaEtapa", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82ProximaEtapa), 6, 0)));
         }
         if ( dynavContratoorigem_codigo.ItemCount > 0 )
         {
            AV53ContratoOrigem_Codigo = (int)(NumberUtil.Val( dynavContratoorigem_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV53ContratoOrigem_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContratoOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53ContratoOrigem_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOORIGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV53ContratoOrigem_Codigo), "ZZZZZ9")));
         }
         if ( cmbavServicoaexecutar.ItemCount > 0 )
         {
            AV67ServicoAExecutar = (int)(NumberUtil.Val( cmbavServicoaexecutar.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV67ServicoAExecutar), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ServicoAExecutar", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ServicoAExecutar), 6, 0)));
         }
         if ( dynavContador_codigo.ItemCount > 0 )
         {
            AV72Contador_Codigo = (int)(NumberUtil.Val( dynavContador_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV72Contador_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72Contador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72Contador_Codigo), 6, 0)));
         }
         if ( cmbavStatusantdmn.ItemCount > 0 )
         {
            AV89StatusAntDmn = cmbavStatusantdmn.getValidValue(AV89StatusAntDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89StatusAntDmn", AV89StatusAntDmn);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJV2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_time = context.localUtil.Time( );
         context.Gx_err = 0;
         dynavContratoorigem_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoorigem_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratoorigem_codigo.Enabled), 5, 0)));
         edtavValorpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValorpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValorpf_Enabled), 5, 0)));
         edtavValor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValor_Enabled), 5, 0)));
      }

      protected void RFJV2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E16JV2 */
         E16JV2 ();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Wccontagemresultadoevidencias_Component) != 0 )
               {
                  WebComp_Wccontagemresultadoevidencias.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00JV5 */
            pr_default.execute(3, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A805ContagemResultado_ContratadaOrigemCod = H00JV5_A805ContagemResultado_ContratadaOrigemCod[0];
               n805ContagemResultado_ContratadaOrigemCod = H00JV5_n805ContagemResultado_ContratadaOrigemCod[0];
               A516Contratada_TipoFabrica = H00JV5_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = H00JV5_n516Contratada_TipoFabrica[0];
               A516Contratada_TipoFabrica = H00JV5_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = H00JV5_n516Contratada_TipoFabrica[0];
               GXVvCONTRATOORIGEM_CODIGO_htmlJV2( ) ;
               GXVvCONTADOR_CODIGO_htmlJV2( AV45Prestadora) ;
               GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlJV2( AV9WWPContext) ;
               /* Execute user event: E18JV2 */
               E18JV2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            WBJV0( ) ;
         }
      }

      protected void STRUPJV0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_time = context.localUtil.Time( );
         context.Gx_err = 0;
         dynavContratoorigem_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoorigem_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratoorigem_codigo.Enabled), 5, 0)));
         edtavValorpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValorpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValorpf_Enabled), 5, 0)));
         edtavValor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValor_Enabled), 5, 0)));
         GXVvCONTRATOORIGEM_CODIGO_htmlJV2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11JV2 */
         E11JV2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlJV2( AV9WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavContagemresultado_contadorfmcod.CurrentValue = cgiGet( dynavContagemresultado_contadorfmcod_Internalname);
            AV5ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contadorfmcod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0)));
            AV97Contestacao = StringUtil.StrToBool( cgiGet( chkavContestacao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97Contestacao", AV97Contestacao);
            AV8ContagemResultado_ParecerTcn = cgiGet( edtavContagemresultado_parecertcn_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_ParecerTcn", AV8ContagemResultado_ParecerTcn);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfb_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfb_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFB");
               GX_FocusControl = edtavContagemresultado_pfb_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV6ContagemResultado_PFB = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_PFB", StringUtil.LTrim( StringUtil.Str( AV6ContagemResultado_PFB, 14, 5)));
            }
            else
            {
               AV6ContagemResultado_PFB = context.localUtil.CToN( cgiGet( edtavContagemresultado_pfb_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_PFB", StringUtil.LTrim( StringUtil.Str( AV6ContagemResultado_PFB, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfl_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfl_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFL");
               GX_FocusControl = edtavContagemresultado_pfl_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7ContagemResultado_PFL = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_PFL", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_PFL, 14, 5)));
            }
            else
            {
               AV7ContagemResultado_PFL = context.localUtil.CToN( cgiGet( edtavContagemresultado_pfl_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_PFL", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_PFL, 14, 5)));
            }
            cmbavProximaetapa.CurrentValue = cgiGet( cmbavProximaetapa_Internalname);
            AV82ProximaEtapa = (int)(NumberUtil.Val( cgiGet( cmbavProximaetapa_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ProximaEtapa", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82ProximaEtapa), 6, 0)));
            dynavContratoorigem_codigo.CurrentValue = cgiGet( dynavContratoorigem_codigo_Internalname);
            AV53ContratoOrigem_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratoorigem_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContratoOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53ContratoOrigem_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOORIGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV53ContratoOrigem_Codigo), "ZZZZZ9")));
            cmbavServicoaexecutar.CurrentValue = cgiGet( cmbavServicoaexecutar_Internalname);
            AV67ServicoAExecutar = (int)(NumberUtil.Val( cgiGet( cmbavServicoaexecutar_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ServicoAExecutar", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ServicoAExecutar), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavValorpf_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavValorpf_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vVALORPF");
               GX_FocusControl = edtavValorpf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70ValorPF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ValorPF", StringUtil.LTrim( StringUtil.Str( AV70ValorPF, 18, 5)));
            }
            else
            {
               AV70ValorPF = context.localUtil.CToN( cgiGet( edtavValorpf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ValorPF", StringUtil.LTrim( StringUtil.Str( AV70ValorPF, 18, 5)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavPrazo_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Prazo"}), 1, "vPRAZO");
               GX_FocusControl = edtavPrazo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64Prazo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64Prazo", context.localUtil.Format(AV64Prazo, "99/99/99"));
            }
            else
            {
               AV64Prazo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPrazo_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64Prazo", context.localUtil.Format(AV64Prazo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavEsforco_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavEsforco_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vESFORCO");
               GX_FocusControl = edtavEsforco_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60Esforco = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Esforco), 4, 0)));
            }
            else
            {
               AV60Esforco = (short)(context.localUtil.CToN( cgiGet( edtavEsforco_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Esforco), 4, 0)));
            }
            dynavContador_codigo.CurrentValue = cgiGet( dynavContador_codigo_Internalname);
            AV72Contador_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContador_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72Contador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72Contador_Codigo), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavBruto_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavBruto_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vBRUTO");
               GX_FocusControl = edtavBruto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV78Bruto = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Bruto", StringUtil.LTrim( StringUtil.Str( AV78Bruto, 6, 2)));
            }
            else
            {
               AV78Bruto = context.localUtil.CToN( cgiGet( edtavBruto_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Bruto", StringUtil.LTrim( StringUtil.Str( AV78Bruto, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLiquido_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLiquido_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLIQUIDO");
               GX_FocusControl = edtavLiquido_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV79Liquido = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79Liquido", StringUtil.LTrim( StringUtil.Str( AV79Liquido, 6, 2)));
            }
            else
            {
               AV79Liquido = context.localUtil.CToN( cgiGet( edtavLiquido_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79Liquido", StringUtil.LTrim( StringUtil.Str( AV79Liquido, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavValor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavValor_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vVALOR");
               GX_FocusControl = edtavValor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69Valor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Valor", StringUtil.LTrim( StringUtil.Str( AV69Valor, 18, 5)));
            }
            else
            {
               AV69Valor = context.localUtil.CToN( cgiGet( edtavValor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Valor", StringUtil.LTrim( StringUtil.Str( AV69Valor, 18, 5)));
            }
            AV57CriarProposta = StringUtil.StrToBool( cgiGet( chkavCriarproposta_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57CriarProposta", AV57CriarProposta);
            AV62IncluiProposta = StringUtil.StrToBool( cgiGet( chkavIncluiproposta_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62IncluiProposta", AV62IncluiProposta);
            AV55Combinada = StringUtil.StrToBool( cgiGet( chkavCombinada_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Combinada", AV55Combinada);
            AV87PFVisible = StringUtil.StrToBool( cgiGet( chkavPfvisible_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87PFVisible", AV87PFVisible);
            cmbavStatusantdmn.CurrentValue = cgiGet( cmbavStatusantdmn_Internalname);
            AV89StatusAntDmn = cgiGet( cmbavStatusantdmn_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89StatusAntDmn", AV89StatusAntDmn);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPrestadora_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPrestadora_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPRESTADORA");
               GX_FocusControl = edtavPrestadora_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45Prestadora = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Prestadora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Prestadora), 6, 0)));
            }
            else
            {
               AV45Prestadora = (int)(context.localUtil.CToN( cgiGet( edtavPrestadora_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Prestadora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Prestadora), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOsvinculada_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOsvinculada_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vOSVINCULADA");
               GX_FocusControl = edtavOsvinculada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24OSVinculada = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24OSVinculada), 6, 0)));
            }
            else
            {
               AV24OSVinculada = (int)(context.localUtil.CToN( cgiGet( edtavOsvinculada_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24OSVinculada), 6, 0)));
            }
            AV37Demanda = StringUtil.Upper( cgiGet( edtavDemanda_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Demanda", AV37Demanda);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavServico_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavServico_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSERVICO");
               GX_FocusControl = edtavServico_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20Servico = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Servico), 6, 0)));
            }
            else
            {
               AV20Servico = (int)(context.localUtil.CToN( cgiGet( edtavServico_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Servico), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAREATRABALHO");
               GX_FocusControl = edtavAreatrabalho_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39AreaTrabalho = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39AreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39AreaTrabalho), 6, 0)));
            }
            else
            {
               AV39AreaTrabalho = (int)(context.localUtil.CToN( cgiGet( edtavAreatrabalho_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39AreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39AreaTrabalho), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratadaorigem_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratadaorigem_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADAORIGEM_CODIGO");
               GX_FocusControl = edtavContratadaorigem_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV93ContratadaOrigem_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV93ContratadaOrigem_Codigo), 6, 0)));
            }
            else
            {
               AV93ContratadaOrigem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContratadaorigem_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV93ContratadaOrigem_Codigo), 6, 0)));
            }
            AV96TemVncCmp = StringUtil.StrToBool( cgiGet( chkavTemvnccmp_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TemVncCmp", AV96TemVncCmp);
            /* Read saved values. */
            AV85Faturamento = cgiGet( "vFATURAMENTO");
            AV83Afaturar = context.localUtil.CToN( cgiGet( "vAFATURAR"), ",", ".");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_RE";
            AV53ContratoOrigem_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratoorigem_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContratoOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53ContratoOrigem_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOORIGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV53ContratoOrigem_Codigo), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV53ContratoOrigem_Codigo), "ZZZZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_re:[SecurityCheckFailed value for]"+"ContratoOrigem_Codigo:"+context.localUtil.Format( (decimal)(AV53ContratoOrigem_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            GXVvCONTRATOORIGEM_CODIGO_htmlJV2( ) ;
            GXVvCONTAGEMRESULTADO_CONTADORFMCOD_htmlJV2( AV9WWPContext) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11JV2 */
         E11JV2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11JV2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.2 - Data: 26/03/2020 00:01", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         chkavCriarproposta.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCriarproposta_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavCriarproposta.Visible), 5, 0)));
         chkavIncluiproposta.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIncluiproposta_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavIncluiproposta.Visible), 5, 0)));
         chkavCombinada.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCombinada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavCombinada.Visible), 5, 0)));
         chkavPfvisible.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavPfvisible_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavPfvisible.Visible), 5, 0)));
         cmbavStatusantdmn.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusantdmn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatusantdmn.Visible), 5, 0)));
         edtavPrestadora_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrestadora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrestadora_Visible), 5, 0)));
         edtavOsvinculada_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOsvinculada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOsvinculada_Visible), 5, 0)));
         edtavDemanda_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDemanda_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDemanda_Visible), 5, 0)));
         edtavServico_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_Visible), 5, 0)));
         edtavAreatrabalho_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_Visible), 5, 0)));
         edtavContratadaorigem_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadaorigem_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadaorigem_codigo_Visible), 5, 0)));
         chkavTemvnccmp.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavTemvnccmp_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavTemvnccmp.Visible), 5, 0)));
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wccontagemresultadoevidencias_Component), StringUtil.Lower( "WC_ContagemResultadoEvidencias")) != 0 )
         {
            WebComp_Wccontagemresultadoevidencias = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
            WebComp_Wccontagemresultadoevidencias.ComponentInit();
            WebComp_Wccontagemresultadoevidencias.Name = "WC_ContagemResultadoEvidencias";
            WebComp_Wccontagemresultadoevidencias_Component = "WC_ContagemResultadoEvidencias";
         }
         if ( StringUtil.Len( WebComp_Wccontagemresultadoevidencias_Component) != 0 )
         {
            WebComp_Wccontagemresultadoevidencias.setjustcreated();
            WebComp_Wccontagemresultadoevidencias.componentprepare(new Object[] {(String)"W0112",(String)""});
            WebComp_Wccontagemresultadoevidencias.componentbind(new Object[] {});
         }
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         AV5ContagemResultado_ContadorFMCod = AV9WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0)));
         dynavContagemresultado_contadorfmcod.Enabled = (AV9WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfmcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contadorfmcod.Enabled), 5, 0)));
         Form.Caption = "Registro de Esfor�o - OS "+StringUtil.Trim( AV43OS);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         AV49Websession.Remove("ArquivosEvd");
         AV81NivelContagem = 2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81NivelContagem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81NivelContagem), 4, 0)));
         /* Execute user subroutine: 'DADOSDAOS' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'VISUALIZAREQUERIDO' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E12JV2 */
         E12JV2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12JV2( )
      {
         /* Enter Routine */
         if ( (0==AV5ContagemResultado_ContadorFMCod) && AV87PFVisible )
         {
            GX_msglist.addItem("Selecione um usu�rio!");
         }
         else if ( AV87PFVisible && AV57CriarProposta && AV75PropostaRqrEsf && (Convert.ToDecimal(0)==AV6ContagemResultado_PFB) )
         {
            GX_msglist.addItem("Informe o valor Bruto!");
            GX_FocusControl = edtavContagemresultado_pfb_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( AV87PFVisible && AV57CriarProposta && AV75PropostaRqrEsf && (Convert.ToDecimal(0)==AV7ContagemResultado_PFL) )
         {
            GX_msglist.addItem("Informe o valor Liquido!");
            GX_FocusControl = edtavContagemresultado_pfl_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( ( cmbavProximaetapa.ItemCount > 2 ) && (0==AV82ProximaEtapa) )
         {
            GX_msglist.addItem("Informe a pr�xima etapa a ser executada!");
            GX_FocusControl = cmbavProximaetapa_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( AV57CriarProposta && String.IsNullOrEmpty(StringUtil.RTrim( AV8ContagemResultado_ParecerTcn)) )
         {
            GX_msglist.addItem("Informe o descri��o da proposta!");
            GX_FocusControl = edtavContagemresultado_parecertcn_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( AV57CriarProposta && (0==AV67ServicoAExecutar) )
         {
            GX_msglist.addItem("Informe o servi�o proposto!");
            GX_FocusControl = cmbavServicoaexecutar_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( AV57CriarProposta && AV76PropostaRqrPrz && ( (DateTime.MinValue==AV64Prazo) || ( AV64Prazo < DateTimeUtil.ServerDate( context, "DEFAULT") ) ) )
         {
            GX_msglist.addItem("Prazo incorreto!");
            GX_FocusControl = edtavPrazo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( AV57CriarProposta && AV75PropostaRqrEsf && (0==AV60Esforco) )
         {
            GX_msglist.addItem("Informe o esfor�o!");
            GX_FocusControl = edtavEsforco_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( AV57CriarProposta && (Convert.ToDecimal(0)==AV69Valor) )
         {
            GX_msglist.addItem("Preencha o valor da unidade contratada no servi�o contratado ou no contrato!");
         }
         else if ( AV57CriarProposta && ( AV79Liquido > new prc_limiteproposta(context).executeUdp( ref  AV67ServicoAExecutar) ) )
         {
            GX_msglist.addItem("O limite autorizado para a proposta foi excedido!");
         }
         else if ( new prc_faltamanexarartefatos(context).executeUdp( ref  A456ContagemResultado_Codigo) )
         {
            GX_msglist.addItem("Faltam anexar artefatos desta OS. Confira no bot�o anexar os artefatos requeridos!");
         }
         else
         {
            if ( ( AV82ProximaEtapa > 0 ) && ( AV82ProximaEtapa < 900000 ) )
            {
               AV49Websession.Set("ProximaEtapa", StringUtil.Trim( StringUtil.Str( (decimal)(AV82ProximaEtapa), 6, 0)));
            }
            AV25StatusCnt = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25StatusCnt), 2, 0)));
            if ( AV62IncluiProposta || AV57CriarProposta )
            {
               AV18StatusDmn = "I";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18StatusDmn", AV18StatusDmn);
               /* Execute user subroutine: 'VALORTOTAL' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV57CriarProposta )
               {
                  /* Execute user subroutine: 'CRIARPROPOSTA' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               else
               {
                  /* Execute user subroutine: 'ALTERARPROPOSTA' */
                  S152 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
            }
            else
            {
               AV18StatusDmn = "R";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18StatusDmn", AV18StatusDmn);
               AV21Divergencia = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Divergencia", StringUtil.LTrim( StringUtil.Str( AV21Divergencia, 6, 2)));
               /* Execute user subroutine: 'EXISTEDIVERGENCIA' */
               S162 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
            if ( StringUtil.StrCmp(AV89StatusAntDmn, "D") == 0 )
            {
               new prc_encerrarcicloexecucao(context ).execute(  A456ContagemResultado_Codigo,  DateTimeUtil.ServerNow( context, "DEFAULT")) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            }
            else
            {
               new prc_setfimexc(context ).execute( ref  A456ContagemResultado_Codigo) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            }
            if ( AV87PFVisible && ! ( AV62IncluiProposta || AV57CriarProposta || AV55Combinada ) )
            {
               /* Execute user subroutine: 'NOVACONTAGEM' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV25StatusCnt == 7 )
               {
                  new prc_setiniciocrr(context ).execute( ref  A456ContagemResultado_Codigo) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  new prc_nadivergenciaficacom(context ).execute(  A456ContagemResultado_Codigo,  AV24OSVinculada,  AV37Demanda,  AV93ContratadaOrigem_Codigo,  AV20Servico,  AV39AreaTrabalho,  AV9WWPContext.gxTpr_Userid,  AV8ContagemResultado_ParecerTcn) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24OSVinculada), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Demanda", AV37Demanda);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV93ContratadaOrigem_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Servico), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39AreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39AreaTrabalho), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_ParecerTcn", AV8ContagemResultado_ParecerTcn);
               }
               else
               {
                  if ( ( StringUtil.StrCmp(AV89StatusAntDmn, "A") == 0 ) && AV9WWPContext.gxTpr_Userehcontratada )
                  {
                     /* Execute user subroutine: 'STATUS' */
                     S182 ();
                     if ( returnInSub )
                     {
                        returnInSub = true;
                        if (true) return;
                     }
                  }
               }
            }
            if ( AV97Contestacao )
            {
               new prc_setnaocnfcontestada(context ).execute( ref  A456ContagemResultado_Codigo) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            }
            if ( AV25StatusCnt != 7 )
            {
               new prc_realterastatusdadmn(context ).execute(  A456ContagemResultado_Codigo,  AV18StatusDmn,  AV54Responsavel_Codigo) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18StatusDmn", AV18StatusDmn);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Responsavel_Codigo), 6, 0)));
            }
            if ( new prc_criaranexoscompartilhados(context).executeUdp(  A456ContagemResultado_Codigo) )
            {
               AV51Codigos.Add(A456ContagemResultado_Codigo, 0);
               if ( ( AV24OSVinculada > 0 ) && ( ( StringUtil.StrCmp(AV88TpVnc, "C") == 0 ) || ( StringUtil.StrCmp(AV88TpVnc, "A") == 0 ) ) )
               {
                  AV51Codigos.Add(AV24OSVinculada, 0);
               }
               AV49Websession.Set("Codigos", AV51Codigos.ToXml(false, true, "Collection", ""));
               new prc_vincularanexoscompartilhadoscomos(context ).execute( ) ;
               AV49Websession.Remove("Codigos");
            }
            AV42Nome = gxdomainstatusdemanda.getDescription(context,AV18StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Nome", AV42Nome);
            /* Execute user subroutine: 'CLOSEREFRESH' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51Codigos", AV51Codigos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV65Proposta", AV65Proposta);
         dynavContagemresultado_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfmcod_Internalname, "Values", dynavContagemresultado_contadorfmcod.ToJavascriptSource());
      }

      protected void E13JV2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {(String)AV42Nome,(String)AV68TipodePrazo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E14JV2( )
      {
         /* 'DoAceita' Routine */
         /* Execute user subroutine: 'STATUSANTERIOR' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         GXt_dtime1 = DateTimeUtil.ResetTime( AV59DataEntrega ) ;
         new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  AV54Responsavel_Codigo,  "V",  "D",  AV9WWPContext.gxTpr_Userid,  0,  AV89StatusAntDmn,  "R",  AV8ContagemResultado_ParecerTcn,  GXt_dtime1,  true) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Responsavel_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89StatusAntDmn", AV89StatusAntDmn);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_ParecerTcn", AV8ContagemResultado_ParecerTcn);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59DataEntrega", context.localUtil.Format(AV59DataEntrega, "99/99/99"));
         new prc_realterastatusdadmn(context ).execute(  A456ContagemResultado_Codigo,  "R",  AV9WWPContext.gxTpr_Userid) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         /* Execute user subroutine: 'CLOSEREFRESH' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavStatusantdmn.CurrentValue = StringUtil.RTrim( AV89StatusAntDmn);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusantdmn_Internalname, "Values", cmbavStatusantdmn.ToJavascriptSource());
      }

      protected void E15JV2( )
      {
         /* 'DoRecusa' Routine */
         /* Execute user subroutine: 'STATUSANTERIOR' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV54Responsavel_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Responsavel_Codigo), 6, 0)));
         /* Using cursor H00JV6 */
         pr_default.execute(4, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A892LogResponsavel_DemandaCod = H00JV6_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = H00JV6_n892LogResponsavel_DemandaCod[0];
            A896LogResponsavel_Owner = H00JV6_A896LogResponsavel_Owner[0];
            A490ContagemResultado_ContratadaCod = H00JV6_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00JV6_n490ContagemResultado_ContratadaCod[0];
            A1797LogResponsavel_Codigo = H00JV6_A1797LogResponsavel_Codigo[0];
            A490ContagemResultado_ContratadaCod = H00JV6_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00JV6_n490ContagemResultado_ContratadaCod[0];
            /* Using cursor H00JV7 */
            pr_default.execute(5, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, A896LogResponsavel_Owner});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A66ContratadaUsuario_ContratadaCod = H00JV7_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = H00JV7_A69ContratadaUsuario_UsuarioCod[0];
               AV54Responsavel_Codigo = A896LogResponsavel_Owner;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Responsavel_Codigo), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(5);
            if ( AV54Responsavel_Codigo > 0 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( (0==AV54Responsavel_Codigo) )
         {
            GX_msglist.addItem("Erro, usu�rio da Contratada n�o achado no hist�rico!");
         }
         else
         {
            AV90ContagemResultado.Load(A456ContagemResultado_Codigo);
            AV90ContagemResultado.gxTpr_Contagemresultado_responsavel = AV54Responsavel_Codigo;
            AV90ContagemResultado.Save();
            GXt_dtime1 = DateTimeUtil.ResetTime( AV59DataEntrega ) ;
            new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  AV54Responsavel_Codigo,  "R",  "D",  AV9WWPContext.gxTpr_Userid,  0,  AV89StatusAntDmn,  "D",  AV8ContagemResultado_ParecerTcn,  GXt_dtime1,  true) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Responsavel_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89StatusAntDmn", AV89StatusAntDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_ParecerTcn", AV8ContagemResultado_ParecerTcn);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59DataEntrega", context.localUtil.Format(AV59DataEntrega, "99/99/99"));
            new prc_realterastatusdadmn(context ).execute(  A456ContagemResultado_Codigo,  "D",  AV9WWPContext.gxTpr_Userid) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            /* Execute user subroutine: 'CLOSEREFRESH' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         cmbavStatusantdmn.CurrentValue = StringUtil.RTrim( AV89StatusAntDmn);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatusantdmn_Internalname, "Values", cmbavStatusantdmn.ToJavascriptSource());
      }

      protected void E16JV2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9WWPContext", AV9WWPContext);
      }

      protected void E17JV2( )
      {
         /* Servicoaexecutar_Click Routine */
         if ( (0==AV67ServicoAExecutar) )
         {
            AV69Valor = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Valor", StringUtil.LTrim( StringUtil.Str( AV69Valor, 18, 5)));
         }
         else
         {
            /* Execute user subroutine: 'CARREGAVALORPF' */
            S222 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: 'VALORTOTAL' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S212( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         bttBtnaceita_Visible = ((AV9WWPContext.gxTpr_Insert) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnaceita_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnaceita_Visible), 5, 0)));
         bttBtnenter_Visible = ((AV9WWPContext.gxTpr_Insert) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
      }

      protected void S162( )
      {
         /* 'EXISTEDIVERGENCIA' Routine */
         /* Using cursor H00JV8 */
         pr_default.execute(6, new Object[] {AV9WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A39Contratada_Codigo = H00JV8_A39Contratada_Codigo[0];
            A516Contratada_TipoFabrica = H00JV8_A516Contratada_TipoFabrica[0];
            n516Contratada_TipoFabrica = H00JV8_n516Contratada_TipoFabrica[0];
            if ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "M") == 0 )
            {
               AV13ContagemResultado_PFBFM = AV6ContagemResultado_PFB;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV13ContagemResultado_PFBFM, 14, 5)));
               AV22ContagemResultado_PFLFM = AV7ContagemResultado_PFL;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV22ContagemResultado_PFLFM, 14, 5)));
            }
            else if ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "S") == 0 )
            {
               AV14ContagemResultado_PFBFS = AV6ContagemResultado_PFB;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFBFS, 14, 5)));
               AV23ContagemResultado_PFLFS = AV7ContagemResultado_PFL;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV23ContagemResultado_PFLFS, 14, 5)));
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(6);
         if ( (0==AV24OSVinculada) )
         {
            if ( (Convert.ToDecimal(0)==AV14ContagemResultado_PFBFS) )
            {
               AV14ContagemResultado_PFBFS = AV94PFBFS;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFBFS, 14, 5)));
               AV23ContagemResultado_PFLFS = AV95PFLFS;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV23ContagemResultado_PFLFS, 14, 5)));
            }
         }
         else
         {
            /* Execute user subroutine: 'DADOSOSVNC' */
            S232 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         if ( (Convert.ToDecimal(0)==AV50QtdUntCns) )
         {
            if ( ( StringUtil.StrCmp(AV88TpVnc, "C") == 0 ) || ( StringUtil.StrCmp(AV88TpVnc, "A") == 0 ) )
            {
               /* Execute user subroutine: 'CALCULADIVERGENCIA' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
            else
            {
               /* Execute user subroutine: 'VINCULADASDECOMPARACAO' */
               S252 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
         }
      }

      protected void S232( )
      {
         /* 'DADOSOSVNC' Routine */
         /* Using cursor H00JV10 */
         pr_default.execute(7, new Object[] {AV24OSVinculada, AV45Prestadora});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A1636ContagemResultado_ServicoSS = H00JV10_A1636ContagemResultado_ServicoSS[0];
            n1636ContagemResultado_ServicoSS = H00JV10_n1636ContagemResultado_ServicoSS[0];
            A484ContagemResultado_StatusDmn = H00JV10_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00JV10_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = H00JV10_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00JV10_n490ContagemResultado_ContratadaCod[0];
            A1326ContagemResultado_ContratadaTipoFab = H00JV10_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00JV10_n1326ContagemResultado_ContratadaTipoFab[0];
            A684ContagemResultado_PFBFSUltima = H00JV10_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00JV10_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = H00JV10_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = H00JV10_A683ContagemResultado_PFLFMUltima[0];
            A1326ContagemResultado_ContratadaTipoFab = H00JV10_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00JV10_n1326ContagemResultado_ContratadaTipoFab[0];
            A684ContagemResultado_PFBFSUltima = H00JV10_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00JV10_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = H00JV10_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = H00JV10_A683ContagemResultado_PFLFMUltima[0];
            if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S") == 0 )
            {
               if ( (Convert.ToDecimal(0)==AV14ContagemResultado_PFBFS) )
               {
                  AV14ContagemResultado_PFBFS = A684ContagemResultado_PFBFSUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFBFS, 14, 5)));
                  AV23ContagemResultado_PFLFS = A685ContagemResultado_PFLFSUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV23ContagemResultado_PFLFS, 14, 5)));
               }
            }
            else if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 )
            {
               if ( (Convert.ToDecimal(0)==AV13ContagemResultado_PFBFM) )
               {
                  AV13ContagemResultado_PFBFM = A682ContagemResultado_PFBFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV13ContagemResultado_PFBFM, 14, 5)));
                  AV22ContagemResultado_PFLFM = A683ContagemResultado_PFLFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV22ContagemResultado_PFLFM, 14, 5)));
               }
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(7);
      }

      protected void S252( )
      {
         /* 'VINCULADASDECOMPARACAO' Routine */
         AV96TemVncCmp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TemVncCmp", AV96TemVncCmp);
         /* Using cursor H00JV12 */
         pr_default.execute(8, new Object[] {A456ContagemResultado_Codigo, AV45Prestadora});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00JV12_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00JV12_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = H00JV12_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00JV12_n602ContagemResultado_OSVinculada[0];
            A1636ContagemResultado_ServicoSS = H00JV12_A1636ContagemResultado_ServicoSS[0];
            n1636ContagemResultado_ServicoSS = H00JV12_n1636ContagemResultado_ServicoSS[0];
            A484ContagemResultado_StatusDmn = H00JV12_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00JV12_n484ContagemResultado_StatusDmn[0];
            A1593ContagemResultado_CntSrvTpVnc = H00JV12_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = H00JV12_n1593ContagemResultado_CntSrvTpVnc[0];
            A490ContagemResultado_ContratadaCod = H00JV12_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00JV12_n490ContagemResultado_ContratadaCod[0];
            A1326ContagemResultado_ContratadaTipoFab = H00JV12_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00JV12_n1326ContagemResultado_ContratadaTipoFab[0];
            A684ContagemResultado_PFBFSUltima = H00JV12_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00JV12_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = H00JV12_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = H00JV12_A683ContagemResultado_PFLFMUltima[0];
            A1593ContagemResultado_CntSrvTpVnc = H00JV12_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = H00JV12_n1593ContagemResultado_CntSrvTpVnc[0];
            A1326ContagemResultado_ContratadaTipoFab = H00JV12_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00JV12_n1326ContagemResultado_ContratadaTipoFab[0];
            A684ContagemResultado_PFBFSUltima = H00JV12_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00JV12_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = H00JV12_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = H00JV12_A683ContagemResultado_PFLFMUltima[0];
            if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S") == 0 )
            {
               if ( (Convert.ToDecimal(0)==AV14ContagemResultado_PFBFS) )
               {
                  AV14ContagemResultado_PFBFS = A684ContagemResultado_PFBFSUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFBFS, 14, 5)));
                  AV23ContagemResultado_PFLFS = A685ContagemResultado_PFLFSUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV23ContagemResultado_PFLFS, 14, 5)));
               }
            }
            else if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 )
            {
               if ( (Convert.ToDecimal(0)==AV13ContagemResultado_PFBFM) )
               {
                  AV13ContagemResultado_PFBFM = A682ContagemResultado_PFBFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV13ContagemResultado_PFBFM, 14, 5)));
                  AV22ContagemResultado_PFLFM = A683ContagemResultado_PFLFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV22ContagemResultado_PFLFM, 14, 5)));
               }
            }
            AV96TemVncCmp = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96TemVncCmp", AV96TemVncCmp);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(8);
         }
         pr_default.close(8);
         if ( AV96TemVncCmp )
         {
            /* Execute user subroutine: 'CALCULADIVERGENCIA' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S242( )
      {
         /* 'CALCULADIVERGENCIA' Routine */
         GXt_decimal2 = AV21Divergencia;
         new prc_calculardivergencia(context ).execute(  AV34CalculoDivergencia,  AV14ContagemResultado_PFBFS,  AV13ContagemResultado_PFBFM,  AV23ContagemResultado_PFLFS,  AV22ContagemResultado_PFLFM, out  GXt_decimal2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34CalculoDivergencia", AV34CalculoDivergencia);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFBFS, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV13ContagemResultado_PFBFM, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV23ContagemResultado_PFLFS, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV22ContagemResultado_PFLFM, 14, 5)));
         AV21Divergencia = GXt_decimal2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Divergencia", StringUtil.LTrim( StringUtil.Str( AV21Divergencia, 6, 2)));
         if ( AV21Divergencia > AV35IndiceDivergencia )
         {
            if ( (0==AV24OSVinculada) || ! ( AV96TemVncCmp || ( ( StringUtil.StrCmp(AV88TpVnc, "C") == 0 ) || ( StringUtil.StrCmp(AV88TpVnc, "A") == 0 ) ) ) )
            {
               AV18StatusDmn = "A";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18StatusDmn", AV18StatusDmn);
            }
            else
            {
               AV18StatusDmn = "B";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18StatusDmn", AV18StatusDmn);
            }
            AV25StatusCnt = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25StatusCnt), 2, 0)));
         }
         else
         {
            AV18StatusDmn = "R";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18StatusDmn", AV18StatusDmn);
            AV25StatusCnt = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25StatusCnt), 2, 0)));
         }
      }

      protected void S152( )
      {
         /* 'ALTERARPROPOSTA' Routine */
         AV65Proposta.Load(AV66Proposta_Codigo);
         AV61HoraCnt = StringUtil.Substring( Gx_time, 1, 5);
         new prc_reupdcnt(context ).execute( ref  A456ContagemResultado_Codigo,  AV6ContagemResultado_PFB,  AV7ContagemResultado_PFL,  AV5ContagemResultado_ContadorFMCod,  AV61HoraCnt) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_PFB", StringUtil.LTrim( StringUtil.Str( AV6ContagemResultado_PFB, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_PFL", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_PFL, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0)));
         /* Execute user subroutine: 'GRAVARPROPOSTA' */
         S262 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'CRIARPROPOSTA' Routine */
         AV65Proposta = new SdtProposta(context);
         if ( StringUtil.StrCmp(AV26TipoFabrica, "S") == 0 )
         {
            AV14ContagemResultado_PFBFS = AV6ContagemResultado_PFB;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFBFS, 14, 5)));
            AV23ContagemResultado_PFLFS = AV7ContagemResultado_PFL;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV23ContagemResultado_PFLFS, 14, 5)));
         }
         else
         {
            AV13ContagemResultado_PFBFM = AV6ContagemResultado_PFB;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV13ContagemResultado_PFBFM, 14, 5)));
            AV22ContagemResultado_PFLFM = AV7ContagemResultado_PFL;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV22ContagemResultado_PFLFM, 14, 5)));
         }
         /* Execute user subroutine: 'GRAVARPROPOSTA' */
         S262 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S262( )
      {
         /* 'GRAVARPROPOSTA' Routine */
         if ( ! AV76PropostaRqrPrz )
         {
            GXt_int3 = AV84Dias;
            new prc_diasparaentrega(context ).execute( ref  AV67ServicoAExecutar,  ((StringUtil.StrCmp(AV85Faturamento, "B")==0) ? AV78Bruto : AV79Liquido),  0, out  GXt_int3) ;
            AV84Dias = GXt_int3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84Dias), 4, 0)));
            GXt_dtime1 = DateTimeUtil.ResetTime( AV64Prazo ) ;
            GXt_dtime4 = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
            new prc_adddiasuteis(context ).execute(  GXt_dtime4,  AV84Dias,  AV86TipoDias, out  GXt_dtime1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84Dias), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TipoDias", AV86TipoDias);
            AV64Prazo = DateTimeUtil.ResetTime(GXt_dtime1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64Prazo", context.localUtil.Format(AV64Prazo, "99/99/99"));
         }
         AV65Proposta.gxTpr_Proposta_oscodigo = A456ContagemResultado_Codigo;
         AV65Proposta.gxTpr_Proposta_cntsrvcod = AV67ServicoAExecutar;
         AV65Proposta.gxTpr_Proposta_esforco = (int)(((0==AV60Esforco) ? AV78Bruto : AV60Esforco));
         AV65Proposta.gxTpr_Proposta_liquido = (int)(AV79Liquido);
         AV65Proposta.gxTpr_Proposta_valor = AV69Valor;
         AV65Proposta.gxTpr_Proposta_prazo = AV64Prazo;
         AV65Proposta.gxTpr_Proposta_prazodias = AV84Dias;
         AV65Proposta.gxTpr_Proposta_objetivo = AV8ContagemResultado_ParecerTcn;
         AV65Proposta.gxTpr_Proposta_contratadacod = AV9WWPContext.gxTpr_Contratada_codigo;
         AV65Proposta.gxTpr_Areatrabalho_codigo = AV9WWPContext.gxTpr_Areatrabalho_codigo;
         AV65Proposta.gxTpr_Proposta_valorundcnt = AV70ValorPF;
         AV65Proposta.gxTpr_Proposta_ativo = true;
         AV65Proposta.Save();
         if ( AV65Proposta.Success() )
         {
            /* Using cursor H00JV13 */
            pr_default.execute(9, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A1797LogResponsavel_Codigo = H00JV13_A1797LogResponsavel_Codigo[0];
               A896LogResponsavel_Owner = H00JV13_A896LogResponsavel_Owner[0];
               A892LogResponsavel_DemandaCod = H00JV13_A892LogResponsavel_DemandaCod[0];
               n892LogResponsavel_DemandaCod = H00JV13_n892LogResponsavel_DemandaCod[0];
               GXt_boolean5 = A1149LogResponsavel_OwnerEhContratante;
               new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean5) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
               A1149LogResponsavel_OwnerEhContratante = GXt_boolean5;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
               if ( A1149LogResponsavel_OwnerEhContratante )
               {
                  AV54Responsavel_Codigo = A896LogResponsavel_Owner;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Responsavel_Codigo), 6, 0)));
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(9);
            }
            pr_default.close(9);
            new prc_alterastatusdmn(context ).execute( ref  AV24OSVinculada,  "I",  0) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24OSVinculada), 6, 0)));
            new prc_alterastatusdmn(context ).execute( ref  A456ContagemResultado_Codigo,  "I",  AV54Responsavel_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Responsavel_Codigo), 6, 0)));
            GXt_dtime4 = DateTimeUtil.ResetTime( AV59DataEntrega ) ;
            new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  AV54Responsavel_Codigo,  "E",  "D",  AV9WWPContext.gxTpr_Userid,  0,  AV89StatusAntDmn,  "I",  AV8ContagemResultado_ParecerTcn,  GXt_dtime4,  true) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Responsavel_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89StatusAntDmn", AV89StatusAntDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_ParecerTcn", AV8ContagemResultado_ParecerTcn);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59DataEntrega", context.localUtil.Format(AV59DataEntrega, "99/99/99"));
            if ( AV74PropostaRqrCnt )
            {
               AV14ContagemResultado_PFBFS = AV78Bruto;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFBFS, 14, 5)));
               AV23ContagemResultado_PFLFS = AV79Liquido;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV23ContagemResultado_PFLFS, 14, 5)));
               AV5ContagemResultado_ContadorFMCod = AV72Contador_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0)));
               AV21Divergencia = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Divergencia", StringUtil.LTrim( StringUtil.Str( AV21Divergencia, 6, 2)));
               AV8ContagemResultado_ParecerTcn = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_ParecerTcn", AV8ContagemResultado_ParecerTcn);
               AV20Servico = AV67ServicoAExecutar;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Servico), 6, 0)));
               /* Execute user subroutine: 'NOVACONTAGEM' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
            context.CommitDataStores( "WP_RE");
         }
         else
         {
            context.RollbackDataStores( "WP_RE");
            context.setWebReturnParms(new Object[] {(String)AV42Nome,(String)AV68TipodePrazo});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'DADOSDAOS' Routine */
         AV110GXLvl451 = 0;
         /* Using cursor H00JV18 */
         pr_default.execute(10, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(10) != 101) )
         {
            A1686Proposta_OSCodigo = H00JV18_A1686Proposta_OSCodigo[0];
            A484ContagemResultado_StatusDmn = H00JV18_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00JV18_n484ContagemResultado_StatusDmn[0];
            A1685Proposta_Codigo = H00JV18_A1685Proposta_Codigo[0];
            A1702Proposta_Prazo = H00JV18_A1702Proposta_Prazo[0];
            A1688Proposta_Valor = H00JV18_A1688Proposta_Valor[0];
            A1703Proposta_Esforco = H00JV18_A1703Proposta_Esforco[0];
            A1715Proposta_OSCntCod = H00JV18_A1715Proposta_OSCntCod[0];
            n1715Proposta_OSCntCod = H00JV18_n1715Proposta_OSCntCod[0];
            A1705Proposta_OSServico = H00JV18_A1705Proposta_OSServico[0];
            n1705Proposta_OSServico = H00JV18_n1705Proposta_OSServico[0];
            A1704Proposta_CntSrvCod = H00JV18_A1704Proposta_CntSrvCod[0];
            A1764Proposta_OSDmn = H00JV18_A1764Proposta_OSDmn[0];
            n1764Proposta_OSDmn = H00JV18_n1764Proposta_OSDmn[0];
            A1690Proposta_Objetivo = H00JV18_A1690Proposta_Objetivo[0];
            A490ContagemResultado_ContratadaCod = H00JV18_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00JV18_n490ContagemResultado_ContratadaCod[0];
            A472ContagemResultado_DataEntrega = H00JV18_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = H00JV18_n472ContagemResultado_DataEntrega[0];
            A602ContagemResultado_OSVinculada = H00JV18_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00JV18_n602ContagemResultado_OSVinculada[0];
            A805ContagemResultado_ContratadaOrigemCod = H00JV18_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = H00JV18_n805ContagemResultado_ContratadaOrigemCod[0];
            A1763Proposta_OSCntSrvFtrm = H00JV18_A1763Proposta_OSCntSrvFtrm[0];
            n1763Proposta_OSCntSrvFtrm = H00JV18_n1763Proposta_OSCntSrvFtrm[0];
            A1707Proposta_OSPrzTpDias = H00JV18_A1707Proposta_OSPrzTpDias[0];
            n1707Proposta_OSPrzTpDias = H00JV18_n1707Proposta_OSPrzTpDias[0];
            A1717Proposta_OSPFB = H00JV18_A1717Proposta_OSPFB[0];
            n1717Proposta_OSPFB = H00JV18_n1717Proposta_OSPFB[0];
            A1718Proposta_OSPFL = H00JV18_A1718Proposta_OSPFL[0];
            n1718Proposta_OSPFL = H00JV18_n1718Proposta_OSPFL[0];
            A1719Proposta_OSDataCnt = H00JV18_A1719Proposta_OSDataCnt[0];
            n1719Proposta_OSDataCnt = H00JV18_n1719Proposta_OSDataCnt[0];
            A1720Proposta_OSHoraCnt = H00JV18_A1720Proposta_OSHoraCnt[0];
            n1720Proposta_OSHoraCnt = H00JV18_n1720Proposta_OSHoraCnt[0];
            A484ContagemResultado_StatusDmn = H00JV18_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00JV18_n484ContagemResultado_StatusDmn[0];
            A1705Proposta_OSServico = H00JV18_A1705Proposta_OSServico[0];
            n1705Proposta_OSServico = H00JV18_n1705Proposta_OSServico[0];
            A1764Proposta_OSDmn = H00JV18_A1764Proposta_OSDmn[0];
            n1764Proposta_OSDmn = H00JV18_n1764Proposta_OSDmn[0];
            A490ContagemResultado_ContratadaCod = H00JV18_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00JV18_n490ContagemResultado_ContratadaCod[0];
            A472ContagemResultado_DataEntrega = H00JV18_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = H00JV18_n472ContagemResultado_DataEntrega[0];
            A602ContagemResultado_OSVinculada = H00JV18_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00JV18_n602ContagemResultado_OSVinculada[0];
            A805ContagemResultado_ContratadaOrigemCod = H00JV18_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = H00JV18_n805ContagemResultado_ContratadaOrigemCod[0];
            A1715Proposta_OSCntCod = H00JV18_A1715Proposta_OSCntCod[0];
            n1715Proposta_OSCntCod = H00JV18_n1715Proposta_OSCntCod[0];
            A1763Proposta_OSCntSrvFtrm = H00JV18_A1763Proposta_OSCntSrvFtrm[0];
            n1763Proposta_OSCntSrvFtrm = H00JV18_n1763Proposta_OSCntSrvFtrm[0];
            A1707Proposta_OSPrzTpDias = H00JV18_A1707Proposta_OSPrzTpDias[0];
            n1707Proposta_OSPrzTpDias = H00JV18_n1707Proposta_OSPrzTpDias[0];
            A1717Proposta_OSPFB = H00JV18_A1717Proposta_OSPFB[0];
            n1717Proposta_OSPFB = H00JV18_n1717Proposta_OSPFB[0];
            A1718Proposta_OSPFL = H00JV18_A1718Proposta_OSPFL[0];
            n1718Proposta_OSPFL = H00JV18_n1718Proposta_OSPFL[0];
            A1719Proposta_OSDataCnt = H00JV18_A1719Proposta_OSDataCnt[0];
            n1719Proposta_OSDataCnt = H00JV18_n1719Proposta_OSDataCnt[0];
            A1720Proposta_OSHoraCnt = H00JV18_A1720Proposta_OSHoraCnt[0];
            n1720Proposta_OSHoraCnt = H00JV18_n1720Proposta_OSHoraCnt[0];
            AV110GXLvl451 = 1;
            AV89StatusAntDmn = A484ContagemResultado_StatusDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89StatusAntDmn", AV89StatusAntDmn);
            if ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "S") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "J") == 0 ) )
            {
               AV62IncluiProposta = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62IncluiProposta", AV62IncluiProposta);
               AV66Proposta_Codigo = A1685Proposta_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Proposta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66Proposta_Codigo), 9, 0)));
               AV64Prazo = A1702Proposta_Prazo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64Prazo", context.localUtil.Format(AV64Prazo, "99/99/99"));
               AV69Valor = A1688Proposta_Valor;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Valor", StringUtil.LTrim( StringUtil.Str( AV69Valor, 18, 5)));
               AV60Esforco = (short)(A1703Proposta_Esforco);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Esforco), 4, 0)));
               AV53ContratoOrigem_Codigo = A1715Proposta_OSCntCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContratoOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53ContratoOrigem_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOORIGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV53ContratoOrigem_Codigo), "ZZZZZ9")));
               AV71CntSrvCod = A1705Proposta_OSServico;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71CntSrvCod), 6, 0)));
               AV67ServicoAExecutar = A1704Proposta_CntSrvCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ServicoAExecutar", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ServicoAExecutar), 6, 0)));
               AV37Demanda = A1764Proposta_OSDmn;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Demanda", AV37Demanda);
               AV8ContagemResultado_ParecerTcn = A1690Proposta_Objetivo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_ParecerTcn", AV8ContagemResultado_ParecerTcn);
               AV45Prestadora = A490ContagemResultado_ContratadaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Prestadora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Prestadora), 6, 0)));
               AV59DataEntrega = A472ContagemResultado_DataEntrega;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59DataEntrega", context.localUtil.Format(AV59DataEntrega, "99/99/99"));
               AV24OSVinculada = A602ContagemResultado_OSVinculada;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24OSVinculada), 6, 0)));
               AV93ContratadaOrigem_Codigo = A805ContagemResultado_ContratadaOrigemCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV93ContratadaOrigem_Codigo), 6, 0)));
               AV6ContagemResultado_PFB = A1717Proposta_OSPFB;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_PFB", StringUtil.LTrim( StringUtil.Str( AV6ContagemResultado_PFB, 14, 5)));
               AV7ContagemResultado_PFL = A1718Proposta_OSPFL;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_PFL", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_PFL, 14, 5)));
               AV58DataCnt = A1719Proposta_OSDataCnt;
               AV61HoraCnt = A1720Proposta_OSHoraCnt;
               AV85Faturamento = A1763Proposta_OSCntSrvFtrm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85Faturamento", AV85Faturamento);
               AV86TipoDias = A1707Proposta_OSPrzTpDias;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TipoDias", AV86TipoDias);
               /* Execute user subroutine: 'CARREGASRVEXECUTAR' */
               S279 ();
               if ( returnInSub )
               {
                  pr_default.close(10);
                  returnInSub = true;
                  if (true) return;
               }
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(10);
         }
         pr_default.close(10);
         if ( AV110GXLvl451 == 0 )
         {
            /* Using cursor H00JV20 */
            pr_default.execute(11, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A468ContagemResultado_NaoCnfDmnCod = H00JV20_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = H00JV20_n468ContagemResultado_NaoCnfDmnCod[0];
               A1121NaoConformidade_EhImpeditiva = H00JV20_A1121NaoConformidade_EhImpeditiva[0];
               n1121NaoConformidade_EhImpeditiva = H00JV20_n1121NaoConformidade_EhImpeditiva[0];
               A516Contratada_TipoFabrica = H00JV20_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = H00JV20_n516Contratada_TipoFabrica[0];
               A484ContagemResultado_StatusDmn = H00JV20_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00JV20_n484ContagemResultado_StatusDmn[0];
               A1603ContagemResultado_CntCod = H00JV20_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00JV20_n1603ContagemResultado_CntCod[0];
               A457ContagemResultado_Demanda = H00JV20_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00JV20_n457ContagemResultado_Demanda[0];
               A512ContagemResultado_ValorPF = H00JV20_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = H00JV20_n512ContagemResultado_ValorPF[0];
               A472ContagemResultado_DataEntrega = H00JV20_A472ContagemResultado_DataEntrega[0];
               n472ContagemResultado_DataEntrega = H00JV20_n472ContagemResultado_DataEntrega[0];
               A1714ContagemResultado_Combinada = H00JV20_A1714ContagemResultado_Combinada[0];
               n1714ContagemResultado_Combinada = H00JV20_n1714ContagemResultado_Combinada[0];
               A1553ContagemResultado_CntSrvCod = H00JV20_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00JV20_n1553ContagemResultado_CntSrvCod[0];
               A490ContagemResultado_ContratadaCod = H00JV20_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00JV20_n490ContagemResultado_ContratadaCod[0];
               A805ContagemResultado_ContratadaOrigemCod = H00JV20_A805ContagemResultado_ContratadaOrigemCod[0];
               n805ContagemResultado_ContratadaOrigemCod = H00JV20_n805ContagemResultado_ContratadaOrigemCod[0];
               A1594ContagemResultado_CntSrvFtrm = H00JV20_A1594ContagemResultado_CntSrvFtrm[0];
               n1594ContagemResultado_CntSrvFtrm = H00JV20_n1594ContagemResultado_CntSrvFtrm[0];
               A1611ContagemResultado_PrzTpDias = H00JV20_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = H00JV20_n1611ContagemResultado_PrzTpDias[0];
               A1326ContagemResultado_ContratadaTipoFab = H00JV20_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = H00JV20_n1326ContagemResultado_ContratadaTipoFab[0];
               A1613ContagemResultado_CntSrvQtdUntCns = H00JV20_A1613ContagemResultado_CntSrvQtdUntCns[0];
               n1613ContagemResultado_CntSrvQtdUntCns = H00JV20_n1613ContagemResultado_CntSrvQtdUntCns[0];
               A798ContagemResultado_PFBFSImp = H00JV20_A798ContagemResultado_PFBFSImp[0];
               n798ContagemResultado_PFBFSImp = H00JV20_n798ContagemResultado_PFBFSImp[0];
               A799ContagemResultado_PFLFSImp = H00JV20_A799ContagemResultado_PFLFSImp[0];
               n799ContagemResultado_PFLFSImp = H00JV20_n799ContagemResultado_PFLFSImp[0];
               A601ContagemResultado_Servico = H00JV20_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00JV20_n601ContagemResultado_Servico[0];
               A602ContagemResultado_OSVinculada = H00JV20_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = H00JV20_n602ContagemResultado_OSVinculada[0];
               A52Contratada_AreaTrabalhoCod = H00JV20_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00JV20_n52Contratada_AreaTrabalhoCod[0];
               A1593ContagemResultado_CntSrvTpVnc = H00JV20_A1593ContagemResultado_CntSrvTpVnc[0];
               n1593ContagemResultado_CntSrvTpVnc = H00JV20_n1593ContagemResultado_CntSrvTpVnc[0];
               A508ContagemResultado_Owner = H00JV20_A508ContagemResultado_Owner[0];
               A1610ContagemResultado_PrzTp = H00JV20_A1610ContagemResultado_PrzTp[0];
               n1610ContagemResultado_PrzTp = H00JV20_n1610ContagemResultado_PrzTp[0];
               A566ContagemResultado_DataUltCnt = H00JV20_A566ContagemResultado_DataUltCnt[0];
               A825ContagemResultado_HoraUltCnt = H00JV20_A825ContagemResultado_HoraUltCnt[0];
               A684ContagemResultado_PFBFSUltima = H00JV20_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00JV20_A685ContagemResultado_PFLFSUltima[0];
               A682ContagemResultado_PFBFMUltima = H00JV20_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00JV20_A683ContagemResultado_PFLFMUltima[0];
               A1121NaoConformidade_EhImpeditiva = H00JV20_A1121NaoConformidade_EhImpeditiva[0];
               n1121NaoConformidade_EhImpeditiva = H00JV20_n1121NaoConformidade_EhImpeditiva[0];
               A1603ContagemResultado_CntCod = H00JV20_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00JV20_n1603ContagemResultado_CntCod[0];
               A1594ContagemResultado_CntSrvFtrm = H00JV20_A1594ContagemResultado_CntSrvFtrm[0];
               n1594ContagemResultado_CntSrvFtrm = H00JV20_n1594ContagemResultado_CntSrvFtrm[0];
               A1611ContagemResultado_PrzTpDias = H00JV20_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = H00JV20_n1611ContagemResultado_PrzTpDias[0];
               A1613ContagemResultado_CntSrvQtdUntCns = H00JV20_A1613ContagemResultado_CntSrvQtdUntCns[0];
               n1613ContagemResultado_CntSrvQtdUntCns = H00JV20_n1613ContagemResultado_CntSrvQtdUntCns[0];
               A601ContagemResultado_Servico = H00JV20_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00JV20_n601ContagemResultado_Servico[0];
               A1593ContagemResultado_CntSrvTpVnc = H00JV20_A1593ContagemResultado_CntSrvTpVnc[0];
               n1593ContagemResultado_CntSrvTpVnc = H00JV20_n1593ContagemResultado_CntSrvTpVnc[0];
               A1610ContagemResultado_PrzTp = H00JV20_A1610ContagemResultado_PrzTp[0];
               n1610ContagemResultado_PrzTp = H00JV20_n1610ContagemResultado_PrzTp[0];
               A1326ContagemResultado_ContratadaTipoFab = H00JV20_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = H00JV20_n1326ContagemResultado_ContratadaTipoFab[0];
               A52Contratada_AreaTrabalhoCod = H00JV20_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00JV20_n52Contratada_AreaTrabalhoCod[0];
               A516Contratada_TipoFabrica = H00JV20_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = H00JV20_n516Contratada_TipoFabrica[0];
               A566ContagemResultado_DataUltCnt = H00JV20_A566ContagemResultado_DataUltCnt[0];
               A825ContagemResultado_HoraUltCnt = H00JV20_A825ContagemResultado_HoraUltCnt[0];
               A684ContagemResultado_PFBFSUltima = H00JV20_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00JV20_A685ContagemResultado_PFLFSUltima[0];
               A682ContagemResultado_PFBFMUltima = H00JV20_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00JV20_A683ContagemResultado_PFLFMUltima[0];
               AV62IncluiProposta = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62IncluiProposta", AV62IncluiProposta);
               AV57CriarProposta = (bool)(((StringUtil.StrCmp(A1610ContagemResultado_PrzTp, "O")==0))||((StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "J")==0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57CriarProposta", AV57CriarProposta);
               AV53ContratoOrigem_Codigo = A1603ContagemResultado_CntCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ContratoOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53ContratoOrigem_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOORIGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV53ContratoOrigem_Codigo), "ZZZZZ9")));
               AV37Demanda = A457ContagemResultado_Demanda;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Demanda", AV37Demanda);
               AV70ValorPF = A512ContagemResultado_ValorPF;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ValorPF", StringUtil.LTrim( StringUtil.Str( AV70ValorPF, 18, 5)));
               AV59DataEntrega = A472ContagemResultado_DataEntrega;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59DataEntrega", context.localUtil.Format(AV59DataEntrega, "99/99/99"));
               AV55Combinada = A1714ContagemResultado_Combinada;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Combinada", AV55Combinada);
               AV71CntSrvCod = A1553ContagemResultado_CntSrvCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71CntSrvCod), 6, 0)));
               AV45Prestadora = A490ContagemResultado_ContratadaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Prestadora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Prestadora), 6, 0)));
               AV93ContratadaOrigem_Codigo = A805ContagemResultado_ContratadaOrigemCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV93ContratadaOrigem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV93ContratadaOrigem_Codigo), 6, 0)));
               AV85Faturamento = A1594ContagemResultado_CntSrvFtrm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85Faturamento", AV85Faturamento);
               AV86TipoDias = A1611ContagemResultado_PrzTpDias;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TipoDias", AV86TipoDias);
               AV26TipoFabrica = A1326ContagemResultado_ContratadaTipoFab;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26TipoFabrica", AV26TipoFabrica);
               AV50QtdUntCns = A1613ContagemResultado_CntSrvQtdUntCns;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50QtdUntCns", StringUtil.LTrim( StringUtil.Str( AV50QtdUntCns, 9, 4)));
               AV89StatusAntDmn = A484ContagemResultado_StatusDmn;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89StatusAntDmn", AV89StatusAntDmn);
               if ( AV55Combinada )
               {
                  AV58DataCnt = A566ContagemResultado_DataUltCnt;
                  AV61HoraCnt = A825ContagemResultado_HoraUltCnt;
                  if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S") == 0 )
                  {
                     AV6ContagemResultado_PFB = A684ContagemResultado_PFBFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_PFB", StringUtil.LTrim( StringUtil.Str( AV6ContagemResultado_PFB, 14, 5)));
                     AV7ContagemResultado_PFL = A685ContagemResultado_PFLFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_PFL", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_PFL, 14, 5)));
                  }
                  else
                  {
                     AV6ContagemResultado_PFB = A682ContagemResultado_PFBFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_PFB", StringUtil.LTrim( StringUtil.Str( AV6ContagemResultado_PFB, 14, 5)));
                     AV7ContagemResultado_PFL = A683ContagemResultado_PFLFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_PFL", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_PFL, 14, 5)));
                  }
                  edtavContagemresultado_pfb_Enabled = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfb_Enabled), 5, 0)));
                  edtavContagemresultado_pfl_Enabled = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfl_Enabled), 5, 0)));
               }
               if ( ( A684ContagemResultado_PFBFSUltima + A685ContagemResultado_PFLFSUltima > Convert.ToDecimal( 0 )) )
               {
                  AV94PFBFS = A684ContagemResultado_PFBFSUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94PFBFS", StringUtil.LTrim( StringUtil.Str( AV94PFBFS, 14, 5)));
                  AV95PFLFS = A685ContagemResultado_PFLFSUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95PFLFS", StringUtil.LTrim( StringUtil.Str( AV95PFLFS, 14, 5)));
               }
               else
               {
                  AV94PFBFS = A798ContagemResultado_PFBFSImp;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94PFBFS", StringUtil.LTrim( StringUtil.Str( AV94PFBFS, 14, 5)));
                  AV95PFLFS = A799ContagemResultado_PFLFSImp;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95PFLFS", StringUtil.LTrim( StringUtil.Str( AV95PFLFS, 14, 5)));
               }
               AV20Servico = A601ContagemResultado_Servico;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Servico), 6, 0)));
               AV40PrazoEntrega = A472ContagemResultado_DataEntrega;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40PrazoEntrega", context.localUtil.Format(AV40PrazoEntrega, "99/99/99"));
               AV24OSVinculada = A602ContagemResultado_OSVinculada;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24OSVinculada), 6, 0)));
               AV39AreaTrabalho = A52Contratada_AreaTrabalhoCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39AreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39AreaTrabalho), 6, 0)));
               AV88TpVnc = A1593ContagemResultado_CntSrvTpVnc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TpVnc", AV88TpVnc);
               AV54Responsavel_Codigo = A508ContagemResultado_Owner;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Responsavel_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(AV89StatusAntDmn, "D") == 0 )
               {
                  if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 )
                  {
                     edtavContagemresultado_pfb_Invitemessage = StringUtil.Trim( StringUtil.Str( A682ContagemResultado_PFBFMUltima, 14, 5));
                     context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfb_Internalname, "Invitemessage", edtavContagemresultado_pfb_Invitemessage);
                     edtavContagemresultado_pfl_Invitemessage = StringUtil.Trim( StringUtil.Str( A683ContagemResultado_PFLFMUltima, 14, 5));
                     context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfl_Internalname, "Invitemessage", edtavContagemresultado_pfl_Invitemessage);
                  }
                  else
                  {
                     edtavContagemresultado_pfb_Invitemessage = StringUtil.Trim( StringUtil.Str( A684ContagemResultado_PFBFSUltima, 14, 5));
                     context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfb_Internalname, "Invitemessage", edtavContagemresultado_pfb_Invitemessage);
                     edtavContagemresultado_pfl_Invitemessage = StringUtil.Trim( StringUtil.Str( A685ContagemResultado_PFLFSUltima, 14, 5));
                     context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfl_Internalname, "Invitemessage", edtavContagemresultado_pfl_Invitemessage);
                  }
                  /* Using cursor H00JV21 */
                  pr_default.execute(12);
                  while ( (pr_default.getStatus(12) != 101) )
                  {
                     chkavContestacao.Visible = (A1121NaoConformidade_EhImpeditiva ? 1 : 0);
                     context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContestacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavContestacao.Visible), 5, 0)));
                     lblTextblockcontestacao_Visible = (A1121NaoConformidade_EhImpeditiva ? 1 : 0);
                     context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontestacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontestacao_Visible), 5, 0)));
                     pr_default.readNext(12);
                  }
                  pr_default.close(12);
               }
               else
               {
                  chkavContestacao.Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContestacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavContestacao.Visible), 5, 0)));
                  lblTextblockcontestacao_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontestacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontestacao_Visible), 5, 0)));
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(11);
            new prc_getparmindicedivergencia(context ).execute( ref  AV71CntSrvCod, out  AV35IndiceDivergencia, out  AV34CalculoDivergencia, out  AV70ValorPF) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71CntSrvCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( AV35IndiceDivergencia, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34CalculoDivergencia", AV34CalculoDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ValorPF", StringUtil.LTrim( StringUtil.Str( AV70ValorPF, 18, 5)));
         }
         tblTblsrvcombinado_Visible = (AV62IncluiProposta||AV57CriarProposta ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblsrvcombinado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblsrvcombinado_Visible), 5, 0)));
         if ( AV62IncluiProposta || AV57CriarProposta )
         {
            Form.Caption = "Proposta - OS "+StringUtil.Trim( AV37Demanda);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            lblTextblockcontagemresultado_parecertcn_Caption = "Descri��o";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontagemresultado_parecertcn_Internalname, "Caption", lblTextblockcontagemresultado_parecertcn_Caption);
            GXt_int6 = AV9WWPContext.gxTpr_Areatrabalho_codigo;
            new prc_propostarqr(context ).execute( ref  GXt_int6, out  AV77PropostaRqrSrv, out  AV76PropostaRqrPrz, out  AV75PropostaRqrEsf, out  AV74PropostaRqrCnt, out  AV73PropostaNvlCnt) ;
            AV9WWPContext.gxTpr_Areatrabalho_codigo = GXt_int6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77PropostaRqrSrv", AV77PropostaRqrSrv);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76PropostaRqrPrz", AV76PropostaRqrPrz);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75PropostaRqrEsf", AV75PropostaRqrEsf);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74PropostaRqrCnt", AV74PropostaRqrCnt);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PropostaNvlCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73PropostaNvlCnt), 4, 0)));
            /* Execute user subroutine: 'CARREGASRVEXECUTAR' */
            S279 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else
         {
            /* Execute user subroutine: 'PROXIMAETAPA' */
            S282 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         if ( StringUtil.StrCmp(AV89StatusAntDmn, "D") == 0 )
         {
            /* Using cursor H00JV22 */
            pr_default.execute(13, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(13) != 101) )
            {
               A1130LogResponsavel_Status = H00JV22_A1130LogResponsavel_Status[0];
               n1130LogResponsavel_Status = H00JV22_n1130LogResponsavel_Status[0];
               A894LogResponsavel_Acao = H00JV22_A894LogResponsavel_Acao[0];
               A1797LogResponsavel_Codigo = H00JV22_A1797LogResponsavel_Codigo[0];
               A896LogResponsavel_Owner = H00JV22_A896LogResponsavel_Owner[0];
               A892LogResponsavel_DemandaCod = H00JV22_A892LogResponsavel_DemandaCod[0];
               n892LogResponsavel_DemandaCod = H00JV22_n892LogResponsavel_DemandaCod[0];
               GXt_boolean5 = A1149LogResponsavel_OwnerEhContratante;
               new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean5) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
               A1149LogResponsavel_OwnerEhContratante = GXt_boolean5;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
               if ( A1149LogResponsavel_OwnerEhContratante )
               {
                  AV92QuemRejeitou = A896LogResponsavel_Owner;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(13);
            }
            pr_default.close(13);
         }
      }

      protected void S279( )
      {
         /* 'CARREGASRVEXECUTAR' Routine */
         /* Using cursor H00JV23 */
         pr_default.execute(14, new Object[] {AV53ContratoOrigem_Codigo});
         while ( (pr_default.getStatus(14) != 101) )
         {
            A155Servico_Codigo = H00JV23_A155Servico_Codigo[0];
            A74Contrato_Codigo = H00JV23_A74Contrato_Codigo[0];
            A160ContratoServicos_Codigo = H00JV23_A160ContratoServicos_Codigo[0];
            A605Servico_Sigla = H00JV23_A605Servico_Sigla[0];
            A605Servico_Sigla = H00JV23_A605Servico_Sigla[0];
            cmbavServicoaexecutar.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)), A605Servico_Sigla, 0);
            pr_default.readNext(14);
         }
         pr_default.close(14);
      }

      protected void S172( )
      {
         /* 'NOVACONTAGEM' Routine */
         GXt_dtime4 = DateTimeUtil.ResetTime( AV40PrazoEntrega ) ;
         new prc_newresultadocontagem(context ).execute(  A456ContagemResultado_Codigo,  DateTimeUtil.ServerDate( context, "DEFAULT"),  AV5ContagemResultado_ContadorFMCod,  AV21Divergencia,  0,  AV8ContagemResultado_ParecerTcn,  AV13ContagemResultado_PFBFM,  AV14ContagemResultado_PFBFS,  AV22ContagemResultado_PFLFM,  AV23ContagemResultado_PFLFS,  AV25StatusCnt,  0,  false,  AV9WWPContext.gxTpr_Contratada_codigo,  AV20Servico,  AV9WWPContext.gxTpr_Areatrabalho_codigo,  AV89StatusAntDmn,  AV18StatusDmn,  GXt_dtime4,  AV9WWPContext.gxTpr_Userid, ref  AV81NivelContagem) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Divergencia", StringUtil.LTrim( StringUtil.Str( AV21Divergencia, 6, 2)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_ParecerTcn", AV8ContagemResultado_ParecerTcn);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV13ContagemResultado_PFBFM, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV14ContagemResultado_PFBFS, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV22ContagemResultado_PFLFM, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV23ContagemResultado_PFLFS, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25StatusCnt), 2, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Servico), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89StatusAntDmn", AV89StatusAntDmn);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18StatusDmn", AV18StatusDmn);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40PrazoEntrega", context.localUtil.Format(AV40PrazoEntrega, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81NivelContagem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81NivelContagem), 4, 0)));
      }

      protected void S222( )
      {
         /* 'CARREGAVALORPF' Routine */
         /* Using cursor H00JV24 */
         pr_default.execute(15, new Object[] {AV67ServicoAExecutar});
         while ( (pr_default.getStatus(15) != 101) )
         {
            A74Contrato_Codigo = H00JV24_A74Contrato_Codigo[0];
            A160ContratoServicos_Codigo = H00JV24_A160ContratoServicos_Codigo[0];
            A557Servico_VlrUnidadeContratada = H00JV24_A557Servico_VlrUnidadeContratada[0];
            A116Contrato_ValorUnidadeContratacao = H00JV24_A116Contrato_ValorUnidadeContratacao[0];
            A116Contrato_ValorUnidadeContratacao = H00JV24_A116Contrato_ValorUnidadeContratacao[0];
            if ( ( A557Servico_VlrUnidadeContratada > Convert.ToDecimal( 0 )) )
            {
               AV70ValorPF = A557Servico_VlrUnidadeContratada;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ValorPF", StringUtil.LTrim( StringUtil.Str( AV70ValorPF, 18, 5)));
            }
            else
            {
               AV70ValorPF = A116Contrato_ValorUnidadeContratacao;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ValorPF", StringUtil.LTrim( StringUtil.Str( AV70ValorPF, 18, 5)));
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(15);
      }

      protected void S122( )
      {
         /* 'VISUALIZAREQUERIDO' Routine */
         if ( ( AV50QtdUntCns > Convert.ToDecimal( 0 )) )
         {
            edtavContagemresultado_pfb_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfb_Enabled), 5, 0)));
            edtavContagemresultado_pfl_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfl_Enabled), 5, 0)));
            AV6ContagemResultado_PFB = AV50QtdUntCns;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_PFB", StringUtil.LTrim( StringUtil.Str( AV6ContagemResultado_PFB, 14, 5)));
            AV7ContagemResultado_PFL = AV50QtdUntCns;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_PFL", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_PFL, 14, 5)));
         }
         cmbavProximaetapa.Visible = ((cmbavProximaetapa.ItemCount>2) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProximaetapa_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavProximaetapa.Visible), 5, 0)));
         lblTextblockproximaetapa_Visible = ((cmbavProximaetapa.ItemCount>2) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockproximaetapa_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockproximaetapa_Visible), 5, 0)));
         AV81NivelContagem = AV73PropostaNvlCnt;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81NivelContagem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81NivelContagem), 4, 0)));
         if ( ! AV77PropostaRqrSrv )
         {
            AV67ServicoAExecutar = AV71CntSrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ServicoAExecutar", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ServicoAExecutar), 6, 0)));
            cmbavServicoaexecutar.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicoaexecutar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavServicoaexecutar.Enabled), 5, 0)));
         }
         edtavPrazo_Enabled = (AV76PropostaRqrPrz ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrazo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazo_Enabled), 5, 0)));
         tblTblesforco_Visible = (AV75PropostaRqrEsf ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblesforco_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblesforco_Visible), 5, 0)));
         tblTblcnt_Visible = (AV74PropostaRqrCnt ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcnt_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcnt_Visible), 5, 0)));
         AV87PFVisible = (bool)((!AV74PropostaRqrCnt&&!AV9WWPContext.gxTpr_Userehcontratante));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87PFVisible", AV87PFVisible);
         lblTextblockcontagemresultado_pfb_Visible = (AV87PFVisible ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontagemresultado_pfb_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontagemresultado_pfb_Visible), 5, 0)));
         edtavContagemresultado_pfb_Visible = (AV87PFVisible ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfb_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfb_Visible), 5, 0)));
         lblTextblockcontagemresultado_pfl_Visible = (AV87PFVisible ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontagemresultado_pfl_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontagemresultado_pfl_Visible), 5, 0)));
         edtavContagemresultado_pfl_Visible = (AV87PFVisible ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfl_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfl_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV89StatusAntDmn, "A") == 0 )
         {
            lblTextblockcontagemresultado_contadorfmcod_Visible = (!AV9WWPContext.gxTpr_Userehcontratante ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontagemresultado_contadorfmcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontagemresultado_contadorfmcod_Visible), 5, 0)));
            dynavContagemresultado_contadorfmcod.Visible = (!AV9WWPContext.gxTpr_Userehcontratante ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfmcod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contadorfmcod.Visible), 5, 0)));
            tblActionstable_Visible = (!AV9WWPContext.gxTpr_Userehcontratante ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblActionstable_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblActionstable_Visible), 5, 0)));
            tblTblaceitarecusa_Visible = (AV9WWPContext.gxTpr_Userehcontratante ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblaceitarecusa_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblaceitarecusa_Visible), 5, 0)));
         }
         else
         {
            tblActionstable_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblActionstable_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblActionstable_Visible), 5, 0)));
            tblTblaceitarecusa_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblaceitarecusa_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblaceitarecusa_Visible), 5, 0)));
         }
         if ( AV73PropostaNvlCnt == 1 )
         {
            lblTextblockbruto_Caption = "Estimada:����Bruto";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockbruto_Internalname, "Caption", lblTextblockbruto_Caption);
         }
         else if ( AV73PropostaNvlCnt == 2 )
         {
            lblTextblockbruto_Caption = "Detalhada:����Bruto";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockbruto_Internalname, "Caption", lblTextblockbruto_Caption);
         }
         else if ( AV73PropostaNvlCnt == 0 )
         {
            lblTextblockbruto_Caption = "Bruto";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockbruto_Internalname, "Caption", lblTextblockbruto_Caption);
         }
      }

      protected void S282( )
      {
         /* 'PROXIMAETAPA' Routine */
         /* Using cursor H00JV25 */
         pr_default.execute(16, new Object[] {AV71CntSrvCod, AV89StatusAntDmn});
         while ( (pr_default.getStatus(16) != 101) )
         {
            A1589ContratoSrvVnc_SrvVncCntSrvCod = H00JV25_A1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            n1589ContratoSrvVnc_SrvVncCntSrvCod = H00JV25_n1589ContratoSrvVnc_SrvVncCntSrvCod[0];
            A923ContratoSrvVnc_ServicoVncCod = H00JV25_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = H00JV25_n923ContratoSrvVnc_ServicoVncCod[0];
            A1453ContratoServicosVnc_Ativo = H00JV25_A1453ContratoServicosVnc_Ativo[0];
            n1453ContratoServicosVnc_Ativo = H00JV25_n1453ContratoServicosVnc_Ativo[0];
            A1084ContratoSrvVnc_StatusDmn = H00JV25_A1084ContratoSrvVnc_StatusDmn[0];
            n1084ContratoSrvVnc_StatusDmn = H00JV25_n1084ContratoSrvVnc_StatusDmn[0];
            A1800ContratoSrvVnc_DoStatusDmn = H00JV25_A1800ContratoSrvVnc_DoStatusDmn[0];
            n1800ContratoSrvVnc_DoStatusDmn = H00JV25_n1800ContratoSrvVnc_DoStatusDmn[0];
            A915ContratoSrvVnc_CntSrvCod = H00JV25_A915ContratoSrvVnc_CntSrvCod[0];
            A924ContratoSrvVnc_ServicoVncSigla = H00JV25_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = H00JV25_n924ContratoSrvVnc_ServicoVncSigla[0];
            A917ContratoSrvVnc_Codigo = H00JV25_A917ContratoSrvVnc_Codigo[0];
            A923ContratoSrvVnc_ServicoVncCod = H00JV25_A923ContratoSrvVnc_ServicoVncCod[0];
            n923ContratoSrvVnc_ServicoVncCod = H00JV25_n923ContratoSrvVnc_ServicoVncCod[0];
            A924ContratoSrvVnc_ServicoVncSigla = H00JV25_A924ContratoSrvVnc_ServicoVncSigla[0];
            n924ContratoSrvVnc_ServicoVncSigla = H00JV25_n924ContratoSrvVnc_ServicoVncSigla[0];
            if ( H00JV25_n924ContratoSrvVnc_ServicoVncSigla[0] )
            {
               cmbavProximaetapa.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)), StringUtil.Upper( A1743ContratoSrvVnc_NovoStatusDmn), 0);
            }
            else
            {
               cmbavProximaetapa.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)), A924ContratoSrvVnc_ServicoVncSigla, 0);
            }
            pr_default.readNext(16);
         }
         pr_default.close(16);
         if ( cmbavProximaetapa.ItemCount > 2 )
         {
            cmbavProximaetapa.addItem("900000", "Todas", 2);
         }
      }

      protected void S132( )
      {
         /* 'VALORTOTAL' Routine */
         if ( StringUtil.StrCmp(AV85Faturamento, "B") == 0 )
         {
            AV83Afaturar = AV78Bruto;
         }
         else
         {
            AV83Afaturar = AV79Liquido;
         }
         AV69Valor = (decimal)((AV60Esforco*AV70ValorPF)+(AV83Afaturar*AV70ValorPF));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Valor", StringUtil.LTrim( StringUtil.Str( AV69Valor, 18, 5)));
      }

      protected void S192( )
      {
         /* 'CLOSEREFRESH' Routine */
         lblTbjava_Caption = "<script language=\"javascript\" type=\"text/javascript\">parent.location.replace(parent.location.href); </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void S202( )
      {
         /* 'STATUSANTERIOR' Routine */
         /* Using cursor H00JV26 */
         pr_default.execute(17, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(17) != 101) )
         {
            A892LogResponsavel_DemandaCod = H00JV26_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = H00JV26_n892LogResponsavel_DemandaCod[0];
            A1234LogResponsavel_NovoStatus = H00JV26_A1234LogResponsavel_NovoStatus[0];
            n1234LogResponsavel_NovoStatus = H00JV26_n1234LogResponsavel_NovoStatus[0];
            A1797LogResponsavel_Codigo = H00JV26_A1797LogResponsavel_Codigo[0];
            if ( ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "T") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "G") == 0 ) )
            {
               AV89StatusAntDmn = A1234LogResponsavel_NovoStatus;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89StatusAntDmn", AV89StatusAntDmn);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(17);
         }
         pr_default.close(17);
      }

      protected void S182( )
      {
         /* 'STATUS' Routine */
         if ( new prc_estevecancelada(context).executeUdp( ref  A456ContagemResultado_Codigo) )
         {
            AV18StatusDmn = "X";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18StatusDmn", AV18StatusDmn);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E18JV2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_JV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 10, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_JV2( true) ;
         }
         else
         {
            wb_table2_8_JV2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_JV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table3_123_JV2( true) ;
         }
         else
         {
            wb_table3_123_JV2( false) ;
         }
         return  ;
      }

      protected void wb_table3_123_JV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_JV2e( true) ;
         }
         else
         {
            wb_table1_2_JV2e( false) ;
         }
      }

      protected void wb_table3_123_JV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblActionstable_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(80), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblActionstable_Internalname, tblActionstable_Internalname, "", "Table", 0, "", "", 10, 20, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:bottom")+"\" class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnenter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:bottom")+"\" class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_123_JV2e( true) ;
         }
         else
         {
            wb_table3_123_JV2e( false) ;
         }
      }

      protected void wb_table2_8_JV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_JV2( true) ;
         }
         else
         {
            wb_table4_11_JV2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_JV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            wb_table5_52_JV2( true) ;
         }
         else
         {
            wb_table5_52_JV2( false) ;
         }
         return  ;
      }

      protected void wb_table5_52_JV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            wb_table6_104_JV2( true) ;
         }
         else
         {
            wb_table6_104_JV2( false) ;
         }
         return  ;
      }

      protected void wb_table6_104_JV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"6\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table7_115_JV2( true) ;
         }
         else
         {
            wb_table7_115_JV2( false) ;
         }
         return  ;
      }

      protected void wb_table7_115_JV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_JV2e( true) ;
         }
         else
         {
            wb_table2_8_JV2e( false) ;
         }
      }

      protected void wb_table7_115_JV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblaceitarecusa_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblaceitarecusa_Internalname, tblTblaceitarecusa_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:bottom")+"\" class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnaceita_Internalname, "", "Aceita", bttBtnaceita_Jsonclick, 5, "Aceita", "", StyleString, ClassString, bttBtnaceita_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOACEITA\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:bottom")+"\" class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnrecusa_Internalname, "", "Recusa", bttBtnrecusa_Jsonclick, 5, "Recusa", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DORECUSA\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_115_JV2e( true) ;
         }
         else
         {
            wb_table7_115_JV2e( false) ;
         }
      }

      protected void wb_table6_104_JV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='DataDescriptionCell'>") ;
            wb_table8_107_JV2( true) ;
         }
         else
         {
            wb_table8_107_JV2( false) ;
         }
         return  ;
      }

      protected void wb_table8_107_JV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_104_JV2e( true) ;
         }
         else
         {
            wb_table6_104_JV2e( false) ;
         }
      }

      protected void wb_table8_107_JV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedtbanexos_Internalname, tblTablemergedtbanexos_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbanexos_Internalname, "Anexos", "", "", lblTbanexos_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top")+"\">") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0112"+"", StringUtil.RTrim( WebComp_Wccontagemresultadoevidencias_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0112"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Wccontagemresultadoevidencias_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWccontagemresultadoevidencias), StringUtil.Lower( WebComp_Wccontagemresultadoevidencias_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0112"+"");
                  }
                  WebComp_Wccontagemresultadoevidencias.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWccontagemresultadoevidencias), StringUtil.Lower( WebComp_Wccontagemresultadoevidencias_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_107_JV2e( true) ;
         }
         else
         {
            wb_table8_107_JV2e( false) ;
         }
      }

      protected void wb_table5_52_JV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblsrvcombinado_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblsrvcombinado_Internalname, tblTblsrvcombinado_Internalname, "", "Table", 0, "", "", 20, 20, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoorigem_codigo_Internalname, "Contrato", "", "", lblTextblockcontratoorigem_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratoorigem_codigo, dynavContratoorigem_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV53ContratoOrigem_Codigo), 6, 0)), 1, dynavContratoorigem_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavContratoorigem_codigo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WP_RE.htm");
            dynavContratoorigem_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV53ContratoOrigem_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoorigem_codigo_Internalname, "Values", (String)(dynavContratoorigem_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicoaexecutar_Internalname, "Servi�o a executar", "", "", lblTextblockservicoaexecutar_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServicoaexecutar, cmbavServicoaexecutar_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV67ServicoAExecutar), 6, 0)), 1, cmbavServicoaexecutar_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSERVICOAEXECUTAR.CLICK."+"'", "int", "", 1, cmbavServicoaexecutar.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_WP_RE.htm");
            cmbavServicoaexecutar.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV67ServicoAExecutar), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicoaexecutar_Internalname, "Values", (String)(cmbavServicoaexecutar.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvalorpf_Internalname, "Valor contratado R$", "", "", lblTextblockvalorpf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavValorpf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV70ValorPF, 18, 5, ",", "")), ((edtavValorpf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV70ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV70ValorPF, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavValorpf_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavValorpf_Enabled, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprazo_Internalname, "Prazo", "", "", lblTextblockprazo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPrazo_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPrazo_Internalname, context.localUtil.Format(AV64Prazo, "99/99/99"), context.localUtil.Format( AV64Prazo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPrazo_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtavPrazo_Enabled, 1, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_WP_RE.htm");
            GxWebStd.gx_bitmap( context, edtavPrazo_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavPrazo_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_RE.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table9_72_JV2( true) ;
         }
         else
         {
            wb_table9_72_JV2( false) ;
         }
         return  ;
      }

      protected void wb_table9_72_JV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvalor_Internalname, "Valor total R$", "", "", lblTextblockvalor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavValor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV69Valor, 18, 5, ",", "")), ((edtavValor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV69Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV69Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavValor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavValor_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_52_JV2e( true) ;
         }
         else
         {
            wb_table5_52_JV2e( false) ;
         }
      }

      protected void wb_table9_72_JV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table10_75_JV2( true) ;
         }
         else
         {
            wb_table10_75_JV2( false) ;
         }
         return  ;
      }

      protected void wb_table10_75_JV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table11_83_JV2( true) ;
         }
         else
         {
            wb_table11_83_JV2( false) ;
         }
         return  ;
      }

      protected void wb_table11_83_JV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_72_JV2e( true) ;
         }
         else
         {
            wb_table9_72_JV2e( false) ;
         }
      }

      protected void wb_table11_83_JV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblcnt_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblcnt_Internalname, tblTblcnt_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontador_codigo_Internalname, "Contador", "", "", lblTextblockcontador_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"3\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContador_codigo, dynavContador_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV72Contador_Codigo), 6, 0)), 1, dynavContador_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"", "", true, "HLP_WP_RE.htm");
            dynavContador_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV72Contador_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContador_codigo_Internalname, "Values", (String)(dynavContador_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockbruto_Internalname, lblTextblockbruto_Caption, "", "", lblTextblockbruto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavBruto_Internalname, StringUtil.LTrim( StringUtil.NToC( AV78Bruto, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV78Bruto, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBruto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockliquido_Internalname, "Liquido", "", "", lblTextblockliquido_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLiquido_Internalname, StringUtil.LTrim( StringUtil.NToC( AV79Liquido, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV79Liquido, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLiquido_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_83_JV2e( true) ;
         }
         else
         {
            wb_table11_83_JV2e( false) ;
         }
      }

      protected void wb_table10_75_JV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblesforco_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblesforco_Internalname, tblTblesforco_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockesforco_Internalname, "Esfor�o", "", "", lblTextblockesforco_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEsforco_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60Esforco), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV60Esforco), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEsforco_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_75_JV2e( true) ;
         }
         else
         {
            wb_table10_75_JV2e( false) ;
         }
      }

      protected void wb_table4_11_JV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_contadorfmcod_Internalname, "Usu�rio", "", "", lblTextblockcontagemresultado_contadorfmcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontagemresultado_contadorfmcod_Visible, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            wb_table12_16_JV2( true) ;
         }
         else
         {
            wb_table12_16_JV2( false) ;
         }
         return  ;
      }

      protected void wb_table12_16_JV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_parecertcn_Internalname, lblTextblockcontagemresultado_parecertcn_Caption, "", "", lblTextblockcontagemresultado_parecertcn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContagemresultado_parecertcn_Internalname, AV8ContagemResultado_ParecerTcn, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", 0, 1, 1, 0, 70, "chr", 4, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfb_Internalname, "Valor Bruto", "", "", lblTextblockcontagemresultado_pfb_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontagemresultado_pfb_Visible, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            wb_table13_33_JV2( true) ;
         }
         else
         {
            wb_table13_33_JV2( false) ;
         }
         return  ;
      }

      protected void wb_table13_33_JV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_JV2e( true) ;
         }
         else
         {
            wb_table4_11_JV2e( false) ;
         }
      }

      protected void wb_table13_33_JV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_pfb_Internalname, tblTablemergedcontagemresultado_pfb_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pfb_Internalname, StringUtil.LTrim( StringUtil.NToC( AV6ContagemResultado_PFB, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV6ContagemResultado_PFB, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", edtavContagemresultado_pfb_Invitemessage, edtavContagemresultado_pfb_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_pfb_Visible, edtavContagemresultado_pfb_Enabled, 1, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfl_Internalname, "Valor L�quido", "", "", lblTextblockcontagemresultado_pfl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontagemresultado_pfl_Visible, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pfl_Internalname, StringUtil.LTrim( StringUtil.NToC( AV7ContagemResultado_PFL, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV7ContagemResultado_PFL, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", edtavContagemresultado_pfl_Invitemessage, edtavContagemresultado_pfl_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_pfl_Visible, edtavContagemresultado_pfl_Enabled, 1, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproximaetapa_Internalname, "Pr�xima etapa", "", "", lblTextblockproximaetapa_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockproximaetapa_Visible, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProximaetapa, cmbavProximaetapa_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV82ProximaEtapa), 6, 0)), 1, cmbavProximaetapa_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavProximaetapa.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_WP_RE.htm");
            cmbavProximaetapa.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV82ProximaEtapa), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProximaetapa_Internalname, "Values", (String)(cmbavProximaetapa.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_33_JV2e( true) ;
         }
         else
         {
            wb_table13_33_JV2e( false) ;
         }
      }

      protected void wb_table12_16_JV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_contadorfmcod_Internalname, tblTablemergedcontagemresultado_contadorfmcod_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contadorfmcod, dynavContagemresultado_contadorfmcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0)), 1, dynavContagemresultado_contadorfmcod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContagemresultado_contadorfmcod.Visible, dynavContagemresultado_contadorfmcod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_WP_RE.htm");
            dynavContagemresultado_contadorfmcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_ContadorFMCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfmcod_Internalname, "Values", (String)(dynavContagemresultado_contadorfmcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontestacao_Internalname, "Contesta��o", "", "", lblTextblockcontestacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontestacao_Visible, 1, 0, "HLP_WP_RE.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavContestacao_Internalname, StringUtil.BoolToStr( AV97Contestacao), "", "", chkavContestacao.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(23, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_16_JV2e( true) ;
         }
         else
         {
            wb_table12_16_JV2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A456ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         AV43OS = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43OS", AV43OS);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vOS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV43OS, "@!"))));
         AV42Nome = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Nome", AV42Nome);
         AV68TipodePrazo = (String)getParm(obj,3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TipodePrazo", AV68TipodePrazo);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJV2( ) ;
         WSJV2( ) ;
         WEJV2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         if ( ! ( WebComp_Wccontagemresultadoevidencias == null ) )
         {
            if ( StringUtil.Len( WebComp_Wccontagemresultadoevidencias_Component) != 0 )
            {
               WebComp_Wccontagemresultadoevidencias.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020326226829");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_re.js", "?2020326226829");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultado_contadorfmcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CONTADORFMCOD";
         dynavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD";
         lblTextblockcontestacao_Internalname = "TEXTBLOCKCONTESTACAO";
         chkavContestacao_Internalname = "vCONTESTACAO";
         tblTablemergedcontagemresultado_contadorfmcod_Internalname = "TABLEMERGEDCONTAGEMRESULTADO_CONTADORFMCOD";
         lblTextblockcontagemresultado_parecertcn_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PARECERTCN";
         edtavContagemresultado_parecertcn_Internalname = "vCONTAGEMRESULTADO_PARECERTCN";
         lblTextblockcontagemresultado_pfb_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFB";
         edtavContagemresultado_pfb_Internalname = "vCONTAGEMRESULTADO_PFB";
         lblTextblockcontagemresultado_pfl_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFL";
         edtavContagemresultado_pfl_Internalname = "vCONTAGEMRESULTADO_PFL";
         lblTextblockproximaetapa_Internalname = "TEXTBLOCKPROXIMAETAPA";
         cmbavProximaetapa_Internalname = "vPROXIMAETAPA";
         tblTablemergedcontagemresultado_pfb_Internalname = "TABLEMERGEDCONTAGEMRESULTADO_PFB";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         lblTextblockcontratoorigem_codigo_Internalname = "TEXTBLOCKCONTRATOORIGEM_CODIGO";
         dynavContratoorigem_codigo_Internalname = "vCONTRATOORIGEM_CODIGO";
         lblTextblockservicoaexecutar_Internalname = "TEXTBLOCKSERVICOAEXECUTAR";
         cmbavServicoaexecutar_Internalname = "vSERVICOAEXECUTAR";
         lblTextblockvalorpf_Internalname = "TEXTBLOCKVALORPF";
         edtavValorpf_Internalname = "vVALORPF";
         lblTextblockprazo_Internalname = "TEXTBLOCKPRAZO";
         edtavPrazo_Internalname = "vPRAZO";
         lblTextblockesforco_Internalname = "TEXTBLOCKESFORCO";
         edtavEsforco_Internalname = "vESFORCO";
         tblTblesforco_Internalname = "TBLESFORCO";
         lblTextblockcontador_codigo_Internalname = "TEXTBLOCKCONTADOR_CODIGO";
         dynavContador_codigo_Internalname = "vCONTADOR_CODIGO";
         lblTextblockbruto_Internalname = "TEXTBLOCKBRUTO";
         edtavBruto_Internalname = "vBRUTO";
         lblTextblockliquido_Internalname = "TEXTBLOCKLIQUIDO";
         edtavLiquido_Internalname = "vLIQUIDO";
         tblTblcnt_Internalname = "TBLCNT";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         lblTextblockvalor_Internalname = "TEXTBLOCKVALOR";
         edtavValor_Internalname = "vVALOR";
         tblTblsrvcombinado_Internalname = "TBLSRVCOMBINADO";
         lblTbanexos_Internalname = "TBANEXOS";
         tblTablemergedtbanexos_Internalname = "TABLEMERGEDTBANEXOS";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         bttBtnaceita_Internalname = "BTNACEITA";
         bttBtnrecusa_Internalname = "BTNRECUSA";
         tblTblaceitarecusa_Internalname = "TBLACEITARECUSA";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblActionstable_Internalname = "ACTIONSTABLE";
         lblTbjava_Internalname = "TBJAVA";
         tblTablemain_Internalname = "TABLEMAIN";
         chkavCriarproposta_Internalname = "vCRIARPROPOSTA";
         chkavIncluiproposta_Internalname = "vINCLUIPROPOSTA";
         chkavCombinada_Internalname = "vCOMBINADA";
         chkavPfvisible_Internalname = "vPFVISIBLE";
         cmbavStatusantdmn_Internalname = "vSTATUSANTDMN";
         edtavPrestadora_Internalname = "vPRESTADORA";
         edtavOsvinculada_Internalname = "vOSVINCULADA";
         edtavDemanda_Internalname = "vDEMANDA";
         edtavServico_Internalname = "vSERVICO";
         edtavAreatrabalho_Internalname = "vAREATRABALHO";
         edtavContratadaorigem_codigo_Internalname = "vCONTRATADAORIGEM_CODIGO";
         chkavTemvnccmp_Internalname = "vTEMVNCCMP";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         lblTextblockcontestacao_Visible = 1;
         dynavContagemresultado_contadorfmcod_Jsonclick = "";
         cmbavProximaetapa_Jsonclick = "";
         lblTextblockproximaetapa_Visible = 1;
         edtavContagemresultado_pfl_Jsonclick = "";
         lblTextblockcontagemresultado_pfl_Visible = 1;
         edtavContagemresultado_pfb_Jsonclick = "";
         lblTextblockcontagemresultado_pfb_Visible = 1;
         lblTextblockcontagemresultado_contadorfmcod_Visible = 1;
         edtavEsforco_Jsonclick = "";
         edtavLiquido_Jsonclick = "";
         edtavBruto_Jsonclick = "";
         dynavContador_codigo_Jsonclick = "";
         edtavValor_Jsonclick = "";
         edtavValor_Enabled = 1;
         edtavPrazo_Jsonclick = "";
         edtavValorpf_Jsonclick = "";
         edtavValorpf_Enabled = 1;
         cmbavServicoaexecutar_Jsonclick = "";
         dynavContratoorigem_codigo_Jsonclick = "";
         dynavContratoorigem_codigo.Enabled = 1;
         bttBtnaceita_Visible = 1;
         bttBtnenter_Visible = 1;
         lblTbjava_Visible = 1;
         lblTbjava_Caption = "tbJava";
         lblTextblockbruto_Caption = "Estimada: ���Bruto";
         tblTblaceitarecusa_Visible = 1;
         tblActionstable_Visible = 1;
         dynavContagemresultado_contadorfmcod.Visible = 1;
         edtavContagemresultado_pfl_Visible = 1;
         edtavContagemresultado_pfb_Visible = 1;
         tblTblcnt_Visible = 1;
         tblTblesforco_Visible = 1;
         edtavPrazo_Enabled = 1;
         cmbavServicoaexecutar.Enabled = 1;
         cmbavProximaetapa.Visible = 1;
         lblTextblockcontagemresultado_parecertcn_Caption = "Parecer t�cnico";
         tblTblsrvcombinado_Visible = 1;
         chkavContestacao.Visible = 1;
         edtavContagemresultado_pfl_Invitemessage = "";
         edtavContagemresultado_pfb_Invitemessage = "";
         edtavContagemresultado_pfl_Enabled = 1;
         edtavContagemresultado_pfb_Enabled = 1;
         dynavContagemresultado_contadorfmcod.Enabled = 1;
         chkavTemvnccmp.Caption = "";
         chkavPfvisible.Caption = "";
         chkavCombinada.Caption = "";
         chkavIncluiproposta.Caption = "";
         chkavCriarproposta.Caption = "";
         chkavContestacao.Caption = "";
         chkavTemvnccmp.Visible = 1;
         edtavContratadaorigem_codigo_Jsonclick = "";
         edtavContratadaorigem_codigo_Visible = 1;
         edtavAreatrabalho_Jsonclick = "";
         edtavAreatrabalho_Visible = 1;
         edtavServico_Jsonclick = "";
         edtavServico_Visible = 1;
         edtavDemanda_Jsonclick = "";
         edtavDemanda_Visible = 1;
         edtavOsvinculada_Jsonclick = "";
         edtavOsvinculada_Visible = 1;
         edtavPrestadora_Jsonclick = "";
         edtavPrestadora_Visible = 1;
         cmbavStatusantdmn_Jsonclick = "";
         cmbavStatusantdmn.Visible = 1;
         chkavPfvisible.Visible = 1;
         chkavCombinada.Visible = 1;
         chkavIncluiproposta.Visible = 1;
         chkavCriarproposta.Visible = 1;
         Form.Caption = "Registro de Esfor�o";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Prestadora( int GX_Parm1 ,
                                     GXCombobox dynGX_Parm2 )
      {
         AV45Prestadora = GX_Parm1;
         dynavContador_codigo = dynGX_Parm2;
         AV72Contador_Codigo = (int)(NumberUtil.Val( dynavContador_codigo.CurrentValue, "."));
         GXVvCONTADOR_CODIGO_htmlJV2( AV45Prestadora) ;
         dynload_actions( ) ;
         if ( dynavContador_codigo.ItemCount > 0 )
         {
            AV72Contador_Codigo = (int)(NumberUtil.Val( dynavContador_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV72Contador_Codigo), 6, 0))), "."));
         }
         dynavContador_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV72Contador_Codigo), 6, 0));
         isValidOutput.Add(dynavContador_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV9WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV9WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'BTNACEITA',prop:'Visible'},{ctrl:'BTNENTER',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E12JV2',iparms:[{av:'AV5ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV6ContagemResultado_PFB',fld:'vCONTAGEMRESULTADO_PFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7ContagemResultado_PFL',fld:'vCONTAGEMRESULTADO_PFL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64Prazo',fld:'vPRAZO',pic:'',nv:''},{av:'AV76PropostaRqrPrz',fld:'vPROPOSTARQRPRZ',pic:'',nv:false},{av:'AV60Esforco',fld:'vESFORCO',pic:'ZZZ9',nv:0},{av:'AV75PropostaRqrEsf',fld:'vPROPOSTARQRESF',pic:'',nv:false},{av:'AV69Valor',fld:'vVALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV67ServicoAExecutar',fld:'vSERVICOAEXECUTAR',pic:'ZZZZZ9',nv:0},{av:'AV79Liquido',fld:'vLIQUIDO',pic:'ZZ9.99',nv:0.0},{av:'AV88TpVnc',fld:'vTPVNC',pic:'',nv:''},{av:'AV51Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV54Responsavel_Codigo',fld:'vRESPONSAVEL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV97Contestacao',fld:'vCONTESTACAO',pic:'',nv:false},{av:'AV8ContagemResultado_ParecerTcn',fld:'vCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV9WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV39AreaTrabalho',fld:'vAREATRABALHO',pic:'ZZZZZ9',nv:0},{av:'AV20Servico',fld:'vSERVICO',pic:'ZZZZZ9',nv:0},{av:'AV93ContratadaOrigem_Codigo',fld:'vCONTRATADAORIGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37Demanda',fld:'vDEMANDA',pic:'@!',nv:''},{av:'AV24OSVinculada',fld:'vOSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV55Combinada',fld:'vCOMBINADA',pic:'',nv:false},{av:'AV87PFVisible',fld:'vPFVISIBLE',pic:'',nv:false},{av:'AV89StatusAntDmn',fld:'vSTATUSANTDMN',pic:'',nv:''},{av:'AV57CriarProposta',fld:'vCRIARPROPOSTA',pic:'',nv:false},{av:'AV62IncluiProposta',fld:'vINCLUIPROPOSTA',pic:'',nv:false},{av:'AV82ProximaEtapa',fld:'vPROXIMAETAPA',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85Faturamento',fld:'vFATURAMENTO',pic:'',nv:''},{av:'AV78Bruto',fld:'vBRUTO',pic:'ZZ9.99',nv:0.0},{av:'AV70ValorPF',fld:'vVALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26TipoFabrica',fld:'vTIPOFABRICA',pic:'',nv:''},{av:'AV66Proposta_Codigo',fld:'vPROPOSTA_CODIGO',pic:'ZZZZZZZZ9',nv:0},{av:'Gx_time',fld:'vTIME',pic:'',nv:''},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A516Contratada_TipoFabrica',fld:'CONTRATADA_TIPOFABRICA',pic:'',nv:''},{av:'AV14ContagemResultado_PFBFS',fld:'vCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94PFBFS',fld:'vPFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV95PFLFS',fld:'vPFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV50QtdUntCns',fld:'vQTDUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV21Divergencia',fld:'vDIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV13ContagemResultado_PFBFM',fld:'vCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22ContagemResultado_PFLFM',fld:'vCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV23ContagemResultado_PFLFS',fld:'vCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25StatusCnt',fld:'vSTATUSCNT',pic:'Z9',nv:0},{av:'AV18StatusDmn',fld:'vSTATUSDMN',pic:'',nv:''},{av:'AV40PrazoEntrega',fld:'vPRAZOENTREGA',pic:'',nv:''},{av:'AV81NivelContagem',fld:'vNIVELCONTAGEM',pic:'ZZZ9',nv:0},{av:'AV86TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV65Proposta',fld:'vPROPOSTA',pic:'',nv:null},{av:'AV84Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV59DataEntrega',fld:'vDATAENTREGA',pic:'',nv:''},{av:'AV74PropostaRqrCnt',fld:'vPROPOSTARQRCNT',pic:'',nv:false},{av:'AV72Contador_Codigo',fld:'vCONTADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV68TipodePrazo',fld:'vTIPODEPRAZO',pic:'',nv:''},{av:'AV42Nome',fld:'vNOME',pic:'@!',nv:''},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV45Prestadora',fld:'vPRESTADORA',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A1636ContagemResultado_ServicoSS',fld:'CONTAGEMRESULTADO_SERVICOSS',pic:'ZZZZZ9',nv:0},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A685ContagemResultado_PFLFSUltima',fld:'CONTAGEMRESULTADO_PFLFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A683ContagemResultado_PFLFMUltima',fld:'CONTAGEMRESULTADO_PFLFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV34CalculoDivergencia',fld:'vCALCULODIVERGENCIA',pic:'',nv:''},{av:'AV35IndiceDivergencia',fld:'vINDICEDIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV96TemVncCmp',fld:'vTEMVNCCMP',pic:'',nv:false},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A1593ContagemResultado_CntSrvTpVnc',fld:'CONTAGEMRESULTADO_CNTSRVTPVNC',pic:'',nv:''}],oparms:[{av:'AV25StatusCnt',fld:'vSTATUSCNT',pic:'Z9',nv:0},{av:'AV21Divergencia',fld:'vDIVERGENCIA',pic:'ZZ9.99',nv:0.0},{av:'AV18StatusDmn',fld:'vSTATUSDMN',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV51Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV42Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV69Valor',fld:'vVALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65Proposta',fld:'vPROPOSTA',pic:'',nv:null},{av:'AV14ContagemResultado_PFBFS',fld:'vCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV23ContagemResultado_PFLFS',fld:'vCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV13ContagemResultado_PFBFM',fld:'vCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22ContagemResultado_PFLFM',fld:'vCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81NivelContagem',fld:'vNIVELCONTAGEM',pic:'ZZZ9',nv:0},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV84Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV64Prazo',fld:'vPRAZO',pic:'',nv:''},{av:'AV54Responsavel_Codigo',fld:'vRESPONSAVEL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24OSVinculada',fld:'vOSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV5ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_ParecerTcn',fld:'vCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV20Servico',fld:'vSERVICO',pic:'ZZZZZ9',nv:0},{av:'AV96TemVncCmp',fld:'vTEMVNCCMP',pic:'',nv:false}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E13JV2',iparms:[{av:'AV68TipodePrazo',fld:'vTIPODEPRAZO',pic:'',nv:''},{av:'AV42Nome',fld:'vNOME',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("'DOACEITA'","{handler:'E14JV2',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV54Responsavel_Codigo',fld:'vRESPONSAVEL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV89StatusAntDmn',fld:'vSTATUSANTDMN',pic:'',nv:''},{av:'AV8ContagemResultado_ParecerTcn',fld:'vCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV59DataEntrega',fld:'vDATAENTREGA',pic:'',nv:''},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',nv:''}],oparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV89StatusAntDmn',fld:'vSTATUSANTDMN',pic:'',nv:''},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'}]}");
         setEventMetadata("'DORECUSA'","{handler:'E15JV2',iparms:[{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV9WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV89StatusAntDmn',fld:'vSTATUSANTDMN',pic:'',nv:''},{av:'AV8ContagemResultado_ParecerTcn',fld:'vCONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'AV59DataEntrega',fld:'vDATAENTREGA',pic:'',nv:''},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',nv:''}],oparms:[{av:'AV54Responsavel_Codigo',fld:'vRESPONSAVEL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV89StatusAntDmn',fld:'vSTATUSANTDMN',pic:'',nv:''},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'}]}");
         setEventMetadata("VSERVICOAEXECUTAR.CLICK","{handler:'E17JV2',iparms:[{av:'AV67ServicoAExecutar',fld:'vSERVICOAEXECUTAR',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A557Servico_VlrUnidadeContratada',fld:'SERVICO_VLRUNIDADECONTRATADA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A116Contrato_ValorUnidadeContratacao',fld:'CONTRATO_VALORUNIDADECONTRATACAO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85Faturamento',fld:'vFATURAMENTO',pic:'',nv:''},{av:'AV78Bruto',fld:'vBRUTO',pic:'ZZ9.99',nv:0.0},{av:'AV79Liquido',fld:'vLIQUIDO',pic:'ZZ9.99',nv:0.0},{av:'AV60Esforco',fld:'vESFORCO',pic:'ZZZ9',nv:0},{av:'AV70ValorPF',fld:'vVALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV69Valor',fld:'vVALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV70ValorPF',fld:'vVALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV43OS = "";
         wcpOAV42Nome = "";
         wcpOAV68TipodePrazo = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         Gx_time = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV88TpVnc = "";
         AV51Codigos = new GxSimpleCollection();
         AV85Faturamento = "B";
         AV26TipoFabrica = "S";
         A516Contratada_TipoFabrica = "";
         AV18StatusDmn = "";
         AV40PrazoEntrega = DateTime.MinValue;
         AV86TipoDias = "";
         AV65Proposta = new SdtProposta(context);
         AV59DataEntrega = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         A1326ContagemResultado_ContratadaTipoFab = "";
         AV34CalculoDivergencia = "";
         A1593ContagemResultado_CntSrvTpVnc = "";
         A1234LogResponsavel_NovoStatus = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV89StatusAntDmn = "";
         AV37Demanda = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldWccontagemresultadoevidencias = "";
         WebComp_Wccontagemresultadoevidencias_Component = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00JV2_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00JV2_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00JV2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00JV2_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00JV2_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00JV2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00JV2_A516Contratada_TipoFabrica = new String[] {""} ;
         H00JV2_n516Contratada_TipoFabrica = new bool[] {false} ;
         H00JV3_A74Contrato_Codigo = new int[1] ;
         H00JV3_A77Contrato_Numero = new String[] {""} ;
         H00JV4_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00JV4_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00JV4_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00JV4_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00JV4_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00JV4_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00JV5_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00JV5_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00JV5_A456ContagemResultado_Codigo = new int[1] ;
         H00JV5_A516Contratada_TipoFabrica = new String[] {""} ;
         H00JV5_n516Contratada_TipoFabrica = new bool[] {false} ;
         AV8ContagemResultado_ParecerTcn = "";
         AV64Prazo = DateTime.MinValue;
         AV60Esforco = 0;
         hsh = "";
         AV49Websession = context.GetSession();
         H00JV6_A892LogResponsavel_DemandaCod = new int[1] ;
         H00JV6_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00JV6_A896LogResponsavel_Owner = new int[1] ;
         H00JV6_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00JV6_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00JV6_A1797LogResponsavel_Codigo = new long[1] ;
         H00JV7_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00JV7_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         AV90ContagemResultado = new SdtContagemResultado(context);
         H00JV8_A39Contratada_Codigo = new int[1] ;
         H00JV8_A516Contratada_TipoFabrica = new String[] {""} ;
         H00JV8_n516Contratada_TipoFabrica = new bool[] {false} ;
         H00JV10_A1636ContagemResultado_ServicoSS = new int[1] ;
         H00JV10_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         H00JV10_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00JV10_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00JV10_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00JV10_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00JV10_A456ContagemResultado_Codigo = new int[1] ;
         H00JV10_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00JV10_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00JV10_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00JV10_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         H00JV10_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00JV10_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         H00JV12_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00JV12_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00JV12_A456ContagemResultado_Codigo = new int[1] ;
         H00JV12_A602ContagemResultado_OSVinculada = new int[1] ;
         H00JV12_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00JV12_A1636ContagemResultado_ServicoSS = new int[1] ;
         H00JV12_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         H00JV12_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00JV12_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00JV12_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         H00JV12_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         H00JV12_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00JV12_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00JV12_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00JV12_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00JV12_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00JV12_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         H00JV12_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00JV12_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         AV61HoraCnt = context.localUtil.Time( );
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         H00JV13_A1797LogResponsavel_Codigo = new long[1] ;
         H00JV13_A896LogResponsavel_Owner = new int[1] ;
         H00JV13_A892LogResponsavel_DemandaCod = new int[1] ;
         H00JV13_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00JV18_A1686Proposta_OSCodigo = new int[1] ;
         H00JV18_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00JV18_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00JV18_A1685Proposta_Codigo = new int[1] ;
         H00JV18_A1702Proposta_Prazo = new DateTime[] {DateTime.MinValue} ;
         H00JV18_A1688Proposta_Valor = new decimal[1] ;
         H00JV18_A1703Proposta_Esforco = new int[1] ;
         H00JV18_A1715Proposta_OSCntCod = new int[1] ;
         H00JV18_n1715Proposta_OSCntCod = new bool[] {false} ;
         H00JV18_A1705Proposta_OSServico = new int[1] ;
         H00JV18_n1705Proposta_OSServico = new bool[] {false} ;
         H00JV18_A1704Proposta_CntSrvCod = new int[1] ;
         H00JV18_A1764Proposta_OSDmn = new String[] {""} ;
         H00JV18_n1764Proposta_OSDmn = new bool[] {false} ;
         H00JV18_A1690Proposta_Objetivo = new String[] {""} ;
         H00JV18_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00JV18_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00JV18_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00JV18_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00JV18_A602ContagemResultado_OSVinculada = new int[1] ;
         H00JV18_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00JV18_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00JV18_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00JV18_A1763Proposta_OSCntSrvFtrm = new String[] {""} ;
         H00JV18_n1763Proposta_OSCntSrvFtrm = new bool[] {false} ;
         H00JV18_A1707Proposta_OSPrzTpDias = new String[] {""} ;
         H00JV18_n1707Proposta_OSPrzTpDias = new bool[] {false} ;
         H00JV18_A1717Proposta_OSPFB = new decimal[1] ;
         H00JV18_n1717Proposta_OSPFB = new bool[] {false} ;
         H00JV18_A1718Proposta_OSPFL = new decimal[1] ;
         H00JV18_n1718Proposta_OSPFL = new bool[] {false} ;
         H00JV18_A1719Proposta_OSDataCnt = new DateTime[] {DateTime.MinValue} ;
         H00JV18_n1719Proposta_OSDataCnt = new bool[] {false} ;
         H00JV18_A1720Proposta_OSHoraCnt = new String[] {""} ;
         H00JV18_n1720Proposta_OSHoraCnt = new bool[] {false} ;
         A1702Proposta_Prazo = DateTime.MinValue;
         A1764Proposta_OSDmn = "";
         A1690Proposta_Objetivo = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A1763Proposta_OSCntSrvFtrm = "";
         A1707Proposta_OSPrzTpDias = "";
         A1719Proposta_OSDataCnt = DateTime.MinValue;
         A1720Proposta_OSHoraCnt = "";
         AV58DataCnt = DateTime.MinValue;
         H00JV20_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         H00JV20_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00JV20_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00JV20_A456ContagemResultado_Codigo = new int[1] ;
         H00JV20_A1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         H00JV20_n1121NaoConformidade_EhImpeditiva = new bool[] {false} ;
         H00JV20_A516Contratada_TipoFabrica = new String[] {""} ;
         H00JV20_n516Contratada_TipoFabrica = new bool[] {false} ;
         H00JV20_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00JV20_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00JV20_A1603ContagemResultado_CntCod = new int[1] ;
         H00JV20_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00JV20_A457ContagemResultado_Demanda = new String[] {""} ;
         H00JV20_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00JV20_A512ContagemResultado_ValorPF = new decimal[1] ;
         H00JV20_n512ContagemResultado_ValorPF = new bool[] {false} ;
         H00JV20_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00JV20_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00JV20_A1714ContagemResultado_Combinada = new bool[] {false} ;
         H00JV20_n1714ContagemResultado_Combinada = new bool[] {false} ;
         H00JV20_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00JV20_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00JV20_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00JV20_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00JV20_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00JV20_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00JV20_A1594ContagemResultado_CntSrvFtrm = new String[] {""} ;
         H00JV20_n1594ContagemResultado_CntSrvFtrm = new bool[] {false} ;
         H00JV20_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         H00JV20_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         H00JV20_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00JV20_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00JV20_A1613ContagemResultado_CntSrvQtdUntCns = new decimal[1] ;
         H00JV20_n1613ContagemResultado_CntSrvQtdUntCns = new bool[] {false} ;
         H00JV20_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         H00JV20_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         H00JV20_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         H00JV20_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         H00JV20_A601ContagemResultado_Servico = new int[1] ;
         H00JV20_n601ContagemResultado_Servico = new bool[] {false} ;
         H00JV20_A602ContagemResultado_OSVinculada = new int[1] ;
         H00JV20_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00JV20_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00JV20_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00JV20_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         H00JV20_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         H00JV20_A508ContagemResultado_Owner = new int[1] ;
         H00JV20_A1610ContagemResultado_PrzTp = new String[] {""} ;
         H00JV20_n1610ContagemResultado_PrzTp = new bool[] {false} ;
         H00JV20_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         H00JV20_A825ContagemResultado_HoraUltCnt = new String[] {""} ;
         H00JV20_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00JV20_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         H00JV20_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00JV20_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         A457ContagemResultado_Demanda = "";
         A1594ContagemResultado_CntSrvFtrm = "";
         A1611ContagemResultado_PrzTpDias = "";
         A1610ContagemResultado_PrzTp = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         A825ContagemResultado_HoraUltCnt = "";
         H00JV21_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         AV77PropostaRqrSrv = true;
         AV73PropostaNvlCnt = 0;
         H00JV22_A1130LogResponsavel_Status = new String[] {""} ;
         H00JV22_n1130LogResponsavel_Status = new bool[] {false} ;
         H00JV22_A894LogResponsavel_Acao = new String[] {""} ;
         H00JV22_A1797LogResponsavel_Codigo = new long[1] ;
         H00JV22_A896LogResponsavel_Owner = new int[1] ;
         H00JV22_A892LogResponsavel_DemandaCod = new int[1] ;
         H00JV22_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         A1130LogResponsavel_Status = "";
         A894LogResponsavel_Acao = "";
         H00JV23_A155Servico_Codigo = new int[1] ;
         H00JV23_A74Contrato_Codigo = new int[1] ;
         H00JV23_A160ContratoServicos_Codigo = new int[1] ;
         H00JV23_A605Servico_Sigla = new String[] {""} ;
         A605Servico_Sigla = "";
         GXt_dtime4 = (DateTime)(DateTime.MinValue);
         H00JV24_A74Contrato_Codigo = new int[1] ;
         H00JV24_A160ContratoServicos_Codigo = new int[1] ;
         H00JV24_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         H00JV24_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         H00JV25_A1589ContratoSrvVnc_SrvVncCntSrvCod = new int[1] ;
         H00JV25_n1589ContratoSrvVnc_SrvVncCntSrvCod = new bool[] {false} ;
         H00JV25_A923ContratoSrvVnc_ServicoVncCod = new int[1] ;
         H00JV25_n923ContratoSrvVnc_ServicoVncCod = new bool[] {false} ;
         H00JV25_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         H00JV25_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         H00JV25_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         H00JV25_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         H00JV25_A1800ContratoSrvVnc_DoStatusDmn = new String[] {""} ;
         H00JV25_n1800ContratoSrvVnc_DoStatusDmn = new bool[] {false} ;
         H00JV25_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         H00JV25_A924ContratoSrvVnc_ServicoVncSigla = new String[] {""} ;
         H00JV25_n924ContratoSrvVnc_ServicoVncSigla = new bool[] {false} ;
         H00JV25_A917ContratoSrvVnc_Codigo = new int[1] ;
         A1084ContratoSrvVnc_StatusDmn = "";
         A1800ContratoSrvVnc_DoStatusDmn = "";
         A924ContratoSrvVnc_ServicoVncSigla = "";
         A1743ContratoSrvVnc_NovoStatusDmn = "";
         H00JV26_A892LogResponsavel_DemandaCod = new int[1] ;
         H00JV26_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00JV26_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         H00JV26_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         H00JV26_A1797LogResponsavel_Codigo = new long[1] ;
         sStyleString = "";
         lblTbjava_Jsonclick = "";
         bttBtnenter_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         bttBtnaceita_Jsonclick = "";
         bttBtnrecusa_Jsonclick = "";
         lblTbanexos_Jsonclick = "";
         lblTextblockcontratoorigem_codigo_Jsonclick = "";
         lblTextblockservicoaexecutar_Jsonclick = "";
         lblTextblockvalorpf_Jsonclick = "";
         lblTextblockprazo_Jsonclick = "";
         lblTextblockvalor_Jsonclick = "";
         lblTextblockcontador_codigo_Jsonclick = "";
         lblTextblockbruto_Jsonclick = "";
         lblTextblockliquido_Jsonclick = "";
         lblTextblockesforco_Jsonclick = "";
         lblTextblockcontagemresultado_contadorfmcod_Jsonclick = "";
         lblTextblockcontagemresultado_parecertcn_Jsonclick = "";
         lblTextblockcontagemresultado_pfb_Jsonclick = "";
         lblTextblockcontagemresultado_pfl_Jsonclick = "";
         lblTextblockproximaetapa_Jsonclick = "";
         lblTextblockcontestacao_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_re__default(),
            new Object[][] {
                new Object[] {
               H00JV2_A70ContratadaUsuario_UsuarioPessoaCod, H00JV2_n70ContratadaUsuario_UsuarioPessoaCod, H00JV2_A69ContratadaUsuario_UsuarioCod, H00JV2_A71ContratadaUsuario_UsuarioPessoaNom, H00JV2_n71ContratadaUsuario_UsuarioPessoaNom, H00JV2_A66ContratadaUsuario_ContratadaCod, H00JV2_A516Contratada_TipoFabrica, H00JV2_n516Contratada_TipoFabrica
               }
               , new Object[] {
               H00JV3_A74Contrato_Codigo, H00JV3_A77Contrato_Numero
               }
               , new Object[] {
               H00JV4_A70ContratadaUsuario_UsuarioPessoaCod, H00JV4_n70ContratadaUsuario_UsuarioPessoaCod, H00JV4_A69ContratadaUsuario_UsuarioCod, H00JV4_A71ContratadaUsuario_UsuarioPessoaNom, H00JV4_n71ContratadaUsuario_UsuarioPessoaNom, H00JV4_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00JV5_A805ContagemResultado_ContratadaOrigemCod, H00JV5_n805ContagemResultado_ContratadaOrigemCod, H00JV5_A456ContagemResultado_Codigo, H00JV5_A516Contratada_TipoFabrica, H00JV5_n516Contratada_TipoFabrica
               }
               , new Object[] {
               H00JV6_A892LogResponsavel_DemandaCod, H00JV6_n892LogResponsavel_DemandaCod, H00JV6_A896LogResponsavel_Owner, H00JV6_A490ContagemResultado_ContratadaCod, H00JV6_n490ContagemResultado_ContratadaCod, H00JV6_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               H00JV7_A66ContratadaUsuario_ContratadaCod, H00JV7_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               H00JV8_A39Contratada_Codigo, H00JV8_A516Contratada_TipoFabrica
               }
               , new Object[] {
               H00JV10_A1636ContagemResultado_ServicoSS, H00JV10_n1636ContagemResultado_ServicoSS, H00JV10_A484ContagemResultado_StatusDmn, H00JV10_n484ContagemResultado_StatusDmn, H00JV10_A490ContagemResultado_ContratadaCod, H00JV10_n490ContagemResultado_ContratadaCod, H00JV10_A456ContagemResultado_Codigo, H00JV10_A1326ContagemResultado_ContratadaTipoFab, H00JV10_n1326ContagemResultado_ContratadaTipoFab, H00JV10_A684ContagemResultado_PFBFSUltima,
               H00JV10_A685ContagemResultado_PFLFSUltima, H00JV10_A682ContagemResultado_PFBFMUltima, H00JV10_A683ContagemResultado_PFLFMUltima
               }
               , new Object[] {
               H00JV12_A1553ContagemResultado_CntSrvCod, H00JV12_n1553ContagemResultado_CntSrvCod, H00JV12_A456ContagemResultado_Codigo, H00JV12_A602ContagemResultado_OSVinculada, H00JV12_n602ContagemResultado_OSVinculada, H00JV12_A1636ContagemResultado_ServicoSS, H00JV12_n1636ContagemResultado_ServicoSS, H00JV12_A484ContagemResultado_StatusDmn, H00JV12_n484ContagemResultado_StatusDmn, H00JV12_A1593ContagemResultado_CntSrvTpVnc,
               H00JV12_n1593ContagemResultado_CntSrvTpVnc, H00JV12_A490ContagemResultado_ContratadaCod, H00JV12_n490ContagemResultado_ContratadaCod, H00JV12_A1326ContagemResultado_ContratadaTipoFab, H00JV12_n1326ContagemResultado_ContratadaTipoFab, H00JV12_A684ContagemResultado_PFBFSUltima, H00JV12_A685ContagemResultado_PFLFSUltima, H00JV12_A682ContagemResultado_PFBFMUltima, H00JV12_A683ContagemResultado_PFLFMUltima
               }
               , new Object[] {
               H00JV13_A1797LogResponsavel_Codigo, H00JV13_A896LogResponsavel_Owner, H00JV13_A892LogResponsavel_DemandaCod, H00JV13_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               H00JV18_A1686Proposta_OSCodigo, H00JV18_A484ContagemResultado_StatusDmn, H00JV18_n484ContagemResultado_StatusDmn, H00JV18_A1685Proposta_Codigo, H00JV18_A1702Proposta_Prazo, H00JV18_A1688Proposta_Valor, H00JV18_A1703Proposta_Esforco, H00JV18_A1715Proposta_OSCntCod, H00JV18_n1715Proposta_OSCntCod, H00JV18_A1705Proposta_OSServico,
               H00JV18_n1705Proposta_OSServico, H00JV18_A1704Proposta_CntSrvCod, H00JV18_A1764Proposta_OSDmn, H00JV18_n1764Proposta_OSDmn, H00JV18_A1690Proposta_Objetivo, H00JV18_A490ContagemResultado_ContratadaCod, H00JV18_n490ContagemResultado_ContratadaCod, H00JV18_A472ContagemResultado_DataEntrega, H00JV18_n472ContagemResultado_DataEntrega, H00JV18_A602ContagemResultado_OSVinculada,
               H00JV18_n602ContagemResultado_OSVinculada, H00JV18_A805ContagemResultado_ContratadaOrigemCod, H00JV18_n805ContagemResultado_ContratadaOrigemCod, H00JV18_A1763Proposta_OSCntSrvFtrm, H00JV18_n1763Proposta_OSCntSrvFtrm, H00JV18_A1707Proposta_OSPrzTpDias, H00JV18_n1707Proposta_OSPrzTpDias, H00JV18_A1717Proposta_OSPFB, H00JV18_n1717Proposta_OSPFB, H00JV18_A1718Proposta_OSPFL,
               H00JV18_n1718Proposta_OSPFL, H00JV18_A1719Proposta_OSDataCnt, H00JV18_n1719Proposta_OSDataCnt, H00JV18_A1720Proposta_OSHoraCnt, H00JV18_n1720Proposta_OSHoraCnt
               }
               , new Object[] {
               H00JV20_A903ContratoServicosPrazo_CntSrvCod, H00JV20_A468ContagemResultado_NaoCnfDmnCod, H00JV20_n468ContagemResultado_NaoCnfDmnCod, H00JV20_A456ContagemResultado_Codigo, H00JV20_A1121NaoConformidade_EhImpeditiva, H00JV20_n1121NaoConformidade_EhImpeditiva, H00JV20_A516Contratada_TipoFabrica, H00JV20_n516Contratada_TipoFabrica, H00JV20_A484ContagemResultado_StatusDmn, H00JV20_n484ContagemResultado_StatusDmn,
               H00JV20_A1603ContagemResultado_CntCod, H00JV20_n1603ContagemResultado_CntCod, H00JV20_A457ContagemResultado_Demanda, H00JV20_n457ContagemResultado_Demanda, H00JV20_A512ContagemResultado_ValorPF, H00JV20_n512ContagemResultado_ValorPF, H00JV20_A472ContagemResultado_DataEntrega, H00JV20_n472ContagemResultado_DataEntrega, H00JV20_A1714ContagemResultado_Combinada, H00JV20_n1714ContagemResultado_Combinada,
               H00JV20_A1553ContagemResultado_CntSrvCod, H00JV20_n1553ContagemResultado_CntSrvCod, H00JV20_A490ContagemResultado_ContratadaCod, H00JV20_n490ContagemResultado_ContratadaCod, H00JV20_A805ContagemResultado_ContratadaOrigemCod, H00JV20_n805ContagemResultado_ContratadaOrigemCod, H00JV20_A1594ContagemResultado_CntSrvFtrm, H00JV20_n1594ContagemResultado_CntSrvFtrm, H00JV20_A1611ContagemResultado_PrzTpDias, H00JV20_n1611ContagemResultado_PrzTpDias,
               H00JV20_A1326ContagemResultado_ContratadaTipoFab, H00JV20_n1326ContagemResultado_ContratadaTipoFab, H00JV20_A1613ContagemResultado_CntSrvQtdUntCns, H00JV20_n1613ContagemResultado_CntSrvQtdUntCns, H00JV20_A798ContagemResultado_PFBFSImp, H00JV20_n798ContagemResultado_PFBFSImp, H00JV20_A799ContagemResultado_PFLFSImp, H00JV20_n799ContagemResultado_PFLFSImp, H00JV20_A601ContagemResultado_Servico, H00JV20_n601ContagemResultado_Servico,
               H00JV20_A602ContagemResultado_OSVinculada, H00JV20_n602ContagemResultado_OSVinculada, H00JV20_A52Contratada_AreaTrabalhoCod, H00JV20_n52Contratada_AreaTrabalhoCod, H00JV20_A1593ContagemResultado_CntSrvTpVnc, H00JV20_n1593ContagemResultado_CntSrvTpVnc, H00JV20_A508ContagemResultado_Owner, H00JV20_A1610ContagemResultado_PrzTp, H00JV20_n1610ContagemResultado_PrzTp, H00JV20_A566ContagemResultado_DataUltCnt,
               H00JV20_A825ContagemResultado_HoraUltCnt, H00JV20_A684ContagemResultado_PFBFSUltima, H00JV20_A685ContagemResultado_PFLFSUltima, H00JV20_A682ContagemResultado_PFBFMUltima, H00JV20_A683ContagemResultado_PFLFMUltima
               }
               , new Object[] {
               H00JV21_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               H00JV22_A1130LogResponsavel_Status, H00JV22_n1130LogResponsavel_Status, H00JV22_A894LogResponsavel_Acao, H00JV22_A1797LogResponsavel_Codigo, H00JV22_A896LogResponsavel_Owner, H00JV22_A892LogResponsavel_DemandaCod, H00JV22_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               H00JV23_A155Servico_Codigo, H00JV23_A74Contrato_Codigo, H00JV23_A160ContratoServicos_Codigo, H00JV23_A605Servico_Sigla
               }
               , new Object[] {
               H00JV24_A74Contrato_Codigo, H00JV24_A160ContratoServicos_Codigo, H00JV24_A557Servico_VlrUnidadeContratada, H00JV24_A116Contrato_ValorUnidadeContratacao
               }
               , new Object[] {
               H00JV25_A1589ContratoSrvVnc_SrvVncCntSrvCod, H00JV25_n1589ContratoSrvVnc_SrvVncCntSrvCod, H00JV25_A923ContratoSrvVnc_ServicoVncCod, H00JV25_n923ContratoSrvVnc_ServicoVncCod, H00JV25_A1453ContratoServicosVnc_Ativo, H00JV25_n1453ContratoServicosVnc_Ativo, H00JV25_A1084ContratoSrvVnc_StatusDmn, H00JV25_n1084ContratoSrvVnc_StatusDmn, H00JV25_A1800ContratoSrvVnc_DoStatusDmn, H00JV25_n1800ContratoSrvVnc_DoStatusDmn,
               H00JV25_A915ContratoSrvVnc_CntSrvCod, H00JV25_A924ContratoSrvVnc_ServicoVncSigla, H00JV25_n924ContratoSrvVnc_ServicoVncSigla, H00JV25_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               H00JV26_A892LogResponsavel_DemandaCod, H00JV26_n892LogResponsavel_DemandaCod, H00JV26_A1234LogResponsavel_NovoStatus, H00JV26_n1234LogResponsavel_NovoStatus, H00JV26_A1797LogResponsavel_Codigo
               }
            }
         );
         WebComp_Wccontagemresultadoevidencias = new GeneXus.Http.GXNullWebComponent();
         Gx_time = context.localUtil.Time( );
         /* GeneXus formulas. */
         Gx_time = context.localUtil.Time( );
         context.Gx_err = 0;
         dynavContratoorigem_codigo.Enabled = 0;
         edtavValorpf_Enabled = 0;
         edtavValor_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short AV25StatusCnt ;
      private short AV81NivelContagem ;
      private short AV84Dias ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV60Esforco ;
      private short GXt_int3 ;
      private short AV110GXLvl451 ;
      private short AV73PropostaNvlCnt ;
      private short nGXWrapped ;
      private short wbTemp ;
      private int A456ContagemResultado_Codigo ;
      private int wcpOA456ContagemResultado_Codigo ;
      private int AV45Prestadora ;
      private int edtavValorpf_Enabled ;
      private int edtavValor_Enabled ;
      private int AV54Responsavel_Codigo ;
      private int AV66Proposta_Codigo ;
      private int A39Contratada_Codigo ;
      private int A892LogResponsavel_DemandaCod ;
      private int A896LogResponsavel_Owner ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A602ContagemResultado_OSVinculada ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A160ContratoServicos_Codigo ;
      private int AV53ContratoOrigem_Codigo ;
      private int edtavPrestadora_Visible ;
      private int AV24OSVinculada ;
      private int edtavOsvinculada_Visible ;
      private int edtavDemanda_Visible ;
      private int AV20Servico ;
      private int edtavServico_Visible ;
      private int AV39AreaTrabalho ;
      private int edtavAreatrabalho_Visible ;
      private int AV93ContratadaOrigem_Codigo ;
      private int edtavContratadaorigem_codigo_Visible ;
      private int AV82ProximaEtapa ;
      private int AV67ServicoAExecutar ;
      private int gxdynajaxindex ;
      private int AV5ContagemResultado_ContadorFMCod ;
      private int AV72Contador_Codigo ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int lblTbjava_Visible ;
      private int bttBtnaceita_Visible ;
      private int bttBtnenter_Visible ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1686Proposta_OSCodigo ;
      private int A1685Proposta_Codigo ;
      private int A1703Proposta_Esforco ;
      private int A1715Proposta_OSCntCod ;
      private int A1705Proposta_OSServico ;
      private int A1704Proposta_CntSrvCod ;
      private int AV71CntSrvCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A601ContagemResultado_Servico ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A508ContagemResultado_Owner ;
      private int edtavContagemresultado_pfb_Enabled ;
      private int edtavContagemresultado_pfl_Enabled ;
      private int lblTextblockcontestacao_Visible ;
      private int tblTblsrvcombinado_Visible ;
      private int GXt_int6 ;
      private int AV92QuemRejeitou ;
      private int A155Servico_Codigo ;
      private int A74Contrato_Codigo ;
      private int lblTextblockproximaetapa_Visible ;
      private int edtavPrazo_Enabled ;
      private int tblTblesforco_Visible ;
      private int tblTblcnt_Visible ;
      private int lblTextblockcontagemresultado_pfb_Visible ;
      private int edtavContagemresultado_pfb_Visible ;
      private int lblTextblockcontagemresultado_pfl_Visible ;
      private int edtavContagemresultado_pfl_Visible ;
      private int lblTextblockcontagemresultado_contadorfmcod_Visible ;
      private int tblActionstable_Visible ;
      private int tblTblaceitarecusa_Visible ;
      private int A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int A923ContratoSrvVnc_ServicoVncCod ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A917ContratoSrvVnc_Codigo ;
      private int idxLst ;
      private long A1797LogResponsavel_Codigo ;
      private decimal AV14ContagemResultado_PFBFS ;
      private decimal AV94PFBFS ;
      private decimal AV95PFLFS ;
      private decimal AV50QtdUntCns ;
      private decimal AV21Divergencia ;
      private decimal AV13ContagemResultado_PFBFM ;
      private decimal AV22ContagemResultado_PFLFM ;
      private decimal AV23ContagemResultado_PFLFS ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal AV35IndiceDivergencia ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal AV83Afaturar ;
      private decimal AV6ContagemResultado_PFB ;
      private decimal AV7ContagemResultado_PFL ;
      private decimal AV70ValorPF ;
      private decimal AV78Bruto ;
      private decimal AV79Liquido ;
      private decimal AV69Valor ;
      private decimal GXt_decimal2 ;
      private decimal A1688Proposta_Valor ;
      private decimal A1717Proposta_OSPFB ;
      private decimal A1718Proposta_OSPFL ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A1613ContagemResultado_CntSrvQtdUntCns ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal A799ContagemResultado_PFLFSImp ;
      private String AV42Nome ;
      private String AV68TipodePrazo ;
      private String wcpOAV42Nome ;
      private String wcpOAV68TipodePrazo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_time ;
      private String dynavContratoorigem_codigo_Internalname ;
      private String edtavValorpf_Internalname ;
      private String edtavValor_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV88TpVnc ;
      private String AV26TipoFabrica ;
      private String A516Contratada_TipoFabrica ;
      private String AV18StatusDmn ;
      private String AV86TipoDias ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String AV34CalculoDivergencia ;
      private String A1593ContagemResultado_CntSrvTpVnc ;
      private String A1234LogResponsavel_NovoStatus ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavCriarproposta_Internalname ;
      private String chkavIncluiproposta_Internalname ;
      private String chkavCombinada_Internalname ;
      private String chkavPfvisible_Internalname ;
      private String cmbavStatusantdmn_Internalname ;
      private String AV89StatusAntDmn ;
      private String cmbavStatusantdmn_Jsonclick ;
      private String edtavPrestadora_Internalname ;
      private String edtavPrestadora_Jsonclick ;
      private String edtavOsvinculada_Internalname ;
      private String edtavOsvinculada_Jsonclick ;
      private String edtavDemanda_Internalname ;
      private String edtavDemanda_Jsonclick ;
      private String edtavServico_Internalname ;
      private String edtavServico_Jsonclick ;
      private String edtavAreatrabalho_Internalname ;
      private String edtavAreatrabalho_Jsonclick ;
      private String edtavContratadaorigem_codigo_Internalname ;
      private String edtavContratadaorigem_codigo_Jsonclick ;
      private String chkavTemvnccmp_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldWccontagemresultadoevidencias ;
      private String WebComp_Wccontagemresultadoevidencias_Component ;
      private String chkavContestacao_Internalname ;
      private String dynavContagemresultado_contadorfmcod_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavContagemresultado_parecertcn_Internalname ;
      private String edtavContagemresultado_pfb_Internalname ;
      private String edtavContagemresultado_pfl_Internalname ;
      private String cmbavProximaetapa_Internalname ;
      private String cmbavServicoaexecutar_Internalname ;
      private String edtavPrazo_Internalname ;
      private String edtavEsforco_Internalname ;
      private String dynavContador_codigo_Internalname ;
      private String edtavBruto_Internalname ;
      private String edtavLiquido_Internalname ;
      private String hsh ;
      private String lblTbjava_Internalname ;
      private String bttBtnaceita_Internalname ;
      private String bttBtnenter_Internalname ;
      private String AV61HoraCnt ;
      private String A1707Proposta_OSPrzTpDias ;
      private String A1720Proposta_OSHoraCnt ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String A1610ContagemResultado_PrzTp ;
      private String A825ContagemResultado_HoraUltCnt ;
      private String edtavContagemresultado_pfb_Invitemessage ;
      private String edtavContagemresultado_pfl_Invitemessage ;
      private String lblTextblockcontestacao_Internalname ;
      private String tblTblsrvcombinado_Internalname ;
      private String lblTextblockcontagemresultado_parecertcn_Caption ;
      private String lblTextblockcontagemresultado_parecertcn_Internalname ;
      private String A1130LogResponsavel_Status ;
      private String A894LogResponsavel_Acao ;
      private String A605Servico_Sigla ;
      private String lblTextblockproximaetapa_Internalname ;
      private String tblTblesforco_Internalname ;
      private String tblTblcnt_Internalname ;
      private String lblTextblockcontagemresultado_pfb_Internalname ;
      private String lblTextblockcontagemresultado_pfl_Internalname ;
      private String lblTextblockcontagemresultado_contadorfmcod_Internalname ;
      private String tblActionstable_Internalname ;
      private String tblTblaceitarecusa_Internalname ;
      private String lblTextblockbruto_Caption ;
      private String lblTextblockbruto_Internalname ;
      private String A1084ContratoSrvVnc_StatusDmn ;
      private String A1800ContratoSrvVnc_DoStatusDmn ;
      private String A924ContratoSrvVnc_ServicoVncSigla ;
      private String A1743ContratoSrvVnc_NovoStatusDmn ;
      private String lblTbjava_Caption ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTbjava_Jsonclick ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String bttBtnaceita_Jsonclick ;
      private String bttBtnrecusa_Internalname ;
      private String bttBtnrecusa_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String tblTablemergedtbanexos_Internalname ;
      private String lblTbanexos_Internalname ;
      private String lblTbanexos_Jsonclick ;
      private String lblTextblockcontratoorigem_codigo_Internalname ;
      private String lblTextblockcontratoorigem_codigo_Jsonclick ;
      private String dynavContratoorigem_codigo_Jsonclick ;
      private String lblTextblockservicoaexecutar_Internalname ;
      private String lblTextblockservicoaexecutar_Jsonclick ;
      private String cmbavServicoaexecutar_Jsonclick ;
      private String lblTextblockvalorpf_Internalname ;
      private String lblTextblockvalorpf_Jsonclick ;
      private String edtavValorpf_Jsonclick ;
      private String lblTextblockprazo_Internalname ;
      private String lblTextblockprazo_Jsonclick ;
      private String edtavPrazo_Jsonclick ;
      private String lblTextblockvalor_Internalname ;
      private String lblTextblockvalor_Jsonclick ;
      private String edtavValor_Jsonclick ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTextblockcontador_codigo_Internalname ;
      private String lblTextblockcontador_codigo_Jsonclick ;
      private String dynavContador_codigo_Jsonclick ;
      private String lblTextblockbruto_Jsonclick ;
      private String edtavBruto_Jsonclick ;
      private String lblTextblockliquido_Internalname ;
      private String lblTextblockliquido_Jsonclick ;
      private String edtavLiquido_Jsonclick ;
      private String lblTextblockesforco_Internalname ;
      private String lblTextblockesforco_Jsonclick ;
      private String edtavEsforco_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockcontagemresultado_contadorfmcod_Jsonclick ;
      private String lblTextblockcontagemresultado_parecertcn_Jsonclick ;
      private String lblTextblockcontagemresultado_pfb_Jsonclick ;
      private String tblTablemergedcontagemresultado_pfb_Internalname ;
      private String edtavContagemresultado_pfb_Jsonclick ;
      private String lblTextblockcontagemresultado_pfl_Jsonclick ;
      private String edtavContagemresultado_pfl_Jsonclick ;
      private String lblTextblockproximaetapa_Jsonclick ;
      private String cmbavProximaetapa_Jsonclick ;
      private String tblTablemergedcontagemresultado_contadorfmcod_Internalname ;
      private String dynavContagemresultado_contadorfmcod_Jsonclick ;
      private String lblTextblockcontestacao_Jsonclick ;
      private DateTime GXt_dtime1 ;
      private DateTime GXt_dtime4 ;
      private DateTime AV40PrazoEntrega ;
      private DateTime AV59DataEntrega ;
      private DateTime AV64Prazo ;
      private DateTime A1702Proposta_Prazo ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A1719Proposta_OSDataCnt ;
      private DateTime AV58DataCnt ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV76PropostaRqrPrz ;
      private bool AV75PropostaRqrEsf ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool AV74PropostaRqrCnt ;
      private bool wbLoad ;
      private bool AV57CriarProposta ;
      private bool AV62IncluiProposta ;
      private bool AV55Combinada ;
      private bool AV87PFVisible ;
      private bool AV96TemVncCmp ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n516Contratada_TipoFabrica ;
      private bool AV97Contestacao ;
      private bool returnInSub ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1593ContagemResultado_CntSrvTpVnc ;
      private bool n1715Proposta_OSCntCod ;
      private bool n1705Proposta_OSServico ;
      private bool n1764Proposta_OSDmn ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n1763Proposta_OSCntSrvFtrm ;
      private bool n1707Proposta_OSPrzTpDias ;
      private bool n1717Proposta_OSPFB ;
      private bool n1718Proposta_OSPFL ;
      private bool n1719Proposta_OSDataCnt ;
      private bool n1720Proposta_OSHoraCnt ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool A1121NaoConformidade_EhImpeditiva ;
      private bool n1121NaoConformidade_EhImpeditiva ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n512ContagemResultado_ValorPF ;
      private bool A1714ContagemResultado_Combinada ;
      private bool n1714ContagemResultado_Combinada ;
      private bool n1594ContagemResultado_CntSrvFtrm ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n1613ContagemResultado_CntSrvQtdUntCns ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool n799ContagemResultado_PFLFSImp ;
      private bool n601ContagemResultado_Servico ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n1610ContagemResultado_PrzTp ;
      private bool AV77PropostaRqrSrv ;
      private bool n1130LogResponsavel_Status ;
      private bool GXt_boolean5 ;
      private bool n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool n923ContratoSrvVnc_ServicoVncCod ;
      private bool A1453ContratoServicosVnc_Ativo ;
      private bool n1453ContratoServicosVnc_Ativo ;
      private bool n1084ContratoSrvVnc_StatusDmn ;
      private bool n1800ContratoSrvVnc_DoStatusDmn ;
      private bool n924ContratoSrvVnc_ServicoVncSigla ;
      private bool n1234LogResponsavel_NovoStatus ;
      private String AV8ContagemResultado_ParecerTcn ;
      private String A1690Proposta_Objetivo ;
      private String AV43OS ;
      private String wcpOAV43OS ;
      private String AV85Faturamento ;
      private String AV37Demanda ;
      private String A1764Proposta_OSDmn ;
      private String A1763Proposta_OSCntSrvFtrm ;
      private String A457ContagemResultado_Demanda ;
      private String A1594ContagemResultado_CntSrvFtrm ;
      private GXWebComponent WebComp_Wccontagemresultadoevidencias ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP2_Nome ;
      private String aP3_TipodePrazo ;
      private GXCombobox dynavContagemresultado_contadorfmcod ;
      private GXCheckbox chkavContestacao ;
      private GXCombobox cmbavProximaetapa ;
      private GXCombobox dynavContratoorigem_codigo ;
      private GXCombobox cmbavServicoaexecutar ;
      private GXCombobox dynavContador_codigo ;
      private GXCheckbox chkavCriarproposta ;
      private GXCheckbox chkavIncluiproposta ;
      private GXCheckbox chkavCombinada ;
      private GXCheckbox chkavPfvisible ;
      private GXCombobox cmbavStatusantdmn ;
      private GXCheckbox chkavTemvnccmp ;
      private IDataStoreProvider pr_default ;
      private int[] H00JV2_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00JV2_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00JV2_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00JV2_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00JV2_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00JV2_A66ContratadaUsuario_ContratadaCod ;
      private String[] H00JV2_A516Contratada_TipoFabrica ;
      private bool[] H00JV2_n516Contratada_TipoFabrica ;
      private int[] H00JV3_A74Contrato_Codigo ;
      private String[] H00JV3_A77Contrato_Numero ;
      private int[] H00JV4_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00JV4_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00JV4_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00JV4_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00JV4_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00JV4_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00JV5_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00JV5_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] H00JV5_A456ContagemResultado_Codigo ;
      private String[] H00JV5_A516Contratada_TipoFabrica ;
      private bool[] H00JV5_n516Contratada_TipoFabrica ;
      private int[] H00JV6_A892LogResponsavel_DemandaCod ;
      private bool[] H00JV6_n892LogResponsavel_DemandaCod ;
      private int[] H00JV6_A896LogResponsavel_Owner ;
      private int[] H00JV6_A490ContagemResultado_ContratadaCod ;
      private bool[] H00JV6_n490ContagemResultado_ContratadaCod ;
      private long[] H00JV6_A1797LogResponsavel_Codigo ;
      private int[] H00JV7_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00JV7_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00JV8_A39Contratada_Codigo ;
      private String[] H00JV8_A516Contratada_TipoFabrica ;
      private bool[] H00JV8_n516Contratada_TipoFabrica ;
      private int[] H00JV10_A1636ContagemResultado_ServicoSS ;
      private bool[] H00JV10_n1636ContagemResultado_ServicoSS ;
      private String[] H00JV10_A484ContagemResultado_StatusDmn ;
      private bool[] H00JV10_n484ContagemResultado_StatusDmn ;
      private int[] H00JV10_A490ContagemResultado_ContratadaCod ;
      private bool[] H00JV10_n490ContagemResultado_ContratadaCod ;
      private int[] H00JV10_A456ContagemResultado_Codigo ;
      private String[] H00JV10_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00JV10_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] H00JV10_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00JV10_A685ContagemResultado_PFLFSUltima ;
      private decimal[] H00JV10_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00JV10_A683ContagemResultado_PFLFMUltima ;
      private int[] H00JV12_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00JV12_n1553ContagemResultado_CntSrvCod ;
      private int[] H00JV12_A456ContagemResultado_Codigo ;
      private int[] H00JV12_A602ContagemResultado_OSVinculada ;
      private bool[] H00JV12_n602ContagemResultado_OSVinculada ;
      private int[] H00JV12_A1636ContagemResultado_ServicoSS ;
      private bool[] H00JV12_n1636ContagemResultado_ServicoSS ;
      private String[] H00JV12_A484ContagemResultado_StatusDmn ;
      private bool[] H00JV12_n484ContagemResultado_StatusDmn ;
      private String[] H00JV12_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] H00JV12_n1593ContagemResultado_CntSrvTpVnc ;
      private int[] H00JV12_A490ContagemResultado_ContratadaCod ;
      private bool[] H00JV12_n490ContagemResultado_ContratadaCod ;
      private String[] H00JV12_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00JV12_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] H00JV12_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00JV12_A685ContagemResultado_PFLFSUltima ;
      private decimal[] H00JV12_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00JV12_A683ContagemResultado_PFLFMUltima ;
      private long[] H00JV13_A1797LogResponsavel_Codigo ;
      private int[] H00JV13_A896LogResponsavel_Owner ;
      private int[] H00JV13_A892LogResponsavel_DemandaCod ;
      private bool[] H00JV13_n892LogResponsavel_DemandaCod ;
      private int[] H00JV18_A1686Proposta_OSCodigo ;
      private String[] H00JV18_A484ContagemResultado_StatusDmn ;
      private bool[] H00JV18_n484ContagemResultado_StatusDmn ;
      private int[] H00JV18_A1685Proposta_Codigo ;
      private DateTime[] H00JV18_A1702Proposta_Prazo ;
      private decimal[] H00JV18_A1688Proposta_Valor ;
      private int[] H00JV18_A1703Proposta_Esforco ;
      private int[] H00JV18_A1715Proposta_OSCntCod ;
      private bool[] H00JV18_n1715Proposta_OSCntCod ;
      private int[] H00JV18_A1705Proposta_OSServico ;
      private bool[] H00JV18_n1705Proposta_OSServico ;
      private int[] H00JV18_A1704Proposta_CntSrvCod ;
      private String[] H00JV18_A1764Proposta_OSDmn ;
      private bool[] H00JV18_n1764Proposta_OSDmn ;
      private String[] H00JV18_A1690Proposta_Objetivo ;
      private int[] H00JV18_A490ContagemResultado_ContratadaCod ;
      private bool[] H00JV18_n490ContagemResultado_ContratadaCod ;
      private DateTime[] H00JV18_A472ContagemResultado_DataEntrega ;
      private bool[] H00JV18_n472ContagemResultado_DataEntrega ;
      private int[] H00JV18_A602ContagemResultado_OSVinculada ;
      private bool[] H00JV18_n602ContagemResultado_OSVinculada ;
      private int[] H00JV18_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00JV18_n805ContagemResultado_ContratadaOrigemCod ;
      private String[] H00JV18_A1763Proposta_OSCntSrvFtrm ;
      private bool[] H00JV18_n1763Proposta_OSCntSrvFtrm ;
      private String[] H00JV18_A1707Proposta_OSPrzTpDias ;
      private bool[] H00JV18_n1707Proposta_OSPrzTpDias ;
      private decimal[] H00JV18_A1717Proposta_OSPFB ;
      private bool[] H00JV18_n1717Proposta_OSPFB ;
      private decimal[] H00JV18_A1718Proposta_OSPFL ;
      private bool[] H00JV18_n1718Proposta_OSPFL ;
      private DateTime[] H00JV18_A1719Proposta_OSDataCnt ;
      private bool[] H00JV18_n1719Proposta_OSDataCnt ;
      private String[] H00JV18_A1720Proposta_OSHoraCnt ;
      private bool[] H00JV18_n1720Proposta_OSHoraCnt ;
      private int[] H00JV20_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] H00JV20_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00JV20_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] H00JV20_A456ContagemResultado_Codigo ;
      private bool[] H00JV20_A1121NaoConformidade_EhImpeditiva ;
      private bool[] H00JV20_n1121NaoConformidade_EhImpeditiva ;
      private String[] H00JV20_A516Contratada_TipoFabrica ;
      private bool[] H00JV20_n516Contratada_TipoFabrica ;
      private String[] H00JV20_A484ContagemResultado_StatusDmn ;
      private bool[] H00JV20_n484ContagemResultado_StatusDmn ;
      private int[] H00JV20_A1603ContagemResultado_CntCod ;
      private bool[] H00JV20_n1603ContagemResultado_CntCod ;
      private String[] H00JV20_A457ContagemResultado_Demanda ;
      private bool[] H00JV20_n457ContagemResultado_Demanda ;
      private decimal[] H00JV20_A512ContagemResultado_ValorPF ;
      private bool[] H00JV20_n512ContagemResultado_ValorPF ;
      private DateTime[] H00JV20_A472ContagemResultado_DataEntrega ;
      private bool[] H00JV20_n472ContagemResultado_DataEntrega ;
      private bool[] H00JV20_A1714ContagemResultado_Combinada ;
      private bool[] H00JV20_n1714ContagemResultado_Combinada ;
      private int[] H00JV20_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00JV20_n1553ContagemResultado_CntSrvCod ;
      private int[] H00JV20_A490ContagemResultado_ContratadaCod ;
      private bool[] H00JV20_n490ContagemResultado_ContratadaCod ;
      private int[] H00JV20_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00JV20_n805ContagemResultado_ContratadaOrigemCod ;
      private String[] H00JV20_A1594ContagemResultado_CntSrvFtrm ;
      private bool[] H00JV20_n1594ContagemResultado_CntSrvFtrm ;
      private String[] H00JV20_A1611ContagemResultado_PrzTpDias ;
      private bool[] H00JV20_n1611ContagemResultado_PrzTpDias ;
      private String[] H00JV20_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00JV20_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] H00JV20_A1613ContagemResultado_CntSrvQtdUntCns ;
      private bool[] H00JV20_n1613ContagemResultado_CntSrvQtdUntCns ;
      private decimal[] H00JV20_A798ContagemResultado_PFBFSImp ;
      private bool[] H00JV20_n798ContagemResultado_PFBFSImp ;
      private decimal[] H00JV20_A799ContagemResultado_PFLFSImp ;
      private bool[] H00JV20_n799ContagemResultado_PFLFSImp ;
      private int[] H00JV20_A601ContagemResultado_Servico ;
      private bool[] H00JV20_n601ContagemResultado_Servico ;
      private int[] H00JV20_A602ContagemResultado_OSVinculada ;
      private bool[] H00JV20_n602ContagemResultado_OSVinculada ;
      private int[] H00JV20_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00JV20_n52Contratada_AreaTrabalhoCod ;
      private String[] H00JV20_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] H00JV20_n1593ContagemResultado_CntSrvTpVnc ;
      private int[] H00JV20_A508ContagemResultado_Owner ;
      private String[] H00JV20_A1610ContagemResultado_PrzTp ;
      private bool[] H00JV20_n1610ContagemResultado_PrzTp ;
      private DateTime[] H00JV20_A566ContagemResultado_DataUltCnt ;
      private String[] H00JV20_A825ContagemResultado_HoraUltCnt ;
      private decimal[] H00JV20_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00JV20_A685ContagemResultado_PFLFSUltima ;
      private decimal[] H00JV20_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00JV20_A683ContagemResultado_PFLFMUltima ;
      private int[] H00JV21_A2024ContagemResultadoNaoCnf_Codigo ;
      private String[] H00JV22_A1130LogResponsavel_Status ;
      private bool[] H00JV22_n1130LogResponsavel_Status ;
      private String[] H00JV22_A894LogResponsavel_Acao ;
      private long[] H00JV22_A1797LogResponsavel_Codigo ;
      private int[] H00JV22_A896LogResponsavel_Owner ;
      private int[] H00JV22_A892LogResponsavel_DemandaCod ;
      private bool[] H00JV22_n892LogResponsavel_DemandaCod ;
      private int[] H00JV23_A155Servico_Codigo ;
      private int[] H00JV23_A74Contrato_Codigo ;
      private int[] H00JV23_A160ContratoServicos_Codigo ;
      private String[] H00JV23_A605Servico_Sigla ;
      private int[] H00JV24_A74Contrato_Codigo ;
      private int[] H00JV24_A160ContratoServicos_Codigo ;
      private decimal[] H00JV24_A557Servico_VlrUnidadeContratada ;
      private decimal[] H00JV24_A116Contrato_ValorUnidadeContratacao ;
      private int[] H00JV25_A1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private bool[] H00JV25_n1589ContratoSrvVnc_SrvVncCntSrvCod ;
      private int[] H00JV25_A923ContratoSrvVnc_ServicoVncCod ;
      private bool[] H00JV25_n923ContratoSrvVnc_ServicoVncCod ;
      private bool[] H00JV25_A1453ContratoServicosVnc_Ativo ;
      private bool[] H00JV25_n1453ContratoServicosVnc_Ativo ;
      private String[] H00JV25_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] H00JV25_n1084ContratoSrvVnc_StatusDmn ;
      private String[] H00JV25_A1800ContratoSrvVnc_DoStatusDmn ;
      private bool[] H00JV25_n1800ContratoSrvVnc_DoStatusDmn ;
      private int[] H00JV25_A915ContratoSrvVnc_CntSrvCod ;
      private String[] H00JV25_A924ContratoSrvVnc_ServicoVncSigla ;
      private bool[] H00JV25_n924ContratoSrvVnc_ServicoVncSigla ;
      private int[] H00JV25_A917ContratoSrvVnc_Codigo ;
      private int[] H00JV26_A892LogResponsavel_DemandaCod ;
      private bool[] H00JV26_n892LogResponsavel_DemandaCod ;
      private String[] H00JV26_A1234LogResponsavel_NovoStatus ;
      private bool[] H00JV26_n1234LogResponsavel_NovoStatus ;
      private long[] H00JV26_A1797LogResponsavel_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV49Websession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV51Codigos ;
      private SdtContagemResultado AV90ContagemResultado ;
      private SdtProposta AV65Proposta ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class wp_re__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00JV2 ;
          prmH00JV2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Userehadministrador",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JV3 ;
          prmH00JV3 = new Object[] {
          } ;
          Object[] prmH00JV4 ;
          prmH00JV4 = new Object[] {
          new Object[] {"@AV45Prestadora",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JV5 ;
          prmH00JV5 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JV6 ;
          prmH00JV6 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JV7 ;
          prmH00JV7 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JV8 ;
          prmH00JV8 = new Object[] {
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JV10 ;
          prmH00JV10 = new Object[] {
          new Object[] {"@AV24OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV45Prestadora",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JV12 ;
          prmH00JV12 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV45Prestadora",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00JV12 ;
          cmdBufferH00JV12=" SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_ServicoSS], T1.[ContagemResultado_StatusDmn], T2.[ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T3.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, COALESCE( T4.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T4.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T4.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T4.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_OSVinculada] = @ContagemResultado_Codigo) AND (T1.[ContagemResultado_ContratadaCod] <> @AV45Prestadora) AND (T2.[ContratoServicos_TipoVnc] = 'C' or T2.[ContratoServicos_TipoVnc] = 'A') AND (Not ( T1.[ContagemResultado_StatusDmn] = 'X' or T1.[ContagemResultado_StatusDmn] "
          + " = 'H' or T1.[ContagemResultado_StatusDmn] = 'O' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L')) AND (T1.[ContagemResultado_ServicoSS] IS NULL) ORDER BY T1.[ContagemResultado_OSVinculada]" ;
          Object[] prmH00JV13 ;
          prmH00JV13 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JV18 ;
          prmH00JV18 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00JV18 ;
          cmdBufferH00JV18=" SELECT TOP 1 T1.[Proposta_OSCodigo] AS Proposta_OSCodigo, T2.[ContagemResultado_StatusDmn], T1.[Proposta_Codigo], T1.[Proposta_Prazo], T1.[Proposta_Valor], T1.[Proposta_Esforco], T3.[Contrato_Codigo] AS Proposta_OSCntCod, T2.[ContagemResultado_CntSrvCod] AS Proposta_OSServico, T1.[Proposta_CntSrvCod], T2.[ContagemResultado_Demanda] AS Proposta_OSDmn, T1.[Proposta_Objetivo], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[ContagemResultado_DataEntrega], T2.[ContagemResultado_OSVinculada], T2.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, T3.[ServicoContrato_Faturamento] AS Proposta_OSCntSrvFtrm, T3.[ContratoServicos_PrazoTpDias] AS Proposta_OSPrzTpDias, COALESCE( T4.[ContagemResultado_PFBFMUltima], 0) AS Proposta_OSPFB, COALESCE( T5.[ContagemResultado_PFLFMUltima], 0) AS Proposta_OSPFL, COALESCE( T6.[Proposta_OSDataCnt], convert( DATETIME, '17530101', 112 )) AS Proposta_OSDataCnt, COALESCE( T7.[Proposta_OSHoraCnt], '') AS Proposta_OSHoraCnt FROM ((((((dbo.[Proposta] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[Proposta_OSCodigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[Proposta_OSCodigo]) LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] "
          + " ) T5 ON T5.[ContagemResultado_Codigo] = T1.[Proposta_OSCodigo]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS Proposta_OSDataCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T6 ON T6.[ContagemResultado_Codigo] = T1.[Proposta_OSCodigo]) LEFT JOIN (SELECT MIN([ContagemResultado_HoraCnt]) AS Proposta_OSHoraCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[Proposta_OSCodigo]) WHERE T1.[Proposta_OSCodigo] = @ContagemResultado_Codigo ORDER BY T1.[Proposta_OSCodigo]" ;
          Object[] prmH00JV20 ;
          prmH00JV20 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00JV20 ;
          cmdBufferH00JV20=" SELECT TOP 1 T4.[ContratoServicosPrazo_CntSrvCod], T1.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, T1.[ContagemResultado_Codigo], T2.[NaoConformidade_EhImpeditiva], T6.[Contratada_TipoFabrica], T1.[ContagemResultado_StatusDmn], T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Demanda], T1.[ContagemResultado_ValorPF], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_Combinada], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, T3.[ServicoContrato_Faturamento] AS ContagemResultado_CntSrvFtrm, T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T5.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T3.[ContratoServicos_QntUntCns] AS ContagemResultado_CntSrvQtdUntCns, T1.[ContagemResultado_PFBFSImp], T1.[ContagemResultado_PFLFSImp], T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_OSVinculada], T5.[Contratada_AreaTrabalhoCod], T3.[ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc, T1.[ContagemResultado_Owner], COALESCE( T4.[ContratoServicosPrazo_Tipo], '') AS ContagemResultado_PrzTp, COALESCE( T7.[Proposta_OSDataCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T7.[Proposta_OSHoraCnt], '') AS ContagemResultado_HoraUltCnt, COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima FROM (((((([ContagemResultado] "
          + " T1 WITH (NOLOCK) LEFT JOIN [NaoConformidade] T2 WITH (NOLOCK) ON T2.[NaoConformidade_Codigo] = T1.[ContagemResultado_NaoCnfDmnCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [ContratoServicosPrazo] T4 WITH (NOLOCK) ON T4.[ContratoServicosPrazo_CntSrvCod] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS Proposta_OSDataCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_HoraCnt]) AS Proposta_OSHoraCnt, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo]" ;
          Object[] prmH00JV21 ;
          prmH00JV21 = new Object[] {
          } ;
          Object[] prmH00JV22 ;
          prmH00JV22 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JV23 ;
          prmH00JV23 = new Object[] {
          new Object[] {"@AV53ContratoOrigem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JV24 ;
          prmH00JV24 = new Object[] {
          new Object[] {"@AV67ServicoAExecutar",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00JV25 ;
          prmH00JV25 = new Object[] {
          new Object[] {"@AV71CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV89StatusAntDmn",SqlDbType.Char,1,0}
          } ;
          Object[] prmH00JV26 ;
          prmH00JV26 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00JV2", "SELECT T4.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T2.[Contratada_TipoFabrica] FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE ( @AV9WWPCo_1Userehadministrador = 0 and T1.[ContratadaUsuario_ContratadaCod] = @AV9WWPCo_2Contratada_codigo) or ( @AV9WWPCo_1Userehadministrador = 1 and T2.[Contratada_TipoFabrica] = 'M') ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV2,0,0,true,false )
             ,new CursorDef("H00JV3", "SELECT [Contrato_Codigo], [Contrato_Numero] FROM [Contrato] WITH (NOLOCK) ORDER BY [Contrato_Numero] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV3,0,0,true,false )
             ,new CursorDef("H00JV4", "SELECT T3.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContratadaUsuario_ContratadaCod] = @AV45Prestadora ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV4,0,0,true,false )
             ,new CursorDef("H00JV5", "SELECT T1.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, T1.[ContagemResultado_Codigo], T2.[Contratada_TipoFabrica] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV5,1,0,true,true )
             ,new CursorDef("H00JV6", "SELECT T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T1.[LogResponsavel_Owner], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[LogResponsavel_Codigo] FROM ([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) WHERE T1.[LogResponsavel_DemandaCod] = @ContagemResultado_Codigo ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV6,100,0,true,false )
             ,new CursorDef("H00JV7", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @ContagemResultado_ContratadaCod and [ContratadaUsuario_UsuarioCod] = @LogResponsavel_Owner ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV7,1,0,false,true )
             ,new CursorDef("H00JV8", "SELECT TOP 1 [Contratada_Codigo], [Contratada_TipoFabrica] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @AV9WWPCo_2Contratada_codigo ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV8,1,0,false,true )
             ,new CursorDef("H00JV10", "SELECT TOP 1 T1.[ContagemResultado_ServicoSS], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T2.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, COALESCE( T3.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T3.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T3.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T3.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_Codigo] = @AV24OSVinculada) AND (T1.[ContagemResultado_ContratadaCod] <> @AV45Prestadora) AND (Not ( T1.[ContagemResultado_StatusDmn] = 'D' or T1.[ContagemResultado_StatusDmn] = 'H' or T1.[ContagemResultado_StatusDmn] = 'O' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L')) AND (T1.[ContagemResultado_ServicoSS] IS NULL) ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV10,1,0,false,true )
             ,new CursorDef("H00JV12", cmdBufferH00JV12,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV12,1,0,false,true )
             ,new CursorDef("H00JV13", "SELECT [LogResponsavel_Codigo], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @ContagemResultado_Codigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV13,100,0,true,false )
             ,new CursorDef("H00JV18", cmdBufferH00JV18,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV18,1,0,true,true )
             ,new CursorDef("H00JV20", cmdBufferH00JV20,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV20,1,0,true,true )
             ,new CursorDef("H00JV21", "SELECT [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) ORDER BY [ContagemResultadoNaoCnf_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV21,100,0,false,false )
             ,new CursorDef("H00JV22", "SELECT [LogResponsavel_Status], [LogResponsavel_Acao], [LogResponsavel_Codigo], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @ContagemResultado_Codigo) AND ([LogResponsavel_Status] <> 'D') AND ([LogResponsavel_Acao] = 'R') ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV22,100,0,true,false )
             ,new CursorDef("H00JV23", "SELECT T1.[Servico_Codigo], T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T2.[Servico_Sigla] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE T1.[Contrato_Codigo] = @AV53ContratoOrigem_Codigo ORDER BY T2.[Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV23,100,0,false,false )
             ,new CursorDef("H00JV24", "SELECT TOP 1 T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[Servico_VlrUnidadeContratada], T2.[Contrato_ValorUnidadeContratacao] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV67ServicoAExecutar ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV24,1,0,false,true )
             ,new CursorDef("H00JV25", "SELECT T1.[ContratoSrvVnc_SrvVncCntSrvCod] AS ContratoSrvVnc_SrvVncCntSrvCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoVncCod, T1.[ContratoServicosVnc_Ativo], T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoSrvVnc_DoStatusDmn], T1.[ContratoSrvVnc_CntSrvCod], T3.[Servico_Sigla] AS ContratoSrvVnc_ServicoVncSigla, T1.[ContratoSrvVnc_Codigo] FROM (([ContratoServicosVnc] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_SrvVncCntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) WHERE (T1.[ContratoSrvVnc_CntSrvCod] = @AV71CntSrvCod) AND (T1.[ContratoServicosVnc_Ativo] = 1) AND (T1.[ContratoSrvVnc_DoStatusDmn] = @AV89StatusAntDmn) AND (T1.[ContratoSrvVnc_StatusDmn] = 'R') ORDER BY T1.[ContratoSrvVnc_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV25,100,0,false,false )
             ,new CursorDef("H00JV26", "SELECT [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, [LogResponsavel_NovoStatus], [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @ContagemResultado_Codigo ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JV26,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((long[]) buf[5])[0] = rslt.getLong(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(9) ;
                ((decimal[]) buf[16])[0] = rslt.getDecimal(10) ;
                ((decimal[]) buf[17])[0] = rslt.getDecimal(11) ;
                ((decimal[]) buf[18])[0] = rslt.getDecimal(12) ;
                return;
             case 9 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(11) ;
                ((int[]) buf[15])[0] = rslt.getInt(12) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(13) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((int[]) buf[19])[0] = rslt.getInt(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((int[]) buf[21])[0] = rslt.getInt(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((String[]) buf[23])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((String[]) buf[25])[0] = rslt.getString(17, 1) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((decimal[]) buf[27])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(18);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(19);
                ((DateTime[]) buf[31])[0] = rslt.getGXDate(20) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(20);
                ((String[]) buf[33])[0] = rslt.getString(21, 5) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(21);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((bool[]) buf[18])[0] = rslt.getBool(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getString(17, 1) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((decimal[]) buf[36])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((int[]) buf[38])[0] = rslt.getInt(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((int[]) buf[40])[0] = rslt.getInt(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((int[]) buf[42])[0] = rslt.getInt(23) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((String[]) buf[44])[0] = rslt.getString(24, 1) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((int[]) buf[46])[0] = rslt.getInt(25) ;
                ((String[]) buf[47])[0] = rslt.getString(26, 20) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(26);
                ((DateTime[]) buf[49])[0] = rslt.getGXDate(27) ;
                ((String[]) buf[50])[0] = rslt.getString(28, 5) ;
                ((decimal[]) buf[51])[0] = rslt.getDecimal(29) ;
                ((decimal[]) buf[52])[0] = rslt.getDecimal(30) ;
                ((decimal[]) buf[53])[0] = rslt.getDecimal(31) ;
                ((decimal[]) buf[54])[0] = rslt.getDecimal(32) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((long[]) buf[3])[0] = rslt.getLong(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((long[]) buf[4])[0] = rslt.getLong(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
