/*
               File: GetPromptServicoAnsFilterData
        Description: Get Prompt Servico Ans Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:50.76
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptservicoansfilterdata : GXProcedure
   {
      public getpromptservicoansfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptservicoansfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptservicoansfilterdata objgetpromptservicoansfilterdata;
         objgetpromptservicoansfilterdata = new getpromptservicoansfilterdata();
         objgetpromptservicoansfilterdata.AV16DDOName = aP0_DDOName;
         objgetpromptservicoansfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetpromptservicoansfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptservicoansfilterdata.AV20OptionsJson = "" ;
         objgetpromptservicoansfilterdata.AV23OptionsDescJson = "" ;
         objgetpromptservicoansfilterdata.AV25OptionIndexesJson = "" ;
         objgetpromptservicoansfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptservicoansfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptservicoansfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptservicoansfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_SERVICO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_SERVICOANS_ITEM") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICOANS_ITEMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("PromptServicoAnsGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptServicoAnsGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("PromptServicoAnsGridState"), "");
         }
         AV48GXV1 = 1;
         while ( AV48GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV48GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME") == 0 )
            {
               AV10TFServico_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME_SEL") == 0 )
            {
               AV11TFServico_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSERVICOANS_ITEM") == 0 )
            {
               AV12TFServicoAns_Item = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSERVICOANS_ITEM_SEL") == 0 )
            {
               AV13TFServicoAns_Item_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            AV48GXV1 = (int)(AV48GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "SERVICOANS_ITEM") == 0 )
            {
               AV33DynamicFiltersOperator1 = AV31GridStateDynamicFilter.gxTpr_Operator;
               AV34ServicoAns_item1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
            {
               AV35Servico_Nome1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV36DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV37DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "SERVICOANS_ITEM") == 0 )
               {
                  AV38DynamicFiltersOperator2 = AV31GridStateDynamicFilter.gxTpr_Operator;
                  AV39ServicoAns_item2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
               {
                  AV40Servico_Nome2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV41DynamicFiltersEnabled3 = true;
                  AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(3));
                  AV42DynamicFiltersSelector3 = AV31GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SERVICOANS_ITEM") == 0 )
                  {
                     AV43DynamicFiltersOperator3 = AV31GridStateDynamicFilter.gxTpr_Operator;
                     AV44ServicoAns_item3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SERVICO_NOME") == 0 )
                  {
                     AV45Servico_Nome3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSERVICO_NOMEOPTIONS' Routine */
         AV10TFServico_Nome = AV14SearchTxt;
         AV11TFServico_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV32DynamicFiltersSelector1 ,
                                              AV33DynamicFiltersOperator1 ,
                                              AV34ServicoAns_item1 ,
                                              AV35Servico_Nome1 ,
                                              AV36DynamicFiltersEnabled2 ,
                                              AV37DynamicFiltersSelector2 ,
                                              AV38DynamicFiltersOperator2 ,
                                              AV39ServicoAns_item2 ,
                                              AV40Servico_Nome2 ,
                                              AV41DynamicFiltersEnabled3 ,
                                              AV42DynamicFiltersSelector3 ,
                                              AV43DynamicFiltersOperator3 ,
                                              AV44ServicoAns_item3 ,
                                              AV45Servico_Nome3 ,
                                              AV11TFServico_Nome_Sel ,
                                              AV10TFServico_Nome ,
                                              AV13TFServicoAns_Item_Sel ,
                                              AV12TFServicoAns_Item ,
                                              A320ServicoAns_Item ,
                                              A608Servico_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV34ServicoAns_item1 = StringUtil.Concat( StringUtil.RTrim( AV34ServicoAns_item1), "%", "");
         lV34ServicoAns_item1 = StringUtil.Concat( StringUtil.RTrim( AV34ServicoAns_item1), "%", "");
         lV39ServicoAns_item2 = StringUtil.Concat( StringUtil.RTrim( AV39ServicoAns_item2), "%", "");
         lV39ServicoAns_item2 = StringUtil.Concat( StringUtil.RTrim( AV39ServicoAns_item2), "%", "");
         lV44ServicoAns_item3 = StringUtil.Concat( StringUtil.RTrim( AV44ServicoAns_item3), "%", "");
         lV44ServicoAns_item3 = StringUtil.Concat( StringUtil.RTrim( AV44ServicoAns_item3), "%", "");
         lV10TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFServico_Nome), 50, "%");
         lV12TFServicoAns_Item = StringUtil.Concat( StringUtil.RTrim( AV12TFServicoAns_Item), "%", "");
         /* Using cursor P00KM2 */
         pr_default.execute(0, new Object[] {lV34ServicoAns_item1, lV34ServicoAns_item1, AV35Servico_Nome1, lV39ServicoAns_item2, lV39ServicoAns_item2, AV40Servico_Nome2, lV44ServicoAns_item3, lV44ServicoAns_item3, AV45Servico_Nome3, lV10TFServico_Nome, AV11TFServico_Nome_Sel, lV12TFServicoAns_Item, AV13TFServicoAns_Item_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKKM2 = false;
            A155Servico_Codigo = P00KM2_A155Servico_Codigo[0];
            A608Servico_Nome = P00KM2_A608Servico_Nome[0];
            A320ServicoAns_Item = P00KM2_A320ServicoAns_Item[0];
            A319ServicoAns_Codigo = P00KM2_A319ServicoAns_Codigo[0];
            A608Servico_Nome = P00KM2_A608Servico_Nome[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00KM2_A155Servico_Codigo[0] == A155Servico_Codigo ) )
            {
               BRKKM2 = false;
               A319ServicoAns_Codigo = P00KM2_A319ServicoAns_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKKM2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A608Servico_Nome)) )
            {
               AV18Option = A608Servico_Nome;
               AV17InsertIndex = 1;
               while ( ( AV17InsertIndex <= AV19Options.Count ) && ( StringUtil.StrCmp(((String)AV19Options.Item(AV17InsertIndex)), AV18Option) < 0 ) )
               {
                  AV17InsertIndex = (int)(AV17InsertIndex+1);
               }
               AV19Options.Add(AV18Option, AV17InsertIndex);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), AV17InsertIndex);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKM2 )
            {
               BRKKM2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSERVICOANS_ITEMOPTIONS' Routine */
         AV12TFServicoAns_Item = AV14SearchTxt;
         AV13TFServicoAns_Item_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV32DynamicFiltersSelector1 ,
                                              AV33DynamicFiltersOperator1 ,
                                              AV34ServicoAns_item1 ,
                                              AV35Servico_Nome1 ,
                                              AV36DynamicFiltersEnabled2 ,
                                              AV37DynamicFiltersSelector2 ,
                                              AV38DynamicFiltersOperator2 ,
                                              AV39ServicoAns_item2 ,
                                              AV40Servico_Nome2 ,
                                              AV41DynamicFiltersEnabled3 ,
                                              AV42DynamicFiltersSelector3 ,
                                              AV43DynamicFiltersOperator3 ,
                                              AV44ServicoAns_item3 ,
                                              AV45Servico_Nome3 ,
                                              AV11TFServico_Nome_Sel ,
                                              AV10TFServico_Nome ,
                                              AV13TFServicoAns_Item_Sel ,
                                              AV12TFServicoAns_Item ,
                                              A320ServicoAns_Item ,
                                              A608Servico_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV34ServicoAns_item1 = StringUtil.Concat( StringUtil.RTrim( AV34ServicoAns_item1), "%", "");
         lV34ServicoAns_item1 = StringUtil.Concat( StringUtil.RTrim( AV34ServicoAns_item1), "%", "");
         lV39ServicoAns_item2 = StringUtil.Concat( StringUtil.RTrim( AV39ServicoAns_item2), "%", "");
         lV39ServicoAns_item2 = StringUtil.Concat( StringUtil.RTrim( AV39ServicoAns_item2), "%", "");
         lV44ServicoAns_item3 = StringUtil.Concat( StringUtil.RTrim( AV44ServicoAns_item3), "%", "");
         lV44ServicoAns_item3 = StringUtil.Concat( StringUtil.RTrim( AV44ServicoAns_item3), "%", "");
         lV10TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFServico_Nome), 50, "%");
         lV12TFServicoAns_Item = StringUtil.Concat( StringUtil.RTrim( AV12TFServicoAns_Item), "%", "");
         /* Using cursor P00KM3 */
         pr_default.execute(1, new Object[] {lV34ServicoAns_item1, lV34ServicoAns_item1, AV35Servico_Nome1, lV39ServicoAns_item2, lV39ServicoAns_item2, AV40Servico_Nome2, lV44ServicoAns_item3, lV44ServicoAns_item3, AV45Servico_Nome3, lV10TFServico_Nome, AV11TFServico_Nome_Sel, lV12TFServicoAns_Item, AV13TFServicoAns_Item_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKKM4 = false;
            A155Servico_Codigo = P00KM3_A155Servico_Codigo[0];
            A320ServicoAns_Item = P00KM3_A320ServicoAns_Item[0];
            A608Servico_Nome = P00KM3_A608Servico_Nome[0];
            A319ServicoAns_Codigo = P00KM3_A319ServicoAns_Codigo[0];
            A608Servico_Nome = P00KM3_A608Servico_Nome[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00KM3_A320ServicoAns_Item[0], A320ServicoAns_Item) == 0 ) )
            {
               BRKKM4 = false;
               A319ServicoAns_Codigo = P00KM3_A319ServicoAns_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKKM4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A320ServicoAns_Item)) )
            {
               AV18Option = A320ServicoAns_Item;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKM4 )
            {
               BRKKM4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFServico_Nome = "";
         AV11TFServico_Nome_Sel = "";
         AV12TFServicoAns_Item = "";
         AV13TFServicoAns_Item_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV34ServicoAns_item1 = "";
         AV35Servico_Nome1 = "";
         AV37DynamicFiltersSelector2 = "";
         AV39ServicoAns_item2 = "";
         AV40Servico_Nome2 = "";
         AV42DynamicFiltersSelector3 = "";
         AV44ServicoAns_item3 = "";
         AV45Servico_Nome3 = "";
         scmdbuf = "";
         lV34ServicoAns_item1 = "";
         lV39ServicoAns_item2 = "";
         lV44ServicoAns_item3 = "";
         lV10TFServico_Nome = "";
         lV12TFServicoAns_Item = "";
         A320ServicoAns_Item = "";
         A608Servico_Nome = "";
         P00KM2_A155Servico_Codigo = new int[1] ;
         P00KM2_A608Servico_Nome = new String[] {""} ;
         P00KM2_A320ServicoAns_Item = new String[] {""} ;
         P00KM2_A319ServicoAns_Codigo = new int[1] ;
         AV18Option = "";
         P00KM3_A155Servico_Codigo = new int[1] ;
         P00KM3_A320ServicoAns_Item = new String[] {""} ;
         P00KM3_A608Servico_Nome = new String[] {""} ;
         P00KM3_A319ServicoAns_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptservicoansfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00KM2_A155Servico_Codigo, P00KM2_A608Servico_Nome, P00KM2_A320ServicoAns_Item, P00KM2_A319ServicoAns_Codigo
               }
               , new Object[] {
               P00KM3_A155Servico_Codigo, P00KM3_A320ServicoAns_Item, P00KM3_A608Servico_Nome, P00KM3_A319ServicoAns_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV33DynamicFiltersOperator1 ;
      private short AV38DynamicFiltersOperator2 ;
      private short AV43DynamicFiltersOperator3 ;
      private int AV48GXV1 ;
      private int A155Servico_Codigo ;
      private int A319ServicoAns_Codigo ;
      private int AV17InsertIndex ;
      private long AV26count ;
      private String AV10TFServico_Nome ;
      private String AV11TFServico_Nome_Sel ;
      private String AV35Servico_Nome1 ;
      private String AV40Servico_Nome2 ;
      private String AV45Servico_Nome3 ;
      private String scmdbuf ;
      private String lV10TFServico_Nome ;
      private String A608Servico_Nome ;
      private bool returnInSub ;
      private bool AV36DynamicFiltersEnabled2 ;
      private bool AV41DynamicFiltersEnabled3 ;
      private bool BRKKM2 ;
      private bool BRKKM4 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV12TFServicoAns_Item ;
      private String AV13TFServicoAns_Item_Sel ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV34ServicoAns_item1 ;
      private String AV37DynamicFiltersSelector2 ;
      private String AV39ServicoAns_item2 ;
      private String AV42DynamicFiltersSelector3 ;
      private String AV44ServicoAns_item3 ;
      private String lV34ServicoAns_item1 ;
      private String lV39ServicoAns_item2 ;
      private String lV44ServicoAns_item3 ;
      private String lV12TFServicoAns_Item ;
      private String A320ServicoAns_Item ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00KM2_A155Servico_Codigo ;
      private String[] P00KM2_A608Servico_Nome ;
      private String[] P00KM2_A320ServicoAns_Item ;
      private int[] P00KM2_A319ServicoAns_Codigo ;
      private int[] P00KM3_A155Servico_Codigo ;
      private String[] P00KM3_A320ServicoAns_Item ;
      private String[] P00KM3_A608Servico_Nome ;
      private int[] P00KM3_A319ServicoAns_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getpromptservicoansfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00KM2( IGxContext context ,
                                             String AV32DynamicFiltersSelector1 ,
                                             short AV33DynamicFiltersOperator1 ,
                                             String AV34ServicoAns_item1 ,
                                             String AV35Servico_Nome1 ,
                                             bool AV36DynamicFiltersEnabled2 ,
                                             String AV37DynamicFiltersSelector2 ,
                                             short AV38DynamicFiltersOperator2 ,
                                             String AV39ServicoAns_item2 ,
                                             String AV40Servico_Nome2 ,
                                             bool AV41DynamicFiltersEnabled3 ,
                                             String AV42DynamicFiltersSelector3 ,
                                             short AV43DynamicFiltersOperator3 ,
                                             String AV44ServicoAns_item3 ,
                                             String AV45Servico_Nome3 ,
                                             String AV11TFServico_Nome_Sel ,
                                             String AV10TFServico_Nome ,
                                             String AV13TFServicoAns_Item_Sel ,
                                             String AV12TFServicoAns_Item ,
                                             String A320ServicoAns_Item ,
                                             String A608Servico_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [13] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Servico_Codigo], T2.[Servico_Nome], T1.[ServicoAns_Item], T1.[ServicoAns_Codigo] FROM ([ServicoAns] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "SERVICOANS_ITEM") == 0 ) && ( AV33DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ServicoAns_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV34ServicoAns_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV34ServicoAns_item1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "SERVICOANS_ITEM") == 0 ) && ( AV33DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ServicoAns_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like '%' + @lV34ServicoAns_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like '%' + @lV34ServicoAns_item1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Servico_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV35Servico_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV35Servico_Nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "SERVICOANS_ITEM") == 0 ) && ( AV38DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ServicoAns_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV39ServicoAns_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV39ServicoAns_item2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "SERVICOANS_ITEM") == 0 ) && ( AV38DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ServicoAns_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like '%' + @lV39ServicoAns_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like '%' + @lV39ServicoAns_item2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Servico_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV40Servico_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV40Servico_Nome2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SERVICOANS_ITEM") == 0 ) && ( AV43DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ServicoAns_item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV44ServicoAns_item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV44ServicoAns_item3)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SERVICOANS_ITEM") == 0 ) && ( AV43DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ServicoAns_item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like '%' + @lV44ServicoAns_item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like '%' + @lV44ServicoAns_item3)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Servico_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV45Servico_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV45Servico_Nome3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServico_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like @lV10TFServico_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like @lV10TFServico_Nome)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV11TFServico_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV11TFServico_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServicoAns_Item_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFServicoAns_Item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV12TFServicoAns_Item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV12TFServicoAns_Item)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServicoAns_Item_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] = @AV13TFServicoAns_Item_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] = @AV13TFServicoAns_Item_Sel)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Servico_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00KM3( IGxContext context ,
                                             String AV32DynamicFiltersSelector1 ,
                                             short AV33DynamicFiltersOperator1 ,
                                             String AV34ServicoAns_item1 ,
                                             String AV35Servico_Nome1 ,
                                             bool AV36DynamicFiltersEnabled2 ,
                                             String AV37DynamicFiltersSelector2 ,
                                             short AV38DynamicFiltersOperator2 ,
                                             String AV39ServicoAns_item2 ,
                                             String AV40Servico_Nome2 ,
                                             bool AV41DynamicFiltersEnabled3 ,
                                             String AV42DynamicFiltersSelector3 ,
                                             short AV43DynamicFiltersOperator3 ,
                                             String AV44ServicoAns_item3 ,
                                             String AV45Servico_Nome3 ,
                                             String AV11TFServico_Nome_Sel ,
                                             String AV10TFServico_Nome ,
                                             String AV13TFServicoAns_Item_Sel ,
                                             String AV12TFServicoAns_Item ,
                                             String A320ServicoAns_Item ,
                                             String A608Servico_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Servico_Codigo], T1.[ServicoAns_Item], T2.[Servico_Nome], T1.[ServicoAns_Codigo] FROM ([ServicoAns] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "SERVICOANS_ITEM") == 0 ) && ( AV33DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ServicoAns_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV34ServicoAns_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV34ServicoAns_item1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "SERVICOANS_ITEM") == 0 ) && ( AV33DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ServicoAns_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like '%' + @lV34ServicoAns_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like '%' + @lV34ServicoAns_item1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Servico_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV35Servico_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV35Servico_Nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "SERVICOANS_ITEM") == 0 ) && ( AV38DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ServicoAns_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV39ServicoAns_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV39ServicoAns_item2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "SERVICOANS_ITEM") == 0 ) && ( AV38DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ServicoAns_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like '%' + @lV39ServicoAns_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like '%' + @lV39ServicoAns_item2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Servico_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV40Servico_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV40Servico_Nome2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SERVICOANS_ITEM") == 0 ) && ( AV43DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ServicoAns_item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV44ServicoAns_item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV44ServicoAns_item3)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SERVICOANS_ITEM") == 0 ) && ( AV43DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ServicoAns_item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like '%' + @lV44ServicoAns_item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like '%' + @lV44ServicoAns_item3)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45Servico_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV45Servico_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV45Servico_Nome3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServico_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like @lV10TFServico_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like @lV10TFServico_Nome)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV11TFServico_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV11TFServico_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServicoAns_Item_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFServicoAns_Item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV12TFServicoAns_Item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV12TFServicoAns_Item)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServicoAns_Item_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] = @AV13TFServicoAns_Item_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] = @AV13TFServicoAns_Item_Sel)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ServicoAns_Item]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00KM2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] );
               case 1 :
                     return conditional_P00KM3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00KM2 ;
          prmP00KM2 = new Object[] {
          new Object[] {"@lV34ServicoAns_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV34ServicoAns_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV35Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV39ServicoAns_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV39ServicoAns_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV40Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44ServicoAns_item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV44ServicoAns_item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV45Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFServicoAns_Item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV13TFServicoAns_Item_Sel",SqlDbType.VarChar,40,0}
          } ;
          Object[] prmP00KM3 ;
          prmP00KM3 = new Object[] {
          new Object[] {"@lV34ServicoAns_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV34ServicoAns_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV35Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV39ServicoAns_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV39ServicoAns_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV40Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44ServicoAns_item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV44ServicoAns_item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV45Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFServicoAns_Item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV13TFServicoAns_Item_Sel",SqlDbType.VarChar,40,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00KM2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KM2,100,0,true,false )
             ,new CursorDef("P00KM3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KM3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptservicoansfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptservicoansfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptservicoansfilterdata") )
          {
             return  ;
          }
          getpromptservicoansfilterdata worker = new getpromptservicoansfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
