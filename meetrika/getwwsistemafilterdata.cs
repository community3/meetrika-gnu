/*
               File: GetWWSistemaFilterData
        Description: Get WWSistema Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:29.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwsistemafilterdata : GXProcedure
   {
      public getwwsistemafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwsistemafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV31DDOName = aP0_DDOName;
         this.AV29SearchTxt = aP1_SearchTxt;
         this.AV30SearchTxtTo = aP2_SearchTxtTo;
         this.AV35OptionsJson = "" ;
         this.AV38OptionsDescJson = "" ;
         this.AV40OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV35OptionsJson;
         aP4_OptionsDescJson=this.AV38OptionsDescJson;
         aP5_OptionIndexesJson=this.AV40OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV31DDOName = aP0_DDOName;
         this.AV29SearchTxt = aP1_SearchTxt;
         this.AV30SearchTxtTo = aP2_SearchTxtTo;
         this.AV35OptionsJson = "" ;
         this.AV38OptionsDescJson = "" ;
         this.AV40OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV35OptionsJson;
         aP4_OptionsDescJson=this.AV38OptionsDescJson;
         aP5_OptionIndexesJson=this.AV40OptionIndexesJson;
         return AV40OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwsistemafilterdata objgetwwsistemafilterdata;
         objgetwwsistemafilterdata = new getwwsistemafilterdata();
         objgetwwsistemafilterdata.AV31DDOName = aP0_DDOName;
         objgetwwsistemafilterdata.AV29SearchTxt = aP1_SearchTxt;
         objgetwwsistemafilterdata.AV30SearchTxtTo = aP2_SearchTxtTo;
         objgetwwsistemafilterdata.AV35OptionsJson = "" ;
         objgetwwsistemafilterdata.AV38OptionsDescJson = "" ;
         objgetwwsistemafilterdata.AV40OptionIndexesJson = "" ;
         objgetwwsistemafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwsistemafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwsistemafilterdata);
         aP3_OptionsJson=this.AV35OptionsJson;
         aP4_OptionsDescJson=this.AV38OptionsDescJson;
         aP5_OptionIndexesJson=this.AV40OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwsistemafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV34Options = (IGxCollection)(new GxSimpleCollection());
         AV37OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV39OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV31DDOName), "DDO_SISTEMA_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADSISTEMA_SIGLAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV31DDOName), "DDO_SISTEMA_COORDENACAO") == 0 )
         {
            /* Execute user subroutine: 'LOADSISTEMA_COORDENACAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV31DDOName), "DDO_AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADAMBIENTETECNOLOGICO_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV31DDOName), "DDO_METODOLOGIA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADMETODOLOGIA_DESCRICAOOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV31DDOName), "DDO_SISTEMA_PROJETONOME") == 0 )
         {
            /* Execute user subroutine: 'LOADSISTEMA_PROJETONOMEOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV31DDOName), "DDO_SISTEMAVERSAO_ID") == 0 )
         {
            /* Execute user subroutine: 'LOADSISTEMAVERSAO_IDOPTIONS' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV35OptionsJson = AV34Options.ToJSonString(false);
         AV38OptionsDescJson = AV37OptionsDesc.ToJSonString(false);
         AV40OptionIndexesJson = AV39OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV42Session.Get("WWSistemaGridState"), "") == 0 )
         {
            AV44GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWSistemaGridState"), "");
         }
         else
         {
            AV44GridState.FromXml(AV42Session.Get("WWSistemaGridState"), "");
         }
         AV71GXV1 = 1;
         while ( AV71GXV1 <= AV44GridState.gxTpr_Filtervalues.Count )
         {
            AV45GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV44GridState.gxTpr_Filtervalues.Item(AV71GXV1));
            if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "SISTEMA_AREATRABALHOCOD") == 0 )
            {
               AV47Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFSISTEMA_SIGLA") == 0 )
            {
               AV10TFSistema_Sigla = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFSISTEMA_SIGLA_SEL") == 0 )
            {
               AV11TFSistema_Sigla_Sel = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFSISTEMA_TECNICA_SEL") == 0 )
            {
               AV14TFSistema_Tecnica_SelsJson = AV45GridStateFilterValue.gxTpr_Value;
               AV15TFSistema_Tecnica_Sels.FromJSonString(AV14TFSistema_Tecnica_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFSISTEMA_COORDENACAO") == 0 )
            {
               AV16TFSistema_Coordenacao = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFSISTEMA_COORDENACAO_SEL") == 0 )
            {
               AV17TFSistema_Coordenacao_Sel = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV18TFAmbienteTecnologico_Descricao = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO_SEL") == 0 )
            {
               AV19TFAmbienteTecnologico_Descricao_Sel = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO") == 0 )
            {
               AV20TFMetodologia_Descricao = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO_SEL") == 0 )
            {
               AV21TFMetodologia_Descricao_Sel = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFSISTEMA_PROJETONOME") == 0 )
            {
               AV26TFSistema_ProjetoNome = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFSISTEMA_PROJETONOME_SEL") == 0 )
            {
               AV27TFSistema_ProjetoNome_Sel = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_ID") == 0 )
            {
               AV67TFSistemaVersao_Id = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_ID_SEL") == 0 )
            {
               AV68TFSistemaVersao_Id_Sel = AV45GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45GridStateFilterValue.gxTpr_Name, "TFSISTEMA_ATIVO_SEL") == 0 )
            {
               AV28TFSistema_Ativo_Sel = (short)(NumberUtil.Val( AV45GridStateFilterValue.gxTpr_Value, "."));
            }
            AV71GXV1 = (int)(AV71GXV1+1);
         }
         if ( AV44GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV46GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV44GridState.gxTpr_Dynamicfilters.Item(1));
            AV48DynamicFiltersSelector1 = AV46GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "SISTEMA_NOME") == 0 )
            {
               AV50Sistema_Nome1 = AV46GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "SISTEMA_SIGLA") == 0 )
            {
               AV51Sistema_Sigla1 = AV46GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "SISTEMA_TIPO") == 0 )
            {
               AV52Sistema_Tipo1 = AV46GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "SISTEMA_TECNICA") == 0 )
            {
               AV53Sistema_Tecnica1 = AV46GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "SISTEMA_COORDENACAO") == 0 )
            {
               AV54Sistema_Coordenacao1 = AV46GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "SISTEMA_PROJETONOME") == 0 )
            {
               AV55Sistema_ProjetoNome1 = AV46GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector1, "SISTEMA_PF") == 0 )
            {
               AV49DynamicFiltersOperator1 = AV46GridStateDynamicFilter.gxTpr_Operator;
               AV56Sistema_PF1 = NumberUtil.Val( AV46GridStateDynamicFilter.gxTpr_Value, ".");
            }
            if ( AV44GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV57DynamicFiltersEnabled2 = true;
               AV46GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV44GridState.gxTpr_Dynamicfilters.Item(2));
               AV58DynamicFiltersSelector2 = AV46GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "SISTEMA_NOME") == 0 )
               {
                  AV60Sistema_Nome2 = AV46GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "SISTEMA_SIGLA") == 0 )
               {
                  AV61Sistema_Sigla2 = AV46GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "SISTEMA_TIPO") == 0 )
               {
                  AV62Sistema_Tipo2 = AV46GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "SISTEMA_TECNICA") == 0 )
               {
                  AV63Sistema_Tecnica2 = AV46GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "SISTEMA_COORDENACAO") == 0 )
               {
                  AV64Sistema_Coordenacao2 = AV46GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "SISTEMA_PROJETONOME") == 0 )
               {
                  AV65Sistema_ProjetoNome2 = AV46GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "SISTEMA_PF") == 0 )
               {
                  AV59DynamicFiltersOperator2 = AV46GridStateDynamicFilter.gxTpr_Operator;
                  AV66Sistema_PF2 = NumberUtil.Val( AV46GridStateDynamicFilter.gxTpr_Value, ".");
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSISTEMA_SIGLAOPTIONS' Routine */
         AV10TFSistema_Sigla = AV29SearchTxt;
         AV11TFSistema_Sigla_Sel = "";
         AV73WWSistemaDS_1_Sistema_areatrabalhocod = AV47Sistema_AreaTrabalhoCod;
         AV74WWSistemaDS_2_Dynamicfiltersselector1 = AV48DynamicFiltersSelector1;
         AV75WWSistemaDS_3_Dynamicfiltersoperator1 = AV49DynamicFiltersOperator1;
         AV76WWSistemaDS_4_Sistema_nome1 = AV50Sistema_Nome1;
         AV77WWSistemaDS_5_Sistema_sigla1 = AV51Sistema_Sigla1;
         AV78WWSistemaDS_6_Sistema_tipo1 = AV52Sistema_Tipo1;
         AV79WWSistemaDS_7_Sistema_tecnica1 = AV53Sistema_Tecnica1;
         AV80WWSistemaDS_8_Sistema_coordenacao1 = AV54Sistema_Coordenacao1;
         AV81WWSistemaDS_9_Sistema_projetonome1 = AV55Sistema_ProjetoNome1;
         AV82WWSistemaDS_10_Sistema_pf1 = AV56Sistema_PF1;
         AV83WWSistemaDS_11_Dynamicfiltersenabled2 = AV57DynamicFiltersEnabled2;
         AV84WWSistemaDS_12_Dynamicfiltersselector2 = AV58DynamicFiltersSelector2;
         AV85WWSistemaDS_13_Dynamicfiltersoperator2 = AV59DynamicFiltersOperator2;
         AV86WWSistemaDS_14_Sistema_nome2 = AV60Sistema_Nome2;
         AV87WWSistemaDS_15_Sistema_sigla2 = AV61Sistema_Sigla2;
         AV88WWSistemaDS_16_Sistema_tipo2 = AV62Sistema_Tipo2;
         AV89WWSistemaDS_17_Sistema_tecnica2 = AV63Sistema_Tecnica2;
         AV90WWSistemaDS_18_Sistema_coordenacao2 = AV64Sistema_Coordenacao2;
         AV91WWSistemaDS_19_Sistema_projetonome2 = AV65Sistema_ProjetoNome2;
         AV92WWSistemaDS_20_Sistema_pf2 = AV66Sistema_PF2;
         AV93WWSistemaDS_21_Tfsistema_sigla = AV10TFSistema_Sigla;
         AV94WWSistemaDS_22_Tfsistema_sigla_sel = AV11TFSistema_Sigla_Sel;
         AV95WWSistemaDS_23_Tfsistema_tecnica_sels = AV15TFSistema_Tecnica_Sels;
         AV96WWSistemaDS_24_Tfsistema_coordenacao = AV16TFSistema_Coordenacao;
         AV97WWSistemaDS_25_Tfsistema_coordenacao_sel = AV17TFSistema_Coordenacao_Sel;
         AV98WWSistemaDS_26_Tfambientetecnologico_descricao = AV18TFAmbienteTecnologico_Descricao;
         AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV19TFAmbienteTecnologico_Descricao_Sel;
         AV100WWSistemaDS_28_Tfmetodologia_descricao = AV20TFMetodologia_Descricao;
         AV101WWSistemaDS_29_Tfmetodologia_descricao_sel = AV21TFMetodologia_Descricao_Sel;
         AV102WWSistemaDS_30_Tfsistema_projetonome = AV26TFSistema_ProjetoNome;
         AV103WWSistemaDS_31_Tfsistema_projetonome_sel = AV27TFSistema_ProjetoNome_Sel;
         AV104WWSistemaDS_32_Tfsistemaversao_id = AV67TFSistemaVersao_Id;
         AV105WWSistemaDS_33_Tfsistemaversao_id_sel = AV68TFSistemaVersao_Id_Sel;
         AV106WWSistemaDS_34_Tfsistema_ativo_sel = AV28TFSistema_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A700Sistema_Tecnica ,
                                              AV95WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                              AV74WWSistemaDS_2_Dynamicfiltersselector1 ,
                                              AV76WWSistemaDS_4_Sistema_nome1 ,
                                              AV77WWSistemaDS_5_Sistema_sigla1 ,
                                              AV78WWSistemaDS_6_Sistema_tipo1 ,
                                              AV79WWSistemaDS_7_Sistema_tecnica1 ,
                                              AV80WWSistemaDS_8_Sistema_coordenacao1 ,
                                              AV81WWSistemaDS_9_Sistema_projetonome1 ,
                                              AV83WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                              AV84WWSistemaDS_12_Dynamicfiltersselector2 ,
                                              AV86WWSistemaDS_14_Sistema_nome2 ,
                                              AV87WWSistemaDS_15_Sistema_sigla2 ,
                                              AV88WWSistemaDS_16_Sistema_tipo2 ,
                                              AV89WWSistemaDS_17_Sistema_tecnica2 ,
                                              AV90WWSistemaDS_18_Sistema_coordenacao2 ,
                                              AV91WWSistemaDS_19_Sistema_projetonome2 ,
                                              AV94WWSistemaDS_22_Tfsistema_sigla_sel ,
                                              AV93WWSistemaDS_21_Tfsistema_sigla ,
                                              AV95WWSistemaDS_23_Tfsistema_tecnica_sels.Count ,
                                              AV97WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                              AV96WWSistemaDS_24_Tfsistema_coordenacao ,
                                              AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                              AV98WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                              AV101WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                              AV100WWSistemaDS_28_Tfmetodologia_descricao ,
                                              AV103WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                              AV102WWSistemaDS_30_Tfsistema_projetonome ,
                                              AV105WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                              AV104WWSistemaDS_32_Tfsistemaversao_id ,
                                              AV106WWSistemaDS_34_Tfsistema_ativo_sel ,
                                              A416Sistema_Nome ,
                                              A129Sistema_Sigla ,
                                              A699Sistema_Tipo ,
                                              A513Sistema_Coordenacao ,
                                              A703Sistema_ProjetoNome ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A138Metodologia_Descricao ,
                                              A1860SistemaVersao_Id ,
                                              A130Sistema_Ativo ,
                                              AV75WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                              AV82WWSistemaDS_10_Sistema_pf1 ,
                                              A395Sistema_PF ,
                                              AV85WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                              AV92WWSistemaDS_20_Sistema_pf2 ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV76WWSistemaDS_4_Sistema_nome1 = StringUtil.Concat( StringUtil.RTrim( AV76WWSistemaDS_4_Sistema_nome1), "%", "");
         lV77WWSistemaDS_5_Sistema_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV77WWSistemaDS_5_Sistema_sigla1), 25, "%");
         lV80WWSistemaDS_8_Sistema_coordenacao1 = StringUtil.Concat( StringUtil.RTrim( AV80WWSistemaDS_8_Sistema_coordenacao1), "%", "");
         lV81WWSistemaDS_9_Sistema_projetonome1 = StringUtil.PadR( StringUtil.RTrim( AV81WWSistemaDS_9_Sistema_projetonome1), 50, "%");
         lV86WWSistemaDS_14_Sistema_nome2 = StringUtil.Concat( StringUtil.RTrim( AV86WWSistemaDS_14_Sistema_nome2), "%", "");
         lV87WWSistemaDS_15_Sistema_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV87WWSistemaDS_15_Sistema_sigla2), 25, "%");
         lV90WWSistemaDS_18_Sistema_coordenacao2 = StringUtil.Concat( StringUtil.RTrim( AV90WWSistemaDS_18_Sistema_coordenacao2), "%", "");
         lV91WWSistemaDS_19_Sistema_projetonome2 = StringUtil.PadR( StringUtil.RTrim( AV91WWSistemaDS_19_Sistema_projetonome2), 50, "%");
         lV93WWSistemaDS_21_Tfsistema_sigla = StringUtil.PadR( StringUtil.RTrim( AV93WWSistemaDS_21_Tfsistema_sigla), 25, "%");
         lV96WWSistemaDS_24_Tfsistema_coordenacao = StringUtil.Concat( StringUtil.RTrim( AV96WWSistemaDS_24_Tfsistema_coordenacao), "%", "");
         lV98WWSistemaDS_26_Tfambientetecnologico_descricao = StringUtil.Concat( StringUtil.RTrim( AV98WWSistemaDS_26_Tfambientetecnologico_descricao), "%", "");
         lV100WWSistemaDS_28_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV100WWSistemaDS_28_Tfmetodologia_descricao), "%", "");
         lV102WWSistemaDS_30_Tfsistema_projetonome = StringUtil.PadR( StringUtil.RTrim( AV102WWSistemaDS_30_Tfsistema_projetonome), 50, "%");
         lV104WWSistemaDS_32_Tfsistemaversao_id = StringUtil.PadR( StringUtil.RTrim( AV104WWSistemaDS_32_Tfsistemaversao_id), 20, "%");
         /* Using cursor P00FR2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV76WWSistemaDS_4_Sistema_nome1, lV77WWSistemaDS_5_Sistema_sigla1, AV78WWSistemaDS_6_Sistema_tipo1, AV79WWSistemaDS_7_Sistema_tecnica1, lV80WWSistemaDS_8_Sistema_coordenacao1, lV81WWSistemaDS_9_Sistema_projetonome1, lV86WWSistemaDS_14_Sistema_nome2, lV87WWSistemaDS_15_Sistema_sigla2, AV88WWSistemaDS_16_Sistema_tipo2, AV89WWSistemaDS_17_Sistema_tecnica2, lV90WWSistemaDS_18_Sistema_coordenacao2, lV91WWSistemaDS_19_Sistema_projetonome2, lV93WWSistemaDS_21_Tfsistema_sigla, AV94WWSistemaDS_22_Tfsistema_sigla_sel, lV96WWSistemaDS_24_Tfsistema_coordenacao, AV97WWSistemaDS_25_Tfsistema_coordenacao_sel, lV98WWSistemaDS_26_Tfambientetecnologico_descricao, AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel, lV100WWSistemaDS_28_Tfmetodologia_descricao, AV101WWSistemaDS_29_Tfmetodologia_descricao_sel, lV102WWSistemaDS_30_Tfsistema_projetonome, AV103WWSistemaDS_31_Tfsistema_projetonome_sel, lV104WWSistemaDS_32_Tfsistemaversao_id, AV105WWSistemaDS_33_Tfsistemaversao_id_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKFR2 = false;
            A137Metodologia_Codigo = P00FR2_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P00FR2_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = P00FR2_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00FR2_n351AmbienteTecnologico_Codigo[0];
            A1859SistemaVersao_Codigo = P00FR2_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = P00FR2_n1859SistemaVersao_Codigo[0];
            A691Sistema_ProjetoCod = P00FR2_A691Sistema_ProjetoCod[0];
            n691Sistema_ProjetoCod = P00FR2_n691Sistema_ProjetoCod[0];
            A129Sistema_Sigla = P00FR2_A129Sistema_Sigla[0];
            A130Sistema_Ativo = P00FR2_A130Sistema_Ativo[0];
            A1860SistemaVersao_Id = P00FR2_A1860SistemaVersao_Id[0];
            A138Metodologia_Descricao = P00FR2_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FR2_A352AmbienteTecnologico_Descricao[0];
            A703Sistema_ProjetoNome = P00FR2_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P00FR2_n703Sistema_ProjetoNome[0];
            A513Sistema_Coordenacao = P00FR2_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = P00FR2_n513Sistema_Coordenacao[0];
            A700Sistema_Tecnica = P00FR2_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = P00FR2_n700Sistema_Tecnica[0];
            A699Sistema_Tipo = P00FR2_A699Sistema_Tipo[0];
            n699Sistema_Tipo = P00FR2_n699Sistema_Tipo[0];
            A416Sistema_Nome = P00FR2_A416Sistema_Nome[0];
            A135Sistema_AreaTrabalhoCod = P00FR2_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = P00FR2_A127Sistema_Codigo[0];
            A138Metodologia_Descricao = P00FR2_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FR2_A352AmbienteTecnologico_Descricao[0];
            A1860SistemaVersao_Id = P00FR2_A1860SistemaVersao_Id[0];
            A703Sistema_ProjetoNome = P00FR2_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P00FR2_n703Sistema_ProjetoNome[0];
            GXt_decimal1 = A395Sistema_PF;
            new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
            A395Sistema_PF = GXt_decimal1;
            if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF < AV82WWSistemaDS_10_Sistema_pf1 ) ) )
            {
               if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF == AV82WWSistemaDS_10_Sistema_pf1 ) ) )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF > AV82WWSistemaDS_10_Sistema_pf1 ) ) )
                  {
                     if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF < AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                     {
                        if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF == AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                        {
                           if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF > AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                           {
                              AV41count = 0;
                              while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00FR2_A129Sistema_Sigla[0], A129Sistema_Sigla) == 0 ) )
                              {
                                 BRKFR2 = false;
                                 A127Sistema_Codigo = P00FR2_A127Sistema_Codigo[0];
                                 AV41count = (long)(AV41count+1);
                                 BRKFR2 = true;
                                 pr_default.readNext(0);
                              }
                              if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A129Sistema_Sigla)) )
                              {
                                 AV33Option = A129Sistema_Sigla;
                                 AV36OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!")));
                                 AV34Options.Add(AV33Option, 0);
                                 AV37OptionsDesc.Add(AV36OptionDesc, 0);
                                 AV39OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV41count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                              }
                              if ( AV34Options.Count == 50 )
                              {
                                 /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                 if (true) break;
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKFR2 )
            {
               BRKFR2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSISTEMA_COORDENACAOOPTIONS' Routine */
         AV16TFSistema_Coordenacao = AV29SearchTxt;
         AV17TFSistema_Coordenacao_Sel = "";
         AV73WWSistemaDS_1_Sistema_areatrabalhocod = AV47Sistema_AreaTrabalhoCod;
         AV74WWSistemaDS_2_Dynamicfiltersselector1 = AV48DynamicFiltersSelector1;
         AV75WWSistemaDS_3_Dynamicfiltersoperator1 = AV49DynamicFiltersOperator1;
         AV76WWSistemaDS_4_Sistema_nome1 = AV50Sistema_Nome1;
         AV77WWSistemaDS_5_Sistema_sigla1 = AV51Sistema_Sigla1;
         AV78WWSistemaDS_6_Sistema_tipo1 = AV52Sistema_Tipo1;
         AV79WWSistemaDS_7_Sistema_tecnica1 = AV53Sistema_Tecnica1;
         AV80WWSistemaDS_8_Sistema_coordenacao1 = AV54Sistema_Coordenacao1;
         AV81WWSistemaDS_9_Sistema_projetonome1 = AV55Sistema_ProjetoNome1;
         AV82WWSistemaDS_10_Sistema_pf1 = AV56Sistema_PF1;
         AV83WWSistemaDS_11_Dynamicfiltersenabled2 = AV57DynamicFiltersEnabled2;
         AV84WWSistemaDS_12_Dynamicfiltersselector2 = AV58DynamicFiltersSelector2;
         AV85WWSistemaDS_13_Dynamicfiltersoperator2 = AV59DynamicFiltersOperator2;
         AV86WWSistemaDS_14_Sistema_nome2 = AV60Sistema_Nome2;
         AV87WWSistemaDS_15_Sistema_sigla2 = AV61Sistema_Sigla2;
         AV88WWSistemaDS_16_Sistema_tipo2 = AV62Sistema_Tipo2;
         AV89WWSistemaDS_17_Sistema_tecnica2 = AV63Sistema_Tecnica2;
         AV90WWSistemaDS_18_Sistema_coordenacao2 = AV64Sistema_Coordenacao2;
         AV91WWSistemaDS_19_Sistema_projetonome2 = AV65Sistema_ProjetoNome2;
         AV92WWSistemaDS_20_Sistema_pf2 = AV66Sistema_PF2;
         AV93WWSistemaDS_21_Tfsistema_sigla = AV10TFSistema_Sigla;
         AV94WWSistemaDS_22_Tfsistema_sigla_sel = AV11TFSistema_Sigla_Sel;
         AV95WWSistemaDS_23_Tfsistema_tecnica_sels = AV15TFSistema_Tecnica_Sels;
         AV96WWSistemaDS_24_Tfsistema_coordenacao = AV16TFSistema_Coordenacao;
         AV97WWSistemaDS_25_Tfsistema_coordenacao_sel = AV17TFSistema_Coordenacao_Sel;
         AV98WWSistemaDS_26_Tfambientetecnologico_descricao = AV18TFAmbienteTecnologico_Descricao;
         AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV19TFAmbienteTecnologico_Descricao_Sel;
         AV100WWSistemaDS_28_Tfmetodologia_descricao = AV20TFMetodologia_Descricao;
         AV101WWSistemaDS_29_Tfmetodologia_descricao_sel = AV21TFMetodologia_Descricao_Sel;
         AV102WWSistemaDS_30_Tfsistema_projetonome = AV26TFSistema_ProjetoNome;
         AV103WWSistemaDS_31_Tfsistema_projetonome_sel = AV27TFSistema_ProjetoNome_Sel;
         AV104WWSistemaDS_32_Tfsistemaversao_id = AV67TFSistemaVersao_Id;
         AV105WWSistemaDS_33_Tfsistemaversao_id_sel = AV68TFSistemaVersao_Id_Sel;
         AV106WWSistemaDS_34_Tfsistema_ativo_sel = AV28TFSistema_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A700Sistema_Tecnica ,
                                              AV95WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                              AV74WWSistemaDS_2_Dynamicfiltersselector1 ,
                                              AV76WWSistemaDS_4_Sistema_nome1 ,
                                              AV77WWSistemaDS_5_Sistema_sigla1 ,
                                              AV78WWSistemaDS_6_Sistema_tipo1 ,
                                              AV79WWSistemaDS_7_Sistema_tecnica1 ,
                                              AV80WWSistemaDS_8_Sistema_coordenacao1 ,
                                              AV81WWSistemaDS_9_Sistema_projetonome1 ,
                                              AV83WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                              AV84WWSistemaDS_12_Dynamicfiltersselector2 ,
                                              AV86WWSistemaDS_14_Sistema_nome2 ,
                                              AV87WWSistemaDS_15_Sistema_sigla2 ,
                                              AV88WWSistemaDS_16_Sistema_tipo2 ,
                                              AV89WWSistemaDS_17_Sistema_tecnica2 ,
                                              AV90WWSistemaDS_18_Sistema_coordenacao2 ,
                                              AV91WWSistemaDS_19_Sistema_projetonome2 ,
                                              AV94WWSistemaDS_22_Tfsistema_sigla_sel ,
                                              AV93WWSistemaDS_21_Tfsistema_sigla ,
                                              AV95WWSistemaDS_23_Tfsistema_tecnica_sels.Count ,
                                              AV97WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                              AV96WWSistemaDS_24_Tfsistema_coordenacao ,
                                              AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                              AV98WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                              AV101WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                              AV100WWSistemaDS_28_Tfmetodologia_descricao ,
                                              AV103WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                              AV102WWSistemaDS_30_Tfsistema_projetonome ,
                                              AV105WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                              AV104WWSistemaDS_32_Tfsistemaversao_id ,
                                              AV106WWSistemaDS_34_Tfsistema_ativo_sel ,
                                              A416Sistema_Nome ,
                                              A129Sistema_Sigla ,
                                              A699Sistema_Tipo ,
                                              A513Sistema_Coordenacao ,
                                              A703Sistema_ProjetoNome ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A138Metodologia_Descricao ,
                                              A1860SistemaVersao_Id ,
                                              A130Sistema_Ativo ,
                                              AV75WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                              AV82WWSistemaDS_10_Sistema_pf1 ,
                                              A395Sistema_PF ,
                                              AV85WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                              AV92WWSistemaDS_20_Sistema_pf2 ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV76WWSistemaDS_4_Sistema_nome1 = StringUtil.Concat( StringUtil.RTrim( AV76WWSistemaDS_4_Sistema_nome1), "%", "");
         lV77WWSistemaDS_5_Sistema_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV77WWSistemaDS_5_Sistema_sigla1), 25, "%");
         lV80WWSistemaDS_8_Sistema_coordenacao1 = StringUtil.Concat( StringUtil.RTrim( AV80WWSistemaDS_8_Sistema_coordenacao1), "%", "");
         lV81WWSistemaDS_9_Sistema_projetonome1 = StringUtil.PadR( StringUtil.RTrim( AV81WWSistemaDS_9_Sistema_projetonome1), 50, "%");
         lV86WWSistemaDS_14_Sistema_nome2 = StringUtil.Concat( StringUtil.RTrim( AV86WWSistemaDS_14_Sistema_nome2), "%", "");
         lV87WWSistemaDS_15_Sistema_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV87WWSistemaDS_15_Sistema_sigla2), 25, "%");
         lV90WWSistemaDS_18_Sistema_coordenacao2 = StringUtil.Concat( StringUtil.RTrim( AV90WWSistemaDS_18_Sistema_coordenacao2), "%", "");
         lV91WWSistemaDS_19_Sistema_projetonome2 = StringUtil.PadR( StringUtil.RTrim( AV91WWSistemaDS_19_Sistema_projetonome2), 50, "%");
         lV93WWSistemaDS_21_Tfsistema_sigla = StringUtil.PadR( StringUtil.RTrim( AV93WWSistemaDS_21_Tfsistema_sigla), 25, "%");
         lV96WWSistemaDS_24_Tfsistema_coordenacao = StringUtil.Concat( StringUtil.RTrim( AV96WWSistemaDS_24_Tfsistema_coordenacao), "%", "");
         lV98WWSistemaDS_26_Tfambientetecnologico_descricao = StringUtil.Concat( StringUtil.RTrim( AV98WWSistemaDS_26_Tfambientetecnologico_descricao), "%", "");
         lV100WWSistemaDS_28_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV100WWSistemaDS_28_Tfmetodologia_descricao), "%", "");
         lV102WWSistemaDS_30_Tfsistema_projetonome = StringUtil.PadR( StringUtil.RTrim( AV102WWSistemaDS_30_Tfsistema_projetonome), 50, "%");
         lV104WWSistemaDS_32_Tfsistemaversao_id = StringUtil.PadR( StringUtil.RTrim( AV104WWSistemaDS_32_Tfsistemaversao_id), 20, "%");
         /* Using cursor P00FR3 */
         pr_default.execute(1, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV76WWSistemaDS_4_Sistema_nome1, lV77WWSistemaDS_5_Sistema_sigla1, AV78WWSistemaDS_6_Sistema_tipo1, AV79WWSistemaDS_7_Sistema_tecnica1, lV80WWSistemaDS_8_Sistema_coordenacao1, lV81WWSistemaDS_9_Sistema_projetonome1, lV86WWSistemaDS_14_Sistema_nome2, lV87WWSistemaDS_15_Sistema_sigla2, AV88WWSistemaDS_16_Sistema_tipo2, AV89WWSistemaDS_17_Sistema_tecnica2, lV90WWSistemaDS_18_Sistema_coordenacao2, lV91WWSistemaDS_19_Sistema_projetonome2, lV93WWSistemaDS_21_Tfsistema_sigla, AV94WWSistemaDS_22_Tfsistema_sigla_sel, lV96WWSistemaDS_24_Tfsistema_coordenacao, AV97WWSistemaDS_25_Tfsistema_coordenacao_sel, lV98WWSistemaDS_26_Tfambientetecnologico_descricao, AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel, lV100WWSistemaDS_28_Tfmetodologia_descricao, AV101WWSistemaDS_29_Tfmetodologia_descricao_sel, lV102WWSistemaDS_30_Tfsistema_projetonome, AV103WWSistemaDS_31_Tfsistema_projetonome_sel, lV104WWSistemaDS_32_Tfsistemaversao_id, AV105WWSistemaDS_33_Tfsistemaversao_id_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKFR4 = false;
            A137Metodologia_Codigo = P00FR3_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P00FR3_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = P00FR3_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00FR3_n351AmbienteTecnologico_Codigo[0];
            A1859SistemaVersao_Codigo = P00FR3_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = P00FR3_n1859SistemaVersao_Codigo[0];
            A691Sistema_ProjetoCod = P00FR3_A691Sistema_ProjetoCod[0];
            n691Sistema_ProjetoCod = P00FR3_n691Sistema_ProjetoCod[0];
            A135Sistema_AreaTrabalhoCod = P00FR3_A135Sistema_AreaTrabalhoCod[0];
            A513Sistema_Coordenacao = P00FR3_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = P00FR3_n513Sistema_Coordenacao[0];
            A130Sistema_Ativo = P00FR3_A130Sistema_Ativo[0];
            A1860SistemaVersao_Id = P00FR3_A1860SistemaVersao_Id[0];
            A138Metodologia_Descricao = P00FR3_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FR3_A352AmbienteTecnologico_Descricao[0];
            A703Sistema_ProjetoNome = P00FR3_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P00FR3_n703Sistema_ProjetoNome[0];
            A700Sistema_Tecnica = P00FR3_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = P00FR3_n700Sistema_Tecnica[0];
            A699Sistema_Tipo = P00FR3_A699Sistema_Tipo[0];
            n699Sistema_Tipo = P00FR3_n699Sistema_Tipo[0];
            A129Sistema_Sigla = P00FR3_A129Sistema_Sigla[0];
            A416Sistema_Nome = P00FR3_A416Sistema_Nome[0];
            A127Sistema_Codigo = P00FR3_A127Sistema_Codigo[0];
            A138Metodologia_Descricao = P00FR3_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FR3_A352AmbienteTecnologico_Descricao[0];
            A1860SistemaVersao_Id = P00FR3_A1860SistemaVersao_Id[0];
            A703Sistema_ProjetoNome = P00FR3_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P00FR3_n703Sistema_ProjetoNome[0];
            GXt_decimal1 = A395Sistema_PF;
            new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
            A395Sistema_PF = GXt_decimal1;
            if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF < AV82WWSistemaDS_10_Sistema_pf1 ) ) )
            {
               if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF == AV82WWSistemaDS_10_Sistema_pf1 ) ) )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF > AV82WWSistemaDS_10_Sistema_pf1 ) ) )
                  {
                     if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF < AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                     {
                        if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF == AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                        {
                           if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF > AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                           {
                              AV41count = 0;
                              while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00FR3_A513Sistema_Coordenacao[0], A513Sistema_Coordenacao) == 0 ) )
                              {
                                 BRKFR4 = false;
                                 A127Sistema_Codigo = P00FR3_A127Sistema_Codigo[0];
                                 AV41count = (long)(AV41count+1);
                                 BRKFR4 = true;
                                 pr_default.readNext(1);
                              }
                              if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A513Sistema_Coordenacao)) )
                              {
                                 AV33Option = A513Sistema_Coordenacao;
                                 AV34Options.Add(AV33Option, 0);
                                 AV39OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV41count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                              }
                              if ( AV34Options.Count == 50 )
                              {
                                 /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                 if (true) break;
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKFR4 )
            {
               BRKFR4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADAMBIENTETECNOLOGICO_DESCRICAOOPTIONS' Routine */
         AV18TFAmbienteTecnologico_Descricao = AV29SearchTxt;
         AV19TFAmbienteTecnologico_Descricao_Sel = "";
         AV73WWSistemaDS_1_Sistema_areatrabalhocod = AV47Sistema_AreaTrabalhoCod;
         AV74WWSistemaDS_2_Dynamicfiltersselector1 = AV48DynamicFiltersSelector1;
         AV75WWSistemaDS_3_Dynamicfiltersoperator1 = AV49DynamicFiltersOperator1;
         AV76WWSistemaDS_4_Sistema_nome1 = AV50Sistema_Nome1;
         AV77WWSistemaDS_5_Sistema_sigla1 = AV51Sistema_Sigla1;
         AV78WWSistemaDS_6_Sistema_tipo1 = AV52Sistema_Tipo1;
         AV79WWSistemaDS_7_Sistema_tecnica1 = AV53Sistema_Tecnica1;
         AV80WWSistemaDS_8_Sistema_coordenacao1 = AV54Sistema_Coordenacao1;
         AV81WWSistemaDS_9_Sistema_projetonome1 = AV55Sistema_ProjetoNome1;
         AV82WWSistemaDS_10_Sistema_pf1 = AV56Sistema_PF1;
         AV83WWSistemaDS_11_Dynamicfiltersenabled2 = AV57DynamicFiltersEnabled2;
         AV84WWSistemaDS_12_Dynamicfiltersselector2 = AV58DynamicFiltersSelector2;
         AV85WWSistemaDS_13_Dynamicfiltersoperator2 = AV59DynamicFiltersOperator2;
         AV86WWSistemaDS_14_Sistema_nome2 = AV60Sistema_Nome2;
         AV87WWSistemaDS_15_Sistema_sigla2 = AV61Sistema_Sigla2;
         AV88WWSistemaDS_16_Sistema_tipo2 = AV62Sistema_Tipo2;
         AV89WWSistemaDS_17_Sistema_tecnica2 = AV63Sistema_Tecnica2;
         AV90WWSistemaDS_18_Sistema_coordenacao2 = AV64Sistema_Coordenacao2;
         AV91WWSistemaDS_19_Sistema_projetonome2 = AV65Sistema_ProjetoNome2;
         AV92WWSistemaDS_20_Sistema_pf2 = AV66Sistema_PF2;
         AV93WWSistemaDS_21_Tfsistema_sigla = AV10TFSistema_Sigla;
         AV94WWSistemaDS_22_Tfsistema_sigla_sel = AV11TFSistema_Sigla_Sel;
         AV95WWSistemaDS_23_Tfsistema_tecnica_sels = AV15TFSistema_Tecnica_Sels;
         AV96WWSistemaDS_24_Tfsistema_coordenacao = AV16TFSistema_Coordenacao;
         AV97WWSistemaDS_25_Tfsistema_coordenacao_sel = AV17TFSistema_Coordenacao_Sel;
         AV98WWSistemaDS_26_Tfambientetecnologico_descricao = AV18TFAmbienteTecnologico_Descricao;
         AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV19TFAmbienteTecnologico_Descricao_Sel;
         AV100WWSistemaDS_28_Tfmetodologia_descricao = AV20TFMetodologia_Descricao;
         AV101WWSistemaDS_29_Tfmetodologia_descricao_sel = AV21TFMetodologia_Descricao_Sel;
         AV102WWSistemaDS_30_Tfsistema_projetonome = AV26TFSistema_ProjetoNome;
         AV103WWSistemaDS_31_Tfsistema_projetonome_sel = AV27TFSistema_ProjetoNome_Sel;
         AV104WWSistemaDS_32_Tfsistemaversao_id = AV67TFSistemaVersao_Id;
         AV105WWSistemaDS_33_Tfsistemaversao_id_sel = AV68TFSistemaVersao_Id_Sel;
         AV106WWSistemaDS_34_Tfsistema_ativo_sel = AV28TFSistema_Ativo_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A700Sistema_Tecnica ,
                                              AV95WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                              AV74WWSistemaDS_2_Dynamicfiltersselector1 ,
                                              AV76WWSistemaDS_4_Sistema_nome1 ,
                                              AV77WWSistemaDS_5_Sistema_sigla1 ,
                                              AV78WWSistemaDS_6_Sistema_tipo1 ,
                                              AV79WWSistemaDS_7_Sistema_tecnica1 ,
                                              AV80WWSistemaDS_8_Sistema_coordenacao1 ,
                                              AV81WWSistemaDS_9_Sistema_projetonome1 ,
                                              AV83WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                              AV84WWSistemaDS_12_Dynamicfiltersselector2 ,
                                              AV86WWSistemaDS_14_Sistema_nome2 ,
                                              AV87WWSistemaDS_15_Sistema_sigla2 ,
                                              AV88WWSistemaDS_16_Sistema_tipo2 ,
                                              AV89WWSistemaDS_17_Sistema_tecnica2 ,
                                              AV90WWSistemaDS_18_Sistema_coordenacao2 ,
                                              AV91WWSistemaDS_19_Sistema_projetonome2 ,
                                              AV94WWSistemaDS_22_Tfsistema_sigla_sel ,
                                              AV93WWSistemaDS_21_Tfsistema_sigla ,
                                              AV95WWSistemaDS_23_Tfsistema_tecnica_sels.Count ,
                                              AV97WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                              AV96WWSistemaDS_24_Tfsistema_coordenacao ,
                                              AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                              AV98WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                              AV101WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                              AV100WWSistemaDS_28_Tfmetodologia_descricao ,
                                              AV103WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                              AV102WWSistemaDS_30_Tfsistema_projetonome ,
                                              AV105WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                              AV104WWSistemaDS_32_Tfsistemaversao_id ,
                                              AV106WWSistemaDS_34_Tfsistema_ativo_sel ,
                                              A416Sistema_Nome ,
                                              A129Sistema_Sigla ,
                                              A699Sistema_Tipo ,
                                              A513Sistema_Coordenacao ,
                                              A703Sistema_ProjetoNome ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A138Metodologia_Descricao ,
                                              A1860SistemaVersao_Id ,
                                              A130Sistema_Ativo ,
                                              AV75WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                              AV82WWSistemaDS_10_Sistema_pf1 ,
                                              A395Sistema_PF ,
                                              AV85WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                              AV92WWSistemaDS_20_Sistema_pf2 ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV76WWSistemaDS_4_Sistema_nome1 = StringUtil.Concat( StringUtil.RTrim( AV76WWSistemaDS_4_Sistema_nome1), "%", "");
         lV77WWSistemaDS_5_Sistema_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV77WWSistemaDS_5_Sistema_sigla1), 25, "%");
         lV80WWSistemaDS_8_Sistema_coordenacao1 = StringUtil.Concat( StringUtil.RTrim( AV80WWSistemaDS_8_Sistema_coordenacao1), "%", "");
         lV81WWSistemaDS_9_Sistema_projetonome1 = StringUtil.PadR( StringUtil.RTrim( AV81WWSistemaDS_9_Sistema_projetonome1), 50, "%");
         lV86WWSistemaDS_14_Sistema_nome2 = StringUtil.Concat( StringUtil.RTrim( AV86WWSistemaDS_14_Sistema_nome2), "%", "");
         lV87WWSistemaDS_15_Sistema_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV87WWSistemaDS_15_Sistema_sigla2), 25, "%");
         lV90WWSistemaDS_18_Sistema_coordenacao2 = StringUtil.Concat( StringUtil.RTrim( AV90WWSistemaDS_18_Sistema_coordenacao2), "%", "");
         lV91WWSistemaDS_19_Sistema_projetonome2 = StringUtil.PadR( StringUtil.RTrim( AV91WWSistemaDS_19_Sistema_projetonome2), 50, "%");
         lV93WWSistemaDS_21_Tfsistema_sigla = StringUtil.PadR( StringUtil.RTrim( AV93WWSistemaDS_21_Tfsistema_sigla), 25, "%");
         lV96WWSistemaDS_24_Tfsistema_coordenacao = StringUtil.Concat( StringUtil.RTrim( AV96WWSistemaDS_24_Tfsistema_coordenacao), "%", "");
         lV98WWSistemaDS_26_Tfambientetecnologico_descricao = StringUtil.Concat( StringUtil.RTrim( AV98WWSistemaDS_26_Tfambientetecnologico_descricao), "%", "");
         lV100WWSistemaDS_28_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV100WWSistemaDS_28_Tfmetodologia_descricao), "%", "");
         lV102WWSistemaDS_30_Tfsistema_projetonome = StringUtil.PadR( StringUtil.RTrim( AV102WWSistemaDS_30_Tfsistema_projetonome), 50, "%");
         lV104WWSistemaDS_32_Tfsistemaversao_id = StringUtil.PadR( StringUtil.RTrim( AV104WWSistemaDS_32_Tfsistemaversao_id), 20, "%");
         /* Using cursor P00FR4 */
         pr_default.execute(2, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV76WWSistemaDS_4_Sistema_nome1, lV77WWSistemaDS_5_Sistema_sigla1, AV78WWSistemaDS_6_Sistema_tipo1, AV79WWSistemaDS_7_Sistema_tecnica1, lV80WWSistemaDS_8_Sistema_coordenacao1, lV81WWSistemaDS_9_Sistema_projetonome1, lV86WWSistemaDS_14_Sistema_nome2, lV87WWSistemaDS_15_Sistema_sigla2, AV88WWSistemaDS_16_Sistema_tipo2, AV89WWSistemaDS_17_Sistema_tecnica2, lV90WWSistemaDS_18_Sistema_coordenacao2, lV91WWSistemaDS_19_Sistema_projetonome2, lV93WWSistemaDS_21_Tfsistema_sigla, AV94WWSistemaDS_22_Tfsistema_sigla_sel, lV96WWSistemaDS_24_Tfsistema_coordenacao, AV97WWSistemaDS_25_Tfsistema_coordenacao_sel, lV98WWSistemaDS_26_Tfambientetecnologico_descricao, AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel, lV100WWSistemaDS_28_Tfmetodologia_descricao, AV101WWSistemaDS_29_Tfmetodologia_descricao_sel, lV102WWSistemaDS_30_Tfsistema_projetonome, AV103WWSistemaDS_31_Tfsistema_projetonome_sel, lV104WWSistemaDS_32_Tfsistemaversao_id, AV105WWSistemaDS_33_Tfsistemaversao_id_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKFR6 = false;
            A137Metodologia_Codigo = P00FR4_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P00FR4_n137Metodologia_Codigo[0];
            A1859SistemaVersao_Codigo = P00FR4_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = P00FR4_n1859SistemaVersao_Codigo[0];
            A691Sistema_ProjetoCod = P00FR4_A691Sistema_ProjetoCod[0];
            n691Sistema_ProjetoCod = P00FR4_n691Sistema_ProjetoCod[0];
            A351AmbienteTecnologico_Codigo = P00FR4_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00FR4_n351AmbienteTecnologico_Codigo[0];
            A130Sistema_Ativo = P00FR4_A130Sistema_Ativo[0];
            A1860SistemaVersao_Id = P00FR4_A1860SistemaVersao_Id[0];
            A138Metodologia_Descricao = P00FR4_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FR4_A352AmbienteTecnologico_Descricao[0];
            A703Sistema_ProjetoNome = P00FR4_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P00FR4_n703Sistema_ProjetoNome[0];
            A513Sistema_Coordenacao = P00FR4_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = P00FR4_n513Sistema_Coordenacao[0];
            A700Sistema_Tecnica = P00FR4_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = P00FR4_n700Sistema_Tecnica[0];
            A699Sistema_Tipo = P00FR4_A699Sistema_Tipo[0];
            n699Sistema_Tipo = P00FR4_n699Sistema_Tipo[0];
            A129Sistema_Sigla = P00FR4_A129Sistema_Sigla[0];
            A416Sistema_Nome = P00FR4_A416Sistema_Nome[0];
            A135Sistema_AreaTrabalhoCod = P00FR4_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = P00FR4_A127Sistema_Codigo[0];
            A138Metodologia_Descricao = P00FR4_A138Metodologia_Descricao[0];
            A1860SistemaVersao_Id = P00FR4_A1860SistemaVersao_Id[0];
            A703Sistema_ProjetoNome = P00FR4_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P00FR4_n703Sistema_ProjetoNome[0];
            A352AmbienteTecnologico_Descricao = P00FR4_A352AmbienteTecnologico_Descricao[0];
            GXt_decimal1 = A395Sistema_PF;
            new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
            A395Sistema_PF = GXt_decimal1;
            if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF < AV82WWSistemaDS_10_Sistema_pf1 ) ) )
            {
               if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF == AV82WWSistemaDS_10_Sistema_pf1 ) ) )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF > AV82WWSistemaDS_10_Sistema_pf1 ) ) )
                  {
                     if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF < AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                     {
                        if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF == AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                        {
                           if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF > AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                           {
                              AV41count = 0;
                              while ( (pr_default.getStatus(2) != 101) && ( P00FR4_A351AmbienteTecnologico_Codigo[0] == A351AmbienteTecnologico_Codigo ) )
                              {
                                 BRKFR6 = false;
                                 A127Sistema_Codigo = P00FR4_A127Sistema_Codigo[0];
                                 AV41count = (long)(AV41count+1);
                                 BRKFR6 = true;
                                 pr_default.readNext(2);
                              }
                              if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A352AmbienteTecnologico_Descricao)) )
                              {
                                 AV33Option = A352AmbienteTecnologico_Descricao;
                                 AV32InsertIndex = 1;
                                 while ( ( AV32InsertIndex <= AV34Options.Count ) && ( StringUtil.StrCmp(((String)AV34Options.Item(AV32InsertIndex)), AV33Option) < 0 ) )
                                 {
                                    AV32InsertIndex = (int)(AV32InsertIndex+1);
                                 }
                                 AV34Options.Add(AV33Option, AV32InsertIndex);
                                 AV39OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV41count), "Z,ZZZ,ZZZ,ZZ9")), AV32InsertIndex);
                              }
                              if ( AV34Options.Count == 50 )
                              {
                                 /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                 if (true) break;
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKFR6 )
            {
               BRKFR6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADMETODOLOGIA_DESCRICAOOPTIONS' Routine */
         AV20TFMetodologia_Descricao = AV29SearchTxt;
         AV21TFMetodologia_Descricao_Sel = "";
         AV73WWSistemaDS_1_Sistema_areatrabalhocod = AV47Sistema_AreaTrabalhoCod;
         AV74WWSistemaDS_2_Dynamicfiltersselector1 = AV48DynamicFiltersSelector1;
         AV75WWSistemaDS_3_Dynamicfiltersoperator1 = AV49DynamicFiltersOperator1;
         AV76WWSistemaDS_4_Sistema_nome1 = AV50Sistema_Nome1;
         AV77WWSistemaDS_5_Sistema_sigla1 = AV51Sistema_Sigla1;
         AV78WWSistemaDS_6_Sistema_tipo1 = AV52Sistema_Tipo1;
         AV79WWSistemaDS_7_Sistema_tecnica1 = AV53Sistema_Tecnica1;
         AV80WWSistemaDS_8_Sistema_coordenacao1 = AV54Sistema_Coordenacao1;
         AV81WWSistemaDS_9_Sistema_projetonome1 = AV55Sistema_ProjetoNome1;
         AV82WWSistemaDS_10_Sistema_pf1 = AV56Sistema_PF1;
         AV83WWSistemaDS_11_Dynamicfiltersenabled2 = AV57DynamicFiltersEnabled2;
         AV84WWSistemaDS_12_Dynamicfiltersselector2 = AV58DynamicFiltersSelector2;
         AV85WWSistemaDS_13_Dynamicfiltersoperator2 = AV59DynamicFiltersOperator2;
         AV86WWSistemaDS_14_Sistema_nome2 = AV60Sistema_Nome2;
         AV87WWSistemaDS_15_Sistema_sigla2 = AV61Sistema_Sigla2;
         AV88WWSistemaDS_16_Sistema_tipo2 = AV62Sistema_Tipo2;
         AV89WWSistemaDS_17_Sistema_tecnica2 = AV63Sistema_Tecnica2;
         AV90WWSistemaDS_18_Sistema_coordenacao2 = AV64Sistema_Coordenacao2;
         AV91WWSistemaDS_19_Sistema_projetonome2 = AV65Sistema_ProjetoNome2;
         AV92WWSistemaDS_20_Sistema_pf2 = AV66Sistema_PF2;
         AV93WWSistemaDS_21_Tfsistema_sigla = AV10TFSistema_Sigla;
         AV94WWSistemaDS_22_Tfsistema_sigla_sel = AV11TFSistema_Sigla_Sel;
         AV95WWSistemaDS_23_Tfsistema_tecnica_sels = AV15TFSistema_Tecnica_Sels;
         AV96WWSistemaDS_24_Tfsistema_coordenacao = AV16TFSistema_Coordenacao;
         AV97WWSistemaDS_25_Tfsistema_coordenacao_sel = AV17TFSistema_Coordenacao_Sel;
         AV98WWSistemaDS_26_Tfambientetecnologico_descricao = AV18TFAmbienteTecnologico_Descricao;
         AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV19TFAmbienteTecnologico_Descricao_Sel;
         AV100WWSistemaDS_28_Tfmetodologia_descricao = AV20TFMetodologia_Descricao;
         AV101WWSistemaDS_29_Tfmetodologia_descricao_sel = AV21TFMetodologia_Descricao_Sel;
         AV102WWSistemaDS_30_Tfsistema_projetonome = AV26TFSistema_ProjetoNome;
         AV103WWSistemaDS_31_Tfsistema_projetonome_sel = AV27TFSistema_ProjetoNome_Sel;
         AV104WWSistemaDS_32_Tfsistemaversao_id = AV67TFSistemaVersao_Id;
         AV105WWSistemaDS_33_Tfsistemaversao_id_sel = AV68TFSistemaVersao_Id_Sel;
         AV106WWSistemaDS_34_Tfsistema_ativo_sel = AV28TFSistema_Ativo_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A700Sistema_Tecnica ,
                                              AV95WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                              AV74WWSistemaDS_2_Dynamicfiltersselector1 ,
                                              AV76WWSistemaDS_4_Sistema_nome1 ,
                                              AV77WWSistemaDS_5_Sistema_sigla1 ,
                                              AV78WWSistemaDS_6_Sistema_tipo1 ,
                                              AV79WWSistemaDS_7_Sistema_tecnica1 ,
                                              AV80WWSistemaDS_8_Sistema_coordenacao1 ,
                                              AV81WWSistemaDS_9_Sistema_projetonome1 ,
                                              AV83WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                              AV84WWSistemaDS_12_Dynamicfiltersselector2 ,
                                              AV86WWSistemaDS_14_Sistema_nome2 ,
                                              AV87WWSistemaDS_15_Sistema_sigla2 ,
                                              AV88WWSistemaDS_16_Sistema_tipo2 ,
                                              AV89WWSistemaDS_17_Sistema_tecnica2 ,
                                              AV90WWSistemaDS_18_Sistema_coordenacao2 ,
                                              AV91WWSistemaDS_19_Sistema_projetonome2 ,
                                              AV94WWSistemaDS_22_Tfsistema_sigla_sel ,
                                              AV93WWSistemaDS_21_Tfsistema_sigla ,
                                              AV95WWSistemaDS_23_Tfsistema_tecnica_sels.Count ,
                                              AV97WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                              AV96WWSistemaDS_24_Tfsistema_coordenacao ,
                                              AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                              AV98WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                              AV101WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                              AV100WWSistemaDS_28_Tfmetodologia_descricao ,
                                              AV103WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                              AV102WWSistemaDS_30_Tfsistema_projetonome ,
                                              AV105WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                              AV104WWSistemaDS_32_Tfsistemaversao_id ,
                                              AV106WWSistemaDS_34_Tfsistema_ativo_sel ,
                                              A416Sistema_Nome ,
                                              A129Sistema_Sigla ,
                                              A699Sistema_Tipo ,
                                              A513Sistema_Coordenacao ,
                                              A703Sistema_ProjetoNome ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A138Metodologia_Descricao ,
                                              A1860SistemaVersao_Id ,
                                              A130Sistema_Ativo ,
                                              AV75WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                              AV82WWSistemaDS_10_Sistema_pf1 ,
                                              A395Sistema_PF ,
                                              AV85WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                              AV92WWSistemaDS_20_Sistema_pf2 ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV76WWSistemaDS_4_Sistema_nome1 = StringUtil.Concat( StringUtil.RTrim( AV76WWSistemaDS_4_Sistema_nome1), "%", "");
         lV77WWSistemaDS_5_Sistema_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV77WWSistemaDS_5_Sistema_sigla1), 25, "%");
         lV80WWSistemaDS_8_Sistema_coordenacao1 = StringUtil.Concat( StringUtil.RTrim( AV80WWSistemaDS_8_Sistema_coordenacao1), "%", "");
         lV81WWSistemaDS_9_Sistema_projetonome1 = StringUtil.PadR( StringUtil.RTrim( AV81WWSistemaDS_9_Sistema_projetonome1), 50, "%");
         lV86WWSistemaDS_14_Sistema_nome2 = StringUtil.Concat( StringUtil.RTrim( AV86WWSistemaDS_14_Sistema_nome2), "%", "");
         lV87WWSistemaDS_15_Sistema_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV87WWSistemaDS_15_Sistema_sigla2), 25, "%");
         lV90WWSistemaDS_18_Sistema_coordenacao2 = StringUtil.Concat( StringUtil.RTrim( AV90WWSistemaDS_18_Sistema_coordenacao2), "%", "");
         lV91WWSistemaDS_19_Sistema_projetonome2 = StringUtil.PadR( StringUtil.RTrim( AV91WWSistemaDS_19_Sistema_projetonome2), 50, "%");
         lV93WWSistemaDS_21_Tfsistema_sigla = StringUtil.PadR( StringUtil.RTrim( AV93WWSistemaDS_21_Tfsistema_sigla), 25, "%");
         lV96WWSistemaDS_24_Tfsistema_coordenacao = StringUtil.Concat( StringUtil.RTrim( AV96WWSistemaDS_24_Tfsistema_coordenacao), "%", "");
         lV98WWSistemaDS_26_Tfambientetecnologico_descricao = StringUtil.Concat( StringUtil.RTrim( AV98WWSistemaDS_26_Tfambientetecnologico_descricao), "%", "");
         lV100WWSistemaDS_28_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV100WWSistemaDS_28_Tfmetodologia_descricao), "%", "");
         lV102WWSistemaDS_30_Tfsistema_projetonome = StringUtil.PadR( StringUtil.RTrim( AV102WWSistemaDS_30_Tfsistema_projetonome), 50, "%");
         lV104WWSistemaDS_32_Tfsistemaversao_id = StringUtil.PadR( StringUtil.RTrim( AV104WWSistemaDS_32_Tfsistemaversao_id), 20, "%");
         /* Using cursor P00FR5 */
         pr_default.execute(3, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV76WWSistemaDS_4_Sistema_nome1, lV77WWSistemaDS_5_Sistema_sigla1, AV78WWSistemaDS_6_Sistema_tipo1, AV79WWSistemaDS_7_Sistema_tecnica1, lV80WWSistemaDS_8_Sistema_coordenacao1, lV81WWSistemaDS_9_Sistema_projetonome1, lV86WWSistemaDS_14_Sistema_nome2, lV87WWSistemaDS_15_Sistema_sigla2, AV88WWSistemaDS_16_Sistema_tipo2, AV89WWSistemaDS_17_Sistema_tecnica2, lV90WWSistemaDS_18_Sistema_coordenacao2, lV91WWSistemaDS_19_Sistema_projetonome2, lV93WWSistemaDS_21_Tfsistema_sigla, AV94WWSistemaDS_22_Tfsistema_sigla_sel, lV96WWSistemaDS_24_Tfsistema_coordenacao, AV97WWSistemaDS_25_Tfsistema_coordenacao_sel, lV98WWSistemaDS_26_Tfambientetecnologico_descricao, AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel, lV100WWSistemaDS_28_Tfmetodologia_descricao, AV101WWSistemaDS_29_Tfmetodologia_descricao_sel, lV102WWSistemaDS_30_Tfsistema_projetonome, AV103WWSistemaDS_31_Tfsistema_projetonome_sel, lV104WWSistemaDS_32_Tfsistemaversao_id, AV105WWSistemaDS_33_Tfsistemaversao_id_sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKFR8 = false;
            A351AmbienteTecnologico_Codigo = P00FR5_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00FR5_n351AmbienteTecnologico_Codigo[0];
            A1859SistemaVersao_Codigo = P00FR5_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = P00FR5_n1859SistemaVersao_Codigo[0];
            A691Sistema_ProjetoCod = P00FR5_A691Sistema_ProjetoCod[0];
            n691Sistema_ProjetoCod = P00FR5_n691Sistema_ProjetoCod[0];
            A137Metodologia_Codigo = P00FR5_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P00FR5_n137Metodologia_Codigo[0];
            A130Sistema_Ativo = P00FR5_A130Sistema_Ativo[0];
            A1860SistemaVersao_Id = P00FR5_A1860SistemaVersao_Id[0];
            A138Metodologia_Descricao = P00FR5_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FR5_A352AmbienteTecnologico_Descricao[0];
            A703Sistema_ProjetoNome = P00FR5_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P00FR5_n703Sistema_ProjetoNome[0];
            A513Sistema_Coordenacao = P00FR5_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = P00FR5_n513Sistema_Coordenacao[0];
            A700Sistema_Tecnica = P00FR5_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = P00FR5_n700Sistema_Tecnica[0];
            A699Sistema_Tipo = P00FR5_A699Sistema_Tipo[0];
            n699Sistema_Tipo = P00FR5_n699Sistema_Tipo[0];
            A129Sistema_Sigla = P00FR5_A129Sistema_Sigla[0];
            A416Sistema_Nome = P00FR5_A416Sistema_Nome[0];
            A135Sistema_AreaTrabalhoCod = P00FR5_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = P00FR5_A127Sistema_Codigo[0];
            A352AmbienteTecnologico_Descricao = P00FR5_A352AmbienteTecnologico_Descricao[0];
            A1860SistemaVersao_Id = P00FR5_A1860SistemaVersao_Id[0];
            A703Sistema_ProjetoNome = P00FR5_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P00FR5_n703Sistema_ProjetoNome[0];
            A138Metodologia_Descricao = P00FR5_A138Metodologia_Descricao[0];
            GXt_decimal1 = A395Sistema_PF;
            new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
            A395Sistema_PF = GXt_decimal1;
            if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF < AV82WWSistemaDS_10_Sistema_pf1 ) ) )
            {
               if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF == AV82WWSistemaDS_10_Sistema_pf1 ) ) )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF > AV82WWSistemaDS_10_Sistema_pf1 ) ) )
                  {
                     if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF < AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                     {
                        if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF == AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                        {
                           if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF > AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                           {
                              AV41count = 0;
                              while ( (pr_default.getStatus(3) != 101) && ( P00FR5_A137Metodologia_Codigo[0] == A137Metodologia_Codigo ) )
                              {
                                 BRKFR8 = false;
                                 A127Sistema_Codigo = P00FR5_A127Sistema_Codigo[0];
                                 AV41count = (long)(AV41count+1);
                                 BRKFR8 = true;
                                 pr_default.readNext(3);
                              }
                              if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A138Metodologia_Descricao)) )
                              {
                                 AV33Option = A138Metodologia_Descricao;
                                 AV32InsertIndex = 1;
                                 while ( ( AV32InsertIndex <= AV34Options.Count ) && ( StringUtil.StrCmp(((String)AV34Options.Item(AV32InsertIndex)), AV33Option) < 0 ) )
                                 {
                                    AV32InsertIndex = (int)(AV32InsertIndex+1);
                                 }
                                 AV34Options.Add(AV33Option, AV32InsertIndex);
                                 AV39OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV41count), "Z,ZZZ,ZZZ,ZZ9")), AV32InsertIndex);
                              }
                              if ( AV34Options.Count == 50 )
                              {
                                 /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                 if (true) break;
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKFR8 )
            {
               BRKFR8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADSISTEMA_PROJETONOMEOPTIONS' Routine */
         AV26TFSistema_ProjetoNome = AV29SearchTxt;
         AV27TFSistema_ProjetoNome_Sel = "";
         AV73WWSistemaDS_1_Sistema_areatrabalhocod = AV47Sistema_AreaTrabalhoCod;
         AV74WWSistemaDS_2_Dynamicfiltersselector1 = AV48DynamicFiltersSelector1;
         AV75WWSistemaDS_3_Dynamicfiltersoperator1 = AV49DynamicFiltersOperator1;
         AV76WWSistemaDS_4_Sistema_nome1 = AV50Sistema_Nome1;
         AV77WWSistemaDS_5_Sistema_sigla1 = AV51Sistema_Sigla1;
         AV78WWSistemaDS_6_Sistema_tipo1 = AV52Sistema_Tipo1;
         AV79WWSistemaDS_7_Sistema_tecnica1 = AV53Sistema_Tecnica1;
         AV80WWSistemaDS_8_Sistema_coordenacao1 = AV54Sistema_Coordenacao1;
         AV81WWSistemaDS_9_Sistema_projetonome1 = AV55Sistema_ProjetoNome1;
         AV82WWSistemaDS_10_Sistema_pf1 = AV56Sistema_PF1;
         AV83WWSistemaDS_11_Dynamicfiltersenabled2 = AV57DynamicFiltersEnabled2;
         AV84WWSistemaDS_12_Dynamicfiltersselector2 = AV58DynamicFiltersSelector2;
         AV85WWSistemaDS_13_Dynamicfiltersoperator2 = AV59DynamicFiltersOperator2;
         AV86WWSistemaDS_14_Sistema_nome2 = AV60Sistema_Nome2;
         AV87WWSistemaDS_15_Sistema_sigla2 = AV61Sistema_Sigla2;
         AV88WWSistemaDS_16_Sistema_tipo2 = AV62Sistema_Tipo2;
         AV89WWSistemaDS_17_Sistema_tecnica2 = AV63Sistema_Tecnica2;
         AV90WWSistemaDS_18_Sistema_coordenacao2 = AV64Sistema_Coordenacao2;
         AV91WWSistemaDS_19_Sistema_projetonome2 = AV65Sistema_ProjetoNome2;
         AV92WWSistemaDS_20_Sistema_pf2 = AV66Sistema_PF2;
         AV93WWSistemaDS_21_Tfsistema_sigla = AV10TFSistema_Sigla;
         AV94WWSistemaDS_22_Tfsistema_sigla_sel = AV11TFSistema_Sigla_Sel;
         AV95WWSistemaDS_23_Tfsistema_tecnica_sels = AV15TFSistema_Tecnica_Sels;
         AV96WWSistemaDS_24_Tfsistema_coordenacao = AV16TFSistema_Coordenacao;
         AV97WWSistemaDS_25_Tfsistema_coordenacao_sel = AV17TFSistema_Coordenacao_Sel;
         AV98WWSistemaDS_26_Tfambientetecnologico_descricao = AV18TFAmbienteTecnologico_Descricao;
         AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV19TFAmbienteTecnologico_Descricao_Sel;
         AV100WWSistemaDS_28_Tfmetodologia_descricao = AV20TFMetodologia_Descricao;
         AV101WWSistemaDS_29_Tfmetodologia_descricao_sel = AV21TFMetodologia_Descricao_Sel;
         AV102WWSistemaDS_30_Tfsistema_projetonome = AV26TFSistema_ProjetoNome;
         AV103WWSistemaDS_31_Tfsistema_projetonome_sel = AV27TFSistema_ProjetoNome_Sel;
         AV104WWSistemaDS_32_Tfsistemaversao_id = AV67TFSistemaVersao_Id;
         AV105WWSistemaDS_33_Tfsistemaversao_id_sel = AV68TFSistemaVersao_Id_Sel;
         AV106WWSistemaDS_34_Tfsistema_ativo_sel = AV28TFSistema_Ativo_Sel;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A700Sistema_Tecnica ,
                                              AV95WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                              AV74WWSistemaDS_2_Dynamicfiltersselector1 ,
                                              AV76WWSistemaDS_4_Sistema_nome1 ,
                                              AV77WWSistemaDS_5_Sistema_sigla1 ,
                                              AV78WWSistemaDS_6_Sistema_tipo1 ,
                                              AV79WWSistemaDS_7_Sistema_tecnica1 ,
                                              AV80WWSistemaDS_8_Sistema_coordenacao1 ,
                                              AV81WWSistemaDS_9_Sistema_projetonome1 ,
                                              AV83WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                              AV84WWSistemaDS_12_Dynamicfiltersselector2 ,
                                              AV86WWSistemaDS_14_Sistema_nome2 ,
                                              AV87WWSistemaDS_15_Sistema_sigla2 ,
                                              AV88WWSistemaDS_16_Sistema_tipo2 ,
                                              AV89WWSistemaDS_17_Sistema_tecnica2 ,
                                              AV90WWSistemaDS_18_Sistema_coordenacao2 ,
                                              AV91WWSistemaDS_19_Sistema_projetonome2 ,
                                              AV94WWSistemaDS_22_Tfsistema_sigla_sel ,
                                              AV93WWSistemaDS_21_Tfsistema_sigla ,
                                              AV95WWSistemaDS_23_Tfsistema_tecnica_sels.Count ,
                                              AV97WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                              AV96WWSistemaDS_24_Tfsistema_coordenacao ,
                                              AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                              AV98WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                              AV101WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                              AV100WWSistemaDS_28_Tfmetodologia_descricao ,
                                              AV103WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                              AV102WWSistemaDS_30_Tfsistema_projetonome ,
                                              AV105WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                              AV104WWSistemaDS_32_Tfsistemaversao_id ,
                                              AV106WWSistemaDS_34_Tfsistema_ativo_sel ,
                                              A416Sistema_Nome ,
                                              A129Sistema_Sigla ,
                                              A699Sistema_Tipo ,
                                              A513Sistema_Coordenacao ,
                                              A703Sistema_ProjetoNome ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A138Metodologia_Descricao ,
                                              A1860SistemaVersao_Id ,
                                              A130Sistema_Ativo ,
                                              AV75WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                              AV82WWSistemaDS_10_Sistema_pf1 ,
                                              A395Sistema_PF ,
                                              AV85WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                              AV92WWSistemaDS_20_Sistema_pf2 ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV76WWSistemaDS_4_Sistema_nome1 = StringUtil.Concat( StringUtil.RTrim( AV76WWSistemaDS_4_Sistema_nome1), "%", "");
         lV77WWSistemaDS_5_Sistema_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV77WWSistemaDS_5_Sistema_sigla1), 25, "%");
         lV80WWSistemaDS_8_Sistema_coordenacao1 = StringUtil.Concat( StringUtil.RTrim( AV80WWSistemaDS_8_Sistema_coordenacao1), "%", "");
         lV81WWSistemaDS_9_Sistema_projetonome1 = StringUtil.PadR( StringUtil.RTrim( AV81WWSistemaDS_9_Sistema_projetonome1), 50, "%");
         lV86WWSistemaDS_14_Sistema_nome2 = StringUtil.Concat( StringUtil.RTrim( AV86WWSistemaDS_14_Sistema_nome2), "%", "");
         lV87WWSistemaDS_15_Sistema_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV87WWSistemaDS_15_Sistema_sigla2), 25, "%");
         lV90WWSistemaDS_18_Sistema_coordenacao2 = StringUtil.Concat( StringUtil.RTrim( AV90WWSistemaDS_18_Sistema_coordenacao2), "%", "");
         lV91WWSistemaDS_19_Sistema_projetonome2 = StringUtil.PadR( StringUtil.RTrim( AV91WWSistemaDS_19_Sistema_projetonome2), 50, "%");
         lV93WWSistemaDS_21_Tfsistema_sigla = StringUtil.PadR( StringUtil.RTrim( AV93WWSistemaDS_21_Tfsistema_sigla), 25, "%");
         lV96WWSistemaDS_24_Tfsistema_coordenacao = StringUtil.Concat( StringUtil.RTrim( AV96WWSistemaDS_24_Tfsistema_coordenacao), "%", "");
         lV98WWSistemaDS_26_Tfambientetecnologico_descricao = StringUtil.Concat( StringUtil.RTrim( AV98WWSistemaDS_26_Tfambientetecnologico_descricao), "%", "");
         lV100WWSistemaDS_28_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV100WWSistemaDS_28_Tfmetodologia_descricao), "%", "");
         lV102WWSistemaDS_30_Tfsistema_projetonome = StringUtil.PadR( StringUtil.RTrim( AV102WWSistemaDS_30_Tfsistema_projetonome), 50, "%");
         lV104WWSistemaDS_32_Tfsistemaversao_id = StringUtil.PadR( StringUtil.RTrim( AV104WWSistemaDS_32_Tfsistemaversao_id), 20, "%");
         /* Using cursor P00FR6 */
         pr_default.execute(4, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV76WWSistemaDS_4_Sistema_nome1, lV77WWSistemaDS_5_Sistema_sigla1, AV78WWSistemaDS_6_Sistema_tipo1, AV79WWSistemaDS_7_Sistema_tecnica1, lV80WWSistemaDS_8_Sistema_coordenacao1, lV81WWSistemaDS_9_Sistema_projetonome1, lV86WWSistemaDS_14_Sistema_nome2, lV87WWSistemaDS_15_Sistema_sigla2, AV88WWSistemaDS_16_Sistema_tipo2, AV89WWSistemaDS_17_Sistema_tecnica2, lV90WWSistemaDS_18_Sistema_coordenacao2, lV91WWSistemaDS_19_Sistema_projetonome2, lV93WWSistemaDS_21_Tfsistema_sigla, AV94WWSistemaDS_22_Tfsistema_sigla_sel, lV96WWSistemaDS_24_Tfsistema_coordenacao, AV97WWSistemaDS_25_Tfsistema_coordenacao_sel, lV98WWSistemaDS_26_Tfambientetecnologico_descricao, AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel, lV100WWSistemaDS_28_Tfmetodologia_descricao, AV101WWSistemaDS_29_Tfmetodologia_descricao_sel, lV102WWSistemaDS_30_Tfsistema_projetonome, AV103WWSistemaDS_31_Tfsistema_projetonome_sel, lV104WWSistemaDS_32_Tfsistemaversao_id, AV105WWSistemaDS_33_Tfsistemaversao_id_sel});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKFR10 = false;
            A137Metodologia_Codigo = P00FR6_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P00FR6_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = P00FR6_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00FR6_n351AmbienteTecnologico_Codigo[0];
            A1859SistemaVersao_Codigo = P00FR6_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = P00FR6_n1859SistemaVersao_Codigo[0];
            A691Sistema_ProjetoCod = P00FR6_A691Sistema_ProjetoCod[0];
            n691Sistema_ProjetoCod = P00FR6_n691Sistema_ProjetoCod[0];
            A130Sistema_Ativo = P00FR6_A130Sistema_Ativo[0];
            A1860SistemaVersao_Id = P00FR6_A1860SistemaVersao_Id[0];
            A138Metodologia_Descricao = P00FR6_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FR6_A352AmbienteTecnologico_Descricao[0];
            A703Sistema_ProjetoNome = P00FR6_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P00FR6_n703Sistema_ProjetoNome[0];
            A513Sistema_Coordenacao = P00FR6_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = P00FR6_n513Sistema_Coordenacao[0];
            A700Sistema_Tecnica = P00FR6_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = P00FR6_n700Sistema_Tecnica[0];
            A699Sistema_Tipo = P00FR6_A699Sistema_Tipo[0];
            n699Sistema_Tipo = P00FR6_n699Sistema_Tipo[0];
            A129Sistema_Sigla = P00FR6_A129Sistema_Sigla[0];
            A416Sistema_Nome = P00FR6_A416Sistema_Nome[0];
            A135Sistema_AreaTrabalhoCod = P00FR6_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = P00FR6_A127Sistema_Codigo[0];
            A138Metodologia_Descricao = P00FR6_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FR6_A352AmbienteTecnologico_Descricao[0];
            A1860SistemaVersao_Id = P00FR6_A1860SistemaVersao_Id[0];
            A703Sistema_ProjetoNome = P00FR6_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P00FR6_n703Sistema_ProjetoNome[0];
            GXt_decimal1 = A395Sistema_PF;
            new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
            A395Sistema_PF = GXt_decimal1;
            if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF < AV82WWSistemaDS_10_Sistema_pf1 ) ) )
            {
               if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF == AV82WWSistemaDS_10_Sistema_pf1 ) ) )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF > AV82WWSistemaDS_10_Sistema_pf1 ) ) )
                  {
                     if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF < AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                     {
                        if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF == AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                        {
                           if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF > AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                           {
                              AV41count = 0;
                              while ( (pr_default.getStatus(4) != 101) && ( P00FR6_A691Sistema_ProjetoCod[0] == A691Sistema_ProjetoCod ) )
                              {
                                 BRKFR10 = false;
                                 A127Sistema_Codigo = P00FR6_A127Sistema_Codigo[0];
                                 AV41count = (long)(AV41count+1);
                                 BRKFR10 = true;
                                 pr_default.readNext(4);
                              }
                              if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A703Sistema_ProjetoNome)) )
                              {
                                 AV33Option = A703Sistema_ProjetoNome;
                                 AV32InsertIndex = 1;
                                 while ( ( AV32InsertIndex <= AV34Options.Count ) && ( StringUtil.StrCmp(((String)AV34Options.Item(AV32InsertIndex)), AV33Option) < 0 ) )
                                 {
                                    AV32InsertIndex = (int)(AV32InsertIndex+1);
                                 }
                                 AV34Options.Add(AV33Option, AV32InsertIndex);
                                 AV39OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV41count), "Z,ZZZ,ZZZ,ZZ9")), AV32InsertIndex);
                              }
                              if ( AV34Options.Count == 50 )
                              {
                                 /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                 if (true) break;
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKFR10 )
            {
               BRKFR10 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      protected void S171( )
      {
         /* 'LOADSISTEMAVERSAO_IDOPTIONS' Routine */
         AV67TFSistemaVersao_Id = AV29SearchTxt;
         AV68TFSistemaVersao_Id_Sel = "";
         AV73WWSistemaDS_1_Sistema_areatrabalhocod = AV47Sistema_AreaTrabalhoCod;
         AV74WWSistemaDS_2_Dynamicfiltersselector1 = AV48DynamicFiltersSelector1;
         AV75WWSistemaDS_3_Dynamicfiltersoperator1 = AV49DynamicFiltersOperator1;
         AV76WWSistemaDS_4_Sistema_nome1 = AV50Sistema_Nome1;
         AV77WWSistemaDS_5_Sistema_sigla1 = AV51Sistema_Sigla1;
         AV78WWSistemaDS_6_Sistema_tipo1 = AV52Sistema_Tipo1;
         AV79WWSistemaDS_7_Sistema_tecnica1 = AV53Sistema_Tecnica1;
         AV80WWSistemaDS_8_Sistema_coordenacao1 = AV54Sistema_Coordenacao1;
         AV81WWSistemaDS_9_Sistema_projetonome1 = AV55Sistema_ProjetoNome1;
         AV82WWSistemaDS_10_Sistema_pf1 = AV56Sistema_PF1;
         AV83WWSistemaDS_11_Dynamicfiltersenabled2 = AV57DynamicFiltersEnabled2;
         AV84WWSistemaDS_12_Dynamicfiltersselector2 = AV58DynamicFiltersSelector2;
         AV85WWSistemaDS_13_Dynamicfiltersoperator2 = AV59DynamicFiltersOperator2;
         AV86WWSistemaDS_14_Sistema_nome2 = AV60Sistema_Nome2;
         AV87WWSistemaDS_15_Sistema_sigla2 = AV61Sistema_Sigla2;
         AV88WWSistemaDS_16_Sistema_tipo2 = AV62Sistema_Tipo2;
         AV89WWSistemaDS_17_Sistema_tecnica2 = AV63Sistema_Tecnica2;
         AV90WWSistemaDS_18_Sistema_coordenacao2 = AV64Sistema_Coordenacao2;
         AV91WWSistemaDS_19_Sistema_projetonome2 = AV65Sistema_ProjetoNome2;
         AV92WWSistemaDS_20_Sistema_pf2 = AV66Sistema_PF2;
         AV93WWSistemaDS_21_Tfsistema_sigla = AV10TFSistema_Sigla;
         AV94WWSistemaDS_22_Tfsistema_sigla_sel = AV11TFSistema_Sigla_Sel;
         AV95WWSistemaDS_23_Tfsistema_tecnica_sels = AV15TFSistema_Tecnica_Sels;
         AV96WWSistemaDS_24_Tfsistema_coordenacao = AV16TFSistema_Coordenacao;
         AV97WWSistemaDS_25_Tfsistema_coordenacao_sel = AV17TFSistema_Coordenacao_Sel;
         AV98WWSistemaDS_26_Tfambientetecnologico_descricao = AV18TFAmbienteTecnologico_Descricao;
         AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV19TFAmbienteTecnologico_Descricao_Sel;
         AV100WWSistemaDS_28_Tfmetodologia_descricao = AV20TFMetodologia_Descricao;
         AV101WWSistemaDS_29_Tfmetodologia_descricao_sel = AV21TFMetodologia_Descricao_Sel;
         AV102WWSistemaDS_30_Tfsistema_projetonome = AV26TFSistema_ProjetoNome;
         AV103WWSistemaDS_31_Tfsistema_projetonome_sel = AV27TFSistema_ProjetoNome_Sel;
         AV104WWSistemaDS_32_Tfsistemaversao_id = AV67TFSistemaVersao_Id;
         AV105WWSistemaDS_33_Tfsistemaversao_id_sel = AV68TFSistemaVersao_Id_Sel;
         AV106WWSistemaDS_34_Tfsistema_ativo_sel = AV28TFSistema_Ativo_Sel;
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              A700Sistema_Tecnica ,
                                              AV95WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                              AV74WWSistemaDS_2_Dynamicfiltersselector1 ,
                                              AV76WWSistemaDS_4_Sistema_nome1 ,
                                              AV77WWSistemaDS_5_Sistema_sigla1 ,
                                              AV78WWSistemaDS_6_Sistema_tipo1 ,
                                              AV79WWSistemaDS_7_Sistema_tecnica1 ,
                                              AV80WWSistemaDS_8_Sistema_coordenacao1 ,
                                              AV81WWSistemaDS_9_Sistema_projetonome1 ,
                                              AV83WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                              AV84WWSistemaDS_12_Dynamicfiltersselector2 ,
                                              AV86WWSistemaDS_14_Sistema_nome2 ,
                                              AV87WWSistemaDS_15_Sistema_sigla2 ,
                                              AV88WWSistemaDS_16_Sistema_tipo2 ,
                                              AV89WWSistemaDS_17_Sistema_tecnica2 ,
                                              AV90WWSistemaDS_18_Sistema_coordenacao2 ,
                                              AV91WWSistemaDS_19_Sistema_projetonome2 ,
                                              AV94WWSistemaDS_22_Tfsistema_sigla_sel ,
                                              AV93WWSistemaDS_21_Tfsistema_sigla ,
                                              AV95WWSistemaDS_23_Tfsistema_tecnica_sels.Count ,
                                              AV97WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                              AV96WWSistemaDS_24_Tfsistema_coordenacao ,
                                              AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                              AV98WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                              AV101WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                              AV100WWSistemaDS_28_Tfmetodologia_descricao ,
                                              AV103WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                              AV102WWSistemaDS_30_Tfsistema_projetonome ,
                                              AV105WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                              AV104WWSistemaDS_32_Tfsistemaversao_id ,
                                              AV106WWSistemaDS_34_Tfsistema_ativo_sel ,
                                              A416Sistema_Nome ,
                                              A129Sistema_Sigla ,
                                              A699Sistema_Tipo ,
                                              A513Sistema_Coordenacao ,
                                              A703Sistema_ProjetoNome ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A138Metodologia_Descricao ,
                                              A1860SistemaVersao_Id ,
                                              A130Sistema_Ativo ,
                                              AV75WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                              AV82WWSistemaDS_10_Sistema_pf1 ,
                                              A395Sistema_PF ,
                                              AV85WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                              AV92WWSistemaDS_20_Sistema_pf2 ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV76WWSistemaDS_4_Sistema_nome1 = StringUtil.Concat( StringUtil.RTrim( AV76WWSistemaDS_4_Sistema_nome1), "%", "");
         lV77WWSistemaDS_5_Sistema_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV77WWSistemaDS_5_Sistema_sigla1), 25, "%");
         lV80WWSistemaDS_8_Sistema_coordenacao1 = StringUtil.Concat( StringUtil.RTrim( AV80WWSistemaDS_8_Sistema_coordenacao1), "%", "");
         lV81WWSistemaDS_9_Sistema_projetonome1 = StringUtil.PadR( StringUtil.RTrim( AV81WWSistemaDS_9_Sistema_projetonome1), 50, "%");
         lV86WWSistemaDS_14_Sistema_nome2 = StringUtil.Concat( StringUtil.RTrim( AV86WWSistemaDS_14_Sistema_nome2), "%", "");
         lV87WWSistemaDS_15_Sistema_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV87WWSistemaDS_15_Sistema_sigla2), 25, "%");
         lV90WWSistemaDS_18_Sistema_coordenacao2 = StringUtil.Concat( StringUtil.RTrim( AV90WWSistemaDS_18_Sistema_coordenacao2), "%", "");
         lV91WWSistemaDS_19_Sistema_projetonome2 = StringUtil.PadR( StringUtil.RTrim( AV91WWSistemaDS_19_Sistema_projetonome2), 50, "%");
         lV93WWSistemaDS_21_Tfsistema_sigla = StringUtil.PadR( StringUtil.RTrim( AV93WWSistemaDS_21_Tfsistema_sigla), 25, "%");
         lV96WWSistemaDS_24_Tfsistema_coordenacao = StringUtil.Concat( StringUtil.RTrim( AV96WWSistemaDS_24_Tfsistema_coordenacao), "%", "");
         lV98WWSistemaDS_26_Tfambientetecnologico_descricao = StringUtil.Concat( StringUtil.RTrim( AV98WWSistemaDS_26_Tfambientetecnologico_descricao), "%", "");
         lV100WWSistemaDS_28_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV100WWSistemaDS_28_Tfmetodologia_descricao), "%", "");
         lV102WWSistemaDS_30_Tfsistema_projetonome = StringUtil.PadR( StringUtil.RTrim( AV102WWSistemaDS_30_Tfsistema_projetonome), 50, "%");
         lV104WWSistemaDS_32_Tfsistemaversao_id = StringUtil.PadR( StringUtil.RTrim( AV104WWSistemaDS_32_Tfsistemaversao_id), 20, "%");
         /* Using cursor P00FR7 */
         pr_default.execute(5, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV76WWSistemaDS_4_Sistema_nome1, lV77WWSistemaDS_5_Sistema_sigla1, AV78WWSistemaDS_6_Sistema_tipo1, AV79WWSistemaDS_7_Sistema_tecnica1, lV80WWSistemaDS_8_Sistema_coordenacao1, lV81WWSistemaDS_9_Sistema_projetonome1, lV86WWSistemaDS_14_Sistema_nome2, lV87WWSistemaDS_15_Sistema_sigla2, AV88WWSistemaDS_16_Sistema_tipo2, AV89WWSistemaDS_17_Sistema_tecnica2, lV90WWSistemaDS_18_Sistema_coordenacao2, lV91WWSistemaDS_19_Sistema_projetonome2, lV93WWSistemaDS_21_Tfsistema_sigla, AV94WWSistemaDS_22_Tfsistema_sigla_sel, lV96WWSistemaDS_24_Tfsistema_coordenacao, AV97WWSistemaDS_25_Tfsistema_coordenacao_sel, lV98WWSistemaDS_26_Tfambientetecnologico_descricao, AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel, lV100WWSistemaDS_28_Tfmetodologia_descricao, AV101WWSistemaDS_29_Tfmetodologia_descricao_sel, lV102WWSistemaDS_30_Tfsistema_projetonome, AV103WWSistemaDS_31_Tfsistema_projetonome_sel, lV104WWSistemaDS_32_Tfsistemaversao_id, AV105WWSistemaDS_33_Tfsistemaversao_id_sel});
         while ( (pr_default.getStatus(5) != 101) )
         {
            BRKFR12 = false;
            A137Metodologia_Codigo = P00FR7_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = P00FR7_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = P00FR7_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00FR7_n351AmbienteTecnologico_Codigo[0];
            A691Sistema_ProjetoCod = P00FR7_A691Sistema_ProjetoCod[0];
            n691Sistema_ProjetoCod = P00FR7_n691Sistema_ProjetoCod[0];
            A1859SistemaVersao_Codigo = P00FR7_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = P00FR7_n1859SistemaVersao_Codigo[0];
            A130Sistema_Ativo = P00FR7_A130Sistema_Ativo[0];
            A1860SistemaVersao_Id = P00FR7_A1860SistemaVersao_Id[0];
            A138Metodologia_Descricao = P00FR7_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FR7_A352AmbienteTecnologico_Descricao[0];
            A703Sistema_ProjetoNome = P00FR7_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P00FR7_n703Sistema_ProjetoNome[0];
            A513Sistema_Coordenacao = P00FR7_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = P00FR7_n513Sistema_Coordenacao[0];
            A700Sistema_Tecnica = P00FR7_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = P00FR7_n700Sistema_Tecnica[0];
            A699Sistema_Tipo = P00FR7_A699Sistema_Tipo[0];
            n699Sistema_Tipo = P00FR7_n699Sistema_Tipo[0];
            A129Sistema_Sigla = P00FR7_A129Sistema_Sigla[0];
            A416Sistema_Nome = P00FR7_A416Sistema_Nome[0];
            A135Sistema_AreaTrabalhoCod = P00FR7_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = P00FR7_A127Sistema_Codigo[0];
            A138Metodologia_Descricao = P00FR7_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = P00FR7_A352AmbienteTecnologico_Descricao[0];
            A703Sistema_ProjetoNome = P00FR7_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = P00FR7_n703Sistema_ProjetoNome[0];
            A1860SistemaVersao_Id = P00FR7_A1860SistemaVersao_Id[0];
            GXt_decimal1 = A395Sistema_PF;
            new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
            A395Sistema_PF = GXt_decimal1;
            if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF < AV82WWSistemaDS_10_Sistema_pf1 ) ) )
            {
               if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF == AV82WWSistemaDS_10_Sistema_pf1 ) ) )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV75WWSistemaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV82WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF > AV82WWSistemaDS_10_Sistema_pf1 ) ) )
                  {
                     if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF < AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                     {
                        if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF == AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                        {
                           if ( ! ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV85WWSistemaDS_13_Dynamicfiltersoperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV92WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF > AV92WWSistemaDS_20_Sistema_pf2 ) ) )
                           {
                              AV41count = 0;
                              while ( (pr_default.getStatus(5) != 101) && ( P00FR7_A1859SistemaVersao_Codigo[0] == A1859SistemaVersao_Codigo ) )
                              {
                                 BRKFR12 = false;
                                 A127Sistema_Codigo = P00FR7_A127Sistema_Codigo[0];
                                 AV41count = (long)(AV41count+1);
                                 BRKFR12 = true;
                                 pr_default.readNext(5);
                              }
                              if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1860SistemaVersao_Id)) )
                              {
                                 AV33Option = A1860SistemaVersao_Id;
                                 AV32InsertIndex = 1;
                                 while ( ( AV32InsertIndex <= AV34Options.Count ) && ( StringUtil.StrCmp(((String)AV34Options.Item(AV32InsertIndex)), AV33Option) < 0 ) )
                                 {
                                    AV32InsertIndex = (int)(AV32InsertIndex+1);
                                 }
                                 AV34Options.Add(AV33Option, AV32InsertIndex);
                                 AV39OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV41count), "Z,ZZZ,ZZZ,ZZ9")), AV32InsertIndex);
                              }
                              if ( AV34Options.Count == 50 )
                              {
                                 /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                 if (true) break;
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKFR12 )
            {
               BRKFR12 = true;
               pr_default.readNext(5);
            }
         }
         pr_default.close(5);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV34Options = new GxSimpleCollection();
         AV37OptionsDesc = new GxSimpleCollection();
         AV39OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV42Session = context.GetSession();
         AV44GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV45GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFSistema_Sigla = "";
         AV11TFSistema_Sigla_Sel = "";
         AV14TFSistema_Tecnica_SelsJson = "";
         AV15TFSistema_Tecnica_Sels = new GxSimpleCollection();
         AV16TFSistema_Coordenacao = "";
         AV17TFSistema_Coordenacao_Sel = "";
         AV18TFAmbienteTecnologico_Descricao = "";
         AV19TFAmbienteTecnologico_Descricao_Sel = "";
         AV20TFMetodologia_Descricao = "";
         AV21TFMetodologia_Descricao_Sel = "";
         AV26TFSistema_ProjetoNome = "";
         AV27TFSistema_ProjetoNome_Sel = "";
         AV67TFSistemaVersao_Id = "";
         AV68TFSistemaVersao_Id_Sel = "";
         AV46GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV48DynamicFiltersSelector1 = "";
         AV50Sistema_Nome1 = "";
         AV51Sistema_Sigla1 = "";
         AV52Sistema_Tipo1 = "A";
         AV53Sistema_Tecnica1 = "";
         AV54Sistema_Coordenacao1 = "";
         AV55Sistema_ProjetoNome1 = "";
         AV58DynamicFiltersSelector2 = "";
         AV60Sistema_Nome2 = "";
         AV61Sistema_Sigla2 = "";
         AV62Sistema_Tipo2 = "A";
         AV63Sistema_Tecnica2 = "";
         AV64Sistema_Coordenacao2 = "";
         AV65Sistema_ProjetoNome2 = "";
         AV74WWSistemaDS_2_Dynamicfiltersselector1 = "";
         AV76WWSistemaDS_4_Sistema_nome1 = "";
         AV77WWSistemaDS_5_Sistema_sigla1 = "";
         AV78WWSistemaDS_6_Sistema_tipo1 = "";
         AV79WWSistemaDS_7_Sistema_tecnica1 = "";
         AV80WWSistemaDS_8_Sistema_coordenacao1 = "";
         AV81WWSistemaDS_9_Sistema_projetonome1 = "";
         AV84WWSistemaDS_12_Dynamicfiltersselector2 = "";
         AV86WWSistemaDS_14_Sistema_nome2 = "";
         AV87WWSistemaDS_15_Sistema_sigla2 = "";
         AV88WWSistemaDS_16_Sistema_tipo2 = "";
         AV89WWSistemaDS_17_Sistema_tecnica2 = "";
         AV90WWSistemaDS_18_Sistema_coordenacao2 = "";
         AV91WWSistemaDS_19_Sistema_projetonome2 = "";
         AV93WWSistemaDS_21_Tfsistema_sigla = "";
         AV94WWSistemaDS_22_Tfsistema_sigla_sel = "";
         AV95WWSistemaDS_23_Tfsistema_tecnica_sels = new GxSimpleCollection();
         AV96WWSistemaDS_24_Tfsistema_coordenacao = "";
         AV97WWSistemaDS_25_Tfsistema_coordenacao_sel = "";
         AV98WWSistemaDS_26_Tfambientetecnologico_descricao = "";
         AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel = "";
         AV100WWSistemaDS_28_Tfmetodologia_descricao = "";
         AV101WWSistemaDS_29_Tfmetodologia_descricao_sel = "";
         AV102WWSistemaDS_30_Tfsistema_projetonome = "";
         AV103WWSistemaDS_31_Tfsistema_projetonome_sel = "";
         AV104WWSistemaDS_32_Tfsistemaversao_id = "";
         AV105WWSistemaDS_33_Tfsistemaversao_id_sel = "";
         scmdbuf = "";
         lV76WWSistemaDS_4_Sistema_nome1 = "";
         lV77WWSistemaDS_5_Sistema_sigla1 = "";
         lV80WWSistemaDS_8_Sistema_coordenacao1 = "";
         lV81WWSistemaDS_9_Sistema_projetonome1 = "";
         lV86WWSistemaDS_14_Sistema_nome2 = "";
         lV87WWSistemaDS_15_Sistema_sigla2 = "";
         lV90WWSistemaDS_18_Sistema_coordenacao2 = "";
         lV91WWSistemaDS_19_Sistema_projetonome2 = "";
         lV93WWSistemaDS_21_Tfsistema_sigla = "";
         lV96WWSistemaDS_24_Tfsistema_coordenacao = "";
         lV98WWSistemaDS_26_Tfambientetecnologico_descricao = "";
         lV100WWSistemaDS_28_Tfmetodologia_descricao = "";
         lV102WWSistemaDS_30_Tfsistema_projetonome = "";
         lV104WWSistemaDS_32_Tfsistemaversao_id = "";
         A700Sistema_Tecnica = "";
         A416Sistema_Nome = "";
         A129Sistema_Sigla = "";
         A699Sistema_Tipo = "";
         A513Sistema_Coordenacao = "";
         A703Sistema_ProjetoNome = "";
         A352AmbienteTecnologico_Descricao = "";
         A138Metodologia_Descricao = "";
         A1860SistemaVersao_Id = "";
         P00FR2_A137Metodologia_Codigo = new int[1] ;
         P00FR2_n137Metodologia_Codigo = new bool[] {false} ;
         P00FR2_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00FR2_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00FR2_A1859SistemaVersao_Codigo = new int[1] ;
         P00FR2_n1859SistemaVersao_Codigo = new bool[] {false} ;
         P00FR2_A691Sistema_ProjetoCod = new int[1] ;
         P00FR2_n691Sistema_ProjetoCod = new bool[] {false} ;
         P00FR2_A129Sistema_Sigla = new String[] {""} ;
         P00FR2_A130Sistema_Ativo = new bool[] {false} ;
         P00FR2_A1860SistemaVersao_Id = new String[] {""} ;
         P00FR2_A138Metodologia_Descricao = new String[] {""} ;
         P00FR2_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00FR2_A703Sistema_ProjetoNome = new String[] {""} ;
         P00FR2_n703Sistema_ProjetoNome = new bool[] {false} ;
         P00FR2_A513Sistema_Coordenacao = new String[] {""} ;
         P00FR2_n513Sistema_Coordenacao = new bool[] {false} ;
         P00FR2_A700Sistema_Tecnica = new String[] {""} ;
         P00FR2_n700Sistema_Tecnica = new bool[] {false} ;
         P00FR2_A699Sistema_Tipo = new String[] {""} ;
         P00FR2_n699Sistema_Tipo = new bool[] {false} ;
         P00FR2_A416Sistema_Nome = new String[] {""} ;
         P00FR2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00FR2_A127Sistema_Codigo = new int[1] ;
         AV33Option = "";
         AV36OptionDesc = "";
         P00FR3_A137Metodologia_Codigo = new int[1] ;
         P00FR3_n137Metodologia_Codigo = new bool[] {false} ;
         P00FR3_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00FR3_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00FR3_A1859SistemaVersao_Codigo = new int[1] ;
         P00FR3_n1859SistemaVersao_Codigo = new bool[] {false} ;
         P00FR3_A691Sistema_ProjetoCod = new int[1] ;
         P00FR3_n691Sistema_ProjetoCod = new bool[] {false} ;
         P00FR3_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00FR3_A513Sistema_Coordenacao = new String[] {""} ;
         P00FR3_n513Sistema_Coordenacao = new bool[] {false} ;
         P00FR3_A130Sistema_Ativo = new bool[] {false} ;
         P00FR3_A1860SistemaVersao_Id = new String[] {""} ;
         P00FR3_A138Metodologia_Descricao = new String[] {""} ;
         P00FR3_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00FR3_A703Sistema_ProjetoNome = new String[] {""} ;
         P00FR3_n703Sistema_ProjetoNome = new bool[] {false} ;
         P00FR3_A700Sistema_Tecnica = new String[] {""} ;
         P00FR3_n700Sistema_Tecnica = new bool[] {false} ;
         P00FR3_A699Sistema_Tipo = new String[] {""} ;
         P00FR3_n699Sistema_Tipo = new bool[] {false} ;
         P00FR3_A129Sistema_Sigla = new String[] {""} ;
         P00FR3_A416Sistema_Nome = new String[] {""} ;
         P00FR3_A127Sistema_Codigo = new int[1] ;
         P00FR4_A137Metodologia_Codigo = new int[1] ;
         P00FR4_n137Metodologia_Codigo = new bool[] {false} ;
         P00FR4_A1859SistemaVersao_Codigo = new int[1] ;
         P00FR4_n1859SistemaVersao_Codigo = new bool[] {false} ;
         P00FR4_A691Sistema_ProjetoCod = new int[1] ;
         P00FR4_n691Sistema_ProjetoCod = new bool[] {false} ;
         P00FR4_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00FR4_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00FR4_A130Sistema_Ativo = new bool[] {false} ;
         P00FR4_A1860SistemaVersao_Id = new String[] {""} ;
         P00FR4_A138Metodologia_Descricao = new String[] {""} ;
         P00FR4_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00FR4_A703Sistema_ProjetoNome = new String[] {""} ;
         P00FR4_n703Sistema_ProjetoNome = new bool[] {false} ;
         P00FR4_A513Sistema_Coordenacao = new String[] {""} ;
         P00FR4_n513Sistema_Coordenacao = new bool[] {false} ;
         P00FR4_A700Sistema_Tecnica = new String[] {""} ;
         P00FR4_n700Sistema_Tecnica = new bool[] {false} ;
         P00FR4_A699Sistema_Tipo = new String[] {""} ;
         P00FR4_n699Sistema_Tipo = new bool[] {false} ;
         P00FR4_A129Sistema_Sigla = new String[] {""} ;
         P00FR4_A416Sistema_Nome = new String[] {""} ;
         P00FR4_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00FR4_A127Sistema_Codigo = new int[1] ;
         P00FR5_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00FR5_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00FR5_A1859SistemaVersao_Codigo = new int[1] ;
         P00FR5_n1859SistemaVersao_Codigo = new bool[] {false} ;
         P00FR5_A691Sistema_ProjetoCod = new int[1] ;
         P00FR5_n691Sistema_ProjetoCod = new bool[] {false} ;
         P00FR5_A137Metodologia_Codigo = new int[1] ;
         P00FR5_n137Metodologia_Codigo = new bool[] {false} ;
         P00FR5_A130Sistema_Ativo = new bool[] {false} ;
         P00FR5_A1860SistemaVersao_Id = new String[] {""} ;
         P00FR5_A138Metodologia_Descricao = new String[] {""} ;
         P00FR5_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00FR5_A703Sistema_ProjetoNome = new String[] {""} ;
         P00FR5_n703Sistema_ProjetoNome = new bool[] {false} ;
         P00FR5_A513Sistema_Coordenacao = new String[] {""} ;
         P00FR5_n513Sistema_Coordenacao = new bool[] {false} ;
         P00FR5_A700Sistema_Tecnica = new String[] {""} ;
         P00FR5_n700Sistema_Tecnica = new bool[] {false} ;
         P00FR5_A699Sistema_Tipo = new String[] {""} ;
         P00FR5_n699Sistema_Tipo = new bool[] {false} ;
         P00FR5_A129Sistema_Sigla = new String[] {""} ;
         P00FR5_A416Sistema_Nome = new String[] {""} ;
         P00FR5_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00FR5_A127Sistema_Codigo = new int[1] ;
         P00FR6_A137Metodologia_Codigo = new int[1] ;
         P00FR6_n137Metodologia_Codigo = new bool[] {false} ;
         P00FR6_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00FR6_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00FR6_A1859SistemaVersao_Codigo = new int[1] ;
         P00FR6_n1859SistemaVersao_Codigo = new bool[] {false} ;
         P00FR6_A691Sistema_ProjetoCod = new int[1] ;
         P00FR6_n691Sistema_ProjetoCod = new bool[] {false} ;
         P00FR6_A130Sistema_Ativo = new bool[] {false} ;
         P00FR6_A1860SistemaVersao_Id = new String[] {""} ;
         P00FR6_A138Metodologia_Descricao = new String[] {""} ;
         P00FR6_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00FR6_A703Sistema_ProjetoNome = new String[] {""} ;
         P00FR6_n703Sistema_ProjetoNome = new bool[] {false} ;
         P00FR6_A513Sistema_Coordenacao = new String[] {""} ;
         P00FR6_n513Sistema_Coordenacao = new bool[] {false} ;
         P00FR6_A700Sistema_Tecnica = new String[] {""} ;
         P00FR6_n700Sistema_Tecnica = new bool[] {false} ;
         P00FR6_A699Sistema_Tipo = new String[] {""} ;
         P00FR6_n699Sistema_Tipo = new bool[] {false} ;
         P00FR6_A129Sistema_Sigla = new String[] {""} ;
         P00FR6_A416Sistema_Nome = new String[] {""} ;
         P00FR6_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00FR6_A127Sistema_Codigo = new int[1] ;
         P00FR7_A137Metodologia_Codigo = new int[1] ;
         P00FR7_n137Metodologia_Codigo = new bool[] {false} ;
         P00FR7_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00FR7_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00FR7_A691Sistema_ProjetoCod = new int[1] ;
         P00FR7_n691Sistema_ProjetoCod = new bool[] {false} ;
         P00FR7_A1859SistemaVersao_Codigo = new int[1] ;
         P00FR7_n1859SistemaVersao_Codigo = new bool[] {false} ;
         P00FR7_A130Sistema_Ativo = new bool[] {false} ;
         P00FR7_A1860SistemaVersao_Id = new String[] {""} ;
         P00FR7_A138Metodologia_Descricao = new String[] {""} ;
         P00FR7_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00FR7_A703Sistema_ProjetoNome = new String[] {""} ;
         P00FR7_n703Sistema_ProjetoNome = new bool[] {false} ;
         P00FR7_A513Sistema_Coordenacao = new String[] {""} ;
         P00FR7_n513Sistema_Coordenacao = new bool[] {false} ;
         P00FR7_A700Sistema_Tecnica = new String[] {""} ;
         P00FR7_n700Sistema_Tecnica = new bool[] {false} ;
         P00FR7_A699Sistema_Tipo = new String[] {""} ;
         P00FR7_n699Sistema_Tipo = new bool[] {false} ;
         P00FR7_A129Sistema_Sigla = new String[] {""} ;
         P00FR7_A416Sistema_Nome = new String[] {""} ;
         P00FR7_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00FR7_A127Sistema_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwsistemafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00FR2_A137Metodologia_Codigo, P00FR2_n137Metodologia_Codigo, P00FR2_A351AmbienteTecnologico_Codigo, P00FR2_n351AmbienteTecnologico_Codigo, P00FR2_A1859SistemaVersao_Codigo, P00FR2_n1859SistemaVersao_Codigo, P00FR2_A691Sistema_ProjetoCod, P00FR2_n691Sistema_ProjetoCod, P00FR2_A129Sistema_Sigla, P00FR2_A130Sistema_Ativo,
               P00FR2_A1860SistemaVersao_Id, P00FR2_A138Metodologia_Descricao, P00FR2_A352AmbienteTecnologico_Descricao, P00FR2_A703Sistema_ProjetoNome, P00FR2_n703Sistema_ProjetoNome, P00FR2_A513Sistema_Coordenacao, P00FR2_n513Sistema_Coordenacao, P00FR2_A700Sistema_Tecnica, P00FR2_n700Sistema_Tecnica, P00FR2_A699Sistema_Tipo,
               P00FR2_n699Sistema_Tipo, P00FR2_A416Sistema_Nome, P00FR2_A135Sistema_AreaTrabalhoCod, P00FR2_A127Sistema_Codigo
               }
               , new Object[] {
               P00FR3_A137Metodologia_Codigo, P00FR3_n137Metodologia_Codigo, P00FR3_A351AmbienteTecnologico_Codigo, P00FR3_n351AmbienteTecnologico_Codigo, P00FR3_A1859SistemaVersao_Codigo, P00FR3_n1859SistemaVersao_Codigo, P00FR3_A691Sistema_ProjetoCod, P00FR3_n691Sistema_ProjetoCod, P00FR3_A135Sistema_AreaTrabalhoCod, P00FR3_A513Sistema_Coordenacao,
               P00FR3_n513Sistema_Coordenacao, P00FR3_A130Sistema_Ativo, P00FR3_A1860SistemaVersao_Id, P00FR3_A138Metodologia_Descricao, P00FR3_A352AmbienteTecnologico_Descricao, P00FR3_A703Sistema_ProjetoNome, P00FR3_n703Sistema_ProjetoNome, P00FR3_A700Sistema_Tecnica, P00FR3_n700Sistema_Tecnica, P00FR3_A699Sistema_Tipo,
               P00FR3_n699Sistema_Tipo, P00FR3_A129Sistema_Sigla, P00FR3_A416Sistema_Nome, P00FR3_A127Sistema_Codigo
               }
               , new Object[] {
               P00FR4_A137Metodologia_Codigo, P00FR4_n137Metodologia_Codigo, P00FR4_A1859SistemaVersao_Codigo, P00FR4_n1859SistemaVersao_Codigo, P00FR4_A691Sistema_ProjetoCod, P00FR4_n691Sistema_ProjetoCod, P00FR4_A351AmbienteTecnologico_Codigo, P00FR4_n351AmbienteTecnologico_Codigo, P00FR4_A130Sistema_Ativo, P00FR4_A1860SistemaVersao_Id,
               P00FR4_A138Metodologia_Descricao, P00FR4_A352AmbienteTecnologico_Descricao, P00FR4_A703Sistema_ProjetoNome, P00FR4_n703Sistema_ProjetoNome, P00FR4_A513Sistema_Coordenacao, P00FR4_n513Sistema_Coordenacao, P00FR4_A700Sistema_Tecnica, P00FR4_n700Sistema_Tecnica, P00FR4_A699Sistema_Tipo, P00FR4_n699Sistema_Tipo,
               P00FR4_A129Sistema_Sigla, P00FR4_A416Sistema_Nome, P00FR4_A135Sistema_AreaTrabalhoCod, P00FR4_A127Sistema_Codigo
               }
               , new Object[] {
               P00FR5_A351AmbienteTecnologico_Codigo, P00FR5_n351AmbienteTecnologico_Codigo, P00FR5_A1859SistemaVersao_Codigo, P00FR5_n1859SistemaVersao_Codigo, P00FR5_A691Sistema_ProjetoCod, P00FR5_n691Sistema_ProjetoCod, P00FR5_A137Metodologia_Codigo, P00FR5_n137Metodologia_Codigo, P00FR5_A130Sistema_Ativo, P00FR5_A1860SistemaVersao_Id,
               P00FR5_A138Metodologia_Descricao, P00FR5_A352AmbienteTecnologico_Descricao, P00FR5_A703Sistema_ProjetoNome, P00FR5_n703Sistema_ProjetoNome, P00FR5_A513Sistema_Coordenacao, P00FR5_n513Sistema_Coordenacao, P00FR5_A700Sistema_Tecnica, P00FR5_n700Sistema_Tecnica, P00FR5_A699Sistema_Tipo, P00FR5_n699Sistema_Tipo,
               P00FR5_A129Sistema_Sigla, P00FR5_A416Sistema_Nome, P00FR5_A135Sistema_AreaTrabalhoCod, P00FR5_A127Sistema_Codigo
               }
               , new Object[] {
               P00FR6_A137Metodologia_Codigo, P00FR6_n137Metodologia_Codigo, P00FR6_A351AmbienteTecnologico_Codigo, P00FR6_n351AmbienteTecnologico_Codigo, P00FR6_A1859SistemaVersao_Codigo, P00FR6_n1859SistemaVersao_Codigo, P00FR6_A691Sistema_ProjetoCod, P00FR6_n691Sistema_ProjetoCod, P00FR6_A130Sistema_Ativo, P00FR6_A1860SistemaVersao_Id,
               P00FR6_A138Metodologia_Descricao, P00FR6_A352AmbienteTecnologico_Descricao, P00FR6_A703Sistema_ProjetoNome, P00FR6_n703Sistema_ProjetoNome, P00FR6_A513Sistema_Coordenacao, P00FR6_n513Sistema_Coordenacao, P00FR6_A700Sistema_Tecnica, P00FR6_n700Sistema_Tecnica, P00FR6_A699Sistema_Tipo, P00FR6_n699Sistema_Tipo,
               P00FR6_A129Sistema_Sigla, P00FR6_A416Sistema_Nome, P00FR6_A135Sistema_AreaTrabalhoCod, P00FR6_A127Sistema_Codigo
               }
               , new Object[] {
               P00FR7_A137Metodologia_Codigo, P00FR7_n137Metodologia_Codigo, P00FR7_A351AmbienteTecnologico_Codigo, P00FR7_n351AmbienteTecnologico_Codigo, P00FR7_A691Sistema_ProjetoCod, P00FR7_n691Sistema_ProjetoCod, P00FR7_A1859SistemaVersao_Codigo, P00FR7_n1859SistemaVersao_Codigo, P00FR7_A130Sistema_Ativo, P00FR7_A1860SistemaVersao_Id,
               P00FR7_A138Metodologia_Descricao, P00FR7_A352AmbienteTecnologico_Descricao, P00FR7_A703Sistema_ProjetoNome, P00FR7_n703Sistema_ProjetoNome, P00FR7_A513Sistema_Coordenacao, P00FR7_n513Sistema_Coordenacao, P00FR7_A700Sistema_Tecnica, P00FR7_n700Sistema_Tecnica, P00FR7_A699Sistema_Tipo, P00FR7_n699Sistema_Tipo,
               P00FR7_A129Sistema_Sigla, P00FR7_A416Sistema_Nome, P00FR7_A135Sistema_AreaTrabalhoCod, P00FR7_A127Sistema_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV28TFSistema_Ativo_Sel ;
      private short AV49DynamicFiltersOperator1 ;
      private short AV59DynamicFiltersOperator2 ;
      private short AV75WWSistemaDS_3_Dynamicfiltersoperator1 ;
      private short AV85WWSistemaDS_13_Dynamicfiltersoperator2 ;
      private short AV106WWSistemaDS_34_Tfsistema_ativo_sel ;
      private int AV71GXV1 ;
      private int AV47Sistema_AreaTrabalhoCod ;
      private int AV73WWSistemaDS_1_Sistema_areatrabalhocod ;
      private int AV95WWSistemaDS_23_Tfsistema_tecnica_sels_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A137Metodologia_Codigo ;
      private int A351AmbienteTecnologico_Codigo ;
      private int A1859SistemaVersao_Codigo ;
      private int A691Sistema_ProjetoCod ;
      private int A127Sistema_Codigo ;
      private int AV32InsertIndex ;
      private long AV41count ;
      private decimal AV56Sistema_PF1 ;
      private decimal AV66Sistema_PF2 ;
      private decimal AV82WWSistemaDS_10_Sistema_pf1 ;
      private decimal AV92WWSistemaDS_20_Sistema_pf2 ;
      private decimal A395Sistema_PF ;
      private decimal GXt_decimal1 ;
      private String AV10TFSistema_Sigla ;
      private String AV11TFSistema_Sigla_Sel ;
      private String AV26TFSistema_ProjetoNome ;
      private String AV27TFSistema_ProjetoNome_Sel ;
      private String AV67TFSistemaVersao_Id ;
      private String AV68TFSistemaVersao_Id_Sel ;
      private String AV51Sistema_Sigla1 ;
      private String AV52Sistema_Tipo1 ;
      private String AV53Sistema_Tecnica1 ;
      private String AV55Sistema_ProjetoNome1 ;
      private String AV61Sistema_Sigla2 ;
      private String AV62Sistema_Tipo2 ;
      private String AV63Sistema_Tecnica2 ;
      private String AV65Sistema_ProjetoNome2 ;
      private String AV77WWSistemaDS_5_Sistema_sigla1 ;
      private String AV78WWSistemaDS_6_Sistema_tipo1 ;
      private String AV79WWSistemaDS_7_Sistema_tecnica1 ;
      private String AV81WWSistemaDS_9_Sistema_projetonome1 ;
      private String AV87WWSistemaDS_15_Sistema_sigla2 ;
      private String AV88WWSistemaDS_16_Sistema_tipo2 ;
      private String AV89WWSistemaDS_17_Sistema_tecnica2 ;
      private String AV91WWSistemaDS_19_Sistema_projetonome2 ;
      private String AV93WWSistemaDS_21_Tfsistema_sigla ;
      private String AV94WWSistemaDS_22_Tfsistema_sigla_sel ;
      private String AV102WWSistemaDS_30_Tfsistema_projetonome ;
      private String AV103WWSistemaDS_31_Tfsistema_projetonome_sel ;
      private String AV104WWSistemaDS_32_Tfsistemaversao_id ;
      private String AV105WWSistemaDS_33_Tfsistemaversao_id_sel ;
      private String scmdbuf ;
      private String lV77WWSistemaDS_5_Sistema_sigla1 ;
      private String lV81WWSistemaDS_9_Sistema_projetonome1 ;
      private String lV87WWSistemaDS_15_Sistema_sigla2 ;
      private String lV91WWSistemaDS_19_Sistema_projetonome2 ;
      private String lV93WWSistemaDS_21_Tfsistema_sigla ;
      private String lV102WWSistemaDS_30_Tfsistema_projetonome ;
      private String lV104WWSistemaDS_32_Tfsistemaversao_id ;
      private String A700Sistema_Tecnica ;
      private String A129Sistema_Sigla ;
      private String A699Sistema_Tipo ;
      private String A703Sistema_ProjetoNome ;
      private String A1860SistemaVersao_Id ;
      private bool returnInSub ;
      private bool AV57DynamicFiltersEnabled2 ;
      private bool AV83WWSistemaDS_11_Dynamicfiltersenabled2 ;
      private bool A130Sistema_Ativo ;
      private bool BRKFR2 ;
      private bool n137Metodologia_Codigo ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool n1859SistemaVersao_Codigo ;
      private bool n691Sistema_ProjetoCod ;
      private bool n703Sistema_ProjetoNome ;
      private bool n513Sistema_Coordenacao ;
      private bool n700Sistema_Tecnica ;
      private bool n699Sistema_Tipo ;
      private bool BRKFR4 ;
      private bool BRKFR6 ;
      private bool BRKFR8 ;
      private bool BRKFR10 ;
      private bool BRKFR12 ;
      private String AV40OptionIndexesJson ;
      private String AV35OptionsJson ;
      private String AV38OptionsDescJson ;
      private String AV14TFSistema_Tecnica_SelsJson ;
      private String AV31DDOName ;
      private String AV29SearchTxt ;
      private String AV30SearchTxtTo ;
      private String AV16TFSistema_Coordenacao ;
      private String AV17TFSistema_Coordenacao_Sel ;
      private String AV18TFAmbienteTecnologico_Descricao ;
      private String AV19TFAmbienteTecnologico_Descricao_Sel ;
      private String AV20TFMetodologia_Descricao ;
      private String AV21TFMetodologia_Descricao_Sel ;
      private String AV48DynamicFiltersSelector1 ;
      private String AV50Sistema_Nome1 ;
      private String AV54Sistema_Coordenacao1 ;
      private String AV58DynamicFiltersSelector2 ;
      private String AV60Sistema_Nome2 ;
      private String AV64Sistema_Coordenacao2 ;
      private String AV74WWSistemaDS_2_Dynamicfiltersselector1 ;
      private String AV76WWSistemaDS_4_Sistema_nome1 ;
      private String AV80WWSistemaDS_8_Sistema_coordenacao1 ;
      private String AV84WWSistemaDS_12_Dynamicfiltersselector2 ;
      private String AV86WWSistemaDS_14_Sistema_nome2 ;
      private String AV90WWSistemaDS_18_Sistema_coordenacao2 ;
      private String AV96WWSistemaDS_24_Tfsistema_coordenacao ;
      private String AV97WWSistemaDS_25_Tfsistema_coordenacao_sel ;
      private String AV98WWSistemaDS_26_Tfambientetecnologico_descricao ;
      private String AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel ;
      private String AV100WWSistemaDS_28_Tfmetodologia_descricao ;
      private String AV101WWSistemaDS_29_Tfmetodologia_descricao_sel ;
      private String lV76WWSistemaDS_4_Sistema_nome1 ;
      private String lV80WWSistemaDS_8_Sistema_coordenacao1 ;
      private String lV86WWSistemaDS_14_Sistema_nome2 ;
      private String lV90WWSistemaDS_18_Sistema_coordenacao2 ;
      private String lV96WWSistemaDS_24_Tfsistema_coordenacao ;
      private String lV98WWSistemaDS_26_Tfambientetecnologico_descricao ;
      private String lV100WWSistemaDS_28_Tfmetodologia_descricao ;
      private String A416Sistema_Nome ;
      private String A513Sistema_Coordenacao ;
      private String A352AmbienteTecnologico_Descricao ;
      private String A138Metodologia_Descricao ;
      private String AV33Option ;
      private String AV36OptionDesc ;
      private IGxSession AV42Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00FR2_A137Metodologia_Codigo ;
      private bool[] P00FR2_n137Metodologia_Codigo ;
      private int[] P00FR2_A351AmbienteTecnologico_Codigo ;
      private bool[] P00FR2_n351AmbienteTecnologico_Codigo ;
      private int[] P00FR2_A1859SistemaVersao_Codigo ;
      private bool[] P00FR2_n1859SistemaVersao_Codigo ;
      private int[] P00FR2_A691Sistema_ProjetoCod ;
      private bool[] P00FR2_n691Sistema_ProjetoCod ;
      private String[] P00FR2_A129Sistema_Sigla ;
      private bool[] P00FR2_A130Sistema_Ativo ;
      private String[] P00FR2_A1860SistemaVersao_Id ;
      private String[] P00FR2_A138Metodologia_Descricao ;
      private String[] P00FR2_A352AmbienteTecnologico_Descricao ;
      private String[] P00FR2_A703Sistema_ProjetoNome ;
      private bool[] P00FR2_n703Sistema_ProjetoNome ;
      private String[] P00FR2_A513Sistema_Coordenacao ;
      private bool[] P00FR2_n513Sistema_Coordenacao ;
      private String[] P00FR2_A700Sistema_Tecnica ;
      private bool[] P00FR2_n700Sistema_Tecnica ;
      private String[] P00FR2_A699Sistema_Tipo ;
      private bool[] P00FR2_n699Sistema_Tipo ;
      private String[] P00FR2_A416Sistema_Nome ;
      private int[] P00FR2_A135Sistema_AreaTrabalhoCod ;
      private int[] P00FR2_A127Sistema_Codigo ;
      private int[] P00FR3_A137Metodologia_Codigo ;
      private bool[] P00FR3_n137Metodologia_Codigo ;
      private int[] P00FR3_A351AmbienteTecnologico_Codigo ;
      private bool[] P00FR3_n351AmbienteTecnologico_Codigo ;
      private int[] P00FR3_A1859SistemaVersao_Codigo ;
      private bool[] P00FR3_n1859SistemaVersao_Codigo ;
      private int[] P00FR3_A691Sistema_ProjetoCod ;
      private bool[] P00FR3_n691Sistema_ProjetoCod ;
      private int[] P00FR3_A135Sistema_AreaTrabalhoCod ;
      private String[] P00FR3_A513Sistema_Coordenacao ;
      private bool[] P00FR3_n513Sistema_Coordenacao ;
      private bool[] P00FR3_A130Sistema_Ativo ;
      private String[] P00FR3_A1860SistemaVersao_Id ;
      private String[] P00FR3_A138Metodologia_Descricao ;
      private String[] P00FR3_A352AmbienteTecnologico_Descricao ;
      private String[] P00FR3_A703Sistema_ProjetoNome ;
      private bool[] P00FR3_n703Sistema_ProjetoNome ;
      private String[] P00FR3_A700Sistema_Tecnica ;
      private bool[] P00FR3_n700Sistema_Tecnica ;
      private String[] P00FR3_A699Sistema_Tipo ;
      private bool[] P00FR3_n699Sistema_Tipo ;
      private String[] P00FR3_A129Sistema_Sigla ;
      private String[] P00FR3_A416Sistema_Nome ;
      private int[] P00FR3_A127Sistema_Codigo ;
      private int[] P00FR4_A137Metodologia_Codigo ;
      private bool[] P00FR4_n137Metodologia_Codigo ;
      private int[] P00FR4_A1859SistemaVersao_Codigo ;
      private bool[] P00FR4_n1859SistemaVersao_Codigo ;
      private int[] P00FR4_A691Sistema_ProjetoCod ;
      private bool[] P00FR4_n691Sistema_ProjetoCod ;
      private int[] P00FR4_A351AmbienteTecnologico_Codigo ;
      private bool[] P00FR4_n351AmbienteTecnologico_Codigo ;
      private bool[] P00FR4_A130Sistema_Ativo ;
      private String[] P00FR4_A1860SistemaVersao_Id ;
      private String[] P00FR4_A138Metodologia_Descricao ;
      private String[] P00FR4_A352AmbienteTecnologico_Descricao ;
      private String[] P00FR4_A703Sistema_ProjetoNome ;
      private bool[] P00FR4_n703Sistema_ProjetoNome ;
      private String[] P00FR4_A513Sistema_Coordenacao ;
      private bool[] P00FR4_n513Sistema_Coordenacao ;
      private String[] P00FR4_A700Sistema_Tecnica ;
      private bool[] P00FR4_n700Sistema_Tecnica ;
      private String[] P00FR4_A699Sistema_Tipo ;
      private bool[] P00FR4_n699Sistema_Tipo ;
      private String[] P00FR4_A129Sistema_Sigla ;
      private String[] P00FR4_A416Sistema_Nome ;
      private int[] P00FR4_A135Sistema_AreaTrabalhoCod ;
      private int[] P00FR4_A127Sistema_Codigo ;
      private int[] P00FR5_A351AmbienteTecnologico_Codigo ;
      private bool[] P00FR5_n351AmbienteTecnologico_Codigo ;
      private int[] P00FR5_A1859SistemaVersao_Codigo ;
      private bool[] P00FR5_n1859SistemaVersao_Codigo ;
      private int[] P00FR5_A691Sistema_ProjetoCod ;
      private bool[] P00FR5_n691Sistema_ProjetoCod ;
      private int[] P00FR5_A137Metodologia_Codigo ;
      private bool[] P00FR5_n137Metodologia_Codigo ;
      private bool[] P00FR5_A130Sistema_Ativo ;
      private String[] P00FR5_A1860SistemaVersao_Id ;
      private String[] P00FR5_A138Metodologia_Descricao ;
      private String[] P00FR5_A352AmbienteTecnologico_Descricao ;
      private String[] P00FR5_A703Sistema_ProjetoNome ;
      private bool[] P00FR5_n703Sistema_ProjetoNome ;
      private String[] P00FR5_A513Sistema_Coordenacao ;
      private bool[] P00FR5_n513Sistema_Coordenacao ;
      private String[] P00FR5_A700Sistema_Tecnica ;
      private bool[] P00FR5_n700Sistema_Tecnica ;
      private String[] P00FR5_A699Sistema_Tipo ;
      private bool[] P00FR5_n699Sistema_Tipo ;
      private String[] P00FR5_A129Sistema_Sigla ;
      private String[] P00FR5_A416Sistema_Nome ;
      private int[] P00FR5_A135Sistema_AreaTrabalhoCod ;
      private int[] P00FR5_A127Sistema_Codigo ;
      private int[] P00FR6_A137Metodologia_Codigo ;
      private bool[] P00FR6_n137Metodologia_Codigo ;
      private int[] P00FR6_A351AmbienteTecnologico_Codigo ;
      private bool[] P00FR6_n351AmbienteTecnologico_Codigo ;
      private int[] P00FR6_A1859SistemaVersao_Codigo ;
      private bool[] P00FR6_n1859SistemaVersao_Codigo ;
      private int[] P00FR6_A691Sistema_ProjetoCod ;
      private bool[] P00FR6_n691Sistema_ProjetoCod ;
      private bool[] P00FR6_A130Sistema_Ativo ;
      private String[] P00FR6_A1860SistemaVersao_Id ;
      private String[] P00FR6_A138Metodologia_Descricao ;
      private String[] P00FR6_A352AmbienteTecnologico_Descricao ;
      private String[] P00FR6_A703Sistema_ProjetoNome ;
      private bool[] P00FR6_n703Sistema_ProjetoNome ;
      private String[] P00FR6_A513Sistema_Coordenacao ;
      private bool[] P00FR6_n513Sistema_Coordenacao ;
      private String[] P00FR6_A700Sistema_Tecnica ;
      private bool[] P00FR6_n700Sistema_Tecnica ;
      private String[] P00FR6_A699Sistema_Tipo ;
      private bool[] P00FR6_n699Sistema_Tipo ;
      private String[] P00FR6_A129Sistema_Sigla ;
      private String[] P00FR6_A416Sistema_Nome ;
      private int[] P00FR6_A135Sistema_AreaTrabalhoCod ;
      private int[] P00FR6_A127Sistema_Codigo ;
      private int[] P00FR7_A137Metodologia_Codigo ;
      private bool[] P00FR7_n137Metodologia_Codigo ;
      private int[] P00FR7_A351AmbienteTecnologico_Codigo ;
      private bool[] P00FR7_n351AmbienteTecnologico_Codigo ;
      private int[] P00FR7_A691Sistema_ProjetoCod ;
      private bool[] P00FR7_n691Sistema_ProjetoCod ;
      private int[] P00FR7_A1859SistemaVersao_Codigo ;
      private bool[] P00FR7_n1859SistemaVersao_Codigo ;
      private bool[] P00FR7_A130Sistema_Ativo ;
      private String[] P00FR7_A1860SistemaVersao_Id ;
      private String[] P00FR7_A138Metodologia_Descricao ;
      private String[] P00FR7_A352AmbienteTecnologico_Descricao ;
      private String[] P00FR7_A703Sistema_ProjetoNome ;
      private bool[] P00FR7_n703Sistema_ProjetoNome ;
      private String[] P00FR7_A513Sistema_Coordenacao ;
      private bool[] P00FR7_n513Sistema_Coordenacao ;
      private String[] P00FR7_A700Sistema_Tecnica ;
      private bool[] P00FR7_n700Sistema_Tecnica ;
      private String[] P00FR7_A699Sistema_Tipo ;
      private bool[] P00FR7_n699Sistema_Tipo ;
      private String[] P00FR7_A129Sistema_Sigla ;
      private String[] P00FR7_A416Sistema_Nome ;
      private int[] P00FR7_A135Sistema_AreaTrabalhoCod ;
      private int[] P00FR7_A127Sistema_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV15TFSistema_Tecnica_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV95WWSistemaDS_23_Tfsistema_tecnica_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV37OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV39OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV44GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV45GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV46GridStateDynamicFilter ;
   }

   public class getwwsistemafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00FR2( IGxContext context ,
                                             String A700Sistema_Tecnica ,
                                             IGxCollection AV95WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                             String AV74WWSistemaDS_2_Dynamicfiltersselector1 ,
                                             String AV76WWSistemaDS_4_Sistema_nome1 ,
                                             String AV77WWSistemaDS_5_Sistema_sigla1 ,
                                             String AV78WWSistemaDS_6_Sistema_tipo1 ,
                                             String AV79WWSistemaDS_7_Sistema_tecnica1 ,
                                             String AV80WWSistemaDS_8_Sistema_coordenacao1 ,
                                             String AV81WWSistemaDS_9_Sistema_projetonome1 ,
                                             bool AV83WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                             String AV84WWSistemaDS_12_Dynamicfiltersselector2 ,
                                             String AV86WWSistemaDS_14_Sistema_nome2 ,
                                             String AV87WWSistemaDS_15_Sistema_sigla2 ,
                                             String AV88WWSistemaDS_16_Sistema_tipo2 ,
                                             String AV89WWSistemaDS_17_Sistema_tecnica2 ,
                                             String AV90WWSistemaDS_18_Sistema_coordenacao2 ,
                                             String AV91WWSistemaDS_19_Sistema_projetonome2 ,
                                             String AV94WWSistemaDS_22_Tfsistema_sigla_sel ,
                                             String AV93WWSistemaDS_21_Tfsistema_sigla ,
                                             int AV95WWSistemaDS_23_Tfsistema_tecnica_sels_Count ,
                                             String AV97WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                             String AV96WWSistemaDS_24_Tfsistema_coordenacao ,
                                             String AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                             String AV98WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                             String AV101WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                             String AV100WWSistemaDS_28_Tfmetodologia_descricao ,
                                             String AV103WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                             String AV102WWSistemaDS_30_Tfsistema_projetonome ,
                                             String AV105WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                             String AV104WWSistemaDS_32_Tfsistemaversao_id ,
                                             short AV106WWSistemaDS_34_Tfsistema_ativo_sel ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A699Sistema_Tipo ,
                                             String A513Sistema_Coordenacao ,
                                             String A703Sistema_ProjetoNome ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             String A1860SistemaVersao_Id ,
                                             bool A130Sistema_Ativo ,
                                             short AV75WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                             decimal AV82WWSistemaDS_10_Sistema_pf1 ,
                                             decimal A395Sistema_PF ,
                                             short AV85WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                             decimal AV92WWSistemaDS_20_Sistema_pf2 ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [25] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[AmbienteTecnologico_Codigo], T1.[SistemaVersao_Codigo], T1.[Sistema_ProjetoCod] AS Sistema_ProjetoCod, T1.[Sistema_Sigla], T1.[Sistema_Ativo], T4.[SistemaVersao_Id], T2.[Metodologia_Descricao], T3.[AmbienteTecnologico_Descricao], T5.[Projeto_Nome] AS Sistema_ProjetoNome, T1.[Sistema_Coordenacao], T1.[Sistema_Tecnica], T1.[Sistema_Tipo], T1.[Sistema_Nome], T1.[Sistema_AreaTrabalhoCod], T1.[Sistema_Codigo] FROM (((([Sistema] T1 WITH (NOLOCK) LEFT JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo]) LEFT JOIN [SistemaVersao] T4 WITH (NOLOCK) ON T4.[SistemaVersao_Codigo] = T1.[SistemaVersao_Codigo]) LEFT JOIN [Projeto] T5 WITH (NOLOCK) ON T5.[Projeto_Codigo] = T1.[Sistema_ProjetoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWSistemaDS_4_Sistema_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV76WWSistemaDS_4_Sistema_nome1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWSistemaDS_5_Sistema_sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV77WWSistemaDS_5_Sistema_sigla1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWSistemaDS_6_Sistema_tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV78WWSistemaDS_6_Sistema_tipo1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWSistemaDS_7_Sistema_tecnica1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV79WWSistemaDS_7_Sistema_tecnica1)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWSistemaDS_8_Sistema_coordenacao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV80WWSistemaDS_8_Sistema_coordenacao1 + '%')";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWSistemaDS_9_Sistema_projetonome1)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like '%' + @lV81WWSistemaDS_9_Sistema_projetonome1 + '%')";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWSistemaDS_14_Sistema_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV86WWSistemaDS_14_Sistema_nome2)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWSistemaDS_15_Sistema_sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV87WWSistemaDS_15_Sistema_sigla2)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWSistemaDS_16_Sistema_tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV88WWSistemaDS_16_Sistema_tipo2)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWSistemaDS_17_Sistema_tecnica2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV89WWSistemaDS_17_Sistema_tecnica2)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWSistemaDS_18_Sistema_coordenacao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV90WWSistemaDS_18_Sistema_coordenacao2 + '%')";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWSistemaDS_19_Sistema_projetonome2)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like '%' + @lV91WWSistemaDS_19_Sistema_projetonome2 + '%')";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWSistemaDS_22_Tfsistema_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWSistemaDS_21_Tfsistema_sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV93WWSistemaDS_21_Tfsistema_sigla)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWSistemaDS_22_Tfsistema_sigla_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV94WWSistemaDS_22_Tfsistema_sigla_sel)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV95WWSistemaDS_23_Tfsistema_tecnica_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV95WWSistemaDS_23_Tfsistema_tecnica_sels, "T1.[Sistema_Tecnica] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWSistemaDS_24_Tfsistema_coordenacao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like @lV96WWSistemaDS_24_Tfsistema_coordenacao)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] = @AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWSistemaDS_26_Tfambientetecnologico_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] like @lV98WWSistemaDS_26_Tfambientetecnologico_descricao)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] = @AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWSistemaDS_28_Tfmetodologia_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV100WWSistemaDS_28_Tfmetodologia_descricao)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWSistemaDS_31_Tfsistema_projetonome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWSistemaDS_30_Tfsistema_projetonome)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like @lV102WWSistemaDS_30_Tfsistema_projetonome)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWSistemaDS_31_Tfsistema_projetonome_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] = @AV103WWSistemaDS_31_Tfsistema_projetonome_sel)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWSistemaDS_33_Tfsistemaversao_id_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWSistemaDS_32_Tfsistemaversao_id)) ) )
         {
            sWhereString = sWhereString + " and (T4.[SistemaVersao_Id] like @lV104WWSistemaDS_32_Tfsistemaversao_id)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWSistemaDS_33_Tfsistemaversao_id_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[SistemaVersao_Id] = @AV105WWSistemaDS_33_Tfsistemaversao_id_sel)";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV106WWSistemaDS_34_Tfsistema_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 1)";
         }
         if ( AV106WWSistemaDS_34_Tfsistema_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Sigla]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P00FR3( IGxContext context ,
                                             String A700Sistema_Tecnica ,
                                             IGxCollection AV95WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                             String AV74WWSistemaDS_2_Dynamicfiltersselector1 ,
                                             String AV76WWSistemaDS_4_Sistema_nome1 ,
                                             String AV77WWSistemaDS_5_Sistema_sigla1 ,
                                             String AV78WWSistemaDS_6_Sistema_tipo1 ,
                                             String AV79WWSistemaDS_7_Sistema_tecnica1 ,
                                             String AV80WWSistemaDS_8_Sistema_coordenacao1 ,
                                             String AV81WWSistemaDS_9_Sistema_projetonome1 ,
                                             bool AV83WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                             String AV84WWSistemaDS_12_Dynamicfiltersselector2 ,
                                             String AV86WWSistemaDS_14_Sistema_nome2 ,
                                             String AV87WWSistemaDS_15_Sistema_sigla2 ,
                                             String AV88WWSistemaDS_16_Sistema_tipo2 ,
                                             String AV89WWSistemaDS_17_Sistema_tecnica2 ,
                                             String AV90WWSistemaDS_18_Sistema_coordenacao2 ,
                                             String AV91WWSistemaDS_19_Sistema_projetonome2 ,
                                             String AV94WWSistemaDS_22_Tfsistema_sigla_sel ,
                                             String AV93WWSistemaDS_21_Tfsistema_sigla ,
                                             int AV95WWSistemaDS_23_Tfsistema_tecnica_sels_Count ,
                                             String AV97WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                             String AV96WWSistemaDS_24_Tfsistema_coordenacao ,
                                             String AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                             String AV98WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                             String AV101WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                             String AV100WWSistemaDS_28_Tfmetodologia_descricao ,
                                             String AV103WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                             String AV102WWSistemaDS_30_Tfsistema_projetonome ,
                                             String AV105WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                             String AV104WWSistemaDS_32_Tfsistemaversao_id ,
                                             short AV106WWSistemaDS_34_Tfsistema_ativo_sel ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A699Sistema_Tipo ,
                                             String A513Sistema_Coordenacao ,
                                             String A703Sistema_ProjetoNome ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             String A1860SistemaVersao_Id ,
                                             bool A130Sistema_Ativo ,
                                             short AV75WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                             decimal AV82WWSistemaDS_10_Sistema_pf1 ,
                                             decimal A395Sistema_PF ,
                                             short AV85WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                             decimal AV92WWSistemaDS_20_Sistema_pf2 ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [25] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[AmbienteTecnologico_Codigo], T1.[SistemaVersao_Codigo], T1.[Sistema_ProjetoCod] AS Sistema_ProjetoCod, T1.[Sistema_AreaTrabalhoCod], T1.[Sistema_Coordenacao], T1.[Sistema_Ativo], T4.[SistemaVersao_Id], T2.[Metodologia_Descricao], T3.[AmbienteTecnologico_Descricao], T5.[Projeto_Nome] AS Sistema_ProjetoNome, T1.[Sistema_Tecnica], T1.[Sistema_Tipo], T1.[Sistema_Sigla], T1.[Sistema_Nome], T1.[Sistema_Codigo] FROM (((([Sistema] T1 WITH (NOLOCK) LEFT JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo]) LEFT JOIN [SistemaVersao] T4 WITH (NOLOCK) ON T4.[SistemaVersao_Codigo] = T1.[SistemaVersao_Codigo]) LEFT JOIN [Projeto] T5 WITH (NOLOCK) ON T5.[Projeto_Codigo] = T1.[Sistema_ProjetoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWSistemaDS_4_Sistema_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV76WWSistemaDS_4_Sistema_nome1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWSistemaDS_5_Sistema_sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV77WWSistemaDS_5_Sistema_sigla1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWSistemaDS_6_Sistema_tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV78WWSistemaDS_6_Sistema_tipo1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWSistemaDS_7_Sistema_tecnica1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV79WWSistemaDS_7_Sistema_tecnica1)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWSistemaDS_8_Sistema_coordenacao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV80WWSistemaDS_8_Sistema_coordenacao1 + '%')";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWSistemaDS_9_Sistema_projetonome1)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like '%' + @lV81WWSistemaDS_9_Sistema_projetonome1 + '%')";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWSistemaDS_14_Sistema_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV86WWSistemaDS_14_Sistema_nome2)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWSistemaDS_15_Sistema_sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV87WWSistemaDS_15_Sistema_sigla2)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWSistemaDS_16_Sistema_tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV88WWSistemaDS_16_Sistema_tipo2)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWSistemaDS_17_Sistema_tecnica2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV89WWSistemaDS_17_Sistema_tecnica2)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWSistemaDS_18_Sistema_coordenacao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV90WWSistemaDS_18_Sistema_coordenacao2 + '%')";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWSistemaDS_19_Sistema_projetonome2)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like '%' + @lV91WWSistemaDS_19_Sistema_projetonome2 + '%')";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWSistemaDS_22_Tfsistema_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWSistemaDS_21_Tfsistema_sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV93WWSistemaDS_21_Tfsistema_sigla)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWSistemaDS_22_Tfsistema_sigla_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV94WWSistemaDS_22_Tfsistema_sigla_sel)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV95WWSistemaDS_23_Tfsistema_tecnica_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV95WWSistemaDS_23_Tfsistema_tecnica_sels, "T1.[Sistema_Tecnica] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWSistemaDS_24_Tfsistema_coordenacao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like @lV96WWSistemaDS_24_Tfsistema_coordenacao)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] = @AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWSistemaDS_26_Tfambientetecnologico_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] like @lV98WWSistemaDS_26_Tfambientetecnologico_descricao)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] = @AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWSistemaDS_28_Tfmetodologia_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV100WWSistemaDS_28_Tfmetodologia_descricao)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWSistemaDS_31_Tfsistema_projetonome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWSistemaDS_30_Tfsistema_projetonome)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like @lV102WWSistemaDS_30_Tfsistema_projetonome)";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWSistemaDS_31_Tfsistema_projetonome_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] = @AV103WWSistemaDS_31_Tfsistema_projetonome_sel)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWSistemaDS_33_Tfsistemaversao_id_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWSistemaDS_32_Tfsistemaversao_id)) ) )
         {
            sWhereString = sWhereString + " and (T4.[SistemaVersao_Id] like @lV104WWSistemaDS_32_Tfsistemaversao_id)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWSistemaDS_33_Tfsistemaversao_id_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[SistemaVersao_Id] = @AV105WWSistemaDS_33_Tfsistemaversao_id_sel)";
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( AV106WWSistemaDS_34_Tfsistema_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 1)";
         }
         if ( AV106WWSistemaDS_34_Tfsistema_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Coordenacao]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_P00FR4( IGxContext context ,
                                             String A700Sistema_Tecnica ,
                                             IGxCollection AV95WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                             String AV74WWSistemaDS_2_Dynamicfiltersselector1 ,
                                             String AV76WWSistemaDS_4_Sistema_nome1 ,
                                             String AV77WWSistemaDS_5_Sistema_sigla1 ,
                                             String AV78WWSistemaDS_6_Sistema_tipo1 ,
                                             String AV79WWSistemaDS_7_Sistema_tecnica1 ,
                                             String AV80WWSistemaDS_8_Sistema_coordenacao1 ,
                                             String AV81WWSistemaDS_9_Sistema_projetonome1 ,
                                             bool AV83WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                             String AV84WWSistemaDS_12_Dynamicfiltersselector2 ,
                                             String AV86WWSistemaDS_14_Sistema_nome2 ,
                                             String AV87WWSistemaDS_15_Sistema_sigla2 ,
                                             String AV88WWSistemaDS_16_Sistema_tipo2 ,
                                             String AV89WWSistemaDS_17_Sistema_tecnica2 ,
                                             String AV90WWSistemaDS_18_Sistema_coordenacao2 ,
                                             String AV91WWSistemaDS_19_Sistema_projetonome2 ,
                                             String AV94WWSistemaDS_22_Tfsistema_sigla_sel ,
                                             String AV93WWSistemaDS_21_Tfsistema_sigla ,
                                             int AV95WWSistemaDS_23_Tfsistema_tecnica_sels_Count ,
                                             String AV97WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                             String AV96WWSistemaDS_24_Tfsistema_coordenacao ,
                                             String AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                             String AV98WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                             String AV101WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                             String AV100WWSistemaDS_28_Tfmetodologia_descricao ,
                                             String AV103WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                             String AV102WWSistemaDS_30_Tfsistema_projetonome ,
                                             String AV105WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                             String AV104WWSistemaDS_32_Tfsistemaversao_id ,
                                             short AV106WWSistemaDS_34_Tfsistema_ativo_sel ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A699Sistema_Tipo ,
                                             String A513Sistema_Coordenacao ,
                                             String A703Sistema_ProjetoNome ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             String A1860SistemaVersao_Id ,
                                             bool A130Sistema_Ativo ,
                                             short AV75WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                             decimal AV82WWSistemaDS_10_Sistema_pf1 ,
                                             decimal A395Sistema_PF ,
                                             short AV85WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                             decimal AV92WWSistemaDS_20_Sistema_pf2 ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [25] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[SistemaVersao_Codigo], T1.[Sistema_ProjetoCod] AS Sistema_ProjetoCod, T1.[AmbienteTecnologico_Codigo], T1.[Sistema_Ativo], T3.[SistemaVersao_Id], T2.[Metodologia_Descricao], T5.[AmbienteTecnologico_Descricao], T4.[Projeto_Nome] AS Sistema_ProjetoNome, T1.[Sistema_Coordenacao], T1.[Sistema_Tecnica], T1.[Sistema_Tipo], T1.[Sistema_Sigla], T1.[Sistema_Nome], T1.[Sistema_AreaTrabalhoCod], T1.[Sistema_Codigo] FROM (((([Sistema] T1 WITH (NOLOCK) LEFT JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [SistemaVersao] T3 WITH (NOLOCK) ON T3.[SistemaVersao_Codigo] = T1.[SistemaVersao_Codigo]) LEFT JOIN [Projeto] T4 WITH (NOLOCK) ON T4.[Projeto_Codigo] = T1.[Sistema_ProjetoCod]) LEFT JOIN [AmbienteTecnologico] T5 WITH (NOLOCK) ON T5.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWSistemaDS_4_Sistema_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV76WWSistemaDS_4_Sistema_nome1)";
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWSistemaDS_5_Sistema_sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV77WWSistemaDS_5_Sistema_sigla1)";
         }
         else
         {
            GXv_int6[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWSistemaDS_6_Sistema_tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV78WWSistemaDS_6_Sistema_tipo1)";
         }
         else
         {
            GXv_int6[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWSistemaDS_7_Sistema_tecnica1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV79WWSistemaDS_7_Sistema_tecnica1)";
         }
         else
         {
            GXv_int6[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWSistemaDS_8_Sistema_coordenacao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV80WWSistemaDS_8_Sistema_coordenacao1 + '%')";
         }
         else
         {
            GXv_int6[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWSistemaDS_9_Sistema_projetonome1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Projeto_Nome] like '%' + @lV81WWSistemaDS_9_Sistema_projetonome1 + '%')";
         }
         else
         {
            GXv_int6[6] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWSistemaDS_14_Sistema_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV86WWSistemaDS_14_Sistema_nome2)";
         }
         else
         {
            GXv_int6[7] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWSistemaDS_15_Sistema_sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV87WWSistemaDS_15_Sistema_sigla2)";
         }
         else
         {
            GXv_int6[8] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWSistemaDS_16_Sistema_tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV88WWSistemaDS_16_Sistema_tipo2)";
         }
         else
         {
            GXv_int6[9] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWSistemaDS_17_Sistema_tecnica2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV89WWSistemaDS_17_Sistema_tecnica2)";
         }
         else
         {
            GXv_int6[10] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWSistemaDS_18_Sistema_coordenacao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV90WWSistemaDS_18_Sistema_coordenacao2 + '%')";
         }
         else
         {
            GXv_int6[11] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWSistemaDS_19_Sistema_projetonome2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Projeto_Nome] like '%' + @lV91WWSistemaDS_19_Sistema_projetonome2 + '%')";
         }
         else
         {
            GXv_int6[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWSistemaDS_22_Tfsistema_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWSistemaDS_21_Tfsistema_sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV93WWSistemaDS_21_Tfsistema_sigla)";
         }
         else
         {
            GXv_int6[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWSistemaDS_22_Tfsistema_sigla_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV94WWSistemaDS_22_Tfsistema_sigla_sel)";
         }
         else
         {
            GXv_int6[14] = 1;
         }
         if ( AV95WWSistemaDS_23_Tfsistema_tecnica_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV95WWSistemaDS_23_Tfsistema_tecnica_sels, "T1.[Sistema_Tecnica] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWSistemaDS_24_Tfsistema_coordenacao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like @lV96WWSistemaDS_24_Tfsistema_coordenacao)";
         }
         else
         {
            GXv_int6[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] = @AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)";
         }
         else
         {
            GXv_int6[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWSistemaDS_26_Tfambientetecnologico_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T5.[AmbienteTecnologico_Descricao] like @lV98WWSistemaDS_26_Tfambientetecnologico_descricao)";
         }
         else
         {
            GXv_int6[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[AmbienteTecnologico_Descricao] = @AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)";
         }
         else
         {
            GXv_int6[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWSistemaDS_28_Tfmetodologia_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV100WWSistemaDS_28_Tfmetodologia_descricao)";
         }
         else
         {
            GXv_int6[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)";
         }
         else
         {
            GXv_int6[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWSistemaDS_31_Tfsistema_projetonome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWSistemaDS_30_Tfsistema_projetonome)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Projeto_Nome] like @lV102WWSistemaDS_30_Tfsistema_projetonome)";
         }
         else
         {
            GXv_int6[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWSistemaDS_31_Tfsistema_projetonome_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Projeto_Nome] = @AV103WWSistemaDS_31_Tfsistema_projetonome_sel)";
         }
         else
         {
            GXv_int6[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWSistemaDS_33_Tfsistemaversao_id_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWSistemaDS_32_Tfsistemaversao_id)) ) )
         {
            sWhereString = sWhereString + " and (T3.[SistemaVersao_Id] like @lV104WWSistemaDS_32_Tfsistemaversao_id)";
         }
         else
         {
            GXv_int6[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWSistemaDS_33_Tfsistemaversao_id_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[SistemaVersao_Id] = @AV105WWSistemaDS_33_Tfsistemaversao_id_sel)";
         }
         else
         {
            GXv_int6[24] = 1;
         }
         if ( AV106WWSistemaDS_34_Tfsistema_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 1)";
         }
         if ( AV106WWSistemaDS_34_Tfsistema_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[AmbienteTecnologico_Codigo]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      protected Object[] conditional_P00FR5( IGxContext context ,
                                             String A700Sistema_Tecnica ,
                                             IGxCollection AV95WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                             String AV74WWSistemaDS_2_Dynamicfiltersselector1 ,
                                             String AV76WWSistemaDS_4_Sistema_nome1 ,
                                             String AV77WWSistemaDS_5_Sistema_sigla1 ,
                                             String AV78WWSistemaDS_6_Sistema_tipo1 ,
                                             String AV79WWSistemaDS_7_Sistema_tecnica1 ,
                                             String AV80WWSistemaDS_8_Sistema_coordenacao1 ,
                                             String AV81WWSistemaDS_9_Sistema_projetonome1 ,
                                             bool AV83WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                             String AV84WWSistemaDS_12_Dynamicfiltersselector2 ,
                                             String AV86WWSistemaDS_14_Sistema_nome2 ,
                                             String AV87WWSistemaDS_15_Sistema_sigla2 ,
                                             String AV88WWSistemaDS_16_Sistema_tipo2 ,
                                             String AV89WWSistemaDS_17_Sistema_tecnica2 ,
                                             String AV90WWSistemaDS_18_Sistema_coordenacao2 ,
                                             String AV91WWSistemaDS_19_Sistema_projetonome2 ,
                                             String AV94WWSistemaDS_22_Tfsistema_sigla_sel ,
                                             String AV93WWSistemaDS_21_Tfsistema_sigla ,
                                             int AV95WWSistemaDS_23_Tfsistema_tecnica_sels_Count ,
                                             String AV97WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                             String AV96WWSistemaDS_24_Tfsistema_coordenacao ,
                                             String AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                             String AV98WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                             String AV101WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                             String AV100WWSistemaDS_28_Tfmetodologia_descricao ,
                                             String AV103WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                             String AV102WWSistemaDS_30_Tfsistema_projetonome ,
                                             String AV105WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                             String AV104WWSistemaDS_32_Tfsistemaversao_id ,
                                             short AV106WWSistemaDS_34_Tfsistema_ativo_sel ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A699Sistema_Tipo ,
                                             String A513Sistema_Coordenacao ,
                                             String A703Sistema_ProjetoNome ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             String A1860SistemaVersao_Id ,
                                             bool A130Sistema_Ativo ,
                                             short AV75WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                             decimal AV82WWSistemaDS_10_Sistema_pf1 ,
                                             decimal A395Sistema_PF ,
                                             short AV85WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                             decimal AV92WWSistemaDS_20_Sistema_pf2 ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int8 ;
         GXv_int8 = new short [25] ;
         Object[] GXv_Object9 ;
         GXv_Object9 = new Object [2] ;
         scmdbuf = "SELECT T1.[AmbienteTecnologico_Codigo], T1.[SistemaVersao_Codigo], T1.[Sistema_ProjetoCod] AS Sistema_ProjetoCod, T1.[Metodologia_Codigo], T1.[Sistema_Ativo], T3.[SistemaVersao_Id], T5.[Metodologia_Descricao], T2.[AmbienteTecnologico_Descricao], T4.[Projeto_Nome] AS Sistema_ProjetoNome, T1.[Sistema_Coordenacao], T1.[Sistema_Tecnica], T1.[Sistema_Tipo], T1.[Sistema_Sigla], T1.[Sistema_Nome], T1.[Sistema_AreaTrabalhoCod], T1.[Sistema_Codigo] FROM (((([Sistema] T1 WITH (NOLOCK) LEFT JOIN [AmbienteTecnologico] T2 WITH (NOLOCK) ON T2.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo]) LEFT JOIN [SistemaVersao] T3 WITH (NOLOCK) ON T3.[SistemaVersao_Codigo] = T1.[SistemaVersao_Codigo]) LEFT JOIN [Projeto] T4 WITH (NOLOCK) ON T4.[Projeto_Codigo] = T1.[Sistema_ProjetoCod]) LEFT JOIN [Metodologia] T5 WITH (NOLOCK) ON T5.[Metodologia_Codigo] = T1.[Metodologia_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWSistemaDS_4_Sistema_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV76WWSistemaDS_4_Sistema_nome1)";
         }
         else
         {
            GXv_int8[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWSistemaDS_5_Sistema_sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV77WWSistemaDS_5_Sistema_sigla1)";
         }
         else
         {
            GXv_int8[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWSistemaDS_6_Sistema_tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV78WWSistemaDS_6_Sistema_tipo1)";
         }
         else
         {
            GXv_int8[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWSistemaDS_7_Sistema_tecnica1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV79WWSistemaDS_7_Sistema_tecnica1)";
         }
         else
         {
            GXv_int8[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWSistemaDS_8_Sistema_coordenacao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV80WWSistemaDS_8_Sistema_coordenacao1 + '%')";
         }
         else
         {
            GXv_int8[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWSistemaDS_9_Sistema_projetonome1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Projeto_Nome] like '%' + @lV81WWSistemaDS_9_Sistema_projetonome1 + '%')";
         }
         else
         {
            GXv_int8[6] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWSistemaDS_14_Sistema_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV86WWSistemaDS_14_Sistema_nome2)";
         }
         else
         {
            GXv_int8[7] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWSistemaDS_15_Sistema_sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV87WWSistemaDS_15_Sistema_sigla2)";
         }
         else
         {
            GXv_int8[8] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWSistemaDS_16_Sistema_tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV88WWSistemaDS_16_Sistema_tipo2)";
         }
         else
         {
            GXv_int8[9] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWSistemaDS_17_Sistema_tecnica2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV89WWSistemaDS_17_Sistema_tecnica2)";
         }
         else
         {
            GXv_int8[10] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWSistemaDS_18_Sistema_coordenacao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV90WWSistemaDS_18_Sistema_coordenacao2 + '%')";
         }
         else
         {
            GXv_int8[11] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWSistemaDS_19_Sistema_projetonome2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Projeto_Nome] like '%' + @lV91WWSistemaDS_19_Sistema_projetonome2 + '%')";
         }
         else
         {
            GXv_int8[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWSistemaDS_22_Tfsistema_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWSistemaDS_21_Tfsistema_sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV93WWSistemaDS_21_Tfsistema_sigla)";
         }
         else
         {
            GXv_int8[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWSistemaDS_22_Tfsistema_sigla_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV94WWSistemaDS_22_Tfsistema_sigla_sel)";
         }
         else
         {
            GXv_int8[14] = 1;
         }
         if ( AV95WWSistemaDS_23_Tfsistema_tecnica_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV95WWSistemaDS_23_Tfsistema_tecnica_sels, "T1.[Sistema_Tecnica] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWSistemaDS_24_Tfsistema_coordenacao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like @lV96WWSistemaDS_24_Tfsistema_coordenacao)";
         }
         else
         {
            GXv_int8[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] = @AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)";
         }
         else
         {
            GXv_int8[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWSistemaDS_26_Tfambientetecnologico_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] like @lV98WWSistemaDS_26_Tfambientetecnologico_descricao)";
         }
         else
         {
            GXv_int8[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] = @AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)";
         }
         else
         {
            GXv_int8[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWSistemaDS_28_Tfmetodologia_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Metodologia_Descricao] like @lV100WWSistemaDS_28_Tfmetodologia_descricao)";
         }
         else
         {
            GXv_int8[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Metodologia_Descricao] = @AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)";
         }
         else
         {
            GXv_int8[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWSistemaDS_31_Tfsistema_projetonome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWSistemaDS_30_Tfsistema_projetonome)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Projeto_Nome] like @lV102WWSistemaDS_30_Tfsistema_projetonome)";
         }
         else
         {
            GXv_int8[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWSistemaDS_31_Tfsistema_projetonome_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Projeto_Nome] = @AV103WWSistemaDS_31_Tfsistema_projetonome_sel)";
         }
         else
         {
            GXv_int8[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWSistemaDS_33_Tfsistemaversao_id_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWSistemaDS_32_Tfsistemaversao_id)) ) )
         {
            sWhereString = sWhereString + " and (T3.[SistemaVersao_Id] like @lV104WWSistemaDS_32_Tfsistemaversao_id)";
         }
         else
         {
            GXv_int8[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWSistemaDS_33_Tfsistemaversao_id_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[SistemaVersao_Id] = @AV105WWSistemaDS_33_Tfsistemaversao_id_sel)";
         }
         else
         {
            GXv_int8[24] = 1;
         }
         if ( AV106WWSistemaDS_34_Tfsistema_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 1)";
         }
         if ( AV106WWSistemaDS_34_Tfsistema_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Metodologia_Codigo]";
         GXv_Object9[0] = scmdbuf;
         GXv_Object9[1] = GXv_int8;
         return GXv_Object9 ;
      }

      protected Object[] conditional_P00FR6( IGxContext context ,
                                             String A700Sistema_Tecnica ,
                                             IGxCollection AV95WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                             String AV74WWSistemaDS_2_Dynamicfiltersselector1 ,
                                             String AV76WWSistemaDS_4_Sistema_nome1 ,
                                             String AV77WWSistemaDS_5_Sistema_sigla1 ,
                                             String AV78WWSistemaDS_6_Sistema_tipo1 ,
                                             String AV79WWSistemaDS_7_Sistema_tecnica1 ,
                                             String AV80WWSistemaDS_8_Sistema_coordenacao1 ,
                                             String AV81WWSistemaDS_9_Sistema_projetonome1 ,
                                             bool AV83WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                             String AV84WWSistemaDS_12_Dynamicfiltersselector2 ,
                                             String AV86WWSistemaDS_14_Sistema_nome2 ,
                                             String AV87WWSistemaDS_15_Sistema_sigla2 ,
                                             String AV88WWSistemaDS_16_Sistema_tipo2 ,
                                             String AV89WWSistemaDS_17_Sistema_tecnica2 ,
                                             String AV90WWSistemaDS_18_Sistema_coordenacao2 ,
                                             String AV91WWSistemaDS_19_Sistema_projetonome2 ,
                                             String AV94WWSistemaDS_22_Tfsistema_sigla_sel ,
                                             String AV93WWSistemaDS_21_Tfsistema_sigla ,
                                             int AV95WWSistemaDS_23_Tfsistema_tecnica_sels_Count ,
                                             String AV97WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                             String AV96WWSistemaDS_24_Tfsistema_coordenacao ,
                                             String AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                             String AV98WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                             String AV101WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                             String AV100WWSistemaDS_28_Tfmetodologia_descricao ,
                                             String AV103WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                             String AV102WWSistemaDS_30_Tfsistema_projetonome ,
                                             String AV105WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                             String AV104WWSistemaDS_32_Tfsistemaversao_id ,
                                             short AV106WWSistemaDS_34_Tfsistema_ativo_sel ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A699Sistema_Tipo ,
                                             String A513Sistema_Coordenacao ,
                                             String A703Sistema_ProjetoNome ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             String A1860SistemaVersao_Id ,
                                             bool A130Sistema_Ativo ,
                                             short AV75WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                             decimal AV82WWSistemaDS_10_Sistema_pf1 ,
                                             decimal A395Sistema_PF ,
                                             short AV85WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                             decimal AV92WWSistemaDS_20_Sistema_pf2 ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int10 ;
         GXv_int10 = new short [25] ;
         Object[] GXv_Object11 ;
         GXv_Object11 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[AmbienteTecnologico_Codigo], T1.[SistemaVersao_Codigo], T1.[Sistema_ProjetoCod] AS Sistema_ProjetoCod, T1.[Sistema_Ativo], T4.[SistemaVersao_Id], T2.[Metodologia_Descricao], T3.[AmbienteTecnologico_Descricao], T5.[Projeto_Nome] AS Sistema_ProjetoNome, T1.[Sistema_Coordenacao], T1.[Sistema_Tecnica], T1.[Sistema_Tipo], T1.[Sistema_Sigla], T1.[Sistema_Nome], T1.[Sistema_AreaTrabalhoCod], T1.[Sistema_Codigo] FROM (((([Sistema] T1 WITH (NOLOCK) LEFT JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo]) LEFT JOIN [SistemaVersao] T4 WITH (NOLOCK) ON T4.[SistemaVersao_Codigo] = T1.[SistemaVersao_Codigo]) LEFT JOIN [Projeto] T5 WITH (NOLOCK) ON T5.[Projeto_Codigo] = T1.[Sistema_ProjetoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWSistemaDS_4_Sistema_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV76WWSistemaDS_4_Sistema_nome1)";
         }
         else
         {
            GXv_int10[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWSistemaDS_5_Sistema_sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV77WWSistemaDS_5_Sistema_sigla1)";
         }
         else
         {
            GXv_int10[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWSistemaDS_6_Sistema_tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV78WWSistemaDS_6_Sistema_tipo1)";
         }
         else
         {
            GXv_int10[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWSistemaDS_7_Sistema_tecnica1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV79WWSistemaDS_7_Sistema_tecnica1)";
         }
         else
         {
            GXv_int10[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWSistemaDS_8_Sistema_coordenacao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV80WWSistemaDS_8_Sistema_coordenacao1 + '%')";
         }
         else
         {
            GXv_int10[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWSistemaDS_9_Sistema_projetonome1)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like '%' + @lV81WWSistemaDS_9_Sistema_projetonome1 + '%')";
         }
         else
         {
            GXv_int10[6] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWSistemaDS_14_Sistema_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV86WWSistemaDS_14_Sistema_nome2)";
         }
         else
         {
            GXv_int10[7] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWSistemaDS_15_Sistema_sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV87WWSistemaDS_15_Sistema_sigla2)";
         }
         else
         {
            GXv_int10[8] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWSistemaDS_16_Sistema_tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV88WWSistemaDS_16_Sistema_tipo2)";
         }
         else
         {
            GXv_int10[9] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWSistemaDS_17_Sistema_tecnica2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV89WWSistemaDS_17_Sistema_tecnica2)";
         }
         else
         {
            GXv_int10[10] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWSistemaDS_18_Sistema_coordenacao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV90WWSistemaDS_18_Sistema_coordenacao2 + '%')";
         }
         else
         {
            GXv_int10[11] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWSistemaDS_19_Sistema_projetonome2)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like '%' + @lV91WWSistemaDS_19_Sistema_projetonome2 + '%')";
         }
         else
         {
            GXv_int10[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWSistemaDS_22_Tfsistema_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWSistemaDS_21_Tfsistema_sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV93WWSistemaDS_21_Tfsistema_sigla)";
         }
         else
         {
            GXv_int10[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWSistemaDS_22_Tfsistema_sigla_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV94WWSistemaDS_22_Tfsistema_sigla_sel)";
         }
         else
         {
            GXv_int10[14] = 1;
         }
         if ( AV95WWSistemaDS_23_Tfsistema_tecnica_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV95WWSistemaDS_23_Tfsistema_tecnica_sels, "T1.[Sistema_Tecnica] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWSistemaDS_24_Tfsistema_coordenacao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like @lV96WWSistemaDS_24_Tfsistema_coordenacao)";
         }
         else
         {
            GXv_int10[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] = @AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)";
         }
         else
         {
            GXv_int10[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWSistemaDS_26_Tfambientetecnologico_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] like @lV98WWSistemaDS_26_Tfambientetecnologico_descricao)";
         }
         else
         {
            GXv_int10[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] = @AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)";
         }
         else
         {
            GXv_int10[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWSistemaDS_28_Tfmetodologia_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV100WWSistemaDS_28_Tfmetodologia_descricao)";
         }
         else
         {
            GXv_int10[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)";
         }
         else
         {
            GXv_int10[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWSistemaDS_31_Tfsistema_projetonome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWSistemaDS_30_Tfsistema_projetonome)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like @lV102WWSistemaDS_30_Tfsistema_projetonome)";
         }
         else
         {
            GXv_int10[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWSistemaDS_31_Tfsistema_projetonome_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] = @AV103WWSistemaDS_31_Tfsistema_projetonome_sel)";
         }
         else
         {
            GXv_int10[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWSistemaDS_33_Tfsistemaversao_id_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWSistemaDS_32_Tfsistemaversao_id)) ) )
         {
            sWhereString = sWhereString + " and (T4.[SistemaVersao_Id] like @lV104WWSistemaDS_32_Tfsistemaversao_id)";
         }
         else
         {
            GXv_int10[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWSistemaDS_33_Tfsistemaversao_id_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[SistemaVersao_Id] = @AV105WWSistemaDS_33_Tfsistemaversao_id_sel)";
         }
         else
         {
            GXv_int10[24] = 1;
         }
         if ( AV106WWSistemaDS_34_Tfsistema_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 1)";
         }
         if ( AV106WWSistemaDS_34_Tfsistema_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_ProjetoCod]";
         GXv_Object11[0] = scmdbuf;
         GXv_Object11[1] = GXv_int10;
         return GXv_Object11 ;
      }

      protected Object[] conditional_P00FR7( IGxContext context ,
                                             String A700Sistema_Tecnica ,
                                             IGxCollection AV95WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                             String AV74WWSistemaDS_2_Dynamicfiltersselector1 ,
                                             String AV76WWSistemaDS_4_Sistema_nome1 ,
                                             String AV77WWSistemaDS_5_Sistema_sigla1 ,
                                             String AV78WWSistemaDS_6_Sistema_tipo1 ,
                                             String AV79WWSistemaDS_7_Sistema_tecnica1 ,
                                             String AV80WWSistemaDS_8_Sistema_coordenacao1 ,
                                             String AV81WWSistemaDS_9_Sistema_projetonome1 ,
                                             bool AV83WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                             String AV84WWSistemaDS_12_Dynamicfiltersselector2 ,
                                             String AV86WWSistemaDS_14_Sistema_nome2 ,
                                             String AV87WWSistemaDS_15_Sistema_sigla2 ,
                                             String AV88WWSistemaDS_16_Sistema_tipo2 ,
                                             String AV89WWSistemaDS_17_Sistema_tecnica2 ,
                                             String AV90WWSistemaDS_18_Sistema_coordenacao2 ,
                                             String AV91WWSistemaDS_19_Sistema_projetonome2 ,
                                             String AV94WWSistemaDS_22_Tfsistema_sigla_sel ,
                                             String AV93WWSistemaDS_21_Tfsistema_sigla ,
                                             int AV95WWSistemaDS_23_Tfsistema_tecnica_sels_Count ,
                                             String AV97WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                             String AV96WWSistemaDS_24_Tfsistema_coordenacao ,
                                             String AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                             String AV98WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                             String AV101WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                             String AV100WWSistemaDS_28_Tfmetodologia_descricao ,
                                             String AV103WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                             String AV102WWSistemaDS_30_Tfsistema_projetonome ,
                                             String AV105WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                             String AV104WWSistemaDS_32_Tfsistemaversao_id ,
                                             short AV106WWSistemaDS_34_Tfsistema_ativo_sel ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A699Sistema_Tipo ,
                                             String A513Sistema_Coordenacao ,
                                             String A703Sistema_ProjetoNome ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             String A1860SistemaVersao_Id ,
                                             bool A130Sistema_Ativo ,
                                             short AV75WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                             decimal AV82WWSistemaDS_10_Sistema_pf1 ,
                                             decimal A395Sistema_PF ,
                                             short AV85WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                             decimal AV92WWSistemaDS_20_Sistema_pf2 ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int12 ;
         GXv_int12 = new short [25] ;
         Object[] GXv_Object13 ;
         GXv_Object13 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[AmbienteTecnologico_Codigo], T1.[Sistema_ProjetoCod] AS Sistema_ProjetoCod, T1.[SistemaVersao_Codigo], T1.[Sistema_Ativo], T5.[SistemaVersao_Id], T2.[Metodologia_Descricao], T3.[AmbienteTecnologico_Descricao], T4.[Projeto_Nome] AS Sistema_ProjetoNome, T1.[Sistema_Coordenacao], T1.[Sistema_Tecnica], T1.[Sistema_Tipo], T1.[Sistema_Sigla], T1.[Sistema_Nome], T1.[Sistema_AreaTrabalhoCod], T1.[Sistema_Codigo] FROM (((([Sistema] T1 WITH (NOLOCK) LEFT JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo]) LEFT JOIN [Projeto] T4 WITH (NOLOCK) ON T4.[Projeto_Codigo] = T1.[Sistema_ProjetoCod]) LEFT JOIN [SistemaVersao] T5 WITH (NOLOCK) ON T5.[SistemaVersao_Codigo] = T1.[SistemaVersao_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWSistemaDS_4_Sistema_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV76WWSistemaDS_4_Sistema_nome1)";
         }
         else
         {
            GXv_int12[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWSistemaDS_5_Sistema_sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV77WWSistemaDS_5_Sistema_sigla1)";
         }
         else
         {
            GXv_int12[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWSistemaDS_6_Sistema_tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV78WWSistemaDS_6_Sistema_tipo1)";
         }
         else
         {
            GXv_int12[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWSistemaDS_7_Sistema_tecnica1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV79WWSistemaDS_7_Sistema_tecnica1)";
         }
         else
         {
            GXv_int12[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWSistemaDS_8_Sistema_coordenacao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV80WWSistemaDS_8_Sistema_coordenacao1 + '%')";
         }
         else
         {
            GXv_int12[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV74WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWSistemaDS_9_Sistema_projetonome1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Projeto_Nome] like '%' + @lV81WWSistemaDS_9_Sistema_projetonome1 + '%')";
         }
         else
         {
            GXv_int12[6] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWSistemaDS_14_Sistema_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV86WWSistemaDS_14_Sistema_nome2)";
         }
         else
         {
            GXv_int12[7] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWSistemaDS_15_Sistema_sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV87WWSistemaDS_15_Sistema_sigla2)";
         }
         else
         {
            GXv_int12[8] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWSistemaDS_16_Sistema_tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV88WWSistemaDS_16_Sistema_tipo2)";
         }
         else
         {
            GXv_int12[9] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWSistemaDS_17_Sistema_tecnica2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV89WWSistemaDS_17_Sistema_tecnica2)";
         }
         else
         {
            GXv_int12[10] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWSistemaDS_18_Sistema_coordenacao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV90WWSistemaDS_18_Sistema_coordenacao2 + '%')";
         }
         else
         {
            GXv_int12[11] = 1;
         }
         if ( AV83WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV84WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWSistemaDS_19_Sistema_projetonome2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Projeto_Nome] like '%' + @lV91WWSistemaDS_19_Sistema_projetonome2 + '%')";
         }
         else
         {
            GXv_int12[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWSistemaDS_22_Tfsistema_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWSistemaDS_21_Tfsistema_sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV93WWSistemaDS_21_Tfsistema_sigla)";
         }
         else
         {
            GXv_int12[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWSistemaDS_22_Tfsistema_sigla_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV94WWSistemaDS_22_Tfsistema_sigla_sel)";
         }
         else
         {
            GXv_int12[14] = 1;
         }
         if ( AV95WWSistemaDS_23_Tfsistema_tecnica_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV95WWSistemaDS_23_Tfsistema_tecnica_sels, "T1.[Sistema_Tecnica] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWSistemaDS_24_Tfsistema_coordenacao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like @lV96WWSistemaDS_24_Tfsistema_coordenacao)";
         }
         else
         {
            GXv_int12[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] = @AV97WWSistemaDS_25_Tfsistema_coordenacao_sel)";
         }
         else
         {
            GXv_int12[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWSistemaDS_26_Tfambientetecnologico_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] like @lV98WWSistemaDS_26_Tfambientetecnologico_descricao)";
         }
         else
         {
            GXv_int12[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] = @AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel)";
         }
         else
         {
            GXv_int12[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWSistemaDS_28_Tfmetodologia_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV100WWSistemaDS_28_Tfmetodologia_descricao)";
         }
         else
         {
            GXv_int12[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV101WWSistemaDS_29_Tfmetodologia_descricao_sel)";
         }
         else
         {
            GXv_int12[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV103WWSistemaDS_31_Tfsistema_projetonome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102WWSistemaDS_30_Tfsistema_projetonome)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Projeto_Nome] like @lV102WWSistemaDS_30_Tfsistema_projetonome)";
         }
         else
         {
            GXv_int12[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV103WWSistemaDS_31_Tfsistema_projetonome_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Projeto_Nome] = @AV103WWSistemaDS_31_Tfsistema_projetonome_sel)";
         }
         else
         {
            GXv_int12[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWSistemaDS_33_Tfsistemaversao_id_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWSistemaDS_32_Tfsistemaversao_id)) ) )
         {
            sWhereString = sWhereString + " and (T5.[SistemaVersao_Id] like @lV104WWSistemaDS_32_Tfsistemaversao_id)";
         }
         else
         {
            GXv_int12[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWSistemaDS_33_Tfsistemaversao_id_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[SistemaVersao_Id] = @AV105WWSistemaDS_33_Tfsistemaversao_id_sel)";
         }
         else
         {
            GXv_int12[24] = 1;
         }
         if ( AV106WWSistemaDS_34_Tfsistema_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 1)";
         }
         if ( AV106WWSistemaDS_34_Tfsistema_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[SistemaVersao_Codigo]";
         GXv_Object13[0] = scmdbuf;
         GXv_Object13[1] = GXv_int12;
         return GXv_Object13 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00FR2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (bool)dynConstraints[39] , (short)dynConstraints[40] , (decimal)dynConstraints[41] , (decimal)dynConstraints[42] , (short)dynConstraints[43] , (decimal)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] );
               case 1 :
                     return conditional_P00FR3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (bool)dynConstraints[39] , (short)dynConstraints[40] , (decimal)dynConstraints[41] , (decimal)dynConstraints[42] , (short)dynConstraints[43] , (decimal)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] );
               case 2 :
                     return conditional_P00FR4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (bool)dynConstraints[39] , (short)dynConstraints[40] , (decimal)dynConstraints[41] , (decimal)dynConstraints[42] , (short)dynConstraints[43] , (decimal)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] );
               case 3 :
                     return conditional_P00FR5(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (bool)dynConstraints[39] , (short)dynConstraints[40] , (decimal)dynConstraints[41] , (decimal)dynConstraints[42] , (short)dynConstraints[43] , (decimal)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] );
               case 4 :
                     return conditional_P00FR6(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (bool)dynConstraints[39] , (short)dynConstraints[40] , (decimal)dynConstraints[41] , (decimal)dynConstraints[42] , (short)dynConstraints[43] , (decimal)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] );
               case 5 :
                     return conditional_P00FR7(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (bool)dynConstraints[39] , (short)dynConstraints[40] , (decimal)dynConstraints[41] , (decimal)dynConstraints[42] , (short)dynConstraints[43] , (decimal)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00FR2 ;
          prmP00FR2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV76WWSistemaDS_4_Sistema_nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV77WWSistemaDS_5_Sistema_sigla1",SqlDbType.Char,25,0} ,
          new Object[] {"@AV78WWSistemaDS_6_Sistema_tipo1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV79WWSistemaDS_7_Sistema_tecnica1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWSistemaDS_8_Sistema_coordenacao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWSistemaDS_9_Sistema_projetonome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV86WWSistemaDS_14_Sistema_nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV87WWSistemaDS_15_Sistema_sigla2",SqlDbType.Char,25,0} ,
          new Object[] {"@AV88WWSistemaDS_16_Sistema_tipo2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV89WWSistemaDS_17_Sistema_tecnica2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV90WWSistemaDS_18_Sistema_coordenacao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91WWSistemaDS_19_Sistema_projetonome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWSistemaDS_21_Tfsistema_sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV94WWSistemaDS_22_Tfsistema_sigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV96WWSistemaDS_24_Tfsistema_coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV97WWSistemaDS_25_Tfsistema_coordenacao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWSistemaDS_26_Tfambientetecnologico_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV100WWSistemaDS_28_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WWSistemaDS_29_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV102WWSistemaDS_30_Tfsistema_projetonome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV103WWSistemaDS_31_Tfsistema_projetonome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWSistemaDS_32_Tfsistemaversao_id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV105WWSistemaDS_33_Tfsistemaversao_id_sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00FR3 ;
          prmP00FR3 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV76WWSistemaDS_4_Sistema_nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV77WWSistemaDS_5_Sistema_sigla1",SqlDbType.Char,25,0} ,
          new Object[] {"@AV78WWSistemaDS_6_Sistema_tipo1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV79WWSistemaDS_7_Sistema_tecnica1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWSistemaDS_8_Sistema_coordenacao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWSistemaDS_9_Sistema_projetonome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV86WWSistemaDS_14_Sistema_nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV87WWSistemaDS_15_Sistema_sigla2",SqlDbType.Char,25,0} ,
          new Object[] {"@AV88WWSistemaDS_16_Sistema_tipo2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV89WWSistemaDS_17_Sistema_tecnica2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV90WWSistemaDS_18_Sistema_coordenacao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91WWSistemaDS_19_Sistema_projetonome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWSistemaDS_21_Tfsistema_sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV94WWSistemaDS_22_Tfsistema_sigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV96WWSistemaDS_24_Tfsistema_coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV97WWSistemaDS_25_Tfsistema_coordenacao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWSistemaDS_26_Tfambientetecnologico_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV100WWSistemaDS_28_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WWSistemaDS_29_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV102WWSistemaDS_30_Tfsistema_projetonome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV103WWSistemaDS_31_Tfsistema_projetonome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWSistemaDS_32_Tfsistemaversao_id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV105WWSistemaDS_33_Tfsistemaversao_id_sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00FR4 ;
          prmP00FR4 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV76WWSistemaDS_4_Sistema_nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV77WWSistemaDS_5_Sistema_sigla1",SqlDbType.Char,25,0} ,
          new Object[] {"@AV78WWSistemaDS_6_Sistema_tipo1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV79WWSistemaDS_7_Sistema_tecnica1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWSistemaDS_8_Sistema_coordenacao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWSistemaDS_9_Sistema_projetonome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV86WWSistemaDS_14_Sistema_nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV87WWSistemaDS_15_Sistema_sigla2",SqlDbType.Char,25,0} ,
          new Object[] {"@AV88WWSistemaDS_16_Sistema_tipo2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV89WWSistemaDS_17_Sistema_tecnica2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV90WWSistemaDS_18_Sistema_coordenacao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91WWSistemaDS_19_Sistema_projetonome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWSistemaDS_21_Tfsistema_sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV94WWSistemaDS_22_Tfsistema_sigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV96WWSistemaDS_24_Tfsistema_coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV97WWSistemaDS_25_Tfsistema_coordenacao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWSistemaDS_26_Tfambientetecnologico_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV100WWSistemaDS_28_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WWSistemaDS_29_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV102WWSistemaDS_30_Tfsistema_projetonome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV103WWSistemaDS_31_Tfsistema_projetonome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWSistemaDS_32_Tfsistemaversao_id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV105WWSistemaDS_33_Tfsistemaversao_id_sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00FR5 ;
          prmP00FR5 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV76WWSistemaDS_4_Sistema_nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV77WWSistemaDS_5_Sistema_sigla1",SqlDbType.Char,25,0} ,
          new Object[] {"@AV78WWSistemaDS_6_Sistema_tipo1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV79WWSistemaDS_7_Sistema_tecnica1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWSistemaDS_8_Sistema_coordenacao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWSistemaDS_9_Sistema_projetonome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV86WWSistemaDS_14_Sistema_nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV87WWSistemaDS_15_Sistema_sigla2",SqlDbType.Char,25,0} ,
          new Object[] {"@AV88WWSistemaDS_16_Sistema_tipo2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV89WWSistemaDS_17_Sistema_tecnica2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV90WWSistemaDS_18_Sistema_coordenacao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91WWSistemaDS_19_Sistema_projetonome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWSistemaDS_21_Tfsistema_sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV94WWSistemaDS_22_Tfsistema_sigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV96WWSistemaDS_24_Tfsistema_coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV97WWSistemaDS_25_Tfsistema_coordenacao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWSistemaDS_26_Tfambientetecnologico_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV100WWSistemaDS_28_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WWSistemaDS_29_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV102WWSistemaDS_30_Tfsistema_projetonome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV103WWSistemaDS_31_Tfsistema_projetonome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWSistemaDS_32_Tfsistemaversao_id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV105WWSistemaDS_33_Tfsistemaversao_id_sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00FR6 ;
          prmP00FR6 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV76WWSistemaDS_4_Sistema_nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV77WWSistemaDS_5_Sistema_sigla1",SqlDbType.Char,25,0} ,
          new Object[] {"@AV78WWSistemaDS_6_Sistema_tipo1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV79WWSistemaDS_7_Sistema_tecnica1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWSistemaDS_8_Sistema_coordenacao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWSistemaDS_9_Sistema_projetonome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV86WWSistemaDS_14_Sistema_nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV87WWSistemaDS_15_Sistema_sigla2",SqlDbType.Char,25,0} ,
          new Object[] {"@AV88WWSistemaDS_16_Sistema_tipo2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV89WWSistemaDS_17_Sistema_tecnica2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV90WWSistemaDS_18_Sistema_coordenacao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91WWSistemaDS_19_Sistema_projetonome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWSistemaDS_21_Tfsistema_sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV94WWSistemaDS_22_Tfsistema_sigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV96WWSistemaDS_24_Tfsistema_coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV97WWSistemaDS_25_Tfsistema_coordenacao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWSistemaDS_26_Tfambientetecnologico_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV100WWSistemaDS_28_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WWSistemaDS_29_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV102WWSistemaDS_30_Tfsistema_projetonome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV103WWSistemaDS_31_Tfsistema_projetonome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWSistemaDS_32_Tfsistemaversao_id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV105WWSistemaDS_33_Tfsistemaversao_id_sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00FR7 ;
          prmP00FR7 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV76WWSistemaDS_4_Sistema_nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV77WWSistemaDS_5_Sistema_sigla1",SqlDbType.Char,25,0} ,
          new Object[] {"@AV78WWSistemaDS_6_Sistema_tipo1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV79WWSistemaDS_7_Sistema_tecnica1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV80WWSistemaDS_8_Sistema_coordenacao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWSistemaDS_9_Sistema_projetonome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV86WWSistemaDS_14_Sistema_nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV87WWSistemaDS_15_Sistema_sigla2",SqlDbType.Char,25,0} ,
          new Object[] {"@AV88WWSistemaDS_16_Sistema_tipo2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV89WWSistemaDS_17_Sistema_tecnica2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV90WWSistemaDS_18_Sistema_coordenacao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91WWSistemaDS_19_Sistema_projetonome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV93WWSistemaDS_21_Tfsistema_sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV94WWSistemaDS_22_Tfsistema_sigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV96WWSistemaDS_24_Tfsistema_coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV97WWSistemaDS_25_Tfsistema_coordenacao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWSistemaDS_26_Tfambientetecnologico_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV99WWSistemaDS_27_Tfambientetecnologico_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV100WWSistemaDS_28_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV101WWSistemaDS_29_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV102WWSistemaDS_30_Tfsistema_projetonome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV103WWSistemaDS_31_Tfsistema_projetonome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV104WWSistemaDS_32_Tfsistemaversao_id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV105WWSistemaDS_33_Tfsistemaversao_id_sel",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00FR2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FR2,100,0,true,false )
             ,new CursorDef("P00FR3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FR3,100,0,true,false )
             ,new CursorDef("P00FR4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FR4,100,0,true,false )
             ,new CursorDef("P00FR5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FR5,100,0,true,false )
             ,new CursorDef("P00FR6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FR6,100,0,true,false )
             ,new CursorDef("P00FR7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FR7,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 25) ;
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 20) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(9) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((String[]) buf[19])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((String[]) buf[21])[0] = rslt.getVarchar(14) ;
                ((int[]) buf[22])[0] = rslt.getInt(15) ;
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((bool[]) buf[11])[0] = rslt.getBool(7) ;
                ((String[]) buf[12])[0] = rslt.getString(8, 20) ;
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((String[]) buf[15])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((String[]) buf[19])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((String[]) buf[21])[0] = rslt.getString(14, 25) ;
                ((String[]) buf[22])[0] = rslt.getVarchar(15) ;
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 20) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 25) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(14) ;
                ((int[]) buf[22])[0] = rslt.getInt(15) ;
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 20) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 25) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(14) ;
                ((int[]) buf[22])[0] = rslt.getInt(15) ;
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 20) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 25) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(14) ;
                ((int[]) buf[22])[0] = rslt.getInt(15) ;
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 20) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 25) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(14) ;
                ((int[]) buf[22])[0] = rslt.getInt(15) ;
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwsistemafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwsistemafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwsistemafilterdata") )
          {
             return  ;
          }
          getwwsistemafilterdata worker = new getwwsistemafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
