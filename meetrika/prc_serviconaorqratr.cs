/*
               File: PRC_ServicoNaoRqrAtr
        Description: Servi�o Nao Requer Atribui��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:13.51
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_serviconaorqratr : GXProcedure
   {
      public prc_serviconaorqratr( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_serviconaorqratr( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratada_Codigo ,
                           ref int aP1_Servico_Codigo ,
                           out bool aP2_NaoRqrAtr )
      {
         this.AV9Contratada_Codigo = aP0_Contratada_Codigo;
         this.A155Servico_Codigo = aP1_Servico_Codigo;
         this.AV8NaoRqrAtr = false ;
         initialize();
         executePrivate();
         aP0_Contratada_Codigo=this.AV9Contratada_Codigo;
         aP1_Servico_Codigo=this.A155Servico_Codigo;
         aP2_NaoRqrAtr=this.AV8NaoRqrAtr;
      }

      public bool executeUdp( ref int aP0_Contratada_Codigo ,
                              ref int aP1_Servico_Codigo )
      {
         this.AV9Contratada_Codigo = aP0_Contratada_Codigo;
         this.A155Servico_Codigo = aP1_Servico_Codigo;
         this.AV8NaoRqrAtr = false ;
         initialize();
         executePrivate();
         aP0_Contratada_Codigo=this.AV9Contratada_Codigo;
         aP1_Servico_Codigo=this.A155Servico_Codigo;
         aP2_NaoRqrAtr=this.AV8NaoRqrAtr;
         return AV8NaoRqrAtr ;
      }

      public void executeSubmit( ref int aP0_Contratada_Codigo ,
                                 ref int aP1_Servico_Codigo ,
                                 out bool aP2_NaoRqrAtr )
      {
         prc_serviconaorqratr objprc_serviconaorqratr;
         objprc_serviconaorqratr = new prc_serviconaorqratr();
         objprc_serviconaorqratr.AV9Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_serviconaorqratr.A155Servico_Codigo = aP1_Servico_Codigo;
         objprc_serviconaorqratr.AV8NaoRqrAtr = false ;
         objprc_serviconaorqratr.context.SetSubmitInitialConfig(context);
         objprc_serviconaorqratr.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_serviconaorqratr);
         aP0_Contratada_Codigo=this.AV9Contratada_Codigo;
         aP1_Servico_Codigo=this.A155Servico_Codigo;
         aP2_NaoRqrAtr=this.AV8NaoRqrAtr;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_serviconaorqratr)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P009O2 */
         pr_default.execute(0, new Object[] {A155Servico_Codigo, AV9Contratada_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P009O2_A74Contrato_Codigo[0];
            A39Contratada_Codigo = P009O2_A39Contratada_Codigo[0];
            A1397ContratoServicos_NaoRequerAtr = P009O2_A1397ContratoServicos_NaoRequerAtr[0];
            n1397ContratoServicos_NaoRequerAtr = P009O2_n1397ContratoServicos_NaoRequerAtr[0];
            A160ContratoServicos_Codigo = P009O2_A160ContratoServicos_Codigo[0];
            A39Contratada_Codigo = P009O2_A39Contratada_Codigo[0];
            AV8NaoRqrAtr = A1397ContratoServicos_NaoRequerAtr;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009O2_A74Contrato_Codigo = new int[1] ;
         P009O2_A155Servico_Codigo = new int[1] ;
         P009O2_A39Contratada_Codigo = new int[1] ;
         P009O2_A1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         P009O2_n1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         P009O2_A160ContratoServicos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_serviconaorqratr__default(),
            new Object[][] {
                new Object[] {
               P009O2_A74Contrato_Codigo, P009O2_A155Servico_Codigo, P009O2_A39Contratada_Codigo, P009O2_A1397ContratoServicos_NaoRequerAtr, P009O2_n1397ContratoServicos_NaoRequerAtr, P009O2_A160ContratoServicos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9Contratada_Codigo ;
      private int A155Servico_Codigo ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private String scmdbuf ;
      private bool AV8NaoRqrAtr ;
      private bool A1397ContratoServicos_NaoRequerAtr ;
      private bool n1397ContratoServicos_NaoRequerAtr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratada_Codigo ;
      private int aP1_Servico_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P009O2_A74Contrato_Codigo ;
      private int[] P009O2_A155Servico_Codigo ;
      private int[] P009O2_A39Contratada_Codigo ;
      private bool[] P009O2_A1397ContratoServicos_NaoRequerAtr ;
      private bool[] P009O2_n1397ContratoServicos_NaoRequerAtr ;
      private int[] P009O2_A160ContratoServicos_Codigo ;
      private bool aP2_NaoRqrAtr ;
   }

   public class prc_serviconaorqratr__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009O2 ;
          prmP009O2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009O2", "SELECT TOP 1 T1.[Contrato_Codigo], T1.[Servico_Codigo], T2.[Contratada_Codigo], T1.[ContratoServicos_NaoRequerAtr], T1.[ContratoServicos_Codigo] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Servico_Codigo] = @Servico_Codigo) AND (T2.[Contratada_Codigo] = @AV9Contratada_Codigo) ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009O2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
