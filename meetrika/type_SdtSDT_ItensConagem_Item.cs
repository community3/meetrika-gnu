/*
               File: type_SdtSDT_ItensConagem_Item
        Description: SDT_ItensConagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:59.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_ItensConagem.Item" )]
   [XmlType(TypeName =  "SDT_ItensConagem.Item" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_ItensConagem_Item : GxUserType
   {
      public SdtSDT_ItensConagem_Item( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_ItensConagem_Item_Itemnome = "";
         gxTv_SdtSDT_ItensConagem_Item_Itemtipo = "";
         gxTv_SdtSDT_ItensConagem_Item_Itemtipofn = "";
         gxTv_SdtSDT_ItensConagem_Item_Itemobservacao = "";
         gxTv_SdtSDT_ItensConagem_Item_Itemnaomens = "";
      }

      public SdtSDT_ItensConagem_Item( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_ItensConagem_Item deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_ItensConagem_Item)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_ItensConagem_Item obj ;
         obj = this;
         obj.gxTpr_Itemcodigo = deserialized.gxTpr_Itemcodigo;
         obj.gxTpr_Itemnome = deserialized.gxTpr_Itemnome;
         obj.gxTpr_Itemtipo = deserialized.gxTpr_Itemtipo;
         obj.gxTpr_Itemtipofn = deserialized.gxTpr_Itemtipofn;
         obj.gxTpr_Itemobservacao = deserialized.gxTpr_Itemobservacao;
         obj.gxTpr_Itemnaomens = deserialized.gxTpr_Itemnaomens;
         obj.gxTpr_Itemvalor = deserialized.gxTpr_Itemvalor;
         obj.gxTpr_Itemtipoinm = deserialized.gxTpr_Itemtipoinm;
         obj.gxTpr_Itempfb = deserialized.gxTpr_Itempfb;
         obj.gxTpr_Itempfl = deserialized.gxTpr_Itempfl;
         obj.gxTpr_Itemcontar = deserialized.gxTpr_Itemcontar;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ItemCodigo") )
               {
                  gxTv_SdtSDT_ItensConagem_Item_Itemcodigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ItemNome") )
               {
                  gxTv_SdtSDT_ItensConagem_Item_Itemnome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ItemTipo") )
               {
                  gxTv_SdtSDT_ItensConagem_Item_Itemtipo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ItemTipoFn") )
               {
                  gxTv_SdtSDT_ItensConagem_Item_Itemtipofn = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ItemObservacao") )
               {
                  gxTv_SdtSDT_ItensConagem_Item_Itemobservacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ItemNaoMens") )
               {
                  gxTv_SdtSDT_ItensConagem_Item_Itemnaomens = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ItemValor") )
               {
                  gxTv_SdtSDT_ItensConagem_Item_Itemvalor = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ItemTipoINM") )
               {
                  gxTv_SdtSDT_ItensConagem_Item_Itemtipoinm = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ItemPFB") )
               {
                  gxTv_SdtSDT_ItensConagem_Item_Itempfb = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ItemPFL") )
               {
                  gxTv_SdtSDT_ItensConagem_Item_Itempfl = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ItemContar") )
               {
                  gxTv_SdtSDT_ItensConagem_Item_Itemcontar = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_ItensConagem.Item";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ItemCodigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ItensConagem_Item_Itemcodigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ItemNome", StringUtil.RTrim( gxTv_SdtSDT_ItensConagem_Item_Itemnome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ItemTipo", StringUtil.RTrim( gxTv_SdtSDT_ItensConagem_Item_Itemtipo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ItemTipoFn", StringUtil.RTrim( gxTv_SdtSDT_ItensConagem_Item_Itemtipofn));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ItemObservacao", StringUtil.RTrim( gxTv_SdtSDT_ItensConagem_Item_Itemobservacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ItemNaoMens", StringUtil.RTrim( gxTv_SdtSDT_ItensConagem_Item_Itemnaomens));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ItemValor", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_ItensConagem_Item_Itemvalor, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ItemTipoINM", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ItensConagem_Item_Itemtipoinm), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ItemPFB", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_ItensConagem_Item_Itempfb, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ItemPFL", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_ItensConagem_Item_Itempfl, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ItemContar", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_ItensConagem_Item_Itemcontar)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ItemCodigo", gxTv_SdtSDT_ItensConagem_Item_Itemcodigo, false);
         AddObjectProperty("ItemNome", gxTv_SdtSDT_ItensConagem_Item_Itemnome, false);
         AddObjectProperty("ItemTipo", gxTv_SdtSDT_ItensConagem_Item_Itemtipo, false);
         AddObjectProperty("ItemTipoFn", gxTv_SdtSDT_ItensConagem_Item_Itemtipofn, false);
         AddObjectProperty("ItemObservacao", gxTv_SdtSDT_ItensConagem_Item_Itemobservacao, false);
         AddObjectProperty("ItemNaoMens", gxTv_SdtSDT_ItensConagem_Item_Itemnaomens, false);
         AddObjectProperty("ItemValor", StringUtil.LTrim( StringUtil.Str( gxTv_SdtSDT_ItensConagem_Item_Itemvalor, 18, 5)), false);
         AddObjectProperty("ItemTipoINM", gxTv_SdtSDT_ItensConagem_Item_Itemtipoinm, false);
         AddObjectProperty("ItemPFB", gxTv_SdtSDT_ItensConagem_Item_Itempfb, false);
         AddObjectProperty("ItemPFL", gxTv_SdtSDT_ItensConagem_Item_Itempfl, false);
         AddObjectProperty("ItemContar", gxTv_SdtSDT_ItensConagem_Item_Itemcontar, false);
         return  ;
      }

      [  SoapElement( ElementName = "ItemCodigo" )]
      [  XmlElement( ElementName = "ItemCodigo"   )]
      public int gxTpr_Itemcodigo
      {
         get {
            return gxTv_SdtSDT_ItensConagem_Item_Itemcodigo ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itemcodigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ItemNome" )]
      [  XmlElement( ElementName = "ItemNome"   )]
      public String gxTpr_Itemnome
      {
         get {
            return gxTv_SdtSDT_ItensConagem_Item_Itemnome ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itemnome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ItemTipo" )]
      [  XmlElement( ElementName = "ItemTipo"   )]
      public String gxTpr_Itemtipo
      {
         get {
            return gxTv_SdtSDT_ItensConagem_Item_Itemtipo ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itemtipo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ItemTipoFn" )]
      [  XmlElement( ElementName = "ItemTipoFn"   )]
      public String gxTpr_Itemtipofn
      {
         get {
            return gxTv_SdtSDT_ItensConagem_Item_Itemtipofn ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itemtipofn = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ItemObservacao" )]
      [  XmlElement( ElementName = "ItemObservacao"   )]
      public String gxTpr_Itemobservacao
      {
         get {
            return gxTv_SdtSDT_ItensConagem_Item_Itemobservacao ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itemobservacao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ItemNaoMens" )]
      [  XmlElement( ElementName = "ItemNaoMens"   )]
      public String gxTpr_Itemnaomens
      {
         get {
            return gxTv_SdtSDT_ItensConagem_Item_Itemnaomens ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itemnaomens = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ItemValor" )]
      [  XmlElement( ElementName = "ItemValor"   )]
      public double gxTpr_Itemvalor_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_ItensConagem_Item_Itemvalor) ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itemvalor = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Itemvalor
      {
         get {
            return gxTv_SdtSDT_ItensConagem_Item_Itemvalor ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itemvalor = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ItemTipoINM" )]
      [  XmlElement( ElementName = "ItemTipoINM"   )]
      public short gxTpr_Itemtipoinm
      {
         get {
            return gxTv_SdtSDT_ItensConagem_Item_Itemtipoinm ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itemtipoinm = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ItemPFB" )]
      [  XmlElement( ElementName = "ItemPFB"   )]
      public double gxTpr_Itempfb_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_ItensConagem_Item_Itempfb) ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itempfb = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Itempfb
      {
         get {
            return gxTv_SdtSDT_ItensConagem_Item_Itempfb ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itempfb = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ItemPFL" )]
      [  XmlElement( ElementName = "ItemPFL"   )]
      public double gxTpr_Itempfl_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_ItensConagem_Item_Itempfl) ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itempfl = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Itempfl
      {
         get {
            return gxTv_SdtSDT_ItensConagem_Item_Itempfl ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itempfl = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ItemContar" )]
      [  XmlElement( ElementName = "ItemContar"   )]
      public bool gxTpr_Itemcontar
      {
         get {
            return gxTv_SdtSDT_ItensConagem_Item_Itemcontar ;
         }

         set {
            gxTv_SdtSDT_ItensConagem_Item_Itemcontar = value;
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_ItensConagem_Item_Itemnome = "";
         gxTv_SdtSDT_ItensConagem_Item_Itemtipo = "";
         gxTv_SdtSDT_ItensConagem_Item_Itemtipofn = "";
         gxTv_SdtSDT_ItensConagem_Item_Itemobservacao = "";
         gxTv_SdtSDT_ItensConagem_Item_Itemnaomens = "";
         gxTv_SdtSDT_ItensConagem_Item_Itemcontar = true;
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_ItensConagem_Item_Itemtipoinm ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_ItensConagem_Item_Itemcodigo ;
      protected decimal gxTv_SdtSDT_ItensConagem_Item_Itemvalor ;
      protected decimal gxTv_SdtSDT_ItensConagem_Item_Itempfb ;
      protected decimal gxTv_SdtSDT_ItensConagem_Item_Itempfl ;
      protected String gxTv_SdtSDT_ItensConagem_Item_Itemnome ;
      protected String gxTv_SdtSDT_ItensConagem_Item_Itemtipo ;
      protected String gxTv_SdtSDT_ItensConagem_Item_Itemtipofn ;
      protected String gxTv_SdtSDT_ItensConagem_Item_Itemnaomens ;
      protected String sTagName ;
      protected bool gxTv_SdtSDT_ItensConagem_Item_Itemcontar ;
      protected String gxTv_SdtSDT_ItensConagem_Item_Itemobservacao ;
   }

   [DataContract(Name = @"SDT_ItensConagem.Item", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_ItensConagem_Item_RESTInterface : GxGenericCollectionItem<SdtSDT_ItensConagem_Item>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_ItensConagem_Item_RESTInterface( ) : base()
      {
      }

      public SdtSDT_ItensConagem_Item_RESTInterface( SdtSDT_ItensConagem_Item psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ItemCodigo" , Order = 0 )]
      public Nullable<int> gxTpr_Itemcodigo
      {
         get {
            return sdt.gxTpr_Itemcodigo ;
         }

         set {
            sdt.gxTpr_Itemcodigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ItemNome" , Order = 1 )]
      public String gxTpr_Itemnome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Itemnome) ;
         }

         set {
            sdt.gxTpr_Itemnome = (String)(value);
         }

      }

      [DataMember( Name = "ItemTipo" , Order = 2 )]
      public String gxTpr_Itemtipo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Itemtipo) ;
         }

         set {
            sdt.gxTpr_Itemtipo = (String)(value);
         }

      }

      [DataMember( Name = "ItemTipoFn" , Order = 3 )]
      public String gxTpr_Itemtipofn
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Itemtipofn) ;
         }

         set {
            sdt.gxTpr_Itemtipofn = (String)(value);
         }

      }

      [DataMember( Name = "ItemObservacao" , Order = 4 )]
      public String gxTpr_Itemobservacao
      {
         get {
            return sdt.gxTpr_Itemobservacao ;
         }

         set {
            sdt.gxTpr_Itemobservacao = (String)(value);
         }

      }

      [DataMember( Name = "ItemNaoMens" , Order = 5 )]
      public String gxTpr_Itemnaomens
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Itemnaomens) ;
         }

         set {
            sdt.gxTpr_Itemnaomens = (String)(value);
         }

      }

      [DataMember( Name = "ItemValor" , Order = 6 )]
      public String gxTpr_Itemvalor
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Itemvalor, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Itemvalor = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ItemTipoINM" , Order = 7 )]
      public Nullable<short> gxTpr_Itemtipoinm
      {
         get {
            return sdt.gxTpr_Itemtipoinm ;
         }

         set {
            sdt.gxTpr_Itemtipoinm = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ItemPFB" , Order = 8 )]
      public String gxTpr_Itempfb
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Itempfb, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Itempfb = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ItemPFL" , Order = 9 )]
      public String gxTpr_Itempfl
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Itempfl, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Itempfl = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ItemContar" , Order = 10 )]
      public bool gxTpr_Itemcontar
      {
         get {
            return sdt.gxTpr_Itemcontar ;
         }

         set {
            sdt.gxTpr_Itemcontar = value;
         }

      }

      public SdtSDT_ItensConagem_Item sdt
      {
         get {
            return (SdtSDT_ItensConagem_Item)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_ItensConagem_Item() ;
         }
      }

   }

}
