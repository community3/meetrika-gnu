/*
               File: Usuario_Servicos
        Description: Usuario_Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:6:52.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuario_servicos : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public usuario_servicos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public usuario_servicos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_UsuarioServicos_UsuarioCod )
      {
         this.AV7UsuarioServicos_UsuarioCod = aP0_UsuarioServicos_UsuarioCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7UsuarioServicos_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UsuarioServicos_UsuarioCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7UsuarioServicos_UsuarioCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_5 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_5_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_5_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
                  AV16TFUsuarioServicos_ServicoSigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TFUsuarioServicos_ServicoSigla", AV16TFUsuarioServicos_ServicoSigla);
                  AV17TFUsuarioServicos_ServicoSigla_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFUsuarioServicos_ServicoSigla_Sel", AV17TFUsuarioServicos_ServicoSigla_Sel);
                  AV7UsuarioServicos_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UsuarioServicos_UsuarioCod), 6, 0)));
                  AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace", AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace);
                  AV22Pgmname = GetNextPar( );
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV16TFUsuarioServicos_ServicoSigla, AV17TFUsuarioServicos_ServicoSigla_Sel, AV7UsuarioServicos_UsuarioCod, AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace, AV22Pgmname, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAEO2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV22Pgmname = "Usuario_Servicos";
               context.Gx_err = 0;
               WSEO2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Usuario_Servicos") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311765287");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("usuario_servicos.aspx") + "?" + UrlEncode("" +AV7UsuarioServicos_UsuarioCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFUSUARIOSERVICOS_SERVICOSIGLA", StringUtil.RTrim( AV16TFUsuarioServicos_ServicoSigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFUSUARIOSERVICOS_SERVICOSIGLA_SEL", StringUtil.RTrim( AV17TFUsuarioServicos_ServicoSigla_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_5", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_5), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV19DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV19DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vUSUARIOSERVICOS_SERVICOSIGLATITLEFILTERDATA", AV15UsuarioServicos_ServicoSiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vUSUARIOSERVICOS_SERVICOSIGLATITLEFILTERDATA", AV15UsuarioServicos_ServicoSiglaTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7UsuarioServicos_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vUSUARIOSERVICOS_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7UsuarioServicos_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV22Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Caption", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Tooltip", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Cls", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_usuarioservicos_servicosigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_usuarioservicos_servicosigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Sortedstatus", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Includefilter", StringUtil.BoolToStr( Ddo_usuarioservicos_servicosigla_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Filtertype", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_usuarioservicos_servicosigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_usuarioservicos_servicosigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Datalisttype", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Datalistproc", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_usuarioservicos_servicosigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Sortasc", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Sortdsc", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Loadingdata", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Cleanfilter", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Noresultsfound", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Activeeventkey", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_usuarioservicos_servicosigla_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormEO2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("usuario_servicos.js", "?2020311765313");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "Usuario_Servicos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Usuario_Servicos" ;
      }

      protected void WBEO0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "usuario_servicos.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_EO2( true) ;
         }
         else
         {
            wb_table1_2_EO2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_EO2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_Usuario_Servicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuarioservicos_servicosigla_Internalname, StringUtil.RTrim( AV16TFUsuarioServicos_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( AV16TFUsuarioServicos_ServicoSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,11);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuarioservicos_servicosigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuarioservicos_servicosigla_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Usuario_Servicos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuarioservicos_servicosigla_sel_Internalname, StringUtil.RTrim( AV17TFUsuarioServicos_ServicoSigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV17TFUsuarioServicos_ServicoSigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,12);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuarioservicos_servicosigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuarioservicos_servicosigla_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Usuario_Servicos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuarioservicos_servicosiglatitlecontrolidtoreplace_Internalname, AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,14);\"", 0, edtavDdo_usuarioservicos_servicosiglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Usuario_Servicos.htm");
         }
         wbLoad = true;
      }

      protected void STARTEO2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Usuario_Servicos", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPEO0( ) ;
            }
         }
      }

      protected void WSEO2( )
      {
         STARTEO2( ) ;
         EVTEO2( ) ;
      }

      protected void EVTEO2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIOSERVICOS_SERVICOSIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11EO2 */
                                    E11EO2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEO0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrdereddsc_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEO0( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEO0( ) ;
                              }
                              nGXsfl_5_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
                              SubsflControlProps_52( ) ;
                              A828UsuarioServicos_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtUsuarioServicos_UsuarioCod_Internalname), ",", "."));
                              A829UsuarioServicos_ServicoCod = (int)(context.localUtil.CToN( cgiGet( edtUsuarioServicos_ServicoCod_Internalname), ",", "."));
                              A831UsuarioServicos_ServicoSigla = StringUtil.Upper( cgiGet( edtUsuarioServicos_ServicoSigla_Internalname));
                              n831UsuarioServicos_ServicoSigla = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12EO2 */
                                          E12EO2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13EO2 */
                                          E13EO2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14EO2 */
                                          E14EO2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfusuarioservicos_servicosigla Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUSUARIOSERVICOS_SERVICOSIGLA"), AV16TFUsuarioServicos_ServicoSigla) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfusuarioservicos_servicosigla_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUSUARIOSERVICOS_SERVICOSIGLA_SEL"), AV17TFUsuarioServicos_ServicoSigla_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPEO0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEO2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormEO2( ) ;
            }
         }
      }

      protected void PAEO2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrdereddsc_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_52( ) ;
         while ( nGXsfl_5_idx <= nRC_GXsfl_5 )
         {
            sendrow_52( ) ;
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       bool AV13OrderedDsc ,
                                       String AV16TFUsuarioServicos_ServicoSigla ,
                                       String AV17TFUsuarioServicos_ServicoSigla_Sel ,
                                       int AV7UsuarioServicos_UsuarioCod ,
                                       String AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace ,
                                       String AV22Pgmname ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFEO2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIOSERVICOS_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A828UsuarioServicos_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIOSERVICOS_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIOSERVICOS_SERVICOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A829UsuarioServicos_ServicoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIOSERVICOS_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEO2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV22Pgmname = "Usuario_Servicos";
         context.Gx_err = 0;
      }

      protected void RFEO2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 5;
         /* Execute user event: E13EO2 */
         E13EO2 ();
         nGXsfl_5_idx = 1;
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         nGXsfl_5_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_52( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV17TFUsuarioServicos_ServicoSigla_Sel ,
                                                 AV16TFUsuarioServicos_ServicoSigla ,
                                                 A831UsuarioServicos_ServicoSigla ,
                                                 AV13OrderedDsc ,
                                                 A828UsuarioServicos_UsuarioCod ,
                                                 AV7UsuarioServicos_UsuarioCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV16TFUsuarioServicos_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV16TFUsuarioServicos_ServicoSigla), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TFUsuarioServicos_ServicoSigla", AV16TFUsuarioServicos_ServicoSigla);
            /* Using cursor H00EO2 */
            pr_default.execute(0, new Object[] {AV7UsuarioServicos_UsuarioCod, lV16TFUsuarioServicos_ServicoSigla, AV17TFUsuarioServicos_ServicoSigla_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_5_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A831UsuarioServicos_ServicoSigla = H00EO2_A831UsuarioServicos_ServicoSigla[0];
               n831UsuarioServicos_ServicoSigla = H00EO2_n831UsuarioServicos_ServicoSigla[0];
               A829UsuarioServicos_ServicoCod = H00EO2_A829UsuarioServicos_ServicoCod[0];
               A828UsuarioServicos_UsuarioCod = H00EO2_A828UsuarioServicos_UsuarioCod[0];
               A831UsuarioServicos_ServicoSigla = H00EO2_A831UsuarioServicos_ServicoSigla[0];
               n831UsuarioServicos_ServicoSigla = H00EO2_n831UsuarioServicos_ServicoSigla[0];
               /* Execute user event: E14EO2 */
               E14EO2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 5;
            WBEO0( ) ;
         }
         nGXsfl_5_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV17TFUsuarioServicos_ServicoSigla_Sel ,
                                              AV16TFUsuarioServicos_ServicoSigla ,
                                              A831UsuarioServicos_ServicoSigla ,
                                              AV13OrderedDsc ,
                                              A828UsuarioServicos_UsuarioCod ,
                                              AV7UsuarioServicos_UsuarioCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV16TFUsuarioServicos_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV16TFUsuarioServicos_ServicoSigla), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TFUsuarioServicos_ServicoSigla", AV16TFUsuarioServicos_ServicoSigla);
         /* Using cursor H00EO3 */
         pr_default.execute(1, new Object[] {AV7UsuarioServicos_UsuarioCod, lV16TFUsuarioServicos_ServicoSigla, AV17TFUsuarioServicos_ServicoSigla_Sel});
         GRID_nRecordCount = H00EO3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV16TFUsuarioServicos_ServicoSigla, AV17TFUsuarioServicos_ServicoSigla_Sel, AV7UsuarioServicos_UsuarioCod, AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace, AV22Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV16TFUsuarioServicos_ServicoSigla, AV17TFUsuarioServicos_ServicoSigla_Sel, AV7UsuarioServicos_UsuarioCod, AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace, AV22Pgmname, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV16TFUsuarioServicos_ServicoSigla, AV17TFUsuarioServicos_ServicoSigla_Sel, AV7UsuarioServicos_UsuarioCod, AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace, AV22Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV16TFUsuarioServicos_ServicoSigla, AV17TFUsuarioServicos_ServicoSigla_Sel, AV7UsuarioServicos_UsuarioCod, AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace, AV22Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV16TFUsuarioServicos_ServicoSigla, AV17TFUsuarioServicos_ServicoSigla_Sel, AV7UsuarioServicos_UsuarioCod, AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace, AV22Pgmname, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPEO0( )
      {
         /* Before Start, stand alone formulas. */
         AV22Pgmname = "Usuario_Servicos";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12EO2 */
         E12EO2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV19DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vUSUARIOSERVICOS_SERVICOSIGLATITLEFILTERDATA"), AV15UsuarioServicos_ServicoSiglaTitleFilterData);
            /* Read variables values. */
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            AV16TFUsuarioServicos_ServicoSigla = StringUtil.Upper( cgiGet( edtavTfusuarioservicos_servicosigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TFUsuarioServicos_ServicoSigla", AV16TFUsuarioServicos_ServicoSigla);
            AV17TFUsuarioServicos_ServicoSigla_Sel = StringUtil.Upper( cgiGet( edtavTfusuarioservicos_servicosigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFUsuarioServicos_ServicoSigla_Sel", AV17TFUsuarioServicos_ServicoSigla_Sel);
            AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace = cgiGet( edtavDdo_usuarioservicos_servicosiglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace", AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_5 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_5"), ",", "."));
            wcpOAV7UsuarioServicos_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7UsuarioServicos_UsuarioCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_usuarioservicos_servicosigla_Caption = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Caption");
            Ddo_usuarioservicos_servicosigla_Tooltip = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Tooltip");
            Ddo_usuarioservicos_servicosigla_Cls = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Cls");
            Ddo_usuarioservicos_servicosigla_Filteredtext_set = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Filteredtext_set");
            Ddo_usuarioservicos_servicosigla_Selectedvalue_set = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Selectedvalue_set");
            Ddo_usuarioservicos_servicosigla_Dropdownoptionstype = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Dropdownoptionstype");
            Ddo_usuarioservicos_servicosigla_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Titlecontrolidtoreplace");
            Ddo_usuarioservicos_servicosigla_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Includesortasc"));
            Ddo_usuarioservicos_servicosigla_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Includesortdsc"));
            Ddo_usuarioservicos_servicosigla_Sortedstatus = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Sortedstatus");
            Ddo_usuarioservicos_servicosigla_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Includefilter"));
            Ddo_usuarioservicos_servicosigla_Filtertype = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Filtertype");
            Ddo_usuarioservicos_servicosigla_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Filterisrange"));
            Ddo_usuarioservicos_servicosigla_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Includedatalist"));
            Ddo_usuarioservicos_servicosigla_Datalisttype = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Datalisttype");
            Ddo_usuarioservicos_servicosigla_Datalistproc = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Datalistproc");
            Ddo_usuarioservicos_servicosigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_usuarioservicos_servicosigla_Sortasc = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Sortasc");
            Ddo_usuarioservicos_servicosigla_Sortdsc = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Sortdsc");
            Ddo_usuarioservicos_servicosigla_Loadingdata = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Loadingdata");
            Ddo_usuarioservicos_servicosigla_Cleanfilter = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Cleanfilter");
            Ddo_usuarioservicos_servicosigla_Noresultsfound = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Noresultsfound");
            Ddo_usuarioservicos_servicosigla_Searchbuttontext = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Searchbuttontext");
            Ddo_usuarioservicos_servicosigla_Activeeventkey = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Activeeventkey");
            Ddo_usuarioservicos_servicosigla_Filteredtext_get = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Filteredtext_get");
            Ddo_usuarioservicos_servicosigla_Selectedvalue_get = cgiGet( sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUSUARIOSERVICOS_SERVICOSIGLA"), AV16TFUsuarioServicos_ServicoSigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUSUARIOSERVICOS_SERVICOSIGLA_SEL"), AV17TFUsuarioServicos_ServicoSigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12EO2 */
         E12EO2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12EO2( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfusuarioservicos_servicosigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfusuarioservicos_servicosigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuarioservicos_servicosigla_Visible), 5, 0)));
         edtavTfusuarioservicos_servicosigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfusuarioservicos_servicosigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuarioservicos_servicosigla_sel_Visible), 5, 0)));
         Ddo_usuarioservicos_servicosigla_Titlecontrolidtoreplace = subGrid_Internalname+"_UsuarioServicos_ServicoSigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuarioservicos_servicosigla_Internalname, "TitleControlIdToReplace", Ddo_usuarioservicos_servicosigla_Titlecontrolidtoreplace);
         AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace = Ddo_usuarioservicos_servicosigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace", AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace);
         edtavDdo_usuarioservicos_servicosiglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_usuarioservicos_servicosiglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuarioservicos_servicosiglatitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV19DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV19DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E13EO2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV15UsuarioServicos_ServicoSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtUsuarioServicos_ServicoSigla_Titleformat = 2;
         edtUsuarioServicos_ServicoSigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Servi�o", AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUsuarioServicos_ServicoSigla_Internalname, "Title", edtUsuarioServicos_ServicoSigla_Title);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV15UsuarioServicos_ServicoSiglaTitleFilterData", AV15UsuarioServicos_ServicoSiglaTitleFilterData);
      }

      protected void E11EO2( )
      {
         /* Ddo_usuarioservicos_servicosigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuarioservicos_servicosigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_usuarioservicos_servicosigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuarioservicos_servicosigla_Internalname, "SortedStatus", Ddo_usuarioservicos_servicosigla_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_usuarioservicos_servicosigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_usuarioservicos_servicosigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuarioservicos_servicosigla_Internalname, "SortedStatus", Ddo_usuarioservicos_servicosigla_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_usuarioservicos_servicosigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV16TFUsuarioServicos_ServicoSigla = Ddo_usuarioservicos_servicosigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TFUsuarioServicos_ServicoSigla", AV16TFUsuarioServicos_ServicoSigla);
            AV17TFUsuarioServicos_ServicoSigla_Sel = Ddo_usuarioservicos_servicosigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFUsuarioServicos_ServicoSigla_Sel", AV17TFUsuarioServicos_ServicoSigla_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E14EO2( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 5;
         }
         sendrow_52( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         Ddo_usuarioservicos_servicosigla_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuarioservicos_servicosigla_Internalname, "SortedStatus", Ddo_usuarioservicos_servicosigla_Sortedstatus);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV14Session.Get(AV22Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV22Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV14Session.Get(AV22Pgmname+"GridState"), "");
         }
         AV13OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23GXV1 = 1;
         while ( AV23GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV23GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFUSUARIOSERVICOS_SERVICOSIGLA") == 0 )
            {
               AV16TFUsuarioServicos_ServicoSigla = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TFUsuarioServicos_ServicoSigla", AV16TFUsuarioServicos_ServicoSigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFUsuarioServicos_ServicoSigla)) )
               {
                  Ddo_usuarioservicos_servicosigla_Filteredtext_set = AV16TFUsuarioServicos_ServicoSigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuarioservicos_servicosigla_Internalname, "FilteredText_set", Ddo_usuarioservicos_servicosigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFUSUARIOSERVICOS_SERVICOSIGLA_SEL") == 0 )
            {
               AV17TFUsuarioServicos_ServicoSigla_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFUsuarioServicos_ServicoSigla_Sel", AV17TFUsuarioServicos_ServicoSigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFUsuarioServicos_ServicoSigla_Sel)) )
               {
                  Ddo_usuarioservicos_servicosigla_Selectedvalue_set = AV17TFUsuarioServicos_ServicoSigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_usuarioservicos_servicosigla_Internalname, "SelectedValue_set", Ddo_usuarioservicos_servicosigla_Selectedvalue_set);
               }
            }
            AV23GXV1 = (int)(AV23GXV1+1);
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV14Session.Get(AV22Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFUsuarioServicos_ServicoSigla)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUSUARIOSERVICOS_SERVICOSIGLA";
            AV12GridStateFilterValue.gxTpr_Value = AV16TFUsuarioServicos_ServicoSigla;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFUsuarioServicos_ServicoSigla_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUSUARIOSERVICOS_SERVICOSIGLA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV17TFUsuarioServicos_ServicoSigla_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7UsuarioServicos_UsuarioCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&USUARIOSERVICOS_USUARIOCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7UsuarioServicos_UsuarioCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV22Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV22Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "UsuarioServicos";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "UsuarioServicos_UsuarioCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7UsuarioServicos_UsuarioCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV14Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_EO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"5\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Usu�rio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuarioServicos_ServicoSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuarioServicos_ServicoSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuarioServicos_ServicoSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A831UsuarioServicos_ServicoSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuarioServicos_ServicoSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuarioServicos_ServicoSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 5 )
         {
            wbEnd = 0;
            nRC_GXsfl_5 = (short)(nGXsfl_5_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_EO2e( true) ;
         }
         else
         {
            wb_table1_2_EO2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7UsuarioServicos_UsuarioCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UsuarioServicos_UsuarioCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEO2( ) ;
         WSEO2( ) ;
         WEEO2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7UsuarioServicos_UsuarioCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAEO2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "usuario_servicos");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAEO2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7UsuarioServicos_UsuarioCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UsuarioServicos_UsuarioCod), 6, 0)));
         }
         wcpOAV7UsuarioServicos_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7UsuarioServicos_UsuarioCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7UsuarioServicos_UsuarioCod != wcpOAV7UsuarioServicos_UsuarioCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7UsuarioServicos_UsuarioCod = AV7UsuarioServicos_UsuarioCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7UsuarioServicos_UsuarioCod = cgiGet( sPrefix+"AV7UsuarioServicos_UsuarioCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7UsuarioServicos_UsuarioCod) > 0 )
         {
            AV7UsuarioServicos_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7UsuarioServicos_UsuarioCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7UsuarioServicos_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UsuarioServicos_UsuarioCod), 6, 0)));
         }
         else
         {
            AV7UsuarioServicos_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7UsuarioServicos_UsuarioCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAEO2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSEO2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSEO2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7UsuarioServicos_UsuarioCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7UsuarioServicos_UsuarioCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7UsuarioServicos_UsuarioCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7UsuarioServicos_UsuarioCod_CTRL", StringUtil.RTrim( sCtrlAV7UsuarioServicos_UsuarioCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEEO2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031176546");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("usuario_servicos.js", "?202031176546");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_52( )
      {
         edtUsuarioServicos_UsuarioCod_Internalname = sPrefix+"USUARIOSERVICOS_USUARIOCOD_"+sGXsfl_5_idx;
         edtUsuarioServicos_ServicoCod_Internalname = sPrefix+"USUARIOSERVICOS_SERVICOCOD_"+sGXsfl_5_idx;
         edtUsuarioServicos_ServicoSigla_Internalname = sPrefix+"USUARIOSERVICOS_SERVICOSIGLA_"+sGXsfl_5_idx;
      }

      protected void SubsflControlProps_fel_52( )
      {
         edtUsuarioServicos_UsuarioCod_Internalname = sPrefix+"USUARIOSERVICOS_USUARIOCOD_"+sGXsfl_5_fel_idx;
         edtUsuarioServicos_ServicoCod_Internalname = sPrefix+"USUARIOSERVICOS_SERVICOCOD_"+sGXsfl_5_fel_idx;
         edtUsuarioServicos_ServicoSigla_Internalname = sPrefix+"USUARIOSERVICOS_SERVICOSIGLA_"+sGXsfl_5_fel_idx;
      }

      protected void sendrow_52( )
      {
         SubsflControlProps_52( ) ;
         WBEO0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_5_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_5_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_5_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuarioServicos_UsuarioCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A828UsuarioServicos_UsuarioCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuarioServicos_UsuarioCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuarioServicos_ServicoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A829UsuarioServicos_ServicoCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuarioServicos_ServicoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuarioServicos_ServicoSigla_Internalname,StringUtil.RTrim( A831UsuarioServicos_ServicoSigla),StringUtil.RTrim( context.localUtil.Format( A831UsuarioServicos_ServicoSigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuarioServicos_ServicoSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIOSERVICOS_USUARIOCOD"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, context.localUtil.Format( (decimal)(A828UsuarioServicos_UsuarioCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_USUARIOSERVICOS_SERVICOCOD"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, context.localUtil.Format( (decimal)(A829UsuarioServicos_ServicoCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         /* End function sendrow_52 */
      }

      protected void init_default_properties( )
      {
         edtUsuarioServicos_UsuarioCod_Internalname = sPrefix+"USUARIOSERVICOS_USUARIOCOD";
         edtUsuarioServicos_ServicoCod_Internalname = sPrefix+"USUARIOSERVICOS_SERVICOCOD";
         edtUsuarioServicos_ServicoSigla_Internalname = sPrefix+"USUARIOSERVICOS_SERVICOSIGLA";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfusuarioservicos_servicosigla_Internalname = sPrefix+"vTFUSUARIOSERVICOS_SERVICOSIGLA";
         edtavTfusuarioservicos_servicosigla_sel_Internalname = sPrefix+"vTFUSUARIOSERVICOS_SERVICOSIGLA_SEL";
         Ddo_usuarioservicos_servicosigla_Internalname = sPrefix+"DDO_USUARIOSERVICOS_SERVICOSIGLA";
         edtavDdo_usuarioservicos_servicosiglatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_USUARIOSERVICOS_SERVICOSIGLATITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtUsuarioServicos_ServicoSigla_Jsonclick = "";
         edtUsuarioServicos_ServicoCod_Jsonclick = "";
         edtUsuarioServicos_UsuarioCod_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtUsuarioServicos_ServicoSigla_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtUsuarioServicos_ServicoSigla_Title = "Servi�o";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_usuarioservicos_servicosiglatitlecontrolidtoreplace_Visible = 1;
         edtavTfusuarioservicos_servicosigla_sel_Jsonclick = "";
         edtavTfusuarioservicos_servicosigla_sel_Visible = 1;
         edtavTfusuarioservicos_servicosigla_Jsonclick = "";
         edtavTfusuarioservicos_servicosigla_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         Ddo_usuarioservicos_servicosigla_Searchbuttontext = "Pesquisar";
         Ddo_usuarioservicos_servicosigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_usuarioservicos_servicosigla_Cleanfilter = "Limpar pesquisa";
         Ddo_usuarioservicos_servicosigla_Loadingdata = "Carregando dados...";
         Ddo_usuarioservicos_servicosigla_Sortdsc = "Ordenar de Z � A";
         Ddo_usuarioservicos_servicosigla_Sortasc = "Ordenar de A � Z";
         Ddo_usuarioservicos_servicosigla_Datalistupdateminimumcharacters = 0;
         Ddo_usuarioservicos_servicosigla_Datalistproc = "GetUsuario_ServicosFilterData";
         Ddo_usuarioservicos_servicosigla_Datalisttype = "Dynamic";
         Ddo_usuarioservicos_servicosigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuarioservicos_servicosigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_usuarioservicos_servicosigla_Filtertype = "Character";
         Ddo_usuarioservicos_servicosigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_usuarioservicos_servicosigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuarioservicos_servicosigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuarioservicos_servicosigla_Titlecontrolidtoreplace = "";
         Ddo_usuarioservicos_servicosigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuarioservicos_servicosigla_Cls = "ColumnSettings";
         Ddo_usuarioservicos_servicosigla_Tooltip = "Op��es";
         Ddo_usuarioservicos_servicosigla_Caption = "";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_USUARIOSERVICOS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFUsuarioServicos_ServicoSigla',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV17TFUsuarioServicos_ServicoSigla_Sel',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV7UsuarioServicos_UsuarioCod',fld:'vUSUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV15UsuarioServicos_ServicoSiglaTitleFilterData',fld:'vUSUARIOSERVICOS_SERVICOSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'edtUsuarioServicos_ServicoSigla_Titleformat',ctrl:'USUARIOSERVICOS_SERVICOSIGLA',prop:'Titleformat'},{av:'edtUsuarioServicos_ServicoSigla_Title',ctrl:'USUARIOSERVICOS_SERVICOSIGLA',prop:'Title'}]}");
         setEventMetadata("DDO_USUARIOSERVICOS_SERVICOSIGLA.ONOPTIONCLICKED","{handler:'E11EO2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFUsuarioServicos_ServicoSigla',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV17TFUsuarioServicos_ServicoSigla_Sel',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV7UsuarioServicos_UsuarioCod',fld:'vUSUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_USUARIOSERVICOS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_usuarioservicos_servicosigla_Activeeventkey',ctrl:'DDO_USUARIOSERVICOS_SERVICOSIGLA',prop:'ActiveEventKey'},{av:'Ddo_usuarioservicos_servicosigla_Filteredtext_get',ctrl:'DDO_USUARIOSERVICOS_SERVICOSIGLA',prop:'FilteredText_get'},{av:'Ddo_usuarioservicos_servicosigla_Selectedvalue_get',ctrl:'DDO_USUARIOSERVICOS_SERVICOSIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuarioservicos_servicosigla_Sortedstatus',ctrl:'DDO_USUARIOSERVICOS_SERVICOSIGLA',prop:'SortedStatus'},{av:'AV16TFUsuarioServicos_ServicoSigla',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV17TFUsuarioServicos_ServicoSigla_Sel',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA_SEL',pic:'@!',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E14EO2',iparms:[],oparms:[]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_USUARIOSERVICOS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFUsuarioServicos_ServicoSigla',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV17TFUsuarioServicos_ServicoSigla_Sel',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV7UsuarioServicos_UsuarioCod',fld:'vUSUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV15UsuarioServicos_ServicoSiglaTitleFilterData',fld:'vUSUARIOSERVICOS_SERVICOSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'edtUsuarioServicos_ServicoSigla_Titleformat',ctrl:'USUARIOSERVICOS_SERVICOSIGLA',prop:'Titleformat'},{av:'edtUsuarioServicos_ServicoSigla_Title',ctrl:'USUARIOSERVICOS_SERVICOSIGLA',prop:'Title'}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_USUARIOSERVICOS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFUsuarioServicos_ServicoSigla',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV17TFUsuarioServicos_ServicoSigla_Sel',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV7UsuarioServicos_UsuarioCod',fld:'vUSUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV15UsuarioServicos_ServicoSiglaTitleFilterData',fld:'vUSUARIOSERVICOS_SERVICOSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'edtUsuarioServicos_ServicoSigla_Titleformat',ctrl:'USUARIOSERVICOS_SERVICOSIGLA',prop:'Titleformat'},{av:'edtUsuarioServicos_ServicoSigla_Title',ctrl:'USUARIOSERVICOS_SERVICOSIGLA',prop:'Title'}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_USUARIOSERVICOS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFUsuarioServicos_ServicoSigla',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV17TFUsuarioServicos_ServicoSigla_Sel',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV7UsuarioServicos_UsuarioCod',fld:'vUSUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV15UsuarioServicos_ServicoSiglaTitleFilterData',fld:'vUSUARIOSERVICOS_SERVICOSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'edtUsuarioServicos_ServicoSigla_Titleformat',ctrl:'USUARIOSERVICOS_SERVICOSIGLA',prop:'Titleformat'},{av:'edtUsuarioServicos_ServicoSigla_Title',ctrl:'USUARIOSERVICOS_SERVICOSIGLA',prop:'Title'}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_USUARIOSERVICOS_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16TFUsuarioServicos_ServicoSigla',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV17TFUsuarioServicos_ServicoSigla_Sel',fld:'vTFUSUARIOSERVICOS_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV7UsuarioServicos_UsuarioCod',fld:'vUSUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV15UsuarioServicos_ServicoSiglaTitleFilterData',fld:'vUSUARIOSERVICOS_SERVICOSIGLATITLEFILTERDATA',pic:'',nv:null},{av:'edtUsuarioServicos_ServicoSigla_Titleformat',ctrl:'USUARIOSERVICOS_SERVICOSIGLA',prop:'Titleformat'},{av:'edtUsuarioServicos_ServicoSigla_Title',ctrl:'USUARIOSERVICOS_SERVICOSIGLA',prop:'Title'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Ddo_usuarioservicos_servicosigla_Activeeventkey = "";
         Ddo_usuarioservicos_servicosigla_Filteredtext_get = "";
         Ddo_usuarioservicos_servicosigla_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16TFUsuarioServicos_ServicoSigla = "";
         AV17TFUsuarioServicos_ServicoSigla_Sel = "";
         AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace = "";
         AV22Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV19DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV15UsuarioServicos_ServicoSiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_usuarioservicos_servicosigla_Filteredtext_set = "";
         Ddo_usuarioservicos_servicosigla_Selectedvalue_set = "";
         Ddo_usuarioservicos_servicosigla_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A831UsuarioServicos_ServicoSigla = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV16TFUsuarioServicos_ServicoSigla = "";
         H00EO2_A831UsuarioServicos_ServicoSigla = new String[] {""} ;
         H00EO2_n831UsuarioServicos_ServicoSigla = new bool[] {false} ;
         H00EO2_A829UsuarioServicos_ServicoCod = new int[1] ;
         H00EO2_A828UsuarioServicos_UsuarioCod = new int[1] ;
         H00EO3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV14Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7UsuarioServicos_UsuarioCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuario_servicos__default(),
            new Object[][] {
                new Object[] {
               H00EO2_A831UsuarioServicos_ServicoSigla, H00EO2_n831UsuarioServicos_ServicoSigla, H00EO2_A829UsuarioServicos_ServicoCod, H00EO2_A828UsuarioServicos_UsuarioCod
               }
               , new Object[] {
               H00EO3_AGRID_nRecordCount
               }
            }
         );
         AV22Pgmname = "Usuario_Servicos";
         /* GeneXus formulas. */
         AV22Pgmname = "Usuario_Servicos";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_5 ;
      private short nGXsfl_5_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_5_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtUsuarioServicos_ServicoSigla_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7UsuarioServicos_UsuarioCod ;
      private int wcpOAV7UsuarioServicos_UsuarioCod ;
      private int subGrid_Rows ;
      private int Ddo_usuarioservicos_servicosigla_Datalistupdateminimumcharacters ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfusuarioservicos_servicosigla_Visible ;
      private int edtavTfusuarioservicos_servicosigla_sel_Visible ;
      private int edtavDdo_usuarioservicos_servicosiglatitlecontrolidtoreplace_Visible ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int A829UsuarioServicos_ServicoCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV23GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Ddo_usuarioservicos_servicosigla_Activeeventkey ;
      private String Ddo_usuarioservicos_servicosigla_Filteredtext_get ;
      private String Ddo_usuarioservicos_servicosigla_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_5_idx="0001" ;
      private String AV16TFUsuarioServicos_ServicoSigla ;
      private String AV17TFUsuarioServicos_ServicoSigla_Sel ;
      private String AV22Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_usuarioservicos_servicosigla_Caption ;
      private String Ddo_usuarioservicos_servicosigla_Tooltip ;
      private String Ddo_usuarioservicos_servicosigla_Cls ;
      private String Ddo_usuarioservicos_servicosigla_Filteredtext_set ;
      private String Ddo_usuarioservicos_servicosigla_Selectedvalue_set ;
      private String Ddo_usuarioservicos_servicosigla_Dropdownoptionstype ;
      private String Ddo_usuarioservicos_servicosigla_Titlecontrolidtoreplace ;
      private String Ddo_usuarioservicos_servicosigla_Sortedstatus ;
      private String Ddo_usuarioservicos_servicosigla_Filtertype ;
      private String Ddo_usuarioservicos_servicosigla_Datalisttype ;
      private String Ddo_usuarioservicos_servicosigla_Datalistproc ;
      private String Ddo_usuarioservicos_servicosigla_Sortasc ;
      private String Ddo_usuarioservicos_servicosigla_Sortdsc ;
      private String Ddo_usuarioservicos_servicosigla_Loadingdata ;
      private String Ddo_usuarioservicos_servicosigla_Cleanfilter ;
      private String Ddo_usuarioservicos_servicosigla_Noresultsfound ;
      private String Ddo_usuarioservicos_servicosigla_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfusuarioservicos_servicosigla_Internalname ;
      private String edtavTfusuarioservicos_servicosigla_Jsonclick ;
      private String edtavTfusuarioservicos_servicosigla_sel_Internalname ;
      private String edtavTfusuarioservicos_servicosigla_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_usuarioservicos_servicosiglatitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtUsuarioServicos_UsuarioCod_Internalname ;
      private String edtUsuarioServicos_ServicoCod_Internalname ;
      private String A831UsuarioServicos_ServicoSigla ;
      private String edtUsuarioServicos_ServicoSigla_Internalname ;
      private String scmdbuf ;
      private String lV16TFUsuarioServicos_ServicoSigla ;
      private String subGrid_Internalname ;
      private String Ddo_usuarioservicos_servicosigla_Internalname ;
      private String edtUsuarioServicos_ServicoSigla_Title ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV7UsuarioServicos_UsuarioCod ;
      private String sGXsfl_5_fel_idx="0001" ;
      private String ROClassString ;
      private String edtUsuarioServicos_UsuarioCod_Jsonclick ;
      private String edtUsuarioServicos_ServicoCod_Jsonclick ;
      private String edtUsuarioServicos_ServicoSigla_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Ddo_usuarioservicos_servicosigla_Includesortasc ;
      private bool Ddo_usuarioservicos_servicosigla_Includesortdsc ;
      private bool Ddo_usuarioservicos_servicosigla_Includefilter ;
      private bool Ddo_usuarioservicos_servicosigla_Filterisrange ;
      private bool Ddo_usuarioservicos_servicosigla_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n831UsuarioServicos_ServicoSigla ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV18ddo_UsuarioServicos_ServicoSiglaTitleControlIdToReplace ;
      private IGxSession AV14Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] H00EO2_A831UsuarioServicos_ServicoSigla ;
      private bool[] H00EO2_n831UsuarioServicos_ServicoSigla ;
      private int[] H00EO2_A829UsuarioServicos_ServicoCod ;
      private int[] H00EO2_A828UsuarioServicos_UsuarioCod ;
      private long[] H00EO3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV15UsuarioServicos_ServicoSiglaTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV19DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class usuario_servicos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00EO2( IGxContext context ,
                                             String AV17TFUsuarioServicos_ServicoSigla_Sel ,
                                             String AV16TFUsuarioServicos_ServicoSigla ,
                                             String A831UsuarioServicos_ServicoSigla ,
                                             bool AV13OrderedDsc ,
                                             int A828UsuarioServicos_UsuarioCod ,
                                             int AV7UsuarioServicos_UsuarioCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [8] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Servico_Sigla] AS UsuarioServicos_ServicoSigla, T1.[UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod, T1.[UsuarioServicos_UsuarioCod]";
         sFromString = " FROM ([UsuarioServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[UsuarioServicos_ServicoCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[UsuarioServicos_UsuarioCod] = @AV7UsuarioServicos_UsuarioCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFUsuarioServicos_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFUsuarioServicos_ServicoSigla)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV16TFUsuarioServicos_ServicoSigla)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFUsuarioServicos_ServicoSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] = @AV17TFUsuarioServicos_ServicoSigla_Sel)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[UsuarioServicos_UsuarioCod], T2.[Servico_Sigla]";
         }
         else if ( AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[UsuarioServicos_UsuarioCod] DESC, T2.[Servico_Sigla] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[UsuarioServicos_UsuarioCod], T1.[UsuarioServicos_ServicoCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00EO3( IGxContext context ,
                                             String AV17TFUsuarioServicos_ServicoSigla_Sel ,
                                             String AV16TFUsuarioServicos_ServicoSigla ,
                                             String A831UsuarioServicos_ServicoSigla ,
                                             bool AV13OrderedDsc ,
                                             int A828UsuarioServicos_UsuarioCod ,
                                             int AV7UsuarioServicos_UsuarioCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [3] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([UsuarioServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[UsuarioServicos_ServicoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[UsuarioServicos_UsuarioCod] = @AV7UsuarioServicos_UsuarioCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFUsuarioServicos_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFUsuarioServicos_ServicoSigla)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV16TFUsuarioServicos_ServicoSigla)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFUsuarioServicos_ServicoSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Sigla] = @AV17TFUsuarioServicos_ServicoSigla_Sel)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00EO2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
               case 1 :
                     return conditional_H00EO3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EO2 ;
          prmH00EO2 = new Object[] {
          new Object[] {"@AV7UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16TFUsuarioServicos_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV17TFUsuarioServicos_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00EO3 ;
          prmH00EO3 = new Object[] {
          new Object[] {"@AV7UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16TFUsuarioServicos_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV17TFUsuarioServicos_ServicoSigla_Sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00EO2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EO2,11,0,true,false )
             ,new CursorDef("H00EO3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EO3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
       }
    }

 }

}
