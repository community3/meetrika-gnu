/*
               File: PRC_UpdPrazoMaisDias
        Description: Upd Prazo Mais Dias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:54.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updprazomaisdias : GXProcedure
   {
      public prc_updprazomaisdias( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updprazomaisdias( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           short aP1_Dias )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8Dias = aP1_Dias;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 short aP1_Dias )
      {
         prc_updprazomaisdias objprc_updprazomaisdias;
         objprc_updprazomaisdias = new prc_updprazomaisdias();
         objprc_updprazomaisdias.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_updprazomaisdias.AV8Dias = aP1_Dias;
         objprc_updprazomaisdias.context.SetSubmitInitialConfig(context);
         objprc_updprazomaisdias.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updprazomaisdias);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updprazomaisdias)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P008A2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1237ContagemResultado_PrazoMaisDias = P008A2_A1237ContagemResultado_PrazoMaisDias[0];
            n1237ContagemResultado_PrazoMaisDias = P008A2_n1237ContagemResultado_PrazoMaisDias[0];
            A1237ContagemResultado_PrazoMaisDias = AV8Dias;
            n1237ContagemResultado_PrazoMaisDias = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P008A3 */
            pr_default.execute(1, new Object[] {n1237ContagemResultado_PrazoMaisDias, A1237ContagemResultado_PrazoMaisDias, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P008A4 */
            pr_default.execute(2, new Object[] {n1237ContagemResultado_PrazoMaisDias, A1237ContagemResultado_PrazoMaisDias, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P008A2_A456ContagemResultado_Codigo = new int[1] ;
         P008A2_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P008A2_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updprazomaisdias__default(),
            new Object[][] {
                new Object[] {
               P008A2_A456ContagemResultado_Codigo, P008A2_A1237ContagemResultado_PrazoMaisDias, P008A2_n1237ContagemResultado_PrazoMaisDias
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8Dias ;
      private short A1237ContagemResultado_PrazoMaisDias ;
      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P008A2_A456ContagemResultado_Codigo ;
      private short[] P008A2_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P008A2_n1237ContagemResultado_PrazoMaisDias ;
   }

   public class prc_updprazomaisdias__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008A2 ;
          prmP008A2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008A3 ;
          prmP008A3 = new Object[] {
          new Object[] {"@ContagemResultado_PrazoMaisDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008A4 ;
          prmP008A4 = new Object[] {
          new Object[] {"@ContagemResultado_PrazoMaisDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008A2", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_PrazoMaisDias] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008A2,1,0,true,true )
             ,new CursorDef("P008A3", "UPDATE [ContagemResultado] SET [ContagemResultado_PrazoMaisDias]=@ContagemResultado_PrazoMaisDias  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008A3)
             ,new CursorDef("P008A4", "UPDATE [ContagemResultado] SET [ContagemResultado_PrazoMaisDias]=@ContagemResultado_PrazoMaisDias  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008A4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
