/*
               File: ReqNegReqTec
        Description: Req Neg Req Tec
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:16.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class reqnegreqtec : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A1895SolicServicoReqNeg_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A1895SolicServicoReqNeg_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A1919Requisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A1919Requisito_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Req Neg Req Tec", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public reqnegreqtec( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public reqnegreqtec( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4W218( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4W218e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4W218( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4W218( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4W218e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Req Neg Req Tec", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ReqNegReqTec.htm");
            wb_table3_28_4W218( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_4W218e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4W218e( true) ;
         }
         else
         {
            wb_table1_2_4W218e( false) ;
         }
      }

      protected void wb_table3_28_4W218( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_4W218( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_4W218e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ReqNegReqTec.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ReqNegReqTec.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ReqNegReqTec.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_4W218e( true) ;
         }
         else
         {
            wb_table3_28_4W218e( false) ;
         }
      }

      protected void wb_table4_34_4W218( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicservicoreqneg_codigo_Internalname, "C�digo", "", "", lblTextblocksolicservicoreqneg_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ReqNegReqTec.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSolicServicoReqNeg_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0, ",", "")), ((edtSolicServicoReqNeg_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1895SolicServicoReqNeg_Codigo), "ZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1895SolicServicoReqNeg_Codigo), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicServicoReqNeg_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtSolicServicoReqNeg_Codigo_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "Codigo10", "right", false, "HLP_ReqNegReqTec.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_codigo_Internalname, "C�digo do Requisito", "", "", lblTextblockrequisito_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ReqNegReqTec.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRequisito_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1919Requisito_Codigo), 6, 0, ",", "")), ((edtRequisito_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1919Requisito_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1919Requisito_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRequisito_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtRequisito_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ReqNegReqTec.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisito_ordem_Internalname, "Ordem", "", "", lblTextblockrequisito_ordem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ReqNegReqTec.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtRequisito_Ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1931Requisito_Ordem), 3, 0, ",", "")), ((edtRequisito_Ordem_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9")) : context.localUtil.Format( (decimal)(A1931Requisito_Ordem), "ZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRequisito_Ordem_Jsonclick, 0, "Attribute", "", "", "", 1, edtRequisito_Ordem_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Ordem", "right", false, "HLP_ReqNegReqTec.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreqnegreqtec_propostacod_Internalname, "Tec_Proposta Cod", "", "", lblTextblockreqnegreqtec_propostacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ReqNegReqTec.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtReqNegReqTec_PropostaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1945ReqNegReqTec_PropostaCod), 6, 0, ",", "")), ((edtReqNegReqTec_PropostaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1945ReqNegReqTec_PropostaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1945ReqNegReqTec_PropostaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtReqNegReqTec_PropostaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtReqNegReqTec_PropostaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ReqNegReqTec.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockreqnegreqtec_oscod_Internalname, "Req Tec_OSCod", "", "", lblTextblockreqnegreqtec_oscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ReqNegReqTec.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtReqNegReqTec_OSCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1946ReqNegReqTec_OSCod), 6, 0, ",", "")), ((edtReqNegReqTec_OSCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1946ReqNegReqTec_OSCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1946ReqNegReqTec_OSCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtReqNegReqTec_OSCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtReqNegReqTec_OSCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ReqNegReqTec.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_4W218e( true) ;
         }
         else
         {
            wb_table4_34_4W218e( false) ;
         }
      }

      protected void wb_table2_5_4W218( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ReqNegReqTec.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4W218e( true) ;
         }
         else
         {
            wb_table2_5_4W218e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtSolicServicoReqNeg_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSolicServicoReqNeg_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SOLICSERVICOREQNEG_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1895SolicServicoReqNeg_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
               }
               else
               {
                  A1895SolicServicoReqNeg_Codigo = (long)(context.localUtil.CToN( cgiGet( edtSolicServicoReqNeg_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtRequisito_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtRequisito_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "REQUISITO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtRequisito_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1919Requisito_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
               }
               else
               {
                  A1919Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( edtRequisito_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
               }
               A1931Requisito_Ordem = (short)(context.localUtil.CToN( cgiGet( edtRequisito_Ordem_Internalname), ",", "."));
               n1931Requisito_Ordem = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtReqNegReqTec_PropostaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtReqNegReqTec_PropostaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "REQNEGREQTEC_PROPOSTACOD");
                  AnyError = 1;
                  GX_FocusControl = edtReqNegReqTec_PropostaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1945ReqNegReqTec_PropostaCod = 0;
                  n1945ReqNegReqTec_PropostaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1945ReqNegReqTec_PropostaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1945ReqNegReqTec_PropostaCod), 6, 0)));
               }
               else
               {
                  A1945ReqNegReqTec_PropostaCod = (int)(context.localUtil.CToN( cgiGet( edtReqNegReqTec_PropostaCod_Internalname), ",", "."));
                  n1945ReqNegReqTec_PropostaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1945ReqNegReqTec_PropostaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1945ReqNegReqTec_PropostaCod), 6, 0)));
               }
               n1945ReqNegReqTec_PropostaCod = ((0==A1945ReqNegReqTec_PropostaCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtReqNegReqTec_OSCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtReqNegReqTec_OSCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "REQNEGREQTEC_OSCOD");
                  AnyError = 1;
                  GX_FocusControl = edtReqNegReqTec_OSCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1946ReqNegReqTec_OSCod = 0;
                  n1946ReqNegReqTec_OSCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1946ReqNegReqTec_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1946ReqNegReqTec_OSCod), 6, 0)));
               }
               else
               {
                  A1946ReqNegReqTec_OSCod = (int)(context.localUtil.CToN( cgiGet( edtReqNegReqTec_OSCod_Internalname), ",", "."));
                  n1946ReqNegReqTec_OSCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1946ReqNegReqTec_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1946ReqNegReqTec_OSCod), 6, 0)));
               }
               n1946ReqNegReqTec_OSCod = ((0==A1946ReqNegReqTec_OSCod) ? true : false);
               /* Read saved values. */
               Z1895SolicServicoReqNeg_Codigo = (long)(context.localUtil.CToN( cgiGet( "Z1895SolicServicoReqNeg_Codigo"), ",", "."));
               Z1919Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1919Requisito_Codigo"), ",", "."));
               Z1945ReqNegReqTec_PropostaCod = (int)(context.localUtil.CToN( cgiGet( "Z1945ReqNegReqTec_PropostaCod"), ",", "."));
               n1945ReqNegReqTec_PropostaCod = ((0==A1945ReqNegReqTec_PropostaCod) ? true : false);
               Z1946ReqNegReqTec_OSCod = (int)(context.localUtil.CToN( cgiGet( "Z1946ReqNegReqTec_OSCod"), ",", "."));
               n1946ReqNegReqTec_OSCod = ((0==A1946ReqNegReqTec_OSCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1895SolicServicoReqNeg_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
                  A1919Requisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4W218( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes4W218( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption4W0( )
      {
      }

      protected void ZM4W218( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1945ReqNegReqTec_PropostaCod = T004W3_A1945ReqNegReqTec_PropostaCod[0];
               Z1946ReqNegReqTec_OSCod = T004W3_A1946ReqNegReqTec_OSCod[0];
            }
            else
            {
               Z1945ReqNegReqTec_PropostaCod = A1945ReqNegReqTec_PropostaCod;
               Z1946ReqNegReqTec_OSCod = A1946ReqNegReqTec_OSCod;
            }
         }
         if ( GX_JID == -1 )
         {
            Z1945ReqNegReqTec_PropostaCod = A1945ReqNegReqTec_PropostaCod;
            Z1946ReqNegReqTec_OSCod = A1946ReqNegReqTec_OSCod;
            Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
            Z1919Requisito_Codigo = A1919Requisito_Codigo;
            Z1931Requisito_Ordem = A1931Requisito_Ordem;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load4W218( )
      {
         /* Using cursor T004W6 */
         pr_default.execute(4, new Object[] {A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound218 = 1;
            A1931Requisito_Ordem = T004W6_A1931Requisito_Ordem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
            n1931Requisito_Ordem = T004W6_n1931Requisito_Ordem[0];
            A1945ReqNegReqTec_PropostaCod = T004W6_A1945ReqNegReqTec_PropostaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1945ReqNegReqTec_PropostaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1945ReqNegReqTec_PropostaCod), 6, 0)));
            n1945ReqNegReqTec_PropostaCod = T004W6_n1945ReqNegReqTec_PropostaCod[0];
            A1946ReqNegReqTec_OSCod = T004W6_A1946ReqNegReqTec_OSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1946ReqNegReqTec_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1946ReqNegReqTec_OSCod), 6, 0)));
            n1946ReqNegReqTec_OSCod = T004W6_n1946ReqNegReqTec_OSCod[0];
            ZM4W218( -1) ;
         }
         pr_default.close(4);
         OnLoadActions4W218( ) ;
      }

      protected void OnLoadActions4W218( )
      {
      }

      protected void CheckExtendedTable4W218( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T004W4 */
         pr_default.execute(2, new Object[] {A1895SolicServicoReqNeg_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Requisitos de Neg�cio da Solicita��o de Servi�o'.", "ForeignKeyNotFound", 1, "SOLICSERVICOREQNEG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T004W5 */
         pr_default.execute(3, new Object[] {A1919Requisito_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T213'.", "ForeignKeyNotFound", 1, "REQUISITO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtRequisito_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1931Requisito_Ordem = T004W5_A1931Requisito_Ordem[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
         n1931Requisito_Ordem = T004W5_n1931Requisito_Ordem[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4W218( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( long A1895SolicServicoReqNeg_Codigo )
      {
         /* Using cursor T004W7 */
         pr_default.execute(5, new Object[] {A1895SolicServicoReqNeg_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Requisitos de Neg�cio da Solicita��o de Servi�o'.", "ForeignKeyNotFound", 1, "SOLICSERVICOREQNEG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_3( int A1919Requisito_Codigo )
      {
         /* Using cursor T004W8 */
         pr_default.execute(6, new Object[] {A1919Requisito_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T213'.", "ForeignKeyNotFound", 1, "REQUISITO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtRequisito_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1931Requisito_Ordem = T004W8_A1931Requisito_Ordem[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
         n1931Requisito_Ordem = T004W8_n1931Requisito_Ordem[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1931Requisito_Ordem), 3, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey4W218( )
      {
         /* Using cursor T004W9 */
         pr_default.execute(7, new Object[] {A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound218 = 1;
         }
         else
         {
            RcdFound218 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004W3 */
         pr_default.execute(1, new Object[] {A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4W218( 1) ;
            RcdFound218 = 1;
            A1945ReqNegReqTec_PropostaCod = T004W3_A1945ReqNegReqTec_PropostaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1945ReqNegReqTec_PropostaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1945ReqNegReqTec_PropostaCod), 6, 0)));
            n1945ReqNegReqTec_PropostaCod = T004W3_n1945ReqNegReqTec_PropostaCod[0];
            A1946ReqNegReqTec_OSCod = T004W3_A1946ReqNegReqTec_OSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1946ReqNegReqTec_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1946ReqNegReqTec_OSCod), 6, 0)));
            n1946ReqNegReqTec_OSCod = T004W3_n1946ReqNegReqTec_OSCod[0];
            A1895SolicServicoReqNeg_Codigo = T004W3_A1895SolicServicoReqNeg_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
            A1919Requisito_Codigo = T004W3_A1919Requisito_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
            Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
            Z1919Requisito_Codigo = A1919Requisito_Codigo;
            sMode218 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load4W218( ) ;
            if ( AnyError == 1 )
            {
               RcdFound218 = 0;
               InitializeNonKey4W218( ) ;
            }
            Gx_mode = sMode218;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound218 = 0;
            InitializeNonKey4W218( ) ;
            sMode218 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode218;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4W218( ) ;
         if ( RcdFound218 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound218 = 0;
         /* Using cursor T004W10 */
         pr_default.execute(8, new Object[] {A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T004W10_A1895SolicServicoReqNeg_Codigo[0] < A1895SolicServicoReqNeg_Codigo ) || ( T004W10_A1895SolicServicoReqNeg_Codigo[0] == A1895SolicServicoReqNeg_Codigo ) && ( T004W10_A1919Requisito_Codigo[0] < A1919Requisito_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T004W10_A1895SolicServicoReqNeg_Codigo[0] > A1895SolicServicoReqNeg_Codigo ) || ( T004W10_A1895SolicServicoReqNeg_Codigo[0] == A1895SolicServicoReqNeg_Codigo ) && ( T004W10_A1919Requisito_Codigo[0] > A1919Requisito_Codigo ) ) )
            {
               A1895SolicServicoReqNeg_Codigo = T004W10_A1895SolicServicoReqNeg_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
               A1919Requisito_Codigo = T004W10_A1919Requisito_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
               RcdFound218 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound218 = 0;
         /* Using cursor T004W11 */
         pr_default.execute(9, new Object[] {A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T004W11_A1895SolicServicoReqNeg_Codigo[0] > A1895SolicServicoReqNeg_Codigo ) || ( T004W11_A1895SolicServicoReqNeg_Codigo[0] == A1895SolicServicoReqNeg_Codigo ) && ( T004W11_A1919Requisito_Codigo[0] > A1919Requisito_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T004W11_A1895SolicServicoReqNeg_Codigo[0] < A1895SolicServicoReqNeg_Codigo ) || ( T004W11_A1895SolicServicoReqNeg_Codigo[0] == A1895SolicServicoReqNeg_Codigo ) && ( T004W11_A1919Requisito_Codigo[0] < A1919Requisito_Codigo ) ) )
            {
               A1895SolicServicoReqNeg_Codigo = T004W11_A1895SolicServicoReqNeg_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
               A1919Requisito_Codigo = T004W11_A1919Requisito_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
               RcdFound218 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4W218( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4W218( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound218 == 1 )
            {
               if ( ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo ) || ( A1919Requisito_Codigo != Z1919Requisito_Codigo ) )
               {
                  A1895SolicServicoReqNeg_Codigo = Z1895SolicServicoReqNeg_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
                  A1919Requisito_Codigo = Z1919Requisito_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SOLICSERVICOREQNEG_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update4W218( ) ;
                  GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo ) || ( A1919Requisito_Codigo != Z1919Requisito_Codigo ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4W218( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SOLICSERVICOREQNEG_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4W218( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo ) || ( A1919Requisito_Codigo != Z1919Requisito_Codigo ) )
         {
            A1895SolicServicoReqNeg_Codigo = Z1895SolicServicoReqNeg_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
            A1919Requisito_Codigo = Z1919Requisito_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SOLICSERVICOREQNEG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound218 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "SOLICSERVICOREQNEG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtReqNegReqTec_PropostaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4W218( ) ;
         if ( RcdFound218 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtReqNegReqTec_PropostaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4W218( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound218 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtReqNegReqTec_PropostaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound218 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtReqNegReqTec_PropostaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4W218( ) ;
         if ( RcdFound218 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound218 != 0 )
            {
               ScanNext4W218( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtReqNegReqTec_PropostaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4W218( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency4W218( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004W2 */
            pr_default.execute(0, new Object[] {A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReqNegReqTec"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1945ReqNegReqTec_PropostaCod != T004W2_A1945ReqNegReqTec_PropostaCod[0] ) || ( Z1946ReqNegReqTec_OSCod != T004W2_A1946ReqNegReqTec_OSCod[0] ) )
            {
               if ( Z1945ReqNegReqTec_PropostaCod != T004W2_A1945ReqNegReqTec_PropostaCod[0] )
               {
                  GXUtil.WriteLog("reqnegreqtec:[seudo value changed for attri]"+"ReqNegReqTec_PropostaCod");
                  GXUtil.WriteLogRaw("Old: ",Z1945ReqNegReqTec_PropostaCod);
                  GXUtil.WriteLogRaw("Current: ",T004W2_A1945ReqNegReqTec_PropostaCod[0]);
               }
               if ( Z1946ReqNegReqTec_OSCod != T004W2_A1946ReqNegReqTec_OSCod[0] )
               {
                  GXUtil.WriteLog("reqnegreqtec:[seudo value changed for attri]"+"ReqNegReqTec_OSCod");
                  GXUtil.WriteLogRaw("Old: ",Z1946ReqNegReqTec_OSCod);
                  GXUtil.WriteLogRaw("Current: ",T004W2_A1946ReqNegReqTec_OSCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ReqNegReqTec"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4W218( )
      {
         BeforeValidate4W218( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4W218( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4W218( 0) ;
            CheckOptimisticConcurrency4W218( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4W218( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4W218( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004W12 */
                     pr_default.execute(10, new Object[] {n1945ReqNegReqTec_PropostaCod, A1945ReqNegReqTec_PropostaCod, n1946ReqNegReqTec_OSCod, A1946ReqNegReqTec_OSCod, A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ReqNegReqTec") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4W0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4W218( ) ;
            }
            EndLevel4W218( ) ;
         }
         CloseExtendedTableCursors4W218( ) ;
      }

      protected void Update4W218( )
      {
         BeforeValidate4W218( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4W218( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4W218( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4W218( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4W218( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004W13 */
                     pr_default.execute(11, new Object[] {n1945ReqNegReqTec_PropostaCod, A1945ReqNegReqTec_PropostaCod, n1946ReqNegReqTec_OSCod, A1946ReqNegReqTec_OSCod, A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ReqNegReqTec") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReqNegReqTec"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4W218( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption4W0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4W218( ) ;
         }
         CloseExtendedTableCursors4W218( ) ;
      }

      protected void DeferredUpdate4W218( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate4W218( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4W218( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4W218( ) ;
            AfterConfirm4W218( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4W218( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004W14 */
                  pr_default.execute(12, new Object[] {A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("ReqNegReqTec") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound218 == 0 )
                        {
                           InitAll4W218( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption4W0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode218 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel4W218( ) ;
         Gx_mode = sMode218;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls4W218( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T004W15 */
            pr_default.execute(13, new Object[] {A1919Requisito_Codigo});
            A1931Requisito_Ordem = T004W15_A1931Requisito_Ordem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
            n1931Requisito_Ordem = T004W15_n1931Requisito_Ordem[0];
            pr_default.close(13);
         }
      }

      protected void EndLevel4W218( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4W218( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            context.CommitDataStores( "ReqNegReqTec");
            if ( AnyError == 0 )
            {
               ConfirmValues4W0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            context.RollbackDataStores( "ReqNegReqTec");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4W218( )
      {
         /* Using cursor T004W16 */
         pr_default.execute(14);
         RcdFound218 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound218 = 1;
            A1895SolicServicoReqNeg_Codigo = T004W16_A1895SolicServicoReqNeg_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
            A1919Requisito_Codigo = T004W16_A1919Requisito_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4W218( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound218 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound218 = 1;
            A1895SolicServicoReqNeg_Codigo = T004W16_A1895SolicServicoReqNeg_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
            A1919Requisito_Codigo = T004W16_A1919Requisito_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd4W218( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm4W218( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4W218( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4W218( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4W218( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4W218( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4W218( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4W218( )
      {
         edtSolicServicoReqNeg_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicServicoReqNeg_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicServicoReqNeg_Codigo_Enabled), 5, 0)));
         edtRequisito_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRequisito_Codigo_Enabled), 5, 0)));
         edtRequisito_Ordem_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRequisito_Ordem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRequisito_Ordem_Enabled), 5, 0)));
         edtReqNegReqTec_PropostaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReqNegReqTec_PropostaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReqNegReqTec_PropostaCod_Enabled), 5, 0)));
         edtReqNegReqTec_OSCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReqNegReqTec_OSCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtReqNegReqTec_OSCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4W0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117301727");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("reqnegreqtec.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1895SolicServicoReqNeg_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1919Requisito_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1919Requisito_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1945ReqNegReqTec_PropostaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1945ReqNegReqTec_PropostaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1946ReqNegReqTec_OSCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1946ReqNegReqTec_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("reqnegreqtec.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ReqNegReqTec" ;
      }

      public override String GetPgmdesc( )
      {
         return "Req Neg Req Tec" ;
      }

      protected void InitializeNonKey4W218( )
      {
         A1931Requisito_Ordem = 0;
         n1931Requisito_Ordem = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
         A1945ReqNegReqTec_PropostaCod = 0;
         n1945ReqNegReqTec_PropostaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1945ReqNegReqTec_PropostaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1945ReqNegReqTec_PropostaCod), 6, 0)));
         n1945ReqNegReqTec_PropostaCod = ((0==A1945ReqNegReqTec_PropostaCod) ? true : false);
         A1946ReqNegReqTec_OSCod = 0;
         n1946ReqNegReqTec_OSCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1946ReqNegReqTec_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1946ReqNegReqTec_OSCod), 6, 0)));
         n1946ReqNegReqTec_OSCod = ((0==A1946ReqNegReqTec_OSCod) ? true : false);
         Z1945ReqNegReqTec_PropostaCod = 0;
         Z1946ReqNegReqTec_OSCod = 0;
      }

      protected void InitAll4W218( )
      {
         A1895SolicServicoReqNeg_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
         A1919Requisito_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1919Requisito_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1919Requisito_Codigo), 6, 0)));
         InitializeNonKey4W218( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117301731");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("reqnegreqtec.js", "?20203117301731");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblocksolicservicoreqneg_codigo_Internalname = "TEXTBLOCKSOLICSERVICOREQNEG_CODIGO";
         edtSolicServicoReqNeg_Codigo_Internalname = "SOLICSERVICOREQNEG_CODIGO";
         lblTextblockrequisito_codigo_Internalname = "TEXTBLOCKREQUISITO_CODIGO";
         edtRequisito_Codigo_Internalname = "REQUISITO_CODIGO";
         lblTextblockrequisito_ordem_Internalname = "TEXTBLOCKREQUISITO_ORDEM";
         edtRequisito_Ordem_Internalname = "REQUISITO_ORDEM";
         lblTextblockreqnegreqtec_propostacod_Internalname = "TEXTBLOCKREQNEGREQTEC_PROPOSTACOD";
         edtReqNegReqTec_PropostaCod_Internalname = "REQNEGREQTEC_PROPOSTACOD";
         lblTextblockreqnegreqtec_oscod_Internalname = "TEXTBLOCKREQNEGREQTEC_OSCOD";
         edtReqNegReqTec_OSCod_Internalname = "REQNEGREQTEC_OSCOD";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Req Neg Req Tec";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtReqNegReqTec_OSCod_Jsonclick = "";
         edtReqNegReqTec_OSCod_Enabled = 1;
         edtReqNegReqTec_PropostaCod_Jsonclick = "";
         edtReqNegReqTec_PropostaCod_Enabled = 1;
         edtRequisito_Ordem_Jsonclick = "";
         edtRequisito_Ordem_Enabled = 0;
         edtRequisito_Codigo_Jsonclick = "";
         edtRequisito_Codigo_Enabled = 1;
         edtSolicServicoReqNeg_Codigo_Jsonclick = "";
         edtSolicServicoReqNeg_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T004W17 */
         pr_default.execute(15, new Object[] {A1895SolicServicoReqNeg_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Requisitos de Neg�cio da Solicita��o de Servi�o'.", "ForeignKeyNotFound", 1, "SOLICSERVICOREQNEG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(15);
         /* Using cursor T004W15 */
         pr_default.execute(13, new Object[] {A1919Requisito_Codigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T213'.", "ForeignKeyNotFound", 1, "REQUISITO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtRequisito_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1931Requisito_Ordem = T004W15_A1931Requisito_Ordem[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
         n1931Requisito_Ordem = T004W15_n1931Requisito_Ordem[0];
         pr_default.close(13);
         GX_FocusControl = edtReqNegReqTec_PropostaCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Solicservicoreqneg_codigo( long GX_Parm1 )
      {
         A1895SolicServicoReqNeg_Codigo = GX_Parm1;
         /* Using cursor T004W17 */
         pr_default.execute(15, new Object[] {A1895SolicServicoReqNeg_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Requisitos de Neg�cio da Solicita��o de Servi�o'.", "ForeignKeyNotFound", 1, "SOLICSERVICOREQNEG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicServicoReqNeg_Codigo_Internalname;
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Requisito_codigo( long GX_Parm1 ,
                                          int GX_Parm2 ,
                                          int GX_Parm3 ,
                                          int GX_Parm4 ,
                                          short GX_Parm5 )
      {
         A1895SolicServicoReqNeg_Codigo = GX_Parm1;
         A1919Requisito_Codigo = GX_Parm2;
         A1945ReqNegReqTec_PropostaCod = GX_Parm3;
         n1945ReqNegReqTec_PropostaCod = false;
         A1946ReqNegReqTec_OSCod = GX_Parm4;
         n1946ReqNegReqTec_OSCod = false;
         A1931Requisito_Ordem = GX_Parm5;
         n1931Requisito_Ordem = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T004W15 */
         pr_default.execute(13, new Object[] {A1919Requisito_Codigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T213'.", "ForeignKeyNotFound", 1, "REQUISITO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtRequisito_Codigo_Internalname;
         }
         A1931Requisito_Ordem = T004W15_A1931Requisito_Ordem[0];
         n1931Requisito_Ordem = T004W15_n1931Requisito_Ordem[0];
         pr_default.close(13);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1931Requisito_Ordem = 0;
            n1931Requisito_Ordem = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1945ReqNegReqTec_PropostaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1946ReqNegReqTec_OSCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1931Requisito_Ordem), 3, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1895SolicServicoReqNeg_Codigo), 10, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1919Requisito_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1945ReqNegReqTec_PropostaCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1946ReqNegReqTec_OSCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1931Requisito_Ordem), 3, 0, ".", "")));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblocksolicservicoreqneg_codigo_Jsonclick = "";
         lblTextblockrequisito_codigo_Jsonclick = "";
         lblTextblockrequisito_ordem_Jsonclick = "";
         lblTextblockreqnegreqtec_propostacod_Jsonclick = "";
         lblTextblockreqnegreqtec_oscod_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T004W6_A1931Requisito_Ordem = new short[1] ;
         T004W6_n1931Requisito_Ordem = new bool[] {false} ;
         T004W6_A1945ReqNegReqTec_PropostaCod = new int[1] ;
         T004W6_n1945ReqNegReqTec_PropostaCod = new bool[] {false} ;
         T004W6_A1946ReqNegReqTec_OSCod = new int[1] ;
         T004W6_n1946ReqNegReqTec_OSCod = new bool[] {false} ;
         T004W6_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004W6_A1919Requisito_Codigo = new int[1] ;
         T004W4_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004W5_A1931Requisito_Ordem = new short[1] ;
         T004W5_n1931Requisito_Ordem = new bool[] {false} ;
         T004W7_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004W8_A1931Requisito_Ordem = new short[1] ;
         T004W8_n1931Requisito_Ordem = new bool[] {false} ;
         T004W9_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004W9_A1919Requisito_Codigo = new int[1] ;
         T004W3_A1945ReqNegReqTec_PropostaCod = new int[1] ;
         T004W3_n1945ReqNegReqTec_PropostaCod = new bool[] {false} ;
         T004W3_A1946ReqNegReqTec_OSCod = new int[1] ;
         T004W3_n1946ReqNegReqTec_OSCod = new bool[] {false} ;
         T004W3_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004W3_A1919Requisito_Codigo = new int[1] ;
         sMode218 = "";
         T004W10_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004W10_A1919Requisito_Codigo = new int[1] ;
         T004W11_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004W11_A1919Requisito_Codigo = new int[1] ;
         T004W2_A1945ReqNegReqTec_PropostaCod = new int[1] ;
         T004W2_n1945ReqNegReqTec_PropostaCod = new bool[] {false} ;
         T004W2_A1946ReqNegReqTec_OSCod = new int[1] ;
         T004W2_n1946ReqNegReqTec_OSCod = new bool[] {false} ;
         T004W2_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004W2_A1919Requisito_Codigo = new int[1] ;
         T004W15_A1931Requisito_Ordem = new short[1] ;
         T004W15_n1931Requisito_Ordem = new bool[] {false} ;
         T004W16_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004W16_A1919Requisito_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T004W17_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.reqnegreqtec__default(),
            new Object[][] {
                new Object[] {
               T004W2_A1945ReqNegReqTec_PropostaCod, T004W2_n1945ReqNegReqTec_PropostaCod, T004W2_A1946ReqNegReqTec_OSCod, T004W2_n1946ReqNegReqTec_OSCod, T004W2_A1895SolicServicoReqNeg_Codigo, T004W2_A1919Requisito_Codigo
               }
               , new Object[] {
               T004W3_A1945ReqNegReqTec_PropostaCod, T004W3_n1945ReqNegReqTec_PropostaCod, T004W3_A1946ReqNegReqTec_OSCod, T004W3_n1946ReqNegReqTec_OSCod, T004W3_A1895SolicServicoReqNeg_Codigo, T004W3_A1919Requisito_Codigo
               }
               , new Object[] {
               T004W4_A1895SolicServicoReqNeg_Codigo
               }
               , new Object[] {
               T004W5_A1931Requisito_Ordem, T004W5_n1931Requisito_Ordem
               }
               , new Object[] {
               T004W6_A1931Requisito_Ordem, T004W6_n1931Requisito_Ordem, T004W6_A1945ReqNegReqTec_PropostaCod, T004W6_n1945ReqNegReqTec_PropostaCod, T004W6_A1946ReqNegReqTec_OSCod, T004W6_n1946ReqNegReqTec_OSCod, T004W6_A1895SolicServicoReqNeg_Codigo, T004W6_A1919Requisito_Codigo
               }
               , new Object[] {
               T004W7_A1895SolicServicoReqNeg_Codigo
               }
               , new Object[] {
               T004W8_A1931Requisito_Ordem, T004W8_n1931Requisito_Ordem
               }
               , new Object[] {
               T004W9_A1895SolicServicoReqNeg_Codigo, T004W9_A1919Requisito_Codigo
               }
               , new Object[] {
               T004W10_A1895SolicServicoReqNeg_Codigo, T004W10_A1919Requisito_Codigo
               }
               , new Object[] {
               T004W11_A1895SolicServicoReqNeg_Codigo, T004W11_A1919Requisito_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004W15_A1931Requisito_Ordem, T004W15_n1931Requisito_Ordem
               }
               , new Object[] {
               T004W16_A1895SolicServicoReqNeg_Codigo, T004W16_A1919Requisito_Codigo
               }
               , new Object[] {
               T004W17_A1895SolicServicoReqNeg_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1931Requisito_Ordem ;
      private short GX_JID ;
      private short Z1931Requisito_Ordem ;
      private short RcdFound218 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1919Requisito_Codigo ;
      private int Z1945ReqNegReqTec_PropostaCod ;
      private int Z1946ReqNegReqTec_OSCod ;
      private int A1919Requisito_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtSolicServicoReqNeg_Codigo_Enabled ;
      private int edtRequisito_Codigo_Enabled ;
      private int edtRequisito_Ordem_Enabled ;
      private int A1945ReqNegReqTec_PropostaCod ;
      private int edtReqNegReqTec_PropostaCod_Enabled ;
      private int A1946ReqNegReqTec_OSCod ;
      private int edtReqNegReqTec_OSCod_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private long Z1895SolicServicoReqNeg_Codigo ;
      private long A1895SolicServicoReqNeg_Codigo ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtSolicServicoReqNeg_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblocksolicservicoreqneg_codigo_Internalname ;
      private String lblTextblocksolicservicoreqneg_codigo_Jsonclick ;
      private String edtSolicServicoReqNeg_Codigo_Jsonclick ;
      private String lblTextblockrequisito_codigo_Internalname ;
      private String lblTextblockrequisito_codigo_Jsonclick ;
      private String edtRequisito_Codigo_Internalname ;
      private String edtRequisito_Codigo_Jsonclick ;
      private String lblTextblockrequisito_ordem_Internalname ;
      private String lblTextblockrequisito_ordem_Jsonclick ;
      private String edtRequisito_Ordem_Internalname ;
      private String edtRequisito_Ordem_Jsonclick ;
      private String lblTextblockreqnegreqtec_propostacod_Internalname ;
      private String lblTextblockreqnegreqtec_propostacod_Jsonclick ;
      private String edtReqNegReqTec_PropostaCod_Internalname ;
      private String edtReqNegReqTec_PropostaCod_Jsonclick ;
      private String lblTextblockreqnegreqtec_oscod_Internalname ;
      private String lblTextblockreqnegreqtec_oscod_Jsonclick ;
      private String edtReqNegReqTec_OSCod_Internalname ;
      private String edtReqNegReqTec_OSCod_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode218 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1931Requisito_Ordem ;
      private bool n1945ReqNegReqTec_PropostaCod ;
      private bool n1946ReqNegReqTec_OSCod ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] T004W6_A1931Requisito_Ordem ;
      private bool[] T004W6_n1931Requisito_Ordem ;
      private int[] T004W6_A1945ReqNegReqTec_PropostaCod ;
      private bool[] T004W6_n1945ReqNegReqTec_PropostaCod ;
      private int[] T004W6_A1946ReqNegReqTec_OSCod ;
      private bool[] T004W6_n1946ReqNegReqTec_OSCod ;
      private long[] T004W6_A1895SolicServicoReqNeg_Codigo ;
      private int[] T004W6_A1919Requisito_Codigo ;
      private long[] T004W4_A1895SolicServicoReqNeg_Codigo ;
      private short[] T004W5_A1931Requisito_Ordem ;
      private bool[] T004W5_n1931Requisito_Ordem ;
      private long[] T004W7_A1895SolicServicoReqNeg_Codigo ;
      private short[] T004W8_A1931Requisito_Ordem ;
      private bool[] T004W8_n1931Requisito_Ordem ;
      private long[] T004W9_A1895SolicServicoReqNeg_Codigo ;
      private int[] T004W9_A1919Requisito_Codigo ;
      private int[] T004W3_A1945ReqNegReqTec_PropostaCod ;
      private bool[] T004W3_n1945ReqNegReqTec_PropostaCod ;
      private int[] T004W3_A1946ReqNegReqTec_OSCod ;
      private bool[] T004W3_n1946ReqNegReqTec_OSCod ;
      private long[] T004W3_A1895SolicServicoReqNeg_Codigo ;
      private int[] T004W3_A1919Requisito_Codigo ;
      private long[] T004W10_A1895SolicServicoReqNeg_Codigo ;
      private int[] T004W10_A1919Requisito_Codigo ;
      private long[] T004W11_A1895SolicServicoReqNeg_Codigo ;
      private int[] T004W11_A1919Requisito_Codigo ;
      private int[] T004W2_A1945ReqNegReqTec_PropostaCod ;
      private bool[] T004W2_n1945ReqNegReqTec_PropostaCod ;
      private int[] T004W2_A1946ReqNegReqTec_OSCod ;
      private bool[] T004W2_n1946ReqNegReqTec_OSCod ;
      private long[] T004W2_A1895SolicServicoReqNeg_Codigo ;
      private int[] T004W2_A1919Requisito_Codigo ;
      private short[] T004W15_A1931Requisito_Ordem ;
      private bool[] T004W15_n1931Requisito_Ordem ;
      private long[] T004W16_A1895SolicServicoReqNeg_Codigo ;
      private int[] T004W16_A1919Requisito_Codigo ;
      private long[] T004W17_A1895SolicServicoReqNeg_Codigo ;
      private GXWebForm Form ;
   }

   public class reqnegreqtec__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004W6 ;
          prmT004W6 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004W4 ;
          prmT004W4 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004W5 ;
          prmT004W5 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004W7 ;
          prmT004W7 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004W8 ;
          prmT004W8 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004W9 ;
          prmT004W9 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004W3 ;
          prmT004W3 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004W10 ;
          prmT004W10 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004W11 ;
          prmT004W11 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004W2 ;
          prmT004W2 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004W12 ;
          prmT004W12 = new Object[] {
          new Object[] {"@ReqNegReqTec_PropostaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ReqNegReqTec_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004W13 ;
          prmT004W13 = new Object[] {
          new Object[] {"@ReqNegReqTec_PropostaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ReqNegReqTec_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004W14 ;
          prmT004W14 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004W16 ;
          prmT004W16 = new Object[] {
          } ;
          Object[] prmT004W17 ;
          prmT004W17 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT004W15 ;
          prmT004W15 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T004W2", "SELECT [ReqNegReqTec_PropostaCod], [ReqNegReqTec_OSCod], [SolicServicoReqNeg_Codigo], [Requisito_Codigo] FROM [ReqNegReqTec] WITH (UPDLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo AND [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004W2,1,0,true,false )
             ,new CursorDef("T004W3", "SELECT [ReqNegReqTec_PropostaCod], [ReqNegReqTec_OSCod], [SolicServicoReqNeg_Codigo], [Requisito_Codigo] FROM [ReqNegReqTec] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo AND [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004W3,1,0,true,false )
             ,new CursorDef("T004W4", "SELECT [SolicServicoReqNeg_Codigo] FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004W4,1,0,true,false )
             ,new CursorDef("T004W5", "SELECT [Requisito_Ordem] FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004W5,1,0,true,false )
             ,new CursorDef("T004W6", "SELECT T2.[Requisito_Ordem], TM1.[ReqNegReqTec_PropostaCod], TM1.[ReqNegReqTec_OSCod], TM1.[SolicServicoReqNeg_Codigo], TM1.[Requisito_Codigo] FROM ([ReqNegReqTec] TM1 WITH (NOLOCK) INNER JOIN [Requisito] T2 WITH (NOLOCK) ON T2.[Requisito_Codigo] = TM1.[Requisito_Codigo]) WHERE TM1.[SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo and TM1.[Requisito_Codigo] = @Requisito_Codigo ORDER BY TM1.[SolicServicoReqNeg_Codigo], TM1.[Requisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004W6,100,0,true,false )
             ,new CursorDef("T004W7", "SELECT [SolicServicoReqNeg_Codigo] FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004W7,1,0,true,false )
             ,new CursorDef("T004W8", "SELECT [Requisito_Ordem] FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004W8,1,0,true,false )
             ,new CursorDef("T004W9", "SELECT [SolicServicoReqNeg_Codigo], [Requisito_Codigo] FROM [ReqNegReqTec] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo AND [Requisito_Codigo] = @Requisito_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004W9,1,0,true,false )
             ,new CursorDef("T004W10", "SELECT TOP 1 [SolicServicoReqNeg_Codigo], [Requisito_Codigo] FROM [ReqNegReqTec] WITH (NOLOCK) WHERE ( [SolicServicoReqNeg_Codigo] > @SolicServicoReqNeg_Codigo or [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo and [Requisito_Codigo] > @Requisito_Codigo) ORDER BY [SolicServicoReqNeg_Codigo], [Requisito_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004W10,1,0,true,true )
             ,new CursorDef("T004W11", "SELECT TOP 1 [SolicServicoReqNeg_Codigo], [Requisito_Codigo] FROM [ReqNegReqTec] WITH (NOLOCK) WHERE ( [SolicServicoReqNeg_Codigo] < @SolicServicoReqNeg_Codigo or [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo and [Requisito_Codigo] < @Requisito_Codigo) ORDER BY [SolicServicoReqNeg_Codigo] DESC, [Requisito_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004W11,1,0,true,true )
             ,new CursorDef("T004W12", "INSERT INTO [ReqNegReqTec]([ReqNegReqTec_PropostaCod], [ReqNegReqTec_OSCod], [SolicServicoReqNeg_Codigo], [Requisito_Codigo]) VALUES(@ReqNegReqTec_PropostaCod, @ReqNegReqTec_OSCod, @SolicServicoReqNeg_Codigo, @Requisito_Codigo)", GxErrorMask.GX_NOMASK,prmT004W12)
             ,new CursorDef("T004W13", "UPDATE [ReqNegReqTec] SET [ReqNegReqTec_PropostaCod]=@ReqNegReqTec_PropostaCod, [ReqNegReqTec_OSCod]=@ReqNegReqTec_OSCod  WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo AND [Requisito_Codigo] = @Requisito_Codigo", GxErrorMask.GX_NOMASK,prmT004W13)
             ,new CursorDef("T004W14", "DELETE FROM [ReqNegReqTec]  WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo AND [Requisito_Codigo] = @Requisito_Codigo", GxErrorMask.GX_NOMASK,prmT004W14)
             ,new CursorDef("T004W15", "SELECT [Requisito_Ordem] FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004W15,1,0,true,false )
             ,new CursorDef("T004W16", "SELECT [SolicServicoReqNeg_Codigo], [Requisito_Codigo] FROM [ReqNegReqTec] WITH (NOLOCK) ORDER BY [SolicServicoReqNeg_Codigo], [Requisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004W16,100,0,true,false )
             ,new CursorDef("T004W17", "SELECT [SolicServicoReqNeg_Codigo] FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004W17,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((long[]) buf[4])[0] = rslt.getLong(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((long[]) buf[4])[0] = rslt.getLong(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((long[]) buf[6])[0] = rslt.getLong(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 5 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 6 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 13 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (long)parms[4]);
                stmt.SetParameter(4, (int)parms[5]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (long)parms[4]);
                stmt.SetParameter(4, (int)parms[5]);
                return;
             case 12 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
       }
    }

 }

}
