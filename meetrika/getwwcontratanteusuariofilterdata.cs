/*
               File: GetWWContratanteUsuarioFilterData
        Description: Get WWContratante Usuario Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:55.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratanteusuariofilterdata : GXProcedure
   {
      public getwwcontratanteusuariofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratanteusuariofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratanteusuariofilterdata objgetwwcontratanteusuariofilterdata;
         objgetwwcontratanteusuariofilterdata = new getwwcontratanteusuariofilterdata();
         objgetwwcontratanteusuariofilterdata.AV24DDOName = aP0_DDOName;
         objgetwwcontratanteusuariofilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetwwcontratanteusuariofilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratanteusuariofilterdata.AV28OptionsJson = "" ;
         objgetwwcontratanteusuariofilterdata.AV31OptionsDescJson = "" ;
         objgetwwcontratanteusuariofilterdata.AV33OptionIndexesJson = "" ;
         objgetwwcontratanteusuariofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratanteusuariofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratanteusuariofilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratanteusuariofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTEUSUARIO_CONTRATANTEFANOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTEUSUARIO_CONTRATANTERAZOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTEUSUARIO_USUARIOPESSOANOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("WWContratanteUsuarioGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratanteUsuarioGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("WWContratanteUsuarioGridState"), "");
         }
         AV51GXV1 = 1;
         while ( AV51GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV51GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTECOD") == 0 )
            {
               AV10TFContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContratanteUsuario_ContratanteCod_To = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTEFAN") == 0 )
            {
               AV12TFContratanteUsuario_ContratanteFan = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL") == 0 )
            {
               AV13TFContratanteUsuario_ContratanteFan_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 )
            {
               AV14TFContratanteUsuario_ContratanteRaz = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL") == 0 )
            {
               AV15TFContratanteUsuario_ContratanteRaz_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_USUARIOCOD") == 0 )
            {
               AV16TFContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV17TFContratanteUsuario_UsuarioCod_To = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_USUARIOPESSOACOD") == 0 )
            {
               AV18TFContratanteUsuario_UsuarioPessoaCod = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV19TFContratanteUsuario_UsuarioPessoaCod_To = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV20TFContratanteUsuario_UsuarioPessoaNom = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL") == 0 )
            {
               AV21TFContratanteUsuario_UsuarioPessoaNom_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            AV51GXV1 = (int)(AV51GXV1+1);
         }
         if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(1));
            AV40DynamicFiltersSelector1 = AV39GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "USUARIO_NOME") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV39GridStateDynamicFilter.gxTpr_Operator;
               AV42Usuario_Nome1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV39GridStateDynamicFilter.gxTpr_Operator;
               AV43ContratanteUsuario_UsuarioPessoaNom1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV44DynamicFiltersEnabled2 = true;
               AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(2));
               AV45DynamicFiltersSelector2 = AV39GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "USUARIO_NOME") == 0 )
               {
                  AV46DynamicFiltersOperator2 = AV39GridStateDynamicFilter.gxTpr_Operator;
                  AV47Usuario_Nome2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
               {
                  AV46DynamicFiltersOperator2 = AV39GridStateDynamicFilter.gxTpr_Operator;
                  AV48ContratanteUsuario_UsuarioPessoaNom2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATANTEUSUARIO_CONTRATANTEFANOPTIONS' Routine */
         AV12TFContratanteUsuario_ContratanteFan = AV22SearchTxt;
         AV13TFContratanteUsuario_ContratanteFan_Sel = "";
         AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV55WWContratanteUsuarioDS_3_Usuario_nome1 = AV42Usuario_Nome1;
         AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = AV43ContratanteUsuario_UsuarioPessoaNom1;
         AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 = AV44DynamicFiltersEnabled2;
         AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2 = AV45DynamicFiltersSelector2;
         AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 = AV46DynamicFiltersOperator2;
         AV60WWContratanteUsuarioDS_8_Usuario_nome2 = AV47Usuario_Nome2;
         AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = AV48ContratanteUsuario_UsuarioPessoaNom2;
         AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod = AV10TFContratanteUsuario_ContratanteCod;
         AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to = AV11TFContratanteUsuario_ContratanteCod_To;
         AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = AV12TFContratanteUsuario_ContratanteFan;
         AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel = AV13TFContratanteUsuario_ContratanteFan_Sel;
         AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = AV14TFContratanteUsuario_ContratanteRaz;
         AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel = AV15TFContratanteUsuario_ContratanteRaz_Sel;
         AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod = AV16TFContratanteUsuario_UsuarioCod;
         AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to = AV17TFContratanteUsuario_UsuarioCod_To;
         AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod = AV18TFContratanteUsuario_UsuarioPessoaCod;
         AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to = AV19TFContratanteUsuario_UsuarioPessoaCod_To;
         AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = AV20TFContratanteUsuario_UsuarioPessoaNom;
         AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel = AV21TFContratanteUsuario_UsuarioPessoaNom_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1 ,
                                              AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 ,
                                              AV55WWContratanteUsuarioDS_3_Usuario_nome1 ,
                                              AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ,
                                              AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 ,
                                              AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2 ,
                                              AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 ,
                                              AV60WWContratanteUsuarioDS_8_Usuario_nome2 ,
                                              AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ,
                                              AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod ,
                                              AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to ,
                                              AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel ,
                                              AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ,
                                              AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel ,
                                              AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ,
                                              AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod ,
                                              AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to ,
                                              AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod ,
                                              AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to ,
                                              AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel ,
                                              AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ,
                                              A2Usuario_Nome ,
                                              A62ContratanteUsuario_UsuarioPessoaNom ,
                                              A63ContratanteUsuario_ContratanteCod ,
                                              A65ContratanteUsuario_ContratanteFan ,
                                              A64ContratanteUsuario_ContratanteRaz ,
                                              A60ContratanteUsuario_UsuarioCod ,
                                              A61ContratanteUsuario_UsuarioPessoaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV55WWContratanteUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV55WWContratanteUsuarioDS_3_Usuario_nome1), 50, "%");
         lV55WWContratanteUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV55WWContratanteUsuarioDS_3_Usuario_nome1), 50, "%");
         lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1), 100, "%");
         lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1), 100, "%");
         lV60WWContratanteUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV60WWContratanteUsuarioDS_8_Usuario_nome2), 50, "%");
         lV60WWContratanteUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV60WWContratanteUsuarioDS_8_Usuario_nome2), 50, "%");
         lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2), 100, "%");
         lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2), 100, "%");
         lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = StringUtil.PadR( StringUtil.RTrim( AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan), 100, "%");
         lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = StringUtil.PadR( StringUtil.RTrim( AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz), 100, "%");
         lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = StringUtil.PadR( StringUtil.RTrim( AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom), 100, "%");
         /* Using cursor P00L42 */
         pr_default.execute(0, new Object[] {lV55WWContratanteUsuarioDS_3_Usuario_nome1, lV55WWContratanteUsuarioDS_3_Usuario_nome1, lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1, lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1, lV60WWContratanteUsuarioDS_8_Usuario_nome2, lV60WWContratanteUsuarioDS_8_Usuario_nome2, lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2, lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2, AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod, AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to, lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan, AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel, lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz, AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel, AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod, AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to, AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod, AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to, lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom, AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKL42 = false;
            A340ContratanteUsuario_ContratantePesCod = P00L42_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = P00L42_n340ContratanteUsuario_ContratantePesCod[0];
            A65ContratanteUsuario_ContratanteFan = P00L42_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = P00L42_n65ContratanteUsuario_ContratanteFan[0];
            A61ContratanteUsuario_UsuarioPessoaCod = P00L42_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P00L42_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A60ContratanteUsuario_UsuarioCod = P00L42_A60ContratanteUsuario_UsuarioCod[0];
            A64ContratanteUsuario_ContratanteRaz = P00L42_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = P00L42_n64ContratanteUsuario_ContratanteRaz[0];
            A63ContratanteUsuario_ContratanteCod = P00L42_A63ContratanteUsuario_ContratanteCod[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P00L42_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P00L42_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A2Usuario_Nome = P00L42_A2Usuario_Nome[0];
            n2Usuario_Nome = P00L42_n2Usuario_Nome[0];
            A61ContratanteUsuario_UsuarioPessoaCod = P00L42_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P00L42_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A2Usuario_Nome = P00L42_A2Usuario_Nome[0];
            n2Usuario_Nome = P00L42_n2Usuario_Nome[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P00L42_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P00L42_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A340ContratanteUsuario_ContratantePesCod = P00L42_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = P00L42_n340ContratanteUsuario_ContratantePesCod[0];
            A65ContratanteUsuario_ContratanteFan = P00L42_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = P00L42_n65ContratanteUsuario_ContratanteFan[0];
            A64ContratanteUsuario_ContratanteRaz = P00L42_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = P00L42_n64ContratanteUsuario_ContratanteRaz[0];
            AV34count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00L42_A65ContratanteUsuario_ContratanteFan[0], A65ContratanteUsuario_ContratanteFan) == 0 ) )
            {
               BRKL42 = false;
               A60ContratanteUsuario_UsuarioCod = P00L42_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = P00L42_A63ContratanteUsuario_ContratanteCod[0];
               AV34count = (long)(AV34count+1);
               BRKL42 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A65ContratanteUsuario_ContratanteFan)) )
            {
               AV26Option = A65ContratanteUsuario_ContratanteFan;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKL42 )
            {
               BRKL42 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATANTEUSUARIO_CONTRATANTERAZOPTIONS' Routine */
         AV14TFContratanteUsuario_ContratanteRaz = AV22SearchTxt;
         AV15TFContratanteUsuario_ContratanteRaz_Sel = "";
         AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV55WWContratanteUsuarioDS_3_Usuario_nome1 = AV42Usuario_Nome1;
         AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = AV43ContratanteUsuario_UsuarioPessoaNom1;
         AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 = AV44DynamicFiltersEnabled2;
         AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2 = AV45DynamicFiltersSelector2;
         AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 = AV46DynamicFiltersOperator2;
         AV60WWContratanteUsuarioDS_8_Usuario_nome2 = AV47Usuario_Nome2;
         AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = AV48ContratanteUsuario_UsuarioPessoaNom2;
         AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod = AV10TFContratanteUsuario_ContratanteCod;
         AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to = AV11TFContratanteUsuario_ContratanteCod_To;
         AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = AV12TFContratanteUsuario_ContratanteFan;
         AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel = AV13TFContratanteUsuario_ContratanteFan_Sel;
         AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = AV14TFContratanteUsuario_ContratanteRaz;
         AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel = AV15TFContratanteUsuario_ContratanteRaz_Sel;
         AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod = AV16TFContratanteUsuario_UsuarioCod;
         AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to = AV17TFContratanteUsuario_UsuarioCod_To;
         AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod = AV18TFContratanteUsuario_UsuarioPessoaCod;
         AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to = AV19TFContratanteUsuario_UsuarioPessoaCod_To;
         AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = AV20TFContratanteUsuario_UsuarioPessoaNom;
         AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel = AV21TFContratanteUsuario_UsuarioPessoaNom_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1 ,
                                              AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 ,
                                              AV55WWContratanteUsuarioDS_3_Usuario_nome1 ,
                                              AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ,
                                              AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 ,
                                              AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2 ,
                                              AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 ,
                                              AV60WWContratanteUsuarioDS_8_Usuario_nome2 ,
                                              AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ,
                                              AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod ,
                                              AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to ,
                                              AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel ,
                                              AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ,
                                              AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel ,
                                              AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ,
                                              AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod ,
                                              AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to ,
                                              AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod ,
                                              AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to ,
                                              AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel ,
                                              AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ,
                                              A2Usuario_Nome ,
                                              A62ContratanteUsuario_UsuarioPessoaNom ,
                                              A63ContratanteUsuario_ContratanteCod ,
                                              A65ContratanteUsuario_ContratanteFan ,
                                              A64ContratanteUsuario_ContratanteRaz ,
                                              A60ContratanteUsuario_UsuarioCod ,
                                              A61ContratanteUsuario_UsuarioPessoaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV55WWContratanteUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV55WWContratanteUsuarioDS_3_Usuario_nome1), 50, "%");
         lV55WWContratanteUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV55WWContratanteUsuarioDS_3_Usuario_nome1), 50, "%");
         lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1), 100, "%");
         lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1), 100, "%");
         lV60WWContratanteUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV60WWContratanteUsuarioDS_8_Usuario_nome2), 50, "%");
         lV60WWContratanteUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV60WWContratanteUsuarioDS_8_Usuario_nome2), 50, "%");
         lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2), 100, "%");
         lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2), 100, "%");
         lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = StringUtil.PadR( StringUtil.RTrim( AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan), 100, "%");
         lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = StringUtil.PadR( StringUtil.RTrim( AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz), 100, "%");
         lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = StringUtil.PadR( StringUtil.RTrim( AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom), 100, "%");
         /* Using cursor P00L43 */
         pr_default.execute(1, new Object[] {lV55WWContratanteUsuarioDS_3_Usuario_nome1, lV55WWContratanteUsuarioDS_3_Usuario_nome1, lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1, lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1, lV60WWContratanteUsuarioDS_8_Usuario_nome2, lV60WWContratanteUsuarioDS_8_Usuario_nome2, lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2, lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2, AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod, AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to, lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan, AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel, lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz, AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel, AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod, AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to, AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod, AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to, lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom, AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKL44 = false;
            A340ContratanteUsuario_ContratantePesCod = P00L43_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = P00L43_n340ContratanteUsuario_ContratantePesCod[0];
            A64ContratanteUsuario_ContratanteRaz = P00L43_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = P00L43_n64ContratanteUsuario_ContratanteRaz[0];
            A61ContratanteUsuario_UsuarioPessoaCod = P00L43_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P00L43_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A60ContratanteUsuario_UsuarioCod = P00L43_A60ContratanteUsuario_UsuarioCod[0];
            A65ContratanteUsuario_ContratanteFan = P00L43_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = P00L43_n65ContratanteUsuario_ContratanteFan[0];
            A63ContratanteUsuario_ContratanteCod = P00L43_A63ContratanteUsuario_ContratanteCod[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P00L43_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P00L43_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A2Usuario_Nome = P00L43_A2Usuario_Nome[0];
            n2Usuario_Nome = P00L43_n2Usuario_Nome[0];
            A61ContratanteUsuario_UsuarioPessoaCod = P00L43_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P00L43_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A2Usuario_Nome = P00L43_A2Usuario_Nome[0];
            n2Usuario_Nome = P00L43_n2Usuario_Nome[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P00L43_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P00L43_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A340ContratanteUsuario_ContratantePesCod = P00L43_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = P00L43_n340ContratanteUsuario_ContratantePesCod[0];
            A65ContratanteUsuario_ContratanteFan = P00L43_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = P00L43_n65ContratanteUsuario_ContratanteFan[0];
            A64ContratanteUsuario_ContratanteRaz = P00L43_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = P00L43_n64ContratanteUsuario_ContratanteRaz[0];
            AV34count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00L43_A64ContratanteUsuario_ContratanteRaz[0], A64ContratanteUsuario_ContratanteRaz) == 0 ) )
            {
               BRKL44 = false;
               A340ContratanteUsuario_ContratantePesCod = P00L43_A340ContratanteUsuario_ContratantePesCod[0];
               n340ContratanteUsuario_ContratantePesCod = P00L43_n340ContratanteUsuario_ContratantePesCod[0];
               A60ContratanteUsuario_UsuarioCod = P00L43_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = P00L43_A63ContratanteUsuario_ContratanteCod[0];
               A340ContratanteUsuario_ContratantePesCod = P00L43_A340ContratanteUsuario_ContratantePesCod[0];
               n340ContratanteUsuario_ContratantePesCod = P00L43_n340ContratanteUsuario_ContratantePesCod[0];
               AV34count = (long)(AV34count+1);
               BRKL44 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A64ContratanteUsuario_ContratanteRaz)) )
            {
               AV26Option = A64ContratanteUsuario_ContratanteRaz;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKL44 )
            {
               BRKL44 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATANTEUSUARIO_USUARIOPESSOANOMOPTIONS' Routine */
         AV20TFContratanteUsuario_UsuarioPessoaNom = AV22SearchTxt;
         AV21TFContratanteUsuario_UsuarioPessoaNom_Sel = "";
         AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV55WWContratanteUsuarioDS_3_Usuario_nome1 = AV42Usuario_Nome1;
         AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = AV43ContratanteUsuario_UsuarioPessoaNom1;
         AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 = AV44DynamicFiltersEnabled2;
         AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2 = AV45DynamicFiltersSelector2;
         AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 = AV46DynamicFiltersOperator2;
         AV60WWContratanteUsuarioDS_8_Usuario_nome2 = AV47Usuario_Nome2;
         AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = AV48ContratanteUsuario_UsuarioPessoaNom2;
         AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod = AV10TFContratanteUsuario_ContratanteCod;
         AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to = AV11TFContratanteUsuario_ContratanteCod_To;
         AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = AV12TFContratanteUsuario_ContratanteFan;
         AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel = AV13TFContratanteUsuario_ContratanteFan_Sel;
         AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = AV14TFContratanteUsuario_ContratanteRaz;
         AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel = AV15TFContratanteUsuario_ContratanteRaz_Sel;
         AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod = AV16TFContratanteUsuario_UsuarioCod;
         AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to = AV17TFContratanteUsuario_UsuarioCod_To;
         AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod = AV18TFContratanteUsuario_UsuarioPessoaCod;
         AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to = AV19TFContratanteUsuario_UsuarioPessoaCod_To;
         AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = AV20TFContratanteUsuario_UsuarioPessoaNom;
         AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel = AV21TFContratanteUsuario_UsuarioPessoaNom_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1 ,
                                              AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 ,
                                              AV55WWContratanteUsuarioDS_3_Usuario_nome1 ,
                                              AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ,
                                              AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 ,
                                              AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2 ,
                                              AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 ,
                                              AV60WWContratanteUsuarioDS_8_Usuario_nome2 ,
                                              AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ,
                                              AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod ,
                                              AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to ,
                                              AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel ,
                                              AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ,
                                              AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel ,
                                              AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ,
                                              AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod ,
                                              AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to ,
                                              AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod ,
                                              AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to ,
                                              AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel ,
                                              AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ,
                                              A2Usuario_Nome ,
                                              A62ContratanteUsuario_UsuarioPessoaNom ,
                                              A63ContratanteUsuario_ContratanteCod ,
                                              A65ContratanteUsuario_ContratanteFan ,
                                              A64ContratanteUsuario_ContratanteRaz ,
                                              A60ContratanteUsuario_UsuarioCod ,
                                              A61ContratanteUsuario_UsuarioPessoaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV55WWContratanteUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV55WWContratanteUsuarioDS_3_Usuario_nome1), 50, "%");
         lV55WWContratanteUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV55WWContratanteUsuarioDS_3_Usuario_nome1), 50, "%");
         lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1), 100, "%");
         lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1), 100, "%");
         lV60WWContratanteUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV60WWContratanteUsuarioDS_8_Usuario_nome2), 50, "%");
         lV60WWContratanteUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV60WWContratanteUsuarioDS_8_Usuario_nome2), 50, "%");
         lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2), 100, "%");
         lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2), 100, "%");
         lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = StringUtil.PadR( StringUtil.RTrim( AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan), 100, "%");
         lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = StringUtil.PadR( StringUtil.RTrim( AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz), 100, "%");
         lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = StringUtil.PadR( StringUtil.RTrim( AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom), 100, "%");
         /* Using cursor P00L44 */
         pr_default.execute(2, new Object[] {lV55WWContratanteUsuarioDS_3_Usuario_nome1, lV55WWContratanteUsuarioDS_3_Usuario_nome1, lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1, lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1, lV60WWContratanteUsuarioDS_8_Usuario_nome2, lV60WWContratanteUsuarioDS_8_Usuario_nome2, lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2, lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2, AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod, AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to, lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan, AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel, lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz, AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel, AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod, AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to, AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod, AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to, lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom, AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKL46 = false;
            A340ContratanteUsuario_ContratantePesCod = P00L44_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = P00L44_n340ContratanteUsuario_ContratantePesCod[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P00L44_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P00L44_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A61ContratanteUsuario_UsuarioPessoaCod = P00L44_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P00L44_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A60ContratanteUsuario_UsuarioCod = P00L44_A60ContratanteUsuario_UsuarioCod[0];
            A64ContratanteUsuario_ContratanteRaz = P00L44_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = P00L44_n64ContratanteUsuario_ContratanteRaz[0];
            A65ContratanteUsuario_ContratanteFan = P00L44_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = P00L44_n65ContratanteUsuario_ContratanteFan[0];
            A63ContratanteUsuario_ContratanteCod = P00L44_A63ContratanteUsuario_ContratanteCod[0];
            A2Usuario_Nome = P00L44_A2Usuario_Nome[0];
            n2Usuario_Nome = P00L44_n2Usuario_Nome[0];
            A61ContratanteUsuario_UsuarioPessoaCod = P00L44_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P00L44_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A2Usuario_Nome = P00L44_A2Usuario_Nome[0];
            n2Usuario_Nome = P00L44_n2Usuario_Nome[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P00L44_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P00L44_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A340ContratanteUsuario_ContratantePesCod = P00L44_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = P00L44_n340ContratanteUsuario_ContratantePesCod[0];
            A65ContratanteUsuario_ContratanteFan = P00L44_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = P00L44_n65ContratanteUsuario_ContratanteFan[0];
            A64ContratanteUsuario_ContratanteRaz = P00L44_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = P00L44_n64ContratanteUsuario_ContratanteRaz[0];
            AV34count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00L44_A62ContratanteUsuario_UsuarioPessoaNom[0], A62ContratanteUsuario_UsuarioPessoaNom) == 0 ) )
            {
               BRKL46 = false;
               A61ContratanteUsuario_UsuarioPessoaCod = P00L44_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = P00L44_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A60ContratanteUsuario_UsuarioCod = P00L44_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = P00L44_A63ContratanteUsuario_ContratanteCod[0];
               A61ContratanteUsuario_UsuarioPessoaCod = P00L44_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = P00L44_n61ContratanteUsuario_UsuarioPessoaCod[0];
               AV34count = (long)(AV34count+1);
               BRKL46 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A62ContratanteUsuario_UsuarioPessoaNom)) )
            {
               AV26Option = A62ContratanteUsuario_UsuarioPessoaNom;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKL46 )
            {
               BRKL46 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFContratanteUsuario_ContratanteFan = "";
         AV13TFContratanteUsuario_ContratanteFan_Sel = "";
         AV14TFContratanteUsuario_ContratanteRaz = "";
         AV15TFContratanteUsuario_ContratanteRaz_Sel = "";
         AV20TFContratanteUsuario_UsuarioPessoaNom = "";
         AV21TFContratanteUsuario_UsuarioPessoaNom_Sel = "";
         AV39GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV40DynamicFiltersSelector1 = "";
         AV42Usuario_Nome1 = "";
         AV43ContratanteUsuario_UsuarioPessoaNom1 = "";
         AV45DynamicFiltersSelector2 = "";
         AV47Usuario_Nome2 = "";
         AV48ContratanteUsuario_UsuarioPessoaNom2 = "";
         AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1 = "";
         AV55WWContratanteUsuarioDS_3_Usuario_nome1 = "";
         AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = "";
         AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2 = "";
         AV60WWContratanteUsuarioDS_8_Usuario_nome2 = "";
         AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = "";
         AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = "";
         AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel = "";
         AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = "";
         AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel = "";
         AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = "";
         AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel = "";
         scmdbuf = "";
         lV55WWContratanteUsuarioDS_3_Usuario_nome1 = "";
         lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 = "";
         lV60WWContratanteUsuarioDS_8_Usuario_nome2 = "";
         lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 = "";
         lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan = "";
         lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz = "";
         lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom = "";
         A2Usuario_Nome = "";
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         A65ContratanteUsuario_ContratanteFan = "";
         A64ContratanteUsuario_ContratanteRaz = "";
         P00L42_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         P00L42_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         P00L42_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         P00L42_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         P00L42_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         P00L42_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00L42_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00L42_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         P00L42_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         P00L42_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00L42_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00L42_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P00L42_A2Usuario_Nome = new String[] {""} ;
         P00L42_n2Usuario_Nome = new bool[] {false} ;
         AV26Option = "";
         P00L43_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         P00L43_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         P00L43_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         P00L43_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         P00L43_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         P00L43_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00L43_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00L43_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         P00L43_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         P00L43_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00L43_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00L43_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P00L43_A2Usuario_Nome = new String[] {""} ;
         P00L43_n2Usuario_Nome = new bool[] {false} ;
         P00L44_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         P00L44_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         P00L44_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00L44_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P00L44_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         P00L44_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00L44_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00L44_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         P00L44_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         P00L44_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         P00L44_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         P00L44_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00L44_A2Usuario_Nome = new String[] {""} ;
         P00L44_n2Usuario_Nome = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratanteusuariofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00L42_A340ContratanteUsuario_ContratantePesCod, P00L42_n340ContratanteUsuario_ContratantePesCod, P00L42_A65ContratanteUsuario_ContratanteFan, P00L42_n65ContratanteUsuario_ContratanteFan, P00L42_A61ContratanteUsuario_UsuarioPessoaCod, P00L42_n61ContratanteUsuario_UsuarioPessoaCod, P00L42_A60ContratanteUsuario_UsuarioCod, P00L42_A64ContratanteUsuario_ContratanteRaz, P00L42_n64ContratanteUsuario_ContratanteRaz, P00L42_A63ContratanteUsuario_ContratanteCod,
               P00L42_A62ContratanteUsuario_UsuarioPessoaNom, P00L42_n62ContratanteUsuario_UsuarioPessoaNom, P00L42_A2Usuario_Nome, P00L42_n2Usuario_Nome
               }
               , new Object[] {
               P00L43_A340ContratanteUsuario_ContratantePesCod, P00L43_n340ContratanteUsuario_ContratantePesCod, P00L43_A64ContratanteUsuario_ContratanteRaz, P00L43_n64ContratanteUsuario_ContratanteRaz, P00L43_A61ContratanteUsuario_UsuarioPessoaCod, P00L43_n61ContratanteUsuario_UsuarioPessoaCod, P00L43_A60ContratanteUsuario_UsuarioCod, P00L43_A65ContratanteUsuario_ContratanteFan, P00L43_n65ContratanteUsuario_ContratanteFan, P00L43_A63ContratanteUsuario_ContratanteCod,
               P00L43_A62ContratanteUsuario_UsuarioPessoaNom, P00L43_n62ContratanteUsuario_UsuarioPessoaNom, P00L43_A2Usuario_Nome, P00L43_n2Usuario_Nome
               }
               , new Object[] {
               P00L44_A340ContratanteUsuario_ContratantePesCod, P00L44_n340ContratanteUsuario_ContratantePesCod, P00L44_A62ContratanteUsuario_UsuarioPessoaNom, P00L44_n62ContratanteUsuario_UsuarioPessoaNom, P00L44_A61ContratanteUsuario_UsuarioPessoaCod, P00L44_n61ContratanteUsuario_UsuarioPessoaCod, P00L44_A60ContratanteUsuario_UsuarioCod, P00L44_A64ContratanteUsuario_ContratanteRaz, P00L44_n64ContratanteUsuario_ContratanteRaz, P00L44_A65ContratanteUsuario_ContratanteFan,
               P00L44_n65ContratanteUsuario_ContratanteFan, P00L44_A63ContratanteUsuario_ContratanteCod, P00L44_A2Usuario_Nome, P00L44_n2Usuario_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV41DynamicFiltersOperator1 ;
      private short AV46DynamicFiltersOperator2 ;
      private short AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 ;
      private short AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 ;
      private int AV51GXV1 ;
      private int AV10TFContratanteUsuario_ContratanteCod ;
      private int AV11TFContratanteUsuario_ContratanteCod_To ;
      private int AV16TFContratanteUsuario_UsuarioCod ;
      private int AV17TFContratanteUsuario_UsuarioCod_To ;
      private int AV18TFContratanteUsuario_UsuarioPessoaCod ;
      private int AV19TFContratanteUsuario_UsuarioPessoaCod_To ;
      private int AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod ;
      private int AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to ;
      private int AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod ;
      private int AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to ;
      private int AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod ;
      private int AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int A340ContratanteUsuario_ContratantePesCod ;
      private long AV34count ;
      private String AV12TFContratanteUsuario_ContratanteFan ;
      private String AV13TFContratanteUsuario_ContratanteFan_Sel ;
      private String AV14TFContratanteUsuario_ContratanteRaz ;
      private String AV15TFContratanteUsuario_ContratanteRaz_Sel ;
      private String AV20TFContratanteUsuario_UsuarioPessoaNom ;
      private String AV21TFContratanteUsuario_UsuarioPessoaNom_Sel ;
      private String AV42Usuario_Nome1 ;
      private String AV43ContratanteUsuario_UsuarioPessoaNom1 ;
      private String AV47Usuario_Nome2 ;
      private String AV48ContratanteUsuario_UsuarioPessoaNom2 ;
      private String AV55WWContratanteUsuarioDS_3_Usuario_nome1 ;
      private String AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ;
      private String AV60WWContratanteUsuarioDS_8_Usuario_nome2 ;
      private String AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ;
      private String AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ;
      private String AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel ;
      private String AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ;
      private String AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel ;
      private String AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ;
      private String AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel ;
      private String scmdbuf ;
      private String lV55WWContratanteUsuarioDS_3_Usuario_nome1 ;
      private String lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ;
      private String lV60WWContratanteUsuarioDS_8_Usuario_nome2 ;
      private String lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ;
      private String lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ;
      private String lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ;
      private String lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ;
      private String A2Usuario_Nome ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String A65ContratanteUsuario_ContratanteFan ;
      private String A64ContratanteUsuario_ContratanteRaz ;
      private bool returnInSub ;
      private bool AV44DynamicFiltersEnabled2 ;
      private bool AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 ;
      private bool BRKL42 ;
      private bool n340ContratanteUsuario_ContratantePesCod ;
      private bool n65ContratanteUsuario_ContratanteFan ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n64ContratanteUsuario_ContratanteRaz ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool n2Usuario_Nome ;
      private bool BRKL44 ;
      private bool BRKL46 ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV40DynamicFiltersSelector1 ;
      private String AV45DynamicFiltersSelector2 ;
      private String AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1 ;
      private String AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2 ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00L42_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] P00L42_n340ContratanteUsuario_ContratantePesCod ;
      private String[] P00L42_A65ContratanteUsuario_ContratanteFan ;
      private bool[] P00L42_n65ContratanteUsuario_ContratanteFan ;
      private int[] P00L42_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] P00L42_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] P00L42_A60ContratanteUsuario_UsuarioCod ;
      private String[] P00L42_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] P00L42_n64ContratanteUsuario_ContratanteRaz ;
      private int[] P00L42_A63ContratanteUsuario_ContratanteCod ;
      private String[] P00L42_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] P00L42_n62ContratanteUsuario_UsuarioPessoaNom ;
      private String[] P00L42_A2Usuario_Nome ;
      private bool[] P00L42_n2Usuario_Nome ;
      private int[] P00L43_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] P00L43_n340ContratanteUsuario_ContratantePesCod ;
      private String[] P00L43_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] P00L43_n64ContratanteUsuario_ContratanteRaz ;
      private int[] P00L43_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] P00L43_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] P00L43_A60ContratanteUsuario_UsuarioCod ;
      private String[] P00L43_A65ContratanteUsuario_ContratanteFan ;
      private bool[] P00L43_n65ContratanteUsuario_ContratanteFan ;
      private int[] P00L43_A63ContratanteUsuario_ContratanteCod ;
      private String[] P00L43_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] P00L43_n62ContratanteUsuario_UsuarioPessoaNom ;
      private String[] P00L43_A2Usuario_Nome ;
      private bool[] P00L43_n2Usuario_Nome ;
      private int[] P00L44_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] P00L44_n340ContratanteUsuario_ContratantePesCod ;
      private String[] P00L44_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] P00L44_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] P00L44_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] P00L44_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] P00L44_A60ContratanteUsuario_UsuarioCod ;
      private String[] P00L44_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] P00L44_n64ContratanteUsuario_ContratanteRaz ;
      private String[] P00L44_A65ContratanteUsuario_ContratanteFan ;
      private bool[] P00L44_n65ContratanteUsuario_ContratanteFan ;
      private int[] P00L44_A63ContratanteUsuario_ContratanteCod ;
      private String[] P00L44_A2Usuario_Nome ;
      private bool[] P00L44_n2Usuario_Nome ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV39GridStateDynamicFilter ;
   }

   public class getwwcontratanteusuariofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00L42( IGxContext context ,
                                             String AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1 ,
                                             short AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 ,
                                             String AV55WWContratanteUsuarioDS_3_Usuario_nome1 ,
                                             String AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ,
                                             bool AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 ,
                                             String AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2 ,
                                             short AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 ,
                                             String AV60WWContratanteUsuarioDS_8_Usuario_nome2 ,
                                             String AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ,
                                             int AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod ,
                                             int AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to ,
                                             String AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel ,
                                             String AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ,
                                             String AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel ,
                                             String AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ,
                                             int AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod ,
                                             int AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to ,
                                             int AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod ,
                                             int AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to ,
                                             String AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel ,
                                             String AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ,
                                             String A2Usuario_Nome ,
                                             String A62ContratanteUsuario_UsuarioPessoaNom ,
                                             int A63ContratanteUsuario_ContratanteCod ,
                                             String A65ContratanteUsuario_ContratanteFan ,
                                             String A64ContratanteUsuario_ContratanteRaz ,
                                             int A60ContratanteUsuario_UsuarioCod ,
                                             int A61ContratanteUsuario_UsuarioPessoaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [20] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T4.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T4.[Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T5.[Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz, T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, T2.[Usuario_Nome] FROM (((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Contratante] T4 WITH (NOLOCK) ON T4.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratante_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratanteUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV55WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV55WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratanteUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like '%' + @lV55WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like '%' + @lV55WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratanteUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV60WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV60WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratanteUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like '%' + @lV60WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like '%' + @lV60WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] >= @AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] >= @AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] <= @AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] <= @AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] like @lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] like @lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] = @AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] = @AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] >= @AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] >= @AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] <= @AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] <= @AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (0==AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] >= @AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (0==AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] <= @AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Contratante_NomeFantasia]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00L43( IGxContext context ,
                                             String AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1 ,
                                             short AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 ,
                                             String AV55WWContratanteUsuarioDS_3_Usuario_nome1 ,
                                             String AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ,
                                             bool AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 ,
                                             String AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2 ,
                                             short AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 ,
                                             String AV60WWContratanteUsuarioDS_8_Usuario_nome2 ,
                                             String AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ,
                                             int AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod ,
                                             int AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to ,
                                             String AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel ,
                                             String AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ,
                                             String AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel ,
                                             String AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ,
                                             int AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod ,
                                             int AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to ,
                                             int AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod ,
                                             int AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to ,
                                             String AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel ,
                                             String AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ,
                                             String A2Usuario_Nome ,
                                             String A62ContratanteUsuario_UsuarioPessoaNom ,
                                             int A63ContratanteUsuario_ContratanteCod ,
                                             String A65ContratanteUsuario_ContratanteFan ,
                                             String A64ContratanteUsuario_ContratanteRaz ,
                                             int A60ContratanteUsuario_UsuarioCod ,
                                             int A61ContratanteUsuario_UsuarioPessoaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [20] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T4.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T5.[Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz, T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T4.[Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, T2.[Usuario_Nome] FROM (((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Contratante] T4 WITH (NOLOCK) ON T4.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratante_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratanteUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV55WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV55WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratanteUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like '%' + @lV55WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like '%' + @lV55WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratanteUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV60WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV60WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratanteUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like '%' + @lV60WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like '%' + @lV60WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! (0==AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] >= @AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] >= @AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (0==AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] <= @AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] <= @AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] like @lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] like @lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] = @AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] = @AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] >= @AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] >= @AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] <= @AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] <= @AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (0==AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] >= @AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (0==AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] <= @AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T5.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00L44( IGxContext context ,
                                             String AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1 ,
                                             short AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 ,
                                             String AV55WWContratanteUsuarioDS_3_Usuario_nome1 ,
                                             String AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1 ,
                                             bool AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 ,
                                             String AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2 ,
                                             short AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 ,
                                             String AV60WWContratanteUsuarioDS_8_Usuario_nome2 ,
                                             String AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2 ,
                                             int AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod ,
                                             int AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to ,
                                             String AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel ,
                                             String AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan ,
                                             String AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel ,
                                             String AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz ,
                                             int AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod ,
                                             int AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to ,
                                             int AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod ,
                                             int AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to ,
                                             String AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel ,
                                             String AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom ,
                                             String A2Usuario_Nome ,
                                             String A62ContratanteUsuario_UsuarioPessoaNom ,
                                             int A63ContratanteUsuario_ContratanteCod ,
                                             String A65ContratanteUsuario_ContratanteFan ,
                                             String A64ContratanteUsuario_ContratanteRaz ,
                                             int A60ContratanteUsuario_UsuarioCod ,
                                             int A61ContratanteUsuario_UsuarioPessoaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [20] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T4.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T5.[Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz, T4.[Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T2.[Usuario_Nome] FROM (((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Contratante] T4 WITH (NOLOCK) ON T4.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratante_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratanteUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV55WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV55WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratanteUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like '%' + @lV55WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like '%' + @lV55WWContratanteUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWContratanteUsuarioDS_1_Dynamicfiltersselector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV54WWContratanteUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratanteUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV60WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV60WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratanteUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like '%' + @lV60WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like '%' + @lV60WWContratanteUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV57WWContratanteUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWContratanteUsuarioDS_6_Dynamicfiltersselector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV59WWContratanteUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! (0==AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] >= @AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] >= @AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! (0==AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] <= @AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] <= @AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] like @lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] like @lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] = @AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] = @AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (0==AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] >= @AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] >= @AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] <= @AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] <= @AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (0==AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] >= @AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (0==AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] <= @AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00L42(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] );
               case 1 :
                     return conditional_P00L43(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] );
               case 2 :
                     return conditional_P00L44(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00L42 ;
          prmP00L42 = new Object[] {
          new Object[] {"@lV55WWContratanteUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWContratanteUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60WWContratanteUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWContratanteUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan",SqlDbType.Char,100,0} ,
          new Object[] {"@AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz",SqlDbType.Char,100,0} ,
          new Object[] {"@AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel",SqlDbType.Char,100,0}
          } ;
          Object[] prmP00L43 ;
          prmP00L43 = new Object[] {
          new Object[] {"@lV55WWContratanteUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWContratanteUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60WWContratanteUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWContratanteUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan",SqlDbType.Char,100,0} ,
          new Object[] {"@AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz",SqlDbType.Char,100,0} ,
          new Object[] {"@AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel",SqlDbType.Char,100,0}
          } ;
          Object[] prmP00L44 ;
          prmP00L44 = new Object[] {
          new Object[] {"@lV55WWContratanteUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWContratanteUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56WWContratanteUsuarioDS_4_Contratanteusuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60WWContratanteUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWContratanteUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV61WWContratanteUsuarioDS_9_Contratanteusuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV62WWContratanteUsuarioDS_10_Tfcontratanteusuario_contratantecod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV63WWContratanteUsuarioDS_11_Tfcontratanteusuario_contratantecod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV64WWContratanteUsuarioDS_12_Tfcontratanteusuario_contratantefan",SqlDbType.Char,100,0} ,
          new Object[] {"@AV65WWContratanteUsuarioDS_13_Tfcontratanteusuario_contratantefan_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV66WWContratanteUsuarioDS_14_Tfcontratanteusuario_contratanteraz",SqlDbType.Char,100,0} ,
          new Object[] {"@AV67WWContratanteUsuarioDS_15_Tfcontratanteusuario_contratanteraz_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV68WWContratanteUsuarioDS_16_Tfcontratanteusuario_usuariocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV69WWContratanteUsuarioDS_17_Tfcontratanteusuario_usuariocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV70WWContratanteUsuarioDS_18_Tfcontratanteusuario_usuariopessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV71WWContratanteUsuarioDS_19_Tfcontratanteusuario_usuariopessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV72WWContratanteUsuarioDS_20_Tfcontratanteusuario_usuariopessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV73WWContratanteUsuarioDS_21_Tfcontratanteusuario_usuariopessoanom_sel",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00L42", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00L42,100,0,true,false )
             ,new CursorDef("P00L43", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00L43,100,0,true,false )
             ,new CursorDef("P00L44", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00L44,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratanteusuariofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratanteusuariofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratanteusuariofilterdata") )
          {
             return  ;
          }
          getwwcontratanteusuariofilterdata worker = new getwwcontratanteusuariofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
