/*
               File: PRC_CurrentPage
        Description: Pagina Atual
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:9.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_currentpage : GXProcedure
   {
      public prc_currentpage( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_currentpage( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_SelectedPage ,
                           out long aP1_PaginaAtual )
      {
         this.AV8SelectedPage = aP0_SelectedPage;
         this.AV9PaginaAtual = 0 ;
         initialize();
         executePrivate();
         aP1_PaginaAtual=this.AV9PaginaAtual;
      }

      public long executeUdp( String aP0_SelectedPage )
      {
         this.AV8SelectedPage = aP0_SelectedPage;
         this.AV9PaginaAtual = 0 ;
         initialize();
         executePrivate();
         aP1_PaginaAtual=this.AV9PaginaAtual;
         return AV9PaginaAtual ;
      }

      public void executeSubmit( String aP0_SelectedPage ,
                                 out long aP1_PaginaAtual )
      {
         prc_currentpage objprc_currentpage;
         objprc_currentpage = new prc_currentpage();
         objprc_currentpage.AV8SelectedPage = aP0_SelectedPage;
         objprc_currentpage.AV9PaginaAtual = 0 ;
         objprc_currentpage.context.SetSubmitInitialConfig(context);
         objprc_currentpage.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_currentpage);
         aP1_PaginaAtual=this.AV9PaginaAtual;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_currentpage)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV8SelectedPage, "Previous") == 0 )
         {
            AV9PaginaAtual = (long)(AV9PaginaAtual-1);
         }
         else if ( StringUtil.StrCmp(AV8SelectedPage, "Next") == 0 )
         {
            AV9PaginaAtual = (long)(AV9PaginaAtual+1);
         }
         else
         {
            AV9PaginaAtual = (long)(NumberUtil.Val( AV8SelectedPage, "."));
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private long AV9PaginaAtual ;
      private String AV8SelectedPage ;
      private long aP1_PaginaAtual ;
   }

}
