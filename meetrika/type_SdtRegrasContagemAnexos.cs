/*
               File: type_SdtRegrasContagemAnexos
        Description: Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:26:10.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "RegrasContagemAnexos" )]
   [XmlType(TypeName =  "RegrasContagemAnexos" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtRegrasContagemAnexos : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtRegrasContagemAnexos( )
      {
         /* Constructor for serialization */
         gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data = DateTime.MinValue;
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao = "";
         gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome = "";
         gxTv_SdtRegrasContagemAnexos_Mode = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra_Z = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_Z = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_Z = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z = DateTime.MinValue;
         gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome_Z = "";
      }

      public SdtRegrasContagemAnexos( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1005RegrasContagemAnexo_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1005RegrasContagemAnexo_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"RegrasContagemAnexo_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "RegrasContagemAnexos");
         metadata.Set("BT", "RegrasContagemAnexos");
         metadata.Set("PK", "[ \"RegrasContagemAnexo_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"RegrasContagemAnexo_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"RegrasContagem_Regra\" ],\"FKMap\":[  ] },{ \"FK\":[ \"TipoDocumento_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Regrascontagemanexo_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Regrascontagem_regra_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Regrascontagemanexo_nomearq_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Regrascontagemanexo_tipoarq_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Regrascontagemanexo_data_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Regrascontagemanexo_arquivo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Regrascontagemanexo_nomearq_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Regrascontagemanexo_tipoarq_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Regrascontagemanexo_data_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Regrascontagemanexo_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_codigo_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtRegrasContagemAnexos deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtRegrasContagemAnexos)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtRegrasContagemAnexos obj ;
         obj = this;
         obj.gxTpr_Regrascontagemanexo_codigo = deserialized.gxTpr_Regrascontagemanexo_codigo;
         obj.gxTpr_Regrascontagem_regra = deserialized.gxTpr_Regrascontagem_regra;
         obj.gxTpr_Regrascontagemanexo_arquivo = deserialized.gxTpr_Regrascontagemanexo_arquivo;
         obj.gxTpr_Regrascontagemanexo_nomearq = deserialized.gxTpr_Regrascontagemanexo_nomearq;
         obj.gxTpr_Regrascontagemanexo_tipoarq = deserialized.gxTpr_Regrascontagemanexo_tipoarq;
         obj.gxTpr_Regrascontagemanexo_data = deserialized.gxTpr_Regrascontagemanexo_data;
         obj.gxTpr_Regrascontagemanexo_descricao = deserialized.gxTpr_Regrascontagemanexo_descricao;
         obj.gxTpr_Tipodocumento_codigo = deserialized.gxTpr_Tipodocumento_codigo;
         obj.gxTpr_Tipodocumento_nome = deserialized.gxTpr_Tipodocumento_nome;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Regrascontagemanexo_codigo_Z = deserialized.gxTpr_Regrascontagemanexo_codigo_Z;
         obj.gxTpr_Regrascontagem_regra_Z = deserialized.gxTpr_Regrascontagem_regra_Z;
         obj.gxTpr_Regrascontagemanexo_nomearq_Z = deserialized.gxTpr_Regrascontagemanexo_nomearq_Z;
         obj.gxTpr_Regrascontagemanexo_tipoarq_Z = deserialized.gxTpr_Regrascontagemanexo_tipoarq_Z;
         obj.gxTpr_Regrascontagemanexo_data_Z = deserialized.gxTpr_Regrascontagemanexo_data_Z;
         obj.gxTpr_Tipodocumento_codigo_Z = deserialized.gxTpr_Tipodocumento_codigo_Z;
         obj.gxTpr_Tipodocumento_nome_Z = deserialized.gxTpr_Tipodocumento_nome_Z;
         obj.gxTpr_Regrascontagemanexo_arquivo_N = deserialized.gxTpr_Regrascontagemanexo_arquivo_N;
         obj.gxTpr_Regrascontagemanexo_nomearq_N = deserialized.gxTpr_Regrascontagemanexo_nomearq_N;
         obj.gxTpr_Regrascontagemanexo_tipoarq_N = deserialized.gxTpr_Regrascontagemanexo_tipoarq_N;
         obj.gxTpr_Regrascontagemanexo_data_N = deserialized.gxTpr_Regrascontagemanexo_data_N;
         obj.gxTpr_Regrascontagemanexo_descricao_N = deserialized.gxTpr_Regrascontagemanexo_descricao_N;
         obj.gxTpr_Tipodocumento_codigo_N = deserialized.gxTpr_Tipodocumento_codigo_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_Codigo") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagem_Regra") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_Arquivo") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo=context.FileFromBase64( oReader.Value) ;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_NomeArq") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_TipoArq") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_Data") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_Descricao") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo") )
               {
                  gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Nome") )
               {
                  gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtRegrasContagemAnexos_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtRegrasContagemAnexos_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_Codigo_Z") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagem_Regra_Z") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_NomeArq_Z") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_TipoArq_Z") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_Data_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo_Z") )
               {
                  gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Nome_Z") )
               {
                  gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_Arquivo_N") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_NomeArq_N") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_TipoArq_N") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_Data_N") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "RegrasContagemAnexo_Descricao_N") )
               {
                  gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo_N") )
               {
                  gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "RegrasContagemAnexos";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("RegrasContagemAnexo_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("RegrasContagem_Regra", StringUtil.RTrim( gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("RegrasContagemAnexo_Arquivo", context.FileToBase64( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("RegrasContagemAnexo_NomeArq", StringUtil.RTrim( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("RegrasContagemAnexo_TipoArq", StringUtil.RTrim( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data) )
         {
            oWriter.WriteStartElement("RegrasContagemAnexo_Data");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("RegrasContagemAnexo_Data", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("RegrasContagemAnexo_Descricao", StringUtil.RTrim( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("TipoDocumento_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("TipoDocumento_Nome", StringUtil.RTrim( gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtRegrasContagemAnexos_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRegrasContagemAnexos_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("RegrasContagemAnexo_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("RegrasContagem_Regra_Z", StringUtil.RTrim( gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("RegrasContagemAnexo_NomeArq_Z", StringUtil.RTrim( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("RegrasContagemAnexo_TipoArq_Z", StringUtil.RTrim( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z) )
            {
               oWriter.WriteStartElement("RegrasContagemAnexo_Data_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("RegrasContagemAnexo_Data_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("TipoDocumento_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("TipoDocumento_Nome_Z", StringUtil.RTrim( gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("RegrasContagemAnexo_Arquivo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("RegrasContagemAnexo_NomeArq_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("RegrasContagemAnexo_TipoArq_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("RegrasContagemAnexo_Data_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("RegrasContagemAnexo_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("TipoDocumento_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("RegrasContagemAnexo_Codigo", gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo, false);
         AddObjectProperty("RegrasContagem_Regra", gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra, false);
         AddObjectProperty("RegrasContagemAnexo_Arquivo", gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo, false);
         AddObjectProperty("RegrasContagemAnexo_NomeArq", gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq, false);
         AddObjectProperty("RegrasContagemAnexo_TipoArq", gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("RegrasContagemAnexo_Data", sDateCnv, false);
         AddObjectProperty("RegrasContagemAnexo_Descricao", gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao, false);
         AddObjectProperty("TipoDocumento_Codigo", gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo, false);
         AddObjectProperty("TipoDocumento_Nome", gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtRegrasContagemAnexos_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtRegrasContagemAnexos_Initialized, false);
            AddObjectProperty("RegrasContagemAnexo_Codigo_Z", gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo_Z, false);
            AddObjectProperty("RegrasContagem_Regra_Z", gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra_Z, false);
            AddObjectProperty("RegrasContagemAnexo_NomeArq_Z", gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_Z, false);
            AddObjectProperty("RegrasContagemAnexo_TipoArq_Z", gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("RegrasContagemAnexo_Data_Z", sDateCnv, false);
            AddObjectProperty("TipoDocumento_Codigo_Z", gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_Z, false);
            AddObjectProperty("TipoDocumento_Nome_Z", gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome_Z, false);
            AddObjectProperty("RegrasContagemAnexo_Arquivo_N", gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_N, false);
            AddObjectProperty("RegrasContagemAnexo_NomeArq_N", gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_N, false);
            AddObjectProperty("RegrasContagemAnexo_TipoArq_N", gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_N, false);
            AddObjectProperty("RegrasContagemAnexo_Data_N", gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_N, false);
            AddObjectProperty("RegrasContagemAnexo_Descricao_N", gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao_N, false);
            AddObjectProperty("TipoDocumento_Codigo_N", gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_Codigo" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_Codigo"   )]
      public int gxTpr_Regrascontagemanexo_codigo
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo ;
         }

         set {
            if ( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo != value )
            {
               gxTv_SdtRegrasContagemAnexos_Mode = "INS";
               this.gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo_Z_SetNull( );
               this.gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra_Z_SetNull( );
               this.gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_Z_SetNull( );
               this.gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_Z_SetNull( );
               this.gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z_SetNull( );
               this.gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_Z_SetNull( );
               this.gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome_Z_SetNull( );
            }
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "RegrasContagem_Regra" )]
      [  XmlElement( ElementName = "RegrasContagem_Regra"   )]
      public String gxTpr_Regrascontagem_regra
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra = (String)(value);
         }

      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_Arquivo" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_Arquivo"   )]
      [GxUpload()]
      public byte[] gxTpr_Regrascontagemanexo_arquivo_Blob
      {
         get {
            IGxContext context = this.context == null ? new GxContext() : this.context;
            return context.FileToByteArray( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo) ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_N = 0;
            IGxContext context = this.context == null ? new GxContext() : this.context;
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo=context.FileFromByteArray( value) ;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      [GxUpload()]
      public String gxTpr_Regrascontagemanexo_arquivo
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_N = 0;
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo = value;
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_SetBlob( String blob ,
                                                                                    String fileName ,
                                                                                    String fileType )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo = blob;
         return  ;
      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_N = 1;
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo = "";
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_NomeArq" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_NomeArq"   )]
      public String gxTpr_Regrascontagemanexo_nomearq
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_N = 0;
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq = (String)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_N = 1;
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq = "";
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_TipoArq" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_TipoArq"   )]
      public String gxTpr_Regrascontagemanexo_tipoarq
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_N = 0;
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq = (String)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_N = 1;
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq = "";
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_Data" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_Data"  , IsNullable=true )]
      public string gxTpr_Regrascontagemanexo_data_Nullable
      {
         get {
            if ( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data).value ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_N = 0;
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data = DateTime.MinValue;
            else
               gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Regrascontagemanexo_data
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_N = 0;
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data = (DateTime)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_N = 1;
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_Descricao" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_Descricao"   )]
      public String gxTpr_Regrascontagemanexo_descricao
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao_N = 0;
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao = (String)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao_N = 1;
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao = "";
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo"   )]
      public int gxTpr_Tipodocumento_codigo
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_N = 0;
            gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo = (int)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_N = 1;
         gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Nome" )]
      [  XmlElement( ElementName = "TipoDocumento_Nome"   )]
      public String gxTpr_Tipodocumento_nome
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Mode ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Mode = (String)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Mode_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Mode = "";
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Initialized ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Initialized_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_Codigo_Z" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_Codigo_Z"   )]
      public int gxTpr_Regrascontagemanexo_codigo_Z
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo_Z ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo_Z_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagem_Regra_Z" )]
      [  XmlElement( ElementName = "RegrasContagem_Regra_Z"   )]
      public String gxTpr_Regrascontagem_regra_Z
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra_Z ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra_Z = (String)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra_Z_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra_Z = "";
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_NomeArq_Z" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_NomeArq_Z"   )]
      public String gxTpr_Regrascontagemanexo_nomearq_Z
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_Z ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_Z = (String)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_Z_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_Z = "";
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_TipoArq_Z" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_TipoArq_Z"   )]
      public String gxTpr_Regrascontagemanexo_tipoarq_Z
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_Z ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_Z = (String)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_Z_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_Z = "";
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_Data_Z" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_Data_Z"  , IsNullable=true )]
      public string gxTpr_Regrascontagemanexo_data_Z_Nullable
      {
         get {
            if ( gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z = DateTime.MinValue;
            else
               gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Regrascontagemanexo_data_Z
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo_Z" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo_Z"   )]
      public int gxTpr_Tipodocumento_codigo_Z
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_Z ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_Z_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Nome_Z" )]
      [  XmlElement( ElementName = "TipoDocumento_Nome_Z"   )]
      public String gxTpr_Tipodocumento_nome_Z
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome_Z ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome_Z_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_Arquivo_N" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_Arquivo_N"   )]
      public short gxTpr_Regrascontagemanexo_arquivo_N
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_N ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_N = (short)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_N_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_N = 0;
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_NomeArq_N" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_NomeArq_N"   )]
      public short gxTpr_Regrascontagemanexo_nomearq_N
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_N ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_N = (short)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_N_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_N = 0;
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_TipoArq_N" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_TipoArq_N"   )]
      public short gxTpr_Regrascontagemanexo_tipoarq_N
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_N ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_N = (short)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_N_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_N = 0;
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_Data_N" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_Data_N"   )]
      public short gxTpr_Regrascontagemanexo_data_N
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_N ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_N = (short)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_N_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_N = 0;
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RegrasContagemAnexo_Descricao_N" )]
      [  XmlElement( ElementName = "RegrasContagemAnexo_Descricao_N"   )]
      public short gxTpr_Regrascontagemanexo_descricao_N
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao_N ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao_N_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo_N" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo_N"   )]
      public short gxTpr_Tipodocumento_codigo_N
      {
         get {
            return gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_N ;
         }

         set {
            gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_N_SetNull( )
      {
         gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data = DateTime.MinValue;
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao = "";
         gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome = "";
         gxTv_SdtRegrasContagemAnexos_Mode = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra_Z = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_Z = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_Z = "";
         gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z = DateTime.MinValue;
         gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "regrascontagemanexos", "GeneXus.Programs.regrascontagemanexos_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtRegrasContagemAnexos_Initialized ;
      private short gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo_N ;
      private short gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_N ;
      private short gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_N ;
      private short gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_N ;
      private short gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao_N ;
      private short gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo ;
      private int gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo ;
      private int gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_codigo_Z ;
      private int gxTv_SdtRegrasContagemAnexos_Tipodocumento_codigo_Z ;
      private String gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq ;
      private String gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq ;
      private String gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome ;
      private String gxTv_SdtRegrasContagemAnexos_Mode ;
      private String gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_nomearq_Z ;
      private String gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_tipoarq_Z ;
      private String gxTv_SdtRegrasContagemAnexos_Tipodocumento_nome_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data ;
      private DateTime gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_data_Z ;
      private String gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_descricao ;
      private String gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra ;
      private String gxTv_SdtRegrasContagemAnexos_Regrascontagem_regra_Z ;
      private String gxTv_SdtRegrasContagemAnexos_Regrascontagemanexo_arquivo ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"RegrasContagemAnexos", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtRegrasContagemAnexos_RESTInterface : GxGenericCollectionItem<SdtRegrasContagemAnexos>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtRegrasContagemAnexos_RESTInterface( ) : base()
      {
      }

      public SdtRegrasContagemAnexos_RESTInterface( SdtRegrasContagemAnexos psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "RegrasContagemAnexo_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Regrascontagemanexo_codigo
      {
         get {
            return sdt.gxTpr_Regrascontagemanexo_codigo ;
         }

         set {
            sdt.gxTpr_Regrascontagemanexo_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "RegrasContagem_Regra" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Regrascontagem_regra
      {
         get {
            return sdt.gxTpr_Regrascontagem_regra ;
         }

         set {
            sdt.gxTpr_Regrascontagem_regra = (String)(value);
         }

      }

      [DataMember( Name = "RegrasContagemAnexo_Arquivo" , Order = 2 )]
      [GxUpload()]
      public String gxTpr_Regrascontagemanexo_arquivo
      {
         get {
            return PathUtil.RelativePath( sdt.gxTpr_Regrascontagemanexo_arquivo) ;
         }

         set {
            sdt.gxTpr_Regrascontagemanexo_arquivo = value;
         }

      }

      [DataMember( Name = "RegrasContagemAnexo_NomeArq" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Regrascontagemanexo_nomearq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Regrascontagemanexo_nomearq) ;
         }

         set {
            sdt.gxTpr_Regrascontagemanexo_nomearq = (String)(value);
         }

      }

      [DataMember( Name = "RegrasContagemAnexo_TipoArq" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Regrascontagemanexo_tipoarq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Regrascontagemanexo_tipoarq) ;
         }

         set {
            sdt.gxTpr_Regrascontagemanexo_tipoarq = (String)(value);
         }

      }

      [DataMember( Name = "RegrasContagemAnexo_Data" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Regrascontagemanexo_data
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Regrascontagemanexo_data) ;
         }

         set {
            sdt.gxTpr_Regrascontagemanexo_data = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "RegrasContagemAnexo_Descricao" , Order = 6 )]
      public String gxTpr_Regrascontagemanexo_descricao
      {
         get {
            return sdt.gxTpr_Regrascontagemanexo_descricao ;
         }

         set {
            sdt.gxTpr_Regrascontagemanexo_descricao = (String)(value);
         }

      }

      [DataMember( Name = "TipoDocumento_Codigo" , Order = 7 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tipodocumento_codigo
      {
         get {
            return sdt.gxTpr_Tipodocumento_codigo ;
         }

         set {
            sdt.gxTpr_Tipodocumento_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "TipoDocumento_Nome" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Tipodocumento_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tipodocumento_nome) ;
         }

         set {
            sdt.gxTpr_Tipodocumento_nome = (String)(value);
         }

      }

      public SdtRegrasContagemAnexos sdt
      {
         get {
            return (SdtRegrasContagemAnexos)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtRegrasContagemAnexos() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 24 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
