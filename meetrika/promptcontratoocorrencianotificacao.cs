/*
               File: PromptContratoOcorrenciaNotificacao
        Description: Selecione Contrato Ocorrencia Notificacao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:36:33.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratoocorrencianotificacao : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratoocorrencianotificacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratoocorrencianotificacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratoOcorrenciaNotificacao_Codigo ,
                           ref DateTime aP1_InOutContratoOcorrenciaNotificacao_Data )
      {
         this.AV7InOutContratoOcorrenciaNotificacao_Codigo = aP0_InOutContratoOcorrenciaNotificacao_Codigo;
         this.AV8InOutContratoOcorrenciaNotificacao_Data = aP1_InOutContratoOcorrenciaNotificacao_Data;
         executePrivate();
         aP0_InOutContratoOcorrenciaNotificacao_Codigo=this.AV7InOutContratoOcorrenciaNotificacao_Codigo;
         aP1_InOutContratoOcorrenciaNotificacao_Data=this.AV8InOutContratoOcorrenciaNotificacao_Data;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_104 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_104_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_104_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoOcorrenciaNotificacao_Data1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
               AV18ContratoOcorrenciaNotificacao_Data_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
               AV19ContratoOcorrenciaNotificacao_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoOcorrenciaNotificacao_Nome1", AV19ContratoOcorrenciaNotificacao_Nome1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23ContratoOcorrenciaNotificacao_Data2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
               AV24ContratoOcorrenciaNotificacao_Data_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
               AV25ContratoOcorrenciaNotificacao_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoOcorrenciaNotificacao_Nome2", AV25ContratoOcorrenciaNotificacao_Nome2);
               AV27DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
               AV29ContratoOcorrenciaNotificacao_Data3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContratoOcorrenciaNotificacao_Data3", context.localUtil.Format(AV29ContratoOcorrenciaNotificacao_Data3, "99/99/99"));
               AV30ContratoOcorrenciaNotificacao_Data_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratoOcorrenciaNotificacao_Data_To3", context.localUtil.Format(AV30ContratoOcorrenciaNotificacao_Data_To3, "99/99/99"));
               AV31ContratoOcorrenciaNotificacao_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrenciaNotificacao_Nome3", AV31ContratoOcorrenciaNotificacao_Nome3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV26DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
               AV37TFContratoOcorrenciaNotificacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoOcorrenciaNotificacao_Codigo), 6, 0)));
               AV38TFContratoOcorrenciaNotificacao_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoOcorrenciaNotificacao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContratoOcorrenciaNotificacao_Codigo_To), 6, 0)));
               AV41TFContratoOcorrencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContratoOcorrencia_Codigo), 6, 0)));
               AV42TFContratoOcorrencia_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoOcorrencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFContratoOcorrencia_Codigo_To), 6, 0)));
               AV45TFContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContrato_Codigo), 6, 0)));
               AV46TFContrato_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContrato_Codigo_To), 6, 0)));
               AV49TFContrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContrato_Numero", AV49TFContrato_Numero);
               AV50TFContrato_Numero_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContrato_Numero_Sel", AV50TFContrato_Numero_Sel);
               AV53TFContratoOcorrenciaNotificacao_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
               AV54TFContratoOcorrenciaNotificacao_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
               AV59TFContratoOcorrenciaNotificacao_Prazo = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
               AV60TFContratoOcorrenciaNotificacao_Prazo_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
               AV63TFContratoOcorrenciaNotificacao_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoOcorrenciaNotificacao_Descricao", AV63TFContratoOcorrenciaNotificacao_Descricao);
               AV64TFContratoOcorrenciaNotificacao_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoOcorrenciaNotificacao_Descricao_Sel", AV64TFContratoOcorrenciaNotificacao_Descricao_Sel);
               AV67TFContratoOcorrenciaNotificacao_Cumprido = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV67TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
               AV68TFContratoOcorrenciaNotificacao_Cumprido_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV68TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
               AV73TFContratoOcorrenciaNotificacao_Protocolo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratoOcorrenciaNotificacao_Protocolo", AV73TFContratoOcorrenciaNotificacao_Protocolo);
               AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel", AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel);
               AV77TFContratoOcorrenciaNotificacao_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
               AV78TFContratoOcorrenciaNotificacao_Responsavel_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContratoOcorrenciaNotificacao_Responsavel_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0)));
               AV81TFContratoOcorrenciaNotificacao_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoOcorrenciaNotificacao_Nome", AV81TFContratoOcorrenciaNotificacao_Nome);
               AV82TFContratoOcorrenciaNotificacao_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratoOcorrenciaNotificacao_Nome_Sel", AV82TFContratoOcorrenciaNotificacao_Nome_Sel);
               AV85TFContratoOcorrenciaNotificacao_Docto = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoOcorrenciaNotificacao_Docto", AV85TFContratoOcorrenciaNotificacao_Docto);
               AV86TFContratoOcorrenciaNotificacao_Docto_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFContratoOcorrenciaNotificacao_Docto_Sel", AV86TFContratoOcorrenciaNotificacao_Docto_Sel);
               AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace", AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace);
               AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace", AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace);
               AV47ddo_Contrato_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Contrato_CodigoTitleControlIdToReplace", AV47ddo_Contrato_CodigoTitleControlIdToReplace);
               AV51ddo_Contrato_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_Contrato_NumeroTitleControlIdToReplace", AV51ddo_Contrato_NumeroTitleControlIdToReplace);
               AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace", AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace);
               AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace", AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace);
               AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace", AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace);
               AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace", AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace);
               AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace", AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace);
               AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace", AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace);
               AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace", AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace);
               AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace", AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace);
               AV95Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV33DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
               AV32DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContratoOcorrenciaNotificacao_Data3, AV30ContratoOcorrenciaNotificacao_Data_To3, AV31ContratoOcorrenciaNotificacao_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFContratoOcorrenciaNotificacao_Codigo, AV38TFContratoOcorrenciaNotificacao_Codigo_To, AV41TFContratoOcorrencia_Codigo, AV42TFContratoOcorrencia_Codigo_To, AV45TFContrato_Codigo, AV46TFContrato_Codigo_To, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoOcorrenciaNotificacao_Data, AV54TFContratoOcorrenciaNotificacao_Data_To, AV59TFContratoOcorrenciaNotificacao_Prazo, AV60TFContratoOcorrenciaNotificacao_Prazo_To, AV63TFContratoOcorrenciaNotificacao_Descricao, AV64TFContratoOcorrenciaNotificacao_Descricao_Sel, AV67TFContratoOcorrenciaNotificacao_Cumprido, AV68TFContratoOcorrenciaNotificacao_Cumprido_To, AV73TFContratoOcorrenciaNotificacao_Protocolo, AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV77TFContratoOcorrenciaNotificacao_Responsavel, AV78TFContratoOcorrenciaNotificacao_Responsavel_To, AV81TFContratoOcorrenciaNotificacao_Nome, AV82TFContratoOcorrenciaNotificacao_Nome_Sel, AV85TFContratoOcorrenciaNotificacao_Docto, AV86TFContratoOcorrenciaNotificacao_Docto_Sel, AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace, AV47ddo_Contrato_CodigoTitleControlIdToReplace, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV95Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratoOcorrenciaNotificacao_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoOcorrenciaNotificacao_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContratoOcorrenciaNotificacao_Data = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV8InOutContratoOcorrenciaNotificacao_Data, "99/99/99"));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA7D2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV95Pgmname = "PromptContratoOcorrenciaNotificacao";
               context.Gx_err = 0;
               WS7D2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE7D2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117363444");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratoocorrencianotificacao.aspx") + "?" + UrlEncode("" +AV7InOutContratoOcorrenciaNotificacao_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV8InOutContratoOcorrenciaNotificacao_Data))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME1", StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME2", StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV27DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA3", context.localUtil.Format(AV29ContratoOcorrenciaNotificacao_Data3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3", context.localUtil.Format(AV30ContratoOcorrenciaNotificacao_Data_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME3", StringUtil.RTrim( AV31ContratoOcorrenciaNotificacao_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV26DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFContratoOcorrenciaNotificacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFContratoOcorrenciaNotificacao_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41TFContratoOcorrencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42TFContratoOcorrencia_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFContrato_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO", StringUtil.RTrim( AV49TFContrato_Numero));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO_SEL", StringUtil.RTrim( AV50TFContrato_Numero_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA", context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO", context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59TFContratoOcorrenciaNotificacao_Prazo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO", AV63TFContratoOcorrenciaNotificacao_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL", AV64TFContratoOcorrenciaNotificacao_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO", context.localUtil.Format(AV67TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO", context.localUtil.Format(AV68TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO", AV73TFContratoOcorrenciaNotificacao_Protocolo);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL", AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77TFContratoOcorrenciaNotificacao_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV78TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME", StringUtil.RTrim( AV81TFContratoOcorrenciaNotificacao_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL", StringUtil.RTrim( AV82TFContratoOcorrenciaNotificacao_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO", AV85TFContratoOcorrenciaNotificacao_Docto);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL", AV86TFContratoOcorrenciaNotificacao_Docto_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_104", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_104), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV90GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV91GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV88DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV88DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLEFILTERDATA", AV36ContratoOcorrenciaNotificacao_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLEFILTERDATA", AV36ContratoOcorrenciaNotificacao_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIA_CODIGOTITLEFILTERDATA", AV40ContratoOcorrencia_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIA_CODIGOTITLEFILTERDATA", AV40ContratoOcorrencia_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_CODIGOTITLEFILTERDATA", AV44Contrato_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_CODIGOTITLEFILTERDATA", AV44Contrato_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_NUMEROTITLEFILTERDATA", AV48Contrato_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_NUMEROTITLEFILTERDATA", AV48Contrato_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_DATATITLEFILTERDATA", AV52ContratoOcorrenciaNotificacao_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_DATATITLEFILTERDATA", AV52ContratoOcorrenciaNotificacao_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLEFILTERDATA", AV58ContratoOcorrenciaNotificacao_PrazoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLEFILTERDATA", AV58ContratoOcorrenciaNotificacao_PrazoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLEFILTERDATA", AV62ContratoOcorrenciaNotificacao_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLEFILTERDATA", AV62ContratoOcorrenciaNotificacao_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLEFILTERDATA", AV66ContratoOcorrenciaNotificacao_CumpridoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLEFILTERDATA", AV66ContratoOcorrenciaNotificacao_CumpridoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLEFILTERDATA", AV72ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLEFILTERDATA", AV72ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLEFILTERDATA", AV76ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLEFILTERDATA", AV76ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_NOMETITLEFILTERDATA", AV80ContratoOcorrenciaNotificacao_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_NOMETITLEFILTERDATA", AV80ContratoOcorrenciaNotificacao_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLEFILTERDATA", AV84ContratoOcorrenciaNotificacao_DoctoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLEFILTERDATA", AV84ContratoOcorrenciaNotificacao_DoctoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV95Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV33DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV32DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOOCORRENCIANOTIFICACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratoOcorrenciaNotificacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOOCORRENCIANOTIFICACAO_DATA", context.localUtil.DToC( AV8InOutContratoOcorrenciaNotificacao_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Caption", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Cls", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencia_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencia_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencia_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencia_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencia_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Caption", StringUtil.RTrim( Ddo_contrato_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contrato_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Cls", StringUtil.RTrim( Ddo_contrato_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contrato_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contrato_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contrato_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contrato_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contrato_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Caption", StringUtil.RTrim( Ddo_contrato_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contrato_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cls", StringUtil.RTrim( Ddo_contrato_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contrato_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalisttype", StringUtil.RTrim( Ddo_contrato_numero_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistproc", StringUtil.RTrim( Ddo_contrato_numero_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contrato_numero_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contrato_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contrato_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Loadingdata", StringUtil.RTrim( Ddo_contrato_numero_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Noresultsfound", StringUtil.RTrim( Ddo_contrato_numero_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencianotificacao_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencianotificacao_protocolo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_responsavel_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_responsavel_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_responsavel_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_responsavel_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_responsavel_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencianotificacao_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_docto_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_docto_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_docto_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_docto_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_docto_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencianotificacao_docto_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencia_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm7D2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratoOcorrenciaNotificacao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contrato Ocorrencia Notificacao" ;
      }

      protected void WB7D0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_7D2( true) ;
         }
         else
         {
            wb_table1_2_7D2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_7D2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(122, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV26DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(123, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFContratoOcorrenciaNotificacao_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37TFContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFContratoOcorrenciaNotificacao_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38TFContratoOcorrenciaNotificacao_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencia_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41TFContratoOcorrencia_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV41TFContratoOcorrencia_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencia_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencia_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencia_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42TFContratoOcorrencia_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV42TFContratoOcorrencia_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,127);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencia_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencia_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFContrato_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45TFContrato_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFContrato_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46TFContrato_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,129);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_Internalname, StringUtil.RTrim( AV49TFContrato_Numero), StringUtil.RTrim( context.localUtil.Format( AV49TFContrato_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_sel_Internalname, StringUtil.RTrim( AV50TFContrato_Numero_Sel), StringUtil.RTrim( context.localUtil.Format( AV50TFContrato_Numero_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencianotificacao_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_data_Internalname, context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Data, "99/99/99"), context.localUtil.Format( AV53TFContratoOcorrenciaNotificacao_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencianotificacao_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencianotificacao_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencianotificacao_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_data_to_Internalname, context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"), context.localUtil.Format( AV54TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,133);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencianotificacao_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencianotificacao_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratoocorrencianotificacao_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname, context.localUtil.Format(AV55DDO_ContratoOcorrenciaNotificacao_DataAuxDate, "99/99/99"), context.localUtil.Format( AV55DDO_ContratoOcorrenciaNotificacao_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencianotificacao_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname, context.localUtil.Format(AV56DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV56DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencianotificacao_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59TFContratoOcorrenciaNotificacao_Prazo), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV59TFContratoOcorrenciaNotificacao_Prazo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_prazo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_prazo_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_prazo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,138);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_prazo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_prazo_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_descricao_Internalname, AV63TFContratoOcorrenciaNotificacao_Descricao, StringUtil.RTrim( context.localUtil.Format( AV63TFContratoOcorrenciaNotificacao_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname, AV64TFContratoOcorrenciaNotificacao_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV64TFContratoOcorrenciaNotificacao_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,140);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencianotificacao_cumprido_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_cumprido_Internalname, context.localUtil.Format(AV67TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"), context.localUtil.Format( AV67TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_cumprido_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_cumprido_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencianotificacao_cumprido_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencianotificacao_cumprido_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname, context.localUtil.Format(AV68TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"), context.localUtil.Format( AV68TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,142);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_cumprido_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_cumprido_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencianotificacao_cumprido_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratoocorrencianotificacao_cumpridoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname, context.localUtil.Format(AV69DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate, "99/99/99"), context.localUtil.Format( AV69DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,144);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname, context.localUtil.Format(AV70DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo, "99/99/99"), context.localUtil.Format( AV70DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,145);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_protocolo_Internalname, AV73TFContratoOcorrenciaNotificacao_Protocolo, StringUtil.RTrim( context.localUtil.Format( AV73TFContratoOcorrenciaNotificacao_Protocolo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,146);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_protocolo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_protocolo_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname, AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel, StringUtil.RTrim( context.localUtil.Format( AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,147);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_protocolo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_responsavel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77TFContratoOcorrenciaNotificacao_Responsavel), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV77TFContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,148);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_responsavel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_responsavel_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV78TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV78TFContratoOcorrenciaNotificacao_Responsavel_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,149);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_responsavel_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_responsavel_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 150,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_nome_Internalname, StringUtil.RTrim( AV81TFContratoOcorrenciaNotificacao_Nome), StringUtil.RTrim( context.localUtil.Format( AV81TFContratoOcorrenciaNotificacao_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,150);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_nome_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratoocorrencianotificacao_nome_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_nome_sel_Internalname, StringUtil.RTrim( AV82TFContratoOcorrenciaNotificacao_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV82TFContratoOcorrenciaNotificacao_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,151);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_nome_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratoocorrencianotificacao_nome_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 152,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_docto_Internalname, AV85TFContratoOcorrenciaNotificacao_Docto, StringUtil.RTrim( context.localUtil.Format( AV85TFContratoOcorrenciaNotificacao_Docto, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,152);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_docto_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_docto_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_docto_sel_Internalname, AV86TFContratoOcorrenciaNotificacao_Docto_Sel, StringUtil.RTrim( context.localUtil.Format( AV86TFContratoOcorrenciaNotificacao_Docto_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,153);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_docto_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_docto_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_codigotitlecontrolidtoreplace_Internalname, AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,155);\"", 0, edtavDdo_contratoocorrencianotificacao_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 157,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencia_codigotitlecontrolidtoreplace_Internalname, AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,157);\"", 0, edtavDdo_contratoocorrencia_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname, AV47ddo_Contrato_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,159);\"", 0, edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 161,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, AV51ddo_Contrato_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,161);\"", 0, edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 163,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname, AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,163);\"", 0, edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 165,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname, AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,165);\"", 0, edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 167,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname, AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,167);\"", 0, edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 169,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname, AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,169);\"", 0, edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 171,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname, AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,171);\"", 0, edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 173,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Internalname, AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,173);\"", 0, edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 175,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname, AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,175);\"", 0, edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 177,'',false,'" + sGXsfl_104_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Internalname, AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,177);\"", 0, edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoOcorrenciaNotificacao.htm");
         }
         wbLoad = true;
      }

      protected void START7D2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contrato Ocorrencia Notificacao", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP7D0( ) ;
      }

      protected void WS7D2( )
      {
         START7D2( ) ;
         EVT7D2( ) ;
      }

      protected void EVT7D2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E117D2 */
                           E117D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E127D2 */
                           E127D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIA_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E137D2 */
                           E137D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E147D2 */
                           E147D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_NUMERO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E157D2 */
                           E157D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E167D2 */
                           E167D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E177D2 */
                           E177D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E187D2 */
                           E187D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E197D2 */
                           E197D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E207D2 */
                           E207D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E217D2 */
                           E217D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E227D2 */
                           E227D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E237D2 */
                           E237D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E247D2 */
                           E247D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E257D2 */
                           E257D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E267D2 */
                           E267D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E277D2 */
                           E277D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E287D2 */
                           E287D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E297D2 */
                           E297D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E307D2 */
                           E307D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E317D2 */
                           E317D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E327D2 */
                           E327D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E337D2 */
                           E337D2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_104_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_104_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_104_idx), 4, 0)), 4, "0");
                           SubsflControlProps_1042( ) ;
                           AV34Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Select)) ? AV94Select_GXI : context.convertURL( context.PathToRelativeUrl( AV34Select))));
                           A297ContratoOcorrenciaNotificacao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Codigo_Internalname), ",", "."));
                           A294ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrencia_Codigo_Internalname), ",", "."));
                           A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                           A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                           A298ContratoOcorrenciaNotificacao_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoOcorrenciaNotificacao_Data_Internalname), 0));
                           A299ContratoOcorrenciaNotificacao_Prazo = (short)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Prazo_Internalname), ",", "."));
                           A300ContratoOcorrenciaNotificacao_Descricao = StringUtil.Upper( cgiGet( edtContratoOcorrenciaNotificacao_Descricao_Internalname));
                           A301ContratoOcorrenciaNotificacao_Cumprido = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoOcorrenciaNotificacao_Cumprido_Internalname), 0));
                           n301ContratoOcorrenciaNotificacao_Cumprido = false;
                           A302ContratoOcorrenciaNotificacao_Protocolo = cgiGet( edtContratoOcorrenciaNotificacao_Protocolo_Internalname);
                           n302ContratoOcorrenciaNotificacao_Protocolo = false;
                           A303ContratoOcorrenciaNotificacao_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Responsavel_Internalname), ",", "."));
                           A304ContratoOcorrenciaNotificacao_Nome = StringUtil.Upper( cgiGet( edtContratoOcorrenciaNotificacao_Nome_Internalname));
                           n304ContratoOcorrenciaNotificacao_Nome = false;
                           A306ContratoOcorrenciaNotificacao_Docto = cgiGet( edtContratoOcorrenciaNotificacao_Docto_Internalname);
                           n306ContratoOcorrenciaNotificacao_Docto = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E347D2 */
                                 E347D2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E357D2 */
                                 E357D2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E367D2 */
                                 E367D2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencianotificacao_data1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA1"), 0) != AV17ContratoOcorrenciaNotificacao_Data1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencianotificacao_data_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1"), 0) != AV18ContratoOcorrenciaNotificacao_Data_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencianotificacao_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME1"), AV19ContratoOcorrenciaNotificacao_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencianotificacao_data2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA2"), 0) != AV23ContratoOcorrenciaNotificacao_Data2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencianotificacao_data_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2"), 0) != AV24ContratoOcorrenciaNotificacao_Data_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencianotificacao_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME2"), AV25ContratoOcorrenciaNotificacao_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencianotificacao_data3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA3"), 0) != AV29ContratoOcorrenciaNotificacao_Data3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencianotificacao_data_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3"), 0) != AV30ContratoOcorrenciaNotificacao_Data_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoocorrencianotificacao_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME3"), AV31ContratoOcorrenciaNotificacao_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO"), ",", ".") != Convert.ToDecimal( AV37TFContratoOcorrenciaNotificacao_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV38TFContratoOcorrenciaNotificacao_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencia_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_CODIGO"), ",", ".") != Convert.ToDecimal( AV41TFContratoOcorrencia_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencia_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV42TFContratoOcorrencia_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV45TFContrato_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV46TFContrato_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_numero Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV49TFContrato_Numero) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_numero_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV50TFContrato_Numero_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_data Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA"), 0) != AV53TFContratoOcorrenciaNotificacao_Data )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_data_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO"), 0) != AV54TFContratoOcorrenciaNotificacao_Data_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_prazo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO"), ",", ".") != Convert.ToDecimal( AV59TFContratoOcorrenciaNotificacao_Prazo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_prazo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO"), ",", ".") != Convert.ToDecimal( AV60TFContratoOcorrenciaNotificacao_Prazo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO"), AV63TFContratoOcorrenciaNotificacao_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL"), AV64TFContratoOcorrenciaNotificacao_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_cumprido Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO"), 0) != AV67TFContratoOcorrenciaNotificacao_Cumprido )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_cumprido_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO"), 0) != AV68TFContratoOcorrenciaNotificacao_Cumprido_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_protocolo Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO"), AV73TFContratoOcorrenciaNotificacao_Protocolo) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_protocolo_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL"), AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_responsavel Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL"), ",", ".") != Convert.ToDecimal( AV77TFContratoOcorrenciaNotificacao_Responsavel )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_responsavel_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO"), ",", ".") != Convert.ToDecimal( AV78TFContratoOcorrenciaNotificacao_Responsavel_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME"), AV81TFContratoOcorrenciaNotificacao_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL"), AV82TFContratoOcorrenciaNotificacao_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_docto Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO"), AV85TFContratoOcorrenciaNotificacao_Docto) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoocorrencianotificacao_docto_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL"), AV86TFContratoOcorrenciaNotificacao_Docto_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E377D2 */
                                       E377D2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE7D2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm7D2( ) ;
            }
         }
      }

      protected void PA7D2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOOCORRENCIANOTIFICACAO_DATA", "de emiss�o", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATOOCORRENCIANOTIFICACAO_NOME", "do respons�vek", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOOCORRENCIANOTIFICACAO_DATA", "de emiss�o", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATOOCORRENCIANOTIFICACAO_NOME", "do respons�vek", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOOCORRENCIANOTIFICACAO_DATA", "de emiss�o", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATOOCORRENCIANOTIFICACAO_NOME", "do respons�vek", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1042( ) ;
         while ( nGXsfl_104_idx <= nRC_GXsfl_104 )
         {
            sendrow_1042( ) ;
            nGXsfl_104_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_104_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_104_idx+1));
            sGXsfl_104_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_104_idx), 4, 0)), 4, "0");
            SubsflControlProps_1042( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       DateTime AV17ContratoOcorrenciaNotificacao_Data1 ,
                                       DateTime AV18ContratoOcorrenciaNotificacao_Data_To1 ,
                                       String AV19ContratoOcorrenciaNotificacao_Nome1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       DateTime AV23ContratoOcorrenciaNotificacao_Data2 ,
                                       DateTime AV24ContratoOcorrenciaNotificacao_Data_To2 ,
                                       String AV25ContratoOcorrenciaNotificacao_Nome2 ,
                                       String AV27DynamicFiltersSelector3 ,
                                       short AV28DynamicFiltersOperator3 ,
                                       DateTime AV29ContratoOcorrenciaNotificacao_Data3 ,
                                       DateTime AV30ContratoOcorrenciaNotificacao_Data_To3 ,
                                       String AV31ContratoOcorrenciaNotificacao_Nome3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV26DynamicFiltersEnabled3 ,
                                       int AV37TFContratoOcorrenciaNotificacao_Codigo ,
                                       int AV38TFContratoOcorrenciaNotificacao_Codigo_To ,
                                       int AV41TFContratoOcorrencia_Codigo ,
                                       int AV42TFContratoOcorrencia_Codigo_To ,
                                       int AV45TFContrato_Codigo ,
                                       int AV46TFContrato_Codigo_To ,
                                       String AV49TFContrato_Numero ,
                                       String AV50TFContrato_Numero_Sel ,
                                       DateTime AV53TFContratoOcorrenciaNotificacao_Data ,
                                       DateTime AV54TFContratoOcorrenciaNotificacao_Data_To ,
                                       short AV59TFContratoOcorrenciaNotificacao_Prazo ,
                                       short AV60TFContratoOcorrenciaNotificacao_Prazo_To ,
                                       String AV63TFContratoOcorrenciaNotificacao_Descricao ,
                                       String AV64TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                       DateTime AV67TFContratoOcorrenciaNotificacao_Cumprido ,
                                       DateTime AV68TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                       String AV73TFContratoOcorrenciaNotificacao_Protocolo ,
                                       String AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                       int AV77TFContratoOcorrenciaNotificacao_Responsavel ,
                                       int AV78TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                       String AV81TFContratoOcorrenciaNotificacao_Nome ,
                                       String AV82TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                       String AV85TFContratoOcorrenciaNotificacao_Docto ,
                                       String AV86TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                       String AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace ,
                                       String AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace ,
                                       String AV47ddo_Contrato_CodigoTitleControlIdToReplace ,
                                       String AV51ddo_Contrato_NumeroTitleControlIdToReplace ,
                                       String AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace ,
                                       String AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace ,
                                       String AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace ,
                                       String AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace ,
                                       String AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace ,
                                       String AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace ,
                                       String AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace ,
                                       String AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace ,
                                       String AV95Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV33DynamicFiltersIgnoreFirst ,
                                       bool AV32DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF7D2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_DATA", GetSecureSignedToken( "", A298ContratoOcorrenciaNotificacao_Data));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_DATA", context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_PRAZO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO", A300ContratoOcorrenciaNotificacao_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO", GetSecureSignedToken( "", A301ContratoOcorrenciaNotificacao_Cumprido));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO", context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A302ContratoOcorrenciaNotificacao_Protocolo, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO", A302ContratoOcorrenciaNotificacao_Protocolo);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF7D2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV95Pgmname = "PromptContratoOcorrenciaNotificacao";
         context.Gx_err = 0;
      }

      protected void RF7D2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 104;
         /* Execute user event: E357D2 */
         E357D2 ();
         nGXsfl_104_idx = 1;
         sGXsfl_104_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_104_idx), 4, 0)), 4, "0");
         SubsflControlProps_1042( ) ;
         nGXsfl_104_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1042( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV17ContratoOcorrenciaNotificacao_Data1 ,
                                                 AV18ContratoOcorrenciaNotificacao_Data_To1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV19ContratoOcorrenciaNotificacao_Nome1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV23ContratoOcorrenciaNotificacao_Data2 ,
                                                 AV24ContratoOcorrenciaNotificacao_Data_To2 ,
                                                 AV22DynamicFiltersOperator2 ,
                                                 AV25ContratoOcorrenciaNotificacao_Nome2 ,
                                                 AV26DynamicFiltersEnabled3 ,
                                                 AV27DynamicFiltersSelector3 ,
                                                 AV29ContratoOcorrenciaNotificacao_Data3 ,
                                                 AV30ContratoOcorrenciaNotificacao_Data_To3 ,
                                                 AV28DynamicFiltersOperator3 ,
                                                 AV31ContratoOcorrenciaNotificacao_Nome3 ,
                                                 AV37TFContratoOcorrenciaNotificacao_Codigo ,
                                                 AV38TFContratoOcorrenciaNotificacao_Codigo_To ,
                                                 AV41TFContratoOcorrencia_Codigo ,
                                                 AV42TFContratoOcorrencia_Codigo_To ,
                                                 AV45TFContrato_Codigo ,
                                                 AV46TFContrato_Codigo_To ,
                                                 AV50TFContrato_Numero_Sel ,
                                                 AV49TFContrato_Numero ,
                                                 AV53TFContratoOcorrenciaNotificacao_Data ,
                                                 AV54TFContratoOcorrenciaNotificacao_Data_To ,
                                                 AV59TFContratoOcorrenciaNotificacao_Prazo ,
                                                 AV60TFContratoOcorrenciaNotificacao_Prazo_To ,
                                                 AV64TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                                 AV63TFContratoOcorrenciaNotificacao_Descricao ,
                                                 AV67TFContratoOcorrenciaNotificacao_Cumprido ,
                                                 AV68TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                                 AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                                 AV73TFContratoOcorrenciaNotificacao_Protocolo ,
                                                 AV77TFContratoOcorrenciaNotificacao_Responsavel ,
                                                 AV78TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                                 AV82TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                                 AV81TFContratoOcorrenciaNotificacao_Nome ,
                                                 AV86TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                                 AV85TFContratoOcorrenciaNotificacao_Docto ,
                                                 A298ContratoOcorrenciaNotificacao_Data ,
                                                 A304ContratoOcorrenciaNotificacao_Nome ,
                                                 A297ContratoOcorrenciaNotificacao_Codigo ,
                                                 A294ContratoOcorrencia_Codigo ,
                                                 A74Contrato_Codigo ,
                                                 A77Contrato_Numero ,
                                                 A299ContratoOcorrenciaNotificacao_Prazo ,
                                                 A300ContratoOcorrenciaNotificacao_Descricao ,
                                                 A301ContratoOcorrenciaNotificacao_Cumprido ,
                                                 A302ContratoOcorrenciaNotificacao_Protocolo ,
                                                 A303ContratoOcorrenciaNotificacao_Responsavel ,
                                                 A306ContratoOcorrenciaNotificacao_Docto ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV19ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoOcorrenciaNotificacao_Nome1", AV19ContratoOcorrenciaNotificacao_Nome1);
            lV19ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoOcorrenciaNotificacao_Nome1", AV19ContratoOcorrenciaNotificacao_Nome1);
            lV25ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoOcorrenciaNotificacao_Nome2", AV25ContratoOcorrenciaNotificacao_Nome2);
            lV25ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoOcorrenciaNotificacao_Nome2", AV25ContratoOcorrenciaNotificacao_Nome2);
            lV31ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV31ContratoOcorrenciaNotificacao_Nome3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrenciaNotificacao_Nome3", AV31ContratoOcorrenciaNotificacao_Nome3);
            lV31ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV31ContratoOcorrenciaNotificacao_Nome3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrenciaNotificacao_Nome3", AV31ContratoOcorrenciaNotificacao_Nome3);
            lV49TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV49TFContrato_Numero), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContrato_Numero", AV49TFContrato_Numero);
            lV63TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV63TFContratoOcorrenciaNotificacao_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoOcorrenciaNotificacao_Descricao", AV63TFContratoOcorrenciaNotificacao_Descricao);
            lV73TFContratoOcorrenciaNotificacao_Protocolo = StringUtil.Concat( StringUtil.RTrim( AV73TFContratoOcorrenciaNotificacao_Protocolo), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratoOcorrenciaNotificacao_Protocolo", AV73TFContratoOcorrenciaNotificacao_Protocolo);
            lV81TFContratoOcorrenciaNotificacao_Nome = StringUtil.PadR( StringUtil.RTrim( AV81TFContratoOcorrenciaNotificacao_Nome), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoOcorrenciaNotificacao_Nome", AV81TFContratoOcorrenciaNotificacao_Nome);
            lV85TFContratoOcorrenciaNotificacao_Docto = StringUtil.Concat( StringUtil.RTrim( AV85TFContratoOcorrenciaNotificacao_Docto), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoOcorrenciaNotificacao_Docto", AV85TFContratoOcorrenciaNotificacao_Docto);
            /* Using cursor H007D2 */
            pr_default.execute(0, new Object[] {AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, lV19ContratoOcorrenciaNotificacao_Nome1, lV19ContratoOcorrenciaNotificacao_Nome1, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, lV25ContratoOcorrenciaNotificacao_Nome2, lV25ContratoOcorrenciaNotificacao_Nome2, AV29ContratoOcorrenciaNotificacao_Data3, AV30ContratoOcorrenciaNotificacao_Data_To3, lV31ContratoOcorrenciaNotificacao_Nome3, lV31ContratoOcorrenciaNotificacao_Nome3, AV37TFContratoOcorrenciaNotificacao_Codigo, AV38TFContratoOcorrenciaNotificacao_Codigo_To, AV41TFContratoOcorrencia_Codigo, AV42TFContratoOcorrencia_Codigo_To, AV45TFContrato_Codigo, AV46TFContrato_Codigo_To, lV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoOcorrenciaNotificacao_Data, AV54TFContratoOcorrenciaNotificacao_Data_To, AV59TFContratoOcorrenciaNotificacao_Prazo, AV60TFContratoOcorrenciaNotificacao_Prazo_To, lV63TFContratoOcorrenciaNotificacao_Descricao, AV64TFContratoOcorrenciaNotificacao_Descricao_Sel, AV67TFContratoOcorrenciaNotificacao_Cumprido, AV68TFContratoOcorrenciaNotificacao_Cumprido_To, lV73TFContratoOcorrenciaNotificacao_Protocolo, AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV77TFContratoOcorrenciaNotificacao_Responsavel, AV78TFContratoOcorrenciaNotificacao_Responsavel_To, lV81TFContratoOcorrenciaNotificacao_Nome, AV82TFContratoOcorrenciaNotificacao_Nome_Sel, lV85TFContratoOcorrenciaNotificacao_Docto, AV86TFContratoOcorrenciaNotificacao_Docto_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_104_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A306ContratoOcorrenciaNotificacao_Docto = H007D2_A306ContratoOcorrenciaNotificacao_Docto[0];
               n306ContratoOcorrenciaNotificacao_Docto = H007D2_n306ContratoOcorrenciaNotificacao_Docto[0];
               A304ContratoOcorrenciaNotificacao_Nome = H007D2_A304ContratoOcorrenciaNotificacao_Nome[0];
               n304ContratoOcorrenciaNotificacao_Nome = H007D2_n304ContratoOcorrenciaNotificacao_Nome[0];
               A303ContratoOcorrenciaNotificacao_Responsavel = H007D2_A303ContratoOcorrenciaNotificacao_Responsavel[0];
               A302ContratoOcorrenciaNotificacao_Protocolo = H007D2_A302ContratoOcorrenciaNotificacao_Protocolo[0];
               n302ContratoOcorrenciaNotificacao_Protocolo = H007D2_n302ContratoOcorrenciaNotificacao_Protocolo[0];
               A301ContratoOcorrenciaNotificacao_Cumprido = H007D2_A301ContratoOcorrenciaNotificacao_Cumprido[0];
               n301ContratoOcorrenciaNotificacao_Cumprido = H007D2_n301ContratoOcorrenciaNotificacao_Cumprido[0];
               A300ContratoOcorrenciaNotificacao_Descricao = H007D2_A300ContratoOcorrenciaNotificacao_Descricao[0];
               A299ContratoOcorrenciaNotificacao_Prazo = H007D2_A299ContratoOcorrenciaNotificacao_Prazo[0];
               A298ContratoOcorrenciaNotificacao_Data = H007D2_A298ContratoOcorrenciaNotificacao_Data[0];
               A77Contrato_Numero = H007D2_A77Contrato_Numero[0];
               A74Contrato_Codigo = H007D2_A74Contrato_Codigo[0];
               A294ContratoOcorrencia_Codigo = H007D2_A294ContratoOcorrencia_Codigo[0];
               A297ContratoOcorrenciaNotificacao_Codigo = H007D2_A297ContratoOcorrenciaNotificacao_Codigo[0];
               A306ContratoOcorrenciaNotificacao_Docto = H007D2_A306ContratoOcorrenciaNotificacao_Docto[0];
               n306ContratoOcorrenciaNotificacao_Docto = H007D2_n306ContratoOcorrenciaNotificacao_Docto[0];
               A304ContratoOcorrenciaNotificacao_Nome = H007D2_A304ContratoOcorrenciaNotificacao_Nome[0];
               n304ContratoOcorrenciaNotificacao_Nome = H007D2_n304ContratoOcorrenciaNotificacao_Nome[0];
               A74Contrato_Codigo = H007D2_A74Contrato_Codigo[0];
               A77Contrato_Numero = H007D2_A77Contrato_Numero[0];
               /* Execute user event: E367D2 */
               E367D2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 104;
            WB7D0( ) ;
         }
         nGXsfl_104_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV17ContratoOcorrenciaNotificacao_Data1 ,
                                              AV18ContratoOcorrenciaNotificacao_Data_To1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV19ContratoOcorrenciaNotificacao_Nome1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV23ContratoOcorrenciaNotificacao_Data2 ,
                                              AV24ContratoOcorrenciaNotificacao_Data_To2 ,
                                              AV22DynamicFiltersOperator2 ,
                                              AV25ContratoOcorrenciaNotificacao_Nome2 ,
                                              AV26DynamicFiltersEnabled3 ,
                                              AV27DynamicFiltersSelector3 ,
                                              AV29ContratoOcorrenciaNotificacao_Data3 ,
                                              AV30ContratoOcorrenciaNotificacao_Data_To3 ,
                                              AV28DynamicFiltersOperator3 ,
                                              AV31ContratoOcorrenciaNotificacao_Nome3 ,
                                              AV37TFContratoOcorrenciaNotificacao_Codigo ,
                                              AV38TFContratoOcorrenciaNotificacao_Codigo_To ,
                                              AV41TFContratoOcorrencia_Codigo ,
                                              AV42TFContratoOcorrencia_Codigo_To ,
                                              AV45TFContrato_Codigo ,
                                              AV46TFContrato_Codigo_To ,
                                              AV50TFContrato_Numero_Sel ,
                                              AV49TFContrato_Numero ,
                                              AV53TFContratoOcorrenciaNotificacao_Data ,
                                              AV54TFContratoOcorrenciaNotificacao_Data_To ,
                                              AV59TFContratoOcorrenciaNotificacao_Prazo ,
                                              AV60TFContratoOcorrenciaNotificacao_Prazo_To ,
                                              AV64TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                              AV63TFContratoOcorrenciaNotificacao_Descricao ,
                                              AV67TFContratoOcorrenciaNotificacao_Cumprido ,
                                              AV68TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                              AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                              AV73TFContratoOcorrenciaNotificacao_Protocolo ,
                                              AV77TFContratoOcorrenciaNotificacao_Responsavel ,
                                              AV78TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                              AV82TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                              AV81TFContratoOcorrenciaNotificacao_Nome ,
                                              AV86TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                              AV85TFContratoOcorrenciaNotificacao_Docto ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A297ContratoOcorrenciaNotificacao_Codigo ,
                                              A294ContratoOcorrencia_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo ,
                                              A303ContratoOcorrenciaNotificacao_Responsavel ,
                                              A306ContratoOcorrenciaNotificacao_Docto ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV19ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoOcorrenciaNotificacao_Nome1", AV19ContratoOcorrenciaNotificacao_Nome1);
         lV19ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoOcorrenciaNotificacao_Nome1", AV19ContratoOcorrenciaNotificacao_Nome1);
         lV25ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoOcorrenciaNotificacao_Nome2", AV25ContratoOcorrenciaNotificacao_Nome2);
         lV25ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoOcorrenciaNotificacao_Nome2", AV25ContratoOcorrenciaNotificacao_Nome2);
         lV31ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV31ContratoOcorrenciaNotificacao_Nome3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrenciaNotificacao_Nome3", AV31ContratoOcorrenciaNotificacao_Nome3);
         lV31ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV31ContratoOcorrenciaNotificacao_Nome3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrenciaNotificacao_Nome3", AV31ContratoOcorrenciaNotificacao_Nome3);
         lV49TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV49TFContrato_Numero), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContrato_Numero", AV49TFContrato_Numero);
         lV63TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV63TFContratoOcorrenciaNotificacao_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoOcorrenciaNotificacao_Descricao", AV63TFContratoOcorrenciaNotificacao_Descricao);
         lV73TFContratoOcorrenciaNotificacao_Protocolo = StringUtil.Concat( StringUtil.RTrim( AV73TFContratoOcorrenciaNotificacao_Protocolo), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratoOcorrenciaNotificacao_Protocolo", AV73TFContratoOcorrenciaNotificacao_Protocolo);
         lV81TFContratoOcorrenciaNotificacao_Nome = StringUtil.PadR( StringUtil.RTrim( AV81TFContratoOcorrenciaNotificacao_Nome), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoOcorrenciaNotificacao_Nome", AV81TFContratoOcorrenciaNotificacao_Nome);
         lV85TFContratoOcorrenciaNotificacao_Docto = StringUtil.Concat( StringUtil.RTrim( AV85TFContratoOcorrenciaNotificacao_Docto), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoOcorrenciaNotificacao_Docto", AV85TFContratoOcorrenciaNotificacao_Docto);
         /* Using cursor H007D3 */
         pr_default.execute(1, new Object[] {AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, lV19ContratoOcorrenciaNotificacao_Nome1, lV19ContratoOcorrenciaNotificacao_Nome1, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, lV25ContratoOcorrenciaNotificacao_Nome2, lV25ContratoOcorrenciaNotificacao_Nome2, AV29ContratoOcorrenciaNotificacao_Data3, AV30ContratoOcorrenciaNotificacao_Data_To3, lV31ContratoOcorrenciaNotificacao_Nome3, lV31ContratoOcorrenciaNotificacao_Nome3, AV37TFContratoOcorrenciaNotificacao_Codigo, AV38TFContratoOcorrenciaNotificacao_Codigo_To, AV41TFContratoOcorrencia_Codigo, AV42TFContratoOcorrencia_Codigo_To, AV45TFContrato_Codigo, AV46TFContrato_Codigo_To, lV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoOcorrenciaNotificacao_Data, AV54TFContratoOcorrenciaNotificacao_Data_To, AV59TFContratoOcorrenciaNotificacao_Prazo, AV60TFContratoOcorrenciaNotificacao_Prazo_To, lV63TFContratoOcorrenciaNotificacao_Descricao, AV64TFContratoOcorrenciaNotificacao_Descricao_Sel, AV67TFContratoOcorrenciaNotificacao_Cumprido, AV68TFContratoOcorrenciaNotificacao_Cumprido_To, lV73TFContratoOcorrenciaNotificacao_Protocolo, AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV77TFContratoOcorrenciaNotificacao_Responsavel, AV78TFContratoOcorrenciaNotificacao_Responsavel_To, lV81TFContratoOcorrenciaNotificacao_Nome, AV82TFContratoOcorrenciaNotificacao_Nome_Sel, lV85TFContratoOcorrenciaNotificacao_Docto, AV86TFContratoOcorrenciaNotificacao_Docto_Sel});
         GRID_nRecordCount = H007D3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContratoOcorrenciaNotificacao_Data3, AV30ContratoOcorrenciaNotificacao_Data_To3, AV31ContratoOcorrenciaNotificacao_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFContratoOcorrenciaNotificacao_Codigo, AV38TFContratoOcorrenciaNotificacao_Codigo_To, AV41TFContratoOcorrencia_Codigo, AV42TFContratoOcorrencia_Codigo_To, AV45TFContrato_Codigo, AV46TFContrato_Codigo_To, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoOcorrenciaNotificacao_Data, AV54TFContratoOcorrenciaNotificacao_Data_To, AV59TFContratoOcorrenciaNotificacao_Prazo, AV60TFContratoOcorrenciaNotificacao_Prazo_To, AV63TFContratoOcorrenciaNotificacao_Descricao, AV64TFContratoOcorrenciaNotificacao_Descricao_Sel, AV67TFContratoOcorrenciaNotificacao_Cumprido, AV68TFContratoOcorrenciaNotificacao_Cumprido_To, AV73TFContratoOcorrenciaNotificacao_Protocolo, AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV77TFContratoOcorrenciaNotificacao_Responsavel, AV78TFContratoOcorrenciaNotificacao_Responsavel_To, AV81TFContratoOcorrenciaNotificacao_Nome, AV82TFContratoOcorrenciaNotificacao_Nome_Sel, AV85TFContratoOcorrenciaNotificacao_Docto, AV86TFContratoOcorrenciaNotificacao_Docto_Sel, AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace, AV47ddo_Contrato_CodigoTitleControlIdToReplace, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV95Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContratoOcorrenciaNotificacao_Data3, AV30ContratoOcorrenciaNotificacao_Data_To3, AV31ContratoOcorrenciaNotificacao_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFContratoOcorrenciaNotificacao_Codigo, AV38TFContratoOcorrenciaNotificacao_Codigo_To, AV41TFContratoOcorrencia_Codigo, AV42TFContratoOcorrencia_Codigo_To, AV45TFContrato_Codigo, AV46TFContrato_Codigo_To, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoOcorrenciaNotificacao_Data, AV54TFContratoOcorrenciaNotificacao_Data_To, AV59TFContratoOcorrenciaNotificacao_Prazo, AV60TFContratoOcorrenciaNotificacao_Prazo_To, AV63TFContratoOcorrenciaNotificacao_Descricao, AV64TFContratoOcorrenciaNotificacao_Descricao_Sel, AV67TFContratoOcorrenciaNotificacao_Cumprido, AV68TFContratoOcorrenciaNotificacao_Cumprido_To, AV73TFContratoOcorrenciaNotificacao_Protocolo, AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV77TFContratoOcorrenciaNotificacao_Responsavel, AV78TFContratoOcorrenciaNotificacao_Responsavel_To, AV81TFContratoOcorrenciaNotificacao_Nome, AV82TFContratoOcorrenciaNotificacao_Nome_Sel, AV85TFContratoOcorrenciaNotificacao_Docto, AV86TFContratoOcorrenciaNotificacao_Docto_Sel, AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace, AV47ddo_Contrato_CodigoTitleControlIdToReplace, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV95Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContratoOcorrenciaNotificacao_Data3, AV30ContratoOcorrenciaNotificacao_Data_To3, AV31ContratoOcorrenciaNotificacao_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFContratoOcorrenciaNotificacao_Codigo, AV38TFContratoOcorrenciaNotificacao_Codigo_To, AV41TFContratoOcorrencia_Codigo, AV42TFContratoOcorrencia_Codigo_To, AV45TFContrato_Codigo, AV46TFContrato_Codigo_To, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoOcorrenciaNotificacao_Data, AV54TFContratoOcorrenciaNotificacao_Data_To, AV59TFContratoOcorrenciaNotificacao_Prazo, AV60TFContratoOcorrenciaNotificacao_Prazo_To, AV63TFContratoOcorrenciaNotificacao_Descricao, AV64TFContratoOcorrenciaNotificacao_Descricao_Sel, AV67TFContratoOcorrenciaNotificacao_Cumprido, AV68TFContratoOcorrenciaNotificacao_Cumprido_To, AV73TFContratoOcorrenciaNotificacao_Protocolo, AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV77TFContratoOcorrenciaNotificacao_Responsavel, AV78TFContratoOcorrenciaNotificacao_Responsavel_To, AV81TFContratoOcorrenciaNotificacao_Nome, AV82TFContratoOcorrenciaNotificacao_Nome_Sel, AV85TFContratoOcorrenciaNotificacao_Docto, AV86TFContratoOcorrenciaNotificacao_Docto_Sel, AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace, AV47ddo_Contrato_CodigoTitleControlIdToReplace, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV95Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContratoOcorrenciaNotificacao_Data3, AV30ContratoOcorrenciaNotificacao_Data_To3, AV31ContratoOcorrenciaNotificacao_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFContratoOcorrenciaNotificacao_Codigo, AV38TFContratoOcorrenciaNotificacao_Codigo_To, AV41TFContratoOcorrencia_Codigo, AV42TFContratoOcorrencia_Codigo_To, AV45TFContrato_Codigo, AV46TFContrato_Codigo_To, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoOcorrenciaNotificacao_Data, AV54TFContratoOcorrenciaNotificacao_Data_To, AV59TFContratoOcorrenciaNotificacao_Prazo, AV60TFContratoOcorrenciaNotificacao_Prazo_To, AV63TFContratoOcorrenciaNotificacao_Descricao, AV64TFContratoOcorrenciaNotificacao_Descricao_Sel, AV67TFContratoOcorrenciaNotificacao_Cumprido, AV68TFContratoOcorrenciaNotificacao_Cumprido_To, AV73TFContratoOcorrenciaNotificacao_Protocolo, AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV77TFContratoOcorrenciaNotificacao_Responsavel, AV78TFContratoOcorrenciaNotificacao_Responsavel_To, AV81TFContratoOcorrenciaNotificacao_Nome, AV82TFContratoOcorrenciaNotificacao_Nome_Sel, AV85TFContratoOcorrenciaNotificacao_Docto, AV86TFContratoOcorrenciaNotificacao_Docto_Sel, AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace, AV47ddo_Contrato_CodigoTitleControlIdToReplace, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV95Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContratoOcorrenciaNotificacao_Data3, AV30ContratoOcorrenciaNotificacao_Data_To3, AV31ContratoOcorrenciaNotificacao_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFContratoOcorrenciaNotificacao_Codigo, AV38TFContratoOcorrenciaNotificacao_Codigo_To, AV41TFContratoOcorrencia_Codigo, AV42TFContratoOcorrencia_Codigo_To, AV45TFContrato_Codigo, AV46TFContrato_Codigo_To, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoOcorrenciaNotificacao_Data, AV54TFContratoOcorrenciaNotificacao_Data_To, AV59TFContratoOcorrenciaNotificacao_Prazo, AV60TFContratoOcorrenciaNotificacao_Prazo_To, AV63TFContratoOcorrenciaNotificacao_Descricao, AV64TFContratoOcorrenciaNotificacao_Descricao_Sel, AV67TFContratoOcorrenciaNotificacao_Cumprido, AV68TFContratoOcorrenciaNotificacao_Cumprido_To, AV73TFContratoOcorrenciaNotificacao_Protocolo, AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV77TFContratoOcorrenciaNotificacao_Responsavel, AV78TFContratoOcorrenciaNotificacao_Responsavel_To, AV81TFContratoOcorrenciaNotificacao_Nome, AV82TFContratoOcorrenciaNotificacao_Nome_Sel, AV85TFContratoOcorrenciaNotificacao_Docto, AV86TFContratoOcorrenciaNotificacao_Docto_Sel, AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace, AV47ddo_Contrato_CodigoTitleControlIdToReplace, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV95Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP7D0( )
      {
         /* Before Start, stand alone formulas. */
         AV95Pgmname = "PromptContratoOcorrenciaNotificacao";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E347D2 */
         E347D2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV88DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLEFILTERDATA"), AV36ContratoOcorrenciaNotificacao_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIA_CODIGOTITLEFILTERDATA"), AV40ContratoOcorrencia_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_CODIGOTITLEFILTERDATA"), AV44Contrato_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_NUMEROTITLEFILTERDATA"), AV48Contrato_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_DATATITLEFILTERDATA"), AV52ContratoOcorrenciaNotificacao_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLEFILTERDATA"), AV58ContratoOcorrenciaNotificacao_PrazoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLEFILTERDATA"), AV62ContratoOcorrenciaNotificacao_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLEFILTERDATA"), AV66ContratoOcorrenciaNotificacao_CumpridoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLEFILTERDATA"), AV72ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLEFILTERDATA"), AV76ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_NOMETITLEFILTERDATA"), AV80ContratoOcorrenciaNotificacao_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLEFILTERDATA"), AV84ContratoOcorrenciaNotificacao_DoctoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data1"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA1");
               GX_FocusControl = edtavContratoocorrencianotificacao_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContratoOcorrenciaNotificacao_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
            }
            else
            {
               AV17ContratoOcorrenciaNotificacao_Data1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data_To1"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1");
               GX_FocusControl = edtavContratoocorrencianotificacao_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18ContratoOcorrenciaNotificacao_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
            }
            else
            {
               AV18ContratoOcorrenciaNotificacao_Data_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
            }
            AV19ContratoOcorrenciaNotificacao_Nome1 = StringUtil.Upper( cgiGet( edtavContratoocorrencianotificacao_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoOcorrenciaNotificacao_Nome1", AV19ContratoOcorrenciaNotificacao_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data2"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA2");
               GX_FocusControl = edtavContratoocorrencianotificacao_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23ContratoOcorrenciaNotificacao_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
            }
            else
            {
               AV23ContratoOcorrenciaNotificacao_Data2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data_To2"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2");
               GX_FocusControl = edtavContratoocorrencianotificacao_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24ContratoOcorrenciaNotificacao_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
            }
            else
            {
               AV24ContratoOcorrenciaNotificacao_Data_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
            }
            AV25ContratoOcorrenciaNotificacao_Nome2 = StringUtil.Upper( cgiGet( edtavContratoocorrencianotificacao_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoOcorrenciaNotificacao_Nome2", AV25ContratoOcorrenciaNotificacao_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV27DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data3"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA3");
               GX_FocusControl = edtavContratoocorrencianotificacao_data3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29ContratoOcorrenciaNotificacao_Data3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContratoOcorrenciaNotificacao_Data3", context.localUtil.Format(AV29ContratoOcorrenciaNotificacao_Data3, "99/99/99"));
            }
            else
            {
               AV29ContratoOcorrenciaNotificacao_Data3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContratoOcorrenciaNotificacao_Data3", context.localUtil.Format(AV29ContratoOcorrenciaNotificacao_Data3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data_To3"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3");
               GX_FocusControl = edtavContratoocorrencianotificacao_data_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30ContratoOcorrenciaNotificacao_Data_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratoOcorrenciaNotificacao_Data_To3", context.localUtil.Format(AV30ContratoOcorrenciaNotificacao_Data_To3, "99/99/99"));
            }
            else
            {
               AV30ContratoOcorrenciaNotificacao_Data_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratoOcorrenciaNotificacao_Data_To3", context.localUtil.Format(AV30ContratoOcorrenciaNotificacao_Data_To3, "99/99/99"));
            }
            AV31ContratoOcorrenciaNotificacao_Nome3 = StringUtil.Upper( cgiGet( edtavContratoocorrencianotificacao_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrenciaNotificacao_Nome3", AV31ContratoOcorrenciaNotificacao_Nome3);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV26DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37TFContratoOcorrenciaNotificacao_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoOcorrenciaNotificacao_Codigo), 6, 0)));
            }
            else
            {
               AV37TFContratoOcorrenciaNotificacao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoOcorrenciaNotificacao_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFContratoOcorrenciaNotificacao_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoOcorrenciaNotificacao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContratoOcorrenciaNotificacao_Codigo_To), 6, 0)));
            }
            else
            {
               AV38TFContratoOcorrenciaNotificacao_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoOcorrenciaNotificacao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContratoOcorrenciaNotificacao_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencia_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencia_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIA_CODIGO");
               GX_FocusControl = edtavTfcontratoocorrencia_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41TFContratoOcorrencia_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContratoOcorrencia_Codigo), 6, 0)));
            }
            else
            {
               AV41TFContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencia_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContratoOcorrencia_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencia_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencia_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIA_CODIGO_TO");
               GX_FocusControl = edtavTfcontratoocorrencia_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42TFContratoOcorrencia_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoOcorrencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFContratoOcorrencia_Codigo_To), 6, 0)));
            }
            else
            {
               AV42TFContratoOcorrencia_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencia_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoOcorrencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFContratoOcorrencia_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_CODIGO");
               GX_FocusControl = edtavTfcontrato_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFContrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContrato_Codigo), 6, 0)));
            }
            else
            {
               AV45TFContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContrato_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_CODIGO_TO");
               GX_FocusControl = edtavTfcontrato_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFContrato_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContrato_Codigo_To), 6, 0)));
            }
            else
            {
               AV46TFContrato_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContrato_Codigo_To), 6, 0)));
            }
            AV49TFContrato_Numero = cgiGet( edtavTfcontrato_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContrato_Numero", AV49TFContrato_Numero);
            AV50TFContrato_Numero_Sel = cgiGet( edtavTfcontrato_numero_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContrato_Numero_Sel", AV50TFContrato_Numero_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencianotificacao_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia Notificacao_Data"}), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_DATA");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53TFContratoOcorrenciaNotificacao_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
            }
            else
            {
               AV53TFContratoOcorrenciaNotificacao_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencianotificacao_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencianotificacao_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia Notificacao_Data_To"}), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFContratoOcorrenciaNotificacao_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
            }
            else
            {
               AV54TFContratoOcorrenciaNotificacao_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencianotificacao_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia Notificacao_Data Aux Date"}), 1, "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55DDO_ContratoOcorrenciaNotificacao_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DDO_ContratoOcorrenciaNotificacao_DataAuxDate", context.localUtil.Format(AV55DDO_ContratoOcorrenciaNotificacao_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV55DDO_ContratoOcorrenciaNotificacao_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DDO_ContratoOcorrenciaNotificacao_DataAuxDate", context.localUtil.Format(AV55DDO_ContratoOcorrenciaNotificacao_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia Notificacao_Data Aux Date To"}), 1, "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo", context.localUtil.Format(AV56DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV56DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo", context.localUtil.Format(AV56DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_prazo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFContratoOcorrenciaNotificacao_Prazo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
            }
            else
            {
               AV59TFContratoOcorrenciaNotificacao_Prazo = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_prazo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60TFContratoOcorrenciaNotificacao_Prazo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
            }
            else
            {
               AV60TFContratoOcorrenciaNotificacao_Prazo_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
            }
            AV63TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencianotificacao_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoOcorrenciaNotificacao_Descricao", AV63TFContratoOcorrenciaNotificacao_Descricao);
            AV64TFContratoOcorrenciaNotificacao_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoOcorrenciaNotificacao_Descricao_Sel", AV64TFContratoOcorrenciaNotificacao_Descricao_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencianotificacao_cumprido_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia Notificacao_Cumprido"}), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_cumprido_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67TFContratoOcorrenciaNotificacao_Cumprido = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV67TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
            }
            else
            {
               AV67TFContratoOcorrenciaNotificacao_Cumprido = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencianotificacao_cumprido_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV67TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia Notificacao_Cumprido_To"}), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68TFContratoOcorrenciaNotificacao_Cumprido_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV68TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
            }
            else
            {
               AV68TFContratoOcorrenciaNotificacao_Cumprido_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV68TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia Notificacao_Cumprido Aux Date"}), 1, "vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATE");
               GX_FocusControl = edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate", context.localUtil.Format(AV69DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate, "99/99/99"));
            }
            else
            {
               AV69DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate", context.localUtil.Format(AV69DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia Notificacao_Cumprido Aux Date To"}), 1, "vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATETO");
               GX_FocusControl = edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo", context.localUtil.Format(AV70DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV70DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo", context.localUtil.Format(AV70DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo, "99/99/99"));
            }
            AV73TFContratoOcorrenciaNotificacao_Protocolo = cgiGet( edtavTfcontratoocorrencianotificacao_protocolo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratoOcorrenciaNotificacao_Protocolo", AV73TFContratoOcorrenciaNotificacao_Protocolo);
            AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel = cgiGet( edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel", AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_responsavel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_responsavel_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_responsavel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV77TFContratoOcorrenciaNotificacao_Responsavel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
            }
            else
            {
               AV77TFContratoOcorrenciaNotificacao_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_responsavel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV78TFContratoOcorrenciaNotificacao_Responsavel_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContratoOcorrenciaNotificacao_Responsavel_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0)));
            }
            else
            {
               AV78TFContratoOcorrenciaNotificacao_Responsavel_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContratoOcorrenciaNotificacao_Responsavel_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0)));
            }
            AV81TFContratoOcorrenciaNotificacao_Nome = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencianotificacao_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoOcorrenciaNotificacao_Nome", AV81TFContratoOcorrenciaNotificacao_Nome);
            AV82TFContratoOcorrenciaNotificacao_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencianotificacao_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratoOcorrenciaNotificacao_Nome_Sel", AV82TFContratoOcorrenciaNotificacao_Nome_Sel);
            AV85TFContratoOcorrenciaNotificacao_Docto = cgiGet( edtavTfcontratoocorrencianotificacao_docto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoOcorrenciaNotificacao_Docto", AV85TFContratoOcorrenciaNotificacao_Docto);
            AV86TFContratoOcorrenciaNotificacao_Docto_Sel = cgiGet( edtavTfcontratoocorrencianotificacao_docto_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFContratoOcorrenciaNotificacao_Docto_Sel", AV86TFContratoOcorrenciaNotificacao_Docto_Sel);
            AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace", AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace);
            AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencia_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace", AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace);
            AV47ddo_Contrato_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Contrato_CodigoTitleControlIdToReplace", AV47ddo_Contrato_CodigoTitleControlIdToReplace);
            AV51ddo_Contrato_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_Contrato_NumeroTitleControlIdToReplace", AV51ddo_Contrato_NumeroTitleControlIdToReplace);
            AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace", AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace);
            AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace", AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace);
            AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace", AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace);
            AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace", AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace);
            AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace", AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace);
            AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace", AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace);
            AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace", AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace);
            AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace", AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_104 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_104"), ",", "."));
            AV90GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV91GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoocorrencianotificacao_codigo_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Caption");
            Ddo_contratoocorrencianotificacao_codigo_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Tooltip");
            Ddo_contratoocorrencianotificacao_codigo_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Cls");
            Ddo_contratoocorrencianotificacao_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Filteredtextto_set");
            Ddo_contratoocorrencianotificacao_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_codigo_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Includefilter"));
            Ddo_contratoocorrencianotificacao_codigo_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Filtertype");
            Ddo_contratoocorrencianotificacao_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_codigo_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Sortasc");
            Ddo_contratoocorrencianotificacao_codigo_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Sortdsc");
            Ddo_contratoocorrencianotificacao_codigo_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Rangefilterfrom");
            Ddo_contratoocorrencianotificacao_codigo_Rangefilterto = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Rangefilterto");
            Ddo_contratoocorrencianotificacao_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Searchbuttontext");
            Ddo_contratoocorrencia_codigo_Caption = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Caption");
            Ddo_contratoocorrencia_codigo_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Tooltip");
            Ddo_contratoocorrencia_codigo_Cls = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Cls");
            Ddo_contratoocorrencia_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Filteredtext_set");
            Ddo_contratoocorrencia_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Filteredtextto_set");
            Ddo_contratoocorrencia_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Dropdownoptionstype");
            Ddo_contratoocorrencia_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencia_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Includesortasc"));
            Ddo_contratoocorrencia_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Includesortdsc"));
            Ddo_contratoocorrencia_codigo_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Sortedstatus");
            Ddo_contratoocorrencia_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Includefilter"));
            Ddo_contratoocorrencia_codigo_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Filtertype");
            Ddo_contratoocorrencia_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Filterisrange"));
            Ddo_contratoocorrencia_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Includedatalist"));
            Ddo_contratoocorrencia_codigo_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Sortasc");
            Ddo_contratoocorrencia_codigo_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Sortdsc");
            Ddo_contratoocorrencia_codigo_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Cleanfilter");
            Ddo_contratoocorrencia_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Rangefilterfrom");
            Ddo_contratoocorrencia_codigo_Rangefilterto = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Rangefilterto");
            Ddo_contratoocorrencia_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Searchbuttontext");
            Ddo_contrato_codigo_Caption = cgiGet( "DDO_CONTRATO_CODIGO_Caption");
            Ddo_contrato_codigo_Tooltip = cgiGet( "DDO_CONTRATO_CODIGO_Tooltip");
            Ddo_contrato_codigo_Cls = cgiGet( "DDO_CONTRATO_CODIGO_Cls");
            Ddo_contrato_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtext_set");
            Ddo_contrato_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtextto_set");
            Ddo_contrato_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_CODIGO_Dropdownoptionstype");
            Ddo_contrato_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_CODIGO_Titlecontrolidtoreplace");
            Ddo_contrato_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includesortasc"));
            Ddo_contrato_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includesortdsc"));
            Ddo_contrato_codigo_Sortedstatus = cgiGet( "DDO_CONTRATO_CODIGO_Sortedstatus");
            Ddo_contrato_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includefilter"));
            Ddo_contrato_codigo_Filtertype = cgiGet( "DDO_CONTRATO_CODIGO_Filtertype");
            Ddo_contrato_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Filterisrange"));
            Ddo_contrato_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includedatalist"));
            Ddo_contrato_codigo_Sortasc = cgiGet( "DDO_CONTRATO_CODIGO_Sortasc");
            Ddo_contrato_codigo_Sortdsc = cgiGet( "DDO_CONTRATO_CODIGO_Sortdsc");
            Ddo_contrato_codigo_Cleanfilter = cgiGet( "DDO_CONTRATO_CODIGO_Cleanfilter");
            Ddo_contrato_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATO_CODIGO_Rangefilterfrom");
            Ddo_contrato_codigo_Rangefilterto = cgiGet( "DDO_CONTRATO_CODIGO_Rangefilterto");
            Ddo_contrato_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATO_CODIGO_Searchbuttontext");
            Ddo_contrato_numero_Caption = cgiGet( "DDO_CONTRATO_NUMERO_Caption");
            Ddo_contrato_numero_Tooltip = cgiGet( "DDO_CONTRATO_NUMERO_Tooltip");
            Ddo_contrato_numero_Cls = cgiGet( "DDO_CONTRATO_NUMERO_Cls");
            Ddo_contrato_numero_Filteredtext_set = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_set");
            Ddo_contrato_numero_Selectedvalue_set = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_set");
            Ddo_contrato_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_NUMERO_Dropdownoptionstype");
            Ddo_contrato_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace");
            Ddo_contrato_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortasc"));
            Ddo_contrato_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortdsc"));
            Ddo_contrato_numero_Sortedstatus = cgiGet( "DDO_CONTRATO_NUMERO_Sortedstatus");
            Ddo_contrato_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includefilter"));
            Ddo_contrato_numero_Filtertype = cgiGet( "DDO_CONTRATO_NUMERO_Filtertype");
            Ddo_contrato_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Filterisrange"));
            Ddo_contrato_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includedatalist"));
            Ddo_contrato_numero_Datalisttype = cgiGet( "DDO_CONTRATO_NUMERO_Datalisttype");
            Ddo_contrato_numero_Datalistproc = cgiGet( "DDO_CONTRATO_NUMERO_Datalistproc");
            Ddo_contrato_numero_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contrato_numero_Sortasc = cgiGet( "DDO_CONTRATO_NUMERO_Sortasc");
            Ddo_contrato_numero_Sortdsc = cgiGet( "DDO_CONTRATO_NUMERO_Sortdsc");
            Ddo_contrato_numero_Loadingdata = cgiGet( "DDO_CONTRATO_NUMERO_Loadingdata");
            Ddo_contrato_numero_Cleanfilter = cgiGet( "DDO_CONTRATO_NUMERO_Cleanfilter");
            Ddo_contrato_numero_Noresultsfound = cgiGet( "DDO_CONTRATO_NUMERO_Noresultsfound");
            Ddo_contrato_numero_Searchbuttontext = cgiGet( "DDO_CONTRATO_NUMERO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_data_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Caption");
            Ddo_contratoocorrencianotificacao_data_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Tooltip");
            Ddo_contratoocorrencianotificacao_data_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Cls");
            Ddo_contratoocorrencianotificacao_data_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_data_Filteredtextto_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtextto_set");
            Ddo_contratoocorrencianotificacao_data_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includesortasc"));
            Ddo_contratoocorrencianotificacao_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_data_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortedstatus");
            Ddo_contratoocorrencianotificacao_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includefilter"));
            Ddo_contratoocorrencianotificacao_data_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filtertype");
            Ddo_contratoocorrencianotificacao_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filterisrange"));
            Ddo_contratoocorrencianotificacao_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includedatalist"));
            Ddo_contratoocorrencianotificacao_data_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortasc");
            Ddo_contratoocorrencianotificacao_data_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortdsc");
            Ddo_contratoocorrencianotificacao_data_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Cleanfilter");
            Ddo_contratoocorrencianotificacao_data_Rangefilterfrom = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Rangefilterfrom");
            Ddo_contratoocorrencianotificacao_data_Rangefilterto = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Rangefilterto");
            Ddo_contratoocorrencianotificacao_data_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_prazo_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Caption");
            Ddo_contratoocorrencianotificacao_prazo_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Tooltip");
            Ddo_contratoocorrencianotificacao_prazo_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Cls");
            Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtextto_set");
            Ddo_contratoocorrencianotificacao_prazo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_prazo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_prazo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_prazo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includefilter"));
            Ddo_contratoocorrencianotificacao_prazo_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filtertype");
            Ddo_contratoocorrencianotificacao_prazo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_prazo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_prazo_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortasc");
            Ddo_contratoocorrencianotificacao_prazo_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortdsc");
            Ddo_contratoocorrencianotificacao_prazo_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_prazo_Rangefilterfrom = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Rangefilterfrom");
            Ddo_contratoocorrencianotificacao_prazo_Rangefilterto = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Rangefilterto");
            Ddo_contratoocorrencianotificacao_prazo_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_descricao_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Caption");
            Ddo_contratoocorrencianotificacao_descricao_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Tooltip");
            Ddo_contratoocorrencianotificacao_descricao_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Cls");
            Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Selectedvalue_set");
            Ddo_contratoocorrencianotificacao_descricao_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includefilter"));
            Ddo_contratoocorrencianotificacao_descricao_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filtertype");
            Ddo_contratoocorrencianotificacao_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_descricao_Datalisttype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalisttype");
            Ddo_contratoocorrencianotificacao_descricao_Datalistproc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalistproc");
            Ddo_contratoocorrencianotificacao_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencianotificacao_descricao_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortasc");
            Ddo_contratoocorrencianotificacao_descricao_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortdsc");
            Ddo_contratoocorrencianotificacao_descricao_Loadingdata = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Loadingdata");
            Ddo_contratoocorrencianotificacao_descricao_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_descricao_Noresultsfound = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Noresultsfound");
            Ddo_contratoocorrencianotificacao_descricao_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_cumprido_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Caption");
            Ddo_contratoocorrencianotificacao_cumprido_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Tooltip");
            Ddo_contratoocorrencianotificacao_cumprido_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Cls");
            Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtextto_set");
            Ddo_contratoocorrencianotificacao_cumprido_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_cumprido_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_cumprido_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_cumprido_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includefilter"));
            Ddo_contratoocorrencianotificacao_cumprido_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filtertype");
            Ddo_contratoocorrencianotificacao_cumprido_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_cumprido_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_cumprido_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortasc");
            Ddo_contratoocorrencianotificacao_cumprido_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortdsc");
            Ddo_contratoocorrencianotificacao_cumprido_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_cumprido_Rangefilterfrom = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Rangefilterfrom");
            Ddo_contratoocorrencianotificacao_cumprido_Rangefilterto = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Rangefilterto");
            Ddo_contratoocorrencianotificacao_cumprido_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_protocolo_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Caption");
            Ddo_contratoocorrencianotificacao_protocolo_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Tooltip");
            Ddo_contratoocorrencianotificacao_protocolo_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Cls");
            Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Selectedvalue_set");
            Ddo_contratoocorrencianotificacao_protocolo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_protocolo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_protocolo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_protocolo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includefilter"));
            Ddo_contratoocorrencianotificacao_protocolo_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filtertype");
            Ddo_contratoocorrencianotificacao_protocolo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_protocolo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_protocolo_Datalisttype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalisttype");
            Ddo_contratoocorrencianotificacao_protocolo_Datalistproc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalistproc");
            Ddo_contratoocorrencianotificacao_protocolo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencianotificacao_protocolo_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortasc");
            Ddo_contratoocorrencianotificacao_protocolo_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortdsc");
            Ddo_contratoocorrencianotificacao_protocolo_Loadingdata = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Loadingdata");
            Ddo_contratoocorrencianotificacao_protocolo_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_protocolo_Noresultsfound = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Noresultsfound");
            Ddo_contratoocorrencianotificacao_protocolo_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_responsavel_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Caption");
            Ddo_contratoocorrencianotificacao_responsavel_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Tooltip");
            Ddo_contratoocorrencianotificacao_responsavel_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Cls");
            Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtextto_set");
            Ddo_contratoocorrencianotificacao_responsavel_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_responsavel_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includesortasc"));
            Ddo_contratoocorrencianotificacao_responsavel_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Sortedstatus");
            Ddo_contratoocorrencianotificacao_responsavel_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includefilter"));
            Ddo_contratoocorrencianotificacao_responsavel_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filtertype");
            Ddo_contratoocorrencianotificacao_responsavel_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filterisrange"));
            Ddo_contratoocorrencianotificacao_responsavel_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includedatalist"));
            Ddo_contratoocorrencianotificacao_responsavel_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Sortasc");
            Ddo_contratoocorrencianotificacao_responsavel_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Sortdsc");
            Ddo_contratoocorrencianotificacao_responsavel_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Cleanfilter");
            Ddo_contratoocorrencianotificacao_responsavel_Rangefilterfrom = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Rangefilterfrom");
            Ddo_contratoocorrencianotificacao_responsavel_Rangefilterto = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Rangefilterto");
            Ddo_contratoocorrencianotificacao_responsavel_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_nome_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Caption");
            Ddo_contratoocorrencianotificacao_nome_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Tooltip");
            Ddo_contratoocorrencianotificacao_nome_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Cls");
            Ddo_contratoocorrencianotificacao_nome_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Selectedvalue_set");
            Ddo_contratoocorrencianotificacao_nome_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includesortasc"));
            Ddo_contratoocorrencianotificacao_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_nome_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortedstatus");
            Ddo_contratoocorrencianotificacao_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includefilter"));
            Ddo_contratoocorrencianotificacao_nome_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filtertype");
            Ddo_contratoocorrencianotificacao_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filterisrange"));
            Ddo_contratoocorrencianotificacao_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includedatalist"));
            Ddo_contratoocorrencianotificacao_nome_Datalisttype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalisttype");
            Ddo_contratoocorrencianotificacao_nome_Datalistproc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalistproc");
            Ddo_contratoocorrencianotificacao_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencianotificacao_nome_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortasc");
            Ddo_contratoocorrencianotificacao_nome_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortdsc");
            Ddo_contratoocorrencianotificacao_nome_Loadingdata = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Loadingdata");
            Ddo_contratoocorrencianotificacao_nome_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Cleanfilter");
            Ddo_contratoocorrencianotificacao_nome_Noresultsfound = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Noresultsfound");
            Ddo_contratoocorrencianotificacao_nome_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_docto_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Caption");
            Ddo_contratoocorrencianotificacao_docto_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Tooltip");
            Ddo_contratoocorrencianotificacao_docto_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Cls");
            Ddo_contratoocorrencianotificacao_docto_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_docto_Selectedvalue_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Selectedvalue_set");
            Ddo_contratoocorrencianotificacao_docto_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_docto_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_docto_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_docto_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_docto_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includefilter"));
            Ddo_contratoocorrencianotificacao_docto_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filtertype");
            Ddo_contratoocorrencianotificacao_docto_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_docto_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_docto_Datalisttype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Datalisttype");
            Ddo_contratoocorrencianotificacao_docto_Datalistproc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Datalistproc");
            Ddo_contratoocorrencianotificacao_docto_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencianotificacao_docto_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Sortasc");
            Ddo_contratoocorrencianotificacao_docto_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Sortdsc");
            Ddo_contratoocorrencianotificacao_docto_Loadingdata = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Loadingdata");
            Ddo_contratoocorrencianotificacao_docto_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_docto_Noresultsfound = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Noresultsfound");
            Ddo_contratoocorrencianotificacao_docto_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoocorrencianotificacao_codigo_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO_Filteredtextto_get");
            Ddo_contratoocorrencia_codigo_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Activeeventkey");
            Ddo_contratoocorrencia_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Filteredtext_get");
            Ddo_contratoocorrencia_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATOOCORRENCIA_CODIGO_Filteredtextto_get");
            Ddo_contrato_codigo_Activeeventkey = cgiGet( "DDO_CONTRATO_CODIGO_Activeeventkey");
            Ddo_contrato_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtext_get");
            Ddo_contrato_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtextto_get");
            Ddo_contrato_numero_Activeeventkey = cgiGet( "DDO_CONTRATO_NUMERO_Activeeventkey");
            Ddo_contrato_numero_Filteredtext_get = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_get");
            Ddo_contrato_numero_Selectedvalue_get = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_get");
            Ddo_contratoocorrencianotificacao_data_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Activeeventkey");
            Ddo_contratoocorrencianotificacao_data_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_data_Filteredtextto_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtextto_get");
            Ddo_contratoocorrencianotificacao_prazo_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtextto_get");
            Ddo_contratoocorrencianotificacao_descricao_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Selectedvalue_get");
            Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtextto_get");
            Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Selectedvalue_get");
            Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Activeeventkey");
            Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtextto_get");
            Ddo_contratoocorrencianotificacao_nome_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Activeeventkey");
            Ddo_contratoocorrencianotificacao_nome_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Selectedvalue_get");
            Ddo_contratoocorrencianotificacao_docto_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_docto_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_docto_Selectedvalue_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA1"), 0) != AV17ContratoOcorrenciaNotificacao_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1"), 0) != AV18ContratoOcorrenciaNotificacao_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME1"), AV19ContratoOcorrenciaNotificacao_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA2"), 0) != AV23ContratoOcorrenciaNotificacao_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2"), 0) != AV24ContratoOcorrenciaNotificacao_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME2"), AV25ContratoOcorrenciaNotificacao_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA3"), 0) != AV29ContratoOcorrenciaNotificacao_Data3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3"), 0) != AV30ContratoOcorrenciaNotificacao_Data_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME3"), AV31ContratoOcorrenciaNotificacao_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO"), ",", ".") != Convert.ToDecimal( AV37TFContratoOcorrenciaNotificacao_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV38TFContratoOcorrenciaNotificacao_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_CODIGO"), ",", ".") != Convert.ToDecimal( AV41TFContratoOcorrencia_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV42TFContratoOcorrencia_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV45TFContrato_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV46TFContrato_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV49TFContrato_Numero) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV50TFContrato_Numero_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA"), 0) != AV53TFContratoOcorrenciaNotificacao_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO"), 0) != AV54TFContratoOcorrenciaNotificacao_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO"), ",", ".") != Convert.ToDecimal( AV59TFContratoOcorrenciaNotificacao_Prazo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO"), ",", ".") != Convert.ToDecimal( AV60TFContratoOcorrenciaNotificacao_Prazo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO"), AV63TFContratoOcorrenciaNotificacao_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL"), AV64TFContratoOcorrenciaNotificacao_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO"), 0) != AV67TFContratoOcorrenciaNotificacao_Cumprido )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO"), 0) != AV68TFContratoOcorrenciaNotificacao_Cumprido_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO"), AV73TFContratoOcorrenciaNotificacao_Protocolo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL"), AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL"), ",", ".") != Convert.ToDecimal( AV77TFContratoOcorrenciaNotificacao_Responsavel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO"), ",", ".") != Convert.ToDecimal( AV78TFContratoOcorrenciaNotificacao_Responsavel_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME"), AV81TFContratoOcorrenciaNotificacao_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL"), AV82TFContratoOcorrenciaNotificacao_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO"), AV85TFContratoOcorrenciaNotificacao_Docto) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL"), AV86TFContratoOcorrenciaNotificacao_Docto_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E347D2 */
         E347D2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E347D2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersSelector3 = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontratoocorrencianotificacao_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_codigo_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_codigo_to_Visible), 5, 0)));
         edtavTfcontratoocorrencia_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencia_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencia_codigo_Visible), 5, 0)));
         edtavTfcontratoocorrencia_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencia_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencia_codigo_to_Visible), 5, 0)));
         edtavTfcontrato_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_codigo_Visible), 5, 0)));
         edtavTfcontrato_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_codigo_to_Visible), 5, 0)));
         edtavTfcontrato_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_Visible), 5, 0)));
         edtavTfcontrato_numero_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_data_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_data_to_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_prazo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_prazo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_prazo_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_prazo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_prazo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_prazo_to_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_descricao_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_descricao_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_cumprido_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_cumprido_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_cumprido_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_cumprido_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_cumprido_to_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_protocolo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_protocolo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_protocolo_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_responsavel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_responsavel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_responsavel_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_responsavel_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_responsavel_to_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_nome_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_nome_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_docto_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_docto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_docto_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_docto_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_docto_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_docto_sel_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_codigo_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_codigo_Titlecontrolidtoreplace);
         AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace", AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencia_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrencia_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_codigo_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencia_codigo_Titlecontrolidtoreplace);
         AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace = Ddo_contratoocorrencia_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace", AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace);
         edtavDdo_contratoocorrencia_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencia_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencia_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contrato_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "TitleControlIdToReplace", Ddo_contrato_codigo_Titlecontrolidtoreplace);
         AV47ddo_Contrato_CodigoTitleControlIdToReplace = Ddo_contrato_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Contrato_CodigoTitleControlIdToReplace", AV47ddo_Contrato_CodigoTitleControlIdToReplace);
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contrato_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "TitleControlIdToReplace", Ddo_contrato_numero_Titlecontrolidtoreplace);
         AV51ddo_Contrato_NumeroTitleControlIdToReplace = Ddo_contrato_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_Contrato_NumeroTitleControlIdToReplace", AV51ddo_Contrato_NumeroTitleControlIdToReplace);
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace);
         AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace", AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Prazo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace);
         AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace", AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace);
         AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace", AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Cumprido";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace);
         AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace", AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Protocolo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace);
         AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace", AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Responsavel";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace);
         AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace", AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace);
         AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace", AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Docto";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_docto_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace);
         AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace", AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Contrato Ocorrencia Notificacao";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "de emiss�o", 0);
         cmbavOrderedby.addItem("2", "da Notifica��o", 0);
         cmbavOrderedby.addItem("3", "C�digo da Ocorr�ncia", 0);
         cmbavOrderedby.addItem("4", "Contrato", 0);
         cmbavOrderedby.addItem("5", "N�mero do Contrato", 0);
         cmbavOrderedby.addItem("6", "de cumprimento", 0);
         cmbavOrderedby.addItem("7", "Descri��o", 0);
         cmbavOrderedby.addItem("8", "de cumprimento", 0);
         cmbavOrderedby.addItem("9", "de Protocolo", 0);
         cmbavOrderedby.addItem("10", "pessoa respons�vel", 0);
         cmbavOrderedby.addItem("11", "do respons�vek", 0);
         cmbavOrderedby.addItem("12", "do respons�vel", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV88DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV88DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E357D2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV36ContratoOcorrenciaNotificacao_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40ContratoOcorrencia_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44Contrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ContratoOcorrenciaNotificacao_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContratoOcorrenciaNotificacao_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62ContratoOcorrenciaNotificacao_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66ContratoOcorrenciaNotificacao_CumpridoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV80ContratoOcorrenciaNotificacao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV84ContratoOcorrenciaNotificacao_DoctoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoOcorrenciaNotificacao_Codigo_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "da Notifica��o", AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Codigo_Internalname, "Title", edtContratoOcorrenciaNotificacao_Codigo_Title);
         edtContratoOcorrencia_Codigo_Titleformat = 2;
         edtContratoOcorrencia_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo da Ocorr�ncia", AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrencia_Codigo_Internalname, "Title", edtContratoOcorrencia_Codigo_Title);
         edtContrato_Codigo_Titleformat = 2;
         edtContrato_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contrato", AV47ddo_Contrato_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Title", edtContrato_Codigo_Title);
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N�mero do Contrato", AV51ddo_Contrato_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContratoOcorrenciaNotificacao_Data_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de emiss�o", AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Data_Internalname, "Title", edtContratoOcorrenciaNotificacao_Data_Title);
         edtContratoOcorrenciaNotificacao_Prazo_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Prazo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de cumprimento", AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Prazo_Internalname, "Title", edtContratoOcorrenciaNotificacao_Prazo_Title);
         edtContratoOcorrenciaNotificacao_Descricao_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Descricao_Internalname, "Title", edtContratoOcorrenciaNotificacao_Descricao_Title);
         edtContratoOcorrenciaNotificacao_Cumprido_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Cumprido_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de cumprimento", AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Cumprido_Internalname, "Title", edtContratoOcorrenciaNotificacao_Cumprido_Title);
         edtContratoOcorrenciaNotificacao_Protocolo_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Protocolo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Protocolo", AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Protocolo_Internalname, "Title", edtContratoOcorrenciaNotificacao_Protocolo_Title);
         edtContratoOcorrenciaNotificacao_Responsavel_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Responsavel_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "pessoa respons�vel", AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Responsavel_Internalname, "Title", edtContratoOcorrenciaNotificacao_Responsavel_Title);
         edtContratoOcorrenciaNotificacao_Nome_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "do respons�vek", AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Nome_Internalname, "Title", edtContratoOcorrenciaNotificacao_Nome_Title);
         edtContratoOcorrenciaNotificacao_Docto_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Docto_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "do respons�vel", AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Docto_Internalname, "Title", edtContratoOcorrenciaNotificacao_Docto_Title);
         AV90GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV90GridCurrentPage), 10, 0)));
         AV91GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV91GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36ContratoOcorrenciaNotificacao_CodigoTitleFilterData", AV36ContratoOcorrenciaNotificacao_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV40ContratoOcorrencia_CodigoTitleFilterData", AV40ContratoOcorrencia_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44Contrato_CodigoTitleFilterData", AV44Contrato_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48Contrato_NumeroTitleFilterData", AV48Contrato_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52ContratoOcorrenciaNotificacao_DataTitleFilterData", AV52ContratoOcorrenciaNotificacao_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58ContratoOcorrenciaNotificacao_PrazoTitleFilterData", AV58ContratoOcorrenciaNotificacao_PrazoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62ContratoOcorrenciaNotificacao_DescricaoTitleFilterData", AV62ContratoOcorrenciaNotificacao_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV66ContratoOcorrenciaNotificacao_CumpridoTitleFilterData", AV66ContratoOcorrenciaNotificacao_CumpridoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV72ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData", AV72ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV76ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData", AV76ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV80ContratoOcorrenciaNotificacao_NomeTitleFilterData", AV80ContratoOcorrenciaNotificacao_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV84ContratoOcorrenciaNotificacao_DoctoTitleFilterData", AV84ContratoOcorrenciaNotificacao_DoctoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E117D2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV89PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV89PageToGo) ;
         }
      }

      protected void E127D2( )
      {
         /* Ddo_contratoocorrencianotificacao_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_codigo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_codigo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFContratoOcorrenciaNotificacao_Codigo = (int)(NumberUtil.Val( Ddo_contratoocorrencianotificacao_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoOcorrenciaNotificacao_Codigo), 6, 0)));
            AV38TFContratoOcorrenciaNotificacao_Codigo_To = (int)(NumberUtil.Val( Ddo_contratoocorrencianotificacao_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoOcorrenciaNotificacao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContratoOcorrenciaNotificacao_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E137D2( )
      {
         /* Ddo_contratoocorrencia_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencia_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencia_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_codigo_Internalname, "SortedStatus", Ddo_contratoocorrencia_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencia_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencia_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_codigo_Internalname, "SortedStatus", Ddo_contratoocorrencia_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencia_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV41TFContratoOcorrencia_Codigo = (int)(NumberUtil.Val( Ddo_contratoocorrencia_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContratoOcorrencia_Codigo), 6, 0)));
            AV42TFContratoOcorrencia_Codigo_To = (int)(NumberUtil.Val( Ddo_contratoocorrencia_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoOcorrencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFContratoOcorrencia_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E147D2( )
      {
         /* Ddo_contrato_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFContrato_Codigo = (int)(NumberUtil.Val( Ddo_contrato_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContrato_Codigo), 6, 0)));
            AV46TFContrato_Codigo_To = (int)(NumberUtil.Val( Ddo_contrato_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContrato_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E157D2( )
      {
         /* Ddo_contrato_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFContrato_Numero = Ddo_contrato_numero_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContrato_Numero", AV49TFContrato_Numero);
            AV50TFContrato_Numero_Sel = Ddo_contrato_numero_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContrato_Numero_Sel", AV50TFContrato_Numero_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E167D2( )
      {
         /* Ddo_contratoocorrencianotificacao_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV53TFContratoOcorrenciaNotificacao_Data = context.localUtil.CToD( Ddo_contratoocorrencianotificacao_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
            AV54TFContratoOcorrenciaNotificacao_Data_To = context.localUtil.CToD( Ddo_contratoocorrencianotificacao_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E177D2( )
      {
         /* Ddo_contratoocorrencianotificacao_prazo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_prazo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_prazo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_prazo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_prazo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_prazo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFContratoOcorrenciaNotificacao_Prazo = (short)(NumberUtil.Val( Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
            AV60TFContratoOcorrenciaNotificacao_Prazo_To = (short)(NumberUtil.Val( Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E187D2( )
      {
         /* Ddo_contratoocorrencianotificacao_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFContratoOcorrenciaNotificacao_Descricao = Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoOcorrenciaNotificacao_Descricao", AV63TFContratoOcorrenciaNotificacao_Descricao);
            AV64TFContratoOcorrenciaNotificacao_Descricao_Sel = Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoOcorrenciaNotificacao_Descricao_Sel", AV64TFContratoOcorrenciaNotificacao_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E197D2( )
      {
         /* Ddo_contratoocorrencianotificacao_cumprido_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV67TFContratoOcorrenciaNotificacao_Cumprido = context.localUtil.CToD( Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV67TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
            AV68TFContratoOcorrenciaNotificacao_Cumprido_To = context.localUtil.CToD( Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV68TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E207D2( )
      {
         /* Ddo_contratoocorrencianotificacao_protocolo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV73TFContratoOcorrenciaNotificacao_Protocolo = Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratoOcorrenciaNotificacao_Protocolo", AV73TFContratoOcorrenciaNotificacao_Protocolo);
            AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel = Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel", AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E217D2( )
      {
         /* Ddo_contratoocorrencianotificacao_responsavel_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV77TFContratoOcorrenciaNotificacao_Responsavel = (int)(NumberUtil.Val( Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
            AV78TFContratoOcorrenciaNotificacao_Responsavel_To = (int)(NumberUtil.Val( Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContratoOcorrenciaNotificacao_Responsavel_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E227D2( )
      {
         /* Ddo_contratoocorrencianotificacao_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV81TFContratoOcorrenciaNotificacao_Nome = Ddo_contratoocorrencianotificacao_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoOcorrenciaNotificacao_Nome", AV81TFContratoOcorrenciaNotificacao_Nome);
            AV82TFContratoOcorrenciaNotificacao_Nome_Sel = Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratoOcorrenciaNotificacao_Nome_Sel", AV82TFContratoOcorrenciaNotificacao_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E237D2( )
      {
         /* Ddo_contratoocorrencianotificacao_docto_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_docto_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_docto_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_docto_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_docto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_docto_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_docto_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_docto_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_docto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_docto_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV85TFContratoOcorrenciaNotificacao_Docto = Ddo_contratoocorrencianotificacao_docto_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoOcorrenciaNotificacao_Docto", AV85TFContratoOcorrenciaNotificacao_Docto);
            AV86TFContratoOcorrenciaNotificacao_Docto_Sel = Ddo_contratoocorrencianotificacao_docto_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFContratoOcorrenciaNotificacao_Docto_Sel", AV86TFContratoOcorrenciaNotificacao_Docto_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E367D2( )
      {
         /* Grid_Load Routine */
         AV34Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV34Select);
         AV94Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 104;
         }
         sendrow_1042( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_104_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(104, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E377D2 */
         E377D2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E377D2( )
      {
         /* Enter Routine */
         AV7InOutContratoOcorrenciaNotificacao_Codigo = A297ContratoOcorrenciaNotificacao_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoOcorrenciaNotificacao_Codigo), 6, 0)));
         AV8InOutContratoOcorrenciaNotificacao_Data = A298ContratoOcorrenciaNotificacao_Data;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV8InOutContratoOcorrenciaNotificacao_Data, "99/99/99"));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratoOcorrenciaNotificacao_Codigo,context.localUtil.Format( AV8InOutContratoOcorrenciaNotificacao_Data, "99/99/99")});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E247D2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E297D2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E257D2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContratoOcorrenciaNotificacao_Data3, AV30ContratoOcorrenciaNotificacao_Data_To3, AV31ContratoOcorrenciaNotificacao_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFContratoOcorrenciaNotificacao_Codigo, AV38TFContratoOcorrenciaNotificacao_Codigo_To, AV41TFContratoOcorrencia_Codigo, AV42TFContratoOcorrencia_Codigo_To, AV45TFContrato_Codigo, AV46TFContrato_Codigo_To, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoOcorrenciaNotificacao_Data, AV54TFContratoOcorrenciaNotificacao_Data_To, AV59TFContratoOcorrenciaNotificacao_Prazo, AV60TFContratoOcorrenciaNotificacao_Prazo_To, AV63TFContratoOcorrenciaNotificacao_Descricao, AV64TFContratoOcorrenciaNotificacao_Descricao_Sel, AV67TFContratoOcorrenciaNotificacao_Cumprido, AV68TFContratoOcorrenciaNotificacao_Cumprido_To, AV73TFContratoOcorrenciaNotificacao_Protocolo, AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV77TFContratoOcorrenciaNotificacao_Responsavel, AV78TFContratoOcorrenciaNotificacao_Responsavel_To, AV81TFContratoOcorrenciaNotificacao_Nome, AV82TFContratoOcorrenciaNotificacao_Nome_Sel, AV85TFContratoOcorrenciaNotificacao_Docto, AV86TFContratoOcorrenciaNotificacao_Docto_Sel, AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace, AV47ddo_Contrato_CodigoTitleControlIdToReplace, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV95Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E307D2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E317D2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV26DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E267D2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContratoOcorrenciaNotificacao_Data3, AV30ContratoOcorrenciaNotificacao_Data_To3, AV31ContratoOcorrenciaNotificacao_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFContratoOcorrenciaNotificacao_Codigo, AV38TFContratoOcorrenciaNotificacao_Codigo_To, AV41TFContratoOcorrencia_Codigo, AV42TFContratoOcorrencia_Codigo_To, AV45TFContrato_Codigo, AV46TFContrato_Codigo_To, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoOcorrenciaNotificacao_Data, AV54TFContratoOcorrenciaNotificacao_Data_To, AV59TFContratoOcorrenciaNotificacao_Prazo, AV60TFContratoOcorrenciaNotificacao_Prazo_To, AV63TFContratoOcorrenciaNotificacao_Descricao, AV64TFContratoOcorrenciaNotificacao_Descricao_Sel, AV67TFContratoOcorrenciaNotificacao_Cumprido, AV68TFContratoOcorrenciaNotificacao_Cumprido_To, AV73TFContratoOcorrenciaNotificacao_Protocolo, AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV77TFContratoOcorrenciaNotificacao_Responsavel, AV78TFContratoOcorrenciaNotificacao_Responsavel_To, AV81TFContratoOcorrenciaNotificacao_Nome, AV82TFContratoOcorrenciaNotificacao_Nome_Sel, AV85TFContratoOcorrenciaNotificacao_Docto, AV86TFContratoOcorrenciaNotificacao_Docto_Sel, AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace, AV47ddo_Contrato_CodigoTitleControlIdToReplace, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV95Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E327D2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E277D2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29ContratoOcorrenciaNotificacao_Data3, AV30ContratoOcorrenciaNotificacao_Data_To3, AV31ContratoOcorrenciaNotificacao_Nome3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFContratoOcorrenciaNotificacao_Codigo, AV38TFContratoOcorrenciaNotificacao_Codigo_To, AV41TFContratoOcorrencia_Codigo, AV42TFContratoOcorrencia_Codigo_To, AV45TFContrato_Codigo, AV46TFContrato_Codigo_To, AV49TFContrato_Numero, AV50TFContrato_Numero_Sel, AV53TFContratoOcorrenciaNotificacao_Data, AV54TFContratoOcorrenciaNotificacao_Data_To, AV59TFContratoOcorrenciaNotificacao_Prazo, AV60TFContratoOcorrenciaNotificacao_Prazo_To, AV63TFContratoOcorrenciaNotificacao_Descricao, AV64TFContratoOcorrenciaNotificacao_Descricao_Sel, AV67TFContratoOcorrenciaNotificacao_Cumprido, AV68TFContratoOcorrenciaNotificacao_Cumprido_To, AV73TFContratoOcorrenciaNotificacao_Protocolo, AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV77TFContratoOcorrenciaNotificacao_Responsavel, AV78TFContratoOcorrenciaNotificacao_Responsavel_To, AV81TFContratoOcorrenciaNotificacao_Nome, AV82TFContratoOcorrenciaNotificacao_Nome_Sel, AV85TFContratoOcorrenciaNotificacao_Docto, AV86TFContratoOcorrenciaNotificacao_Docto_Sel, AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace, AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace, AV47ddo_Contrato_CodigoTitleControlIdToReplace, AV51ddo_Contrato_NumeroTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV95Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E337D2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E287D2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoocorrencianotificacao_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_codigo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_codigo_Sortedstatus);
         Ddo_contratoocorrencia_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_codigo_Internalname, "SortedStatus", Ddo_contratoocorrencia_codigo_Sortedstatus);
         Ddo_contrato_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
         Ddo_contrato_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         Ddo_contratoocorrencianotificacao_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_data_Sortedstatus);
         Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_prazo_Sortedstatus);
         Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_descricao_Sortedstatus);
         Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus);
         Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus);
         Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus);
         Ddo_contratoocorrencianotificacao_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_nome_Sortedstatus);
         Ddo_contratoocorrencianotificacao_docto_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_docto_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_docto_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contratoocorrencianotificacao_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_codigo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoocorrencia_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_codigo_Internalname, "SortedStatus", Ddo_contratoocorrencia_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contrato_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contrato_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoocorrencianotificacao_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_prazo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 10 )
         {
            Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus);
         }
         else if ( AV13OrderedBy == 11 )
         {
            Ddo_contratoocorrencianotificacao_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 12 )
         {
            Ddo_contratoocorrencianotificacao_docto_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_docto_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_docto_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible), 5, 0)));
         edtavContratoocorrencianotificacao_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencianotificacao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencianotificacao_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
         {
            edtavContratoocorrencianotificacao_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencianotificacao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencianotificacao_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible), 5, 0)));
         edtavContratoocorrencianotificacao_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencianotificacao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencianotificacao_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
         {
            edtavContratoocorrencianotificacao_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencianotificacao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencianotificacao_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Visible), 5, 0)));
         edtavContratoocorrencianotificacao_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencianotificacao_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencianotificacao_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
         {
            edtavContratoocorrencianotificacao_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencianotificacao_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencianotificacao_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV23ContratoOcorrenciaNotificacao_Data2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
         AV24ContratoOcorrenciaNotificacao_Data_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         AV27DynamicFiltersSelector3 = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         AV29ContratoOcorrenciaNotificacao_Data3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContratoOcorrenciaNotificacao_Data3", context.localUtil.Format(AV29ContratoOcorrenciaNotificacao_Data3, "99/99/99"));
         AV30ContratoOcorrenciaNotificacao_Data_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratoOcorrenciaNotificacao_Data_To3", context.localUtil.Format(AV30ContratoOcorrenciaNotificacao_Data_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV37TFContratoOcorrenciaNotificacao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContratoOcorrenciaNotificacao_Codigo), 6, 0)));
         Ddo_contratoocorrencianotificacao_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_codigo_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_codigo_Filteredtext_set);
         AV38TFContratoOcorrenciaNotificacao_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContratoOcorrenciaNotificacao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContratoOcorrenciaNotificacao_Codigo_To), 6, 0)));
         Ddo_contratoocorrencianotificacao_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_codigo_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_codigo_Filteredtextto_set);
         AV41TFContratoOcorrencia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContratoOcorrencia_Codigo), 6, 0)));
         Ddo_contratoocorrencia_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_codigo_Internalname, "FilteredText_set", Ddo_contratoocorrencia_codigo_Filteredtext_set);
         AV42TFContratoOcorrencia_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratoOcorrencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFContratoOcorrencia_Codigo_To), 6, 0)));
         Ddo_contratoocorrencia_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencia_codigo_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencia_codigo_Filteredtextto_set);
         AV45TFContrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContrato_Codigo), 6, 0)));
         Ddo_contrato_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredText_set", Ddo_contrato_codigo_Filteredtext_set);
         AV46TFContrato_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContrato_Codigo_To), 6, 0)));
         Ddo_contrato_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredTextTo_set", Ddo_contrato_codigo_Filteredtextto_set);
         AV49TFContrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContrato_Numero", AV49TFContrato_Numero);
         Ddo_contrato_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
         AV50TFContrato_Numero_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContrato_Numero_Sel", AV50TFContrato_Numero_Sel);
         Ddo_contrato_numero_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
         AV53TFContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
         Ddo_contratoocorrencianotificacao_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_data_Filteredtext_set);
         AV54TFContratoOcorrenciaNotificacao_Data_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
         Ddo_contratoocorrencianotificacao_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_data_Filteredtextto_set);
         AV59TFContratoOcorrenciaNotificacao_Prazo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
         Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set);
         AV60TFContratoOcorrenciaNotificacao_Prazo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
         Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set);
         AV63TFContratoOcorrenciaNotificacao_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoOcorrenciaNotificacao_Descricao", AV63TFContratoOcorrenciaNotificacao_Descricao);
         Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set);
         AV64TFContratoOcorrenciaNotificacao_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoOcorrenciaNotificacao_Descricao_Sel", AV64TFContratoOcorrenciaNotificacao_Descricao_Sel);
         Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set);
         AV67TFContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV67TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set);
         AV68TFContratoOcorrenciaNotificacao_Cumprido_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV68TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set);
         AV73TFContratoOcorrenciaNotificacao_Protocolo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratoOcorrenciaNotificacao_Protocolo", AV73TFContratoOcorrenciaNotificacao_Protocolo);
         Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set);
         AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel", AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel);
         Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set);
         AV77TFContratoOcorrenciaNotificacao_Responsavel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77TFContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
         Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_set);
         AV78TFContratoOcorrenciaNotificacao_Responsavel_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78TFContratoOcorrenciaNotificacao_Responsavel_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0)));
         Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_set);
         AV81TFContratoOcorrenciaNotificacao_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoOcorrenciaNotificacao_Nome", AV81TFContratoOcorrenciaNotificacao_Nome);
         Ddo_contratoocorrencianotificacao_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_nome_Filteredtext_set);
         AV82TFContratoOcorrenciaNotificacao_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFContratoOcorrenciaNotificacao_Nome_Sel", AV82TFContratoOcorrenciaNotificacao_Nome_Sel);
         Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set);
         AV85TFContratoOcorrenciaNotificacao_Docto = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoOcorrenciaNotificacao_Docto", AV85TFContratoOcorrenciaNotificacao_Docto);
         Ddo_contratoocorrencianotificacao_docto_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_docto_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_docto_Filteredtext_set);
         AV86TFContratoOcorrenciaNotificacao_Docto_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFContratoOcorrenciaNotificacao_Docto_Sel", AV86TFContratoOcorrenciaNotificacao_Docto_Sel);
         Ddo_contratoocorrencianotificacao_docto_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_docto_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_docto_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17ContratoOcorrenciaNotificacao_Data1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
         AV18ContratoOcorrenciaNotificacao_Data_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
            {
               AV17ContratoOcorrenciaNotificacao_Data1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
               AV18ContratoOcorrenciaNotificacao_Data_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV19ContratoOcorrenciaNotificacao_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoOcorrenciaNotificacao_Nome1", AV19ContratoOcorrenciaNotificacao_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
               {
                  AV23ContratoOcorrenciaNotificacao_Data2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
                  AV24ContratoOcorrenciaNotificacao_Data_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV25ContratoOcorrenciaNotificacao_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoOcorrenciaNotificacao_Nome2", AV25ContratoOcorrenciaNotificacao_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV26DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV27DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
                  {
                     AV29ContratoOcorrenciaNotificacao_Data3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContratoOcorrenciaNotificacao_Data3", context.localUtil.Format(AV29ContratoOcorrenciaNotificacao_Data3, "99/99/99"));
                     AV30ContratoOcorrenciaNotificacao_Data_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratoOcorrenciaNotificacao_Data_To3", context.localUtil.Format(AV30ContratoOcorrenciaNotificacao_Data_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV31ContratoOcorrenciaNotificacao_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoOcorrenciaNotificacao_Nome3", AV31ContratoOcorrenciaNotificacao_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV32DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV37TFContratoOcorrenciaNotificacao_Codigo) && (0==AV38TFContratoOcorrenciaNotificacao_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV37TFContratoOcorrenciaNotificacao_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV38TFContratoOcorrenciaNotificacao_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV41TFContratoOcorrencia_Codigo) && (0==AV42TFContratoOcorrencia_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV41TFContratoOcorrencia_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV42TFContratoOcorrencia_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV45TFContrato_Codigo) && (0==AV46TFContrato_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV45TFContrato_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV46TFContrato_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContrato_Numero)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = AV49TFContrato_Numero;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContrato_Numero_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV50TFContrato_Numero_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV53TFContratoOcorrenciaNotificacao_Data) && (DateTime.MinValue==AV54TFContratoOcorrenciaNotificacao_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV53TFContratoOcorrenciaNotificacao_Data, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV54TFContratoOcorrenciaNotificacao_Data_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV59TFContratoOcorrenciaNotificacao_Prazo) && (0==AV60TFContratoOcorrenciaNotificacao_Prazo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_PRAZO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV59TFContratoOcorrenciaNotificacao_Prazo), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFContratoOcorrenciaNotificacao_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV63TFContratoOcorrenciaNotificacao_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV64TFContratoOcorrenciaNotificacao_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV67TFContratoOcorrenciaNotificacao_Cumprido) && (DateTime.MinValue==AV68TFContratoOcorrenciaNotificacao_Cumprido_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV67TFContratoOcorrenciaNotificacao_Cumprido, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV68TFContratoOcorrenciaNotificacao_Cumprido_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFContratoOcorrenciaNotificacao_Protocolo)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
            AV11GridStateFilterValue.gxTpr_Value = AV73TFContratoOcorrenciaNotificacao_Protocolo;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV77TFContratoOcorrenciaNotificacao_Responsavel) && (0==AV78TFContratoOcorrenciaNotificacao_Responsavel_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV77TFContratoOcorrenciaNotificacao_Responsavel), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV78TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81TFContratoOcorrenciaNotificacao_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV81TFContratoOcorrenciaNotificacao_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV82TFContratoOcorrenciaNotificacao_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85TFContratoOcorrenciaNotificacao_Docto)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_DOCTO";
            AV11GridStateFilterValue.gxTpr_Value = AV85TFContratoOcorrenciaNotificacao_Docto;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV86TFContratoOcorrenciaNotificacao_Docto_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV95Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV33DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV17ContratoOcorrenciaNotificacao_Data1) && (DateTime.MinValue==AV18ContratoOcorrenciaNotificacao_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV17ContratoOcorrenciaNotificacao_Data1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV18ContratoOcorrenciaNotificacao_Data_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19ContratoOcorrenciaNotificacao_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV23ContratoOcorrenciaNotificacao_Data2) && (DateTime.MinValue==AV24ContratoOcorrenciaNotificacao_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV23ContratoOcorrenciaNotificacao_Data2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV24ContratoOcorrenciaNotificacao_Data_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ContratoOcorrenciaNotificacao_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV26DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV27DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV29ContratoOcorrenciaNotificacao_Data3) && (DateTime.MinValue==AV30ContratoOcorrenciaNotificacao_Data_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV29ContratoOcorrenciaNotificacao_Data3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV30ContratoOcorrenciaNotificacao_Data_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratoOcorrenciaNotificacao_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31ContratoOcorrenciaNotificacao_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_7D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_7D2( true) ;
         }
         else
         {
            wb_table2_5_7D2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_7D2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_98_7D2( true) ;
         }
         else
         {
            wb_table3_98_7D2( false) ;
         }
         return  ;
      }

      protected void wb_table3_98_7D2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_7D2e( true) ;
         }
         else
         {
            wb_table1_2_7D2e( false) ;
         }
      }

      protected void wb_table3_98_7D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_101_7D2( true) ;
         }
         else
         {
            wb_table4_101_7D2( false) ;
         }
         return  ;
      }

      protected void wb_table4_101_7D2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_98_7D2e( true) ;
         }
         else
         {
            wb_table3_98_7D2e( false) ;
         }
      }

      protected void wb_table4_101_7D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"104\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrencia_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrencia_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrencia_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(127), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Prazo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Prazo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Prazo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Cumprido_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Cumprido_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Cumprido_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Protocolo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Protocolo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Protocolo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Responsavel_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Responsavel_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Responsavel_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Docto_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Docto_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Docto_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrencia_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrencia_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Prazo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Prazo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A300ContratoOcorrenciaNotificacao_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Cumprido_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Cumprido_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A302ContratoOcorrenciaNotificacao_Protocolo);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Protocolo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Protocolo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Responsavel_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Responsavel_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A304ContratoOcorrenciaNotificacao_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A306ContratoOcorrenciaNotificacao_Docto);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Docto_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Docto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 104 )
         {
            wbEnd = 0;
            nRC_GXsfl_104 = (short)(nGXsfl_104_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_101_7D2e( true) ;
         }
         else
         {
            wb_table4_101_7D2e( false) ;
         }
      }

      protected void wb_table2_5_7D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_7D2( true) ;
         }
         else
         {
            wb_table5_14_7D2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_7D2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_7D2e( true) ;
         }
         else
         {
            wb_table2_5_7D2e( false) ;
         }
      }

      protected void wb_table5_14_7D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_7D2( true) ;
         }
         else
         {
            wb_table6_19_7D2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_7D2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_7D2e( true) ;
         }
         else
         {
            wb_table5_14_7D2e( false) ;
         }
      }

      protected void wb_table6_19_7D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_7D2( true) ;
         }
         else
         {
            wb_table7_28_7D2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_7D2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_53_7D2( true) ;
         }
         else
         {
            wb_table8_53_7D2( false) ;
         }
         return  ;
      }

      protected void wb_table8_53_7D2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV27DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_78_7D2( true) ;
         }
         else
         {
            wb_table9_78_7D2( false) ;
         }
         return  ;
      }

      protected void wb_table9_78_7D2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_7D2e( true) ;
         }
         else
         {
            wb_table6_19_7D2e( false) ;
         }
      }

      protected void wb_table9_78_7D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", "", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_83_7D2( true) ;
         }
         else
         {
            wb_table10_83_7D2( false) ;
         }
         return  ;
      }

      protected void wb_table10_83_7D2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_nome3_Internalname, StringUtil.RTrim( AV31ContratoOcorrenciaNotificacao_Nome3), StringUtil.RTrim( context.localUtil.Format( AV31ContratoOcorrenciaNotificacao_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoocorrencianotificacao_nome3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_78_7D2e( true) ;
         }
         else
         {
            wb_table9_78_7D2e( false) ;
         }
      }

      protected void wb_table10_83_7D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Internalname, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data3_Internalname, context.localUtil.Format(AV29ContratoOcorrenciaNotificacao_Data3, "99/99/99"), context.localUtil.Format( AV29ContratoOcorrenciaNotificacao_Data3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data_to3_Internalname, context.localUtil.Format(AV30ContratoOcorrenciaNotificacao_Data_To3, "99/99/99"), context.localUtil.Format( AV30ContratoOcorrenciaNotificacao_Data_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_83_7D2e( true) ;
         }
         else
         {
            wb_table10_83_7D2e( false) ;
         }
      }

      protected void wb_table8_53_7D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_58_7D2( true) ;
         }
         else
         {
            wb_table11_58_7D2( false) ;
         }
         return  ;
      }

      protected void wb_table11_58_7D2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_nome2_Internalname, StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2), StringUtil.RTrim( context.localUtil.Format( AV25ContratoOcorrenciaNotificacao_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoocorrencianotificacao_nome2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_53_7D2e( true) ;
         }
         else
         {
            wb_table8_53_7D2e( false) ;
         }
      }

      protected void wb_table11_58_7D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data2_Internalname, context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"), context.localUtil.Format( AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data_to2_Internalname, context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"), context.localUtil.Format( AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_58_7D2e( true) ;
         }
         else
         {
            wb_table11_58_7D2e( false) ;
         }
      }

      protected void wb_table7_28_7D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_104_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table12_33_7D2( true) ;
         }
         else
         {
            wb_table12_33_7D2( false) ;
         }
         return  ;
      }

      protected void wb_table12_33_7D2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_104_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_nome1_Internalname, StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1), StringUtil.RTrim( context.localUtil.Format( AV19ContratoOcorrenciaNotificacao_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoocorrencianotificacao_nome1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_7D2e( true) ;
         }
         else
         {
            wb_table7_28_7D2e( false) ;
         }
      }

      protected void wb_table12_33_7D2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data1_Internalname, context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"), context.localUtil.Format( AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_104_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data_to1_Internalname, context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"), context.localUtil.Format( AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_33_7D2e( true) ;
         }
         else
         {
            wb_table12_33_7D2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratoOcorrenciaNotificacao_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoOcorrenciaNotificacao_Codigo), 6, 0)));
         AV8InOutContratoOcorrenciaNotificacao_Data = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV8InOutContratoOcorrenciaNotificacao_Data, "99/99/99"));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA7D2( ) ;
         WS7D2( ) ;
         WE7D2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311737793");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratoocorrencianotificacao.js", "?2020311737794");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1042( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_104_idx;
         edtContratoOcorrenciaNotificacao_Codigo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CODIGO_"+sGXsfl_104_idx;
         edtContratoOcorrencia_Codigo_Internalname = "CONTRATOOCORRENCIA_CODIGO_"+sGXsfl_104_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_104_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_104_idx;
         edtContratoOcorrenciaNotificacao_Data_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DATA_"+sGXsfl_104_idx;
         edtContratoOcorrenciaNotificacao_Prazo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PRAZO_"+sGXsfl_104_idx;
         edtContratoOcorrenciaNotificacao_Descricao_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_"+sGXsfl_104_idx;
         edtContratoOcorrenciaNotificacao_Cumprido_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_"+sGXsfl_104_idx;
         edtContratoOcorrenciaNotificacao_Protocolo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_"+sGXsfl_104_idx;
         edtContratoOcorrenciaNotificacao_Responsavel_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_"+sGXsfl_104_idx;
         edtContratoOcorrenciaNotificacao_Nome_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_NOME_"+sGXsfl_104_idx;
         edtContratoOcorrenciaNotificacao_Docto_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DOCTO_"+sGXsfl_104_idx;
      }

      protected void SubsflControlProps_fel_1042( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_104_fel_idx;
         edtContratoOcorrenciaNotificacao_Codigo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CODIGO_"+sGXsfl_104_fel_idx;
         edtContratoOcorrencia_Codigo_Internalname = "CONTRATOOCORRENCIA_CODIGO_"+sGXsfl_104_fel_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_104_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_104_fel_idx;
         edtContratoOcorrenciaNotificacao_Data_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DATA_"+sGXsfl_104_fel_idx;
         edtContratoOcorrenciaNotificacao_Prazo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PRAZO_"+sGXsfl_104_fel_idx;
         edtContratoOcorrenciaNotificacao_Descricao_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_"+sGXsfl_104_fel_idx;
         edtContratoOcorrenciaNotificacao_Cumprido_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_"+sGXsfl_104_fel_idx;
         edtContratoOcorrenciaNotificacao_Protocolo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_"+sGXsfl_104_fel_idx;
         edtContratoOcorrenciaNotificacao_Responsavel_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_"+sGXsfl_104_fel_idx;
         edtContratoOcorrenciaNotificacao_Nome_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_NOME_"+sGXsfl_104_fel_idx;
         edtContratoOcorrenciaNotificacao_Docto_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DOCTO_"+sGXsfl_104_fel_idx;
      }

      protected void sendrow_1042( )
      {
         SubsflControlProps_1042( ) ;
         WB7D0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_104_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_104_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_104_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 105,'',false,'',104)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV34Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV94Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Select)) ? AV94Select_GXI : context.PathToRelativeUrl( AV34Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_104_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV34Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrencia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrencia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)127,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Data_Internalname,context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"),context.localUtil.Format( A298ContratoOcorrenciaNotificacao_Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Prazo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Prazo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Descricao_Internalname,(String)A300ContratoOcorrenciaNotificacao_Descricao,StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Cumprido_Internalname,context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"),context.localUtil.Format( A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Protocolo_Internalname,(String)A302ContratoOcorrenciaNotificacao_Protocolo,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Responsavel_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Nome_Internalname,StringUtil.RTrim( A304ContratoOcorrenciaNotificacao_Nome),StringUtil.RTrim( context.localUtil.Format( A304ContratoOcorrenciaNotificacao_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Docto_Internalname,(String)A306ContratoOcorrenciaNotificacao_Docto,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Docto_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)104,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_CODIGO"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, context.localUtil.Format( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIA_CODIGO"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_DATA"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, A298ContratoOcorrenciaNotificacao_Data));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_PRAZO"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, A301ContratoOcorrenciaNotificacao_Cumprido));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, StringUtil.RTrim( context.localUtil.Format( A302ContratoOcorrenciaNotificacao_Protocolo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL"+"_"+sGXsfl_104_idx, GetSecureSignedToken( sGXsfl_104_idx, context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_104_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_104_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_104_idx+1));
            sGXsfl_104_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_104_idx), 4, 0)), 4, "0");
            SubsflControlProps_1042( ) ;
         }
         /* End function sendrow_1042 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContratoocorrencianotificacao_data1_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_DATA1";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA_RANGEMIDDLETEXT1";
         edtavContratoocorrencianotificacao_data_to1_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1";
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1";
         edtavContratoocorrencianotificacao_nome1_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContratoocorrencianotificacao_data2_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_DATA2";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA_RANGEMIDDLETEXT2";
         edtavContratoocorrencianotificacao_data_to2_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2";
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2";
         edtavContratoocorrencianotificacao_nome2_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContratoocorrencianotificacao_data3_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_DATA3";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA_RANGEMIDDLETEXT3";
         edtavContratoocorrencianotificacao_data_to3_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3";
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA3";
         edtavContratoocorrencianotificacao_nome3_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_NOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContratoOcorrenciaNotificacao_Codigo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CODIGO";
         edtContratoOcorrencia_Codigo_Internalname = "CONTRATOOCORRENCIA_CODIGO";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtContratoOcorrenciaNotificacao_Data_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         edtContratoOcorrenciaNotificacao_Prazo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PRAZO";
         edtContratoOcorrenciaNotificacao_Descricao_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
         edtContratoOcorrenciaNotificacao_Cumprido_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
         edtContratoOcorrenciaNotificacao_Protocolo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
         edtContratoOcorrenciaNotificacao_Responsavel_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL";
         edtContratoOcorrenciaNotificacao_Nome_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_NOME";
         edtContratoOcorrenciaNotificacao_Docto_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DOCTO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontratoocorrencianotificacao_codigo_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO";
         edtavTfcontratoocorrencianotificacao_codigo_to_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO";
         edtavTfcontratoocorrencia_codigo_Internalname = "vTFCONTRATOOCORRENCIA_CODIGO";
         edtavTfcontratoocorrencia_codigo_to_Internalname = "vTFCONTRATOOCORRENCIA_CODIGO_TO";
         edtavTfcontrato_codigo_Internalname = "vTFCONTRATO_CODIGO";
         edtavTfcontrato_codigo_to_Internalname = "vTFCONTRATO_CODIGO_TO";
         edtavTfcontrato_numero_Internalname = "vTFCONTRATO_NUMERO";
         edtavTfcontrato_numero_sel_Internalname = "vTFCONTRATO_NUMERO_SEL";
         edtavTfcontratoocorrencianotificacao_data_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_DATA";
         edtavTfcontratoocorrencianotificacao_data_to_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO";
         edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATE";
         edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATETO";
         divDdo_contratoocorrencianotificacao_dataauxdates_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATES";
         edtavTfcontratoocorrencianotificacao_prazo_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO";
         edtavTfcontratoocorrencianotificacao_prazo_to_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO";
         edtavTfcontratoocorrencianotificacao_descricao_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
         edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL";
         edtavTfcontratoocorrencianotificacao_cumprido_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
         edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO";
         edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATE";
         edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATETO";
         divDdo_contratoocorrencianotificacao_cumpridoauxdates_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATES";
         edtavTfcontratoocorrencianotificacao_protocolo_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
         edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL";
         edtavTfcontratoocorrencianotificacao_responsavel_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL";
         edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO";
         edtavTfcontratoocorrencianotificacao_nome_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_NOME";
         edtavTfcontratoocorrencianotificacao_nome_sel_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL";
         edtavTfcontratoocorrencianotificacao_docto_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO";
         edtavTfcontratoocorrencianotificacao_docto_sel_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL";
         Ddo_contratoocorrencianotificacao_codigo_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO";
         edtavDdo_contratoocorrencianotificacao_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencia_codigo_Internalname = "DDO_CONTRATOOCORRENCIA_CODIGO";
         edtavDdo_contratoocorrencia_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contrato_codigo_Internalname = "DDO_CONTRATO_CODIGO";
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contrato_numero_Internalname = "DDO_CONTRATO_NUMERO";
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_data_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA";
         edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_prazo_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO";
         edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_descricao_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
         edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_cumprido_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
         edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_protocolo_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
         edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_responsavel_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL";
         edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_nome_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME";
         edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_docto_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO";
         edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoOcorrenciaNotificacao_Docto_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Nome_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Descricao_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Prazo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Data_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         edtContratoOcorrencia_Codigo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContratoocorrencianotificacao_data_to1_Jsonclick = "";
         edtavContratoocorrencianotificacao_data1_Jsonclick = "";
         edtavContratoocorrencianotificacao_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratoocorrencianotificacao_data_to2_Jsonclick = "";
         edtavContratoocorrencianotificacao_data2_Jsonclick = "";
         edtavContratoocorrencianotificacao_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContratoocorrencianotificacao_data_to3_Jsonclick = "";
         edtavContratoocorrencianotificacao_data3_Jsonclick = "";
         edtavContratoocorrencianotificacao_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContratoOcorrenciaNotificacao_Docto_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Nome_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Responsavel_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Protocolo_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Cumprido_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Descricao_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Prazo_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Data_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         edtContrato_Codigo_Titleformat = 0;
         edtContratoOcorrencia_Codigo_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContratoocorrencianotificacao_nome3_Visible = 1;
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratoocorrencianotificacao_nome2_Visible = 1;
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratoocorrencianotificacao_nome1_Visible = 1;
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible = 1;
         edtContratoOcorrenciaNotificacao_Docto_Title = "do respons�vel";
         edtContratoOcorrenciaNotificacao_Nome_Title = "do respons�vek";
         edtContratoOcorrenciaNotificacao_Responsavel_Title = "pessoa respons�vel";
         edtContratoOcorrenciaNotificacao_Protocolo_Title = "de Protocolo";
         edtContratoOcorrenciaNotificacao_Cumprido_Title = "de cumprimento";
         edtContratoOcorrenciaNotificacao_Descricao_Title = "Descri��o";
         edtContratoOcorrenciaNotificacao_Prazo_Title = "de cumprimento";
         edtContratoOcorrenciaNotificacao_Data_Title = "de emiss�o";
         edtContrato_Numero_Title = "N�mero do Contrato";
         edtContrato_Codigo_Title = "Contrato";
         edtContratoOcorrencia_Codigo_Title = "C�digo da Ocorr�ncia";
         edtContratoOcorrenciaNotificacao_Codigo_Title = "da Notifica��o";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencia_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoocorrencianotificacao_docto_sel_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_docto_sel_Visible = 1;
         edtavTfcontratoocorrencianotificacao_docto_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_docto_Visible = 1;
         edtavTfcontratoocorrencianotificacao_nome_sel_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_nome_sel_Visible = 1;
         edtavTfcontratoocorrencianotificacao_nome_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_nome_Visible = 1;
         edtavTfcontratoocorrencianotificacao_responsavel_to_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_responsavel_to_Visible = 1;
         edtavTfcontratoocorrencianotificacao_responsavel_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_responsavel_Visible = 1;
         edtavTfcontratoocorrencianotificacao_protocolo_sel_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible = 1;
         edtavTfcontratoocorrencianotificacao_protocolo_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_protocolo_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Jsonclick = "";
         edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_cumprido_to_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_cumprido_to_Visible = 1;
         edtavTfcontratoocorrencianotificacao_cumprido_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_cumprido_Visible = 1;
         edtavTfcontratoocorrencianotificacao_descricao_sel_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_descricao_sel_Visible = 1;
         edtavTfcontratoocorrencianotificacao_descricao_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_descricao_Visible = 1;
         edtavTfcontratoocorrencianotificacao_prazo_to_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_prazo_to_Visible = 1;
         edtavTfcontratoocorrencianotificacao_prazo_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_prazo_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_dataauxdateto_Jsonclick = "";
         edtavDdo_contratoocorrencianotificacao_dataauxdate_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_data_to_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_data_to_Visible = 1;
         edtavTfcontratoocorrencianotificacao_data_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_data_Visible = 1;
         edtavTfcontrato_numero_sel_Jsonclick = "";
         edtavTfcontrato_numero_sel_Visible = 1;
         edtavTfcontrato_numero_Jsonclick = "";
         edtavTfcontrato_numero_Visible = 1;
         edtavTfcontrato_codigo_to_Jsonclick = "";
         edtavTfcontrato_codigo_to_Visible = 1;
         edtavTfcontrato_codigo_Jsonclick = "";
         edtavTfcontrato_codigo_Visible = 1;
         edtavTfcontratoocorrencia_codigo_to_Jsonclick = "";
         edtavTfcontratoocorrencia_codigo_to_Visible = 1;
         edtavTfcontratoocorrencia_codigo_Jsonclick = "";
         edtavTfcontratoocorrencia_codigo_Visible = 1;
         edtavTfcontratoocorrencianotificacao_codigo_to_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_codigo_to_Visible = 1;
         edtavTfcontratoocorrencianotificacao_codigo_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratoocorrencianotificacao_docto_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_docto_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencianotificacao_docto_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_docto_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencianotificacao_docto_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_docto_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_docto_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencianotificacao_docto_Datalistproc = "GetPromptContratoOcorrenciaNotificacaoFilterData";
         Ddo_contratoocorrencianotificacao_docto_Datalisttype = "Dynamic";
         Ddo_contratoocorrencianotificacao_docto_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_docto_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_docto_Filtertype = "Character";
         Ddo_contratoocorrencianotificacao_docto_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_docto_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_docto_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_docto_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_docto_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_docto_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_docto_Caption = "";
         Ddo_contratoocorrencianotificacao_nome_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencianotificacao_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_nome_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencianotificacao_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_nome_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_nome_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencianotificacao_nome_Datalistproc = "GetPromptContratoOcorrenciaNotificacaoFilterData";
         Ddo_contratoocorrencianotificacao_nome_Datalisttype = "Dynamic";
         Ddo_contratoocorrencianotificacao_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_nome_Filtertype = "Character";
         Ddo_contratoocorrencianotificacao_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_nome_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_nome_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_nome_Caption = "";
         Ddo_contratoocorrencianotificacao_responsavel_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_responsavel_Rangefilterto = "At�";
         Ddo_contratoocorrencianotificacao_responsavel_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencianotificacao_responsavel_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_responsavel_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_responsavel_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_responsavel_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_responsavel_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_responsavel_Filtertype = "Numeric";
         Ddo_contratoocorrencianotificacao_responsavel_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_responsavel_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_responsavel_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_responsavel_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_responsavel_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_responsavel_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_responsavel_Caption = "";
         Ddo_contratoocorrencianotificacao_protocolo_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_protocolo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencianotificacao_protocolo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_protocolo_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencianotificacao_protocolo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_protocolo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_protocolo_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencianotificacao_protocolo_Datalistproc = "GetPromptContratoOcorrenciaNotificacaoFilterData";
         Ddo_contratoocorrencianotificacao_protocolo_Datalisttype = "Dynamic";
         Ddo_contratoocorrencianotificacao_protocolo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_protocolo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_protocolo_Filtertype = "Character";
         Ddo_contratoocorrencianotificacao_protocolo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_protocolo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_protocolo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_protocolo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_protocolo_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_protocolo_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_protocolo_Caption = "";
         Ddo_contratoocorrencianotificacao_cumprido_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_cumprido_Rangefilterto = "At�";
         Ddo_contratoocorrencianotificacao_cumprido_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencianotificacao_cumprido_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_cumprido_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_cumprido_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_cumprido_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_cumprido_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_cumprido_Filtertype = "Date";
         Ddo_contratoocorrencianotificacao_cumprido_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_cumprido_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_cumprido_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_cumprido_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_cumprido_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_cumprido_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_cumprido_Caption = "";
         Ddo_contratoocorrencianotificacao_descricao_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencianotificacao_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_descricao_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencianotificacao_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencianotificacao_descricao_Datalistproc = "GetPromptContratoOcorrenciaNotificacaoFilterData";
         Ddo_contratoocorrencianotificacao_descricao_Datalisttype = "Dynamic";
         Ddo_contratoocorrencianotificacao_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_descricao_Filtertype = "Character";
         Ddo_contratoocorrencianotificacao_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_descricao_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_descricao_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_descricao_Caption = "";
         Ddo_contratoocorrencianotificacao_prazo_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_prazo_Rangefilterto = "At�";
         Ddo_contratoocorrencianotificacao_prazo_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencianotificacao_prazo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_prazo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_prazo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_prazo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_prazo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_prazo_Filtertype = "Numeric";
         Ddo_contratoocorrencianotificacao_prazo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_prazo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_prazo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_prazo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_prazo_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_prazo_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_prazo_Caption = "";
         Ddo_contratoocorrencianotificacao_data_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_data_Rangefilterto = "At�";
         Ddo_contratoocorrencianotificacao_data_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencianotificacao_data_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_data_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_data_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_data_Filtertype = "Date";
         Ddo_contratoocorrencianotificacao_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_data_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_data_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_data_Caption = "";
         Ddo_contrato_numero_Searchbuttontext = "Pesquisar";
         Ddo_contrato_numero_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contrato_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_numero_Loadingdata = "Carregando dados...";
         Ddo_contrato_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_numero_Datalistupdateminimumcharacters = 0;
         Ddo_contrato_numero_Datalistproc = "GetPromptContratoOcorrenciaNotificacaoFilterData";
         Ddo_contrato_numero_Datalisttype = "Dynamic";
         Ddo_contrato_numero_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contrato_numero_Filtertype = "Character";
         Ddo_contrato_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Titlecontrolidtoreplace = "";
         Ddo_contrato_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_numero_Cls = "ColumnSettings";
         Ddo_contrato_numero_Tooltip = "Op��es";
         Ddo_contrato_numero_Caption = "";
         Ddo_contrato_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contrato_codigo_Rangefilterto = "At�";
         Ddo_contrato_codigo_Rangefilterfrom = "Desde";
         Ddo_contrato_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contrato_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Filtertype = "Numeric";
         Ddo_contrato_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Titlecontrolidtoreplace = "";
         Ddo_contrato_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_codigo_Cls = "ColumnSettings";
         Ddo_contrato_codigo_Tooltip = "Op��es";
         Ddo_contrato_codigo_Caption = "";
         Ddo_contratoocorrencia_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencia_codigo_Rangefilterto = "At�";
         Ddo_contratoocorrencia_codigo_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencia_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencia_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencia_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencia_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencia_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_codigo_Filtertype = "Numeric";
         Ddo_contratoocorrencia_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencia_codigo_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencia_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencia_codigo_Cls = "ColumnSettings";
         Ddo_contratoocorrencia_codigo_Tooltip = "Op��es";
         Ddo_contratoocorrencia_codigo_Caption = "";
         Ddo_contratoocorrencianotificacao_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_codigo_Rangefilterto = "At�";
         Ddo_contratoocorrencianotificacao_codigo_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencianotificacao_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_codigo_Filtertype = "Numeric";
         Ddo_contratoocorrencianotificacao_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_codigo_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_codigo_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_codigo_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contrato Ocorrencia Notificacao";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'AV36ContratoOcorrenciaNotificacao_CodigoTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV40ContratoOcorrencia_CodigoTitleFilterData',fld:'vCONTRATOOCORRENCIA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV44Contrato_CodigoTitleFilterData',fld:'vCONTRATO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV48Contrato_NumeroTitleFilterData',fld:'vCONTRATO_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV52ContratoOcorrenciaNotificacao_DataTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV58ContratoOcorrenciaNotificacao_PrazoTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV62ContratoOcorrenciaNotificacao_DescricaoTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV66ContratoOcorrenciaNotificacao_CumpridoTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLEFILTERDATA',pic:'',nv:null},{av:'AV72ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLEFILTERDATA',pic:'',nv:null},{av:'AV76ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLEFILTERDATA',pic:'',nv:null},{av:'AV80ContratoOcorrenciaNotificacao_NomeTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV84ContratoOcorrenciaNotificacao_DoctoTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoOcorrenciaNotificacao_Codigo_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Codigo_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'Title'},{av:'edtContratoOcorrencia_Codigo_Titleformat',ctrl:'CONTRATOOCORRENCIA_CODIGO',prop:'Titleformat'},{av:'edtContratoOcorrencia_Codigo_Title',ctrl:'CONTRATOOCORRENCIA_CODIGO',prop:'Title'},{av:'edtContrato_Codigo_Titleformat',ctrl:'CONTRATO_CODIGO',prop:'Titleformat'},{av:'edtContrato_Codigo_Title',ctrl:'CONTRATO_CODIGO',prop:'Title'},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Data_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Data_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Prazo_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Prazo_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Descricao_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Descricao_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Cumprido_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Cumprido_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Protocolo_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Protocolo_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Responsavel_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Responsavel_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Nome_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Nome_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Docto_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Docto_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'Title'},{av:'AV90GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV91GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E117D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO.ONOPTIONCLICKED","{handler:'E127D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_codigo_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_codigo_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'SortedStatus'},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIA_CODIGO.ONOPTIONCLICKED","{handler:'E137D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoocorrencia_codigo_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencia_codigo_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencia_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'SortedStatus'},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATO_CODIGO.ONOPTIONCLICKED","{handler:'E147D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contrato_codigo_Activeeventkey',ctrl:'DDO_CONTRATO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contrato_codigo_Filteredtext_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contrato_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATO_NUMERO.ONOPTIONCLICKED","{handler:'E157D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contrato_numero_Activeeventkey',ctrl:'DDO_CONTRATO_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contrato_numero_Filteredtext_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contrato_numero_Selectedvalue_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA.ONOPTIONCLICKED","{handler:'E167D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_data_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_data_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_data_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO.ONOPTIONCLICKED","{handler:'E177D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_prazo_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO.ONOPTIONCLICKED","{handler:'E187D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_descricao_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_contratoocorrencianotificacao_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO.ONOPTIONCLICKED","{handler:'E197D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO.ONOPTIONCLICKED","{handler:'E207D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL.ONOPTIONCLICKED","{handler:'E217D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME.ONOPTIONCLICKED","{handler:'E227D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_nome_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_nome_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_contratoocorrencianotificacao_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO.ONOPTIONCLICKED","{handler:'E237D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_docto_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_docto_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_docto_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencia_codigo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E367D2',iparms:[],oparms:[{av:'AV34Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E377D2',iparms:[{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A298ContratoOcorrenciaNotificacao_Data',fld:'CONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',hsh:true,nv:''}],oparms:[{av:'AV7InOutContratoOcorrenciaNotificacao_Codigo',fld:'vINOUTCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContratoOcorrenciaNotificacao_Data',fld:'vINOUTCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E247D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E297D2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E257D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome2_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA3',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome3_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome1_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E307D2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome1_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E317D2',iparms:[],oparms:[{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E267D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome2_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA3',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome3_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome1_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E327D2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome2_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E277D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome2_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA3',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome3_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome1_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E337D2',iparms:[{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA3',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome3_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E287D2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV95Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV37TFContratoOcorrenciaNotificacao_Codigo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_codigo_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'FilteredText_set'},{av:'AV38TFContratoOcorrenciaNotificacao_Codigo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV41TFContratoOcorrencia_Codigo',fld:'vTFCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencia_codigo_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'FilteredText_set'},{av:'AV42TFContratoOcorrencia_Codigo_To',fld:'vTFCONTRATOOCORRENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencia_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATOOCORRENCIA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV45TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Filteredtext_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_set'},{av:'AV46TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV49TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'Ddo_contrato_numero_Filteredtext_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_set'},{av:'AV50TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Selectedvalue_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_set'},{av:'AV53TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_data_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'FilteredText_set'},{av:'AV54TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_data_Filteredtextto_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'FilteredTextTo_set'},{av:'AV59TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'FilteredText_set'},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'FilteredTextTo_set'},{av:'AV63TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'FilteredText_set'},{av:'AV64TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV67TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'FilteredText_set'},{av:'AV68TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'FilteredTextTo_set'},{av:'AV73TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'FilteredText_set'},{av:'AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SelectedValue_set'},{av:'AV77TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'FilteredText_set'},{av:'AV78TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'FilteredTextTo_set'},{av:'AV81TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'Ddo_contratoocorrencianotificacao_nome_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'FilteredText_set'},{av:'AV82TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SelectedValue_set'},{av:'AV85TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_docto_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'FilteredText_set'},{av:'AV86TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_docto_Selectedvalue_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome1_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29ContratoOcorrenciaNotificacao_Data3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA3',pic:'',nv:''},{av:'AV30ContratoOcorrenciaNotificacao_Data_To3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV31ContratoOcorrenciaNotificacao_Nome3',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome2_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA3',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome3_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoocorrencianotificacao_codigo_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_codigo_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_codigo_Filteredtextto_get = "";
         Ddo_contratoocorrencia_codigo_Activeeventkey = "";
         Ddo_contratoocorrencia_codigo_Filteredtext_get = "";
         Ddo_contratoocorrencia_codigo_Filteredtextto_get = "";
         Ddo_contrato_codigo_Activeeventkey = "";
         Ddo_contrato_codigo_Filteredtext_get = "";
         Ddo_contrato_codigo_Filteredtextto_get = "";
         Ddo_contrato_numero_Activeeventkey = "";
         Ddo_contrato_numero_Filteredtext_get = "";
         Ddo_contrato_numero_Selectedvalue_get = "";
         Ddo_contratoocorrencianotificacao_data_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_data_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_data_Filteredtextto_get = "";
         Ddo_contratoocorrencianotificacao_prazo_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get = "";
         Ddo_contratoocorrencianotificacao_descricao_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get = "";
         Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get = "";
         Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get = "";
         Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_get = "";
         Ddo_contratoocorrencianotificacao_nome_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_nome_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get = "";
         Ddo_contratoocorrencianotificacao_docto_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_docto_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_docto_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ContratoOcorrenciaNotificacao_Data1 = DateTime.MinValue;
         AV18ContratoOcorrenciaNotificacao_Data_To1 = DateTime.MinValue;
         AV19ContratoOcorrenciaNotificacao_Nome1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23ContratoOcorrenciaNotificacao_Data2 = DateTime.MinValue;
         AV24ContratoOcorrenciaNotificacao_Data_To2 = DateTime.MinValue;
         AV25ContratoOcorrenciaNotificacao_Nome2 = "";
         AV27DynamicFiltersSelector3 = "";
         AV29ContratoOcorrenciaNotificacao_Data3 = DateTime.MinValue;
         AV30ContratoOcorrenciaNotificacao_Data_To3 = DateTime.MinValue;
         AV31ContratoOcorrenciaNotificacao_Nome3 = "";
         AV49TFContrato_Numero = "";
         AV50TFContrato_Numero_Sel = "";
         AV53TFContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         AV54TFContratoOcorrenciaNotificacao_Data_To = DateTime.MinValue;
         AV63TFContratoOcorrenciaNotificacao_Descricao = "";
         AV64TFContratoOcorrenciaNotificacao_Descricao_Sel = "";
         AV67TFContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         AV68TFContratoOcorrenciaNotificacao_Cumprido_To = DateTime.MinValue;
         AV73TFContratoOcorrenciaNotificacao_Protocolo = "";
         AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel = "";
         AV81TFContratoOcorrenciaNotificacao_Nome = "";
         AV82TFContratoOcorrenciaNotificacao_Nome_Sel = "";
         AV85TFContratoOcorrenciaNotificacao_Docto = "";
         AV86TFContratoOcorrenciaNotificacao_Docto_Sel = "";
         AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace = "";
         AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace = "";
         AV47ddo_Contrato_CodigoTitleControlIdToReplace = "";
         AV51ddo_Contrato_NumeroTitleControlIdToReplace = "";
         AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace = "";
         AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace = "";
         AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace = "";
         AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace = "";
         AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace = "";
         AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace = "";
         AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace = "";
         AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace = "";
         AV95Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV88DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV36ContratoOcorrenciaNotificacao_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40ContratoOcorrencia_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44Contrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ContratoOcorrenciaNotificacao_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContratoOcorrenciaNotificacao_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62ContratoOcorrenciaNotificacao_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66ContratoOcorrenciaNotificacao_CumpridoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV80ContratoOcorrenciaNotificacao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV84ContratoOcorrenciaNotificacao_DoctoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoocorrencianotificacao_codigo_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_codigo_Filteredtextto_set = "";
         Ddo_contratoocorrencianotificacao_codigo_Sortedstatus = "";
         Ddo_contratoocorrencia_codigo_Filteredtext_set = "";
         Ddo_contratoocorrencia_codigo_Filteredtextto_set = "";
         Ddo_contratoocorrencia_codigo_Sortedstatus = "";
         Ddo_contrato_codigo_Filteredtext_set = "";
         Ddo_contrato_codigo_Filteredtextto_set = "";
         Ddo_contrato_codigo_Sortedstatus = "";
         Ddo_contrato_numero_Filteredtext_set = "";
         Ddo_contrato_numero_Selectedvalue_set = "";
         Ddo_contrato_numero_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_data_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_data_Filteredtextto_set = "";
         Ddo_contratoocorrencianotificacao_data_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set = "";
         Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set = "";
         Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set = "";
         Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set = "";
         Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_set = "";
         Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_nome_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set = "";
         Ddo_contratoocorrencianotificacao_nome_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_docto_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_docto_Selectedvalue_set = "";
         Ddo_contratoocorrencianotificacao_docto_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV55DDO_ContratoOcorrenciaNotificacao_DataAuxDate = DateTime.MinValue;
         AV56DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo = DateTime.MinValue;
         AV69DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate = DateTime.MinValue;
         AV70DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV34Select = "";
         AV94Select_GXI = "";
         A77Contrato_Numero = "";
         A298ContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         A300ContratoOcorrenciaNotificacao_Descricao = "";
         A301ContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         A302ContratoOcorrenciaNotificacao_Protocolo = "";
         A304ContratoOcorrenciaNotificacao_Nome = "";
         A306ContratoOcorrenciaNotificacao_Docto = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV19ContratoOcorrenciaNotificacao_Nome1 = "";
         lV25ContratoOcorrenciaNotificacao_Nome2 = "";
         lV31ContratoOcorrenciaNotificacao_Nome3 = "";
         lV49TFContrato_Numero = "";
         lV63TFContratoOcorrenciaNotificacao_Descricao = "";
         lV73TFContratoOcorrenciaNotificacao_Protocolo = "";
         lV81TFContratoOcorrenciaNotificacao_Nome = "";
         lV85TFContratoOcorrenciaNotificacao_Docto = "";
         H007D2_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         H007D2_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         H007D2_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         H007D2_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         H007D2_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         H007D2_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         H007D2_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         H007D2_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         H007D2_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         H007D2_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         H007D2_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         H007D2_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         H007D2_A77Contrato_Numero = new String[] {""} ;
         H007D2_A74Contrato_Codigo = new int[1] ;
         H007D2_A294ContratoOcorrencia_Codigo = new int[1] ;
         H007D2_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         H007D3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratoocorrencianotificacao__default(),
            new Object[][] {
                new Object[] {
               H007D2_A306ContratoOcorrenciaNotificacao_Docto, H007D2_n306ContratoOcorrenciaNotificacao_Docto, H007D2_A304ContratoOcorrenciaNotificacao_Nome, H007D2_n304ContratoOcorrenciaNotificacao_Nome, H007D2_A303ContratoOcorrenciaNotificacao_Responsavel, H007D2_A302ContratoOcorrenciaNotificacao_Protocolo, H007D2_n302ContratoOcorrenciaNotificacao_Protocolo, H007D2_A301ContratoOcorrenciaNotificacao_Cumprido, H007D2_n301ContratoOcorrenciaNotificacao_Cumprido, H007D2_A300ContratoOcorrenciaNotificacao_Descricao,
               H007D2_A299ContratoOcorrenciaNotificacao_Prazo, H007D2_A298ContratoOcorrenciaNotificacao_Data, H007D2_A77Contrato_Numero, H007D2_A74Contrato_Codigo, H007D2_A294ContratoOcorrencia_Codigo, H007D2_A297ContratoOcorrenciaNotificacao_Codigo
               }
               , new Object[] {
               H007D3_AGRID_nRecordCount
               }
            }
         );
         AV95Pgmname = "PromptContratoOcorrenciaNotificacao";
         /* GeneXus formulas. */
         AV95Pgmname = "PromptContratoOcorrenciaNotificacao";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_104 ;
      private short nGXsfl_104_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV28DynamicFiltersOperator3 ;
      private short AV59TFContratoOcorrenciaNotificacao_Prazo ;
      private short AV60TFContratoOcorrenciaNotificacao_Prazo_To ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A299ContratoOcorrenciaNotificacao_Prazo ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_104_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoOcorrenciaNotificacao_Codigo_Titleformat ;
      private short edtContratoOcorrencia_Codigo_Titleformat ;
      private short edtContrato_Codigo_Titleformat ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Data_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Prazo_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Descricao_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Cumprido_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Protocolo_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Responsavel_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Nome_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Docto_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratoOcorrenciaNotificacao_Codigo ;
      private int wcpOAV7InOutContratoOcorrenciaNotificacao_Codigo ;
      private int subGrid_Rows ;
      private int AV37TFContratoOcorrenciaNotificacao_Codigo ;
      private int AV38TFContratoOcorrenciaNotificacao_Codigo_To ;
      private int AV41TFContratoOcorrencia_Codigo ;
      private int AV42TFContratoOcorrencia_Codigo_To ;
      private int AV45TFContrato_Codigo ;
      private int AV46TFContrato_Codigo_To ;
      private int AV77TFContratoOcorrenciaNotificacao_Responsavel ;
      private int AV78TFContratoOcorrenciaNotificacao_Responsavel_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contrato_numero_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencianotificacao_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencianotificacao_protocolo_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencianotificacao_nome_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencianotificacao_docto_Datalistupdateminimumcharacters ;
      private int edtavTfcontratoocorrencianotificacao_codigo_Visible ;
      private int edtavTfcontratoocorrencianotificacao_codigo_to_Visible ;
      private int edtavTfcontratoocorrencia_codigo_Visible ;
      private int edtavTfcontratoocorrencia_codigo_to_Visible ;
      private int edtavTfcontrato_codigo_Visible ;
      private int edtavTfcontrato_codigo_to_Visible ;
      private int edtavTfcontrato_numero_Visible ;
      private int edtavTfcontrato_numero_sel_Visible ;
      private int edtavTfcontratoocorrencianotificacao_data_Visible ;
      private int edtavTfcontratoocorrencianotificacao_data_to_Visible ;
      private int edtavTfcontratoocorrencianotificacao_prazo_Visible ;
      private int edtavTfcontratoocorrencianotificacao_prazo_to_Visible ;
      private int edtavTfcontratoocorrencianotificacao_descricao_Visible ;
      private int edtavTfcontratoocorrencianotificacao_descricao_sel_Visible ;
      private int edtavTfcontratoocorrencianotificacao_cumprido_Visible ;
      private int edtavTfcontratoocorrencianotificacao_cumprido_to_Visible ;
      private int edtavTfcontratoocorrencianotificacao_protocolo_Visible ;
      private int edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible ;
      private int edtavTfcontratoocorrencianotificacao_responsavel_Visible ;
      private int edtavTfcontratoocorrencianotificacao_responsavel_to_Visible ;
      private int edtavTfcontratoocorrencianotificacao_nome_Visible ;
      private int edtavTfcontratoocorrencianotificacao_nome_sel_Visible ;
      private int edtavTfcontratoocorrencianotificacao_docto_Visible ;
      private int edtavTfcontratoocorrencianotificacao_docto_sel_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencia_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Visible ;
      private int A297ContratoOcorrenciaNotificacao_Codigo ;
      private int A294ContratoOcorrencia_Codigo ;
      private int A74Contrato_Codigo ;
      private int A303ContratoOcorrenciaNotificacao_Responsavel ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV89PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible ;
      private int edtavContratoocorrencianotificacao_nome1_Visible ;
      private int tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible ;
      private int edtavContratoocorrencianotificacao_nome2_Visible ;
      private int tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Visible ;
      private int edtavContratoocorrencianotificacao_nome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV90GridCurrentPage ;
      private long AV91GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoocorrencianotificacao_codigo_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_codigo_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_codigo_Filteredtextto_get ;
      private String Ddo_contratoocorrencia_codigo_Activeeventkey ;
      private String Ddo_contratoocorrencia_codigo_Filteredtext_get ;
      private String Ddo_contratoocorrencia_codigo_Filteredtextto_get ;
      private String Ddo_contrato_codigo_Activeeventkey ;
      private String Ddo_contrato_codigo_Filteredtext_get ;
      private String Ddo_contrato_codigo_Filteredtextto_get ;
      private String Ddo_contrato_numero_Activeeventkey ;
      private String Ddo_contrato_numero_Filteredtext_get ;
      private String Ddo_contrato_numero_Selectedvalue_get ;
      private String Ddo_contratoocorrencianotificacao_data_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_data_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_data_Filteredtextto_get ;
      private String Ddo_contratoocorrencianotificacao_prazo_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get ;
      private String Ddo_contratoocorrencianotificacao_descricao_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_get ;
      private String Ddo_contratoocorrencianotificacao_nome_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_nome_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get ;
      private String Ddo_contratoocorrencianotificacao_docto_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_docto_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_docto_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_104_idx="0001" ;
      private String AV19ContratoOcorrenciaNotificacao_Nome1 ;
      private String AV25ContratoOcorrenciaNotificacao_Nome2 ;
      private String AV31ContratoOcorrenciaNotificacao_Nome3 ;
      private String AV49TFContrato_Numero ;
      private String AV50TFContrato_Numero_Sel ;
      private String AV81TFContratoOcorrenciaNotificacao_Nome ;
      private String AV82TFContratoOcorrenciaNotificacao_Nome_Sel ;
      private String AV95Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoocorrencianotificacao_codigo_Caption ;
      private String Ddo_contratoocorrencianotificacao_codigo_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_codigo_Cls ;
      private String Ddo_contratoocorrencianotificacao_codigo_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_codigo_Filteredtextto_set ;
      private String Ddo_contratoocorrencianotificacao_codigo_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_codigo_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_codigo_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_codigo_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_codigo_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_codigo_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_codigo_Rangefilterfrom ;
      private String Ddo_contratoocorrencianotificacao_codigo_Rangefilterto ;
      private String Ddo_contratoocorrencianotificacao_codigo_Searchbuttontext ;
      private String Ddo_contratoocorrencia_codigo_Caption ;
      private String Ddo_contratoocorrencia_codigo_Tooltip ;
      private String Ddo_contratoocorrencia_codigo_Cls ;
      private String Ddo_contratoocorrencia_codigo_Filteredtext_set ;
      private String Ddo_contratoocorrencia_codigo_Filteredtextto_set ;
      private String Ddo_contratoocorrencia_codigo_Dropdownoptionstype ;
      private String Ddo_contratoocorrencia_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencia_codigo_Sortedstatus ;
      private String Ddo_contratoocorrencia_codigo_Filtertype ;
      private String Ddo_contratoocorrencia_codigo_Sortasc ;
      private String Ddo_contratoocorrencia_codigo_Sortdsc ;
      private String Ddo_contratoocorrencia_codigo_Cleanfilter ;
      private String Ddo_contratoocorrencia_codigo_Rangefilterfrom ;
      private String Ddo_contratoocorrencia_codigo_Rangefilterto ;
      private String Ddo_contratoocorrencia_codigo_Searchbuttontext ;
      private String Ddo_contrato_codigo_Caption ;
      private String Ddo_contrato_codigo_Tooltip ;
      private String Ddo_contrato_codigo_Cls ;
      private String Ddo_contrato_codigo_Filteredtext_set ;
      private String Ddo_contrato_codigo_Filteredtextto_set ;
      private String Ddo_contrato_codigo_Dropdownoptionstype ;
      private String Ddo_contrato_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contrato_codigo_Sortedstatus ;
      private String Ddo_contrato_codigo_Filtertype ;
      private String Ddo_contrato_codigo_Sortasc ;
      private String Ddo_contrato_codigo_Sortdsc ;
      private String Ddo_contrato_codigo_Cleanfilter ;
      private String Ddo_contrato_codigo_Rangefilterfrom ;
      private String Ddo_contrato_codigo_Rangefilterto ;
      private String Ddo_contrato_codigo_Searchbuttontext ;
      private String Ddo_contrato_numero_Caption ;
      private String Ddo_contrato_numero_Tooltip ;
      private String Ddo_contrato_numero_Cls ;
      private String Ddo_contrato_numero_Filteredtext_set ;
      private String Ddo_contrato_numero_Selectedvalue_set ;
      private String Ddo_contrato_numero_Dropdownoptionstype ;
      private String Ddo_contrato_numero_Titlecontrolidtoreplace ;
      private String Ddo_contrato_numero_Sortedstatus ;
      private String Ddo_contrato_numero_Filtertype ;
      private String Ddo_contrato_numero_Datalisttype ;
      private String Ddo_contrato_numero_Datalistproc ;
      private String Ddo_contrato_numero_Sortasc ;
      private String Ddo_contrato_numero_Sortdsc ;
      private String Ddo_contrato_numero_Loadingdata ;
      private String Ddo_contrato_numero_Cleanfilter ;
      private String Ddo_contrato_numero_Noresultsfound ;
      private String Ddo_contrato_numero_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_data_Caption ;
      private String Ddo_contratoocorrencianotificacao_data_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_data_Cls ;
      private String Ddo_contratoocorrencianotificacao_data_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_data_Filteredtextto_set ;
      private String Ddo_contratoocorrencianotificacao_data_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_data_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_data_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_data_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_data_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_data_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_data_Rangefilterfrom ;
      private String Ddo_contratoocorrencianotificacao_data_Rangefilterto ;
      private String Ddo_contratoocorrencianotificacao_data_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_prazo_Caption ;
      private String Ddo_contratoocorrencianotificacao_prazo_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_prazo_Cls ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set ;
      private String Ddo_contratoocorrencianotificacao_prazo_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_prazo_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_prazo_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_prazo_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_prazo_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_prazo_Rangefilterfrom ;
      private String Ddo_contratoocorrencianotificacao_prazo_Rangefilterto ;
      private String Ddo_contratoocorrencianotificacao_prazo_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_descricao_Caption ;
      private String Ddo_contratoocorrencianotificacao_descricao_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_descricao_Cls ;
      private String Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set ;
      private String Ddo_contratoocorrencianotificacao_descricao_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_descricao_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_descricao_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_descricao_Datalisttype ;
      private String Ddo_contratoocorrencianotificacao_descricao_Datalistproc ;
      private String Ddo_contratoocorrencianotificacao_descricao_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_descricao_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_descricao_Loadingdata ;
      private String Ddo_contratoocorrencianotificacao_descricao_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_descricao_Noresultsfound ;
      private String Ddo_contratoocorrencianotificacao_descricao_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Caption ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Cls ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Rangefilterfrom ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Rangefilterto ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Caption ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Cls ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Datalisttype ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Datalistproc ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Loadingdata ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Noresultsfound ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Caption ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Cls ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_set ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Rangefilterfrom ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Rangefilterto ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_nome_Caption ;
      private String Ddo_contratoocorrencianotificacao_nome_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_nome_Cls ;
      private String Ddo_contratoocorrencianotificacao_nome_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set ;
      private String Ddo_contratoocorrencianotificacao_nome_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_nome_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_nome_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_nome_Datalisttype ;
      private String Ddo_contratoocorrencianotificacao_nome_Datalistproc ;
      private String Ddo_contratoocorrencianotificacao_nome_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_nome_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_nome_Loadingdata ;
      private String Ddo_contratoocorrencianotificacao_nome_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_nome_Noresultsfound ;
      private String Ddo_contratoocorrencianotificacao_nome_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_docto_Caption ;
      private String Ddo_contratoocorrencianotificacao_docto_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_docto_Cls ;
      private String Ddo_contratoocorrencianotificacao_docto_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_docto_Selectedvalue_set ;
      private String Ddo_contratoocorrencianotificacao_docto_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_docto_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_docto_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_docto_Datalisttype ;
      private String Ddo_contratoocorrencianotificacao_docto_Datalistproc ;
      private String Ddo_contratoocorrencianotificacao_docto_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_docto_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_docto_Loadingdata ;
      private String Ddo_contratoocorrencianotificacao_docto_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_docto_Noresultsfound ;
      private String Ddo_contratoocorrencianotificacao_docto_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_codigo_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_codigo_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_codigo_to_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_codigo_to_Jsonclick ;
      private String edtavTfcontratoocorrencia_codigo_Internalname ;
      private String edtavTfcontratoocorrencia_codigo_Jsonclick ;
      private String edtavTfcontratoocorrencia_codigo_to_Internalname ;
      private String edtavTfcontratoocorrencia_codigo_to_Jsonclick ;
      private String edtavTfcontrato_codigo_Internalname ;
      private String edtavTfcontrato_codigo_Jsonclick ;
      private String edtavTfcontrato_codigo_to_Internalname ;
      private String edtavTfcontrato_codigo_to_Jsonclick ;
      private String edtavTfcontrato_numero_Internalname ;
      private String edtavTfcontrato_numero_Jsonclick ;
      private String edtavTfcontrato_numero_sel_Internalname ;
      private String edtavTfcontrato_numero_sel_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_data_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_data_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_data_to_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_data_to_Jsonclick ;
      private String divDdo_contratoocorrencianotificacao_dataauxdates_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_dataauxdate_Jsonclick ;
      private String edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_dataauxdateto_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_prazo_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_prazo_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_prazo_to_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_prazo_to_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_descricao_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_descricao_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_descricao_sel_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_cumprido_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_cumprido_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_cumprido_to_Jsonclick ;
      private String divDdo_contratoocorrencianotificacao_cumpridoauxdates_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Jsonclick ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_protocolo_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_protocolo_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_protocolo_sel_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_responsavel_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_responsavel_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_responsavel_to_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_nome_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_nome_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_nome_sel_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_nome_sel_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_docto_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_docto_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_docto_sel_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_docto_sel_Jsonclick ;
      private String edtavDdo_contratoocorrencianotificacao_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencia_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Codigo_Internalname ;
      private String edtContratoOcorrencia_Codigo_Internalname ;
      private String edtContrato_Codigo_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Data_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Prazo_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Descricao_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Cumprido_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Protocolo_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Responsavel_Internalname ;
      private String A304ContratoOcorrenciaNotificacao_Nome ;
      private String edtContratoOcorrenciaNotificacao_Nome_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Docto_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV19ContratoOcorrenciaNotificacao_Nome1 ;
      private String lV25ContratoOcorrenciaNotificacao_Nome2 ;
      private String lV31ContratoOcorrenciaNotificacao_Nome3 ;
      private String lV49TFContrato_Numero ;
      private String lV81TFContratoOcorrenciaNotificacao_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratoocorrencianotificacao_data1_Internalname ;
      private String edtavContratoocorrencianotificacao_data_to1_Internalname ;
      private String edtavContratoocorrencianotificacao_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratoocorrencianotificacao_data2_Internalname ;
      private String edtavContratoocorrencianotificacao_data_to2_Internalname ;
      private String edtavContratoocorrencianotificacao_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContratoocorrencianotificacao_data3_Internalname ;
      private String edtavContratoocorrencianotificacao_data_to3_Internalname ;
      private String edtavContratoocorrencianotificacao_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratoocorrencianotificacao_codigo_Internalname ;
      private String Ddo_contratoocorrencia_codigo_Internalname ;
      private String Ddo_contrato_codigo_Internalname ;
      private String Ddo_contrato_numero_Internalname ;
      private String Ddo_contratoocorrencianotificacao_data_Internalname ;
      private String Ddo_contratoocorrencianotificacao_prazo_Internalname ;
      private String Ddo_contratoocorrencianotificacao_descricao_Internalname ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Internalname ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Internalname ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Internalname ;
      private String Ddo_contratoocorrencianotificacao_nome_Internalname ;
      private String Ddo_contratoocorrencianotificacao_docto_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Codigo_Title ;
      private String edtContratoOcorrencia_Codigo_Title ;
      private String edtContrato_Codigo_Title ;
      private String edtContrato_Numero_Title ;
      private String edtContratoOcorrenciaNotificacao_Data_Title ;
      private String edtContratoOcorrenciaNotificacao_Prazo_Title ;
      private String edtContratoOcorrenciaNotificacao_Descricao_Title ;
      private String edtContratoOcorrenciaNotificacao_Cumprido_Title ;
      private String edtContratoOcorrenciaNotificacao_Protocolo_Title ;
      private String edtContratoOcorrenciaNotificacao_Responsavel_Title ;
      private String edtContratoOcorrenciaNotificacao_Nome_Title ;
      private String edtContratoOcorrenciaNotificacao_Docto_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname ;
      private String tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname ;
      private String tblTablemergeddynamicfilterscontratoocorrencianotificacao_data3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContratoocorrencianotificacao_nome3_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data3_Jsonclick ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext3_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data_to3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContratoocorrencianotificacao_nome2_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data2_Jsonclick ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContratoocorrencianotificacao_nome1_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data1_Jsonclick ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data_to1_Jsonclick ;
      private String sGXsfl_104_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContratoOcorrenciaNotificacao_Codigo_Jsonclick ;
      private String edtContratoOcorrencia_Codigo_Jsonclick ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Data_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Prazo_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Descricao_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Nome_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Docto_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV8InOutContratoOcorrenciaNotificacao_Data ;
      private DateTime wcpOAV8InOutContratoOcorrenciaNotificacao_Data ;
      private DateTime AV17ContratoOcorrenciaNotificacao_Data1 ;
      private DateTime AV18ContratoOcorrenciaNotificacao_Data_To1 ;
      private DateTime AV23ContratoOcorrenciaNotificacao_Data2 ;
      private DateTime AV24ContratoOcorrenciaNotificacao_Data_To2 ;
      private DateTime AV29ContratoOcorrenciaNotificacao_Data3 ;
      private DateTime AV30ContratoOcorrenciaNotificacao_Data_To3 ;
      private DateTime AV53TFContratoOcorrenciaNotificacao_Data ;
      private DateTime AV54TFContratoOcorrenciaNotificacao_Data_To ;
      private DateTime AV67TFContratoOcorrenciaNotificacao_Cumprido ;
      private DateTime AV68TFContratoOcorrenciaNotificacao_Cumprido_To ;
      private DateTime AV55DDO_ContratoOcorrenciaNotificacao_DataAuxDate ;
      private DateTime AV56DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo ;
      private DateTime AV69DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate ;
      private DateTime AV70DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo ;
      private DateTime A298ContratoOcorrenciaNotificacao_Data ;
      private DateTime A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV26DynamicFiltersEnabled3 ;
      private bool AV33DynamicFiltersIgnoreFirst ;
      private bool AV32DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoocorrencianotificacao_codigo_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_codigo_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_codigo_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_codigo_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_codigo_Includedatalist ;
      private bool Ddo_contratoocorrencia_codigo_Includesortasc ;
      private bool Ddo_contratoocorrencia_codigo_Includesortdsc ;
      private bool Ddo_contratoocorrencia_codigo_Includefilter ;
      private bool Ddo_contratoocorrencia_codigo_Filterisrange ;
      private bool Ddo_contratoocorrencia_codigo_Includedatalist ;
      private bool Ddo_contrato_codigo_Includesortasc ;
      private bool Ddo_contrato_codigo_Includesortdsc ;
      private bool Ddo_contrato_codigo_Includefilter ;
      private bool Ddo_contrato_codigo_Filterisrange ;
      private bool Ddo_contrato_codigo_Includedatalist ;
      private bool Ddo_contrato_numero_Includesortasc ;
      private bool Ddo_contrato_numero_Includesortdsc ;
      private bool Ddo_contrato_numero_Includefilter ;
      private bool Ddo_contrato_numero_Filterisrange ;
      private bool Ddo_contrato_numero_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_data_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_data_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_data_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_data_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_data_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_responsavel_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_responsavel_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_responsavel_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_responsavel_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_responsavel_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_nome_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_nome_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_nome_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_nome_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_nome_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_docto_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_docto_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_docto_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_docto_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_docto_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool n302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool n304ContratoOcorrenciaNotificacao_Nome ;
      private bool n306ContratoOcorrenciaNotificacao_Docto ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV34Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV27DynamicFiltersSelector3 ;
      private String AV63TFContratoOcorrenciaNotificacao_Descricao ;
      private String AV64TFContratoOcorrenciaNotificacao_Descricao_Sel ;
      private String AV73TFContratoOcorrenciaNotificacao_Protocolo ;
      private String AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel ;
      private String AV85TFContratoOcorrenciaNotificacao_Docto ;
      private String AV86TFContratoOcorrenciaNotificacao_Docto_Sel ;
      private String AV39ddo_ContratoOcorrenciaNotificacao_CodigoTitleControlIdToReplace ;
      private String AV43ddo_ContratoOcorrencia_CodigoTitleControlIdToReplace ;
      private String AV47ddo_Contrato_CodigoTitleControlIdToReplace ;
      private String AV51ddo_Contrato_NumeroTitleControlIdToReplace ;
      private String AV57ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace ;
      private String AV61ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace ;
      private String AV65ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace ;
      private String AV71ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace ;
      private String AV75ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace ;
      private String AV79ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace ;
      private String AV83ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace ;
      private String AV87ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace ;
      private String AV94Select_GXI ;
      private String A300ContratoOcorrenciaNotificacao_Descricao ;
      private String A302ContratoOcorrenciaNotificacao_Protocolo ;
      private String A306ContratoOcorrenciaNotificacao_Docto ;
      private String lV63TFContratoOcorrenciaNotificacao_Descricao ;
      private String lV73TFContratoOcorrenciaNotificacao_Protocolo ;
      private String lV85TFContratoOcorrenciaNotificacao_Docto ;
      private String AV34Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratoOcorrenciaNotificacao_Codigo ;
      private DateTime aP1_InOutContratoOcorrenciaNotificacao_Data ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H007D2_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] H007D2_n306ContratoOcorrenciaNotificacao_Docto ;
      private String[] H007D2_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] H007D2_n304ContratoOcorrenciaNotificacao_Nome ;
      private int[] H007D2_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] H007D2_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] H007D2_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] H007D2_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] H007D2_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private String[] H007D2_A300ContratoOcorrenciaNotificacao_Descricao ;
      private short[] H007D2_A299ContratoOcorrenciaNotificacao_Prazo ;
      private DateTime[] H007D2_A298ContratoOcorrenciaNotificacao_Data ;
      private String[] H007D2_A77Contrato_Numero ;
      private int[] H007D2_A74Contrato_Codigo ;
      private int[] H007D2_A294ContratoOcorrencia_Codigo ;
      private int[] H007D2_A297ContratoOcorrenciaNotificacao_Codigo ;
      private long[] H007D3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36ContratoOcorrenciaNotificacao_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV40ContratoOcorrencia_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44Contrato_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48Contrato_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV52ContratoOcorrenciaNotificacao_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58ContratoOcorrenciaNotificacao_PrazoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62ContratoOcorrenciaNotificacao_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV66ContratoOcorrenciaNotificacao_CumpridoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV72ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV76ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV80ContratoOcorrenciaNotificacao_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV84ContratoOcorrenciaNotificacao_DoctoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV88DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratoocorrencianotificacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H007D2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV17ContratoOcorrenciaNotificacao_Data1 ,
                                             DateTime AV18ContratoOcorrenciaNotificacao_Data_To1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV19ContratoOcorrenciaNotificacao_Nome1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             DateTime AV23ContratoOcorrenciaNotificacao_Data2 ,
                                             DateTime AV24ContratoOcorrenciaNotificacao_Data_To2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV25ContratoOcorrenciaNotificacao_Nome2 ,
                                             bool AV26DynamicFiltersEnabled3 ,
                                             String AV27DynamicFiltersSelector3 ,
                                             DateTime AV29ContratoOcorrenciaNotificacao_Data3 ,
                                             DateTime AV30ContratoOcorrenciaNotificacao_Data_To3 ,
                                             short AV28DynamicFiltersOperator3 ,
                                             String AV31ContratoOcorrenciaNotificacao_Nome3 ,
                                             int AV37TFContratoOcorrenciaNotificacao_Codigo ,
                                             int AV38TFContratoOcorrenciaNotificacao_Codigo_To ,
                                             int AV41TFContratoOcorrencia_Codigo ,
                                             int AV42TFContratoOcorrencia_Codigo_To ,
                                             int AV45TFContrato_Codigo ,
                                             int AV46TFContrato_Codigo_To ,
                                             String AV50TFContrato_Numero_Sel ,
                                             String AV49TFContrato_Numero ,
                                             DateTime AV53TFContratoOcorrenciaNotificacao_Data ,
                                             DateTime AV54TFContratoOcorrenciaNotificacao_Data_To ,
                                             short AV59TFContratoOcorrenciaNotificacao_Prazo ,
                                             short AV60TFContratoOcorrenciaNotificacao_Prazo_To ,
                                             String AV64TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                             String AV63TFContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime AV67TFContratoOcorrenciaNotificacao_Cumprido ,
                                             DateTime AV68TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                             String AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                             String AV73TFContratoOcorrenciaNotificacao_Protocolo ,
                                             int AV77TFContratoOcorrenciaNotificacao_Responsavel ,
                                             int AV78TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                             String AV82TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                             String AV81TFContratoOcorrenciaNotificacao_Nome ,
                                             String AV86TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                             String AV85TFContratoOcorrenciaNotificacao_Docto ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             int A297ContratoOcorrenciaNotificacao_Codigo ,
                                             int A294ContratoOcorrencia_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                             String A306ContratoOcorrenciaNotificacao_Docto ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [41] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto, T2.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Descricao], T1.[ContratoOcorrenciaNotificacao_Prazo], T1.[ContratoOcorrenciaNotificacao_Data], T4.[Contrato_Numero], T3.[Contrato_Codigo], T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Codigo]";
         sFromString = " FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel]) INNER JOIN [ContratoOcorrencia] T3 WITH (NOLOCK) ON T3.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV17ContratoOcorrenciaNotificacao_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV17ContratoOcorrenciaNotificacao_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV17ContratoOcorrenciaNotificacao_Data1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV18ContratoOcorrenciaNotificacao_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV18ContratoOcorrenciaNotificacao_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV18ContratoOcorrenciaNotificacao_Data_To1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV19ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV19ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV19ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV19ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV23ContratoOcorrenciaNotificacao_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV23ContratoOcorrenciaNotificacao_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV23ContratoOcorrenciaNotificacao_Data2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV24ContratoOcorrenciaNotificacao_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV24ContratoOcorrenciaNotificacao_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV24ContratoOcorrenciaNotificacao_Data_To2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV25ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV25ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV25ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV25ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV29ContratoOcorrenciaNotificacao_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV29ContratoOcorrenciaNotificacao_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV29ContratoOcorrenciaNotificacao_Data3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV30ContratoOcorrenciaNotificacao_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV30ContratoOcorrenciaNotificacao_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV30ContratoOcorrenciaNotificacao_Data_To3)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV31ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV31ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV31ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV31ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV37TFContratoOcorrenciaNotificacao_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV37TFContratoOcorrenciaNotificacao_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV37TFContratoOcorrenciaNotificacao_Codigo)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV38TFContratoOcorrenciaNotificacao_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV38TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV38TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (0==AV41TFContratoOcorrencia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] >= @AV41TFContratoOcorrencia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] >= @AV41TFContratoOcorrencia_Codigo)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (0==AV42TFContratoOcorrencia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] <= @AV42TFContratoOcorrencia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] <= @AV42TFContratoOcorrencia_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (0==AV45TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Codigo] >= @AV45TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Codigo] >= @AV45TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (0==AV46TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Codigo] <= @AV46TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Codigo] <= @AV46TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] like @lV49TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] like @lV49TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] = @AV50TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] = @AV50TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV53TFContratoOcorrenciaNotificacao_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV53TFContratoOcorrenciaNotificacao_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV53TFContratoOcorrenciaNotificacao_Data)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV54TFContratoOcorrenciaNotificacao_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV54TFContratoOcorrenciaNotificacao_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV54TFContratoOcorrenciaNotificacao_Data_To)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! (0==AV59TFContratoOcorrenciaNotificacao_Prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV59TFContratoOcorrenciaNotificacao_Prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV59TFContratoOcorrenciaNotificacao_Prazo)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! (0==AV60TFContratoOcorrenciaNotificacao_Prazo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV60TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV60TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64TFContratoOcorrenciaNotificacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFContratoOcorrenciaNotificacao_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV63TFContratoOcorrenciaNotificacao_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV63TFContratoOcorrenciaNotificacao_Descricao)";
            }
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV64TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV64TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV67TFContratoOcorrenciaNotificacao_Cumprido) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV67TFContratoOcorrenciaNotificacao_Cumprido)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV67TFContratoOcorrenciaNotificacao_Cumprido)";
            }
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV68TFContratoOcorrenciaNotificacao_Cumprido_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV68TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV68TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFContratoOcorrenciaNotificacao_Protocolo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV73TFContratoOcorrenciaNotificacao_Protocolo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV73TFContratoOcorrenciaNotificacao_Protocolo)";
            }
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
         }
         else
         {
            GXv_int2[29] = 1;
         }
         if ( ! (0==AV77TFContratoOcorrenciaNotificacao_Responsavel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV77TFContratoOcorrenciaNotificacao_Responsavel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV77TFContratoOcorrenciaNotificacao_Responsavel)";
            }
         }
         else
         {
            GXv_int2[30] = 1;
         }
         if ( ! (0==AV78TFContratoOcorrenciaNotificacao_Responsavel_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV78TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV78TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV82TFContratoOcorrenciaNotificacao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81TFContratoOcorrenciaNotificacao_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV81TFContratoOcorrenciaNotificacao_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV81TFContratoOcorrenciaNotificacao_Nome)";
            }
         }
         else
         {
            GXv_int2[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV82TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] = @AV82TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV86TFContratoOcorrenciaNotificacao_Docto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85TFContratoOcorrenciaNotificacao_Docto)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV85TFContratoOcorrenciaNotificacao_Docto)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like @lV85TFContratoOcorrenciaNotificacao_Docto)";
            }
         }
         else
         {
            GXv_int2[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV86TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] = @AV86TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
         }
         else
         {
            GXv_int2[35] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Contrato_Codigo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Contrato_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Contrato_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Prazo]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Prazo] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Descricao]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Cumprido]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Cumprido] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Protocolo]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Protocolo] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Responsavel]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Responsavel] DESC";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Docto] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H007D3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV17ContratoOcorrenciaNotificacao_Data1 ,
                                             DateTime AV18ContratoOcorrenciaNotificacao_Data_To1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV19ContratoOcorrenciaNotificacao_Nome1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             DateTime AV23ContratoOcorrenciaNotificacao_Data2 ,
                                             DateTime AV24ContratoOcorrenciaNotificacao_Data_To2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV25ContratoOcorrenciaNotificacao_Nome2 ,
                                             bool AV26DynamicFiltersEnabled3 ,
                                             String AV27DynamicFiltersSelector3 ,
                                             DateTime AV29ContratoOcorrenciaNotificacao_Data3 ,
                                             DateTime AV30ContratoOcorrenciaNotificacao_Data_To3 ,
                                             short AV28DynamicFiltersOperator3 ,
                                             String AV31ContratoOcorrenciaNotificacao_Nome3 ,
                                             int AV37TFContratoOcorrenciaNotificacao_Codigo ,
                                             int AV38TFContratoOcorrenciaNotificacao_Codigo_To ,
                                             int AV41TFContratoOcorrencia_Codigo ,
                                             int AV42TFContratoOcorrencia_Codigo_To ,
                                             int AV45TFContrato_Codigo ,
                                             int AV46TFContrato_Codigo_To ,
                                             String AV50TFContrato_Numero_Sel ,
                                             String AV49TFContrato_Numero ,
                                             DateTime AV53TFContratoOcorrenciaNotificacao_Data ,
                                             DateTime AV54TFContratoOcorrenciaNotificacao_Data_To ,
                                             short AV59TFContratoOcorrenciaNotificacao_Prazo ,
                                             short AV60TFContratoOcorrenciaNotificacao_Prazo_To ,
                                             String AV64TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                             String AV63TFContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime AV67TFContratoOcorrenciaNotificacao_Cumprido ,
                                             DateTime AV68TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                             String AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                             String AV73TFContratoOcorrenciaNotificacao_Protocolo ,
                                             int AV77TFContratoOcorrenciaNotificacao_Responsavel ,
                                             int AV78TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                             String AV82TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                             String AV81TFContratoOcorrenciaNotificacao_Nome ,
                                             String AV86TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                             String AV85TFContratoOcorrenciaNotificacao_Docto ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             int A297ContratoOcorrenciaNotificacao_Codigo ,
                                             int A294ContratoOcorrencia_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                             String A306ContratoOcorrenciaNotificacao_Docto ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [36] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel]) INNER JOIN [ContratoOcorrencia] T2 WITH (NOLOCK) ON T2.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV17ContratoOcorrenciaNotificacao_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV17ContratoOcorrenciaNotificacao_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV17ContratoOcorrenciaNotificacao_Data1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV18ContratoOcorrenciaNotificacao_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV18ContratoOcorrenciaNotificacao_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV18ContratoOcorrenciaNotificacao_Data_To1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV19ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV19ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV19ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV19ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV23ContratoOcorrenciaNotificacao_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV23ContratoOcorrenciaNotificacao_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV23ContratoOcorrenciaNotificacao_Data2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV24ContratoOcorrenciaNotificacao_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV24ContratoOcorrenciaNotificacao_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV24ContratoOcorrenciaNotificacao_Data_To2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV25ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV25ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV25ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV25ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV29ContratoOcorrenciaNotificacao_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV29ContratoOcorrenciaNotificacao_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV29ContratoOcorrenciaNotificacao_Data3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV30ContratoOcorrenciaNotificacao_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV30ContratoOcorrenciaNotificacao_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV30ContratoOcorrenciaNotificacao_Data_To3)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV31ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV31ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV31ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV31ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV37TFContratoOcorrenciaNotificacao_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV37TFContratoOcorrenciaNotificacao_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV37TFContratoOcorrenciaNotificacao_Codigo)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV38TFContratoOcorrenciaNotificacao_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV38TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV38TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (0==AV41TFContratoOcorrencia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] >= @AV41TFContratoOcorrencia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] >= @AV41TFContratoOcorrencia_Codigo)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (0==AV42TFContratoOcorrencia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] <= @AV42TFContratoOcorrencia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] <= @AV42TFContratoOcorrencia_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (0==AV45TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Codigo] >= @AV45TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Codigo] >= @AV45TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (0==AV46TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Codigo] <= @AV46TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Codigo] <= @AV46TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV49TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] like @lV49TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV50TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] = @AV50TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV53TFContratoOcorrenciaNotificacao_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV53TFContratoOcorrenciaNotificacao_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV53TFContratoOcorrenciaNotificacao_Data)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV54TFContratoOcorrenciaNotificacao_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV54TFContratoOcorrenciaNotificacao_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV54TFContratoOcorrenciaNotificacao_Data_To)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! (0==AV59TFContratoOcorrenciaNotificacao_Prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV59TFContratoOcorrenciaNotificacao_Prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV59TFContratoOcorrenciaNotificacao_Prazo)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! (0==AV60TFContratoOcorrenciaNotificacao_Prazo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV60TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV60TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64TFContratoOcorrenciaNotificacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFContratoOcorrenciaNotificacao_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV63TFContratoOcorrenciaNotificacao_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV63TFContratoOcorrenciaNotificacao_Descricao)";
            }
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV64TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV64TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV67TFContratoOcorrenciaNotificacao_Cumprido) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV67TFContratoOcorrenciaNotificacao_Cumprido)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV67TFContratoOcorrenciaNotificacao_Cumprido)";
            }
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV68TFContratoOcorrenciaNotificacao_Cumprido_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV68TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV68TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFContratoOcorrenciaNotificacao_Protocolo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV73TFContratoOcorrenciaNotificacao_Protocolo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV73TFContratoOcorrenciaNotificacao_Protocolo)";
            }
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
         }
         else
         {
            GXv_int4[29] = 1;
         }
         if ( ! (0==AV77TFContratoOcorrenciaNotificacao_Responsavel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV77TFContratoOcorrenciaNotificacao_Responsavel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV77TFContratoOcorrenciaNotificacao_Responsavel)";
            }
         }
         else
         {
            GXv_int4[30] = 1;
         }
         if ( ! (0==AV78TFContratoOcorrenciaNotificacao_Responsavel_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV78TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV78TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
         }
         else
         {
            GXv_int4[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV82TFContratoOcorrenciaNotificacao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81TFContratoOcorrenciaNotificacao_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV81TFContratoOcorrenciaNotificacao_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV81TFContratoOcorrenciaNotificacao_Nome)";
            }
         }
         else
         {
            GXv_int4[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV82TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV82TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV86TFContratoOcorrenciaNotificacao_Docto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85TFContratoOcorrenciaNotificacao_Docto)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV85TFContratoOcorrenciaNotificacao_Docto)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV85TFContratoOcorrenciaNotificacao_Docto)";
            }
         }
         else
         {
            GXv_int4[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV86TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV86TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
         }
         else
         {
            GXv_int4[35] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 12 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H007D2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (DateTime)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (String)dynConstraints[50] , (int)dynConstraints[51] , (String)dynConstraints[52] , (short)dynConstraints[53] , (bool)dynConstraints[54] );
               case 1 :
                     return conditional_H007D3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (DateTime)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (String)dynConstraints[50] , (int)dynConstraints[51] , (String)dynConstraints[52] , (short)dynConstraints[53] , (bool)dynConstraints[54] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH007D2 ;
          prmH007D2 = new Object[] {
          new Object[] {"@AV17ContratoOcorrenciaNotificacao_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV18ContratoOcorrenciaNotificacao_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV19ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV23ContratoOcorrenciaNotificacao_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24ContratoOcorrenciaNotificacao_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV25ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV25ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV29ContratoOcorrenciaNotificacao_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV30ContratoOcorrenciaNotificacao_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV31ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV31ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV37TFContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV38TFContratoOcorrenciaNotificacao_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41TFContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42TFContratoOcorrencia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV45TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV49TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV50TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV53TFContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54TFContratoOcorrenciaNotificacao_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV59TFContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV60TFContratoOcorrenciaNotificacao_Prazo_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV63TFContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV64TFContratoOcorrenciaNotificacao_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV67TFContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV68TFContratoOcorrenciaNotificacao_Cumprido_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV73TFContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV77TFContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV78TFContratoOcorrenciaNotificacao_Responsavel_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV81TFContratoOcorrenciaNotificacao_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV82TFContratoOcorrenciaNotificacao_Nome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV85TFContratoOcorrenciaNotificacao_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV86TFContratoOcorrenciaNotificacao_Docto_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH007D3 ;
          prmH007D3 = new Object[] {
          new Object[] {"@AV17ContratoOcorrenciaNotificacao_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV18ContratoOcorrenciaNotificacao_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV19ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV23ContratoOcorrenciaNotificacao_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24ContratoOcorrenciaNotificacao_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV25ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV25ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV29ContratoOcorrenciaNotificacao_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV30ContratoOcorrenciaNotificacao_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV31ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV31ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV37TFContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV38TFContratoOcorrenciaNotificacao_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41TFContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42TFContratoOcorrencia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV45TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV49TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV50TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV53TFContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54TFContratoOcorrenciaNotificacao_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV59TFContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV60TFContratoOcorrenciaNotificacao_Prazo_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV63TFContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV64TFContratoOcorrenciaNotificacao_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV67TFContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV68TFContratoOcorrenciaNotificacao_Cumprido_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV73TFContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV74TFContratoOcorrenciaNotificacao_Protocolo_Sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV77TFContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV78TFContratoOcorrenciaNotificacao_Responsavel_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV81TFContratoOcorrenciaNotificacao_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV82TFContratoOcorrenciaNotificacao_Nome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV85TFContratoOcorrenciaNotificacao_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV86TFContratoOcorrenciaNotificacao_Docto_Sel",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H007D2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007D2,11,0,true,false )
             ,new CursorDef("H007D3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007D3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((short[]) buf[10])[0] = rslt.getShort(7) ;
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 20) ;
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                ((int[]) buf[14])[0] = rslt.getInt(11) ;
                ((int[]) buf[15])[0] = rslt.getInt(12) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[63]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[64]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[77]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[79]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                return;
       }
    }

 }

}
