/*
               File: SolicitacaoServico_BC
        Description: Solicitacao Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:12:12.95
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacaoservico_bc : GXHttpHandler, IGxSilentTrn
   {
      public solicitacaoservico_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public solicitacaoservico_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4169( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4169( ) ;
         standaloneModal( ) ;
         AddRow4169( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11412 */
            E11412 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_410( )
      {
         BeforeValidate4169( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4169( ) ;
            }
            else
            {
               CheckExtendedTable4169( ) ;
               if ( AnyError == 0 )
               {
                  ZM4169( 20) ;
                  ZM4169( 21) ;
                  ZM4169( 22) ;
                  ZM4169( 23) ;
                  ZM4169( 24) ;
                  ZM4169( 25) ;
                  ZM4169( 26) ;
                  ZM4169( 27) ;
                  ZM4169( 28) ;
                  ZM4169( 29) ;
                  ZM4169( 30) ;
                  ZM4169( 31) ;
               }
               CloseExtendedTableCursors4169( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12412( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7WWPContext) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7WWPContext) ;
         AV8TrnContext.FromXml(AV9WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV8TrnContext.gxTpr_Transactionname, AV26Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV27GXV1 = 1;
            while ( AV27GXV1 <= AV8TrnContext.gxTpr_Attributes.Count )
            {
               AV22TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV8TrnContext.gxTpr_Attributes.Item(AV27GXV1));
               if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_ServicoSS") == 0 )
               {
                  AV23Insert_ContagemResultado_ServicoSS = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_CntSrvCod") == 0 )
               {
                  AV11Insert_ContagemResultado_CntSrvCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_OSVinculada") == 0 )
               {
                  AV12Insert_ContagemResultado_OSVinculada = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_NaoCnfDmnCod") == 0 )
               {
                  AV13Insert_ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_LoteAceiteCod") == 0 )
               {
                  AV14Insert_ContagemResultado_LoteAceiteCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_ContratadaOrigemCod") == 0 )
               {
                  AV15Insert_ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_ContadorFSCod") == 0 )
               {
                  AV16Insert_ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_Responsavel") == 0 )
               {
                  AV17Insert_ContagemResultado_Responsavel = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_LiqLogCod") == 0 )
               {
                  AV18Insert_ContagemResultado_LiqLogCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "Modulo_Codigo") == 0 )
               {
                  AV19Insert_Modulo_Codigo = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_ContratadaCod") == 0 )
               {
                  AV20Insert_ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_SistemaCod") == 0 )
               {
                  AV21Insert_ContagemResultado_SistemaCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV27GXV1 = (int)(AV27GXV1+1);
            }
         }
      }

      protected void E11412( )
      {
         /* After Trn Routine */
      }

      protected void ZM4169( short GX_JID )
      {
         if ( ( GX_JID == 19 ) || ( GX_JID == 0 ) )
         {
            Z508ContagemResultado_Owner = A508ContagemResultado_Owner;
            Z494ContagemResultado_Descricao = A494ContagemResultado_Descricao;
            Z472ContagemResultado_DataEntrega = A472ContagemResultado_DataEntrega;
            Z912ContagemResultado_HoraEntrega = A912ContagemResultado_HoraEntrega;
            Z1351ContagemResultado_DataPrevista = A1351ContagemResultado_DataPrevista;
            Z1452ContagemResultado_SS = A1452ContagemResultado_SS;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z1584ContagemResultado_UOOwner = A1584ContagemResultado_UOOwner;
            Z1515ContagemResultado_Evento = A1515ContagemResultado_Evento;
            Z1583ContagemResultado_TipoRegistro = A1583ContagemResultado_TipoRegistro;
            Z1585ContagemResultado_Referencia = A1585ContagemResultado_Referencia;
            Z1586ContagemResultado_Restricoes = A1586ContagemResultado_Restricoes;
            Z1587ContagemResultado_PrioridadePrevista = A1587ContagemResultado_PrioridadePrevista;
            Z484ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
            Z471ContagemResultado_DataDmn = A471ContagemResultado_DataDmn;
            Z1044ContagemResultado_FncUsrCod = A1044ContagemResultado_FncUsrCod;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
            Z1350ContagemResultado_DataCadastro = A1350ContagemResultado_DataCadastro;
            Z1173ContagemResultado_OSManual = A1173ContagemResultado_OSManual;
            Z598ContagemResultado_Baseline = A598ContagemResultado_Baseline;
            Z485ContagemResultado_EhValidacao = A485ContagemResultado_EhValidacao;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z454ContagemResultado_ContadorFSCod = A454ContagemResultado_ContadorFSCod;
            Z890ContagemResultado_Responsavel = A890ContagemResultado_Responsavel;
            Z468ContagemResultado_NaoCnfDmnCod = A468ContagemResultado_NaoCnfDmnCod;
            Z490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
            Z805ContagemResultado_ContratadaOrigemCod = A805ContagemResultado_ContratadaOrigemCod;
            Z489ContagemResultado_SistemaCod = A489ContagemResultado_SistemaCod;
            Z597ContagemResultado_LoteAceiteCod = A597ContagemResultado_LoteAceiteCod;
            Z1636ContagemResultado_ServicoSS = A1636ContagemResultado_ServicoSS;
            Z1043ContagemResultado_LiqLogCod = A1043ContagemResultado_LiqLogCod;
            Z1553ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
            Z602ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
         }
         if ( ( GX_JID == 20 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 21 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 22 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 23 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 24 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 25 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 26 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 27 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 28 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 29 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 30 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 31 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -19 )
         {
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z508ContagemResultado_Owner = A508ContagemResultado_Owner;
            Z494ContagemResultado_Descricao = A494ContagemResultado_Descricao;
            Z514ContagemResultado_Observacao = A514ContagemResultado_Observacao;
            Z472ContagemResultado_DataEntrega = A472ContagemResultado_DataEntrega;
            Z912ContagemResultado_HoraEntrega = A912ContagemResultado_HoraEntrega;
            Z1351ContagemResultado_DataPrevista = A1351ContagemResultado_DataPrevista;
            Z1452ContagemResultado_SS = A1452ContagemResultado_SS;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z1584ContagemResultado_UOOwner = A1584ContagemResultado_UOOwner;
            Z1515ContagemResultado_Evento = A1515ContagemResultado_Evento;
            Z1583ContagemResultado_TipoRegistro = A1583ContagemResultado_TipoRegistro;
            Z1585ContagemResultado_Referencia = A1585ContagemResultado_Referencia;
            Z1586ContagemResultado_Restricoes = A1586ContagemResultado_Restricoes;
            Z1587ContagemResultado_PrioridadePrevista = A1587ContagemResultado_PrioridadePrevista;
            Z484ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
            Z471ContagemResultado_DataDmn = A471ContagemResultado_DataDmn;
            Z1044ContagemResultado_FncUsrCod = A1044ContagemResultado_FncUsrCod;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
            Z1350ContagemResultado_DataCadastro = A1350ContagemResultado_DataCadastro;
            Z1173ContagemResultado_OSManual = A1173ContagemResultado_OSManual;
            Z598ContagemResultado_Baseline = A598ContagemResultado_Baseline;
            Z485ContagemResultado_EhValidacao = A485ContagemResultado_EhValidacao;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z454ContagemResultado_ContadorFSCod = A454ContagemResultado_ContadorFSCod;
            Z890ContagemResultado_Responsavel = A890ContagemResultado_Responsavel;
            Z468ContagemResultado_NaoCnfDmnCod = A468ContagemResultado_NaoCnfDmnCod;
            Z490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
            Z805ContagemResultado_ContratadaOrigemCod = A805ContagemResultado_ContratadaOrigemCod;
            Z489ContagemResultado_SistemaCod = A489ContagemResultado_SistemaCod;
            Z597ContagemResultado_LoteAceiteCod = A597ContagemResultado_LoteAceiteCod;
            Z1636ContagemResultado_ServicoSS = A1636ContagemResultado_ServicoSS;
            Z1043ContagemResultado_LiqLogCod = A1043ContagemResultado_LiqLogCod;
            Z1553ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
            Z602ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
         }
      }

      protected void standaloneNotModal( )
      {
         AV26Pgmname = "SolicitacaoServico_BC";
         Gx_date = DateTimeUtil.Today( context);
         Gx_BScreen = 0;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A598ContagemResultado_Baseline) && ( Gx_BScreen == 0 ) )
         {
            A598ContagemResultado_Baseline = false;
            n598ContagemResultado_Baseline = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A485ContagemResultado_EhValidacao) && ( Gx_BScreen == 0 ) )
         {
            A485ContagemResultado_EhValidacao = false;
            n485ContagemResultado_EhValidacao = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A1350ContagemResultado_DataCadastro) && ( Gx_BScreen == 0 ) )
         {
            A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
            n1350ContagemResultado_DataCadastro = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1515ContagemResultado_Evento) && ( Gx_BScreen == 0 ) )
         {
            A1515ContagemResultado_Evento = 1;
            n1515ContagemResultado_Evento = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1452ContagemResultado_SS) && ( Gx_BScreen == 0 ) )
         {
            A1452ContagemResultado_SS = 0;
            n1452ContagemResultado_SS = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1583ContagemResultado_TipoRegistro) && ( Gx_BScreen == 0 ) )
         {
            A1583ContagemResultado_TipoRegistro = 2;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A484ContagemResultado_StatusDmn)) && ( Gx_BScreen == 0 ) )
         {
            A484ContagemResultado_StatusDmn = "S";
            n484ContagemResultado_StatusDmn = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A508ContagemResultado_Owner) && ( Gx_BScreen == 0 ) )
         {
            A508ContagemResultado_Owner = AV7WWPContext.gxTpr_Userid;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A471ContagemResultado_DataDmn) && ( Gx_BScreen == 0 ) )
         {
            A471ContagemResultado_DataDmn = Gx_date;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load4169( )
      {
         /* Using cursor BC004116 */
         pr_default.execute(14, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound69 = 1;
            A508ContagemResultado_Owner = BC004116_A508ContagemResultado_Owner[0];
            A494ContagemResultado_Descricao = BC004116_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = BC004116_n494ContagemResultado_Descricao[0];
            A514ContagemResultado_Observacao = BC004116_A514ContagemResultado_Observacao[0];
            n514ContagemResultado_Observacao = BC004116_n514ContagemResultado_Observacao[0];
            A472ContagemResultado_DataEntrega = BC004116_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = BC004116_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = BC004116_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = BC004116_n912ContagemResultado_HoraEntrega[0];
            A1351ContagemResultado_DataPrevista = BC004116_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = BC004116_n1351ContagemResultado_DataPrevista[0];
            A1452ContagemResultado_SS = BC004116_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = BC004116_n1452ContagemResultado_SS[0];
            A493ContagemResultado_DemandaFM = BC004116_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC004116_n493ContagemResultado_DemandaFM[0];
            A1584ContagemResultado_UOOwner = BC004116_A1584ContagemResultado_UOOwner[0];
            n1584ContagemResultado_UOOwner = BC004116_n1584ContagemResultado_UOOwner[0];
            A1515ContagemResultado_Evento = BC004116_A1515ContagemResultado_Evento[0];
            n1515ContagemResultado_Evento = BC004116_n1515ContagemResultado_Evento[0];
            A1583ContagemResultado_TipoRegistro = BC004116_A1583ContagemResultado_TipoRegistro[0];
            A1585ContagemResultado_Referencia = BC004116_A1585ContagemResultado_Referencia[0];
            n1585ContagemResultado_Referencia = BC004116_n1585ContagemResultado_Referencia[0];
            A1586ContagemResultado_Restricoes = BC004116_A1586ContagemResultado_Restricoes[0];
            n1586ContagemResultado_Restricoes = BC004116_n1586ContagemResultado_Restricoes[0];
            A1587ContagemResultado_PrioridadePrevista = BC004116_A1587ContagemResultado_PrioridadePrevista[0];
            n1587ContagemResultado_PrioridadePrevista = BC004116_n1587ContagemResultado_PrioridadePrevista[0];
            A484ContagemResultado_StatusDmn = BC004116_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = BC004116_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = BC004116_A471ContagemResultado_DataDmn[0];
            A1044ContagemResultado_FncUsrCod = BC004116_A1044ContagemResultado_FncUsrCod[0];
            n1044ContagemResultado_FncUsrCod = BC004116_n1044ContagemResultado_FncUsrCod[0];
            A1227ContagemResultado_PrazoInicialDias = BC004116_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = BC004116_n1227ContagemResultado_PrazoInicialDias[0];
            A1350ContagemResultado_DataCadastro = BC004116_A1350ContagemResultado_DataCadastro[0];
            n1350ContagemResultado_DataCadastro = BC004116_n1350ContagemResultado_DataCadastro[0];
            A1173ContagemResultado_OSManual = BC004116_A1173ContagemResultado_OSManual[0];
            n1173ContagemResultado_OSManual = BC004116_n1173ContagemResultado_OSManual[0];
            A598ContagemResultado_Baseline = BC004116_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = BC004116_n598ContagemResultado_Baseline[0];
            A485ContagemResultado_EhValidacao = BC004116_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = BC004116_n485ContagemResultado_EhValidacao[0];
            A146Modulo_Codigo = BC004116_A146Modulo_Codigo[0];
            n146Modulo_Codigo = BC004116_n146Modulo_Codigo[0];
            A454ContagemResultado_ContadorFSCod = BC004116_A454ContagemResultado_ContadorFSCod[0];
            n454ContagemResultado_ContadorFSCod = BC004116_n454ContagemResultado_ContadorFSCod[0];
            A890ContagemResultado_Responsavel = BC004116_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = BC004116_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = BC004116_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = BC004116_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = BC004116_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC004116_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = BC004116_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = BC004116_n805ContagemResultado_ContratadaOrigemCod[0];
            A489ContagemResultado_SistemaCod = BC004116_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = BC004116_n489ContagemResultado_SistemaCod[0];
            A597ContagemResultado_LoteAceiteCod = BC004116_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = BC004116_n597ContagemResultado_LoteAceiteCod[0];
            A1636ContagemResultado_ServicoSS = BC004116_A1636ContagemResultado_ServicoSS[0];
            n1636ContagemResultado_ServicoSS = BC004116_n1636ContagemResultado_ServicoSS[0];
            A1043ContagemResultado_LiqLogCod = BC004116_A1043ContagemResultado_LiqLogCod[0];
            n1043ContagemResultado_LiqLogCod = BC004116_n1043ContagemResultado_LiqLogCod[0];
            A1553ContagemResultado_CntSrvCod = BC004116_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC004116_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = BC004116_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = BC004116_n602ContagemResultado_OSVinculada[0];
            ZM4169( -19) ;
         }
         pr_default.close(14);
         OnLoadActions4169( ) ;
      }

      protected void OnLoadActions4169( )
      {
      }

      protected void CheckExtendedTable4169( )
      {
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A472ContagemResultado_DataEntrega) || ( A472ContagemResultado_DataEntrega >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data para Entrega fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A1351ContagemResultado_DataPrevista) || ( A1351ContagemResultado_DataPrevista >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data Prevista fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC004112 */
         pr_default.execute(10, new Object[] {n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS});
         if ( (pr_default.getStatus(10) == 101) )
         {
            if ( ! ( (0==A1636ContagemResultado_ServicoSS) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Servico'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_SERVICOSS");
               AnyError = 1;
            }
         }
         pr_default.close(10);
         if ( ! ( ( A1515ContagemResultado_Evento == 1 ) || ( A1515ContagemResultado_Evento == 2 ) || ( A1515ContagemResultado_Evento == 3 ) || ( A1515ContagemResultado_Evento == 4 ) || (0==A1515ContagemResultado_Evento) ) )
         {
            GX_msglist.addItem("Campo Evento fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A1583ContagemResultado_TipoRegistro == 1 ) || ( A1583ContagemResultado_TipoRegistro == 2 ) || ( A1583ContagemResultado_TipoRegistro == 3 ) ) )
         {
            GX_msglist.addItem("Campo Contagem Resultado_Tipo Registro fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "B") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "S") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "E") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "A") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "D") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "H") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "P") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "L") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "N") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "J") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "I") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "T") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "Q") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "G") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "M") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "U") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A484ContagemResultado_StatusDmn)) ) )
         {
            GX_msglist.addItem("Campo Status fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A471ContagemResultado_DataDmn) || ( A471ContagemResultado_DataDmn >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data da OS fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC004114 */
         pr_default.execute(12, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CNTSRVCOD");
               AnyError = 1;
            }
         }
         pr_default.close(12);
         /* Using cursor BC004115 */
         pr_default.execute(13, new Object[] {n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada});
         if ( (pr_default.getStatus(13) == 101) )
         {
            if ( ! ( (0==A602ContagemResultado_OSVinculada) ) )
            {
               GX_msglist.addItem("N�o existe 'OS Vinculada'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_OSVINCULADA");
               AnyError = 1;
            }
         }
         pr_default.close(13);
         /* Using cursor BC00417 */
         pr_default.execute(5, new Object[] {n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A468ContagemResultado_NaoCnfDmnCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Nao Conformidade da Demanda'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_NAOCNFDMNCOD");
               AnyError = 1;
            }
         }
         pr_default.close(5);
         /* Using cursor BC004111 */
         pr_default.execute(9, new Object[] {n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A597ContagemResultado_LoteAceiteCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resultado_Lote'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_LOTEACEITECOD");
               AnyError = 1;
            }
         }
         pr_default.close(9);
         /* Using cursor BC00419 */
         pr_default.execute(7, new Object[] {n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A805ContagemResultado_ContratadaOrigemCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resutlado_Contratada Origem'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD");
               AnyError = 1;
            }
         }
         pr_default.close(7);
         /* Using cursor BC00415 */
         pr_default.execute(3, new Object[] {n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A454ContagemResultado_ContadorFSCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Usuario Contador Fab. de Soft'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTADORFSCOD");
               AnyError = 1;
            }
         }
         pr_default.close(3);
         /* Using cursor BC00416 */
         pr_default.execute(4, new Object[] {n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A890ContagemResultado_Responsavel) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Usuario Responsavel'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_RESPONSAVEL");
               AnyError = 1;
            }
         }
         pr_default.close(4);
         /* Using cursor BC004113 */
         pr_default.execute(11, new Object[] {n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            if ( ! ( (0==A1043ContagemResultado_LiqLogCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resultado_Contagem Resultado Liq Log'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_LIQLOGCOD");
               AnyError = 1;
            }
         }
         pr_default.close(11);
         /* Using cursor BC00414 */
         pr_default.execute(2, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A146Modulo_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "MODULO_CODIGO");
               AnyError = 1;
            }
         }
         pr_default.close(2);
         /* Using cursor BC00418 */
         pr_default.execute(6, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTRATADACOD");
               AnyError = 1;
            }
         }
         pr_default.close(6);
         /* Using cursor BC004110 */
         pr_default.execute(8, new Object[] {n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A489ContagemResultado_SistemaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Sistema'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_SISTEMACOD");
               AnyError = 1;
            }
         }
         pr_default.close(8);
         if ( ! ( (DateTime.MinValue==A1350ContagemResultado_DataCadastro) || ( A1350ContagemResultado_DataCadastro >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data de Cadastro fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors4169( )
      {
         pr_default.close(10);
         pr_default.close(12);
         pr_default.close(13);
         pr_default.close(5);
         pr_default.close(9);
         pr_default.close(7);
         pr_default.close(3);
         pr_default.close(4);
         pr_default.close(11);
         pr_default.close(2);
         pr_default.close(6);
         pr_default.close(8);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4169( )
      {
         /* Using cursor BC004117 */
         pr_default.execute(15, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound69 = 1;
         }
         else
         {
            RcdFound69 = 0;
         }
         pr_default.close(15);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00413 */
         pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4169( 19) ;
            RcdFound69 = 1;
            A456ContagemResultado_Codigo = BC00413_A456ContagemResultado_Codigo[0];
            A508ContagemResultado_Owner = BC00413_A508ContagemResultado_Owner[0];
            A494ContagemResultado_Descricao = BC00413_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = BC00413_n494ContagemResultado_Descricao[0];
            A514ContagemResultado_Observacao = BC00413_A514ContagemResultado_Observacao[0];
            n514ContagemResultado_Observacao = BC00413_n514ContagemResultado_Observacao[0];
            A472ContagemResultado_DataEntrega = BC00413_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = BC00413_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = BC00413_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = BC00413_n912ContagemResultado_HoraEntrega[0];
            A1351ContagemResultado_DataPrevista = BC00413_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = BC00413_n1351ContagemResultado_DataPrevista[0];
            A1452ContagemResultado_SS = BC00413_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = BC00413_n1452ContagemResultado_SS[0];
            A493ContagemResultado_DemandaFM = BC00413_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC00413_n493ContagemResultado_DemandaFM[0];
            A1584ContagemResultado_UOOwner = BC00413_A1584ContagemResultado_UOOwner[0];
            n1584ContagemResultado_UOOwner = BC00413_n1584ContagemResultado_UOOwner[0];
            A1515ContagemResultado_Evento = BC00413_A1515ContagemResultado_Evento[0];
            n1515ContagemResultado_Evento = BC00413_n1515ContagemResultado_Evento[0];
            A1583ContagemResultado_TipoRegistro = BC00413_A1583ContagemResultado_TipoRegistro[0];
            A1585ContagemResultado_Referencia = BC00413_A1585ContagemResultado_Referencia[0];
            n1585ContagemResultado_Referencia = BC00413_n1585ContagemResultado_Referencia[0];
            A1586ContagemResultado_Restricoes = BC00413_A1586ContagemResultado_Restricoes[0];
            n1586ContagemResultado_Restricoes = BC00413_n1586ContagemResultado_Restricoes[0];
            A1587ContagemResultado_PrioridadePrevista = BC00413_A1587ContagemResultado_PrioridadePrevista[0];
            n1587ContagemResultado_PrioridadePrevista = BC00413_n1587ContagemResultado_PrioridadePrevista[0];
            A484ContagemResultado_StatusDmn = BC00413_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = BC00413_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = BC00413_A471ContagemResultado_DataDmn[0];
            A1044ContagemResultado_FncUsrCod = BC00413_A1044ContagemResultado_FncUsrCod[0];
            n1044ContagemResultado_FncUsrCod = BC00413_n1044ContagemResultado_FncUsrCod[0];
            A1227ContagemResultado_PrazoInicialDias = BC00413_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = BC00413_n1227ContagemResultado_PrazoInicialDias[0];
            A1350ContagemResultado_DataCadastro = BC00413_A1350ContagemResultado_DataCadastro[0];
            n1350ContagemResultado_DataCadastro = BC00413_n1350ContagemResultado_DataCadastro[0];
            A1173ContagemResultado_OSManual = BC00413_A1173ContagemResultado_OSManual[0];
            n1173ContagemResultado_OSManual = BC00413_n1173ContagemResultado_OSManual[0];
            A598ContagemResultado_Baseline = BC00413_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = BC00413_n598ContagemResultado_Baseline[0];
            A485ContagemResultado_EhValidacao = BC00413_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = BC00413_n485ContagemResultado_EhValidacao[0];
            A146Modulo_Codigo = BC00413_A146Modulo_Codigo[0];
            n146Modulo_Codigo = BC00413_n146Modulo_Codigo[0];
            A454ContagemResultado_ContadorFSCod = BC00413_A454ContagemResultado_ContadorFSCod[0];
            n454ContagemResultado_ContadorFSCod = BC00413_n454ContagemResultado_ContadorFSCod[0];
            A890ContagemResultado_Responsavel = BC00413_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = BC00413_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = BC00413_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = BC00413_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = BC00413_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC00413_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = BC00413_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = BC00413_n805ContagemResultado_ContratadaOrigemCod[0];
            A489ContagemResultado_SistemaCod = BC00413_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = BC00413_n489ContagemResultado_SistemaCod[0];
            A597ContagemResultado_LoteAceiteCod = BC00413_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = BC00413_n597ContagemResultado_LoteAceiteCod[0];
            A1636ContagemResultado_ServicoSS = BC00413_A1636ContagemResultado_ServicoSS[0];
            n1636ContagemResultado_ServicoSS = BC00413_n1636ContagemResultado_ServicoSS[0];
            A1043ContagemResultado_LiqLogCod = BC00413_A1043ContagemResultado_LiqLogCod[0];
            n1043ContagemResultado_LiqLogCod = BC00413_n1043ContagemResultado_LiqLogCod[0];
            A1553ContagemResultado_CntSrvCod = BC00413_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC00413_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = BC00413_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = BC00413_n602ContagemResultado_OSVinculada[0];
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            sMode69 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4169( ) ;
            if ( AnyError == 1 )
            {
               RcdFound69 = 0;
               InitializeNonKey4169( ) ;
            }
            Gx_mode = sMode69;
         }
         else
         {
            RcdFound69 = 0;
            InitializeNonKey4169( ) ;
            sMode69 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode69;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4169( ) ;
         if ( RcdFound69 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_410( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4169( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00412 */
            pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultado"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z508ContagemResultado_Owner != BC00412_A508ContagemResultado_Owner[0] ) || ( StringUtil.StrCmp(Z494ContagemResultado_Descricao, BC00412_A494ContagemResultado_Descricao[0]) != 0 ) || ( Z472ContagemResultado_DataEntrega != BC00412_A472ContagemResultado_DataEntrega[0] ) || ( Z912ContagemResultado_HoraEntrega != BC00412_A912ContagemResultado_HoraEntrega[0] ) || ( Z1351ContagemResultado_DataPrevista != BC00412_A1351ContagemResultado_DataPrevista[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1452ContagemResultado_SS != BC00412_A1452ContagemResultado_SS[0] ) || ( StringUtil.StrCmp(Z493ContagemResultado_DemandaFM, BC00412_A493ContagemResultado_DemandaFM[0]) != 0 ) || ( Z1584ContagemResultado_UOOwner != BC00412_A1584ContagemResultado_UOOwner[0] ) || ( Z1515ContagemResultado_Evento != BC00412_A1515ContagemResultado_Evento[0] ) || ( Z1583ContagemResultado_TipoRegistro != BC00412_A1583ContagemResultado_TipoRegistro[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1585ContagemResultado_Referencia, BC00412_A1585ContagemResultado_Referencia[0]) != 0 ) || ( StringUtil.StrCmp(Z1586ContagemResultado_Restricoes, BC00412_A1586ContagemResultado_Restricoes[0]) != 0 ) || ( StringUtil.StrCmp(Z1587ContagemResultado_PrioridadePrevista, BC00412_A1587ContagemResultado_PrioridadePrevista[0]) != 0 ) || ( StringUtil.StrCmp(Z484ContagemResultado_StatusDmn, BC00412_A484ContagemResultado_StatusDmn[0]) != 0 ) || ( Z471ContagemResultado_DataDmn != BC00412_A471ContagemResultado_DataDmn[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1044ContagemResultado_FncUsrCod != BC00412_A1044ContagemResultado_FncUsrCod[0] ) || ( Z1227ContagemResultado_PrazoInicialDias != BC00412_A1227ContagemResultado_PrazoInicialDias[0] ) || ( Z1350ContagemResultado_DataCadastro != BC00412_A1350ContagemResultado_DataCadastro[0] ) || ( Z1173ContagemResultado_OSManual != BC00412_A1173ContagemResultado_OSManual[0] ) || ( Z598ContagemResultado_Baseline != BC00412_A598ContagemResultado_Baseline[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z485ContagemResultado_EhValidacao != BC00412_A485ContagemResultado_EhValidacao[0] ) || ( Z146Modulo_Codigo != BC00412_A146Modulo_Codigo[0] ) || ( Z454ContagemResultado_ContadorFSCod != BC00412_A454ContagemResultado_ContadorFSCod[0] ) || ( Z890ContagemResultado_Responsavel != BC00412_A890ContagemResultado_Responsavel[0] ) || ( Z468ContagemResultado_NaoCnfDmnCod != BC00412_A468ContagemResultado_NaoCnfDmnCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z490ContagemResultado_ContratadaCod != BC00412_A490ContagemResultado_ContratadaCod[0] ) || ( Z805ContagemResultado_ContratadaOrigemCod != BC00412_A805ContagemResultado_ContratadaOrigemCod[0] ) || ( Z489ContagemResultado_SistemaCod != BC00412_A489ContagemResultado_SistemaCod[0] ) || ( Z597ContagemResultado_LoteAceiteCod != BC00412_A597ContagemResultado_LoteAceiteCod[0] ) || ( Z1636ContagemResultado_ServicoSS != BC00412_A1636ContagemResultado_ServicoSS[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1043ContagemResultado_LiqLogCod != BC00412_A1043ContagemResultado_LiqLogCod[0] ) || ( Z1553ContagemResultado_CntSrvCod != BC00412_A1553ContagemResultado_CntSrvCod[0] ) || ( Z602ContagemResultado_OSVinculada != BC00412_A602ContagemResultado_OSVinculada[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultado"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4169( )
      {
         BeforeValidate4169( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4169( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4169( 0) ;
            CheckOptimisticConcurrency4169( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4169( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4169( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004118 */
                     pr_default.execute(16, new Object[] {A508ContagemResultado_Owner, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, n1584ContagemResultado_UOOwner, A1584ContagemResultado_UOOwner, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, A1583ContagemResultado_TipoRegistro, n1585ContagemResultado_Referencia, A1585ContagemResultado_Referencia, n1586ContagemResultado_Restricoes, A1586ContagemResultado_Restricoes, n1587ContagemResultado_PrioridadePrevista, A1587ContagemResultado_PrioridadePrevista, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A471ContagemResultado_DataDmn, n1044ContagemResultado_FncUsrCod, A1044ContagemResultado_FncUsrCod, n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1173ContagemResultado_OSManual, A1173ContagemResultado_OSManual, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n146Modulo_Codigo, A146Modulo_Codigo, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada});
                     A456ContagemResultado_Codigo = BC004118_A456ContagemResultado_Codigo[0];
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4169( ) ;
            }
            EndLevel4169( ) ;
         }
         CloseExtendedTableCursors4169( ) ;
      }

      protected void Update4169( )
      {
         BeforeValidate4169( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4169( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4169( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4169( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4169( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004119 */
                     pr_default.execute(17, new Object[] {A508ContagemResultado_Owner, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, n1584ContagemResultado_UOOwner, A1584ContagemResultado_UOOwner, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, A1583ContagemResultado_TipoRegistro, n1585ContagemResultado_Referencia, A1585ContagemResultado_Referencia, n1586ContagemResultado_Restricoes, A1586ContagemResultado_Restricoes, n1587ContagemResultado_PrioridadePrevista, A1587ContagemResultado_PrioridadePrevista, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A471ContagemResultado_DataDmn, n1044ContagemResultado_FncUsrCod, A1044ContagemResultado_FncUsrCod, n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1173ContagemResultado_OSManual, A1173ContagemResultado_OSManual, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n146Modulo_Codigo, A146Modulo_Codigo, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, A456ContagemResultado_Codigo});
                     pr_default.close(17);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                     if ( (pr_default.getStatus(17) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultado"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4169( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4169( ) ;
         }
         CloseExtendedTableCursors4169( ) ;
      }

      protected void DeferredUpdate4169( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4169( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4169( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4169( ) ;
            AfterConfirm4169( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4169( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004120 */
                  pr_default.execute(18, new Object[] {A456ContagemResultado_Codigo});
                  pr_default.close(18);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode69 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4169( ) ;
         Gx_mode = sMode69;
      }

      protected void OnDeleteControls4169( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor BC004121 */
            pr_default.execute(19, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(19);
            /* Using cursor BC004122 */
            pr_default.execute(20, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Nao Cnf"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
            /* Using cursor BC004123 */
            pr_default.execute(21, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Requisito"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor BC004124 */
            pr_default.execute(22, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado QA"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
            /* Using cursor BC004125 */
            pr_default.execute(23, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Chck Lst"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor BC004126 */
            pr_default.execute(24, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Chck Lst Log"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor BC004127 */
            pr_default.execute(25, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Artefatos das OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
            /* Using cursor BC004128 */
            pr_default.execute(26, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Proposta"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
            /* Using cursor BC004129 */
            pr_default.execute(27, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(27) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Execucao"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(27);
            /* Using cursor BC004130 */
            pr_default.execute(28, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(28) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(28);
            /* Using cursor BC004131 */
            pr_default.execute(29, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(29) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Nota da OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(29);
            /* Using cursor BC004132 */
            pr_default.execute(30, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(30) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Indicadores"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(30);
            /* Using cursor BC004133 */
            pr_default.execute(31, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(31) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Incidentes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(31);
            /* Using cursor BC004134 */
            pr_default.execute(32, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(32) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Agenda Atendimento"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(32);
            /* Using cursor BC004135 */
            pr_default.execute(33, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Log de a��o dos Respons�veis"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(33);
            /* Using cursor BC004136 */
            pr_default.execute(34, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Requisitos de Neg�cio da Solicita��o de Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(34);
            /* Using cursor BC004137 */
            pr_default.execute(35, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(35) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Evidencias"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(35);
            /* Using cursor BC004138 */
            pr_default.execute(36, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(36) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Demanda"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(36);
            /* Using cursor BC004139 */
            pr_default.execute(37, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(37) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Erros nas confer�ncias de contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(37);
            /* Using cursor BC004140 */
            pr_default.execute(38, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(38) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(38);
         }
      }

      protected void EndLevel4169( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4169( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4169( )
      {
         /* Scan By routine */
         /* Using cursor BC004141 */
         pr_default.execute(39, new Object[] {A456ContagemResultado_Codigo});
         RcdFound69 = 0;
         if ( (pr_default.getStatus(39) != 101) )
         {
            RcdFound69 = 1;
            A456ContagemResultado_Codigo = BC004141_A456ContagemResultado_Codigo[0];
            A508ContagemResultado_Owner = BC004141_A508ContagemResultado_Owner[0];
            A494ContagemResultado_Descricao = BC004141_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = BC004141_n494ContagemResultado_Descricao[0];
            A514ContagemResultado_Observacao = BC004141_A514ContagemResultado_Observacao[0];
            n514ContagemResultado_Observacao = BC004141_n514ContagemResultado_Observacao[0];
            A472ContagemResultado_DataEntrega = BC004141_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = BC004141_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = BC004141_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = BC004141_n912ContagemResultado_HoraEntrega[0];
            A1351ContagemResultado_DataPrevista = BC004141_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = BC004141_n1351ContagemResultado_DataPrevista[0];
            A1452ContagemResultado_SS = BC004141_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = BC004141_n1452ContagemResultado_SS[0];
            A493ContagemResultado_DemandaFM = BC004141_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC004141_n493ContagemResultado_DemandaFM[0];
            A1584ContagemResultado_UOOwner = BC004141_A1584ContagemResultado_UOOwner[0];
            n1584ContagemResultado_UOOwner = BC004141_n1584ContagemResultado_UOOwner[0];
            A1515ContagemResultado_Evento = BC004141_A1515ContagemResultado_Evento[0];
            n1515ContagemResultado_Evento = BC004141_n1515ContagemResultado_Evento[0];
            A1583ContagemResultado_TipoRegistro = BC004141_A1583ContagemResultado_TipoRegistro[0];
            A1585ContagemResultado_Referencia = BC004141_A1585ContagemResultado_Referencia[0];
            n1585ContagemResultado_Referencia = BC004141_n1585ContagemResultado_Referencia[0];
            A1586ContagemResultado_Restricoes = BC004141_A1586ContagemResultado_Restricoes[0];
            n1586ContagemResultado_Restricoes = BC004141_n1586ContagemResultado_Restricoes[0];
            A1587ContagemResultado_PrioridadePrevista = BC004141_A1587ContagemResultado_PrioridadePrevista[0];
            n1587ContagemResultado_PrioridadePrevista = BC004141_n1587ContagemResultado_PrioridadePrevista[0];
            A484ContagemResultado_StatusDmn = BC004141_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = BC004141_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = BC004141_A471ContagemResultado_DataDmn[0];
            A1044ContagemResultado_FncUsrCod = BC004141_A1044ContagemResultado_FncUsrCod[0];
            n1044ContagemResultado_FncUsrCod = BC004141_n1044ContagemResultado_FncUsrCod[0];
            A1227ContagemResultado_PrazoInicialDias = BC004141_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = BC004141_n1227ContagemResultado_PrazoInicialDias[0];
            A1350ContagemResultado_DataCadastro = BC004141_A1350ContagemResultado_DataCadastro[0];
            n1350ContagemResultado_DataCadastro = BC004141_n1350ContagemResultado_DataCadastro[0];
            A1173ContagemResultado_OSManual = BC004141_A1173ContagemResultado_OSManual[0];
            n1173ContagemResultado_OSManual = BC004141_n1173ContagemResultado_OSManual[0];
            A598ContagemResultado_Baseline = BC004141_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = BC004141_n598ContagemResultado_Baseline[0];
            A485ContagemResultado_EhValidacao = BC004141_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = BC004141_n485ContagemResultado_EhValidacao[0];
            A146Modulo_Codigo = BC004141_A146Modulo_Codigo[0];
            n146Modulo_Codigo = BC004141_n146Modulo_Codigo[0];
            A454ContagemResultado_ContadorFSCod = BC004141_A454ContagemResultado_ContadorFSCod[0];
            n454ContagemResultado_ContadorFSCod = BC004141_n454ContagemResultado_ContadorFSCod[0];
            A890ContagemResultado_Responsavel = BC004141_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = BC004141_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = BC004141_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = BC004141_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = BC004141_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC004141_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = BC004141_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = BC004141_n805ContagemResultado_ContratadaOrigemCod[0];
            A489ContagemResultado_SistemaCod = BC004141_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = BC004141_n489ContagemResultado_SistemaCod[0];
            A597ContagemResultado_LoteAceiteCod = BC004141_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = BC004141_n597ContagemResultado_LoteAceiteCod[0];
            A1636ContagemResultado_ServicoSS = BC004141_A1636ContagemResultado_ServicoSS[0];
            n1636ContagemResultado_ServicoSS = BC004141_n1636ContagemResultado_ServicoSS[0];
            A1043ContagemResultado_LiqLogCod = BC004141_A1043ContagemResultado_LiqLogCod[0];
            n1043ContagemResultado_LiqLogCod = BC004141_n1043ContagemResultado_LiqLogCod[0];
            A1553ContagemResultado_CntSrvCod = BC004141_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC004141_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = BC004141_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = BC004141_n602ContagemResultado_OSVinculada[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4169( )
      {
         /* Scan next routine */
         pr_default.readNext(39);
         RcdFound69 = 0;
         ScanKeyLoad4169( ) ;
      }

      protected void ScanKeyLoad4169( )
      {
         sMode69 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(39) != 101) )
         {
            RcdFound69 = 1;
            A456ContagemResultado_Codigo = BC004141_A456ContagemResultado_Codigo[0];
            A508ContagemResultado_Owner = BC004141_A508ContagemResultado_Owner[0];
            A494ContagemResultado_Descricao = BC004141_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = BC004141_n494ContagemResultado_Descricao[0];
            A514ContagemResultado_Observacao = BC004141_A514ContagemResultado_Observacao[0];
            n514ContagemResultado_Observacao = BC004141_n514ContagemResultado_Observacao[0];
            A472ContagemResultado_DataEntrega = BC004141_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = BC004141_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = BC004141_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = BC004141_n912ContagemResultado_HoraEntrega[0];
            A1351ContagemResultado_DataPrevista = BC004141_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = BC004141_n1351ContagemResultado_DataPrevista[0];
            A1452ContagemResultado_SS = BC004141_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = BC004141_n1452ContagemResultado_SS[0];
            A493ContagemResultado_DemandaFM = BC004141_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC004141_n493ContagemResultado_DemandaFM[0];
            A1584ContagemResultado_UOOwner = BC004141_A1584ContagemResultado_UOOwner[0];
            n1584ContagemResultado_UOOwner = BC004141_n1584ContagemResultado_UOOwner[0];
            A1515ContagemResultado_Evento = BC004141_A1515ContagemResultado_Evento[0];
            n1515ContagemResultado_Evento = BC004141_n1515ContagemResultado_Evento[0];
            A1583ContagemResultado_TipoRegistro = BC004141_A1583ContagemResultado_TipoRegistro[0];
            A1585ContagemResultado_Referencia = BC004141_A1585ContagemResultado_Referencia[0];
            n1585ContagemResultado_Referencia = BC004141_n1585ContagemResultado_Referencia[0];
            A1586ContagemResultado_Restricoes = BC004141_A1586ContagemResultado_Restricoes[0];
            n1586ContagemResultado_Restricoes = BC004141_n1586ContagemResultado_Restricoes[0];
            A1587ContagemResultado_PrioridadePrevista = BC004141_A1587ContagemResultado_PrioridadePrevista[0];
            n1587ContagemResultado_PrioridadePrevista = BC004141_n1587ContagemResultado_PrioridadePrevista[0];
            A484ContagemResultado_StatusDmn = BC004141_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = BC004141_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = BC004141_A471ContagemResultado_DataDmn[0];
            A1044ContagemResultado_FncUsrCod = BC004141_A1044ContagemResultado_FncUsrCod[0];
            n1044ContagemResultado_FncUsrCod = BC004141_n1044ContagemResultado_FncUsrCod[0];
            A1227ContagemResultado_PrazoInicialDias = BC004141_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = BC004141_n1227ContagemResultado_PrazoInicialDias[0];
            A1350ContagemResultado_DataCadastro = BC004141_A1350ContagemResultado_DataCadastro[0];
            n1350ContagemResultado_DataCadastro = BC004141_n1350ContagemResultado_DataCadastro[0];
            A1173ContagemResultado_OSManual = BC004141_A1173ContagemResultado_OSManual[0];
            n1173ContagemResultado_OSManual = BC004141_n1173ContagemResultado_OSManual[0];
            A598ContagemResultado_Baseline = BC004141_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = BC004141_n598ContagemResultado_Baseline[0];
            A485ContagemResultado_EhValidacao = BC004141_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = BC004141_n485ContagemResultado_EhValidacao[0];
            A146Modulo_Codigo = BC004141_A146Modulo_Codigo[0];
            n146Modulo_Codigo = BC004141_n146Modulo_Codigo[0];
            A454ContagemResultado_ContadorFSCod = BC004141_A454ContagemResultado_ContadorFSCod[0];
            n454ContagemResultado_ContadorFSCod = BC004141_n454ContagemResultado_ContadorFSCod[0];
            A890ContagemResultado_Responsavel = BC004141_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = BC004141_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = BC004141_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = BC004141_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = BC004141_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = BC004141_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = BC004141_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = BC004141_n805ContagemResultado_ContratadaOrigemCod[0];
            A489ContagemResultado_SistemaCod = BC004141_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = BC004141_n489ContagemResultado_SistemaCod[0];
            A597ContagemResultado_LoteAceiteCod = BC004141_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = BC004141_n597ContagemResultado_LoteAceiteCod[0];
            A1636ContagemResultado_ServicoSS = BC004141_A1636ContagemResultado_ServicoSS[0];
            n1636ContagemResultado_ServicoSS = BC004141_n1636ContagemResultado_ServicoSS[0];
            A1043ContagemResultado_LiqLogCod = BC004141_A1043ContagemResultado_LiqLogCod[0];
            n1043ContagemResultado_LiqLogCod = BC004141_n1043ContagemResultado_LiqLogCod[0];
            A1553ContagemResultado_CntSrvCod = BC004141_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = BC004141_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = BC004141_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = BC004141_n602ContagemResultado_OSVinculada[0];
         }
         Gx_mode = sMode69;
      }

      protected void ScanKeyEnd4169( )
      {
         pr_default.close(39);
      }

      protected void AfterConfirm4169( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4169( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4169( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4169( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4169( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4169( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4169( )
      {
      }

      protected void AddRow4169( )
      {
         VarsToRow69( bcSolicitacaoServico) ;
      }

      protected void ReadRow4169( )
      {
         RowToVars69( bcSolicitacaoServico, 1) ;
      }

      protected void InitializeNonKey4169( )
      {
         A494ContagemResultado_Descricao = "";
         n494ContagemResultado_Descricao = false;
         A514ContagemResultado_Observacao = "";
         n514ContagemResultado_Observacao = false;
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         n472ContagemResultado_DataEntrega = false;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         n912ContagemResultado_HoraEntrega = false;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         n1351ContagemResultado_DataPrevista = false;
         A1636ContagemResultado_ServicoSS = 0;
         n1636ContagemResultado_ServicoSS = false;
         A493ContagemResultado_DemandaFM = "";
         n493ContagemResultado_DemandaFM = false;
         A1584ContagemResultado_UOOwner = 0;
         n1584ContagemResultado_UOOwner = false;
         A1585ContagemResultado_Referencia = "";
         n1585ContagemResultado_Referencia = false;
         A1586ContagemResultado_Restricoes = "";
         n1586ContagemResultado_Restricoes = false;
         A1587ContagemResultado_PrioridadePrevista = "";
         n1587ContagemResultado_PrioridadePrevista = false;
         A1553ContagemResultado_CntSrvCod = 0;
         n1553ContagemResultado_CntSrvCod = false;
         A602ContagemResultado_OSVinculada = 0;
         n602ContagemResultado_OSVinculada = false;
         A468ContagemResultado_NaoCnfDmnCod = 0;
         n468ContagemResultado_NaoCnfDmnCod = false;
         A597ContagemResultado_LoteAceiteCod = 0;
         n597ContagemResultado_LoteAceiteCod = false;
         A805ContagemResultado_ContratadaOrigemCod = 0;
         n805ContagemResultado_ContratadaOrigemCod = false;
         A454ContagemResultado_ContadorFSCod = 0;
         n454ContagemResultado_ContadorFSCod = false;
         A890ContagemResultado_Responsavel = 0;
         n890ContagemResultado_Responsavel = false;
         A1043ContagemResultado_LiqLogCod = 0;
         n1043ContagemResultado_LiqLogCod = false;
         A146Modulo_Codigo = 0;
         n146Modulo_Codigo = false;
         A490ContagemResultado_ContratadaCod = 0;
         n490ContagemResultado_ContratadaCod = false;
         A489ContagemResultado_SistemaCod = 0;
         n489ContagemResultado_SistemaCod = false;
         A1044ContagemResultado_FncUsrCod = 0;
         n1044ContagemResultado_FncUsrCod = false;
         A1227ContagemResultado_PrazoInicialDias = 0;
         n1227ContagemResultado_PrazoInicialDias = false;
         A1173ContagemResultado_OSManual = false;
         n1173ContagemResultado_OSManual = false;
         A508ContagemResultado_Owner = AV7WWPContext.gxTpr_Userid;
         A1452ContagemResultado_SS = 0;
         n1452ContagemResultado_SS = false;
         A1515ContagemResultado_Evento = 1;
         n1515ContagemResultado_Evento = false;
         A1583ContagemResultado_TipoRegistro = 2;
         A484ContagemResultado_StatusDmn = "S";
         n484ContagemResultado_StatusDmn = false;
         A471ContagemResultado_DataDmn = Gx_date;
         A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1350ContagemResultado_DataCadastro = false;
         A598ContagemResultado_Baseline = false;
         n598ContagemResultado_Baseline = false;
         A485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         Z508ContagemResultado_Owner = 0;
         Z494ContagemResultado_Descricao = "";
         Z472ContagemResultado_DataEntrega = DateTime.MinValue;
         Z912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         Z1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         Z1452ContagemResultado_SS = 0;
         Z493ContagemResultado_DemandaFM = "";
         Z1584ContagemResultado_UOOwner = 0;
         Z1515ContagemResultado_Evento = 0;
         Z1583ContagemResultado_TipoRegistro = 0;
         Z1585ContagemResultado_Referencia = "";
         Z1586ContagemResultado_Restricoes = "";
         Z1587ContagemResultado_PrioridadePrevista = "";
         Z484ContagemResultado_StatusDmn = "";
         Z471ContagemResultado_DataDmn = DateTime.MinValue;
         Z1044ContagemResultado_FncUsrCod = 0;
         Z1227ContagemResultado_PrazoInicialDias = 0;
         Z1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         Z1173ContagemResultado_OSManual = false;
         Z598ContagemResultado_Baseline = false;
         Z485ContagemResultado_EhValidacao = false;
         Z146Modulo_Codigo = 0;
         Z454ContagemResultado_ContadorFSCod = 0;
         Z890ContagemResultado_Responsavel = 0;
         Z468ContagemResultado_NaoCnfDmnCod = 0;
         Z490ContagemResultado_ContratadaCod = 0;
         Z805ContagemResultado_ContratadaOrigemCod = 0;
         Z489ContagemResultado_SistemaCod = 0;
         Z597ContagemResultado_LoteAceiteCod = 0;
         Z1636ContagemResultado_ServicoSS = 0;
         Z1043ContagemResultado_LiqLogCod = 0;
         Z1553ContagemResultado_CntSrvCod = 0;
         Z602ContagemResultado_OSVinculada = 0;
      }

      protected void InitAll4169( )
      {
         A456ContagemResultado_Codigo = 0;
         InitializeNonKey4169( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A598ContagemResultado_Baseline = i598ContagemResultado_Baseline;
         n598ContagemResultado_Baseline = false;
         A485ContagemResultado_EhValidacao = i485ContagemResultado_EhValidacao;
         n485ContagemResultado_EhValidacao = false;
         A1350ContagemResultado_DataCadastro = i1350ContagemResultado_DataCadastro;
         n1350ContagemResultado_DataCadastro = false;
         A1515ContagemResultado_Evento = i1515ContagemResultado_Evento;
         n1515ContagemResultado_Evento = false;
         A1452ContagemResultado_SS = i1452ContagemResultado_SS;
         n1452ContagemResultado_SS = false;
         A1583ContagemResultado_TipoRegistro = i1583ContagemResultado_TipoRegistro;
         A484ContagemResultado_StatusDmn = i484ContagemResultado_StatusDmn;
         n484ContagemResultado_StatusDmn = false;
         A508ContagemResultado_Owner = i508ContagemResultado_Owner;
         A471ContagemResultado_DataDmn = i471ContagemResultado_DataDmn;
      }

      public void VarsToRow69( SdtSolicitacaoServico obj69 )
      {
         obj69.gxTpr_Mode = Gx_mode;
         obj69.gxTpr_Contagemresultado_descricao = A494ContagemResultado_Descricao;
         obj69.gxTpr_Contagemresultado_observacao = A514ContagemResultado_Observacao;
         obj69.gxTpr_Contagemresultado_dataentrega = A472ContagemResultado_DataEntrega;
         obj69.gxTpr_Contagemresultado_horaentrega = A912ContagemResultado_HoraEntrega;
         obj69.gxTpr_Contagemresultado_dataprevista = A1351ContagemResultado_DataPrevista;
         obj69.gxTpr_Contagemresultado_servicoss = A1636ContagemResultado_ServicoSS;
         obj69.gxTpr_Contagemresultado_demandafm = A493ContagemResultado_DemandaFM;
         obj69.gxTpr_Contagemresultado_uoowner = A1584ContagemResultado_UOOwner;
         obj69.gxTpr_Contagemresultado_referencia = A1585ContagemResultado_Referencia;
         obj69.gxTpr_Contagemresultado_restricoes = A1586ContagemResultado_Restricoes;
         obj69.gxTpr_Contagemresultado_prioridadeprevista = A1587ContagemResultado_PrioridadePrevista;
         obj69.gxTpr_Contagemresultado_cntsrvcod = A1553ContagemResultado_CntSrvCod;
         obj69.gxTpr_Contagemresultado_osvinculada = A602ContagemResultado_OSVinculada;
         obj69.gxTpr_Contagemresultado_naocnfdmncod = A468ContagemResultado_NaoCnfDmnCod;
         obj69.gxTpr_Contagemresultado_loteaceitecod = A597ContagemResultado_LoteAceiteCod;
         obj69.gxTpr_Contagemresultado_contratadaorigemcod = A805ContagemResultado_ContratadaOrigemCod;
         obj69.gxTpr_Contagemresultado_contadorfscod = A454ContagemResultado_ContadorFSCod;
         obj69.gxTpr_Contagemresultado_responsavel = A890ContagemResultado_Responsavel;
         obj69.gxTpr_Contagemresultado_liqlogcod = A1043ContagemResultado_LiqLogCod;
         obj69.gxTpr_Modulo_codigo = A146Modulo_Codigo;
         obj69.gxTpr_Contagemresultado_contratadacod = A490ContagemResultado_ContratadaCod;
         obj69.gxTpr_Contagemresultado_sistemacod = A489ContagemResultado_SistemaCod;
         obj69.gxTpr_Contagemresultado_fncusrcod = A1044ContagemResultado_FncUsrCod;
         obj69.gxTpr_Contagemresultado_prazoinicialdias = A1227ContagemResultado_PrazoInicialDias;
         obj69.gxTpr_Contagemresultado_osmanual = A1173ContagemResultado_OSManual;
         obj69.gxTpr_Contagemresultado_owner = A508ContagemResultado_Owner;
         obj69.gxTpr_Contagemresultado_ss = A1452ContagemResultado_SS;
         obj69.gxTpr_Contagemresultado_evento = A1515ContagemResultado_Evento;
         obj69.gxTpr_Contagemresultado_tiporegistro = A1583ContagemResultado_TipoRegistro;
         obj69.gxTpr_Contagemresultado_statusdmn = A484ContagemResultado_StatusDmn;
         obj69.gxTpr_Contagemresultado_datadmn = A471ContagemResultado_DataDmn;
         obj69.gxTpr_Contagemresultado_datacadastro = A1350ContagemResultado_DataCadastro;
         obj69.gxTpr_Contagemresultado_baseline = A598ContagemResultado_Baseline;
         obj69.gxTpr_Contagemresultado_ehvalidacao = A485ContagemResultado_EhValidacao;
         obj69.gxTpr_Contagemresultado_codigo = A456ContagemResultado_Codigo;
         obj69.gxTpr_Contagemresultado_codigo_Z = Z456ContagemResultado_Codigo;
         obj69.gxTpr_Contagemresultado_owner_Z = Z508ContagemResultado_Owner;
         obj69.gxTpr_Contagemresultado_descricao_Z = Z494ContagemResultado_Descricao;
         obj69.gxTpr_Contagemresultado_dataentrega_Z = Z472ContagemResultado_DataEntrega;
         obj69.gxTpr_Contagemresultado_horaentrega_Z = Z912ContagemResultado_HoraEntrega;
         obj69.gxTpr_Contagemresultado_dataprevista_Z = Z1351ContagemResultado_DataPrevista;
         obj69.gxTpr_Contagemresultado_servicoss_Z = Z1636ContagemResultado_ServicoSS;
         obj69.gxTpr_Contagemresultado_ss_Z = Z1452ContagemResultado_SS;
         obj69.gxTpr_Contagemresultado_demandafm_Z = Z493ContagemResultado_DemandaFM;
         obj69.gxTpr_Contagemresultado_uoowner_Z = Z1584ContagemResultado_UOOwner;
         obj69.gxTpr_Contagemresultado_evento_Z = Z1515ContagemResultado_Evento;
         obj69.gxTpr_Contagemresultado_tiporegistro_Z = Z1583ContagemResultado_TipoRegistro;
         obj69.gxTpr_Contagemresultado_referencia_Z = Z1585ContagemResultado_Referencia;
         obj69.gxTpr_Contagemresultado_restricoes_Z = Z1586ContagemResultado_Restricoes;
         obj69.gxTpr_Contagemresultado_prioridadeprevista_Z = Z1587ContagemResultado_PrioridadePrevista;
         obj69.gxTpr_Contagemresultado_statusdmn_Z = Z484ContagemResultado_StatusDmn;
         obj69.gxTpr_Contagemresultado_datadmn_Z = Z471ContagemResultado_DataDmn;
         obj69.gxTpr_Contagemresultado_cntsrvcod_Z = Z1553ContagemResultado_CntSrvCod;
         obj69.gxTpr_Contagemresultado_osvinculada_Z = Z602ContagemResultado_OSVinculada;
         obj69.gxTpr_Contagemresultado_naocnfdmncod_Z = Z468ContagemResultado_NaoCnfDmnCod;
         obj69.gxTpr_Contagemresultado_loteaceitecod_Z = Z597ContagemResultado_LoteAceiteCod;
         obj69.gxTpr_Contagemresultado_contratadaorigemcod_Z = Z805ContagemResultado_ContratadaOrigemCod;
         obj69.gxTpr_Contagemresultado_contadorfscod_Z = Z454ContagemResultado_ContadorFSCod;
         obj69.gxTpr_Contagemresultado_responsavel_Z = Z890ContagemResultado_Responsavel;
         obj69.gxTpr_Contagemresultado_liqlogcod_Z = Z1043ContagemResultado_LiqLogCod;
         obj69.gxTpr_Modulo_codigo_Z = Z146Modulo_Codigo;
         obj69.gxTpr_Contagemresultado_contratadacod_Z = Z490ContagemResultado_ContratadaCod;
         obj69.gxTpr_Contagemresultado_sistemacod_Z = Z489ContagemResultado_SistemaCod;
         obj69.gxTpr_Contagemresultado_fncusrcod_Z = Z1044ContagemResultado_FncUsrCod;
         obj69.gxTpr_Contagemresultado_prazoinicialdias_Z = Z1227ContagemResultado_PrazoInicialDias;
         obj69.gxTpr_Contagemresultado_datacadastro_Z = Z1350ContagemResultado_DataCadastro;
         obj69.gxTpr_Contagemresultado_osmanual_Z = Z1173ContagemResultado_OSManual;
         obj69.gxTpr_Contagemresultado_baseline_Z = Z598ContagemResultado_Baseline;
         obj69.gxTpr_Contagemresultado_ehvalidacao_Z = Z485ContagemResultado_EhValidacao;
         obj69.gxTpr_Contagemresultado_descricao_N = (short)(Convert.ToInt16(n494ContagemResultado_Descricao));
         obj69.gxTpr_Contagemresultado_observacao_N = (short)(Convert.ToInt16(n514ContagemResultado_Observacao));
         obj69.gxTpr_Contagemresultado_dataentrega_N = (short)(Convert.ToInt16(n472ContagemResultado_DataEntrega));
         obj69.gxTpr_Contagemresultado_horaentrega_N = (short)(Convert.ToInt16(n912ContagemResultado_HoraEntrega));
         obj69.gxTpr_Contagemresultado_dataprevista_N = (short)(Convert.ToInt16(n1351ContagemResultado_DataPrevista));
         obj69.gxTpr_Contagemresultado_servicoss_N = (short)(Convert.ToInt16(n1636ContagemResultado_ServicoSS));
         obj69.gxTpr_Contagemresultado_ss_N = (short)(Convert.ToInt16(n1452ContagemResultado_SS));
         obj69.gxTpr_Contagemresultado_demandafm_N = (short)(Convert.ToInt16(n493ContagemResultado_DemandaFM));
         obj69.gxTpr_Contagemresultado_uoowner_N = (short)(Convert.ToInt16(n1584ContagemResultado_UOOwner));
         obj69.gxTpr_Contagemresultado_evento_N = (short)(Convert.ToInt16(n1515ContagemResultado_Evento));
         obj69.gxTpr_Contagemresultado_referencia_N = (short)(Convert.ToInt16(n1585ContagemResultado_Referencia));
         obj69.gxTpr_Contagemresultado_restricoes_N = (short)(Convert.ToInt16(n1586ContagemResultado_Restricoes));
         obj69.gxTpr_Contagemresultado_prioridadeprevista_N = (short)(Convert.ToInt16(n1587ContagemResultado_PrioridadePrevista));
         obj69.gxTpr_Contagemresultado_statusdmn_N = (short)(Convert.ToInt16(n484ContagemResultado_StatusDmn));
         obj69.gxTpr_Contagemresultado_cntsrvcod_N = (short)(Convert.ToInt16(n1553ContagemResultado_CntSrvCod));
         obj69.gxTpr_Contagemresultado_osvinculada_N = (short)(Convert.ToInt16(n602ContagemResultado_OSVinculada));
         obj69.gxTpr_Contagemresultado_naocnfdmncod_N = (short)(Convert.ToInt16(n468ContagemResultado_NaoCnfDmnCod));
         obj69.gxTpr_Contagemresultado_loteaceitecod_N = (short)(Convert.ToInt16(n597ContagemResultado_LoteAceiteCod));
         obj69.gxTpr_Contagemresultado_contratadaorigemcod_N = (short)(Convert.ToInt16(n805ContagemResultado_ContratadaOrigemCod));
         obj69.gxTpr_Contagemresultado_contadorfscod_N = (short)(Convert.ToInt16(n454ContagemResultado_ContadorFSCod));
         obj69.gxTpr_Contagemresultado_responsavel_N = (short)(Convert.ToInt16(n890ContagemResultado_Responsavel));
         obj69.gxTpr_Contagemresultado_liqlogcod_N = (short)(Convert.ToInt16(n1043ContagemResultado_LiqLogCod));
         obj69.gxTpr_Modulo_codigo_N = (short)(Convert.ToInt16(n146Modulo_Codigo));
         obj69.gxTpr_Contagemresultado_contratadacod_N = (short)(Convert.ToInt16(n490ContagemResultado_ContratadaCod));
         obj69.gxTpr_Contagemresultado_sistemacod_N = (short)(Convert.ToInt16(n489ContagemResultado_SistemaCod));
         obj69.gxTpr_Contagemresultado_fncusrcod_N = (short)(Convert.ToInt16(n1044ContagemResultado_FncUsrCod));
         obj69.gxTpr_Contagemresultado_prazoinicialdias_N = (short)(Convert.ToInt16(n1227ContagemResultado_PrazoInicialDias));
         obj69.gxTpr_Contagemresultado_datacadastro_N = (short)(Convert.ToInt16(n1350ContagemResultado_DataCadastro));
         obj69.gxTpr_Contagemresultado_osmanual_N = (short)(Convert.ToInt16(n1173ContagemResultado_OSManual));
         obj69.gxTpr_Contagemresultado_baseline_N = (short)(Convert.ToInt16(n598ContagemResultado_Baseline));
         obj69.gxTpr_Contagemresultado_ehvalidacao_N = (short)(Convert.ToInt16(n485ContagemResultado_EhValidacao));
         obj69.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow69( SdtSolicitacaoServico obj69 )
      {
         obj69.gxTpr_Contagemresultado_codigo = A456ContagemResultado_Codigo;
         return  ;
      }

      public void RowToVars69( SdtSolicitacaoServico obj69 ,
                               int forceLoad )
      {
         Gx_mode = obj69.gxTpr_Mode;
         A494ContagemResultado_Descricao = obj69.gxTpr_Contagemresultado_descricao;
         n494ContagemResultado_Descricao = false;
         A514ContagemResultado_Observacao = obj69.gxTpr_Contagemresultado_observacao;
         n514ContagemResultado_Observacao = false;
         A472ContagemResultado_DataEntrega = obj69.gxTpr_Contagemresultado_dataentrega;
         n472ContagemResultado_DataEntrega = false;
         A912ContagemResultado_HoraEntrega = obj69.gxTpr_Contagemresultado_horaentrega;
         n912ContagemResultado_HoraEntrega = false;
         A1351ContagemResultado_DataPrevista = obj69.gxTpr_Contagemresultado_dataprevista;
         n1351ContagemResultado_DataPrevista = false;
         A1636ContagemResultado_ServicoSS = obj69.gxTpr_Contagemresultado_servicoss;
         n1636ContagemResultado_ServicoSS = false;
         A493ContagemResultado_DemandaFM = obj69.gxTpr_Contagemresultado_demandafm;
         n493ContagemResultado_DemandaFM = false;
         A1584ContagemResultado_UOOwner = obj69.gxTpr_Contagemresultado_uoowner;
         n1584ContagemResultado_UOOwner = false;
         A1585ContagemResultado_Referencia = obj69.gxTpr_Contagemresultado_referencia;
         n1585ContagemResultado_Referencia = false;
         A1586ContagemResultado_Restricoes = obj69.gxTpr_Contagemresultado_restricoes;
         n1586ContagemResultado_Restricoes = false;
         A1587ContagemResultado_PrioridadePrevista = obj69.gxTpr_Contagemresultado_prioridadeprevista;
         n1587ContagemResultado_PrioridadePrevista = false;
         A1553ContagemResultado_CntSrvCod = obj69.gxTpr_Contagemresultado_cntsrvcod;
         n1553ContagemResultado_CntSrvCod = false;
         A602ContagemResultado_OSVinculada = obj69.gxTpr_Contagemresultado_osvinculada;
         n602ContagemResultado_OSVinculada = false;
         A468ContagemResultado_NaoCnfDmnCod = obj69.gxTpr_Contagemresultado_naocnfdmncod;
         n468ContagemResultado_NaoCnfDmnCod = false;
         A597ContagemResultado_LoteAceiteCod = obj69.gxTpr_Contagemresultado_loteaceitecod;
         n597ContagemResultado_LoteAceiteCod = false;
         A805ContagemResultado_ContratadaOrigemCod = obj69.gxTpr_Contagemresultado_contratadaorigemcod;
         n805ContagemResultado_ContratadaOrigemCod = false;
         A454ContagemResultado_ContadorFSCod = obj69.gxTpr_Contagemresultado_contadorfscod;
         n454ContagemResultado_ContadorFSCod = false;
         A890ContagemResultado_Responsavel = obj69.gxTpr_Contagemresultado_responsavel;
         n890ContagemResultado_Responsavel = false;
         A1043ContagemResultado_LiqLogCod = obj69.gxTpr_Contagemresultado_liqlogcod;
         n1043ContagemResultado_LiqLogCod = false;
         A146Modulo_Codigo = obj69.gxTpr_Modulo_codigo;
         n146Modulo_Codigo = false;
         A490ContagemResultado_ContratadaCod = obj69.gxTpr_Contagemresultado_contratadacod;
         n490ContagemResultado_ContratadaCod = false;
         A489ContagemResultado_SistemaCod = obj69.gxTpr_Contagemresultado_sistemacod;
         n489ContagemResultado_SistemaCod = false;
         A1044ContagemResultado_FncUsrCod = obj69.gxTpr_Contagemresultado_fncusrcod;
         n1044ContagemResultado_FncUsrCod = false;
         A1227ContagemResultado_PrazoInicialDias = obj69.gxTpr_Contagemresultado_prazoinicialdias;
         n1227ContagemResultado_PrazoInicialDias = false;
         A1173ContagemResultado_OSManual = obj69.gxTpr_Contagemresultado_osmanual;
         n1173ContagemResultado_OSManual = false;
         A508ContagemResultado_Owner = obj69.gxTpr_Contagemresultado_owner;
         A1452ContagemResultado_SS = obj69.gxTpr_Contagemresultado_ss;
         n1452ContagemResultado_SS = false;
         A1515ContagemResultado_Evento = obj69.gxTpr_Contagemresultado_evento;
         n1515ContagemResultado_Evento = false;
         A1583ContagemResultado_TipoRegistro = obj69.gxTpr_Contagemresultado_tiporegistro;
         A484ContagemResultado_StatusDmn = obj69.gxTpr_Contagemresultado_statusdmn;
         n484ContagemResultado_StatusDmn = false;
         A471ContagemResultado_DataDmn = obj69.gxTpr_Contagemresultado_datadmn;
         A1350ContagemResultado_DataCadastro = obj69.gxTpr_Contagemresultado_datacadastro;
         n1350ContagemResultado_DataCadastro = false;
         A598ContagemResultado_Baseline = obj69.gxTpr_Contagemresultado_baseline;
         n598ContagemResultado_Baseline = false;
         A485ContagemResultado_EhValidacao = obj69.gxTpr_Contagemresultado_ehvalidacao;
         n485ContagemResultado_EhValidacao = false;
         A456ContagemResultado_Codigo = obj69.gxTpr_Contagemresultado_codigo;
         Z456ContagemResultado_Codigo = obj69.gxTpr_Contagemresultado_codigo_Z;
         Z508ContagemResultado_Owner = obj69.gxTpr_Contagemresultado_owner_Z;
         Z494ContagemResultado_Descricao = obj69.gxTpr_Contagemresultado_descricao_Z;
         Z472ContagemResultado_DataEntrega = obj69.gxTpr_Contagemresultado_dataentrega_Z;
         Z912ContagemResultado_HoraEntrega = obj69.gxTpr_Contagemresultado_horaentrega_Z;
         Z1351ContagemResultado_DataPrevista = obj69.gxTpr_Contagemresultado_dataprevista_Z;
         Z1636ContagemResultado_ServicoSS = obj69.gxTpr_Contagemresultado_servicoss_Z;
         Z1452ContagemResultado_SS = obj69.gxTpr_Contagemresultado_ss_Z;
         Z493ContagemResultado_DemandaFM = obj69.gxTpr_Contagemresultado_demandafm_Z;
         Z1584ContagemResultado_UOOwner = obj69.gxTpr_Contagemresultado_uoowner_Z;
         Z1515ContagemResultado_Evento = obj69.gxTpr_Contagemresultado_evento_Z;
         Z1583ContagemResultado_TipoRegistro = obj69.gxTpr_Contagemresultado_tiporegistro_Z;
         Z1585ContagemResultado_Referencia = obj69.gxTpr_Contagemresultado_referencia_Z;
         Z1586ContagemResultado_Restricoes = obj69.gxTpr_Contagemresultado_restricoes_Z;
         Z1587ContagemResultado_PrioridadePrevista = obj69.gxTpr_Contagemresultado_prioridadeprevista_Z;
         Z484ContagemResultado_StatusDmn = obj69.gxTpr_Contagemresultado_statusdmn_Z;
         Z471ContagemResultado_DataDmn = obj69.gxTpr_Contagemresultado_datadmn_Z;
         Z1553ContagemResultado_CntSrvCod = obj69.gxTpr_Contagemresultado_cntsrvcod_Z;
         Z602ContagemResultado_OSVinculada = obj69.gxTpr_Contagemresultado_osvinculada_Z;
         Z468ContagemResultado_NaoCnfDmnCod = obj69.gxTpr_Contagemresultado_naocnfdmncod_Z;
         Z597ContagemResultado_LoteAceiteCod = obj69.gxTpr_Contagemresultado_loteaceitecod_Z;
         Z805ContagemResultado_ContratadaOrigemCod = obj69.gxTpr_Contagemresultado_contratadaorigemcod_Z;
         Z454ContagemResultado_ContadorFSCod = obj69.gxTpr_Contagemresultado_contadorfscod_Z;
         Z890ContagemResultado_Responsavel = obj69.gxTpr_Contagemresultado_responsavel_Z;
         Z1043ContagemResultado_LiqLogCod = obj69.gxTpr_Contagemresultado_liqlogcod_Z;
         Z146Modulo_Codigo = obj69.gxTpr_Modulo_codigo_Z;
         Z490ContagemResultado_ContratadaCod = obj69.gxTpr_Contagemresultado_contratadacod_Z;
         Z489ContagemResultado_SistemaCod = obj69.gxTpr_Contagemresultado_sistemacod_Z;
         Z1044ContagemResultado_FncUsrCod = obj69.gxTpr_Contagemresultado_fncusrcod_Z;
         Z1227ContagemResultado_PrazoInicialDias = obj69.gxTpr_Contagemresultado_prazoinicialdias_Z;
         Z1350ContagemResultado_DataCadastro = obj69.gxTpr_Contagemresultado_datacadastro_Z;
         Z1173ContagemResultado_OSManual = obj69.gxTpr_Contagemresultado_osmanual_Z;
         Z598ContagemResultado_Baseline = obj69.gxTpr_Contagemresultado_baseline_Z;
         Z485ContagemResultado_EhValidacao = obj69.gxTpr_Contagemresultado_ehvalidacao_Z;
         n494ContagemResultado_Descricao = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_descricao_N));
         n514ContagemResultado_Observacao = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_observacao_N));
         n472ContagemResultado_DataEntrega = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_dataentrega_N));
         n912ContagemResultado_HoraEntrega = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_horaentrega_N));
         n1351ContagemResultado_DataPrevista = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_dataprevista_N));
         n1636ContagemResultado_ServicoSS = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_servicoss_N));
         n1452ContagemResultado_SS = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_ss_N));
         n493ContagemResultado_DemandaFM = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_demandafm_N));
         n1584ContagemResultado_UOOwner = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_uoowner_N));
         n1515ContagemResultado_Evento = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_evento_N));
         n1585ContagemResultado_Referencia = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_referencia_N));
         n1586ContagemResultado_Restricoes = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_restricoes_N));
         n1587ContagemResultado_PrioridadePrevista = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_prioridadeprevista_N));
         n484ContagemResultado_StatusDmn = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_statusdmn_N));
         n1553ContagemResultado_CntSrvCod = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_cntsrvcod_N));
         n602ContagemResultado_OSVinculada = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_osvinculada_N));
         n468ContagemResultado_NaoCnfDmnCod = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_naocnfdmncod_N));
         n597ContagemResultado_LoteAceiteCod = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_loteaceitecod_N));
         n805ContagemResultado_ContratadaOrigemCod = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_contratadaorigemcod_N));
         n454ContagemResultado_ContadorFSCod = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_contadorfscod_N));
         n890ContagemResultado_Responsavel = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_responsavel_N));
         n1043ContagemResultado_LiqLogCod = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_liqlogcod_N));
         n146Modulo_Codigo = (bool)(Convert.ToBoolean(obj69.gxTpr_Modulo_codigo_N));
         n490ContagemResultado_ContratadaCod = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_contratadacod_N));
         n489ContagemResultado_SistemaCod = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_sistemacod_N));
         n1044ContagemResultado_FncUsrCod = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_fncusrcod_N));
         n1227ContagemResultado_PrazoInicialDias = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_prazoinicialdias_N));
         n1350ContagemResultado_DataCadastro = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_datacadastro_N));
         n1173ContagemResultado_OSManual = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_osmanual_N));
         n598ContagemResultado_Baseline = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_baseline_N));
         n485ContagemResultado_EhValidacao = (bool)(Convert.ToBoolean(obj69.gxTpr_Contagemresultado_ehvalidacao_N));
         Gx_mode = obj69.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A456ContagemResultado_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4169( ) ;
         ScanKeyStart4169( ) ;
         if ( RcdFound69 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
         }
         ZM4169( -19) ;
         OnLoadActions4169( ) ;
         AddRow4169( ) ;
         ScanKeyEnd4169( ) ;
         if ( RcdFound69 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars69( bcSolicitacaoServico, 0) ;
         ScanKeyStart4169( ) ;
         if ( RcdFound69 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
         }
         ZM4169( -19) ;
         OnLoadActions4169( ) ;
         AddRow4169( ) ;
         ScanKeyEnd4169( ) ;
         if ( RcdFound69 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars69( bcSolicitacaoServico, 0) ;
         nKeyPressed = 1;
         GetKey4169( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4169( ) ;
         }
         else
         {
            if ( RcdFound69 == 1 )
            {
               if ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo )
               {
                  A456ContagemResultado_Codigo = Z456ContagemResultado_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4169( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4169( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4169( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow69( bcSolicitacaoServico) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars69( bcSolicitacaoServico, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4169( ) ;
         if ( RcdFound69 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo )
            {
               A456ContagemResultado_Codigo = Z456ContagemResultado_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         context.RollbackDataStores( "SolicitacaoServico_BC");
         VarsToRow69( bcSolicitacaoServico) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcSolicitacaoServico.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcSolicitacaoServico.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcSolicitacaoServico )
         {
            bcSolicitacaoServico = (SdtSolicitacaoServico)(sdt);
            if ( StringUtil.StrCmp(bcSolicitacaoServico.gxTpr_Mode, "") == 0 )
            {
               bcSolicitacaoServico.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow69( bcSolicitacaoServico) ;
            }
            else
            {
               RowToVars69( bcSolicitacaoServico, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcSolicitacaoServico.gxTpr_Mode, "") == 0 )
            {
               bcSolicitacaoServico.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars69( bcSolicitacaoServico, 1) ;
         return  ;
      }

      public SdtSolicitacaoServico SolicitacaoServico_BC
      {
         get {
            return bcSolicitacaoServico ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV7WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9WebSession = context.GetSession();
         AV26Pgmname = "";
         AV22TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z494ContagemResultado_Descricao = "";
         A494ContagemResultado_Descricao = "";
         Z472ContagemResultado_DataEntrega = DateTime.MinValue;
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         Z912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         Z1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         Z493ContagemResultado_DemandaFM = "";
         A493ContagemResultado_DemandaFM = "";
         Z1585ContagemResultado_Referencia = "";
         A1585ContagemResultado_Referencia = "";
         Z1586ContagemResultado_Restricoes = "";
         A1586ContagemResultado_Restricoes = "";
         Z1587ContagemResultado_PrioridadePrevista = "";
         A1587ContagemResultado_PrioridadePrevista = "";
         Z484ContagemResultado_StatusDmn = "";
         A484ContagemResultado_StatusDmn = "";
         Z471ContagemResultado_DataDmn = DateTime.MinValue;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         Z1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         Z514ContagemResultado_Observacao = "";
         A514ContagemResultado_Observacao = "";
         Gx_date = DateTime.MinValue;
         BC004116_A456ContagemResultado_Codigo = new int[1] ;
         BC004116_A508ContagemResultado_Owner = new int[1] ;
         BC004116_A494ContagemResultado_Descricao = new String[] {""} ;
         BC004116_n494ContagemResultado_Descricao = new bool[] {false} ;
         BC004116_A514ContagemResultado_Observacao = new String[] {""} ;
         BC004116_n514ContagemResultado_Observacao = new bool[] {false} ;
         BC004116_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         BC004116_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         BC004116_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         BC004116_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         BC004116_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         BC004116_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         BC004116_A1452ContagemResultado_SS = new int[1] ;
         BC004116_n1452ContagemResultado_SS = new bool[] {false} ;
         BC004116_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC004116_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC004116_A1584ContagemResultado_UOOwner = new int[1] ;
         BC004116_n1584ContagemResultado_UOOwner = new bool[] {false} ;
         BC004116_A1515ContagemResultado_Evento = new short[1] ;
         BC004116_n1515ContagemResultado_Evento = new bool[] {false} ;
         BC004116_A1583ContagemResultado_TipoRegistro = new short[1] ;
         BC004116_A1585ContagemResultado_Referencia = new String[] {""} ;
         BC004116_n1585ContagemResultado_Referencia = new bool[] {false} ;
         BC004116_A1586ContagemResultado_Restricoes = new String[] {""} ;
         BC004116_n1586ContagemResultado_Restricoes = new bool[] {false} ;
         BC004116_A1587ContagemResultado_PrioridadePrevista = new String[] {""} ;
         BC004116_n1587ContagemResultado_PrioridadePrevista = new bool[] {false} ;
         BC004116_A484ContagemResultado_StatusDmn = new String[] {""} ;
         BC004116_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         BC004116_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         BC004116_A1044ContagemResultado_FncUsrCod = new int[1] ;
         BC004116_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         BC004116_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         BC004116_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         BC004116_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         BC004116_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         BC004116_A1173ContagemResultado_OSManual = new bool[] {false} ;
         BC004116_n1173ContagemResultado_OSManual = new bool[] {false} ;
         BC004116_A598ContagemResultado_Baseline = new bool[] {false} ;
         BC004116_n598ContagemResultado_Baseline = new bool[] {false} ;
         BC004116_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC004116_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC004116_A146Modulo_Codigo = new int[1] ;
         BC004116_n146Modulo_Codigo = new bool[] {false} ;
         BC004116_A454ContagemResultado_ContadorFSCod = new int[1] ;
         BC004116_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         BC004116_A890ContagemResultado_Responsavel = new int[1] ;
         BC004116_n890ContagemResultado_Responsavel = new bool[] {false} ;
         BC004116_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         BC004116_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         BC004116_A490ContagemResultado_ContratadaCod = new int[1] ;
         BC004116_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         BC004116_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         BC004116_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         BC004116_A489ContagemResultado_SistemaCod = new int[1] ;
         BC004116_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         BC004116_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         BC004116_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         BC004116_A1636ContagemResultado_ServicoSS = new int[1] ;
         BC004116_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         BC004116_A1043ContagemResultado_LiqLogCod = new int[1] ;
         BC004116_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         BC004116_A1553ContagemResultado_CntSrvCod = new int[1] ;
         BC004116_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         BC004116_A602ContagemResultado_OSVinculada = new int[1] ;
         BC004116_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         BC004112_A1636ContagemResultado_ServicoSS = new int[1] ;
         BC004112_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         BC004114_A1553ContagemResultado_CntSrvCod = new int[1] ;
         BC004114_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         BC004115_A602ContagemResultado_OSVinculada = new int[1] ;
         BC004115_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         BC00417_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         BC00417_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         BC004111_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         BC004111_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         BC00419_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         BC00419_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         BC00415_A454ContagemResultado_ContadorFSCod = new int[1] ;
         BC00415_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         BC00416_A890ContagemResultado_Responsavel = new int[1] ;
         BC00416_n890ContagemResultado_Responsavel = new bool[] {false} ;
         BC004113_A1043ContagemResultado_LiqLogCod = new int[1] ;
         BC004113_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         BC00414_A146Modulo_Codigo = new int[1] ;
         BC00414_n146Modulo_Codigo = new bool[] {false} ;
         BC00418_A490ContagemResultado_ContratadaCod = new int[1] ;
         BC00418_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         BC004110_A489ContagemResultado_SistemaCod = new int[1] ;
         BC004110_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         BC004117_A456ContagemResultado_Codigo = new int[1] ;
         BC00413_A456ContagemResultado_Codigo = new int[1] ;
         BC00413_A508ContagemResultado_Owner = new int[1] ;
         BC00413_A494ContagemResultado_Descricao = new String[] {""} ;
         BC00413_n494ContagemResultado_Descricao = new bool[] {false} ;
         BC00413_A514ContagemResultado_Observacao = new String[] {""} ;
         BC00413_n514ContagemResultado_Observacao = new bool[] {false} ;
         BC00413_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         BC00413_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         BC00413_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         BC00413_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         BC00413_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         BC00413_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         BC00413_A1452ContagemResultado_SS = new int[1] ;
         BC00413_n1452ContagemResultado_SS = new bool[] {false} ;
         BC00413_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC00413_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC00413_A1584ContagemResultado_UOOwner = new int[1] ;
         BC00413_n1584ContagemResultado_UOOwner = new bool[] {false} ;
         BC00413_A1515ContagemResultado_Evento = new short[1] ;
         BC00413_n1515ContagemResultado_Evento = new bool[] {false} ;
         BC00413_A1583ContagemResultado_TipoRegistro = new short[1] ;
         BC00413_A1585ContagemResultado_Referencia = new String[] {""} ;
         BC00413_n1585ContagemResultado_Referencia = new bool[] {false} ;
         BC00413_A1586ContagemResultado_Restricoes = new String[] {""} ;
         BC00413_n1586ContagemResultado_Restricoes = new bool[] {false} ;
         BC00413_A1587ContagemResultado_PrioridadePrevista = new String[] {""} ;
         BC00413_n1587ContagemResultado_PrioridadePrevista = new bool[] {false} ;
         BC00413_A484ContagemResultado_StatusDmn = new String[] {""} ;
         BC00413_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         BC00413_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         BC00413_A1044ContagemResultado_FncUsrCod = new int[1] ;
         BC00413_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         BC00413_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         BC00413_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         BC00413_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         BC00413_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         BC00413_A1173ContagemResultado_OSManual = new bool[] {false} ;
         BC00413_n1173ContagemResultado_OSManual = new bool[] {false} ;
         BC00413_A598ContagemResultado_Baseline = new bool[] {false} ;
         BC00413_n598ContagemResultado_Baseline = new bool[] {false} ;
         BC00413_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC00413_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC00413_A146Modulo_Codigo = new int[1] ;
         BC00413_n146Modulo_Codigo = new bool[] {false} ;
         BC00413_A454ContagemResultado_ContadorFSCod = new int[1] ;
         BC00413_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         BC00413_A890ContagemResultado_Responsavel = new int[1] ;
         BC00413_n890ContagemResultado_Responsavel = new bool[] {false} ;
         BC00413_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         BC00413_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         BC00413_A490ContagemResultado_ContratadaCod = new int[1] ;
         BC00413_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         BC00413_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         BC00413_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         BC00413_A489ContagemResultado_SistemaCod = new int[1] ;
         BC00413_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         BC00413_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         BC00413_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         BC00413_A1636ContagemResultado_ServicoSS = new int[1] ;
         BC00413_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         BC00413_A1043ContagemResultado_LiqLogCod = new int[1] ;
         BC00413_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         BC00413_A1553ContagemResultado_CntSrvCod = new int[1] ;
         BC00413_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         BC00413_A602ContagemResultado_OSVinculada = new int[1] ;
         BC00413_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         sMode69 = "";
         BC00412_A456ContagemResultado_Codigo = new int[1] ;
         BC00412_A508ContagemResultado_Owner = new int[1] ;
         BC00412_A494ContagemResultado_Descricao = new String[] {""} ;
         BC00412_n494ContagemResultado_Descricao = new bool[] {false} ;
         BC00412_A514ContagemResultado_Observacao = new String[] {""} ;
         BC00412_n514ContagemResultado_Observacao = new bool[] {false} ;
         BC00412_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         BC00412_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         BC00412_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         BC00412_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         BC00412_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         BC00412_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         BC00412_A1452ContagemResultado_SS = new int[1] ;
         BC00412_n1452ContagemResultado_SS = new bool[] {false} ;
         BC00412_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC00412_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC00412_A1584ContagemResultado_UOOwner = new int[1] ;
         BC00412_n1584ContagemResultado_UOOwner = new bool[] {false} ;
         BC00412_A1515ContagemResultado_Evento = new short[1] ;
         BC00412_n1515ContagemResultado_Evento = new bool[] {false} ;
         BC00412_A1583ContagemResultado_TipoRegistro = new short[1] ;
         BC00412_A1585ContagemResultado_Referencia = new String[] {""} ;
         BC00412_n1585ContagemResultado_Referencia = new bool[] {false} ;
         BC00412_A1586ContagemResultado_Restricoes = new String[] {""} ;
         BC00412_n1586ContagemResultado_Restricoes = new bool[] {false} ;
         BC00412_A1587ContagemResultado_PrioridadePrevista = new String[] {""} ;
         BC00412_n1587ContagemResultado_PrioridadePrevista = new bool[] {false} ;
         BC00412_A484ContagemResultado_StatusDmn = new String[] {""} ;
         BC00412_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         BC00412_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         BC00412_A1044ContagemResultado_FncUsrCod = new int[1] ;
         BC00412_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         BC00412_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         BC00412_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         BC00412_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         BC00412_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         BC00412_A1173ContagemResultado_OSManual = new bool[] {false} ;
         BC00412_n1173ContagemResultado_OSManual = new bool[] {false} ;
         BC00412_A598ContagemResultado_Baseline = new bool[] {false} ;
         BC00412_n598ContagemResultado_Baseline = new bool[] {false} ;
         BC00412_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC00412_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC00412_A146Modulo_Codigo = new int[1] ;
         BC00412_n146Modulo_Codigo = new bool[] {false} ;
         BC00412_A454ContagemResultado_ContadorFSCod = new int[1] ;
         BC00412_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         BC00412_A890ContagemResultado_Responsavel = new int[1] ;
         BC00412_n890ContagemResultado_Responsavel = new bool[] {false} ;
         BC00412_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         BC00412_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         BC00412_A490ContagemResultado_ContratadaCod = new int[1] ;
         BC00412_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         BC00412_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         BC00412_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         BC00412_A489ContagemResultado_SistemaCod = new int[1] ;
         BC00412_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         BC00412_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         BC00412_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         BC00412_A1636ContagemResultado_ServicoSS = new int[1] ;
         BC00412_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         BC00412_A1043ContagemResultado_LiqLogCod = new int[1] ;
         BC00412_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         BC00412_A1553ContagemResultado_CntSrvCod = new int[1] ;
         BC00412_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         BC00412_A602ContagemResultado_OSVinculada = new int[1] ;
         BC00412_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         BC004118_A456ContagemResultado_Codigo = new int[1] ;
         BC004121_A602ContagemResultado_OSVinculada = new int[1] ;
         BC004121_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         BC004122_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         BC004123_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         BC004124_A1984ContagemResultadoQA_Codigo = new int[1] ;
         BC004125_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         BC004126_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         BC004127_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         BC004128_A1685Proposta_Codigo = new int[1] ;
         BC004129_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         BC004130_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         BC004130_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         BC004131_A1331ContagemResultadoNota_Codigo = new int[1] ;
         BC004132_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         BC004132_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         BC004133_A1241Incidentes_Codigo = new int[1] ;
         BC004134_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         BC004134_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         BC004134_A1209AgendaAtendimento_CodDmn = new int[1] ;
         BC004135_A1797LogResponsavel_Codigo = new long[1] ;
         BC004136_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004137_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         BC004138_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         BC004138_A456ContagemResultado_Codigo = new int[1] ;
         BC004139_A456ContagemResultado_Codigo = new int[1] ;
         BC004139_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         BC004140_A456ContagemResultado_Codigo = new int[1] ;
         BC004140_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         BC004140_A511ContagemResultado_HoraCnt = new String[] {""} ;
         BC004141_A456ContagemResultado_Codigo = new int[1] ;
         BC004141_A508ContagemResultado_Owner = new int[1] ;
         BC004141_A494ContagemResultado_Descricao = new String[] {""} ;
         BC004141_n494ContagemResultado_Descricao = new bool[] {false} ;
         BC004141_A514ContagemResultado_Observacao = new String[] {""} ;
         BC004141_n514ContagemResultado_Observacao = new bool[] {false} ;
         BC004141_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         BC004141_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         BC004141_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         BC004141_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         BC004141_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         BC004141_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         BC004141_A1452ContagemResultado_SS = new int[1] ;
         BC004141_n1452ContagemResultado_SS = new bool[] {false} ;
         BC004141_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC004141_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC004141_A1584ContagemResultado_UOOwner = new int[1] ;
         BC004141_n1584ContagemResultado_UOOwner = new bool[] {false} ;
         BC004141_A1515ContagemResultado_Evento = new short[1] ;
         BC004141_n1515ContagemResultado_Evento = new bool[] {false} ;
         BC004141_A1583ContagemResultado_TipoRegistro = new short[1] ;
         BC004141_A1585ContagemResultado_Referencia = new String[] {""} ;
         BC004141_n1585ContagemResultado_Referencia = new bool[] {false} ;
         BC004141_A1586ContagemResultado_Restricoes = new String[] {""} ;
         BC004141_n1586ContagemResultado_Restricoes = new bool[] {false} ;
         BC004141_A1587ContagemResultado_PrioridadePrevista = new String[] {""} ;
         BC004141_n1587ContagemResultado_PrioridadePrevista = new bool[] {false} ;
         BC004141_A484ContagemResultado_StatusDmn = new String[] {""} ;
         BC004141_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         BC004141_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         BC004141_A1044ContagemResultado_FncUsrCod = new int[1] ;
         BC004141_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         BC004141_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         BC004141_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         BC004141_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         BC004141_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         BC004141_A1173ContagemResultado_OSManual = new bool[] {false} ;
         BC004141_n1173ContagemResultado_OSManual = new bool[] {false} ;
         BC004141_A598ContagemResultado_Baseline = new bool[] {false} ;
         BC004141_n598ContagemResultado_Baseline = new bool[] {false} ;
         BC004141_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC004141_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         BC004141_A146Modulo_Codigo = new int[1] ;
         BC004141_n146Modulo_Codigo = new bool[] {false} ;
         BC004141_A454ContagemResultado_ContadorFSCod = new int[1] ;
         BC004141_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         BC004141_A890ContagemResultado_Responsavel = new int[1] ;
         BC004141_n890ContagemResultado_Responsavel = new bool[] {false} ;
         BC004141_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         BC004141_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         BC004141_A490ContagemResultado_ContratadaCod = new int[1] ;
         BC004141_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         BC004141_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         BC004141_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         BC004141_A489ContagemResultado_SistemaCod = new int[1] ;
         BC004141_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         BC004141_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         BC004141_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         BC004141_A1636ContagemResultado_ServicoSS = new int[1] ;
         BC004141_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         BC004141_A1043ContagemResultado_LiqLogCod = new int[1] ;
         BC004141_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         BC004141_A1553ContagemResultado_CntSrvCod = new int[1] ;
         BC004141_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         BC004141_A602ContagemResultado_OSVinculada = new int[1] ;
         BC004141_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         i1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         i484ContagemResultado_StatusDmn = "";
         i471ContagemResultado_DataDmn = DateTime.MinValue;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacaoservico_bc__default(),
            new Object[][] {
                new Object[] {
               BC00412_A456ContagemResultado_Codigo, BC00412_A508ContagemResultado_Owner, BC00412_A494ContagemResultado_Descricao, BC00412_n494ContagemResultado_Descricao, BC00412_A514ContagemResultado_Observacao, BC00412_n514ContagemResultado_Observacao, BC00412_A472ContagemResultado_DataEntrega, BC00412_n472ContagemResultado_DataEntrega, BC00412_A912ContagemResultado_HoraEntrega, BC00412_n912ContagemResultado_HoraEntrega,
               BC00412_A1351ContagemResultado_DataPrevista, BC00412_n1351ContagemResultado_DataPrevista, BC00412_A1452ContagemResultado_SS, BC00412_n1452ContagemResultado_SS, BC00412_A493ContagemResultado_DemandaFM, BC00412_n493ContagemResultado_DemandaFM, BC00412_A1584ContagemResultado_UOOwner, BC00412_n1584ContagemResultado_UOOwner, BC00412_A1515ContagemResultado_Evento, BC00412_n1515ContagemResultado_Evento,
               BC00412_A1583ContagemResultado_TipoRegistro, BC00412_A1585ContagemResultado_Referencia, BC00412_n1585ContagemResultado_Referencia, BC00412_A1586ContagemResultado_Restricoes, BC00412_n1586ContagemResultado_Restricoes, BC00412_A1587ContagemResultado_PrioridadePrevista, BC00412_n1587ContagemResultado_PrioridadePrevista, BC00412_A484ContagemResultado_StatusDmn, BC00412_n484ContagemResultado_StatusDmn, BC00412_A471ContagemResultado_DataDmn,
               BC00412_A1044ContagemResultado_FncUsrCod, BC00412_n1044ContagemResultado_FncUsrCod, BC00412_A1227ContagemResultado_PrazoInicialDias, BC00412_n1227ContagemResultado_PrazoInicialDias, BC00412_A1350ContagemResultado_DataCadastro, BC00412_n1350ContagemResultado_DataCadastro, BC00412_A1173ContagemResultado_OSManual, BC00412_n1173ContagemResultado_OSManual, BC00412_A598ContagemResultado_Baseline, BC00412_n598ContagemResultado_Baseline,
               BC00412_A485ContagemResultado_EhValidacao, BC00412_n485ContagemResultado_EhValidacao, BC00412_A146Modulo_Codigo, BC00412_n146Modulo_Codigo, BC00412_A454ContagemResultado_ContadorFSCod, BC00412_n454ContagemResultado_ContadorFSCod, BC00412_A890ContagemResultado_Responsavel, BC00412_n890ContagemResultado_Responsavel, BC00412_A468ContagemResultado_NaoCnfDmnCod, BC00412_n468ContagemResultado_NaoCnfDmnCod,
               BC00412_A490ContagemResultado_ContratadaCod, BC00412_n490ContagemResultado_ContratadaCod, BC00412_A805ContagemResultado_ContratadaOrigemCod, BC00412_n805ContagemResultado_ContratadaOrigemCod, BC00412_A489ContagemResultado_SistemaCod, BC00412_n489ContagemResultado_SistemaCod, BC00412_A597ContagemResultado_LoteAceiteCod, BC00412_n597ContagemResultado_LoteAceiteCod, BC00412_A1636ContagemResultado_ServicoSS, BC00412_n1636ContagemResultado_ServicoSS,
               BC00412_A1043ContagemResultado_LiqLogCod, BC00412_n1043ContagemResultado_LiqLogCod, BC00412_A1553ContagemResultado_CntSrvCod, BC00412_n1553ContagemResultado_CntSrvCod, BC00412_A602ContagemResultado_OSVinculada, BC00412_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               BC00413_A456ContagemResultado_Codigo, BC00413_A508ContagemResultado_Owner, BC00413_A494ContagemResultado_Descricao, BC00413_n494ContagemResultado_Descricao, BC00413_A514ContagemResultado_Observacao, BC00413_n514ContagemResultado_Observacao, BC00413_A472ContagemResultado_DataEntrega, BC00413_n472ContagemResultado_DataEntrega, BC00413_A912ContagemResultado_HoraEntrega, BC00413_n912ContagemResultado_HoraEntrega,
               BC00413_A1351ContagemResultado_DataPrevista, BC00413_n1351ContagemResultado_DataPrevista, BC00413_A1452ContagemResultado_SS, BC00413_n1452ContagemResultado_SS, BC00413_A493ContagemResultado_DemandaFM, BC00413_n493ContagemResultado_DemandaFM, BC00413_A1584ContagemResultado_UOOwner, BC00413_n1584ContagemResultado_UOOwner, BC00413_A1515ContagemResultado_Evento, BC00413_n1515ContagemResultado_Evento,
               BC00413_A1583ContagemResultado_TipoRegistro, BC00413_A1585ContagemResultado_Referencia, BC00413_n1585ContagemResultado_Referencia, BC00413_A1586ContagemResultado_Restricoes, BC00413_n1586ContagemResultado_Restricoes, BC00413_A1587ContagemResultado_PrioridadePrevista, BC00413_n1587ContagemResultado_PrioridadePrevista, BC00413_A484ContagemResultado_StatusDmn, BC00413_n484ContagemResultado_StatusDmn, BC00413_A471ContagemResultado_DataDmn,
               BC00413_A1044ContagemResultado_FncUsrCod, BC00413_n1044ContagemResultado_FncUsrCod, BC00413_A1227ContagemResultado_PrazoInicialDias, BC00413_n1227ContagemResultado_PrazoInicialDias, BC00413_A1350ContagemResultado_DataCadastro, BC00413_n1350ContagemResultado_DataCadastro, BC00413_A1173ContagemResultado_OSManual, BC00413_n1173ContagemResultado_OSManual, BC00413_A598ContagemResultado_Baseline, BC00413_n598ContagemResultado_Baseline,
               BC00413_A485ContagemResultado_EhValidacao, BC00413_n485ContagemResultado_EhValidacao, BC00413_A146Modulo_Codigo, BC00413_n146Modulo_Codigo, BC00413_A454ContagemResultado_ContadorFSCod, BC00413_n454ContagemResultado_ContadorFSCod, BC00413_A890ContagemResultado_Responsavel, BC00413_n890ContagemResultado_Responsavel, BC00413_A468ContagemResultado_NaoCnfDmnCod, BC00413_n468ContagemResultado_NaoCnfDmnCod,
               BC00413_A490ContagemResultado_ContratadaCod, BC00413_n490ContagemResultado_ContratadaCod, BC00413_A805ContagemResultado_ContratadaOrigemCod, BC00413_n805ContagemResultado_ContratadaOrigemCod, BC00413_A489ContagemResultado_SistemaCod, BC00413_n489ContagemResultado_SistemaCod, BC00413_A597ContagemResultado_LoteAceiteCod, BC00413_n597ContagemResultado_LoteAceiteCod, BC00413_A1636ContagemResultado_ServicoSS, BC00413_n1636ContagemResultado_ServicoSS,
               BC00413_A1043ContagemResultado_LiqLogCod, BC00413_n1043ContagemResultado_LiqLogCod, BC00413_A1553ContagemResultado_CntSrvCod, BC00413_n1553ContagemResultado_CntSrvCod, BC00413_A602ContagemResultado_OSVinculada, BC00413_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               BC00414_A146Modulo_Codigo
               }
               , new Object[] {
               BC00415_A454ContagemResultado_ContadorFSCod
               }
               , new Object[] {
               BC00416_A890ContagemResultado_Responsavel
               }
               , new Object[] {
               BC00417_A468ContagemResultado_NaoCnfDmnCod
               }
               , new Object[] {
               BC00418_A490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               BC00419_A805ContagemResultado_ContratadaOrigemCod
               }
               , new Object[] {
               BC004110_A489ContagemResultado_SistemaCod
               }
               , new Object[] {
               BC004111_A597ContagemResultado_LoteAceiteCod
               }
               , new Object[] {
               BC004112_A1636ContagemResultado_ServicoSS
               }
               , new Object[] {
               BC004113_A1043ContagemResultado_LiqLogCod
               }
               , new Object[] {
               BC004114_A1553ContagemResultado_CntSrvCod
               }
               , new Object[] {
               BC004115_A602ContagemResultado_OSVinculada
               }
               , new Object[] {
               BC004116_A456ContagemResultado_Codigo, BC004116_A508ContagemResultado_Owner, BC004116_A494ContagemResultado_Descricao, BC004116_n494ContagemResultado_Descricao, BC004116_A514ContagemResultado_Observacao, BC004116_n514ContagemResultado_Observacao, BC004116_A472ContagemResultado_DataEntrega, BC004116_n472ContagemResultado_DataEntrega, BC004116_A912ContagemResultado_HoraEntrega, BC004116_n912ContagemResultado_HoraEntrega,
               BC004116_A1351ContagemResultado_DataPrevista, BC004116_n1351ContagemResultado_DataPrevista, BC004116_A1452ContagemResultado_SS, BC004116_n1452ContagemResultado_SS, BC004116_A493ContagemResultado_DemandaFM, BC004116_n493ContagemResultado_DemandaFM, BC004116_A1584ContagemResultado_UOOwner, BC004116_n1584ContagemResultado_UOOwner, BC004116_A1515ContagemResultado_Evento, BC004116_n1515ContagemResultado_Evento,
               BC004116_A1583ContagemResultado_TipoRegistro, BC004116_A1585ContagemResultado_Referencia, BC004116_n1585ContagemResultado_Referencia, BC004116_A1586ContagemResultado_Restricoes, BC004116_n1586ContagemResultado_Restricoes, BC004116_A1587ContagemResultado_PrioridadePrevista, BC004116_n1587ContagemResultado_PrioridadePrevista, BC004116_A484ContagemResultado_StatusDmn, BC004116_n484ContagemResultado_StatusDmn, BC004116_A471ContagemResultado_DataDmn,
               BC004116_A1044ContagemResultado_FncUsrCod, BC004116_n1044ContagemResultado_FncUsrCod, BC004116_A1227ContagemResultado_PrazoInicialDias, BC004116_n1227ContagemResultado_PrazoInicialDias, BC004116_A1350ContagemResultado_DataCadastro, BC004116_n1350ContagemResultado_DataCadastro, BC004116_A1173ContagemResultado_OSManual, BC004116_n1173ContagemResultado_OSManual, BC004116_A598ContagemResultado_Baseline, BC004116_n598ContagemResultado_Baseline,
               BC004116_A485ContagemResultado_EhValidacao, BC004116_n485ContagemResultado_EhValidacao, BC004116_A146Modulo_Codigo, BC004116_n146Modulo_Codigo, BC004116_A454ContagemResultado_ContadorFSCod, BC004116_n454ContagemResultado_ContadorFSCod, BC004116_A890ContagemResultado_Responsavel, BC004116_n890ContagemResultado_Responsavel, BC004116_A468ContagemResultado_NaoCnfDmnCod, BC004116_n468ContagemResultado_NaoCnfDmnCod,
               BC004116_A490ContagemResultado_ContratadaCod, BC004116_n490ContagemResultado_ContratadaCod, BC004116_A805ContagemResultado_ContratadaOrigemCod, BC004116_n805ContagemResultado_ContratadaOrigemCod, BC004116_A489ContagemResultado_SistemaCod, BC004116_n489ContagemResultado_SistemaCod, BC004116_A597ContagemResultado_LoteAceiteCod, BC004116_n597ContagemResultado_LoteAceiteCod, BC004116_A1636ContagemResultado_ServicoSS, BC004116_n1636ContagemResultado_ServicoSS,
               BC004116_A1043ContagemResultado_LiqLogCod, BC004116_n1043ContagemResultado_LiqLogCod, BC004116_A1553ContagemResultado_CntSrvCod, BC004116_n1553ContagemResultado_CntSrvCod, BC004116_A602ContagemResultado_OSVinculada, BC004116_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               BC004117_A456ContagemResultado_Codigo
               }
               , new Object[] {
               BC004118_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004121_A602ContagemResultado_OSVinculada
               }
               , new Object[] {
               BC004122_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               BC004123_A2005ContagemResultadoRequisito_Codigo
               }
               , new Object[] {
               BC004124_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               BC004125_A761ContagemResultadoChckLst_Codigo
               }
               , new Object[] {
               BC004126_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               BC004127_A1769ContagemResultadoArtefato_Codigo
               }
               , new Object[] {
               BC004128_A1685Proposta_Codigo
               }
               , new Object[] {
               BC004129_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               BC004130_A1033ContagemResultadoLiqLog_Codigo, BC004130_A1370ContagemResultadoLiqLogOS_Codigo
               }
               , new Object[] {
               BC004131_A1331ContagemResultadoNota_Codigo
               }
               , new Object[] {
               BC004132_A1314ContagemResultadoIndicadores_DemandaCod, BC004132_A1315ContagemResultadoIndicadores_IndicadorCod
               }
               , new Object[] {
               BC004133_A1241Incidentes_Codigo
               }
               , new Object[] {
               BC004134_A1183AgendaAtendimento_CntSrcCod, BC004134_A1184AgendaAtendimento_Data, BC004134_A1209AgendaAtendimento_CodDmn
               }
               , new Object[] {
               BC004135_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               BC004136_A1895SolicServicoReqNeg_Codigo
               }
               , new Object[] {
               BC004137_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               BC004138_A1412ContagemResultadoNotificacao_Codigo, BC004138_A456ContagemResultado_Codigo
               }
               , new Object[] {
               BC004139_A456ContagemResultado_Codigo, BC004139_A579ContagemResultadoErro_Tipo
               }
               , new Object[] {
               BC004140_A456ContagemResultado_Codigo, BC004140_A473ContagemResultado_DataCnt, BC004140_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               BC004141_A456ContagemResultado_Codigo, BC004141_A508ContagemResultado_Owner, BC004141_A494ContagemResultado_Descricao, BC004141_n494ContagemResultado_Descricao, BC004141_A514ContagemResultado_Observacao, BC004141_n514ContagemResultado_Observacao, BC004141_A472ContagemResultado_DataEntrega, BC004141_n472ContagemResultado_DataEntrega, BC004141_A912ContagemResultado_HoraEntrega, BC004141_n912ContagemResultado_HoraEntrega,
               BC004141_A1351ContagemResultado_DataPrevista, BC004141_n1351ContagemResultado_DataPrevista, BC004141_A1452ContagemResultado_SS, BC004141_n1452ContagemResultado_SS, BC004141_A493ContagemResultado_DemandaFM, BC004141_n493ContagemResultado_DemandaFM, BC004141_A1584ContagemResultado_UOOwner, BC004141_n1584ContagemResultado_UOOwner, BC004141_A1515ContagemResultado_Evento, BC004141_n1515ContagemResultado_Evento,
               BC004141_A1583ContagemResultado_TipoRegistro, BC004141_A1585ContagemResultado_Referencia, BC004141_n1585ContagemResultado_Referencia, BC004141_A1586ContagemResultado_Restricoes, BC004141_n1586ContagemResultado_Restricoes, BC004141_A1587ContagemResultado_PrioridadePrevista, BC004141_n1587ContagemResultado_PrioridadePrevista, BC004141_A484ContagemResultado_StatusDmn, BC004141_n484ContagemResultado_StatusDmn, BC004141_A471ContagemResultado_DataDmn,
               BC004141_A1044ContagemResultado_FncUsrCod, BC004141_n1044ContagemResultado_FncUsrCod, BC004141_A1227ContagemResultado_PrazoInicialDias, BC004141_n1227ContagemResultado_PrazoInicialDias, BC004141_A1350ContagemResultado_DataCadastro, BC004141_n1350ContagemResultado_DataCadastro, BC004141_A1173ContagemResultado_OSManual, BC004141_n1173ContagemResultado_OSManual, BC004141_A598ContagemResultado_Baseline, BC004141_n598ContagemResultado_Baseline,
               BC004141_A485ContagemResultado_EhValidacao, BC004141_n485ContagemResultado_EhValidacao, BC004141_A146Modulo_Codigo, BC004141_n146Modulo_Codigo, BC004141_A454ContagemResultado_ContadorFSCod, BC004141_n454ContagemResultado_ContadorFSCod, BC004141_A890ContagemResultado_Responsavel, BC004141_n890ContagemResultado_Responsavel, BC004141_A468ContagemResultado_NaoCnfDmnCod, BC004141_n468ContagemResultado_NaoCnfDmnCod,
               BC004141_A490ContagemResultado_ContratadaCod, BC004141_n490ContagemResultado_ContratadaCod, BC004141_A805ContagemResultado_ContratadaOrigemCod, BC004141_n805ContagemResultado_ContratadaOrigemCod, BC004141_A489ContagemResultado_SistemaCod, BC004141_n489ContagemResultado_SistemaCod, BC004141_A597ContagemResultado_LoteAceiteCod, BC004141_n597ContagemResultado_LoteAceiteCod, BC004141_A1636ContagemResultado_ServicoSS, BC004141_n1636ContagemResultado_ServicoSS,
               BC004141_A1043ContagemResultado_LiqLogCod, BC004141_n1043ContagemResultado_LiqLogCod, BC004141_A1553ContagemResultado_CntSrvCod, BC004141_n1553ContagemResultado_CntSrvCod, BC004141_A602ContagemResultado_OSVinculada, BC004141_n602ContagemResultado_OSVinculada
               }
            }
         );
         Z598ContagemResultado_Baseline = false;
         n598ContagemResultado_Baseline = false;
         A598ContagemResultado_Baseline = false;
         n598ContagemResultado_Baseline = false;
         i598ContagemResultado_Baseline = false;
         n598ContagemResultado_Baseline = false;
         Z485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         A485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         i485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         Z1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1350ContagemResultado_DataCadastro = false;
         A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1350ContagemResultado_DataCadastro = false;
         i1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1350ContagemResultado_DataCadastro = false;
         Z1515ContagemResultado_Evento = 1;
         n1515ContagemResultado_Evento = false;
         A1515ContagemResultado_Evento = 1;
         n1515ContagemResultado_Evento = false;
         i1515ContagemResultado_Evento = 1;
         n1515ContagemResultado_Evento = false;
         Z1452ContagemResultado_SS = 0;
         n1452ContagemResultado_SS = false;
         A1452ContagemResultado_SS = 0;
         n1452ContagemResultado_SS = false;
         i1452ContagemResultado_SS = 0;
         n1452ContagemResultado_SS = false;
         AV26Pgmname = "SolicitacaoServico_BC";
         Z471ContagemResultado_DataDmn = DateTime.MinValue;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         i471ContagemResultado_DataDmn = DateTime.MinValue;
         Gx_date = DateTimeUtil.Today( context);
         Z508ContagemResultado_Owner = AV7WWPContext.gxTpr_Userid;
         A508ContagemResultado_Owner = AV7WWPContext.gxTpr_Userid;
         i508ContagemResultado_Owner = AV7WWPContext.gxTpr_Userid;
         Z484ContagemResultado_StatusDmn = "S";
         n484ContagemResultado_StatusDmn = false;
         A484ContagemResultado_StatusDmn = "S";
         n484ContagemResultado_StatusDmn = false;
         i484ContagemResultado_StatusDmn = "S";
         n484ContagemResultado_StatusDmn = false;
         Z1583ContagemResultado_TipoRegistro = 2;
         A1583ContagemResultado_TipoRegistro = 2;
         i1583ContagemResultado_TipoRegistro = 2;
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12412 */
         E12412 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z1515ContagemResultado_Evento ;
      private short A1515ContagemResultado_Evento ;
      private short Z1583ContagemResultado_TipoRegistro ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short Z1227ContagemResultado_PrazoInicialDias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short Gx_BScreen ;
      private short RcdFound69 ;
      private short i1515ContagemResultado_Evento ;
      private short i1583ContagemResultado_TipoRegistro ;
      private int trnEnded ;
      private int Z456ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int AV27GXV1 ;
      private int AV23Insert_ContagemResultado_ServicoSS ;
      private int AV11Insert_ContagemResultado_CntSrvCod ;
      private int AV12Insert_ContagemResultado_OSVinculada ;
      private int AV13Insert_ContagemResultado_NaoCnfDmnCod ;
      private int AV14Insert_ContagemResultado_LoteAceiteCod ;
      private int AV15Insert_ContagemResultado_ContratadaOrigemCod ;
      private int AV16Insert_ContagemResultado_ContadorFSCod ;
      private int AV17Insert_ContagemResultado_Responsavel ;
      private int AV18Insert_ContagemResultado_LiqLogCod ;
      private int AV19Insert_Modulo_Codigo ;
      private int AV20Insert_ContagemResultado_ContratadaCod ;
      private int AV21Insert_ContagemResultado_SistemaCod ;
      private int Z508ContagemResultado_Owner ;
      private int A508ContagemResultado_Owner ;
      private int Z1452ContagemResultado_SS ;
      private int A1452ContagemResultado_SS ;
      private int Z1584ContagemResultado_UOOwner ;
      private int A1584ContagemResultado_UOOwner ;
      private int Z1044ContagemResultado_FncUsrCod ;
      private int A1044ContagemResultado_FncUsrCod ;
      private int Z146Modulo_Codigo ;
      private int A146Modulo_Codigo ;
      private int Z454ContagemResultado_ContadorFSCod ;
      private int A454ContagemResultado_ContadorFSCod ;
      private int Z890ContagemResultado_Responsavel ;
      private int A890ContagemResultado_Responsavel ;
      private int Z468ContagemResultado_NaoCnfDmnCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int Z490ContagemResultado_ContratadaCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int Z805ContagemResultado_ContratadaOrigemCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int Z489ContagemResultado_SistemaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int Z597ContagemResultado_LoteAceiteCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int Z1636ContagemResultado_ServicoSS ;
      private int A1636ContagemResultado_ServicoSS ;
      private int Z1043ContagemResultado_LiqLogCod ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int Z1553ContagemResultado_CntSrvCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int Z602ContagemResultado_OSVinculada ;
      private int A602ContagemResultado_OSVinculada ;
      private int i1452ContagemResultado_SS ;
      private int i508ContagemResultado_Owner ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV26Pgmname ;
      private String Z484ContagemResultado_StatusDmn ;
      private String A484ContagemResultado_StatusDmn ;
      private String sMode69 ;
      private String i484ContagemResultado_StatusDmn ;
      private DateTime Z912ContagemResultado_HoraEntrega ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime Z1351ContagemResultado_DataPrevista ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime Z1350ContagemResultado_DataCadastro ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime i1350ContagemResultado_DataCadastro ;
      private DateTime Z472ContagemResultado_DataEntrega ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime Z471ContagemResultado_DataDmn ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime Gx_date ;
      private DateTime i471ContagemResultado_DataDmn ;
      private bool Z1173ContagemResultado_OSManual ;
      private bool A1173ContagemResultado_OSManual ;
      private bool Z598ContagemResultado_Baseline ;
      private bool A598ContagemResultado_Baseline ;
      private bool Z485ContagemResultado_EhValidacao ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool n598ContagemResultado_Baseline ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n1515ContagemResultado_Evento ;
      private bool n1452ContagemResultado_SS ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n494ContagemResultado_Descricao ;
      private bool n514ContagemResultado_Observacao ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1584ContagemResultado_UOOwner ;
      private bool n1585ContagemResultado_Referencia ;
      private bool n1586ContagemResultado_Restricoes ;
      private bool n1587ContagemResultado_PrioridadePrevista ;
      private bool n1044ContagemResultado_FncUsrCod ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n1173ContagemResultado_OSManual ;
      private bool n146Modulo_Codigo ;
      private bool n454ContagemResultado_ContadorFSCod ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool Gx_longc ;
      private bool i598ContagemResultado_Baseline ;
      private bool i485ContagemResultado_EhValidacao ;
      private String Z514ContagemResultado_Observacao ;
      private String A514ContagemResultado_Observacao ;
      private String Z494ContagemResultado_Descricao ;
      private String A494ContagemResultado_Descricao ;
      private String Z493ContagemResultado_DemandaFM ;
      private String A493ContagemResultado_DemandaFM ;
      private String Z1585ContagemResultado_Referencia ;
      private String A1585ContagemResultado_Referencia ;
      private String Z1586ContagemResultado_Restricoes ;
      private String A1586ContagemResultado_Restricoes ;
      private String Z1587ContagemResultado_PrioridadePrevista ;
      private String A1587ContagemResultado_PrioridadePrevista ;
      private IGxSession AV9WebSession ;
      private SdtSolicitacaoServico bcSolicitacaoServico ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC004116_A456ContagemResultado_Codigo ;
      private int[] BC004116_A508ContagemResultado_Owner ;
      private String[] BC004116_A494ContagemResultado_Descricao ;
      private bool[] BC004116_n494ContagemResultado_Descricao ;
      private String[] BC004116_A514ContagemResultado_Observacao ;
      private bool[] BC004116_n514ContagemResultado_Observacao ;
      private DateTime[] BC004116_A472ContagemResultado_DataEntrega ;
      private bool[] BC004116_n472ContagemResultado_DataEntrega ;
      private DateTime[] BC004116_A912ContagemResultado_HoraEntrega ;
      private bool[] BC004116_n912ContagemResultado_HoraEntrega ;
      private DateTime[] BC004116_A1351ContagemResultado_DataPrevista ;
      private bool[] BC004116_n1351ContagemResultado_DataPrevista ;
      private int[] BC004116_A1452ContagemResultado_SS ;
      private bool[] BC004116_n1452ContagemResultado_SS ;
      private String[] BC004116_A493ContagemResultado_DemandaFM ;
      private bool[] BC004116_n493ContagemResultado_DemandaFM ;
      private int[] BC004116_A1584ContagemResultado_UOOwner ;
      private bool[] BC004116_n1584ContagemResultado_UOOwner ;
      private short[] BC004116_A1515ContagemResultado_Evento ;
      private bool[] BC004116_n1515ContagemResultado_Evento ;
      private short[] BC004116_A1583ContagemResultado_TipoRegistro ;
      private String[] BC004116_A1585ContagemResultado_Referencia ;
      private bool[] BC004116_n1585ContagemResultado_Referencia ;
      private String[] BC004116_A1586ContagemResultado_Restricoes ;
      private bool[] BC004116_n1586ContagemResultado_Restricoes ;
      private String[] BC004116_A1587ContagemResultado_PrioridadePrevista ;
      private bool[] BC004116_n1587ContagemResultado_PrioridadePrevista ;
      private String[] BC004116_A484ContagemResultado_StatusDmn ;
      private bool[] BC004116_n484ContagemResultado_StatusDmn ;
      private DateTime[] BC004116_A471ContagemResultado_DataDmn ;
      private int[] BC004116_A1044ContagemResultado_FncUsrCod ;
      private bool[] BC004116_n1044ContagemResultado_FncUsrCod ;
      private short[] BC004116_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] BC004116_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] BC004116_A1350ContagemResultado_DataCadastro ;
      private bool[] BC004116_n1350ContagemResultado_DataCadastro ;
      private bool[] BC004116_A1173ContagemResultado_OSManual ;
      private bool[] BC004116_n1173ContagemResultado_OSManual ;
      private bool[] BC004116_A598ContagemResultado_Baseline ;
      private bool[] BC004116_n598ContagemResultado_Baseline ;
      private bool[] BC004116_A485ContagemResultado_EhValidacao ;
      private bool[] BC004116_n485ContagemResultado_EhValidacao ;
      private int[] BC004116_A146Modulo_Codigo ;
      private bool[] BC004116_n146Modulo_Codigo ;
      private int[] BC004116_A454ContagemResultado_ContadorFSCod ;
      private bool[] BC004116_n454ContagemResultado_ContadorFSCod ;
      private int[] BC004116_A890ContagemResultado_Responsavel ;
      private bool[] BC004116_n890ContagemResultado_Responsavel ;
      private int[] BC004116_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] BC004116_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] BC004116_A490ContagemResultado_ContratadaCod ;
      private bool[] BC004116_n490ContagemResultado_ContratadaCod ;
      private int[] BC004116_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] BC004116_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] BC004116_A489ContagemResultado_SistemaCod ;
      private bool[] BC004116_n489ContagemResultado_SistemaCod ;
      private int[] BC004116_A597ContagemResultado_LoteAceiteCod ;
      private bool[] BC004116_n597ContagemResultado_LoteAceiteCod ;
      private int[] BC004116_A1636ContagemResultado_ServicoSS ;
      private bool[] BC004116_n1636ContagemResultado_ServicoSS ;
      private int[] BC004116_A1043ContagemResultado_LiqLogCod ;
      private bool[] BC004116_n1043ContagemResultado_LiqLogCod ;
      private int[] BC004116_A1553ContagemResultado_CntSrvCod ;
      private bool[] BC004116_n1553ContagemResultado_CntSrvCod ;
      private int[] BC004116_A602ContagemResultado_OSVinculada ;
      private bool[] BC004116_n602ContagemResultado_OSVinculada ;
      private int[] BC004112_A1636ContagemResultado_ServicoSS ;
      private bool[] BC004112_n1636ContagemResultado_ServicoSS ;
      private int[] BC004114_A1553ContagemResultado_CntSrvCod ;
      private bool[] BC004114_n1553ContagemResultado_CntSrvCod ;
      private int[] BC004115_A602ContagemResultado_OSVinculada ;
      private bool[] BC004115_n602ContagemResultado_OSVinculada ;
      private int[] BC00417_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] BC00417_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] BC004111_A597ContagemResultado_LoteAceiteCod ;
      private bool[] BC004111_n597ContagemResultado_LoteAceiteCod ;
      private int[] BC00419_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] BC00419_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] BC00415_A454ContagemResultado_ContadorFSCod ;
      private bool[] BC00415_n454ContagemResultado_ContadorFSCod ;
      private int[] BC00416_A890ContagemResultado_Responsavel ;
      private bool[] BC00416_n890ContagemResultado_Responsavel ;
      private int[] BC004113_A1043ContagemResultado_LiqLogCod ;
      private bool[] BC004113_n1043ContagemResultado_LiqLogCod ;
      private int[] BC00414_A146Modulo_Codigo ;
      private bool[] BC00414_n146Modulo_Codigo ;
      private int[] BC00418_A490ContagemResultado_ContratadaCod ;
      private bool[] BC00418_n490ContagemResultado_ContratadaCod ;
      private int[] BC004110_A489ContagemResultado_SistemaCod ;
      private bool[] BC004110_n489ContagemResultado_SistemaCod ;
      private int[] BC004117_A456ContagemResultado_Codigo ;
      private int[] BC00413_A456ContagemResultado_Codigo ;
      private int[] BC00413_A508ContagemResultado_Owner ;
      private String[] BC00413_A494ContagemResultado_Descricao ;
      private bool[] BC00413_n494ContagemResultado_Descricao ;
      private String[] BC00413_A514ContagemResultado_Observacao ;
      private bool[] BC00413_n514ContagemResultado_Observacao ;
      private DateTime[] BC00413_A472ContagemResultado_DataEntrega ;
      private bool[] BC00413_n472ContagemResultado_DataEntrega ;
      private DateTime[] BC00413_A912ContagemResultado_HoraEntrega ;
      private bool[] BC00413_n912ContagemResultado_HoraEntrega ;
      private DateTime[] BC00413_A1351ContagemResultado_DataPrevista ;
      private bool[] BC00413_n1351ContagemResultado_DataPrevista ;
      private int[] BC00413_A1452ContagemResultado_SS ;
      private bool[] BC00413_n1452ContagemResultado_SS ;
      private String[] BC00413_A493ContagemResultado_DemandaFM ;
      private bool[] BC00413_n493ContagemResultado_DemandaFM ;
      private int[] BC00413_A1584ContagemResultado_UOOwner ;
      private bool[] BC00413_n1584ContagemResultado_UOOwner ;
      private short[] BC00413_A1515ContagemResultado_Evento ;
      private bool[] BC00413_n1515ContagemResultado_Evento ;
      private short[] BC00413_A1583ContagemResultado_TipoRegistro ;
      private String[] BC00413_A1585ContagemResultado_Referencia ;
      private bool[] BC00413_n1585ContagemResultado_Referencia ;
      private String[] BC00413_A1586ContagemResultado_Restricoes ;
      private bool[] BC00413_n1586ContagemResultado_Restricoes ;
      private String[] BC00413_A1587ContagemResultado_PrioridadePrevista ;
      private bool[] BC00413_n1587ContagemResultado_PrioridadePrevista ;
      private String[] BC00413_A484ContagemResultado_StatusDmn ;
      private bool[] BC00413_n484ContagemResultado_StatusDmn ;
      private DateTime[] BC00413_A471ContagemResultado_DataDmn ;
      private int[] BC00413_A1044ContagemResultado_FncUsrCod ;
      private bool[] BC00413_n1044ContagemResultado_FncUsrCod ;
      private short[] BC00413_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] BC00413_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] BC00413_A1350ContagemResultado_DataCadastro ;
      private bool[] BC00413_n1350ContagemResultado_DataCadastro ;
      private bool[] BC00413_A1173ContagemResultado_OSManual ;
      private bool[] BC00413_n1173ContagemResultado_OSManual ;
      private bool[] BC00413_A598ContagemResultado_Baseline ;
      private bool[] BC00413_n598ContagemResultado_Baseline ;
      private bool[] BC00413_A485ContagemResultado_EhValidacao ;
      private bool[] BC00413_n485ContagemResultado_EhValidacao ;
      private int[] BC00413_A146Modulo_Codigo ;
      private bool[] BC00413_n146Modulo_Codigo ;
      private int[] BC00413_A454ContagemResultado_ContadorFSCod ;
      private bool[] BC00413_n454ContagemResultado_ContadorFSCod ;
      private int[] BC00413_A890ContagemResultado_Responsavel ;
      private bool[] BC00413_n890ContagemResultado_Responsavel ;
      private int[] BC00413_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] BC00413_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] BC00413_A490ContagemResultado_ContratadaCod ;
      private bool[] BC00413_n490ContagemResultado_ContratadaCod ;
      private int[] BC00413_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] BC00413_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] BC00413_A489ContagemResultado_SistemaCod ;
      private bool[] BC00413_n489ContagemResultado_SistemaCod ;
      private int[] BC00413_A597ContagemResultado_LoteAceiteCod ;
      private bool[] BC00413_n597ContagemResultado_LoteAceiteCod ;
      private int[] BC00413_A1636ContagemResultado_ServicoSS ;
      private bool[] BC00413_n1636ContagemResultado_ServicoSS ;
      private int[] BC00413_A1043ContagemResultado_LiqLogCod ;
      private bool[] BC00413_n1043ContagemResultado_LiqLogCod ;
      private int[] BC00413_A1553ContagemResultado_CntSrvCod ;
      private bool[] BC00413_n1553ContagemResultado_CntSrvCod ;
      private int[] BC00413_A602ContagemResultado_OSVinculada ;
      private bool[] BC00413_n602ContagemResultado_OSVinculada ;
      private int[] BC00412_A456ContagemResultado_Codigo ;
      private int[] BC00412_A508ContagemResultado_Owner ;
      private String[] BC00412_A494ContagemResultado_Descricao ;
      private bool[] BC00412_n494ContagemResultado_Descricao ;
      private String[] BC00412_A514ContagemResultado_Observacao ;
      private bool[] BC00412_n514ContagemResultado_Observacao ;
      private DateTime[] BC00412_A472ContagemResultado_DataEntrega ;
      private bool[] BC00412_n472ContagemResultado_DataEntrega ;
      private DateTime[] BC00412_A912ContagemResultado_HoraEntrega ;
      private bool[] BC00412_n912ContagemResultado_HoraEntrega ;
      private DateTime[] BC00412_A1351ContagemResultado_DataPrevista ;
      private bool[] BC00412_n1351ContagemResultado_DataPrevista ;
      private int[] BC00412_A1452ContagemResultado_SS ;
      private bool[] BC00412_n1452ContagemResultado_SS ;
      private String[] BC00412_A493ContagemResultado_DemandaFM ;
      private bool[] BC00412_n493ContagemResultado_DemandaFM ;
      private int[] BC00412_A1584ContagemResultado_UOOwner ;
      private bool[] BC00412_n1584ContagemResultado_UOOwner ;
      private short[] BC00412_A1515ContagemResultado_Evento ;
      private bool[] BC00412_n1515ContagemResultado_Evento ;
      private short[] BC00412_A1583ContagemResultado_TipoRegistro ;
      private String[] BC00412_A1585ContagemResultado_Referencia ;
      private bool[] BC00412_n1585ContagemResultado_Referencia ;
      private String[] BC00412_A1586ContagemResultado_Restricoes ;
      private bool[] BC00412_n1586ContagemResultado_Restricoes ;
      private String[] BC00412_A1587ContagemResultado_PrioridadePrevista ;
      private bool[] BC00412_n1587ContagemResultado_PrioridadePrevista ;
      private String[] BC00412_A484ContagemResultado_StatusDmn ;
      private bool[] BC00412_n484ContagemResultado_StatusDmn ;
      private DateTime[] BC00412_A471ContagemResultado_DataDmn ;
      private int[] BC00412_A1044ContagemResultado_FncUsrCod ;
      private bool[] BC00412_n1044ContagemResultado_FncUsrCod ;
      private short[] BC00412_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] BC00412_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] BC00412_A1350ContagemResultado_DataCadastro ;
      private bool[] BC00412_n1350ContagemResultado_DataCadastro ;
      private bool[] BC00412_A1173ContagemResultado_OSManual ;
      private bool[] BC00412_n1173ContagemResultado_OSManual ;
      private bool[] BC00412_A598ContagemResultado_Baseline ;
      private bool[] BC00412_n598ContagemResultado_Baseline ;
      private bool[] BC00412_A485ContagemResultado_EhValidacao ;
      private bool[] BC00412_n485ContagemResultado_EhValidacao ;
      private int[] BC00412_A146Modulo_Codigo ;
      private bool[] BC00412_n146Modulo_Codigo ;
      private int[] BC00412_A454ContagemResultado_ContadorFSCod ;
      private bool[] BC00412_n454ContagemResultado_ContadorFSCod ;
      private int[] BC00412_A890ContagemResultado_Responsavel ;
      private bool[] BC00412_n890ContagemResultado_Responsavel ;
      private int[] BC00412_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] BC00412_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] BC00412_A490ContagemResultado_ContratadaCod ;
      private bool[] BC00412_n490ContagemResultado_ContratadaCod ;
      private int[] BC00412_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] BC00412_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] BC00412_A489ContagemResultado_SistemaCod ;
      private bool[] BC00412_n489ContagemResultado_SistemaCod ;
      private int[] BC00412_A597ContagemResultado_LoteAceiteCod ;
      private bool[] BC00412_n597ContagemResultado_LoteAceiteCod ;
      private int[] BC00412_A1636ContagemResultado_ServicoSS ;
      private bool[] BC00412_n1636ContagemResultado_ServicoSS ;
      private int[] BC00412_A1043ContagemResultado_LiqLogCod ;
      private bool[] BC00412_n1043ContagemResultado_LiqLogCod ;
      private int[] BC00412_A1553ContagemResultado_CntSrvCod ;
      private bool[] BC00412_n1553ContagemResultado_CntSrvCod ;
      private int[] BC00412_A602ContagemResultado_OSVinculada ;
      private bool[] BC00412_n602ContagemResultado_OSVinculada ;
      private int[] BC004118_A456ContagemResultado_Codigo ;
      private int[] BC004121_A602ContagemResultado_OSVinculada ;
      private bool[] BC004121_n602ContagemResultado_OSVinculada ;
      private int[] BC004122_A2024ContagemResultadoNaoCnf_Codigo ;
      private int[] BC004123_A2005ContagemResultadoRequisito_Codigo ;
      private int[] BC004124_A1984ContagemResultadoQA_Codigo ;
      private short[] BC004125_A761ContagemResultadoChckLst_Codigo ;
      private int[] BC004126_A820ContagemResultadoChckLstLog_Codigo ;
      private int[] BC004127_A1769ContagemResultadoArtefato_Codigo ;
      private int[] BC004128_A1685Proposta_Codigo ;
      private int[] BC004129_A1405ContagemResultadoExecucao_Codigo ;
      private int[] BC004130_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] BC004130_A1370ContagemResultadoLiqLogOS_Codigo ;
      private int[] BC004131_A1331ContagemResultadoNota_Codigo ;
      private int[] BC004132_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] BC004132_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] BC004133_A1241Incidentes_Codigo ;
      private int[] BC004134_A1183AgendaAtendimento_CntSrcCod ;
      private DateTime[] BC004134_A1184AgendaAtendimento_Data ;
      private int[] BC004134_A1209AgendaAtendimento_CodDmn ;
      private long[] BC004135_A1797LogResponsavel_Codigo ;
      private long[] BC004136_A1895SolicServicoReqNeg_Codigo ;
      private int[] BC004137_A586ContagemResultadoEvidencia_Codigo ;
      private long[] BC004138_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] BC004138_A456ContagemResultado_Codigo ;
      private int[] BC004139_A456ContagemResultado_Codigo ;
      private String[] BC004139_A579ContagemResultadoErro_Tipo ;
      private int[] BC004140_A456ContagemResultado_Codigo ;
      private DateTime[] BC004140_A473ContagemResultado_DataCnt ;
      private String[] BC004140_A511ContagemResultado_HoraCnt ;
      private int[] BC004141_A456ContagemResultado_Codigo ;
      private int[] BC004141_A508ContagemResultado_Owner ;
      private String[] BC004141_A494ContagemResultado_Descricao ;
      private bool[] BC004141_n494ContagemResultado_Descricao ;
      private String[] BC004141_A514ContagemResultado_Observacao ;
      private bool[] BC004141_n514ContagemResultado_Observacao ;
      private DateTime[] BC004141_A472ContagemResultado_DataEntrega ;
      private bool[] BC004141_n472ContagemResultado_DataEntrega ;
      private DateTime[] BC004141_A912ContagemResultado_HoraEntrega ;
      private bool[] BC004141_n912ContagemResultado_HoraEntrega ;
      private DateTime[] BC004141_A1351ContagemResultado_DataPrevista ;
      private bool[] BC004141_n1351ContagemResultado_DataPrevista ;
      private int[] BC004141_A1452ContagemResultado_SS ;
      private bool[] BC004141_n1452ContagemResultado_SS ;
      private String[] BC004141_A493ContagemResultado_DemandaFM ;
      private bool[] BC004141_n493ContagemResultado_DemandaFM ;
      private int[] BC004141_A1584ContagemResultado_UOOwner ;
      private bool[] BC004141_n1584ContagemResultado_UOOwner ;
      private short[] BC004141_A1515ContagemResultado_Evento ;
      private bool[] BC004141_n1515ContagemResultado_Evento ;
      private short[] BC004141_A1583ContagemResultado_TipoRegistro ;
      private String[] BC004141_A1585ContagemResultado_Referencia ;
      private bool[] BC004141_n1585ContagemResultado_Referencia ;
      private String[] BC004141_A1586ContagemResultado_Restricoes ;
      private bool[] BC004141_n1586ContagemResultado_Restricoes ;
      private String[] BC004141_A1587ContagemResultado_PrioridadePrevista ;
      private bool[] BC004141_n1587ContagemResultado_PrioridadePrevista ;
      private String[] BC004141_A484ContagemResultado_StatusDmn ;
      private bool[] BC004141_n484ContagemResultado_StatusDmn ;
      private DateTime[] BC004141_A471ContagemResultado_DataDmn ;
      private int[] BC004141_A1044ContagemResultado_FncUsrCod ;
      private bool[] BC004141_n1044ContagemResultado_FncUsrCod ;
      private short[] BC004141_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] BC004141_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] BC004141_A1350ContagemResultado_DataCadastro ;
      private bool[] BC004141_n1350ContagemResultado_DataCadastro ;
      private bool[] BC004141_A1173ContagemResultado_OSManual ;
      private bool[] BC004141_n1173ContagemResultado_OSManual ;
      private bool[] BC004141_A598ContagemResultado_Baseline ;
      private bool[] BC004141_n598ContagemResultado_Baseline ;
      private bool[] BC004141_A485ContagemResultado_EhValidacao ;
      private bool[] BC004141_n485ContagemResultado_EhValidacao ;
      private int[] BC004141_A146Modulo_Codigo ;
      private bool[] BC004141_n146Modulo_Codigo ;
      private int[] BC004141_A454ContagemResultado_ContadorFSCod ;
      private bool[] BC004141_n454ContagemResultado_ContadorFSCod ;
      private int[] BC004141_A890ContagemResultado_Responsavel ;
      private bool[] BC004141_n890ContagemResultado_Responsavel ;
      private int[] BC004141_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] BC004141_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] BC004141_A490ContagemResultado_ContratadaCod ;
      private bool[] BC004141_n490ContagemResultado_ContratadaCod ;
      private int[] BC004141_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] BC004141_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] BC004141_A489ContagemResultado_SistemaCod ;
      private bool[] BC004141_n489ContagemResultado_SistemaCod ;
      private int[] BC004141_A597ContagemResultado_LoteAceiteCod ;
      private bool[] BC004141_n597ContagemResultado_LoteAceiteCod ;
      private int[] BC004141_A1636ContagemResultado_ServicoSS ;
      private bool[] BC004141_n1636ContagemResultado_ServicoSS ;
      private int[] BC004141_A1043ContagemResultado_LiqLogCod ;
      private bool[] BC004141_n1043ContagemResultado_LiqLogCod ;
      private int[] BC004141_A1553ContagemResultado_CntSrvCod ;
      private bool[] BC004141_n1553ContagemResultado_CntSrvCod ;
      private int[] BC004141_A602ContagemResultado_OSVinculada ;
      private bool[] BC004141_n602ContagemResultado_OSVinculada ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV7WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV22TrnContextAtt ;
   }

   public class solicitacaoservico_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004116 ;
          prmBC004116 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004112 ;
          prmBC004112 = new Object[] {
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004114 ;
          prmBC004114 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004115 ;
          prmBC004115 = new Object[] {
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00417 ;
          prmBC00417 = new Object[] {
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004111 ;
          prmBC004111 = new Object[] {
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00419 ;
          prmBC00419 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00415 ;
          prmBC00415 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00416 ;
          prmBC00416 = new Object[] {
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004113 ;
          prmBC004113 = new Object[] {
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00414 ;
          prmBC00414 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00418 ;
          prmBC00418 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004110 ;
          prmBC004110 = new Object[] {
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004117 ;
          prmBC004117 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00413 ;
          prmBC00413 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00412 ;
          prmBC00412 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004118 ;
          prmBC004118 = new Object[] {
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_UOOwner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Referencia",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Restricoes",SqlDbType.VarChar,250,0} ,
          new Object[] {"@ContagemResultado_PrioridadePrevista",SqlDbType.VarChar,250,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_FncUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_OSManual",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC004118 ;
          cmdBufferBC004118=" INSERT INTO [ContagemResultado]([ContagemResultado_Owner], [ContagemResultado_Descricao], [ContagemResultado_Observacao], [ContagemResultado_DataEntrega], [ContagemResultado_HoraEntrega], [ContagemResultado_DataPrevista], [ContagemResultado_SS], [ContagemResultado_DemandaFM], [ContagemResultado_UOOwner], [ContagemResultado_Evento], [ContagemResultado_TipoRegistro], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_StatusDmn], [ContagemResultado_DataDmn], [ContagemResultado_FncUsrCod], [ContagemResultado_PrazoInicialDias], [ContagemResultado_DataCadastro], [ContagemResultado_OSManual], [ContagemResultado_Baseline], [ContagemResultado_EhValidacao], [Modulo_Codigo], [ContagemResultado_ContadorFSCod], [ContagemResultado_Responsavel], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_ContratadaCod], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_SistemaCod], [ContagemResultado_LoteAceiteCod], [ContagemResultado_ServicoSS], [ContagemResultado_LiqLogCod], [ContagemResultado_CntSrvCod], [ContagemResultado_OSVinculada], [ContagemResultado_Demanda], [ContagemResultado_Link], [ContagemResultado_ValorPF], [ContagemResultado_Evidencia], [ContagemResultado_PFBFSImp], [ContagemResultado_PFLFSImp], [ContagemResultado_Agrupador], [ContagemResultado_GlsData], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsValor], [ContagemResultado_GlsUser], [ContagemResultado_PBFinal], [ContagemResultado_PLFinal], [ContagemResultado_Custo], [ContagemResultado_PrazoMaisDias], [ContagemResultado_DataHomologacao], [ContagemResultado_DataExecucao], [ContagemResultado_RdmnIssueId], [ContagemResultado_RdmnProjectId], [ContagemResultado_RdmnUpdated], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_CntSrvPrrPrz], "
          + " [ContagemResultado_CntSrvPrrCst], [ContagemResultado_TemDpnHmlg], [ContagemResultado_TmpEstExc], [ContagemResultado_TmpEstCrr], [ContagemResultado_InicioExc], [ContagemResultado_FimExc], [ContagemResultado_InicioCrr], [ContagemResultado_FimCrr], [ContagemResultado_TmpEstAnl], [ContagemResultado_InicioAnl], [ContagemResultado_FimAnl], [ContagemResultado_ProjetoCod], [ContagemResultado_VlrAceite], [ContagemResultado_Combinada], [ContagemResultado_Entrega], [ContagemResultado_DataInicio], [ContagemResultado_SemCusto], [ContagemResultado_VlrCnc], [ContagemResultado_PFCnc], [ContagemResultado_DataPrvPgm], [ContagemResultado_DataEntregaReal], [ContagemResultado_QuantidadeSolicitada]) VALUES(@ContagemResultado_Owner, @ContagemResultado_Descricao, @ContagemResultado_Observacao, @ContagemResultado_DataEntrega, @ContagemResultado_HoraEntrega, @ContagemResultado_DataPrevista, @ContagemResultado_SS, @ContagemResultado_DemandaFM, @ContagemResultado_UOOwner, @ContagemResultado_Evento, @ContagemResultado_TipoRegistro, @ContagemResultado_Referencia, @ContagemResultado_Restricoes, @ContagemResultado_PrioridadePrevista, @ContagemResultado_StatusDmn, @ContagemResultado_DataDmn, @ContagemResultado_FncUsrCod, @ContagemResultado_PrazoInicialDias, @ContagemResultado_DataCadastro, @ContagemResultado_OSManual, @ContagemResultado_Baseline, @ContagemResultado_EhValidacao, @Modulo_Codigo, @ContagemResultado_ContadorFSCod, @ContagemResultado_Responsavel, @ContagemResultado_NaoCnfDmnCod, @ContagemResultado_ContratadaCod, @ContagemResultado_ContratadaOrigemCod, @ContagemResultado_SistemaCod, @ContagemResultado_LoteAceiteCod, @ContagemResultado_ServicoSS, @ContagemResultado_LiqLogCod, @ContagemResultado_CntSrvCod, @ContagemResultado_OSVinculada, '', '', convert(int, 0), '', convert(int, 0), convert(int,"
          + " 0), '', convert( DATETIME, '17530101', 112 ), '', convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0)); SELECT SCOPE_IDENTITY()" ;
          Object[] prmBC004119 ;
          prmBC004119 = new Object[] {
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_UOOwner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Referencia",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Restricoes",SqlDbType.VarChar,250,0} ,
          new Object[] {"@ContagemResultado_PrioridadePrevista",SqlDbType.VarChar,250,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_FncUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_OSManual",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC004119 ;
          cmdBufferBC004119=" UPDATE [ContagemResultado] SET [ContagemResultado_Owner]=@ContagemResultado_Owner, [ContagemResultado_Descricao]=@ContagemResultado_Descricao, [ContagemResultado_Observacao]=@ContagemResultado_Observacao, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_SS]=@ContagemResultado_SS, [ContagemResultado_DemandaFM]=@ContagemResultado_DemandaFM, [ContagemResultado_UOOwner]=@ContagemResultado_UOOwner, [ContagemResultado_Evento]=@ContagemResultado_Evento, [ContagemResultado_TipoRegistro]=@ContagemResultado_TipoRegistro, [ContagemResultado_Referencia]=@ContagemResultado_Referencia, [ContagemResultado_Restricoes]=@ContagemResultado_Restricoes, [ContagemResultado_PrioridadePrevista]=@ContagemResultado_PrioridadePrevista, [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataDmn]=@ContagemResultado_DataDmn, [ContagemResultado_FncUsrCod]=@ContagemResultado_FncUsrCod, [ContagemResultado_PrazoInicialDias]=@ContagemResultado_PrazoInicialDias, [ContagemResultado_DataCadastro]=@ContagemResultado_DataCadastro, [ContagemResultado_OSManual]=@ContagemResultado_OSManual, [ContagemResultado_Baseline]=@ContagemResultado_Baseline, [ContagemResultado_EhValidacao]=@ContagemResultado_EhValidacao, [Modulo_Codigo]=@Modulo_Codigo, [ContagemResultado_ContadorFSCod]=@ContagemResultado_ContadorFSCod, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_NaoCnfDmnCod]=@ContagemResultado_NaoCnfDmnCod, [ContagemResultado_ContratadaCod]=@ContagemResultado_ContratadaCod, [ContagemResultado_ContratadaOrigemCod]=@ContagemResultado_ContratadaOrigemCod, [ContagemResultado_SistemaCod]=@ContagemResultado_SistemaCod, "
          + " [ContagemResultado_LoteAceiteCod]=@ContagemResultado_LoteAceiteCod, [ContagemResultado_ServicoSS]=@ContagemResultado_ServicoSS, [ContagemResultado_LiqLogCod]=@ContagemResultado_LiqLogCod, [ContagemResultado_CntSrvCod]=@ContagemResultado_CntSrvCod, [ContagemResultado_OSVinculada]=@ContagemResultado_OSVinculada  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo" ;
          Object[] prmBC004120 ;
          prmBC004120 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004121 ;
          prmBC004121 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004122 ;
          prmBC004122 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004123 ;
          prmBC004123 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004124 ;
          prmBC004124 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004125 ;
          prmBC004125 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004126 ;
          prmBC004126 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004127 ;
          prmBC004127 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004128 ;
          prmBC004128 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004129 ;
          prmBC004129 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004130 ;
          prmBC004130 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004131 ;
          prmBC004131 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004132 ;
          prmBC004132 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004133 ;
          prmBC004133 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004134 ;
          prmBC004134 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004135 ;
          prmBC004135 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004136 ;
          prmBC004136 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004137 ;
          prmBC004137 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004138 ;
          prmBC004138 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004139 ;
          prmBC004139 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004140 ;
          prmBC004140 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004141 ;
          prmBC004141 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00412", "SELECT [ContagemResultado_Codigo], [ContagemResultado_Owner], [ContagemResultado_Descricao], [ContagemResultado_Observacao], [ContagemResultado_DataEntrega], [ContagemResultado_HoraEntrega], [ContagemResultado_DataPrevista], [ContagemResultado_SS], [ContagemResultado_DemandaFM], [ContagemResultado_UOOwner], [ContagemResultado_Evento], [ContagemResultado_TipoRegistro], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_StatusDmn], [ContagemResultado_DataDmn], [ContagemResultado_FncUsrCod], [ContagemResultado_PrazoInicialDias], [ContagemResultado_DataCadastro], [ContagemResultado_OSManual], [ContagemResultado_Baseline], [ContagemResultado_EhValidacao], [Modulo_Codigo], [ContagemResultado_ContadorFSCod] AS ContagemResultado_ContadorFSCod, [ContagemResultado_Responsavel] AS ContagemResultado_Responsavel, [ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, [ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, [ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, [ContagemResultado_ServicoSS] AS ContagemResultado_ServicoSS, [ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00412,1,0,true,false )
             ,new CursorDef("BC00413", "SELECT [ContagemResultado_Codigo], [ContagemResultado_Owner], [ContagemResultado_Descricao], [ContagemResultado_Observacao], [ContagemResultado_DataEntrega], [ContagemResultado_HoraEntrega], [ContagemResultado_DataPrevista], [ContagemResultado_SS], [ContagemResultado_DemandaFM], [ContagemResultado_UOOwner], [ContagemResultado_Evento], [ContagemResultado_TipoRegistro], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_StatusDmn], [ContagemResultado_DataDmn], [ContagemResultado_FncUsrCod], [ContagemResultado_PrazoInicialDias], [ContagemResultado_DataCadastro], [ContagemResultado_OSManual], [ContagemResultado_Baseline], [ContagemResultado_EhValidacao], [Modulo_Codigo], [ContagemResultado_ContadorFSCod] AS ContagemResultado_ContadorFSCod, [ContagemResultado_Responsavel] AS ContagemResultado_Responsavel, [ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, [ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, [ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, [ContagemResultado_ServicoSS] AS ContagemResultado_ServicoSS, [ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00413,1,0,true,false )
             ,new CursorDef("BC00414", "SELECT [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00414,1,0,true,false )
             ,new CursorDef("BC00415", "SELECT [Usuario_Codigo] AS ContagemResultado_ContadorFSCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_ContadorFSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00415,1,0,true,false )
             ,new CursorDef("BC00416", "SELECT [Usuario_Codigo] AS ContagemResultado_Responsavel FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_Responsavel ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00416,1,0,true,false )
             ,new CursorDef("BC00417", "SELECT [NaoConformidade_Codigo] AS ContagemResultado_NaoCnfDmnCod FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultado_NaoCnfDmnCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00417,1,0,true,false )
             ,new CursorDef("BC00418", "SELECT [Contratada_Codigo] AS ContagemResultado_ContratadaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00418,1,0,true,false )
             ,new CursorDef("BC00419", "SELECT [Contratada_Codigo] AS ContagemResultado_ContratadaOr FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaOrigemCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00419,1,0,true,false )
             ,new CursorDef("BC004110", "SELECT [Sistema_Codigo] AS ContagemResultado_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContagemResultado_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004110,1,0,true,false )
             ,new CursorDef("BC004111", "SELECT [Lote_Codigo] AS ContagemResultado_LoteAceiteCod FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @ContagemResultado_LoteAceiteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004111,1,0,true,false )
             ,new CursorDef("BC004112", "SELECT [Servico_Codigo] AS ContagemResultado_ServicoSS FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_ServicoSS ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004112,1,0,true,false )
             ,new CursorDef("BC004113", "SELECT [ContagemResultadoLiqLog_Codigo] AS ContagemResultado_LiqLogCod FROM [ContagemResultadoLiqLog] WITH (NOLOCK) WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultado_LiqLogCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004113,1,0,true,false )
             ,new CursorDef("BC004114", "SELECT [ContratoServicos_Codigo] AS ContagemResultado_CntSrvCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004114,1,0,true,false )
             ,new CursorDef("BC004115", "SELECT [ContagemResultado_Codigo] AS ContagemResultado_OSVinculada FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_OSVinculada ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004115,1,0,true,false )
             ,new CursorDef("BC004116", "SELECT TM1.[ContagemResultado_Codigo], TM1.[ContagemResultado_Owner], TM1.[ContagemResultado_Descricao], TM1.[ContagemResultado_Observacao], TM1.[ContagemResultado_DataEntrega], TM1.[ContagemResultado_HoraEntrega], TM1.[ContagemResultado_DataPrevista], TM1.[ContagemResultado_SS], TM1.[ContagemResultado_DemandaFM], TM1.[ContagemResultado_UOOwner], TM1.[ContagemResultado_Evento], TM1.[ContagemResultado_TipoRegistro], TM1.[ContagemResultado_Referencia], TM1.[ContagemResultado_Restricoes], TM1.[ContagemResultado_PrioridadePrevista], TM1.[ContagemResultado_StatusDmn], TM1.[ContagemResultado_DataDmn], TM1.[ContagemResultado_FncUsrCod], TM1.[ContagemResultado_PrazoInicialDias], TM1.[ContagemResultado_DataCadastro], TM1.[ContagemResultado_OSManual], TM1.[ContagemResultado_Baseline], TM1.[ContagemResultado_EhValidacao], TM1.[Modulo_Codigo], TM1.[ContagemResultado_ContadorFSCod] AS ContagemResultado_ContadorFSCod, TM1.[ContagemResultado_Responsavel] AS ContagemResultado_Responsavel, TM1.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, TM1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, TM1.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, TM1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, TM1.[ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, TM1.[ContagemResultado_ServicoSS] AS ContagemResultado_ServicoSS, TM1.[ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, TM1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, TM1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada FROM [ContagemResultado] TM1 WITH (NOLOCK) WHERE TM1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY TM1.[ContagemResultado_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004116,100,0,true,false )
             ,new CursorDef("BC004117", "SELECT [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004117,1,0,true,false )
             ,new CursorDef("BC004118", cmdBufferBC004118, GxErrorMask.GX_NOMASK,prmBC004118)
             ,new CursorDef("BC004119", cmdBufferBC004119, GxErrorMask.GX_NOMASK,prmBC004119)
             ,new CursorDef("BC004120", "DELETE FROM [ContagemResultado]  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK,prmBC004120)
             ,new CursorDef("BC004121", "SELECT TOP 1 [ContagemResultado_Codigo] AS ContagemResultado_OSVinculada FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_OSVinculada] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004121,1,0,true,true )
             ,new CursorDef("BC004122", "SELECT TOP 1 [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) WHERE [ContagemResultadoNaoCnf_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004122,1,0,true,true )
             ,new CursorDef("BC004123", "SELECT TOP 1 [ContagemResultadoRequisito_Codigo] FROM [ContagemResultadoRequisito] WITH (NOLOCK) WHERE [ContagemResultadoRequisito_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004123,1,0,true,true )
             ,new CursorDef("BC004124", "SELECT TOP 1 [ContagemResultadoQA_Codigo] FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004124,1,0,true,true )
             ,new CursorDef("BC004125", "SELECT TOP 1 [ContagemResultadoChckLst_Codigo] FROM [ContagemResultadoChckLst] WITH (NOLOCK) WHERE [ContagemResultadoChckLst_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004125,1,0,true,true )
             ,new CursorDef("BC004126", "SELECT TOP 1 [ContagemResultadoChckLstLog_Codigo] FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) WHERE [ContagemResultadoChckLstLog_OSCodigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004126,1,0,true,true )
             ,new CursorDef("BC004127", "SELECT TOP 1 [ContagemResultadoArtefato_Codigo] FROM [ContagemResultadoArtefato] WITH (NOLOCK) WHERE [ContagemResultadoArtefato_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004127,1,0,true,true )
             ,new CursorDef("BC004128", "SELECT TOP 1 [Proposta_Codigo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Proposta_OSCodigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004128,1,0,true,true )
             ,new CursorDef("BC004129", "SELECT TOP 1 [ContagemResultadoExecucao_Codigo] FROM [ContagemResultadoExecucao] WITH (NOLOCK) WHERE [ContagemResultadoExecucao_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004129,1,0,true,true )
             ,new CursorDef("BC004130", "SELECT TOP 1 [ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_Codigo] FROM [ContagemResultadoLiqLogOS] WITH (NOLOCK) WHERE [ContagemResultadoLiqLogOS_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004130,1,0,true,true )
             ,new CursorDef("BC004131", "SELECT TOP 1 [ContagemResultadoNota_Codigo] FROM [ContagemResultadoNotas] WITH (NOLOCK) WHERE [ContagemResultadoNota_DemandaCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004131,1,0,true,true )
             ,new CursorDef("BC004132", "SELECT TOP 1 [ContagemResultadoIndicadores_DemandaCod], [ContagemResultadoIndicadores_IndicadorCod] FROM [ContagemResultadoIndicadores] WITH (NOLOCK) WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004132,1,0,true,true )
             ,new CursorDef("BC004133", "SELECT TOP 1 [Incidentes_Codigo] FROM [ContagemResultadoIncidentes] WITH (NOLOCK) WHERE [Incidentes_DemandaCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004133,1,0,true,true )
             ,new CursorDef("BC004134", "SELECT TOP 1 [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data], [AgendaAtendimento_CodDmn] FROM [AgendaAtendimento] WITH (NOLOCK) WHERE [AgendaAtendimento_CodDmn] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004134,1,0,true,true )
             ,new CursorDef("BC004135", "SELECT TOP 1 [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004135,1,0,true,true )
             ,new CursorDef("BC004136", "SELECT TOP 1 [SolicServicoReqNeg_Codigo] FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004136,1,0,true,true )
             ,new CursorDef("BC004137", "SELECT TOP 1 [ContagemResultadoEvidencia_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004137,1,0,true,true )
             ,new CursorDef("BC004138", "SELECT TOP 1 [ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004138,1,0,true,true )
             ,new CursorDef("BC004139", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo] FROM [ContagemResultadoErro] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004139,1,0,true,true )
             ,new CursorDef("BC004140", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004140,1,0,true,true )
             ,new CursorDef("BC004141", "SELECT TM1.[ContagemResultado_Codigo], TM1.[ContagemResultado_Owner], TM1.[ContagemResultado_Descricao], TM1.[ContagemResultado_Observacao], TM1.[ContagemResultado_DataEntrega], TM1.[ContagemResultado_HoraEntrega], TM1.[ContagemResultado_DataPrevista], TM1.[ContagemResultado_SS], TM1.[ContagemResultado_DemandaFM], TM1.[ContagemResultado_UOOwner], TM1.[ContagemResultado_Evento], TM1.[ContagemResultado_TipoRegistro], TM1.[ContagemResultado_Referencia], TM1.[ContagemResultado_Restricoes], TM1.[ContagemResultado_PrioridadePrevista], TM1.[ContagemResultado_StatusDmn], TM1.[ContagemResultado_DataDmn], TM1.[ContagemResultado_FncUsrCod], TM1.[ContagemResultado_PrazoInicialDias], TM1.[ContagemResultado_DataCadastro], TM1.[ContagemResultado_OSManual], TM1.[ContagemResultado_Baseline], TM1.[ContagemResultado_EhValidacao], TM1.[Modulo_Codigo], TM1.[ContagemResultado_ContadorFSCod] AS ContagemResultado_ContadorFSCod, TM1.[ContagemResultado_Responsavel] AS ContagemResultado_Responsavel, TM1.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, TM1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, TM1.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, TM1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, TM1.[ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, TM1.[ContagemResultado_ServicoSS] AS ContagemResultado_ServicoSS, TM1.[ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, TM1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, TM1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada FROM [ContagemResultado] TM1 WITH (NOLOCK) WHERE TM1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY TM1.[ContagemResultado_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004141,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((short[]) buf[18])[0] = rslt.getShort(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(17) ;
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((short[]) buf[32])[0] = rslt.getShort(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((DateTime[]) buf[34])[0] = rslt.getGXDateTime(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((bool[]) buf[36])[0] = rslt.getBool(21) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((bool[]) buf[38])[0] = rslt.getBool(22) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((bool[]) buf[40])[0] = rslt.getBool(23) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(23);
                ((int[]) buf[42])[0] = rslt.getInt(24) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(24);
                ((int[]) buf[44])[0] = rslt.getInt(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((int[]) buf[46])[0] = rslt.getInt(26) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((int[]) buf[48])[0] = rslt.getInt(27) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(28);
                ((int[]) buf[52])[0] = rslt.getInt(29) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(29);
                ((int[]) buf[54])[0] = rslt.getInt(30) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(30);
                ((int[]) buf[56])[0] = rslt.getInt(31) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(31);
                ((int[]) buf[58])[0] = rslt.getInt(32) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(32);
                ((int[]) buf[60])[0] = rslt.getInt(33) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(33);
                ((int[]) buf[62])[0] = rslt.getInt(34) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(34);
                ((int[]) buf[64])[0] = rslt.getInt(35) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(35);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((short[]) buf[18])[0] = rslt.getShort(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(17) ;
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((short[]) buf[32])[0] = rslt.getShort(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((DateTime[]) buf[34])[0] = rslt.getGXDateTime(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((bool[]) buf[36])[0] = rslt.getBool(21) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((bool[]) buf[38])[0] = rslt.getBool(22) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((bool[]) buf[40])[0] = rslt.getBool(23) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(23);
                ((int[]) buf[42])[0] = rslt.getInt(24) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(24);
                ((int[]) buf[44])[0] = rslt.getInt(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((int[]) buf[46])[0] = rslt.getInt(26) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((int[]) buf[48])[0] = rslt.getInt(27) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(28);
                ((int[]) buf[52])[0] = rslt.getInt(29) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(29);
                ((int[]) buf[54])[0] = rslt.getInt(30) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(30);
                ((int[]) buf[56])[0] = rslt.getInt(31) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(31);
                ((int[]) buf[58])[0] = rslt.getInt(32) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(32);
                ((int[]) buf[60])[0] = rslt.getInt(33) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(33);
                ((int[]) buf[62])[0] = rslt.getInt(34) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(34);
                ((int[]) buf[64])[0] = rslt.getInt(35) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(35);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((short[]) buf[18])[0] = rslt.getShort(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(17) ;
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((short[]) buf[32])[0] = rslt.getShort(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((DateTime[]) buf[34])[0] = rslt.getGXDateTime(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((bool[]) buf[36])[0] = rslt.getBool(21) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((bool[]) buf[38])[0] = rslt.getBool(22) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((bool[]) buf[40])[0] = rslt.getBool(23) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(23);
                ((int[]) buf[42])[0] = rslt.getInt(24) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(24);
                ((int[]) buf[44])[0] = rslt.getInt(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((int[]) buf[46])[0] = rslt.getInt(26) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((int[]) buf[48])[0] = rslt.getInt(27) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(28);
                ((int[]) buf[52])[0] = rslt.getInt(29) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(29);
                ((int[]) buf[54])[0] = rslt.getInt(30) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(30);
                ((int[]) buf[56])[0] = rslt.getInt(31) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(31);
                ((int[]) buf[58])[0] = rslt.getInt(32) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(32);
                ((int[]) buf[60])[0] = rslt.getInt(33) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(33);
                ((int[]) buf[62])[0] = rslt.getInt(34) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(34);
                ((int[]) buf[64])[0] = rslt.getInt(35) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(35);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 33 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 34 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 36 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                return;
             case 39 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((short[]) buf[18])[0] = rslt.getShort(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(17) ;
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((short[]) buf[32])[0] = rslt.getShort(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((DateTime[]) buf[34])[0] = rslt.getGXDateTime(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((bool[]) buf[36])[0] = rslt.getBool(21) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((bool[]) buf[38])[0] = rslt.getBool(22) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((bool[]) buf[40])[0] = rslt.getBool(23) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(23);
                ((int[]) buf[42])[0] = rslt.getInt(24) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(24);
                ((int[]) buf[44])[0] = rslt.getInt(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((int[]) buf[46])[0] = rslt.getInt(26) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((int[]) buf[48])[0] = rslt.getInt(27) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(28);
                ((int[]) buf[52])[0] = rslt.getInt(29) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(29);
                ((int[]) buf[54])[0] = rslt.getInt(30) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(30);
                ((int[]) buf[56])[0] = rslt.getInt(31) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(31);
                ((int[]) buf[58])[0] = rslt.getInt(32) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(32);
                ((int[]) buf[60])[0] = rslt.getInt(33) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(33);
                ((int[]) buf[62])[0] = rslt.getInt(34) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(34);
                ((int[]) buf[64])[0] = rslt.getInt(35) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(35);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[18]);
                }
                stmt.SetParameter(11, (short)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 15 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[27]);
                }
                stmt.SetParameter(16, (DateTime)parms[28]);
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 18 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(18, (short)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 19 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(19, (DateTime)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 20 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(20, (bool)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 21 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(21, (bool)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 22 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(22, (bool)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 25 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(25, (int)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[48]);
                }
                if ( (bool)parms[49] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[50]);
                }
                if ( (bool)parms[51] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[52]);
                }
                if ( (bool)parms[53] )
                {
                   stmt.setNull( 29 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(29, (int)parms[54]);
                }
                if ( (bool)parms[55] )
                {
                   stmt.setNull( 30 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(30, (int)parms[56]);
                }
                if ( (bool)parms[57] )
                {
                   stmt.setNull( 31 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(31, (int)parms[58]);
                }
                if ( (bool)parms[59] )
                {
                   stmt.setNull( 32 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(32, (int)parms[60]);
                }
                if ( (bool)parms[61] )
                {
                   stmt.setNull( 33 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(33, (int)parms[62]);
                }
                if ( (bool)parms[63] )
                {
                   stmt.setNull( 34 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(34, (int)parms[64]);
                }
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[18]);
                }
                stmt.SetParameter(11, (short)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 15 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[27]);
                }
                stmt.SetParameter(16, (DateTime)parms[28]);
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 18 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(18, (short)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 19 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(19, (DateTime)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 20 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(20, (bool)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 21 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(21, (bool)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 22 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(22, (bool)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 25 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(25, (int)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[48]);
                }
                if ( (bool)parms[49] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[50]);
                }
                if ( (bool)parms[51] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[52]);
                }
                if ( (bool)parms[53] )
                {
                   stmt.setNull( 29 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(29, (int)parms[54]);
                }
                if ( (bool)parms[55] )
                {
                   stmt.setNull( 30 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(30, (int)parms[56]);
                }
                if ( (bool)parms[57] )
                {
                   stmt.setNull( 31 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(31, (int)parms[58]);
                }
                if ( (bool)parms[59] )
                {
                   stmt.setNull( 32 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(32, (int)parms[60]);
                }
                if ( (bool)parms[61] )
                {
                   stmt.setNull( 33 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(33, (int)parms[62]);
                }
                if ( (bool)parms[63] )
                {
                   stmt.setNull( 34 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(34, (int)parms[64]);
                }
                stmt.SetParameter(35, (int)parms[65]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 36 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 38 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 39 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
