/*
               File: GetWWContratoDadosCertameFilterData
        Description: Get WWContrato Dados Certame Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:49.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratodadoscertamefilterdata : GXProcedure
   {
      public getwwcontratodadoscertamefilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratodadoscertamefilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV28DDOName = aP0_DDOName;
         this.AV26SearchTxt = aP1_SearchTxt;
         this.AV27SearchTxtTo = aP2_SearchTxtTo;
         this.AV32OptionsJson = "" ;
         this.AV35OptionsDescJson = "" ;
         this.AV37OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV32OptionsJson;
         aP4_OptionsDescJson=this.AV35OptionsDescJson;
         aP5_OptionIndexesJson=this.AV37OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV28DDOName = aP0_DDOName;
         this.AV26SearchTxt = aP1_SearchTxt;
         this.AV27SearchTxtTo = aP2_SearchTxtTo;
         this.AV32OptionsJson = "" ;
         this.AV35OptionsDescJson = "" ;
         this.AV37OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV32OptionsJson;
         aP4_OptionsDescJson=this.AV35OptionsDescJson;
         aP5_OptionIndexesJson=this.AV37OptionIndexesJson;
         return AV37OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratodadoscertamefilterdata objgetwwcontratodadoscertamefilterdata;
         objgetwwcontratodadoscertamefilterdata = new getwwcontratodadoscertamefilterdata();
         objgetwwcontratodadoscertamefilterdata.AV28DDOName = aP0_DDOName;
         objgetwwcontratodadoscertamefilterdata.AV26SearchTxt = aP1_SearchTxt;
         objgetwwcontratodadoscertamefilterdata.AV27SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratodadoscertamefilterdata.AV32OptionsJson = "" ;
         objgetwwcontratodadoscertamefilterdata.AV35OptionsDescJson = "" ;
         objgetwwcontratodadoscertamefilterdata.AV37OptionIndexesJson = "" ;
         objgetwwcontratodadoscertamefilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratodadoscertamefilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratodadoscertamefilterdata);
         aP3_OptionsJson=this.AV32OptionsJson;
         aP4_OptionsDescJson=this.AV35OptionsDescJson;
         aP5_OptionIndexesJson=this.AV37OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratodadoscertamefilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV31Options = (IGxCollection)(new GxSimpleCollection());
         AV34OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV36OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV28DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV28DDOName), "DDO_CONTRATODADOSCERTAME_MODALIDADE") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATODADOSCERTAME_MODALIDADEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV28DDOName), "DDO_CONTRATODADOSCERTAME_SITE") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATODADOSCERTAME_SITEOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV32OptionsJson = AV31Options.ToJSonString(false);
         AV35OptionsDescJson = AV34OptionsDesc.ToJSonString(false);
         AV37OptionIndexesJson = AV36OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV39Session.Get("WWContratoDadosCertameGridState"), "") == 0 )
         {
            AV41GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoDadosCertameGridState"), "");
         }
         else
         {
            AV41GridState.FromXml(AV39Session.Get("WWContratoDadosCertameGridState"), "");
         }
         AV66GXV1 = 1;
         while ( AV66GXV1 <= AV41GridState.gxTpr_Filtervalues.Count )
         {
            AV42GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV41GridState.gxTpr_Filtervalues.Item(AV66GXV1));
            if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_DATA") == 0 )
            {
               AV12TFContratoDadosCertame_Data = context.localUtil.CToD( AV42GridStateFilterValue.gxTpr_Value, 2);
               AV13TFContratoDadosCertame_Data_To = context.localUtil.CToD( AV42GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_MODALIDADE") == 0 )
            {
               AV14TFContratoDadosCertame_Modalidade = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_MODALIDADE_SEL") == 0 )
            {
               AV15TFContratoDadosCertame_Modalidade_Sel = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_NUMERO") == 0 )
            {
               AV16TFContratoDadosCertame_Numero = (long)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Value, "."));
               AV17TFContratoDadosCertame_Numero_To = (long)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_SITE") == 0 )
            {
               AV18TFContratoDadosCertame_Site = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_SITE_SEL") == 0 )
            {
               AV19TFContratoDadosCertame_Site_Sel = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_UASG") == 0 )
            {
               AV20TFContratoDadosCertame_Uasg = (short)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Value, "."));
               AV21TFContratoDadosCertame_Uasg_To = (short)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_DATAHOMOLOGACAO") == 0 )
            {
               AV22TFContratoDadosCertame_DataHomologacao = context.localUtil.CToD( AV42GridStateFilterValue.gxTpr_Value, 2);
               AV23TFContratoDadosCertame_DataHomologacao_To = context.localUtil.CToD( AV42GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATODADOSCERTAME_DATAADJUDICACAO") == 0 )
            {
               AV24TFContratoDadosCertame_DataAdjudicacao = context.localUtil.CToD( AV42GridStateFilterValue.gxTpr_Value, 2);
               AV25TFContratoDadosCertame_DataAdjudicacao_To = context.localUtil.CToD( AV42GridStateFilterValue.gxTpr_Valueto, 2);
            }
            AV66GXV1 = (int)(AV66GXV1+1);
         }
         if ( AV41GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV43GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV41GridState.gxTpr_Dynamicfilters.Item(1));
            AV44DynamicFiltersSelector1 = AV43GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATODADOSCERTAME_DATA") == 0 )
            {
               AV46ContratoDadosCertame_Data1 = context.localUtil.CToD( AV43GridStateDynamicFilter.gxTpr_Value, 2);
               AV47ContratoDadosCertame_Data_To1 = context.localUtil.CToD( AV43GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
            {
               AV45DynamicFiltersOperator1 = AV43GridStateDynamicFilter.gxTpr_Operator;
               AV48ContratoDadosCertame_Modalidade1 = AV43GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATODADOSCERTAME_NUMERO") == 0 )
            {
               AV45DynamicFiltersOperator1 = AV43GridStateDynamicFilter.gxTpr_Operator;
               AV49ContratoDadosCertame_Numero1 = (long)(NumberUtil.Val( AV43GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV41GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV50DynamicFiltersEnabled2 = true;
               AV43GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV41GridState.gxTpr_Dynamicfilters.Item(2));
               AV51DynamicFiltersSelector2 = AV43GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTRATODADOSCERTAME_DATA") == 0 )
               {
                  AV53ContratoDadosCertame_Data2 = context.localUtil.CToD( AV43GridStateDynamicFilter.gxTpr_Value, 2);
                  AV54ContratoDadosCertame_Data_To2 = context.localUtil.CToD( AV43GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
               {
                  AV52DynamicFiltersOperator2 = AV43GridStateDynamicFilter.gxTpr_Operator;
                  AV55ContratoDadosCertame_Modalidade2 = AV43GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTRATODADOSCERTAME_NUMERO") == 0 )
               {
                  AV52DynamicFiltersOperator2 = AV43GridStateDynamicFilter.gxTpr_Operator;
                  AV56ContratoDadosCertame_Numero2 = (long)(NumberUtil.Val( AV43GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV41GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV57DynamicFiltersEnabled3 = true;
                  AV43GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV41GridState.gxTpr_Dynamicfilters.Item(3));
                  AV58DynamicFiltersSelector3 = AV43GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTRATODADOSCERTAME_DATA") == 0 )
                  {
                     AV60ContratoDadosCertame_Data3 = context.localUtil.CToD( AV43GridStateDynamicFilter.gxTpr_Value, 2);
                     AV61ContratoDadosCertame_Data_To3 = context.localUtil.CToD( AV43GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 )
                  {
                     AV59DynamicFiltersOperator3 = AV43GridStateDynamicFilter.gxTpr_Operator;
                     AV62ContratoDadosCertame_Modalidade3 = AV43GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTRATODADOSCERTAME_NUMERO") == 0 )
                  {
                     AV59DynamicFiltersOperator3 = AV43GridStateDynamicFilter.gxTpr_Operator;
                     AV63ContratoDadosCertame_Numero3 = (long)(NumberUtil.Val( AV43GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV26SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1 = AV44DynamicFiltersSelector1;
         AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 = AV45DynamicFiltersOperator1;
         AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1 = AV46ContratoDadosCertame_Data1;
         AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 = AV47ContratoDadosCertame_Data_To1;
         AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = AV48ContratoDadosCertame_Modalidade1;
         AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 = AV49ContratoDadosCertame_Numero1;
         AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 = AV50DynamicFiltersEnabled2;
         AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2 = AV51DynamicFiltersSelector2;
         AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 = AV52DynamicFiltersOperator2;
         AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2 = AV53ContratoDadosCertame_Data2;
         AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 = AV54ContratoDadosCertame_Data_To2;
         AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = AV55ContratoDadosCertame_Modalidade2;
         AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 = AV56ContratoDadosCertame_Numero2;
         AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 = AV57DynamicFiltersEnabled3;
         AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3 = AV58DynamicFiltersSelector3;
         AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 = AV59DynamicFiltersOperator3;
         AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3 = AV60ContratoDadosCertame_Data3;
         AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 = AV61ContratoDadosCertame_Data_To3;
         AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = AV62ContratoDadosCertame_Modalidade3;
         AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 = AV63ContratoDadosCertame_Numero3;
         AV88WWContratoDadosCertameDS_21_Tfcontrato_numero = AV10TFContrato_Numero;
         AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data = AV12TFContratoDadosCertame_Data;
         AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to = AV13TFContratoDadosCertame_Data_To;
         AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = AV14TFContratoDadosCertame_Modalidade;
         AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel = AV15TFContratoDadosCertame_Modalidade_Sel;
         AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero = AV16TFContratoDadosCertame_Numero;
         AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to = AV17TFContratoDadosCertame_Numero_To;
         AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = AV18TFContratoDadosCertame_Site;
         AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel = AV19TFContratoDadosCertame_Site_Sel;
         AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg = AV20TFContratoDadosCertame_Uasg;
         AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to = AV21TFContratoDadosCertame_Uasg_To;
         AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao = AV22TFContratoDadosCertame_DataHomologacao;
         AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to = AV23TFContratoDadosCertame_DataHomologacao_To;
         AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao = AV24TFContratoDadosCertame_DataAdjudicacao;
         AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to = AV25TFContratoDadosCertame_DataAdjudicacao_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1 ,
                                              AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1 ,
                                              AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 ,
                                              AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ,
                                              AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 ,
                                              AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 ,
                                              AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2 ,
                                              AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2 ,
                                              AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 ,
                                              AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 ,
                                              AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ,
                                              AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 ,
                                              AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 ,
                                              AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3 ,
                                              AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3 ,
                                              AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 ,
                                              AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 ,
                                              AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ,
                                              AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 ,
                                              AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel ,
                                              AV88WWContratoDadosCertameDS_21_Tfcontrato_numero ,
                                              AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data ,
                                              AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to ,
                                              AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel ,
                                              AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ,
                                              AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero ,
                                              AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to ,
                                              AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel ,
                                              AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ,
                                              AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg ,
                                              AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to ,
                                              AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao ,
                                              AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to ,
                                              AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao ,
                                              AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to ,
                                              A311ContratoDadosCertame_Data ,
                                              A307ContratoDadosCertame_Modalidade ,
                                              A308ContratoDadosCertame_Numero ,
                                              A77Contrato_Numero ,
                                              A309ContratoDadosCertame_Site ,
                                              A310ContratoDadosCertame_Uasg ,
                                              A312ContratoDadosCertame_DataHomologacao ,
                                              A313ContratoDadosCertame_DataAdjudicacao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN
                                              }
         });
         lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1), "%", "");
         lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1), "%", "");
         lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2), "%", "");
         lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2), "%", "");
         lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3), "%", "");
         lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3), "%", "");
         lV88WWContratoDadosCertameDS_21_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV88WWContratoDadosCertameDS_21_Tfcontrato_numero), 20, "%");
         lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = StringUtil.Concat( StringUtil.RTrim( AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade), "%", "");
         lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = StringUtil.Concat( StringUtil.RTrim( AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site), "%", "");
         /* Using cursor P00KD2 */
         pr_default.execute(0, new Object[] {AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1, AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1, lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1, lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1, AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2, AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2, lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2, lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2, AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3, AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3, lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3, lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3, AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, lV88WWContratoDadosCertameDS_21_Tfcontrato_numero, AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel, AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data, AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to, lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade, AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel, AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero, AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to, lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site, AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel, AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg, AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to, AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao, AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to, AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao, AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKKD2 = false;
            A74Contrato_Codigo = P00KD2_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00KD2_A77Contrato_Numero[0];
            A313ContratoDadosCertame_DataAdjudicacao = P00KD2_A313ContratoDadosCertame_DataAdjudicacao[0];
            n313ContratoDadosCertame_DataAdjudicacao = P00KD2_n313ContratoDadosCertame_DataAdjudicacao[0];
            A312ContratoDadosCertame_DataHomologacao = P00KD2_A312ContratoDadosCertame_DataHomologacao[0];
            n312ContratoDadosCertame_DataHomologacao = P00KD2_n312ContratoDadosCertame_DataHomologacao[0];
            A310ContratoDadosCertame_Uasg = P00KD2_A310ContratoDadosCertame_Uasg[0];
            n310ContratoDadosCertame_Uasg = P00KD2_n310ContratoDadosCertame_Uasg[0];
            A309ContratoDadosCertame_Site = P00KD2_A309ContratoDadosCertame_Site[0];
            n309ContratoDadosCertame_Site = P00KD2_n309ContratoDadosCertame_Site[0];
            A308ContratoDadosCertame_Numero = P00KD2_A308ContratoDadosCertame_Numero[0];
            n308ContratoDadosCertame_Numero = P00KD2_n308ContratoDadosCertame_Numero[0];
            A307ContratoDadosCertame_Modalidade = P00KD2_A307ContratoDadosCertame_Modalidade[0];
            A311ContratoDadosCertame_Data = P00KD2_A311ContratoDadosCertame_Data[0];
            A314ContratoDadosCertame_Codigo = P00KD2_A314ContratoDadosCertame_Codigo[0];
            A77Contrato_Numero = P00KD2_A77Contrato_Numero[0];
            AV38count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00KD2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKKD2 = false;
               A74Contrato_Codigo = P00KD2_A74Contrato_Codigo[0];
               A314ContratoDadosCertame_Codigo = P00KD2_A314ContratoDadosCertame_Codigo[0];
               AV38count = (long)(AV38count+1);
               BRKKD2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV30Option = A77Contrato_Numero;
               AV31Options.Add(AV30Option, 0);
               AV36OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV38count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV31Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKD2 )
            {
               BRKKD2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATODADOSCERTAME_MODALIDADEOPTIONS' Routine */
         AV14TFContratoDadosCertame_Modalidade = AV26SearchTxt;
         AV15TFContratoDadosCertame_Modalidade_Sel = "";
         AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1 = AV44DynamicFiltersSelector1;
         AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 = AV45DynamicFiltersOperator1;
         AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1 = AV46ContratoDadosCertame_Data1;
         AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 = AV47ContratoDadosCertame_Data_To1;
         AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = AV48ContratoDadosCertame_Modalidade1;
         AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 = AV49ContratoDadosCertame_Numero1;
         AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 = AV50DynamicFiltersEnabled2;
         AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2 = AV51DynamicFiltersSelector2;
         AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 = AV52DynamicFiltersOperator2;
         AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2 = AV53ContratoDadosCertame_Data2;
         AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 = AV54ContratoDadosCertame_Data_To2;
         AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = AV55ContratoDadosCertame_Modalidade2;
         AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 = AV56ContratoDadosCertame_Numero2;
         AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 = AV57DynamicFiltersEnabled3;
         AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3 = AV58DynamicFiltersSelector3;
         AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 = AV59DynamicFiltersOperator3;
         AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3 = AV60ContratoDadosCertame_Data3;
         AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 = AV61ContratoDadosCertame_Data_To3;
         AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = AV62ContratoDadosCertame_Modalidade3;
         AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 = AV63ContratoDadosCertame_Numero3;
         AV88WWContratoDadosCertameDS_21_Tfcontrato_numero = AV10TFContrato_Numero;
         AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data = AV12TFContratoDadosCertame_Data;
         AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to = AV13TFContratoDadosCertame_Data_To;
         AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = AV14TFContratoDadosCertame_Modalidade;
         AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel = AV15TFContratoDadosCertame_Modalidade_Sel;
         AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero = AV16TFContratoDadosCertame_Numero;
         AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to = AV17TFContratoDadosCertame_Numero_To;
         AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = AV18TFContratoDadosCertame_Site;
         AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel = AV19TFContratoDadosCertame_Site_Sel;
         AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg = AV20TFContratoDadosCertame_Uasg;
         AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to = AV21TFContratoDadosCertame_Uasg_To;
         AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao = AV22TFContratoDadosCertame_DataHomologacao;
         AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to = AV23TFContratoDadosCertame_DataHomologacao_To;
         AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao = AV24TFContratoDadosCertame_DataAdjudicacao;
         AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to = AV25TFContratoDadosCertame_DataAdjudicacao_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1 ,
                                              AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1 ,
                                              AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 ,
                                              AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ,
                                              AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 ,
                                              AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 ,
                                              AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2 ,
                                              AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2 ,
                                              AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 ,
                                              AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 ,
                                              AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ,
                                              AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 ,
                                              AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 ,
                                              AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3 ,
                                              AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3 ,
                                              AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 ,
                                              AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 ,
                                              AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ,
                                              AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 ,
                                              AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel ,
                                              AV88WWContratoDadosCertameDS_21_Tfcontrato_numero ,
                                              AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data ,
                                              AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to ,
                                              AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel ,
                                              AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ,
                                              AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero ,
                                              AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to ,
                                              AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel ,
                                              AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ,
                                              AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg ,
                                              AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to ,
                                              AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao ,
                                              AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to ,
                                              AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao ,
                                              AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to ,
                                              A311ContratoDadosCertame_Data ,
                                              A307ContratoDadosCertame_Modalidade ,
                                              A308ContratoDadosCertame_Numero ,
                                              A77Contrato_Numero ,
                                              A309ContratoDadosCertame_Site ,
                                              A310ContratoDadosCertame_Uasg ,
                                              A312ContratoDadosCertame_DataHomologacao ,
                                              A313ContratoDadosCertame_DataAdjudicacao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN
                                              }
         });
         lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1), "%", "");
         lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1), "%", "");
         lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2), "%", "");
         lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2), "%", "");
         lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3), "%", "");
         lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3), "%", "");
         lV88WWContratoDadosCertameDS_21_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV88WWContratoDadosCertameDS_21_Tfcontrato_numero), 20, "%");
         lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = StringUtil.Concat( StringUtil.RTrim( AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade), "%", "");
         lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = StringUtil.Concat( StringUtil.RTrim( AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site), "%", "");
         /* Using cursor P00KD3 */
         pr_default.execute(1, new Object[] {AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1, AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1, lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1, lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1, AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2, AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2, lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2, lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2, AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3, AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3, lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3, lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3, AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, lV88WWContratoDadosCertameDS_21_Tfcontrato_numero, AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel, AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data, AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to, lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade, AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel, AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero, AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to, lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site, AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel, AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg, AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to, AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao, AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to, AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao, AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKKD4 = false;
            A74Contrato_Codigo = P00KD3_A74Contrato_Codigo[0];
            A307ContratoDadosCertame_Modalidade = P00KD3_A307ContratoDadosCertame_Modalidade[0];
            A313ContratoDadosCertame_DataAdjudicacao = P00KD3_A313ContratoDadosCertame_DataAdjudicacao[0];
            n313ContratoDadosCertame_DataAdjudicacao = P00KD3_n313ContratoDadosCertame_DataAdjudicacao[0];
            A312ContratoDadosCertame_DataHomologacao = P00KD3_A312ContratoDadosCertame_DataHomologacao[0];
            n312ContratoDadosCertame_DataHomologacao = P00KD3_n312ContratoDadosCertame_DataHomologacao[0];
            A310ContratoDadosCertame_Uasg = P00KD3_A310ContratoDadosCertame_Uasg[0];
            n310ContratoDadosCertame_Uasg = P00KD3_n310ContratoDadosCertame_Uasg[0];
            A309ContratoDadosCertame_Site = P00KD3_A309ContratoDadosCertame_Site[0];
            n309ContratoDadosCertame_Site = P00KD3_n309ContratoDadosCertame_Site[0];
            A77Contrato_Numero = P00KD3_A77Contrato_Numero[0];
            A308ContratoDadosCertame_Numero = P00KD3_A308ContratoDadosCertame_Numero[0];
            n308ContratoDadosCertame_Numero = P00KD3_n308ContratoDadosCertame_Numero[0];
            A311ContratoDadosCertame_Data = P00KD3_A311ContratoDadosCertame_Data[0];
            A314ContratoDadosCertame_Codigo = P00KD3_A314ContratoDadosCertame_Codigo[0];
            A77Contrato_Numero = P00KD3_A77Contrato_Numero[0];
            AV38count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00KD3_A307ContratoDadosCertame_Modalidade[0], A307ContratoDadosCertame_Modalidade) == 0 ) )
            {
               BRKKD4 = false;
               A314ContratoDadosCertame_Codigo = P00KD3_A314ContratoDadosCertame_Codigo[0];
               AV38count = (long)(AV38count+1);
               BRKKD4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A307ContratoDadosCertame_Modalidade)) )
            {
               AV30Option = A307ContratoDadosCertame_Modalidade;
               AV31Options.Add(AV30Option, 0);
               AV36OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV38count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV31Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKD4 )
            {
               BRKKD4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATODADOSCERTAME_SITEOPTIONS' Routine */
         AV18TFContratoDadosCertame_Site = AV26SearchTxt;
         AV19TFContratoDadosCertame_Site_Sel = "";
         AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1 = AV44DynamicFiltersSelector1;
         AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 = AV45DynamicFiltersOperator1;
         AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1 = AV46ContratoDadosCertame_Data1;
         AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 = AV47ContratoDadosCertame_Data_To1;
         AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = AV48ContratoDadosCertame_Modalidade1;
         AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 = AV49ContratoDadosCertame_Numero1;
         AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 = AV50DynamicFiltersEnabled2;
         AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2 = AV51DynamicFiltersSelector2;
         AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 = AV52DynamicFiltersOperator2;
         AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2 = AV53ContratoDadosCertame_Data2;
         AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 = AV54ContratoDadosCertame_Data_To2;
         AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = AV55ContratoDadosCertame_Modalidade2;
         AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 = AV56ContratoDadosCertame_Numero2;
         AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 = AV57DynamicFiltersEnabled3;
         AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3 = AV58DynamicFiltersSelector3;
         AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 = AV59DynamicFiltersOperator3;
         AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3 = AV60ContratoDadosCertame_Data3;
         AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 = AV61ContratoDadosCertame_Data_To3;
         AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = AV62ContratoDadosCertame_Modalidade3;
         AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 = AV63ContratoDadosCertame_Numero3;
         AV88WWContratoDadosCertameDS_21_Tfcontrato_numero = AV10TFContrato_Numero;
         AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data = AV12TFContratoDadosCertame_Data;
         AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to = AV13TFContratoDadosCertame_Data_To;
         AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = AV14TFContratoDadosCertame_Modalidade;
         AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel = AV15TFContratoDadosCertame_Modalidade_Sel;
         AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero = AV16TFContratoDadosCertame_Numero;
         AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to = AV17TFContratoDadosCertame_Numero_To;
         AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = AV18TFContratoDadosCertame_Site;
         AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel = AV19TFContratoDadosCertame_Site_Sel;
         AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg = AV20TFContratoDadosCertame_Uasg;
         AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to = AV21TFContratoDadosCertame_Uasg_To;
         AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao = AV22TFContratoDadosCertame_DataHomologacao;
         AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to = AV23TFContratoDadosCertame_DataHomologacao_To;
         AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao = AV24TFContratoDadosCertame_DataAdjudicacao;
         AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to = AV25TFContratoDadosCertame_DataAdjudicacao_To;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1 ,
                                              AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1 ,
                                              AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 ,
                                              AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 ,
                                              AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ,
                                              AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 ,
                                              AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 ,
                                              AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2 ,
                                              AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2 ,
                                              AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 ,
                                              AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 ,
                                              AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ,
                                              AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 ,
                                              AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 ,
                                              AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3 ,
                                              AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3 ,
                                              AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 ,
                                              AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 ,
                                              AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ,
                                              AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 ,
                                              AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel ,
                                              AV88WWContratoDadosCertameDS_21_Tfcontrato_numero ,
                                              AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data ,
                                              AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to ,
                                              AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel ,
                                              AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ,
                                              AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero ,
                                              AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to ,
                                              AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel ,
                                              AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ,
                                              AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg ,
                                              AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to ,
                                              AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao ,
                                              AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to ,
                                              AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao ,
                                              AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to ,
                                              A311ContratoDadosCertame_Data ,
                                              A307ContratoDadosCertame_Modalidade ,
                                              A308ContratoDadosCertame_Numero ,
                                              A77Contrato_Numero ,
                                              A309ContratoDadosCertame_Site ,
                                              A310ContratoDadosCertame_Uasg ,
                                              A312ContratoDadosCertame_DataHomologacao ,
                                              A313ContratoDadosCertame_DataAdjudicacao },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.LONG,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN
                                              }
         });
         lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1), "%", "");
         lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1), "%", "");
         lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2), "%", "");
         lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = StringUtil.Concat( StringUtil.RTrim( AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2), "%", "");
         lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3), "%", "");
         lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = StringUtil.Concat( StringUtil.RTrim( AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3), "%", "");
         lV88WWContratoDadosCertameDS_21_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV88WWContratoDadosCertameDS_21_Tfcontrato_numero), 20, "%");
         lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = StringUtil.Concat( StringUtil.RTrim( AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade), "%", "");
         lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = StringUtil.Concat( StringUtil.RTrim( AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site), "%", "");
         /* Using cursor P00KD4 */
         pr_default.execute(2, new Object[] {AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1, AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1, lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1, lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1, AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1, AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2, AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2, lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2, lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2, AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2, AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3, AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3, lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3, lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3, AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3, lV88WWContratoDadosCertameDS_21_Tfcontrato_numero, AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel, AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data, AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to, lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade, AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel, AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero, AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to, lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site, AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel, AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg, AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to, AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao, AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to, AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao, AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKKD6 = false;
            A74Contrato_Codigo = P00KD4_A74Contrato_Codigo[0];
            A309ContratoDadosCertame_Site = P00KD4_A309ContratoDadosCertame_Site[0];
            n309ContratoDadosCertame_Site = P00KD4_n309ContratoDadosCertame_Site[0];
            A313ContratoDadosCertame_DataAdjudicacao = P00KD4_A313ContratoDadosCertame_DataAdjudicacao[0];
            n313ContratoDadosCertame_DataAdjudicacao = P00KD4_n313ContratoDadosCertame_DataAdjudicacao[0];
            A312ContratoDadosCertame_DataHomologacao = P00KD4_A312ContratoDadosCertame_DataHomologacao[0];
            n312ContratoDadosCertame_DataHomologacao = P00KD4_n312ContratoDadosCertame_DataHomologacao[0];
            A310ContratoDadosCertame_Uasg = P00KD4_A310ContratoDadosCertame_Uasg[0];
            n310ContratoDadosCertame_Uasg = P00KD4_n310ContratoDadosCertame_Uasg[0];
            A77Contrato_Numero = P00KD4_A77Contrato_Numero[0];
            A308ContratoDadosCertame_Numero = P00KD4_A308ContratoDadosCertame_Numero[0];
            n308ContratoDadosCertame_Numero = P00KD4_n308ContratoDadosCertame_Numero[0];
            A307ContratoDadosCertame_Modalidade = P00KD4_A307ContratoDadosCertame_Modalidade[0];
            A311ContratoDadosCertame_Data = P00KD4_A311ContratoDadosCertame_Data[0];
            A314ContratoDadosCertame_Codigo = P00KD4_A314ContratoDadosCertame_Codigo[0];
            A77Contrato_Numero = P00KD4_A77Contrato_Numero[0];
            AV38count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00KD4_A309ContratoDadosCertame_Site[0], A309ContratoDadosCertame_Site) == 0 ) )
            {
               BRKKD6 = false;
               A314ContratoDadosCertame_Codigo = P00KD4_A314ContratoDadosCertame_Codigo[0];
               AV38count = (long)(AV38count+1);
               BRKKD6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A309ContratoDadosCertame_Site)) )
            {
               AV30Option = A309ContratoDadosCertame_Site;
               AV31Options.Add(AV30Option, 0);
               AV36OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV38count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV31Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKD6 )
            {
               BRKKD6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV31Options = new GxSimpleCollection();
         AV34OptionsDesc = new GxSimpleCollection();
         AV36OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV39Session = context.GetSession();
         AV41GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV42GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContratoDadosCertame_Data = DateTime.MinValue;
         AV13TFContratoDadosCertame_Data_To = DateTime.MinValue;
         AV14TFContratoDadosCertame_Modalidade = "";
         AV15TFContratoDadosCertame_Modalidade_Sel = "";
         AV18TFContratoDadosCertame_Site = "";
         AV19TFContratoDadosCertame_Site_Sel = "";
         AV22TFContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         AV23TFContratoDadosCertame_DataHomologacao_To = DateTime.MinValue;
         AV24TFContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         AV25TFContratoDadosCertame_DataAdjudicacao_To = DateTime.MinValue;
         AV43GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV44DynamicFiltersSelector1 = "";
         AV46ContratoDadosCertame_Data1 = DateTime.MinValue;
         AV47ContratoDadosCertame_Data_To1 = DateTime.MinValue;
         AV48ContratoDadosCertame_Modalidade1 = "";
         AV51DynamicFiltersSelector2 = "";
         AV53ContratoDadosCertame_Data2 = DateTime.MinValue;
         AV54ContratoDadosCertame_Data_To2 = DateTime.MinValue;
         AV55ContratoDadosCertame_Modalidade2 = "";
         AV58DynamicFiltersSelector3 = "";
         AV60ContratoDadosCertame_Data3 = DateTime.MinValue;
         AV61ContratoDadosCertame_Data_To3 = DateTime.MinValue;
         AV62ContratoDadosCertame_Modalidade3 = "";
         AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1 = "";
         AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1 = DateTime.MinValue;
         AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 = DateTime.MinValue;
         AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = "";
         AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2 = "";
         AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2 = DateTime.MinValue;
         AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 = DateTime.MinValue;
         AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = "";
         AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3 = "";
         AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3 = DateTime.MinValue;
         AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 = DateTime.MinValue;
         AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = "";
         AV88WWContratoDadosCertameDS_21_Tfcontrato_numero = "";
         AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel = "";
         AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data = DateTime.MinValue;
         AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to = DateTime.MinValue;
         AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = "";
         AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel = "";
         AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = "";
         AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel = "";
         AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao = DateTime.MinValue;
         AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to = DateTime.MinValue;
         AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao = DateTime.MinValue;
         AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to = DateTime.MinValue;
         scmdbuf = "";
         lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 = "";
         lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 = "";
         lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 = "";
         lV88WWContratoDadosCertameDS_21_Tfcontrato_numero = "";
         lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade = "";
         lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site = "";
         A311ContratoDadosCertame_Data = DateTime.MinValue;
         A307ContratoDadosCertame_Modalidade = "";
         A77Contrato_Numero = "";
         A309ContratoDadosCertame_Site = "";
         A312ContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         A313ContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         P00KD2_A74Contrato_Codigo = new int[1] ;
         P00KD2_A77Contrato_Numero = new String[] {""} ;
         P00KD2_A313ContratoDadosCertame_DataAdjudicacao = new DateTime[] {DateTime.MinValue} ;
         P00KD2_n313ContratoDadosCertame_DataAdjudicacao = new bool[] {false} ;
         P00KD2_A312ContratoDadosCertame_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00KD2_n312ContratoDadosCertame_DataHomologacao = new bool[] {false} ;
         P00KD2_A310ContratoDadosCertame_Uasg = new short[1] ;
         P00KD2_n310ContratoDadosCertame_Uasg = new bool[] {false} ;
         P00KD2_A309ContratoDadosCertame_Site = new String[] {""} ;
         P00KD2_n309ContratoDadosCertame_Site = new bool[] {false} ;
         P00KD2_A308ContratoDadosCertame_Numero = new long[1] ;
         P00KD2_n308ContratoDadosCertame_Numero = new bool[] {false} ;
         P00KD2_A307ContratoDadosCertame_Modalidade = new String[] {""} ;
         P00KD2_A311ContratoDadosCertame_Data = new DateTime[] {DateTime.MinValue} ;
         P00KD2_A314ContratoDadosCertame_Codigo = new int[1] ;
         AV30Option = "";
         P00KD3_A74Contrato_Codigo = new int[1] ;
         P00KD3_A307ContratoDadosCertame_Modalidade = new String[] {""} ;
         P00KD3_A313ContratoDadosCertame_DataAdjudicacao = new DateTime[] {DateTime.MinValue} ;
         P00KD3_n313ContratoDadosCertame_DataAdjudicacao = new bool[] {false} ;
         P00KD3_A312ContratoDadosCertame_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00KD3_n312ContratoDadosCertame_DataHomologacao = new bool[] {false} ;
         P00KD3_A310ContratoDadosCertame_Uasg = new short[1] ;
         P00KD3_n310ContratoDadosCertame_Uasg = new bool[] {false} ;
         P00KD3_A309ContratoDadosCertame_Site = new String[] {""} ;
         P00KD3_n309ContratoDadosCertame_Site = new bool[] {false} ;
         P00KD3_A77Contrato_Numero = new String[] {""} ;
         P00KD3_A308ContratoDadosCertame_Numero = new long[1] ;
         P00KD3_n308ContratoDadosCertame_Numero = new bool[] {false} ;
         P00KD3_A311ContratoDadosCertame_Data = new DateTime[] {DateTime.MinValue} ;
         P00KD3_A314ContratoDadosCertame_Codigo = new int[1] ;
         P00KD4_A74Contrato_Codigo = new int[1] ;
         P00KD4_A309ContratoDadosCertame_Site = new String[] {""} ;
         P00KD4_n309ContratoDadosCertame_Site = new bool[] {false} ;
         P00KD4_A313ContratoDadosCertame_DataAdjudicacao = new DateTime[] {DateTime.MinValue} ;
         P00KD4_n313ContratoDadosCertame_DataAdjudicacao = new bool[] {false} ;
         P00KD4_A312ContratoDadosCertame_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00KD4_n312ContratoDadosCertame_DataHomologacao = new bool[] {false} ;
         P00KD4_A310ContratoDadosCertame_Uasg = new short[1] ;
         P00KD4_n310ContratoDadosCertame_Uasg = new bool[] {false} ;
         P00KD4_A77Contrato_Numero = new String[] {""} ;
         P00KD4_A308ContratoDadosCertame_Numero = new long[1] ;
         P00KD4_n308ContratoDadosCertame_Numero = new bool[] {false} ;
         P00KD4_A307ContratoDadosCertame_Modalidade = new String[] {""} ;
         P00KD4_A311ContratoDadosCertame_Data = new DateTime[] {DateTime.MinValue} ;
         P00KD4_A314ContratoDadosCertame_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratodadoscertamefilterdata__default(),
            new Object[][] {
                new Object[] {
               P00KD2_A74Contrato_Codigo, P00KD2_A77Contrato_Numero, P00KD2_A313ContratoDadosCertame_DataAdjudicacao, P00KD2_n313ContratoDadosCertame_DataAdjudicacao, P00KD2_A312ContratoDadosCertame_DataHomologacao, P00KD2_n312ContratoDadosCertame_DataHomologacao, P00KD2_A310ContratoDadosCertame_Uasg, P00KD2_n310ContratoDadosCertame_Uasg, P00KD2_A309ContratoDadosCertame_Site, P00KD2_n309ContratoDadosCertame_Site,
               P00KD2_A308ContratoDadosCertame_Numero, P00KD2_n308ContratoDadosCertame_Numero, P00KD2_A307ContratoDadosCertame_Modalidade, P00KD2_A311ContratoDadosCertame_Data, P00KD2_A314ContratoDadosCertame_Codigo
               }
               , new Object[] {
               P00KD3_A74Contrato_Codigo, P00KD3_A307ContratoDadosCertame_Modalidade, P00KD3_A313ContratoDadosCertame_DataAdjudicacao, P00KD3_n313ContratoDadosCertame_DataAdjudicacao, P00KD3_A312ContratoDadosCertame_DataHomologacao, P00KD3_n312ContratoDadosCertame_DataHomologacao, P00KD3_A310ContratoDadosCertame_Uasg, P00KD3_n310ContratoDadosCertame_Uasg, P00KD3_A309ContratoDadosCertame_Site, P00KD3_n309ContratoDadosCertame_Site,
               P00KD3_A77Contrato_Numero, P00KD3_A308ContratoDadosCertame_Numero, P00KD3_n308ContratoDadosCertame_Numero, P00KD3_A311ContratoDadosCertame_Data, P00KD3_A314ContratoDadosCertame_Codigo
               }
               , new Object[] {
               P00KD4_A74Contrato_Codigo, P00KD4_A309ContratoDadosCertame_Site, P00KD4_n309ContratoDadosCertame_Site, P00KD4_A313ContratoDadosCertame_DataAdjudicacao, P00KD4_n313ContratoDadosCertame_DataAdjudicacao, P00KD4_A312ContratoDadosCertame_DataHomologacao, P00KD4_n312ContratoDadosCertame_DataHomologacao, P00KD4_A310ContratoDadosCertame_Uasg, P00KD4_n310ContratoDadosCertame_Uasg, P00KD4_A77Contrato_Numero,
               P00KD4_A308ContratoDadosCertame_Numero, P00KD4_n308ContratoDadosCertame_Numero, P00KD4_A307ContratoDadosCertame_Modalidade, P00KD4_A311ContratoDadosCertame_Data, P00KD4_A314ContratoDadosCertame_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV20TFContratoDadosCertame_Uasg ;
      private short AV21TFContratoDadosCertame_Uasg_To ;
      private short AV45DynamicFiltersOperator1 ;
      private short AV52DynamicFiltersOperator2 ;
      private short AV59DynamicFiltersOperator3 ;
      private short AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 ;
      private short AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 ;
      private short AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 ;
      private short AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg ;
      private short AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to ;
      private short A310ContratoDadosCertame_Uasg ;
      private int AV66GXV1 ;
      private int A74Contrato_Codigo ;
      private int A314ContratoDadosCertame_Codigo ;
      private long AV16TFContratoDadosCertame_Numero ;
      private long AV17TFContratoDadosCertame_Numero_To ;
      private long AV49ContratoDadosCertame_Numero1 ;
      private long AV56ContratoDadosCertame_Numero2 ;
      private long AV63ContratoDadosCertame_Numero3 ;
      private long AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 ;
      private long AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 ;
      private long AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 ;
      private long AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero ;
      private long AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to ;
      private long A308ContratoDadosCertame_Numero ;
      private long AV38count ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String AV88WWContratoDadosCertameDS_21_Tfcontrato_numero ;
      private String AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel ;
      private String scmdbuf ;
      private String lV88WWContratoDadosCertameDS_21_Tfcontrato_numero ;
      private String A77Contrato_Numero ;
      private DateTime AV12TFContratoDadosCertame_Data ;
      private DateTime AV13TFContratoDadosCertame_Data_To ;
      private DateTime AV22TFContratoDadosCertame_DataHomologacao ;
      private DateTime AV23TFContratoDadosCertame_DataHomologacao_To ;
      private DateTime AV24TFContratoDadosCertame_DataAdjudicacao ;
      private DateTime AV25TFContratoDadosCertame_DataAdjudicacao_To ;
      private DateTime AV46ContratoDadosCertame_Data1 ;
      private DateTime AV47ContratoDadosCertame_Data_To1 ;
      private DateTime AV53ContratoDadosCertame_Data2 ;
      private DateTime AV54ContratoDadosCertame_Data_To2 ;
      private DateTime AV60ContratoDadosCertame_Data3 ;
      private DateTime AV61ContratoDadosCertame_Data_To3 ;
      private DateTime AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1 ;
      private DateTime AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 ;
      private DateTime AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2 ;
      private DateTime AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 ;
      private DateTime AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3 ;
      private DateTime AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 ;
      private DateTime AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data ;
      private DateTime AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to ;
      private DateTime AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao ;
      private DateTime AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to ;
      private DateTime AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao ;
      private DateTime AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to ;
      private DateTime A311ContratoDadosCertame_Data ;
      private DateTime A312ContratoDadosCertame_DataHomologacao ;
      private DateTime A313ContratoDadosCertame_DataAdjudicacao ;
      private bool returnInSub ;
      private bool AV50DynamicFiltersEnabled2 ;
      private bool AV57DynamicFiltersEnabled3 ;
      private bool AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 ;
      private bool AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 ;
      private bool BRKKD2 ;
      private bool n313ContratoDadosCertame_DataAdjudicacao ;
      private bool n312ContratoDadosCertame_DataHomologacao ;
      private bool n310ContratoDadosCertame_Uasg ;
      private bool n309ContratoDadosCertame_Site ;
      private bool n308ContratoDadosCertame_Numero ;
      private bool BRKKD4 ;
      private bool BRKKD6 ;
      private String AV37OptionIndexesJson ;
      private String AV32OptionsJson ;
      private String AV35OptionsDescJson ;
      private String AV28DDOName ;
      private String AV26SearchTxt ;
      private String AV27SearchTxtTo ;
      private String AV14TFContratoDadosCertame_Modalidade ;
      private String AV15TFContratoDadosCertame_Modalidade_Sel ;
      private String AV18TFContratoDadosCertame_Site ;
      private String AV19TFContratoDadosCertame_Site_Sel ;
      private String AV44DynamicFiltersSelector1 ;
      private String AV48ContratoDadosCertame_Modalidade1 ;
      private String AV51DynamicFiltersSelector2 ;
      private String AV55ContratoDadosCertame_Modalidade2 ;
      private String AV58DynamicFiltersSelector3 ;
      private String AV62ContratoDadosCertame_Modalidade3 ;
      private String AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1 ;
      private String AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ;
      private String AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2 ;
      private String AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ;
      private String AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3 ;
      private String AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ;
      private String AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ;
      private String AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel ;
      private String AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ;
      private String AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel ;
      private String lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ;
      private String lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ;
      private String lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ;
      private String lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ;
      private String lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ;
      private String A307ContratoDadosCertame_Modalidade ;
      private String A309ContratoDadosCertame_Site ;
      private String AV30Option ;
      private IGxSession AV39Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00KD2_A74Contrato_Codigo ;
      private String[] P00KD2_A77Contrato_Numero ;
      private DateTime[] P00KD2_A313ContratoDadosCertame_DataAdjudicacao ;
      private bool[] P00KD2_n313ContratoDadosCertame_DataAdjudicacao ;
      private DateTime[] P00KD2_A312ContratoDadosCertame_DataHomologacao ;
      private bool[] P00KD2_n312ContratoDadosCertame_DataHomologacao ;
      private short[] P00KD2_A310ContratoDadosCertame_Uasg ;
      private bool[] P00KD2_n310ContratoDadosCertame_Uasg ;
      private String[] P00KD2_A309ContratoDadosCertame_Site ;
      private bool[] P00KD2_n309ContratoDadosCertame_Site ;
      private long[] P00KD2_A308ContratoDadosCertame_Numero ;
      private bool[] P00KD2_n308ContratoDadosCertame_Numero ;
      private String[] P00KD2_A307ContratoDadosCertame_Modalidade ;
      private DateTime[] P00KD2_A311ContratoDadosCertame_Data ;
      private int[] P00KD2_A314ContratoDadosCertame_Codigo ;
      private int[] P00KD3_A74Contrato_Codigo ;
      private String[] P00KD3_A307ContratoDadosCertame_Modalidade ;
      private DateTime[] P00KD3_A313ContratoDadosCertame_DataAdjudicacao ;
      private bool[] P00KD3_n313ContratoDadosCertame_DataAdjudicacao ;
      private DateTime[] P00KD3_A312ContratoDadosCertame_DataHomologacao ;
      private bool[] P00KD3_n312ContratoDadosCertame_DataHomologacao ;
      private short[] P00KD3_A310ContratoDadosCertame_Uasg ;
      private bool[] P00KD3_n310ContratoDadosCertame_Uasg ;
      private String[] P00KD3_A309ContratoDadosCertame_Site ;
      private bool[] P00KD3_n309ContratoDadosCertame_Site ;
      private String[] P00KD3_A77Contrato_Numero ;
      private long[] P00KD3_A308ContratoDadosCertame_Numero ;
      private bool[] P00KD3_n308ContratoDadosCertame_Numero ;
      private DateTime[] P00KD3_A311ContratoDadosCertame_Data ;
      private int[] P00KD3_A314ContratoDadosCertame_Codigo ;
      private int[] P00KD4_A74Contrato_Codigo ;
      private String[] P00KD4_A309ContratoDadosCertame_Site ;
      private bool[] P00KD4_n309ContratoDadosCertame_Site ;
      private DateTime[] P00KD4_A313ContratoDadosCertame_DataAdjudicacao ;
      private bool[] P00KD4_n313ContratoDadosCertame_DataAdjudicacao ;
      private DateTime[] P00KD4_A312ContratoDadosCertame_DataHomologacao ;
      private bool[] P00KD4_n312ContratoDadosCertame_DataHomologacao ;
      private short[] P00KD4_A310ContratoDadosCertame_Uasg ;
      private bool[] P00KD4_n310ContratoDadosCertame_Uasg ;
      private String[] P00KD4_A77Contrato_Numero ;
      private long[] P00KD4_A308ContratoDadosCertame_Numero ;
      private bool[] P00KD4_n308ContratoDadosCertame_Numero ;
      private String[] P00KD4_A307ContratoDadosCertame_Modalidade ;
      private DateTime[] P00KD4_A311ContratoDadosCertame_Data ;
      private int[] P00KD4_A314ContratoDadosCertame_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV41GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV42GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV43GridStateDynamicFilter ;
   }

   public class getwwcontratodadoscertamefilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00KD2( IGxContext context ,
                                             String AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1 ,
                                             DateTime AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 ,
                                             short AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ,
                                             long AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 ,
                                             bool AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 ,
                                             String AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2 ,
                                             DateTime AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2 ,
                                             DateTime AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 ,
                                             short AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 ,
                                             String AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ,
                                             long AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 ,
                                             bool AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 ,
                                             String AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3 ,
                                             DateTime AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3 ,
                                             DateTime AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 ,
                                             short AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 ,
                                             String AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ,
                                             long AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 ,
                                             String AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel ,
                                             String AV88WWContratoDadosCertameDS_21_Tfcontrato_numero ,
                                             DateTime AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data ,
                                             DateTime AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to ,
                                             String AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel ,
                                             String AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ,
                                             long AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero ,
                                             long AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to ,
                                             String AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel ,
                                             String AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ,
                                             short AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg ,
                                             short AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to ,
                                             DateTime AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao ,
                                             DateTime AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to ,
                                             DateTime AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao ,
                                             DateTime AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to ,
                                             DateTime A311ContratoDadosCertame_Data ,
                                             String A307ContratoDadosCertame_Modalidade ,
                                             long A308ContratoDadosCertame_Numero ,
                                             String A77Contrato_Numero ,
                                             String A309ContratoDadosCertame_Site ,
                                             short A310ContratoDadosCertame_Uasg ,
                                             DateTime A312ContratoDadosCertame_DataHomologacao ,
                                             DateTime A313ContratoDadosCertame_DataAdjudicacao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [37] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contrato_Numero], T1.[ContratoDadosCertame_DataAdjudicacao], T1.[ContratoDadosCertame_DataHomologacao], T1.[ContratoDadosCertame_Uasg], T1.[ContratoDadosCertame_Site], T1.[ContratoDadosCertame_Numero], T1.[ContratoDadosCertame_Modalidade], T1.[ContratoDadosCertame_Data], T1.[ContratoDadosCertame_Codigo] FROM ([ContratoDadosCertame] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWContratoDadosCertameDS_21_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV88WWContratoDadosCertameDS_21_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV88WWContratoDadosCertameDS_21_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] = @AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] = @AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( ! (0==AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] >= @AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] >= @AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( ! (0==AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] <= @AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] <= @AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to)";
            }
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] like @lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] like @lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)";
            }
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] = @AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] = @AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)";
            }
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( ! (0==AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] >= @AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] >= @AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg)";
            }
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( ! (0==AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] <= @AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] <= @AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to)";
            }
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] >= @AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] >= @AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao)";
            }
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( ! (DateTime.MinValue==AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] <= @AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] <= @AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to)";
            }
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( ! (DateTime.MinValue==AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao)";
            }
         }
         else
         {
            GXv_int1[35] = 1;
         }
         if ( ! (DateTime.MinValue==AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to)";
            }
         }
         else
         {
            GXv_int1[36] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00KD3( IGxContext context ,
                                             String AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1 ,
                                             DateTime AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 ,
                                             short AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ,
                                             long AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 ,
                                             bool AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 ,
                                             String AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2 ,
                                             DateTime AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2 ,
                                             DateTime AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 ,
                                             short AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 ,
                                             String AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ,
                                             long AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 ,
                                             bool AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 ,
                                             String AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3 ,
                                             DateTime AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3 ,
                                             DateTime AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 ,
                                             short AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 ,
                                             String AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ,
                                             long AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 ,
                                             String AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel ,
                                             String AV88WWContratoDadosCertameDS_21_Tfcontrato_numero ,
                                             DateTime AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data ,
                                             DateTime AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to ,
                                             String AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel ,
                                             String AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ,
                                             long AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero ,
                                             long AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to ,
                                             String AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel ,
                                             String AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ,
                                             short AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg ,
                                             short AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to ,
                                             DateTime AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao ,
                                             DateTime AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to ,
                                             DateTime AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao ,
                                             DateTime AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to ,
                                             DateTime A311ContratoDadosCertame_Data ,
                                             String A307ContratoDadosCertame_Modalidade ,
                                             long A308ContratoDadosCertame_Numero ,
                                             String A77Contrato_Numero ,
                                             String A309ContratoDadosCertame_Site ,
                                             short A310ContratoDadosCertame_Uasg ,
                                             DateTime A312ContratoDadosCertame_DataHomologacao ,
                                             DateTime A313ContratoDadosCertame_DataAdjudicacao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [37] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[ContratoDadosCertame_Modalidade], T1.[ContratoDadosCertame_DataAdjudicacao], T1.[ContratoDadosCertame_DataHomologacao], T1.[ContratoDadosCertame_Uasg], T1.[ContratoDadosCertame_Site], T2.[Contrato_Numero], T1.[ContratoDadosCertame_Numero], T1.[ContratoDadosCertame_Data], T1.[ContratoDadosCertame_Codigo] FROM ([ContratoDadosCertame] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWContratoDadosCertameDS_21_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV88WWContratoDadosCertameDS_21_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV88WWContratoDadosCertameDS_21_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] = @AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] = @AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! (0==AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] >= @AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] >= @AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( ! (0==AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] <= @AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] <= @AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to)";
            }
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] like @lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] like @lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)";
            }
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] = @AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] = @AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)";
            }
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( ! (0==AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] >= @AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] >= @AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg)";
            }
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( ! (0==AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] <= @AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] <= @AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to)";
            }
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] >= @AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] >= @AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao)";
            }
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( ! (DateTime.MinValue==AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] <= @AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] <= @AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to)";
            }
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( ! (DateTime.MinValue==AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao)";
            }
         }
         else
         {
            GXv_int3[35] = 1;
         }
         if ( ! (DateTime.MinValue==AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to)";
            }
         }
         else
         {
            GXv_int3[36] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoDadosCertame_Modalidade]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00KD4( IGxContext context ,
                                             String AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1 ,
                                             DateTime AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1 ,
                                             short AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 ,
                                             String AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1 ,
                                             long AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1 ,
                                             bool AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 ,
                                             String AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2 ,
                                             DateTime AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2 ,
                                             DateTime AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2 ,
                                             short AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 ,
                                             String AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2 ,
                                             long AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2 ,
                                             bool AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 ,
                                             String AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3 ,
                                             DateTime AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3 ,
                                             DateTime AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3 ,
                                             short AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 ,
                                             String AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3 ,
                                             long AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3 ,
                                             String AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel ,
                                             String AV88WWContratoDadosCertameDS_21_Tfcontrato_numero ,
                                             DateTime AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data ,
                                             DateTime AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to ,
                                             String AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel ,
                                             String AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade ,
                                             long AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero ,
                                             long AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to ,
                                             String AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel ,
                                             String AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site ,
                                             short AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg ,
                                             short AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to ,
                                             DateTime AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao ,
                                             DateTime AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to ,
                                             DateTime AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao ,
                                             DateTime AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to ,
                                             DateTime A311ContratoDadosCertame_Data ,
                                             String A307ContratoDadosCertame_Modalidade ,
                                             long A308ContratoDadosCertame_Numero ,
                                             String A77Contrato_Numero ,
                                             String A309ContratoDadosCertame_Site ,
                                             short A310ContratoDadosCertame_Uasg ,
                                             DateTime A312ContratoDadosCertame_DataHomologacao ,
                                             DateTime A313ContratoDadosCertame_DataAdjudicacao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [37] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[ContratoDadosCertame_Site], T1.[ContratoDadosCertame_DataAdjudicacao], T1.[ContratoDadosCertame_DataHomologacao], T1.[ContratoDadosCertame_Uasg], T2.[Contrato_Numero], T1.[ContratoDadosCertame_Numero], T1.[ContratoDadosCertame_Modalidade], T1.[ContratoDadosCertame_Data], T1.[ContratoDadosCertame_Codigo] FROM ([ContratoDadosCertame] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV68WWContratoDadosCertameDS_1_Dynamicfiltersselector1, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV69WWContratoDadosCertameDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV74WWContratoDadosCertameDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV75WWContratoDadosCertameDS_8_Dynamicfiltersselector2, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV76WWContratoDadosCertameDS_9_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_DATA") == 0 ) && ( ! (DateTime.MinValue==AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_MODALIDADE") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like '%' + @lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] < @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] < @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] = @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] = @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( AV81WWContratoDadosCertameDS_14_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV82WWContratoDadosCertameDS_15_Dynamicfiltersselector3, "CONTRATODADOSCERTAME_NUMERO") == 0 ) && ( AV83WWContratoDadosCertameDS_16_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] > @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] > @AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWContratoDadosCertameDS_21_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV88WWContratoDadosCertameDS_21_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV88WWContratoDadosCertameDS_21_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] >= @AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] >= @AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Data] <= @AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Data] <= @AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] like @lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] like @lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Modalidade] = @AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Modalidade] = @AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel)";
            }
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( ! (0==AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] >= @AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] >= @AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero)";
            }
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( ! (0==AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Numero] <= @AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Numero] <= @AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to)";
            }
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] like @lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] like @lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site)";
            }
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Site] = @AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Site] = @AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel)";
            }
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( ! (0==AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] >= @AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] >= @AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg)";
            }
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( ! (0==AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_Uasg] <= @AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_Uasg] <= @AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to)";
            }
         }
         else
         {
            GXv_int5[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] >= @AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] >= @AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao)";
            }
         }
         else
         {
            GXv_int5[33] = 1;
         }
         if ( ! (DateTime.MinValue==AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataHomologacao] <= @AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataHomologacao] <= @AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to)";
            }
         }
         else
         {
            GXv_int5[34] = 1;
         }
         if ( ! (DateTime.MinValue==AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] >= @AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao)";
            }
         }
         else
         {
            GXv_int5[35] = 1;
         }
         if ( ! (DateTime.MinValue==AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoDadosCertame_DataAdjudicacao] <= @AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to)";
            }
         }
         else
         {
            GXv_int5[36] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoDadosCertame_Site]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00KD2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (long)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (long)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (long)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (long)dynConstraints[26] , (long)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (short)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (String)dynConstraints[37] , (long)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (short)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] );
               case 1 :
                     return conditional_P00KD3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (long)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (long)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (long)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (long)dynConstraints[26] , (long)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (short)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (String)dynConstraints[37] , (long)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (short)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] );
               case 2 :
                     return conditional_P00KD4(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (long)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (long)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (long)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (long)dynConstraints[26] , (long)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (short)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (String)dynConstraints[37] , (long)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (short)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00KD2 ;
          prmP00KD2 = new Object[] {
          new Object[] {"@AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@lV88WWContratoDadosCertameDS_21_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to",SqlDbType.Decimal,10,0} ,
          new Object[] {"@lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP00KD3 ;
          prmP00KD3 = new Object[] {
          new Object[] {"@AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@lV88WWContratoDadosCertameDS_21_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to",SqlDbType.Decimal,10,0} ,
          new Object[] {"@lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP00KD4 ;
          prmP00KD4 = new Object[] {
          new Object[] {"@AV70WWContratoDadosCertameDS_3_Contratodadoscertame_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV71WWContratoDadosCertameDS_4_Contratodadoscertame_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV72WWContratoDadosCertameDS_5_Contratodadoscertame_modalidade1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV73WWContratoDadosCertameDS_6_Contratodadoscertame_numero1",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV77WWContratoDadosCertameDS_10_Contratodadoscertame_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV78WWContratoDadosCertameDS_11_Contratodadoscertame_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV79WWContratoDadosCertameDS_12_Contratodadoscertame_modalidade2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV80WWContratoDadosCertameDS_13_Contratodadoscertame_numero2",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV84WWContratoDadosCertameDS_17_Contratodadoscertame_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85WWContratoDadosCertameDS_18_Contratodadoscertame_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV86WWContratoDadosCertameDS_19_Contratodadoscertame_modalidade3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV87WWContratoDadosCertameDS_20_Contratodadoscertame_numero3",SqlDbType.Decimal,10,0} ,
          new Object[] {"@lV88WWContratoDadosCertameDS_21_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV89WWContratoDadosCertameDS_22_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV90WWContratoDadosCertameDS_23_Tfcontratodadoscertame_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV91WWContratoDadosCertameDS_24_Tfcontratodadoscertame_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV92WWContratoDadosCertameDS_25_Tfcontratodadoscertame_modalidade",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV93WWContratoDadosCertameDS_26_Tfcontratodadoscertame_modalidade_sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV94WWContratoDadosCertameDS_27_Tfcontratodadoscertame_numero",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV95WWContratoDadosCertameDS_28_Tfcontratodadoscertame_numero_to",SqlDbType.Decimal,10,0} ,
          new Object[] {"@lV96WWContratoDadosCertameDS_29_Tfcontratodadoscertame_site",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV97WWContratoDadosCertameDS_30_Tfcontratodadoscertame_site_sel",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@AV98WWContratoDadosCertameDS_31_Tfcontratodadoscertame_uasg",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV99WWContratoDadosCertameDS_32_Tfcontratodadoscertame_uasg_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV100WWContratoDadosCertameDS_33_Tfcontratodadoscertame_datahomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWContratoDadosCertameDS_34_Tfcontratodadoscertame_datahomologacao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WWContratoDadosCertameDS_35_Tfcontratodadoscertame_dataadjudicacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WWContratoDadosCertameDS_36_Tfcontratodadoscertame_dataadjudicacao_to",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00KD2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KD2,100,0,true,false )
             ,new CursorDef("P00KD3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KD3,100,0,true,false )
             ,new CursorDef("P00KD4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KD4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((long[]) buf[10])[0] = rslt.getLong(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 20) ;
                ((long[]) buf[11])[0] = rslt.getLong(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 20) ;
                ((long[]) buf[10])[0] = rslt.getLong(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[41]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[42]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[43]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[48]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[49]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[50]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[55]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[56]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[57]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[64]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[65]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[68]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[69]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[72]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[41]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[42]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[43]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[48]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[49]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[50]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[55]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[56]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[57]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[64]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[65]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[68]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[69]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[72]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[41]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[42]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[43]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[48]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[49]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[50]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[55]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[56]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[57]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[64]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[65]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[68]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[69]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[71]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[72]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratodadoscertamefilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratodadoscertamefilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratodadoscertamefilterdata") )
          {
             return  ;
          }
          getwwcontratodadoscertamefilterdata worker = new getwwcontratodadoscertamefilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
