/*
               File: GetWWAgendaAtendimentoFilterData
        Description: Get WWAgenda Atendimento Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:7:37.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwagendaatendimentofilterdata : GXProcedure
   {
      public getwwagendaatendimentofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwagendaatendimentofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwagendaatendimentofilterdata objgetwwagendaatendimentofilterdata;
         objgetwwagendaatendimentofilterdata = new getwwagendaatendimentofilterdata();
         objgetwwagendaatendimentofilterdata.AV18DDOName = aP0_DDOName;
         objgetwwagendaatendimentofilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwagendaatendimentofilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwagendaatendimentofilterdata.AV22OptionsJson = "" ;
         objgetwwagendaatendimentofilterdata.AV25OptionsDescJson = "" ;
         objgetwwagendaatendimentofilterdata.AV27OptionIndexesJson = "" ;
         objgetwwagendaatendimentofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwagendaatendimentofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwagendaatendimentofilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwagendaatendimentofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DEMANDAFMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DEMANDAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTAGEMRESULTADO_CNTSRVALS") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_CNTSRVALSOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWAgendaAtendimentoGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWAgendaAtendimentoGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWAgendaAtendimentoGridState"), "");
         }
         AV66GXV1 = 1;
         while ( AV66GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV66GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV61Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFAGENDAATENDIMENTO_DATA") == 0 )
            {
               AV10TFAgendaAtendimento_Data = context.localUtil.CToD( AV32GridStateFilterValue.gxTpr_Value, 2);
               AV11TFAgendaAtendimento_Data_To = context.localUtil.CToD( AV32GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFAGENDAATENDIMENTO_CODDMN") == 0 )
            {
               AV56TFAgendaAtendimento_CodDmn = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
               AV57TFAgendaAtendimento_CodDmn_To = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV48TFContagemResultado_DemandaFM = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM_SEL") == 0 )
            {
               AV49TFContagemResultado_DemandaFM_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV12TFContagemResultado_Demanda = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA_SEL") == 0 )
            {
               AV13TFContagemResultado_Demanda_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CNTSRVALS") == 0 )
            {
               AV62TFContagemResultado_CntSrvAls = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CNTSRVALS_SEL") == 0 )
            {
               AV63TFContagemResultado_CntSrvAls_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFAGENDAATENDIMENTO_QTDUND") == 0 )
            {
               AV14TFAgendaAtendimento_QtdUnd = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV15TFAgendaAtendimento_QtdUnd_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV66GXV1 = (int)(AV66GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "AGENDAATENDIMENTO_DATA") == 0 )
            {
               AV35AgendaAtendimento_Data1 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Value, 2);
               AV36AgendaAtendimento_Data_To1 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV50DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV51ContagemResultado_DemandaFM1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV50DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV37ContagemResultado_Demanda1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "AGENDAATENDIMENTO_CODDMN") == 0 )
            {
               AV58AgendaAtendimento_CodDmn1 = (int)(NumberUtil.Val( AV33GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV38DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV39DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "AGENDAATENDIMENTO_DATA") == 0 )
               {
                  AV40AgendaAtendimento_Data2 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Value, 2);
                  AV41AgendaAtendimento_Data_To2 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
               {
                  AV52DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV53ContagemResultado_DemandaFM2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 )
               {
                  AV52DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV42ContagemResultado_Demanda2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "AGENDAATENDIMENTO_CODDMN") == 0 )
               {
                  AV59AgendaAtendimento_CodDmn2 = (int)(NumberUtil.Val( AV33GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV43DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV44DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "AGENDAATENDIMENTO_DATA") == 0 )
                  {
                     AV45AgendaAtendimento_Data3 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Value, 2);
                     AV46AgendaAtendimento_Data_To3 = context.localUtil.CToD( AV33GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
                  {
                     AV54DynamicFiltersOperator3 = AV33GridStateDynamicFilter.gxTpr_Operator;
                     AV55ContagemResultado_DemandaFM3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 )
                  {
                     AV54DynamicFiltersOperator3 = AV33GridStateDynamicFilter.gxTpr_Operator;
                     AV47ContagemResultado_Demanda3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "AGENDAATENDIMENTO_CODDMN") == 0 )
                  {
                     AV60AgendaAtendimento_CodDmn3 = (int)(NumberUtil.Val( AV33GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADO_DEMANDAFMOPTIONS' Routine */
         AV48TFContagemResultado_DemandaFM = AV16SearchTxt;
         AV49TFContagemResultado_DemandaFM_Sel = "";
         AV68WWAgendaAtendimentoDS_1_Contratada_areatrabalhocod = AV61Contratada_AreaTrabalhoCod;
         AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 = AV50DynamicFiltersOperator1;
         AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1 = AV35AgendaAtendimento_Data1;
         AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 = AV36AgendaAtendimento_Data_To1;
         AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = AV51ContagemResultado_DemandaFM1;
         AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = AV37ContagemResultado_Demanda1;
         AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 = AV58AgendaAtendimento_CodDmn1;
         AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 = AV52DynamicFiltersOperator2;
         AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2 = AV40AgendaAtendimento_Data2;
         AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 = AV41AgendaAtendimento_Data_To2;
         AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = AV53ContagemResultado_DemandaFM2;
         AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = AV42ContagemResultado_Demanda2;
         AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 = AV59AgendaAtendimento_CodDmn2;
         AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 = AV43DynamicFiltersEnabled3;
         AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 = AV44DynamicFiltersSelector3;
         AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 = AV54DynamicFiltersOperator3;
         AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3 = AV45AgendaAtendimento_Data3;
         AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 = AV46AgendaAtendimento_Data_To3;
         AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = AV55ContagemResultado_DemandaFM3;
         AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = AV47ContagemResultado_Demanda3;
         AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 = AV60AgendaAtendimento_CodDmn3;
         AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data = AV10TFAgendaAtendimento_Data;
         AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to = AV11TFAgendaAtendimento_Data_To;
         AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn = AV56TFAgendaAtendimento_CodDmn;
         AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to = AV57TFAgendaAtendimento_CodDmn_To;
         AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = AV48TFContagemResultado_DemandaFM;
         AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel = AV49TFContagemResultado_DemandaFM_Sel;
         AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = AV12TFContagemResultado_Demanda;
         AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel = AV13TFContagemResultado_Demanda_Sel;
         AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = AV62TFContagemResultado_CntSrvAls;
         AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel = AV63TFContagemResultado_CntSrvAls_Sel;
         AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund = AV14TFAgendaAtendimento_QtdUnd;
         AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to = AV15TFAgendaAtendimento_QtdUnd_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 ,
                                              AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1 ,
                                              AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 ,
                                              AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 ,
                                              AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ,
                                              AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ,
                                              AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 ,
                                              AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 ,
                                              AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 ,
                                              AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2 ,
                                              AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 ,
                                              AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 ,
                                              AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ,
                                              AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ,
                                              AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 ,
                                              AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 ,
                                              AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 ,
                                              AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3 ,
                                              AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 ,
                                              AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 ,
                                              AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ,
                                              AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ,
                                              AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 ,
                                              AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data ,
                                              AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to ,
                                              AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn ,
                                              AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to ,
                                              AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel ,
                                              AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ,
                                              AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel ,
                                              AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ,
                                              AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel ,
                                              AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ,
                                              AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund ,
                                              AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to ,
                                              A1184AgendaAtendimento_Data ,
                                              A493ContagemResultado_DemandaFM ,
                                              A457ContagemResultado_Demanda ,
                                              A1209AgendaAtendimento_CodDmn ,
                                              A2008ContagemResultado_CntSrvAls ,
                                              A1186AgendaAtendimento_QtdUnd ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1), "%", "");
         lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1), "%", "");
         lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1), "%", "");
         lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1), "%", "");
         lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2), "%", "");
         lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2), "%", "");
         lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2), "%", "");
         lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2), "%", "");
         lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3), "%", "");
         lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3), "%", "");
         lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3), "%", "");
         lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3), "%", "");
         lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm), "%", "");
         lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda), "%", "");
         lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = StringUtil.PadR( StringUtil.RTrim( AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals), 15, "%");
         /* Using cursor P00PZ2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1, AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1, AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1, AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2, AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2, AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2, AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3, AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3, AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3, AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data, AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to, AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn, AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to, lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm, AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel, lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda, AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel, lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals, AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel, AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund, AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKPZ2 = false;
            A1553ContagemResultado_CntSrvCod = P00PZ2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00PZ2_n1553ContagemResultado_CntSrvCod[0];
            A490ContagemResultado_ContratadaCod = P00PZ2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00PZ2_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00PZ2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00PZ2_n52Contratada_AreaTrabalhoCod[0];
            A493ContagemResultado_DemandaFM = P00PZ2_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00PZ2_n493ContagemResultado_DemandaFM[0];
            A1186AgendaAtendimento_QtdUnd = P00PZ2_A1186AgendaAtendimento_QtdUnd[0];
            A2008ContagemResultado_CntSrvAls = P00PZ2_A2008ContagemResultado_CntSrvAls[0];
            n2008ContagemResultado_CntSrvAls = P00PZ2_n2008ContagemResultado_CntSrvAls[0];
            A1209AgendaAtendimento_CodDmn = P00PZ2_A1209AgendaAtendimento_CodDmn[0];
            A457ContagemResultado_Demanda = P00PZ2_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00PZ2_n457ContagemResultado_Demanda[0];
            A1184AgendaAtendimento_Data = P00PZ2_A1184AgendaAtendimento_Data[0];
            A1183AgendaAtendimento_CntSrcCod = P00PZ2_A1183AgendaAtendimento_CntSrcCod[0];
            A1553ContagemResultado_CntSrvCod = P00PZ2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00PZ2_n1553ContagemResultado_CntSrvCod[0];
            A490ContagemResultado_ContratadaCod = P00PZ2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00PZ2_n490ContagemResultado_ContratadaCod[0];
            A493ContagemResultado_DemandaFM = P00PZ2_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00PZ2_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00PZ2_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00PZ2_n457ContagemResultado_Demanda[0];
            A2008ContagemResultado_CntSrvAls = P00PZ2_A2008ContagemResultado_CntSrvAls[0];
            n2008ContagemResultado_CntSrvAls = P00PZ2_n2008ContagemResultado_CntSrvAls[0];
            A52Contratada_AreaTrabalhoCod = P00PZ2_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00PZ2_n52Contratada_AreaTrabalhoCod[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00PZ2_A493ContagemResultado_DemandaFM[0], A493ContagemResultado_DemandaFM) == 0 ) )
            {
               BRKPZ2 = false;
               A1209AgendaAtendimento_CodDmn = P00PZ2_A1209AgendaAtendimento_CodDmn[0];
               A1184AgendaAtendimento_Data = P00PZ2_A1184AgendaAtendimento_Data[0];
               A1183AgendaAtendimento_CntSrcCod = P00PZ2_A1183AgendaAtendimento_CntSrcCod[0];
               AV28count = (long)(AV28count+1);
               BRKPZ2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) )
            {
               AV20Option = A493ContagemResultado_DemandaFM;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPZ2 )
            {
               BRKPZ2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTAGEMRESULTADO_DEMANDAOPTIONS' Routine */
         AV12TFContagemResultado_Demanda = AV16SearchTxt;
         AV13TFContagemResultado_Demanda_Sel = "";
         AV68WWAgendaAtendimentoDS_1_Contratada_areatrabalhocod = AV61Contratada_AreaTrabalhoCod;
         AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 = AV50DynamicFiltersOperator1;
         AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1 = AV35AgendaAtendimento_Data1;
         AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 = AV36AgendaAtendimento_Data_To1;
         AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = AV51ContagemResultado_DemandaFM1;
         AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = AV37ContagemResultado_Demanda1;
         AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 = AV58AgendaAtendimento_CodDmn1;
         AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 = AV52DynamicFiltersOperator2;
         AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2 = AV40AgendaAtendimento_Data2;
         AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 = AV41AgendaAtendimento_Data_To2;
         AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = AV53ContagemResultado_DemandaFM2;
         AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = AV42ContagemResultado_Demanda2;
         AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 = AV59AgendaAtendimento_CodDmn2;
         AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 = AV43DynamicFiltersEnabled3;
         AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 = AV44DynamicFiltersSelector3;
         AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 = AV54DynamicFiltersOperator3;
         AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3 = AV45AgendaAtendimento_Data3;
         AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 = AV46AgendaAtendimento_Data_To3;
         AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = AV55ContagemResultado_DemandaFM3;
         AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = AV47ContagemResultado_Demanda3;
         AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 = AV60AgendaAtendimento_CodDmn3;
         AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data = AV10TFAgendaAtendimento_Data;
         AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to = AV11TFAgendaAtendimento_Data_To;
         AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn = AV56TFAgendaAtendimento_CodDmn;
         AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to = AV57TFAgendaAtendimento_CodDmn_To;
         AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = AV48TFContagemResultado_DemandaFM;
         AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel = AV49TFContagemResultado_DemandaFM_Sel;
         AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = AV12TFContagemResultado_Demanda;
         AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel = AV13TFContagemResultado_Demanda_Sel;
         AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = AV62TFContagemResultado_CntSrvAls;
         AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel = AV63TFContagemResultado_CntSrvAls_Sel;
         AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund = AV14TFAgendaAtendimento_QtdUnd;
         AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to = AV15TFAgendaAtendimento_QtdUnd_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 ,
                                              AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1 ,
                                              AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 ,
                                              AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 ,
                                              AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ,
                                              AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ,
                                              AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 ,
                                              AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 ,
                                              AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 ,
                                              AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2 ,
                                              AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 ,
                                              AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 ,
                                              AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ,
                                              AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ,
                                              AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 ,
                                              AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 ,
                                              AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 ,
                                              AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3 ,
                                              AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 ,
                                              AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 ,
                                              AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ,
                                              AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ,
                                              AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 ,
                                              AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data ,
                                              AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to ,
                                              AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn ,
                                              AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to ,
                                              AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel ,
                                              AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ,
                                              AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel ,
                                              AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ,
                                              AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel ,
                                              AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ,
                                              AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund ,
                                              AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to ,
                                              A1184AgendaAtendimento_Data ,
                                              A493ContagemResultado_DemandaFM ,
                                              A457ContagemResultado_Demanda ,
                                              A1209AgendaAtendimento_CodDmn ,
                                              A2008ContagemResultado_CntSrvAls ,
                                              A1186AgendaAtendimento_QtdUnd ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1), "%", "");
         lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1), "%", "");
         lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1), "%", "");
         lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1), "%", "");
         lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2), "%", "");
         lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2), "%", "");
         lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2), "%", "");
         lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2), "%", "");
         lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3), "%", "");
         lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3), "%", "");
         lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3), "%", "");
         lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3), "%", "");
         lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm), "%", "");
         lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda), "%", "");
         lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = StringUtil.PadR( StringUtil.RTrim( AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals), 15, "%");
         /* Using cursor P00PZ3 */
         pr_default.execute(1, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1, AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1, AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1, AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2, AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2, AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2, AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3, AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3, AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3, AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data, AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to, AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn, AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to, lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm, AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel, lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda, AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel, lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals, AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel, AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund, AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKPZ4 = false;
            A1553ContagemResultado_CntSrvCod = P00PZ3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00PZ3_n1553ContagemResultado_CntSrvCod[0];
            A490ContagemResultado_ContratadaCod = P00PZ3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00PZ3_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00PZ3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00PZ3_n52Contratada_AreaTrabalhoCod[0];
            A457ContagemResultado_Demanda = P00PZ3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00PZ3_n457ContagemResultado_Demanda[0];
            A1186AgendaAtendimento_QtdUnd = P00PZ3_A1186AgendaAtendimento_QtdUnd[0];
            A2008ContagemResultado_CntSrvAls = P00PZ3_A2008ContagemResultado_CntSrvAls[0];
            n2008ContagemResultado_CntSrvAls = P00PZ3_n2008ContagemResultado_CntSrvAls[0];
            A1209AgendaAtendimento_CodDmn = P00PZ3_A1209AgendaAtendimento_CodDmn[0];
            A493ContagemResultado_DemandaFM = P00PZ3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00PZ3_n493ContagemResultado_DemandaFM[0];
            A1184AgendaAtendimento_Data = P00PZ3_A1184AgendaAtendimento_Data[0];
            A1183AgendaAtendimento_CntSrcCod = P00PZ3_A1183AgendaAtendimento_CntSrcCod[0];
            A1553ContagemResultado_CntSrvCod = P00PZ3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00PZ3_n1553ContagemResultado_CntSrvCod[0];
            A490ContagemResultado_ContratadaCod = P00PZ3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00PZ3_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = P00PZ3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00PZ3_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00PZ3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00PZ3_n493ContagemResultado_DemandaFM[0];
            A2008ContagemResultado_CntSrvAls = P00PZ3_A2008ContagemResultado_CntSrvAls[0];
            n2008ContagemResultado_CntSrvAls = P00PZ3_n2008ContagemResultado_CntSrvAls[0];
            A52Contratada_AreaTrabalhoCod = P00PZ3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00PZ3_n52Contratada_AreaTrabalhoCod[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00PZ3_A457ContagemResultado_Demanda[0], A457ContagemResultado_Demanda) == 0 ) )
            {
               BRKPZ4 = false;
               A1209AgendaAtendimento_CodDmn = P00PZ3_A1209AgendaAtendimento_CodDmn[0];
               A1184AgendaAtendimento_Data = P00PZ3_A1184AgendaAtendimento_Data[0];
               A1183AgendaAtendimento_CntSrcCod = P00PZ3_A1183AgendaAtendimento_CntSrcCod[0];
               AV28count = (long)(AV28count+1);
               BRKPZ4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) )
            {
               AV20Option = A457ContagemResultado_Demanda;
               AV23OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")));
               AV21Options.Add(AV20Option, 0);
               AV24OptionsDesc.Add(AV23OptionDesc, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPZ4 )
            {
               BRKPZ4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTAGEMRESULTADO_CNTSRVALSOPTIONS' Routine */
         AV62TFContagemResultado_CntSrvAls = AV16SearchTxt;
         AV63TFContagemResultado_CntSrvAls_Sel = "";
         AV68WWAgendaAtendimentoDS_1_Contratada_areatrabalhocod = AV61Contratada_AreaTrabalhoCod;
         AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 = AV50DynamicFiltersOperator1;
         AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1 = AV35AgendaAtendimento_Data1;
         AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 = AV36AgendaAtendimento_Data_To1;
         AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = AV51ContagemResultado_DemandaFM1;
         AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = AV37ContagemResultado_Demanda1;
         AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 = AV58AgendaAtendimento_CodDmn1;
         AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 = AV52DynamicFiltersOperator2;
         AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2 = AV40AgendaAtendimento_Data2;
         AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 = AV41AgendaAtendimento_Data_To2;
         AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = AV53ContagemResultado_DemandaFM2;
         AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = AV42ContagemResultado_Demanda2;
         AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 = AV59AgendaAtendimento_CodDmn2;
         AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 = AV43DynamicFiltersEnabled3;
         AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 = AV44DynamicFiltersSelector3;
         AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 = AV54DynamicFiltersOperator3;
         AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3 = AV45AgendaAtendimento_Data3;
         AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 = AV46AgendaAtendimento_Data_To3;
         AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = AV55ContagemResultado_DemandaFM3;
         AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = AV47ContagemResultado_Demanda3;
         AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 = AV60AgendaAtendimento_CodDmn3;
         AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data = AV10TFAgendaAtendimento_Data;
         AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to = AV11TFAgendaAtendimento_Data_To;
         AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn = AV56TFAgendaAtendimento_CodDmn;
         AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to = AV57TFAgendaAtendimento_CodDmn_To;
         AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = AV48TFContagemResultado_DemandaFM;
         AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel = AV49TFContagemResultado_DemandaFM_Sel;
         AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = AV12TFContagemResultado_Demanda;
         AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel = AV13TFContagemResultado_Demanda_Sel;
         AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = AV62TFContagemResultado_CntSrvAls;
         AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel = AV63TFContagemResultado_CntSrvAls_Sel;
         AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund = AV14TFAgendaAtendimento_QtdUnd;
         AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to = AV15TFAgendaAtendimento_QtdUnd_To;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 ,
                                              AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1 ,
                                              AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 ,
                                              AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 ,
                                              AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ,
                                              AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ,
                                              AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 ,
                                              AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 ,
                                              AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 ,
                                              AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2 ,
                                              AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 ,
                                              AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 ,
                                              AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ,
                                              AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ,
                                              AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 ,
                                              AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 ,
                                              AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 ,
                                              AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3 ,
                                              AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 ,
                                              AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 ,
                                              AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ,
                                              AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ,
                                              AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 ,
                                              AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data ,
                                              AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to ,
                                              AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn ,
                                              AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to ,
                                              AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel ,
                                              AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ,
                                              AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel ,
                                              AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ,
                                              AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel ,
                                              AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ,
                                              AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund ,
                                              AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to ,
                                              A1184AgendaAtendimento_Data ,
                                              A493ContagemResultado_DemandaFM ,
                                              A457ContagemResultado_Demanda ,
                                              A1209AgendaAtendimento_CodDmn ,
                                              A2008ContagemResultado_CntSrvAls ,
                                              A1186AgendaAtendimento_QtdUnd ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1), "%", "");
         lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1), "%", "");
         lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1), "%", "");
         lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1), "%", "");
         lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2), "%", "");
         lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2), "%", "");
         lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2), "%", "");
         lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2), "%", "");
         lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3), "%", "");
         lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3), "%", "");
         lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3), "%", "");
         lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3), "%", "");
         lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm), "%", "");
         lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda), "%", "");
         lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = StringUtil.PadR( StringUtil.RTrim( AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals), 15, "%");
         /* Using cursor P00PZ4 */
         pr_default.execute(2, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1, AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1, AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1, AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1, AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1, AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2, AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2, AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2, AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2, AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2, AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3, AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3, AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3, AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3, AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3, AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data, AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to, AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn, AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to, lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm, AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel, lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda, AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel, lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals, AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel, AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund, AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKPZ6 = false;
            A1553ContagemResultado_CntSrvCod = P00PZ4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00PZ4_n1553ContagemResultado_CntSrvCod[0];
            A490ContagemResultado_ContratadaCod = P00PZ4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00PZ4_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00PZ4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00PZ4_n52Contratada_AreaTrabalhoCod[0];
            A2008ContagemResultado_CntSrvAls = P00PZ4_A2008ContagemResultado_CntSrvAls[0];
            n2008ContagemResultado_CntSrvAls = P00PZ4_n2008ContagemResultado_CntSrvAls[0];
            A1186AgendaAtendimento_QtdUnd = P00PZ4_A1186AgendaAtendimento_QtdUnd[0];
            A1209AgendaAtendimento_CodDmn = P00PZ4_A1209AgendaAtendimento_CodDmn[0];
            A457ContagemResultado_Demanda = P00PZ4_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00PZ4_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00PZ4_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00PZ4_n493ContagemResultado_DemandaFM[0];
            A1184AgendaAtendimento_Data = P00PZ4_A1184AgendaAtendimento_Data[0];
            A1183AgendaAtendimento_CntSrcCod = P00PZ4_A1183AgendaAtendimento_CntSrcCod[0];
            A1553ContagemResultado_CntSrvCod = P00PZ4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00PZ4_n1553ContagemResultado_CntSrvCod[0];
            A490ContagemResultado_ContratadaCod = P00PZ4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00PZ4_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = P00PZ4_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00PZ4_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00PZ4_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00PZ4_n493ContagemResultado_DemandaFM[0];
            A2008ContagemResultado_CntSrvAls = P00PZ4_A2008ContagemResultado_CntSrvAls[0];
            n2008ContagemResultado_CntSrvAls = P00PZ4_n2008ContagemResultado_CntSrvAls[0];
            A52Contratada_AreaTrabalhoCod = P00PZ4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00PZ4_n52Contratada_AreaTrabalhoCod[0];
            AV28count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00PZ4_A2008ContagemResultado_CntSrvAls[0], A2008ContagemResultado_CntSrvAls) == 0 ) )
            {
               BRKPZ6 = false;
               A1553ContagemResultado_CntSrvCod = P00PZ4_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00PZ4_n1553ContagemResultado_CntSrvCod[0];
               A1209AgendaAtendimento_CodDmn = P00PZ4_A1209AgendaAtendimento_CodDmn[0];
               A1184AgendaAtendimento_Data = P00PZ4_A1184AgendaAtendimento_Data[0];
               A1183AgendaAtendimento_CntSrcCod = P00PZ4_A1183AgendaAtendimento_CntSrcCod[0];
               A1553ContagemResultado_CntSrvCod = P00PZ4_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00PZ4_n1553ContagemResultado_CntSrvCod[0];
               AV28count = (long)(AV28count+1);
               BRKPZ6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2008ContagemResultado_CntSrvAls)) )
            {
               AV20Option = A2008ContagemResultado_CntSrvAls;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPZ6 )
            {
               BRKPZ6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFAgendaAtendimento_Data = DateTime.MinValue;
         AV11TFAgendaAtendimento_Data_To = DateTime.MinValue;
         AV48TFContagemResultado_DemandaFM = "";
         AV49TFContagemResultado_DemandaFM_Sel = "";
         AV12TFContagemResultado_Demanda = "";
         AV13TFContagemResultado_Demanda_Sel = "";
         AV62TFContagemResultado_CntSrvAls = "";
         AV63TFContagemResultado_CntSrvAls_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV35AgendaAtendimento_Data1 = DateTime.MinValue;
         AV36AgendaAtendimento_Data_To1 = DateTime.MinValue;
         AV51ContagemResultado_DemandaFM1 = "";
         AV37ContagemResultado_Demanda1 = "";
         AV39DynamicFiltersSelector2 = "";
         AV40AgendaAtendimento_Data2 = DateTime.MinValue;
         AV41AgendaAtendimento_Data_To2 = DateTime.MinValue;
         AV53ContagemResultado_DemandaFM2 = "";
         AV42ContagemResultado_Demanda2 = "";
         AV44DynamicFiltersSelector3 = "";
         AV45AgendaAtendimento_Data3 = DateTime.MinValue;
         AV46AgendaAtendimento_Data_To3 = DateTime.MinValue;
         AV55ContagemResultado_DemandaFM3 = "";
         AV47ContagemResultado_Demanda3 = "";
         AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 = "";
         AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1 = DateTime.MinValue;
         AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 = DateTime.MinValue;
         AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = "";
         AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = "";
         AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 = "";
         AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2 = DateTime.MinValue;
         AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 = DateTime.MinValue;
         AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = "";
         AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = "";
         AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 = "";
         AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3 = DateTime.MinValue;
         AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 = DateTime.MinValue;
         AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = "";
         AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = "";
         AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data = DateTime.MinValue;
         AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to = DateTime.MinValue;
         AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = "";
         AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel = "";
         AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = "";
         AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel = "";
         AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = "";
         AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel = "";
         scmdbuf = "";
         lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 = "";
         lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 = "";
         lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 = "";
         lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 = "";
         lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 = "";
         lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 = "";
         lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm = "";
         lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda = "";
         lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals = "";
         A1184AgendaAtendimento_Data = DateTime.MinValue;
         A493ContagemResultado_DemandaFM = "";
         A457ContagemResultado_Demanda = "";
         A2008ContagemResultado_CntSrvAls = "";
         P00PZ2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00PZ2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00PZ2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00PZ2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00PZ2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00PZ2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00PZ2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00PZ2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00PZ2_A1186AgendaAtendimento_QtdUnd = new decimal[1] ;
         P00PZ2_A2008ContagemResultado_CntSrvAls = new String[] {""} ;
         P00PZ2_n2008ContagemResultado_CntSrvAls = new bool[] {false} ;
         P00PZ2_A1209AgendaAtendimento_CodDmn = new int[1] ;
         P00PZ2_A457ContagemResultado_Demanda = new String[] {""} ;
         P00PZ2_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00PZ2_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         P00PZ2_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         AV20Option = "";
         P00PZ3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00PZ3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00PZ3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00PZ3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00PZ3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00PZ3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00PZ3_A457ContagemResultado_Demanda = new String[] {""} ;
         P00PZ3_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00PZ3_A1186AgendaAtendimento_QtdUnd = new decimal[1] ;
         P00PZ3_A2008ContagemResultado_CntSrvAls = new String[] {""} ;
         P00PZ3_n2008ContagemResultado_CntSrvAls = new bool[] {false} ;
         P00PZ3_A1209AgendaAtendimento_CodDmn = new int[1] ;
         P00PZ3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00PZ3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00PZ3_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         P00PZ3_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         AV23OptionDesc = "";
         P00PZ4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00PZ4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00PZ4_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00PZ4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00PZ4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00PZ4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00PZ4_A2008ContagemResultado_CntSrvAls = new String[] {""} ;
         P00PZ4_n2008ContagemResultado_CntSrvAls = new bool[] {false} ;
         P00PZ4_A1186AgendaAtendimento_QtdUnd = new decimal[1] ;
         P00PZ4_A1209AgendaAtendimento_CodDmn = new int[1] ;
         P00PZ4_A457ContagemResultado_Demanda = new String[] {""} ;
         P00PZ4_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00PZ4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00PZ4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00PZ4_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         P00PZ4_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwagendaatendimentofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00PZ2_A1553ContagemResultado_CntSrvCod, P00PZ2_n1553ContagemResultado_CntSrvCod, P00PZ2_A490ContagemResultado_ContratadaCod, P00PZ2_n490ContagemResultado_ContratadaCod, P00PZ2_A52Contratada_AreaTrabalhoCod, P00PZ2_n52Contratada_AreaTrabalhoCod, P00PZ2_A493ContagemResultado_DemandaFM, P00PZ2_n493ContagemResultado_DemandaFM, P00PZ2_A1186AgendaAtendimento_QtdUnd, P00PZ2_A2008ContagemResultado_CntSrvAls,
               P00PZ2_n2008ContagemResultado_CntSrvAls, P00PZ2_A1209AgendaAtendimento_CodDmn, P00PZ2_A457ContagemResultado_Demanda, P00PZ2_n457ContagemResultado_Demanda, P00PZ2_A1184AgendaAtendimento_Data, P00PZ2_A1183AgendaAtendimento_CntSrcCod
               }
               , new Object[] {
               P00PZ3_A1553ContagemResultado_CntSrvCod, P00PZ3_n1553ContagemResultado_CntSrvCod, P00PZ3_A490ContagemResultado_ContratadaCod, P00PZ3_n490ContagemResultado_ContratadaCod, P00PZ3_A52Contratada_AreaTrabalhoCod, P00PZ3_n52Contratada_AreaTrabalhoCod, P00PZ3_A457ContagemResultado_Demanda, P00PZ3_n457ContagemResultado_Demanda, P00PZ3_A1186AgendaAtendimento_QtdUnd, P00PZ3_A2008ContagemResultado_CntSrvAls,
               P00PZ3_n2008ContagemResultado_CntSrvAls, P00PZ3_A1209AgendaAtendimento_CodDmn, P00PZ3_A493ContagemResultado_DemandaFM, P00PZ3_n493ContagemResultado_DemandaFM, P00PZ3_A1184AgendaAtendimento_Data, P00PZ3_A1183AgendaAtendimento_CntSrcCod
               }
               , new Object[] {
               P00PZ4_A1553ContagemResultado_CntSrvCod, P00PZ4_n1553ContagemResultado_CntSrvCod, P00PZ4_A490ContagemResultado_ContratadaCod, P00PZ4_n490ContagemResultado_ContratadaCod, P00PZ4_A52Contratada_AreaTrabalhoCod, P00PZ4_n52Contratada_AreaTrabalhoCod, P00PZ4_A2008ContagemResultado_CntSrvAls, P00PZ4_n2008ContagemResultado_CntSrvAls, P00PZ4_A1186AgendaAtendimento_QtdUnd, P00PZ4_A1209AgendaAtendimento_CodDmn,
               P00PZ4_A457ContagemResultado_Demanda, P00PZ4_n457ContagemResultado_Demanda, P00PZ4_A493ContagemResultado_DemandaFM, P00PZ4_n493ContagemResultado_DemandaFM, P00PZ4_A1184AgendaAtendimento_Data, P00PZ4_A1183AgendaAtendimento_CntSrcCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV50DynamicFiltersOperator1 ;
      private short AV52DynamicFiltersOperator2 ;
      private short AV54DynamicFiltersOperator3 ;
      private short AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 ;
      private short AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 ;
      private short AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 ;
      private int AV66GXV1 ;
      private int AV61Contratada_AreaTrabalhoCod ;
      private int AV56TFAgendaAtendimento_CodDmn ;
      private int AV57TFAgendaAtendimento_CodDmn_To ;
      private int AV58AgendaAtendimento_CodDmn1 ;
      private int AV59AgendaAtendimento_CodDmn2 ;
      private int AV60AgendaAtendimento_CodDmn3 ;
      private int AV68WWAgendaAtendimentoDS_1_Contratada_areatrabalhocod ;
      private int AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 ;
      private int AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 ;
      private int AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 ;
      private int AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn ;
      private int AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A1209AgendaAtendimento_CodDmn ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1183AgendaAtendimento_CntSrcCod ;
      private long AV28count ;
      private decimal AV14TFAgendaAtendimento_QtdUnd ;
      private decimal AV15TFAgendaAtendimento_QtdUnd_To ;
      private decimal AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund ;
      private decimal AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to ;
      private decimal A1186AgendaAtendimento_QtdUnd ;
      private String AV62TFContagemResultado_CntSrvAls ;
      private String AV63TFContagemResultado_CntSrvAls_Sel ;
      private String AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ;
      private String AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel ;
      private String scmdbuf ;
      private String lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ;
      private String A2008ContagemResultado_CntSrvAls ;
      private DateTime AV10TFAgendaAtendimento_Data ;
      private DateTime AV11TFAgendaAtendimento_Data_To ;
      private DateTime AV35AgendaAtendimento_Data1 ;
      private DateTime AV36AgendaAtendimento_Data_To1 ;
      private DateTime AV40AgendaAtendimento_Data2 ;
      private DateTime AV41AgendaAtendimento_Data_To2 ;
      private DateTime AV45AgendaAtendimento_Data3 ;
      private DateTime AV46AgendaAtendimento_Data_To3 ;
      private DateTime AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1 ;
      private DateTime AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 ;
      private DateTime AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2 ;
      private DateTime AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 ;
      private DateTime AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3 ;
      private DateTime AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 ;
      private DateTime AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data ;
      private DateTime AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to ;
      private DateTime A1184AgendaAtendimento_Data ;
      private bool returnInSub ;
      private bool AV38DynamicFiltersEnabled2 ;
      private bool AV43DynamicFiltersEnabled3 ;
      private bool AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 ;
      private bool AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 ;
      private bool BRKPZ2 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n2008ContagemResultado_CntSrvAls ;
      private bool n457ContagemResultado_Demanda ;
      private bool BRKPZ4 ;
      private bool BRKPZ6 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV48TFContagemResultado_DemandaFM ;
      private String AV49TFContagemResultado_DemandaFM_Sel ;
      private String AV12TFContagemResultado_Demanda ;
      private String AV13TFContagemResultado_Demanda_Sel ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV51ContagemResultado_DemandaFM1 ;
      private String AV37ContagemResultado_Demanda1 ;
      private String AV39DynamicFiltersSelector2 ;
      private String AV53ContagemResultado_DemandaFM2 ;
      private String AV42ContagemResultado_Demanda2 ;
      private String AV44DynamicFiltersSelector3 ;
      private String AV55ContagemResultado_DemandaFM3 ;
      private String AV47ContagemResultado_Demanda3 ;
      private String AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 ;
      private String AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ;
      private String AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ;
      private String AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 ;
      private String AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ;
      private String AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ;
      private String AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 ;
      private String AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ;
      private String AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ;
      private String AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ;
      private String AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel ;
      private String AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ;
      private String AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel ;
      private String lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ;
      private String lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ;
      private String lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ;
      private String lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ;
      private String lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ;
      private String lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ;
      private String lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ;
      private String lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String AV20Option ;
      private String AV23OptionDesc ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00PZ2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00PZ2_n1553ContagemResultado_CntSrvCod ;
      private int[] P00PZ2_A490ContagemResultado_ContratadaCod ;
      private bool[] P00PZ2_n490ContagemResultado_ContratadaCod ;
      private int[] P00PZ2_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00PZ2_n52Contratada_AreaTrabalhoCod ;
      private String[] P00PZ2_A493ContagemResultado_DemandaFM ;
      private bool[] P00PZ2_n493ContagemResultado_DemandaFM ;
      private decimal[] P00PZ2_A1186AgendaAtendimento_QtdUnd ;
      private String[] P00PZ2_A2008ContagemResultado_CntSrvAls ;
      private bool[] P00PZ2_n2008ContagemResultado_CntSrvAls ;
      private int[] P00PZ2_A1209AgendaAtendimento_CodDmn ;
      private String[] P00PZ2_A457ContagemResultado_Demanda ;
      private bool[] P00PZ2_n457ContagemResultado_Demanda ;
      private DateTime[] P00PZ2_A1184AgendaAtendimento_Data ;
      private int[] P00PZ2_A1183AgendaAtendimento_CntSrcCod ;
      private int[] P00PZ3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00PZ3_n1553ContagemResultado_CntSrvCod ;
      private int[] P00PZ3_A490ContagemResultado_ContratadaCod ;
      private bool[] P00PZ3_n490ContagemResultado_ContratadaCod ;
      private int[] P00PZ3_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00PZ3_n52Contratada_AreaTrabalhoCod ;
      private String[] P00PZ3_A457ContagemResultado_Demanda ;
      private bool[] P00PZ3_n457ContagemResultado_Demanda ;
      private decimal[] P00PZ3_A1186AgendaAtendimento_QtdUnd ;
      private String[] P00PZ3_A2008ContagemResultado_CntSrvAls ;
      private bool[] P00PZ3_n2008ContagemResultado_CntSrvAls ;
      private int[] P00PZ3_A1209AgendaAtendimento_CodDmn ;
      private String[] P00PZ3_A493ContagemResultado_DemandaFM ;
      private bool[] P00PZ3_n493ContagemResultado_DemandaFM ;
      private DateTime[] P00PZ3_A1184AgendaAtendimento_Data ;
      private int[] P00PZ3_A1183AgendaAtendimento_CntSrcCod ;
      private int[] P00PZ4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00PZ4_n1553ContagemResultado_CntSrvCod ;
      private int[] P00PZ4_A490ContagemResultado_ContratadaCod ;
      private bool[] P00PZ4_n490ContagemResultado_ContratadaCod ;
      private int[] P00PZ4_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00PZ4_n52Contratada_AreaTrabalhoCod ;
      private String[] P00PZ4_A2008ContagemResultado_CntSrvAls ;
      private bool[] P00PZ4_n2008ContagemResultado_CntSrvAls ;
      private decimal[] P00PZ4_A1186AgendaAtendimento_QtdUnd ;
      private int[] P00PZ4_A1209AgendaAtendimento_CodDmn ;
      private String[] P00PZ4_A457ContagemResultado_Demanda ;
      private bool[] P00PZ4_n457ContagemResultado_Demanda ;
      private String[] P00PZ4_A493ContagemResultado_DemandaFM ;
      private bool[] P00PZ4_n493ContagemResultado_DemandaFM ;
      private DateTime[] P00PZ4_A1184AgendaAtendimento_Data ;
      private int[] P00PZ4_A1183AgendaAtendimento_CntSrcCod ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getwwagendaatendimentofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00PZ2( IGxContext context ,
                                             String AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 ,
                                             DateTime AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1 ,
                                             DateTime AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 ,
                                             short AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 ,
                                             String AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ,
                                             String AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ,
                                             int AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 ,
                                             bool AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 ,
                                             String AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 ,
                                             DateTime AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2 ,
                                             DateTime AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 ,
                                             short AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 ,
                                             String AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ,
                                             String AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ,
                                             int AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 ,
                                             bool AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 ,
                                             String AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 ,
                                             DateTime AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3 ,
                                             DateTime AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 ,
                                             short AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 ,
                                             String AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ,
                                             String AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ,
                                             int AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 ,
                                             DateTime AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data ,
                                             DateTime AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to ,
                                             int AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn ,
                                             int AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to ,
                                             String AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel ,
                                             String AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ,
                                             String AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel ,
                                             String AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ,
                                             String AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel ,
                                             String AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ,
                                             decimal AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund ,
                                             decimal AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to ,
                                             DateTime A1184AgendaAtendimento_Data ,
                                             String A493ContagemResultado_DemandaFM ,
                                             String A457ContagemResultado_Demanda ,
                                             int A1209AgendaAtendimento_CodDmn ,
                                             String A2008ContagemResultado_CntSrvAls ,
                                             decimal A1186AgendaAtendimento_QtdUnd ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [40] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T4.[Contratada_AreaTrabalhoCod], T2.[ContagemResultado_DemandaFM], T1.[AgendaAtendimento_QtdUnd], T3.[ContratoServicos_Alias] AS ContagemResultado_CntSrvAls, T1.[AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn, T2.[ContagemResultado_Demanda], T1.[AgendaAtendimento_Data], T1.[AgendaAtendimento_CntSrcCod] FROM ((([AgendaAtendimento] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[AgendaAtendimento_CodDmn]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T4.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3)";
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data)";
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to)";
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( ! (0==AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] >= @AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn)";
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( ! (0==AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] <= @AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to)";
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int1[35] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ContratoServicos_Alias] like @lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals)";
         }
         else
         {
            GXv_int1[36] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[ContratoServicos_Alias] = @AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)";
         }
         else
         {
            GXv_int1[37] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] >= @AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund)";
         }
         else
         {
            GXv_int1[38] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] <= @AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to)";
         }
         else
         {
            GXv_int1[39] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[ContagemResultado_DemandaFM]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00PZ3( IGxContext context ,
                                             String AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 ,
                                             DateTime AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1 ,
                                             DateTime AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 ,
                                             short AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 ,
                                             String AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ,
                                             String AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ,
                                             int AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 ,
                                             bool AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 ,
                                             String AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 ,
                                             DateTime AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2 ,
                                             DateTime AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 ,
                                             short AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 ,
                                             String AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ,
                                             String AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ,
                                             int AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 ,
                                             bool AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 ,
                                             String AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 ,
                                             DateTime AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3 ,
                                             DateTime AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 ,
                                             short AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 ,
                                             String AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ,
                                             String AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ,
                                             int AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 ,
                                             DateTime AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data ,
                                             DateTime AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to ,
                                             int AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn ,
                                             int AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to ,
                                             String AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel ,
                                             String AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ,
                                             String AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel ,
                                             String AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ,
                                             String AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel ,
                                             String AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ,
                                             decimal AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund ,
                                             decimal AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to ,
                                             DateTime A1184AgendaAtendimento_Data ,
                                             String A493ContagemResultado_DemandaFM ,
                                             String A457ContagemResultado_Demanda ,
                                             int A1209AgendaAtendimento_CodDmn ,
                                             String A2008ContagemResultado_CntSrvAls ,
                                             decimal A1186AgendaAtendimento_QtdUnd ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [40] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T4.[Contratada_AreaTrabalhoCod], T2.[ContagemResultado_Demanda], T1.[AgendaAtendimento_QtdUnd], T3.[ContratoServicos_Alias] AS ContagemResultado_CntSrvAls, T1.[AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn, T2.[ContagemResultado_DemandaFM], T1.[AgendaAtendimento_Data], T1.[AgendaAtendimento_CntSrcCod] FROM ((([AgendaAtendimento] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[AgendaAtendimento_CodDmn]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T4.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3)";
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data)";
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to)";
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( ! (0==AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] >= @AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn)";
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( ! (0==AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] <= @AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to)";
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int3[35] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ContratoServicos_Alias] like @lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals)";
         }
         else
         {
            GXv_int3[36] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[ContratoServicos_Alias] = @AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)";
         }
         else
         {
            GXv_int3[37] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] >= @AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund)";
         }
         else
         {
            GXv_int3[38] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] <= @AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to)";
         }
         else
         {
            GXv_int3[39] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[ContagemResultado_Demanda]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00PZ4( IGxContext context ,
                                             String AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1 ,
                                             DateTime AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1 ,
                                             DateTime AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1 ,
                                             short AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 ,
                                             String AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1 ,
                                             String AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1 ,
                                             int AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1 ,
                                             bool AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 ,
                                             String AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2 ,
                                             DateTime AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2 ,
                                             DateTime AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2 ,
                                             short AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 ,
                                             String AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2 ,
                                             String AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2 ,
                                             int AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2 ,
                                             bool AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 ,
                                             String AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3 ,
                                             DateTime AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3 ,
                                             DateTime AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3 ,
                                             short AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 ,
                                             String AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3 ,
                                             String AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3 ,
                                             int AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3 ,
                                             DateTime AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data ,
                                             DateTime AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to ,
                                             int AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn ,
                                             int AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to ,
                                             String AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel ,
                                             String AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm ,
                                             String AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel ,
                                             String AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda ,
                                             String AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel ,
                                             String AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals ,
                                             decimal AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund ,
                                             decimal AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to ,
                                             DateTime A1184AgendaAtendimento_Data ,
                                             String A493ContagemResultado_DemandaFM ,
                                             String A457ContagemResultado_Demanda ,
                                             int A1209AgendaAtendimento_CodDmn ,
                                             String A2008ContagemResultado_CntSrvAls ,
                                             decimal A1186AgendaAtendimento_QtdUnd ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [40] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T4.[Contratada_AreaTrabalhoCod], T3.[ContratoServicos_Alias] AS ContagemResultado_CntSrvAls, T1.[AgendaAtendimento_QtdUnd], T1.[AgendaAtendimento_CodDmn] AS AgendaAtendimento_CodDmn, T2.[ContagemResultado_Demanda], T2.[ContagemResultado_DemandaFM], T1.[AgendaAtendimento_Data], T1.[AgendaAtendimento_CntSrcCod] FROM ((([AgendaAtendimento] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[AgendaAtendimento_CodDmn]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T4.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV70WWAgendaAtendimentoDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV69WWAgendaAtendimentoDS_2_Dynamicfiltersselector1, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV78WWAgendaAtendimentoDS_11_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( AV76WWAgendaAtendimentoDS_9_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV77WWAgendaAtendimentoDS_10_Dynamicfiltersselector2, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3)";
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3)";
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV86WWAgendaAtendimentoDS_19_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( AV84WWAgendaAtendimentoDS_17_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV85WWAgendaAtendimentoDS_18_Dynamicfiltersselector3, "AGENDAATENDIMENTO_CODDMN") == 0 ) && ( ! (0==AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] = @AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3)";
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] >= @AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data)";
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_Data] <= @AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to)";
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( ! (0==AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] >= @AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn)";
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( ! (0==AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_CodDmn] <= @AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to)";
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int5[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int5[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int5[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int5[35] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals)) ) )
         {
            sWhereString = sWhereString + " and (T3.[ContratoServicos_Alias] like @lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals)";
         }
         else
         {
            GXv_int5[36] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[ContratoServicos_Alias] = @AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel)";
         }
         else
         {
            GXv_int5[37] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] >= @AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund)";
         }
         else
         {
            GXv_int5[38] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to) )
         {
            sWhereString = sWhereString + " and (T1.[AgendaAtendimento_QtdUnd] <= @AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to)";
         }
         else
         {
            GXv_int5[39] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[ContratoServicos_Alias]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00PZ2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (DateTime)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (String)dynConstraints[39] , (decimal)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] );
               case 1 :
                     return conditional_P00PZ3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (DateTime)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (String)dynConstraints[39] , (decimal)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] );
               case 2 :
                     return conditional_P00PZ4(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (DateTime)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (String)dynConstraints[39] , (decimal)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00PZ2 ;
          prmP00PZ2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn",SqlDbType.Int,6,0} ,
          new Object[] {"@AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals",SqlDbType.Char,15,0} ,
          new Object[] {"@AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmP00PZ3 ;
          prmP00PZ3 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn",SqlDbType.Int,6,0} ,
          new Object[] {"@AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals",SqlDbType.Char,15,0} ,
          new Object[] {"@AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmP00PZ4 ;
          prmP00PZ4 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV71WWAgendaAtendimentoDS_4_Agendaatendimento_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV72WWAgendaAtendimentoDS_5_Agendaatendimento_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV73WWAgendaAtendimentoDS_6_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV74WWAgendaAtendimentoDS_7_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV75WWAgendaAtendimentoDS_8_Agendaatendimento_coddmn1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV79WWAgendaAtendimentoDS_12_Agendaatendimento_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80WWAgendaAtendimentoDS_13_Agendaatendimento_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV81WWAgendaAtendimentoDS_14_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV82WWAgendaAtendimentoDS_15_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV83WWAgendaAtendimentoDS_16_Agendaatendimento_coddmn2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV87WWAgendaAtendimentoDS_20_Agendaatendimento_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV88WWAgendaAtendimentoDS_21_Agendaatendimento_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV89WWAgendaAtendimentoDS_22_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV90WWAgendaAtendimentoDS_23_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV91WWAgendaAtendimentoDS_24_Agendaatendimento_coddmn3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV92WWAgendaAtendimentoDS_25_Tfagendaatendimento_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV93WWAgendaAtendimentoDS_26_Tfagendaatendimento_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV94WWAgendaAtendimentoDS_27_Tfagendaatendimento_coddmn",SqlDbType.Int,6,0} ,
          new Object[] {"@AV95WWAgendaAtendimentoDS_28_Tfagendaatendimento_coddmn_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV96WWAgendaAtendimentoDS_29_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV97WWAgendaAtendimentoDS_30_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV98WWAgendaAtendimentoDS_31_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV99WWAgendaAtendimentoDS_32_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV100WWAgendaAtendimentoDS_33_Tfcontagemresultado_cntsrvals",SqlDbType.Char,15,0} ,
          new Object[] {"@AV101WWAgendaAtendimentoDS_34_Tfcontagemresultado_cntsrvals_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV102WWAgendaAtendimentoDS_35_Tfagendaatendimento_qtdund",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV103WWAgendaAtendimentoDS_36_Tfagendaatendimento_qtdund_to",SqlDbType.Decimal,14,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00PZ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PZ2,100,0,true,false )
             ,new CursorDef("P00PZ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PZ3,100,0,true,false )
             ,new CursorDef("P00PZ4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PZ4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[59]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[69]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[78]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[79]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[59]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[69]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[78]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[79]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[59]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[69]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[78]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[79]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwagendaatendimentofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwagendaatendimentofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwagendaatendimentofilterdata") )
          {
             return  ;
          }
          getwwagendaatendimentofilterdata worker = new getwwagendaatendimentofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
