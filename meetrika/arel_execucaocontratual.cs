/*
               File: REL_ExecucaoContratual
        Description: Execucao Contratual
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:13.29
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_execucaocontratual : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV8AreaTrabalho_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV12Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV39Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV33Vigencia = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV34Vigencia_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV45Tipo = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_execucaocontratual( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_execucaocontratual( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           int aP1_Contratada_Codigo ,
                           int aP2_Codigo ,
                           DateTime aP3_Vigencia ,
                           DateTime aP4_Vigencia_To ,
                           String aP5_Tipo )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV12Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV39Codigo = aP2_Codigo;
         this.AV33Vigencia = aP3_Vigencia;
         this.AV34Vigencia_To = aP4_Vigencia_To;
         this.AV45Tipo = aP5_Tipo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 int aP1_Contratada_Codigo ,
                                 int aP2_Codigo ,
                                 DateTime aP3_Vigencia ,
                                 DateTime aP4_Vigencia_To ,
                                 String aP5_Tipo )
      {
         arel_execucaocontratual objarel_execucaocontratual;
         objarel_execucaocontratual = new arel_execucaocontratual();
         objarel_execucaocontratual.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objarel_execucaocontratual.AV12Contratada_Codigo = aP1_Contratada_Codigo;
         objarel_execucaocontratual.AV39Codigo = aP2_Codigo;
         objarel_execucaocontratual.AV33Vigencia = aP3_Vigencia;
         objarel_execucaocontratual.AV34Vigencia_To = aP4_Vigencia_To;
         objarel_execucaocontratual.AV45Tipo = aP5_Tipo;
         objarel_execucaocontratual.context.SetSubmitInitialConfig(context);
         objarel_execucaocontratual.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_execucaocontratual);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_execucaocontratual)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 3;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 1, 15840, 12240, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*3));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            AV40Data = DateTimeUtil.ServerDate( context, "DEFAULT");
            AV41DataHora = DateTimeUtil.ServerNow( context, "DEFAULT");
            if ( StringUtil.StrCmp(AV45Tipo, "C") == 0 )
            {
               /* Using cursor P00DM2 */
               pr_default.execute(0, new Object[] {AV39Codigo});
               while ( (pr_default.getStatus(0) != 101) )
               {
                  A39Contratada_Codigo = P00DM2_A39Contratada_Codigo[0];
                  A52Contratada_AreaTrabalhoCod = P00DM2_A52Contratada_AreaTrabalhoCod[0];
                  A40Contratada_PessoaCod = P00DM2_A40Contratada_PessoaCod[0];
                  A29Contratante_Codigo = P00DM2_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = P00DM2_n29Contratante_Codigo[0];
                  A335Contratante_PessoaCod = P00DM2_A335Contratante_PessoaCod[0];
                  A74Contrato_Codigo = P00DM2_A74Contrato_Codigo[0];
                  A1125Contratante_LogoNomeArq = P00DM2_A1125Contratante_LogoNomeArq[0];
                  n1125Contratante_LogoNomeArq = P00DM2_n1125Contratante_LogoNomeArq[0];
                  A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
                  A1126Contratante_LogoTipoArq = P00DM2_A1126Contratante_LogoTipoArq[0];
                  n1126Contratante_LogoTipoArq = P00DM2_n1126Contratante_LogoTipoArq[0];
                  A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
                  A42Contratada_PessoaCNPJ = P00DM2_A42Contratada_PessoaCNPJ[0];
                  n42Contratada_PessoaCNPJ = P00DM2_n42Contratada_PessoaCNPJ[0];
                  A41Contratada_PessoaNom = P00DM2_A41Contratada_PessoaNom[0];
                  n41Contratada_PessoaNom = P00DM2_n41Contratada_PessoaNom[0];
                  A12Contratante_CNPJ = P00DM2_A12Contratante_CNPJ[0];
                  n12Contratante_CNPJ = P00DM2_n12Contratante_CNPJ[0];
                  A10Contratante_NomeFantasia = P00DM2_A10Contratante_NomeFantasia[0];
                  A77Contrato_Numero = P00DM2_A77Contrato_Numero[0];
                  A80Contrato_Objeto = P00DM2_A80Contrato_Objeto[0];
                  A89Contrato_Valor = P00DM2_A89Contrato_Valor[0];
                  A81Contrato_Quantidade = P00DM2_A81Contrato_Quantidade[0];
                  A1124Contratante_LogoArquivo = P00DM2_A1124Contratante_LogoArquivo[0];
                  n1124Contratante_LogoArquivo = P00DM2_n1124Contratante_LogoArquivo[0];
                  A52Contratada_AreaTrabalhoCod = P00DM2_A52Contratada_AreaTrabalhoCod[0];
                  A40Contratada_PessoaCod = P00DM2_A40Contratada_PessoaCod[0];
                  A29Contratante_Codigo = P00DM2_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = P00DM2_n29Contratante_Codigo[0];
                  A335Contratante_PessoaCod = P00DM2_A335Contratante_PessoaCod[0];
                  A1125Contratante_LogoNomeArq = P00DM2_A1125Contratante_LogoNomeArq[0];
                  n1125Contratante_LogoNomeArq = P00DM2_n1125Contratante_LogoNomeArq[0];
                  A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
                  A1126Contratante_LogoTipoArq = P00DM2_A1126Contratante_LogoTipoArq[0];
                  n1126Contratante_LogoTipoArq = P00DM2_n1126Contratante_LogoTipoArq[0];
                  A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
                  A10Contratante_NomeFantasia = P00DM2_A10Contratante_NomeFantasia[0];
                  A1124Contratante_LogoArquivo = P00DM2_A1124Contratante_LogoArquivo[0];
                  n1124Contratante_LogoArquivo = P00DM2_n1124Contratante_LogoArquivo[0];
                  A12Contratante_CNPJ = P00DM2_A12Contratante_CNPJ[0];
                  n12Contratante_CNPJ = P00DM2_n12Contratante_CNPJ[0];
                  A42Contratada_PessoaCNPJ = P00DM2_A42Contratada_PessoaCNPJ[0];
                  n42Contratada_PessoaCNPJ = P00DM2_n42Contratada_PessoaCNPJ[0];
                  A41Contratada_PessoaNom = P00DM2_A41Contratada_PessoaNom[0];
                  n41Contratada_PessoaNom = P00DM2_n41Contratada_PessoaNom[0];
                  AV11Contratada_CNPJ = A42Contratada_PessoaCNPJ;
                  AV13Contratada_Nome = A41Contratada_PessoaNom;
                  AV14Contratante_Logo = A1124Contratante_LogoArquivo;
                  A40000Contratante_Logo_GXI = GeneXus.Utils.GXDbFile.GetUriFromFile( A1125Contratante_LogoNomeArq, A1126Contratante_LogoTipoArq);
                  AV36Contratante_CNPJ = (long)(NumberUtil.Val( A12Contratante_CNPJ, "."));
                  AV37ContratanteInfo = "Contratante: " + StringUtil.Trim( A10Contratante_NomeFantasia) + "  CNPJ: " + context.localUtil.Format( (decimal)(AV36Contratante_CNPJ), "99.999.999/9999-99");
                  AV16Contrato_Numero = A77Contrato_Numero;
                  AV9Contrato_Objeto = StringUtil.Trim( A80Contrato_Objeto);
                  AV20Contrato_Valor = A89Contrato_Valor;
                  AV18Contrato_Quantidade = A81Contrato_Quantidade;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(0);
            }
            else
            {
               /* Using cursor P00DM3 */
               pr_default.execute(1, new Object[] {AV39Codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A74Contrato_Codigo = P00DM3_A74Contrato_Codigo[0];
                  A39Contratada_Codigo = P00DM3_A39Contratada_Codigo[0];
                  A52Contratada_AreaTrabalhoCod = P00DM3_A52Contratada_AreaTrabalhoCod[0];
                  A40Contratada_PessoaCod = P00DM3_A40Contratada_PessoaCod[0];
                  A29Contratante_Codigo = P00DM3_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = P00DM3_n29Contratante_Codigo[0];
                  A335Contratante_PessoaCod = P00DM3_A335Contratante_PessoaCod[0];
                  A315ContratoTermoAditivo_Codigo = P00DM3_A315ContratoTermoAditivo_Codigo[0];
                  A1125Contratante_LogoNomeArq = P00DM3_A1125Contratante_LogoNomeArq[0];
                  n1125Contratante_LogoNomeArq = P00DM3_n1125Contratante_LogoNomeArq[0];
                  A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
                  A1126Contratante_LogoTipoArq = P00DM3_A1126Contratante_LogoTipoArq[0];
                  n1126Contratante_LogoTipoArq = P00DM3_n1126Contratante_LogoTipoArq[0];
                  A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
                  A42Contratada_PessoaCNPJ = P00DM3_A42Contratada_PessoaCNPJ[0];
                  n42Contratada_PessoaCNPJ = P00DM3_n42Contratada_PessoaCNPJ[0];
                  A41Contratada_PessoaNom = P00DM3_A41Contratada_PessoaNom[0];
                  n41Contratada_PessoaNom = P00DM3_n41Contratada_PessoaNom[0];
                  A12Contratante_CNPJ = P00DM3_A12Contratante_CNPJ[0];
                  n12Contratante_CNPJ = P00DM3_n12Contratante_CNPJ[0];
                  A10Contratante_NomeFantasia = P00DM3_A10Contratante_NomeFantasia[0];
                  A77Contrato_Numero = P00DM3_A77Contrato_Numero[0];
                  A80Contrato_Objeto = P00DM3_A80Contrato_Objeto[0];
                  A1360ContratoTermoAditivo_VlrTtlPrvto = P00DM3_A1360ContratoTermoAditivo_VlrTtlPrvto[0];
                  n1360ContratoTermoAditivo_VlrTtlPrvto = P00DM3_n1360ContratoTermoAditivo_VlrTtlPrvto[0];
                  A1362ContratoTermoAditivo_QtdContratada = P00DM3_A1362ContratoTermoAditivo_QtdContratada[0];
                  n1362ContratoTermoAditivo_QtdContratada = P00DM3_n1362ContratoTermoAditivo_QtdContratada[0];
                  A1124Contratante_LogoArquivo = P00DM3_A1124Contratante_LogoArquivo[0];
                  n1124Contratante_LogoArquivo = P00DM3_n1124Contratante_LogoArquivo[0];
                  A39Contratada_Codigo = P00DM3_A39Contratada_Codigo[0];
                  A77Contrato_Numero = P00DM3_A77Contrato_Numero[0];
                  A80Contrato_Objeto = P00DM3_A80Contrato_Objeto[0];
                  A52Contratada_AreaTrabalhoCod = P00DM3_A52Contratada_AreaTrabalhoCod[0];
                  A40Contratada_PessoaCod = P00DM3_A40Contratada_PessoaCod[0];
                  A29Contratante_Codigo = P00DM3_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = P00DM3_n29Contratante_Codigo[0];
                  A335Contratante_PessoaCod = P00DM3_A335Contratante_PessoaCod[0];
                  A1125Contratante_LogoNomeArq = P00DM3_A1125Contratante_LogoNomeArq[0];
                  n1125Contratante_LogoNomeArq = P00DM3_n1125Contratante_LogoNomeArq[0];
                  A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
                  A1126Contratante_LogoTipoArq = P00DM3_A1126Contratante_LogoTipoArq[0];
                  n1126Contratante_LogoTipoArq = P00DM3_n1126Contratante_LogoTipoArq[0];
                  A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
                  A10Contratante_NomeFantasia = P00DM3_A10Contratante_NomeFantasia[0];
                  A1124Contratante_LogoArquivo = P00DM3_A1124Contratante_LogoArquivo[0];
                  n1124Contratante_LogoArquivo = P00DM3_n1124Contratante_LogoArquivo[0];
                  A12Contratante_CNPJ = P00DM3_A12Contratante_CNPJ[0];
                  n12Contratante_CNPJ = P00DM3_n12Contratante_CNPJ[0];
                  A42Contratada_PessoaCNPJ = P00DM3_A42Contratada_PessoaCNPJ[0];
                  n42Contratada_PessoaCNPJ = P00DM3_n42Contratada_PessoaCNPJ[0];
                  A41Contratada_PessoaNom = P00DM3_A41Contratada_PessoaNom[0];
                  n41Contratada_PessoaNom = P00DM3_n41Contratada_PessoaNom[0];
                  AV11Contratada_CNPJ = A42Contratada_PessoaCNPJ;
                  AV13Contratada_Nome = A41Contratada_PessoaNom;
                  AV14Contratante_Logo = A1124Contratante_LogoArquivo;
                  A40000Contratante_Logo_GXI = GeneXus.Utils.GXDbFile.GetUriFromFile( A1125Contratante_LogoNomeArq, A1126Contratante_LogoTipoArq);
                  AV36Contratante_CNPJ = (long)(NumberUtil.Val( A12Contratante_CNPJ, "."));
                  AV37ContratanteInfo = "Contratante: " + StringUtil.Trim( A10Contratante_NomeFantasia) + "  CNPJ: " + context.localUtil.Format( (decimal)(AV36Contratante_CNPJ), "99.999.999/9999-99");
                  AV16Contrato_Numero = A77Contrato_Numero;
                  AV9Contrato_Objeto = StringUtil.Trim( A80Contrato_Objeto);
                  AV20Contrato_Valor = A1360ContratoTermoAditivo_VlrTtlPrvto;
                  AV18Contrato_Quantidade = (long)(A1362ContratoTermoAditivo_QtdContratada);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(1);
            }
            if ( ( ( DateTimeUtil.DDiff( AV34Vigencia_To , AV33Vigencia ) ) == 364 ) || ( ( DateTimeUtil.DDiff( AV34Vigencia_To , AV33Vigencia ) ) == 365 ) )
            {
               AV42DiasPorMes = (decimal)((DateTimeUtil.DDiff(AV34Vigencia_To,AV33Vigencia))/ (decimal)(12));
            }
            else
            {
               AV42DiasPorMes = (decimal)(364/ (decimal)(12));
            }
            /* Execute user subroutine: 'ITEM1' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'ITEM2' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'ITEM3' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'ITEM4' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HDM0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'ITEM1' Routine */
         AV22Item = "1. Dados da Contratada";
         HDM0( false, 50) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV22Item, "")), 33, Gx_line+17, 294, Gx_line+32, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+50);
         AV24Linha = "CNPJ   " + AV11Contratada_CNPJ;
         HDM0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         AV24Linha = "Prestadora   " + StringUtil.Upper( AV13Contratada_Nome);
         HDM0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
      }

      protected void S121( )
      {
         /* 'ITEM2' Routine */
         AV22Item = "2. Dados do Contrato";
         HDM0( false, 50) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV22Item, "")), 33, Gx_line+17, 294, Gx_line+32, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+50);
         AV24Linha = "Contrato   " + AV16Contrato_Numero;
         HDM0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         AV24Linha = "Vig�ncia   " + context.localUtil.DToC( AV33Vigencia, 2, "/") + " - " + context.localUtil.DToC( AV34Vigencia_To, 2, "/");
         HDM0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         AV24Linha = "Objeto";
         HDM0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         AV23Length = (short)(StringUtil.Len( AV9Contrato_Objeto));
         AV21i = 1;
         AV27p = 130;
         while ( true )
         {
            if ( AV23Length > 130 )
            {
               /* Execute user subroutine: 'WRAPCONTRATOOBJETO' */
               S131 ();
               if (returnInSub) return;
               AV24Linha = StringUtil.Substring( AV9Contrato_Objeto, AV21i, AV27p);
               AV21i = (short)(AV21i+AV27p);
               AV23Length = (short)(AV23Length-AV27p);
               AV27p = 130;
               HDM0( false, 17) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+17);
            }
            else
            {
               AV24Linha = StringUtil.Substring( AV9Contrato_Objeto, AV21i, AV27p);
               HDM0( false, 17) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+17);
               if (true) break;
            }
         }
         /* Execute user subroutine: 'TOTAIS' */
         S141 ();
         if (returnInSub) return;
         HDM0( false, 17) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         AV24Linha = "Valor contratado R$ " + context.localUtil.Format( AV20Contrato_Valor, "ZZZ,ZZZ,ZZ9.99");
         HDM0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         if ( ( AV20Contrato_Valor > Convert.ToDecimal( 0 )) )
         {
            AV28Percentual = (decimal)((AV19Contrato_RSExecutado/ (decimal)(AV20Contrato_Valor))*100);
         }
         AV24Linha = "Valor executado  R$ " + context.localUtil.Format( AV19Contrato_RSExecutado, "ZZZ,ZZZ,ZZ9.99") + "  (" + context.localUtil.Format( AV28Percentual, "ZZ9.99") + "%)";
         HDM0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         HDM0( false, 17) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         AV24Linha = "Qtd de Unidades contratadas " + context.localUtil.Format( (decimal)(AV18Contrato_Quantidade), "ZZ,ZZZ,ZZZ,ZZ9");
         HDM0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         AV24Linha = "Qtd de Unidades executadas  " + context.localUtil.Format( AV17Contrato_PFExecutado, "ZZZ,ZZZ,ZZZ,ZZ9");
         HDM0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         AV31Saldo = (long)(AV18Contrato_Quantidade-AV17Contrato_PFExecutado);
         AV24Linha = "Saldo dispon�vel            " + context.localUtil.Format( (decimal)(AV31Saldo), "ZZ,ZZZ,ZZZ,ZZ9");
         HDM0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         HDM0( false, 17) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         if ( AV40Data > AV34Vigencia_To )
         {
            AV26Meses = (decimal)(12);
         }
         else
         {
            AV26Meses = (decimal)((DateTimeUtil.DDiff(AV40Data,AV33Vigencia))/ (decimal)(AV42DiasPorMes));
         }
         if ( ( AV26Meses > Convert.ToDecimal( 0 )) )
         {
            AV25Media = (long)(AV17Contrato_PFExecutado/ (decimal)(AV26Meses));
         }
         AV24Linha = "M�dia mensal de consumo " + context.localUtil.Format( (decimal)(AV25Media), "ZZZ,ZZZ,ZZZ,ZZ9");
         HDM0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         if ( AV40Data < AV34Vigencia_To )
         {
            HDM0( false, 17) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+17);
            AV43MesesExc = (decimal)((DateTimeUtil.DDiff(AV40Data,AV33Vigencia))/ (decimal)(AV42DiasPorMes));
            AV24Linha = "Qtd de meses Executados " + context.localUtil.Format( AV43MesesExc, "ZZZZZZZZZZ9.99");
            HDM0( false, 17) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+17);
            AV26Meses = (decimal)((DateTimeUtil.DDiff(AV34Vigencia_To,AV40Data))/ (decimal)(AV42DiasPorMes));
            AV24Linha = "Qtd de meses para T�rmino da Vig�ncia " + context.localUtil.Format( AV26Meses, "ZZZZZZZZZZ9.99");
            HDM0( false, 17) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+17);
            AV35MediaPrev = (long)(AV25Media*AV26Meses);
            AV24Linha = "Qtd de Unidades M�dia para atender a Vig�ncia " + context.localUtil.Format( (decimal)(AV35MediaPrev), "ZZ,ZZZ,ZZZ,ZZ9");
            HDM0( false, 17) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+17);
            if ( AV25Media > 0 )
            {
               AV26Meses = (decimal)((AV31Saldo/ (decimal)(AV25Media))/ (decimal)(AV42DiasPorMes));
            }
            AV24Linha = "Qtd de meses para atendimento do saldo atual " + context.localUtil.Format( AV26Meses, "ZZZZZZZZZZ9.99");
            HDM0( false, 17) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+17);
         }
      }

      protected void S151( )
      {
         /* 'ITEM3' Routine */
         AV22Item = "3. Servi�os Executados";
         HDM0( false, 50) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV22Item, "")), 33, Gx_line+17, 294, Gx_line+32, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+50);
         AV24Linha = "";
         /* Using cursor P00DM4 */
         pr_default.execute(2, new Object[] {AV12Contratada_Codigo, AV33Vigencia, AV34Vigencia_To});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKDM6 = false;
            A601ContagemResultado_Servico = P00DM4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00DM4_n601ContagemResultado_Servico[0];
            A490ContagemResultado_ContratadaCod = P00DM4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00DM4_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = P00DM4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00DM4_n1553ContagemResultado_CntSrvCod[0];
            A471ContagemResultado_DataDmn = P00DM4_A471ContagemResultado_DataDmn[0];
            A484ContagemResultado_StatusDmn = P00DM4_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00DM4_n484ContagemResultado_StatusDmn[0];
            A801ContagemResultado_ServicoSigla = P00DM4_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00DM4_n801ContagemResultado_ServicoSigla[0];
            A456ContagemResultado_Codigo = P00DM4_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P00DM4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00DM4_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00DM4_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00DM4_n801ContagemResultado_ServicoSigla[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            if ( StringUtil.Len( AV24Linha) + StringUtil.Len( StringUtil.Trim( A801ContagemResultado_ServicoSigla)) > 100 )
            {
               HDM0( false, 17) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+17);
               AV24Linha = StringUtil.Trim( A801ContagemResultado_ServicoSigla);
            }
            else
            {
               if ( StringUtil.Len( AV24Linha) > 0 )
               {
                  AV24Linha = AV24Linha + ", ";
               }
               AV24Linha = AV24Linha + StringUtil.Trim( A801ContagemResultado_ServicoSigla);
            }
            AV32Servicos.Add(A801ContagemResultado_ServicoSigla, 0);
            AV29PF = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00DM4_A490ContagemResultado_ContratadaCod[0] == A490ContagemResultado_ContratadaCod ) && ( P00DM4_A1553ContagemResultado_CntSrvCod[0] == A1553ContagemResultado_CntSrvCod ) )
            {
               BRKDM6 = false;
               A471ContagemResultado_DataDmn = P00DM4_A471ContagemResultado_DataDmn[0];
               A456ContagemResultado_Codigo = P00DM4_A456ContagemResultado_Codigo[0];
               GXt_decimal1 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
               A574ContagemResultado_PFFinal = GXt_decimal1;
               AV29PF = (decimal)(AV29PF+A574ContagemResultado_PFFinal);
               BRKDM6 = true;
               pr_default.readNext(2);
            }
            AV30PFs.Add((double)(AV29PF), 0);
            if ( ! BRKDM6 )
            {
               BRKDM6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
         if ( StringUtil.Len( AV24Linha) <= 100 )
         {
            HDM0( false, 17) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+17);
         }
         HDM0( false, 17) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         AV21i = 1;
         while ( AV21i <= AV32Servicos.Count )
         {
            AV24Linha = "Servico " + AV32Servicos.GetString(AV21i);
            HDM0( false, 17) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+17);
            AV29PF = (decimal)(AV30PFs.GetNumeric(AV21i));
            if ( ( AV17Contrato_PFExecutado > Convert.ToDecimal( 0 )) )
            {
               AV28Percentual = (decimal)((AV29PF/ (decimal)(AV17Contrato_PFExecutado))*100);
            }
            else
            {
               AV28Percentual = 0;
            }
            if ( ( AV43MesesExc > Convert.ToDecimal( 0 )) )
            {
               AV25Media = (long)(AV29PF/ (decimal)(AV43MesesExc));
            }
            else
            {
               AV25Media = 0;
            }
            AV24Linha = "Qtd executada  " + context.localUtil.Format( AV29PF, "ZZZ,ZZZ,ZZ9.99") + "  (" + context.localUtil.Format( AV28Percentual, "ZZ9.99") + "% - M�dia " + StringUtil.LTrim( context.localUtil.Format( (decimal)(AV25Media), "ZZZ,ZZZ,ZZZ,ZZ9")) + ")";
            HDM0( false, 17) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+17);
            HDM0( false, 17) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+17);
            AV21i = (short)(AV21i+1);
         }
      }

      protected void S161( )
      {
         /* 'ITEM4' Routine */
         AV22Item = "4. Aditamento";
         HDM0( false, 50) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV22Item, "")), 33, Gx_line+17, 294, Gx_line+32, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+50);
         AV38Aditar = (decimal)(AV18Contrato_Quantidade*0.25m);
         AV24Linha = "Quantidade para Aditar   " + context.localUtil.Format( AV38Aditar, "ZZ,ZZZ,ZZZ,Z9");
         HDM0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
         if ( AV35MediaPrev > 0 )
         {
            AV44PercAtendido = (decimal)(AV38Aditar/ (decimal)(AV35MediaPrev)*100);
         }
         AV24Linha = "Percentual Atendido     " + StringUtil.Trim( StringUtil.Str( AV44PercAtendido, 8, 2)) + "%";
         HDM0( false, 17) ;
         getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
         getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Linha, "")), 75, Gx_line+0, 753, Gx_line+15, 0+256, 0, 0, 0) ;
         Gx_OldLine = Gx_line;
         Gx_line = (int)(Gx_line+17);
      }

      protected void S131( )
      {
         /* 'WRAPCONTRATOOBJETO' Routine */
         AV10Chars = " .,:;-";
         while ( StringUtil.StringSearch( AV10Chars, StringUtil.Substring( AV9Contrato_Objeto, AV27p, 1), 1) == 0 )
         {
            AV27p = (short)(AV27p-1);
         }
      }

      protected void S141( )
      {
         /* 'TOTAIS' Routine */
         /* Using cursor P00DM5 */
         pr_default.execute(3, new Object[] {AV12Contratada_Codigo, AV33Vigencia, AV34Vigencia_To});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A490ContagemResultado_ContratadaCod = P00DM5_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00DM5_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = P00DM5_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00DM5_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = P00DM5_A471ContagemResultado_DataDmn[0];
            A1553ContagemResultado_CntSrvCod = P00DM5_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00DM5_n1553ContagemResultado_CntSrvCod[0];
            A512ContagemResultado_ValorPF = P00DM5_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00DM5_n512ContagemResultado_ValorPF[0];
            A456ContagemResultado_Codigo = P00DM5_A456ContagemResultado_Codigo[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            AV17Contrato_PFExecutado = (decimal)(AV17Contrato_PFExecutado+A574ContagemResultado_PFFinal);
            AV19Contrato_RSExecutado = (decimal)(AV19Contrato_RSExecutado+((A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF)));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void HDM0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV41DataHora, "99/99/99 99:99"), 377, Gx_line+9, 457, Gx_line+24, 1+256, 0, 0, 1) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+32);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Calibri", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Relat�rio de acompanhamento", 242, Gx_line+17, 592, Gx_line+45, 1, 0, 0, 2) ;
               getPrinter().GxDrawText("de Execu��o Contrataual", 242, Gx_line+46, 592, Gx_line+74, 1, 0, 0, 0) ;
               getPrinter().GxDrawBitMap(AV14Contratante_Logo, 0, Gx_line+0, 120, Gx_line+100) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV37ContratanteInfo, "")), 100, Gx_line+83, 726, Gx_line+101, 1+256, 0, 0, 2) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+117);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV40Data = DateTime.MinValue;
         AV41DataHora = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         P00DM2_A39Contratada_Codigo = new int[1] ;
         P00DM2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00DM2_A40Contratada_PessoaCod = new int[1] ;
         P00DM2_A29Contratante_Codigo = new int[1] ;
         P00DM2_n29Contratante_Codigo = new bool[] {false} ;
         P00DM2_A335Contratante_PessoaCod = new int[1] ;
         P00DM2_A74Contrato_Codigo = new int[1] ;
         P00DM2_A1125Contratante_LogoNomeArq = new String[] {""} ;
         P00DM2_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         P00DM2_A1126Contratante_LogoTipoArq = new String[] {""} ;
         P00DM2_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         P00DM2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00DM2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00DM2_A41Contratada_PessoaNom = new String[] {""} ;
         P00DM2_n41Contratada_PessoaNom = new bool[] {false} ;
         P00DM2_A12Contratante_CNPJ = new String[] {""} ;
         P00DM2_n12Contratante_CNPJ = new bool[] {false} ;
         P00DM2_A10Contratante_NomeFantasia = new String[] {""} ;
         P00DM2_A77Contrato_Numero = new String[] {""} ;
         P00DM2_A80Contrato_Objeto = new String[] {""} ;
         P00DM2_A89Contrato_Valor = new decimal[1] ;
         P00DM2_A81Contrato_Quantidade = new int[1] ;
         P00DM2_A1124Contratante_LogoArquivo = new String[] {""} ;
         P00DM2_n1124Contratante_LogoArquivo = new bool[] {false} ;
         A1125Contratante_LogoNomeArq = "";
         A1124Contratante_LogoArquivo_Filename = "";
         A1126Contratante_LogoTipoArq = "";
         A1124Contratante_LogoArquivo_Filetype = "";
         A42Contratada_PessoaCNPJ = "";
         A41Contratada_PessoaNom = "";
         A12Contratante_CNPJ = "";
         A10Contratante_NomeFantasia = "";
         A77Contrato_Numero = "";
         A80Contrato_Objeto = "";
         A1124Contratante_LogoArquivo = "";
         AV11Contratada_CNPJ = "";
         AV13Contratada_Nome = "";
         AV14Contratante_Logo = "";
         A40000Contratante_Logo_GXI = "";
         AV37ContratanteInfo = "";
         AV16Contrato_Numero = "";
         AV9Contrato_Objeto = "";
         P00DM3_A74Contrato_Codigo = new int[1] ;
         P00DM3_A39Contratada_Codigo = new int[1] ;
         P00DM3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00DM3_A40Contratada_PessoaCod = new int[1] ;
         P00DM3_A29Contratante_Codigo = new int[1] ;
         P00DM3_n29Contratante_Codigo = new bool[] {false} ;
         P00DM3_A335Contratante_PessoaCod = new int[1] ;
         P00DM3_A315ContratoTermoAditivo_Codigo = new int[1] ;
         P00DM3_A1125Contratante_LogoNomeArq = new String[] {""} ;
         P00DM3_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         P00DM3_A1126Contratante_LogoTipoArq = new String[] {""} ;
         P00DM3_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         P00DM3_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00DM3_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00DM3_A41Contratada_PessoaNom = new String[] {""} ;
         P00DM3_n41Contratada_PessoaNom = new bool[] {false} ;
         P00DM3_A12Contratante_CNPJ = new String[] {""} ;
         P00DM3_n12Contratante_CNPJ = new bool[] {false} ;
         P00DM3_A10Contratante_NomeFantasia = new String[] {""} ;
         P00DM3_A77Contrato_Numero = new String[] {""} ;
         P00DM3_A80Contrato_Objeto = new String[] {""} ;
         P00DM3_A1360ContratoTermoAditivo_VlrTtlPrvto = new decimal[1] ;
         P00DM3_n1360ContratoTermoAditivo_VlrTtlPrvto = new bool[] {false} ;
         P00DM3_A1362ContratoTermoAditivo_QtdContratada = new decimal[1] ;
         P00DM3_n1362ContratoTermoAditivo_QtdContratada = new bool[] {false} ;
         P00DM3_A1124Contratante_LogoArquivo = new String[] {""} ;
         P00DM3_n1124Contratante_LogoArquivo = new bool[] {false} ;
         AV22Item = "";
         AV24Linha = "";
         P00DM4_A601ContagemResultado_Servico = new int[1] ;
         P00DM4_n601ContagemResultado_Servico = new bool[] {false} ;
         P00DM4_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00DM4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00DM4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00DM4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00DM4_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00DM4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00DM4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00DM4_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00DM4_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00DM4_A456ContagemResultado_Codigo = new int[1] ;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         A801ContagemResultado_ServicoSigla = "";
         AV32Servicos = new GxSimpleCollection();
         AV30PFs = new GxSimpleCollection();
         AV10Chars = "";
         P00DM5_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00DM5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00DM5_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00DM5_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00DM5_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00DM5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00DM5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00DM5_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00DM5_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00DM5_A456ContagemResultado_Codigo = new int[1] ;
         AV14Contratante_Logo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_execucaocontratual__default(),
            new Object[][] {
                new Object[] {
               P00DM2_A39Contratada_Codigo, P00DM2_A52Contratada_AreaTrabalhoCod, P00DM2_A40Contratada_PessoaCod, P00DM2_A29Contratante_Codigo, P00DM2_n29Contratante_Codigo, P00DM2_A335Contratante_PessoaCod, P00DM2_A74Contrato_Codigo, P00DM2_A1125Contratante_LogoNomeArq, P00DM2_n1125Contratante_LogoNomeArq, P00DM2_A1126Contratante_LogoTipoArq,
               P00DM2_n1126Contratante_LogoTipoArq, P00DM2_A42Contratada_PessoaCNPJ, P00DM2_n42Contratada_PessoaCNPJ, P00DM2_A41Contratada_PessoaNom, P00DM2_n41Contratada_PessoaNom, P00DM2_A12Contratante_CNPJ, P00DM2_n12Contratante_CNPJ, P00DM2_A10Contratante_NomeFantasia, P00DM2_A77Contrato_Numero, P00DM2_A80Contrato_Objeto,
               P00DM2_A89Contrato_Valor, P00DM2_A81Contrato_Quantidade, P00DM2_A1124Contratante_LogoArquivo, P00DM2_n1124Contratante_LogoArquivo
               }
               , new Object[] {
               P00DM3_A74Contrato_Codigo, P00DM3_A39Contratada_Codigo, P00DM3_A52Contratada_AreaTrabalhoCod, P00DM3_A40Contratada_PessoaCod, P00DM3_A29Contratante_Codigo, P00DM3_n29Contratante_Codigo, P00DM3_A335Contratante_PessoaCod, P00DM3_A315ContratoTermoAditivo_Codigo, P00DM3_A1125Contratante_LogoNomeArq, P00DM3_n1125Contratante_LogoNomeArq,
               P00DM3_A1126Contratante_LogoTipoArq, P00DM3_n1126Contratante_LogoTipoArq, P00DM3_A42Contratada_PessoaCNPJ, P00DM3_n42Contratada_PessoaCNPJ, P00DM3_A41Contratada_PessoaNom, P00DM3_n41Contratada_PessoaNom, P00DM3_A12Contratante_CNPJ, P00DM3_n12Contratante_CNPJ, P00DM3_A10Contratante_NomeFantasia, P00DM3_A77Contrato_Numero,
               P00DM3_A80Contrato_Objeto, P00DM3_A1360ContratoTermoAditivo_VlrTtlPrvto, P00DM3_n1360ContratoTermoAditivo_VlrTtlPrvto, P00DM3_A1362ContratoTermoAditivo_QtdContratada, P00DM3_n1362ContratoTermoAditivo_QtdContratada, P00DM3_A1124Contratante_LogoArquivo, P00DM3_n1124Contratante_LogoArquivo
               }
               , new Object[] {
               P00DM4_A601ContagemResultado_Servico, P00DM4_n601ContagemResultado_Servico, P00DM4_A490ContagemResultado_ContratadaCod, P00DM4_n490ContagemResultado_ContratadaCod, P00DM4_A1553ContagemResultado_CntSrvCod, P00DM4_n1553ContagemResultado_CntSrvCod, P00DM4_A471ContagemResultado_DataDmn, P00DM4_A484ContagemResultado_StatusDmn, P00DM4_n484ContagemResultado_StatusDmn, P00DM4_A801ContagemResultado_ServicoSigla,
               P00DM4_n801ContagemResultado_ServicoSigla, P00DM4_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00DM5_A490ContagemResultado_ContratadaCod, P00DM5_n490ContagemResultado_ContratadaCod, P00DM5_A484ContagemResultado_StatusDmn, P00DM5_n484ContagemResultado_StatusDmn, P00DM5_A471ContagemResultado_DataDmn, P00DM5_A1553ContagemResultado_CntSrvCod, P00DM5_n1553ContagemResultado_CntSrvCod, P00DM5_A512ContagemResultado_ValorPF, P00DM5_n512ContagemResultado_ValorPF, P00DM5_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV23Length ;
      private short AV21i ;
      private short AV27p ;
      private int AV8AreaTrabalho_Codigo ;
      private int AV12Contratada_Codigo ;
      private int AV39Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A39Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A40Contratada_PessoaCod ;
      private int A29Contratante_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int A74Contrato_Codigo ;
      private int A81Contrato_Quantidade ;
      private int A315ContratoTermoAditivo_Codigo ;
      private int Gx_OldLine ;
      private int A601ContagemResultado_Servico ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private long AV36Contratante_CNPJ ;
      private long AV18Contrato_Quantidade ;
      private long AV31Saldo ;
      private long AV25Media ;
      private long AV35MediaPrev ;
      private decimal A89Contrato_Valor ;
      private decimal AV20Contrato_Valor ;
      private decimal A1360ContratoTermoAditivo_VlrTtlPrvto ;
      private decimal A1362ContratoTermoAditivo_QtdContratada ;
      private decimal AV42DiasPorMes ;
      private decimal AV28Percentual ;
      private decimal AV19Contrato_RSExecutado ;
      private decimal AV17Contrato_PFExecutado ;
      private decimal AV26Meses ;
      private decimal AV43MesesExc ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal AV29PF ;
      private decimal AV38Aditar ;
      private decimal AV44PercAtendido ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal GXt_decimal1 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV45Tipo ;
      private String scmdbuf ;
      private String A1125Contratante_LogoNomeArq ;
      private String A1124Contratante_LogoArquivo_Filename ;
      private String A1126Contratante_LogoTipoArq ;
      private String A1124Contratante_LogoArquivo_Filetype ;
      private String A41Contratada_PessoaNom ;
      private String A10Contratante_NomeFantasia ;
      private String A77Contrato_Numero ;
      private String AV11Contratada_CNPJ ;
      private String AV13Contratada_Nome ;
      private String AV37ContratanteInfo ;
      private String AV16Contrato_Numero ;
      private String AV22Item ;
      private String AV24Linha ;
      private String A484ContagemResultado_StatusDmn ;
      private String A801ContagemResultado_ServicoSigla ;
      private String AV10Chars ;
      private DateTime AV41DataHora ;
      private DateTime AV33Vigencia ;
      private DateTime AV34Vigencia_To ;
      private DateTime AV40Data ;
      private DateTime A471ContagemResultado_DataDmn ;
      private bool entryPointCalled ;
      private bool n29Contratante_Codigo ;
      private bool n1125Contratante_LogoNomeArq ;
      private bool n1126Contratante_LogoTipoArq ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n41Contratada_PessoaNom ;
      private bool n12Contratante_CNPJ ;
      private bool n1124Contratante_LogoArquivo ;
      private bool n1360ContratoTermoAditivo_VlrTtlPrvto ;
      private bool n1362ContratoTermoAditivo_QtdContratada ;
      private bool returnInSub ;
      private bool BRKDM6 ;
      private bool n601ContagemResultado_Servico ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n512ContagemResultado_ValorPF ;
      private String A80Contrato_Objeto ;
      private String AV9Contrato_Objeto ;
      private String A42Contratada_PessoaCNPJ ;
      private String A12Contratante_CNPJ ;
      private String A40000Contratante_Logo_GXI ;
      private String AV14Contratante_Logo ;
      private String Contratante_logo ;
      private String A1124Contratante_LogoArquivo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00DM2_A39Contratada_Codigo ;
      private int[] P00DM2_A52Contratada_AreaTrabalhoCod ;
      private int[] P00DM2_A40Contratada_PessoaCod ;
      private int[] P00DM2_A29Contratante_Codigo ;
      private bool[] P00DM2_n29Contratante_Codigo ;
      private int[] P00DM2_A335Contratante_PessoaCod ;
      private int[] P00DM2_A74Contrato_Codigo ;
      private String[] P00DM2_A1125Contratante_LogoNomeArq ;
      private bool[] P00DM2_n1125Contratante_LogoNomeArq ;
      private String[] P00DM2_A1126Contratante_LogoTipoArq ;
      private bool[] P00DM2_n1126Contratante_LogoTipoArq ;
      private String[] P00DM2_A42Contratada_PessoaCNPJ ;
      private bool[] P00DM2_n42Contratada_PessoaCNPJ ;
      private String[] P00DM2_A41Contratada_PessoaNom ;
      private bool[] P00DM2_n41Contratada_PessoaNom ;
      private String[] P00DM2_A12Contratante_CNPJ ;
      private bool[] P00DM2_n12Contratante_CNPJ ;
      private String[] P00DM2_A10Contratante_NomeFantasia ;
      private String[] P00DM2_A77Contrato_Numero ;
      private String[] P00DM2_A80Contrato_Objeto ;
      private decimal[] P00DM2_A89Contrato_Valor ;
      private int[] P00DM2_A81Contrato_Quantidade ;
      private String[] P00DM2_A1124Contratante_LogoArquivo ;
      private bool[] P00DM2_n1124Contratante_LogoArquivo ;
      private int[] P00DM3_A74Contrato_Codigo ;
      private int[] P00DM3_A39Contratada_Codigo ;
      private int[] P00DM3_A52Contratada_AreaTrabalhoCod ;
      private int[] P00DM3_A40Contratada_PessoaCod ;
      private int[] P00DM3_A29Contratante_Codigo ;
      private bool[] P00DM3_n29Contratante_Codigo ;
      private int[] P00DM3_A335Contratante_PessoaCod ;
      private int[] P00DM3_A315ContratoTermoAditivo_Codigo ;
      private String[] P00DM3_A1125Contratante_LogoNomeArq ;
      private bool[] P00DM3_n1125Contratante_LogoNomeArq ;
      private String[] P00DM3_A1126Contratante_LogoTipoArq ;
      private bool[] P00DM3_n1126Contratante_LogoTipoArq ;
      private String[] P00DM3_A42Contratada_PessoaCNPJ ;
      private bool[] P00DM3_n42Contratada_PessoaCNPJ ;
      private String[] P00DM3_A41Contratada_PessoaNom ;
      private bool[] P00DM3_n41Contratada_PessoaNom ;
      private String[] P00DM3_A12Contratante_CNPJ ;
      private bool[] P00DM3_n12Contratante_CNPJ ;
      private String[] P00DM3_A10Contratante_NomeFantasia ;
      private String[] P00DM3_A77Contrato_Numero ;
      private String[] P00DM3_A80Contrato_Objeto ;
      private decimal[] P00DM3_A1360ContratoTermoAditivo_VlrTtlPrvto ;
      private bool[] P00DM3_n1360ContratoTermoAditivo_VlrTtlPrvto ;
      private decimal[] P00DM3_A1362ContratoTermoAditivo_QtdContratada ;
      private bool[] P00DM3_n1362ContratoTermoAditivo_QtdContratada ;
      private String[] P00DM3_A1124Contratante_LogoArquivo ;
      private bool[] P00DM3_n1124Contratante_LogoArquivo ;
      private int[] P00DM4_A601ContagemResultado_Servico ;
      private bool[] P00DM4_n601ContagemResultado_Servico ;
      private int[] P00DM4_A490ContagemResultado_ContratadaCod ;
      private bool[] P00DM4_n490ContagemResultado_ContratadaCod ;
      private int[] P00DM4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00DM4_n1553ContagemResultado_CntSrvCod ;
      private DateTime[] P00DM4_A471ContagemResultado_DataDmn ;
      private String[] P00DM4_A484ContagemResultado_StatusDmn ;
      private bool[] P00DM4_n484ContagemResultado_StatusDmn ;
      private String[] P00DM4_A801ContagemResultado_ServicoSigla ;
      private bool[] P00DM4_n801ContagemResultado_ServicoSigla ;
      private int[] P00DM4_A456ContagemResultado_Codigo ;
      private int[] P00DM5_A490ContagemResultado_ContratadaCod ;
      private bool[] P00DM5_n490ContagemResultado_ContratadaCod ;
      private String[] P00DM5_A484ContagemResultado_StatusDmn ;
      private bool[] P00DM5_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00DM5_A471ContagemResultado_DataDmn ;
      private int[] P00DM5_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00DM5_n1553ContagemResultado_CntSrvCod ;
      private decimal[] P00DM5_A512ContagemResultado_ValorPF ;
      private bool[] P00DM5_n512ContagemResultado_ValorPF ;
      private int[] P00DM5_A456ContagemResultado_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV30PFs ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32Servicos ;
   }

   public class arel_execucaocontratual__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DM2 ;
          prmP00DM2 = new Object[] {
          new Object[] {"@AV39Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00DM3 ;
          prmP00DM3 = new Object[] {
          new Object[] {"@AV39Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00DM4 ;
          prmP00DM4 = new Object[] {
          new Object[] {"@AV12Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV33Vigencia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34Vigencia_To",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP00DM5 ;
          prmP00DM5 = new Object[] {
          new Object[] {"@AV12Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV33Vigencia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34Vigencia_To",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DM2", "SELECT TOP 1 T1.[Contratada_Codigo], T2.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T2.[Contratada_PessoaCod] AS Contratada_PessoaCod, T3.[Contratante_Codigo], T4.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[Contrato_Codigo], T4.[Contratante_LogoNomeArq], T4.[Contratante_LogoTipoArq], T6.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T6.[Pessoa_Nome] AS Contratada_PessoaNom, T5.[Pessoa_Docto] AS Contratante_CNPJ, T4.[Contratante_NomeFantasia], T1.[Contrato_Numero], T1.[Contrato_Objeto], T1.[Contrato_Valor], T1.[Contrato_Quantidade], T4.[Contratante_LogoArquivo] FROM ((((([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Contratante] T4 WITH (NOLOCK) ON T4.[Contratante_Codigo] = T3.[Contratante_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratante_PessoaCod]) INNER JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) WHERE T1.[Contrato_Codigo] = @AV39Codigo ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DM2,1,0,false,true )
             ,new CursorDef("P00DM3", "SELECT TOP 1 T1.[Contrato_Codigo], T2.[Contratada_Codigo], T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T4.[Contratante_Codigo], T5.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[ContratoTermoAditivo_Codigo], T5.[Contratante_LogoNomeArq], T5.[Contratante_LogoTipoArq], T7.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T7.[Pessoa_Nome] AS Contratada_PessoaNom, T6.[Pessoa_Docto] AS Contratante_CNPJ, T5.[Contratante_NomeFantasia], T2.[Contrato_Numero], T2.[Contrato_Objeto], T1.[ContratoTermoAditivo_VlrTtlPrvto], T1.[ContratoTermoAditivo_QtdContratada], T5.[Contratante_LogoArquivo] FROM (((((([ContratoTermoAditivo] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Contratante] T5 WITH (NOLOCK) ON T5.[Contratante_Codigo] = T4.[Contratante_Codigo]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Contratante_PessoaCod]) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE T1.[ContratoTermoAditivo_Codigo] = @AV39Codigo ORDER BY T1.[ContratoTermoAditivo_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DM3,1,0,false,true )
             ,new CursorDef("P00DM4", "SELECT T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_StatusDmn], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) WHERE (T1.[ContagemResultado_ContratadaCod] = @AV12Contratada_Codigo and T1.[ContagemResultado_CntSrvCod] > 0 and T1.[ContagemResultado_DataDmn] >= @AV33Vigencia) AND (T1.[ContagemResultado_DataDmn] <= @AV34Vigencia_To) AND (T1.[ContagemResultado_StatusDmn] <> 'X') ORDER BY T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_CntSrvCod], T1.[ContagemResultado_DataDmn] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DM4,100,0,true,false )
             ,new CursorDef("P00DM5", "SELECT [ContagemResultado_ContratadaCod], [ContagemResultado_StatusDmn], [ContagemResultado_DataDmn], [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_ContratadaCod] = @AV12Contratada_Codigo and [ContagemResultado_CntSrvCod] > 0 and [ContagemResultado_DataDmn] >= @AV33Vigencia) AND ([ContagemResultado_DataDmn] <= @AV34Vigencia_To) AND ([ContagemResultado_StatusDmn] <> 'X') ORDER BY [ContagemResultado_ContratadaCod], [ContagemResultado_CntSrvCod], [ContagemResultado_DataDmn] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DM5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 100) ;
                ((String[]) buf[18])[0] = rslt.getString(13, 20) ;
                ((String[]) buf[19])[0] = rslt.getLongVarchar(14) ;
                ((decimal[]) buf[20])[0] = rslt.getDecimal(15) ;
                ((int[]) buf[21])[0] = rslt.getInt(16) ;
                ((String[]) buf[22])[0] = rslt.getBLOBFile(17, rslt.getString(8, 10), rslt.getString(7, 50)) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(17);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((String[]) buf[8])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((String[]) buf[10])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((String[]) buf[12])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((String[]) buf[16])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((String[]) buf[18])[0] = rslt.getString(13, 100) ;
                ((String[]) buf[19])[0] = rslt.getString(14, 20) ;
                ((String[]) buf[20])[0] = rslt.getLongVarchar(15) ;
                ((decimal[]) buf[21])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(16);
                ((decimal[]) buf[23])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(17);
                ((String[]) buf[25])[0] = rslt.getBLOBFile(18, rslt.getString(9, 10), rslt.getString(8, 50)) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(18);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                return;
       }
    }

 }

}
