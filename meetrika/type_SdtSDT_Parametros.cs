/*
               File: type_SdtSDT_Parametros
        Description: SDT_Parametros
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 3:44:43.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Parametros" )]
   [XmlType(TypeName =  "SDT_Parametros" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_Parametros : GxUserType
   {
      public SdtSDT_Parametros( )
      {
         /* Constructor for serialization */
      }

      public SdtSDT_Parametros( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Parametros deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Parametros)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Parametros obj ;
         obj = this;
         obj.gxTpr_Character = deserialized.gxTpr_Character;
         obj.gxTpr_Numeric = deserialized.gxTpr_Numeric;
         obj.gxTpr_Datetime = deserialized.gxTpr_Datetime;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Character") )
               {
                  if ( gxTv_SdtSDT_Parametros_Character == null )
                  {
                     gxTv_SdtSDT_Parametros_Character = new GxSimpleCollection();
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtSDT_Parametros_Character.readxmlcollection(oReader, "Character", "Value");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Numeric") )
               {
                  if ( gxTv_SdtSDT_Parametros_Numeric == null )
                  {
                     gxTv_SdtSDT_Parametros_Numeric = new GxSimpleCollection();
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtSDT_Parametros_Numeric.readxmlcollection(oReader, "Numeric", "Value");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DateTime") )
               {
                  if ( gxTv_SdtSDT_Parametros_Datetime == null )
                  {
                     gxTv_SdtSDT_Parametros_Datetime = new GxSimpleCollection();
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtSDT_Parametros_Datetime.readxmlcollection(oReader, "DateTime", "Value");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Parametros";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         if ( gxTv_SdtSDT_Parametros_Character != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtSDT_Parametros_Character.writexmlcollection(oWriter, "Character", sNameSpace1, "Value", sNameSpace1);
         }
         if ( gxTv_SdtSDT_Parametros_Numeric != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtSDT_Parametros_Numeric.writexmlcollection(oWriter, "Numeric", sNameSpace1, "Value", sNameSpace1);
         }
         if ( gxTv_SdtSDT_Parametros_Datetime != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtSDT_Parametros_Datetime.writexmlcollection(oWriter, "DateTime", sNameSpace1, "Value", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         if ( gxTv_SdtSDT_Parametros_Character != null )
         {
            AddObjectProperty("Character", gxTv_SdtSDT_Parametros_Character, false);
         }
         if ( gxTv_SdtSDT_Parametros_Numeric != null )
         {
            AddObjectProperty("Numeric", gxTv_SdtSDT_Parametros_Numeric, false);
         }
         if ( gxTv_SdtSDT_Parametros_Datetime != null )
         {
            AddObjectProperty("DateTime", gxTv_SdtSDT_Parametros_Datetime, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Character" )]
      [  XmlArray( ElementName = "Character"  )]
      [  XmlArrayItemAttribute( Type= typeof( String ), ElementName= "Value"  , IsNullable=false)]
      public GxSimpleCollection gxTpr_Character_GxSimpleCollection
      {
         get {
            if ( gxTv_SdtSDT_Parametros_Character == null )
            {
               gxTv_SdtSDT_Parametros_Character = new GxSimpleCollection();
            }
            return (GxSimpleCollection)gxTv_SdtSDT_Parametros_Character ;
         }

         set {
            if ( gxTv_SdtSDT_Parametros_Character == null )
            {
               gxTv_SdtSDT_Parametros_Character = new GxSimpleCollection();
            }
            gxTv_SdtSDT_Parametros_Character = (GxSimpleCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Character
      {
         get {
            if ( gxTv_SdtSDT_Parametros_Character == null )
            {
               gxTv_SdtSDT_Parametros_Character = new GxSimpleCollection();
            }
            return gxTv_SdtSDT_Parametros_Character ;
         }

         set {
            gxTv_SdtSDT_Parametros_Character = value;
         }

      }

      public void gxTv_SdtSDT_Parametros_Character_SetNull( )
      {
         gxTv_SdtSDT_Parametros_Character = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Parametros_Character_IsNull( )
      {
         if ( gxTv_SdtSDT_Parametros_Character == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "Numeric" )]
      [  XmlArray( ElementName = "Numeric"  )]
      [  XmlArrayItemAttribute( Type= typeof( decimal ), ElementName= "Value"  , IsNullable=false)]
      public GxSimpleCollection gxTpr_Numeric_GxSimpleCollection
      {
         get {
            if ( gxTv_SdtSDT_Parametros_Numeric == null )
            {
               gxTv_SdtSDT_Parametros_Numeric = new GxSimpleCollection();
            }
            return (GxSimpleCollection)gxTv_SdtSDT_Parametros_Numeric ;
         }

         set {
            if ( gxTv_SdtSDT_Parametros_Numeric == null )
            {
               gxTv_SdtSDT_Parametros_Numeric = new GxSimpleCollection();
            }
            gxTv_SdtSDT_Parametros_Numeric = (GxSimpleCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Numeric
      {
         get {
            if ( gxTv_SdtSDT_Parametros_Numeric == null )
            {
               gxTv_SdtSDT_Parametros_Numeric = new GxSimpleCollection();
            }
            return gxTv_SdtSDT_Parametros_Numeric ;
         }

         set {
            gxTv_SdtSDT_Parametros_Numeric = value;
         }

      }

      public void gxTv_SdtSDT_Parametros_Numeric_SetNull( )
      {
         gxTv_SdtSDT_Parametros_Numeric = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Parametros_Numeric_IsNull( )
      {
         if ( gxTv_SdtSDT_Parametros_Numeric == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "DateTime" )]
      [  XmlArray( ElementName = "DateTime"  )]
      [  XmlArrayItemAttribute( Type= typeof( DateTime ), ElementName= "Value"  , IsNullable=false)]
      public GxSimpleCollection gxTpr_Datetime_GxSimpleCollection
      {
         get {
            if ( gxTv_SdtSDT_Parametros_Datetime == null )
            {
               gxTv_SdtSDT_Parametros_Datetime = new GxSimpleCollection();
            }
            return (GxSimpleCollection)gxTv_SdtSDT_Parametros_Datetime ;
         }

         set {
            if ( gxTv_SdtSDT_Parametros_Datetime == null )
            {
               gxTv_SdtSDT_Parametros_Datetime = new GxSimpleCollection();
            }
            gxTv_SdtSDT_Parametros_Datetime = (GxSimpleCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Datetime
      {
         get {
            if ( gxTv_SdtSDT_Parametros_Datetime == null )
            {
               gxTv_SdtSDT_Parametros_Datetime = new GxSimpleCollection();
            }
            return gxTv_SdtSDT_Parametros_Datetime ;
         }

         set {
            gxTv_SdtSDT_Parametros_Datetime = value;
         }

      }

      public void gxTv_SdtSDT_Parametros_Datetime_SetNull( )
      {
         gxTv_SdtSDT_Parametros_Datetime = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Parametros_Datetime_IsNull( )
      {
         if ( gxTv_SdtSDT_Parametros_Datetime == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( decimal ))]
      protected IGxCollection gxTv_SdtSDT_Parametros_Numeric=null ;
      [ObjectCollection(ItemType=typeof( String ))]
      protected IGxCollection gxTv_SdtSDT_Parametros_Character=null ;
      [ObjectCollection(ItemType=typeof( DateTime ))]
      protected IGxCollection gxTv_SdtSDT_Parametros_Datetime=null ;
   }

   [DataContract(Name = @"SDT_Parametros", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_Parametros_RESTInterface : GxGenericCollectionItem<SdtSDT_Parametros>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Parametros_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Parametros_RESTInterface( SdtSDT_Parametros psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Character" , Order = 0 )]
      public GxSimpleCollection gxTpr_Character
      {
         get {
            return (GxSimpleCollection)(sdt.gxTpr_Character) ;
         }

         set {
            sdt.gxTpr_Character = value;
         }

      }

      [DataMember( Name = "Numeric" , Order = 1 )]
      public GxSimpleCollection gxTpr_Numeric
      {
         get {
            return (GxSimpleCollection)(sdt.gxTpr_Numeric) ;
         }

         set {
            sdt.gxTpr_Numeric = value;
         }

      }

      [DataMember( Name = "DateTime" , Order = 2 )]
      public GxSimpleCollection gxTpr_Datetime
      {
         get {
            return (GxSimpleCollection)(sdt.gxTpr_Datetime) ;
         }

         set {
            sdt.gxTpr_Datetime = value;
         }

      }

      public SdtSDT_Parametros sdt
      {
         get {
            return (SdtSDT_Parametros)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Parametros() ;
         }
      }

   }

}
