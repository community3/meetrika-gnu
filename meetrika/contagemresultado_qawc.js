/**@preserve  GeneXus C# 10_3_14-114418 on 3/24/2020 22:57:20.81
*/
gx.evt.autoSkip = false;
gx.define('contagemresultado_qawc', true, function (CmpContext) {
   this.ServerClass =  "contagemresultado_qawc" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.A456ContagemResultado_Codigo=gx.fn.getIntegerValue("CONTAGEMRESULTADO_CODIGO",'.') ;
      this.AV53OS_Codigo=gx.fn.getIntegerValue("vOS_CODIGO",'.') ;
      this.A1603ContagemResultado_CntCod=gx.fn.getIntegerValue("CONTAGEMRESULTADO_CNTCOD",'.') ;
      this.A1078ContratoGestor_ContratoCod=gx.fn.getIntegerValue("CONTRATOGESTOR_CONTRATOCOD",'.') ;
      this.AV22Contrato_Codigo=gx.fn.getIntegerValue("vCONTRATO_CODIGO",'.') ;
      this.A1079ContratoGestor_UsuarioCod=gx.fn.getIntegerValue("CONTRATOGESTOR_USUARIOCOD",'.') ;
      this.AV6WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.A1985ContagemResultadoQA_OSCod=gx.fn.getIntegerValue("CONTAGEMRESULTADOQA_OSCOD",'.') ;
      this.A1996ContagemResultadoQA_RespostaDe=gx.fn.getIntegerValue("CONTAGEMRESULTADOQA_RESPOSTADE",'.') ;
      this.A484ContagemResultado_StatusDmn=gx.fn.getControlValue("CONTAGEMRESULTADO_STATUSDMN") ;
      this.A1984ContagemResultadoQA_Codigo=gx.fn.getIntegerValue("CONTAGEMRESULTADOQA_CODIGO",'.') ;
      this.A1986ContagemResultadoQA_DataHora=gx.fn.getDateTimeValue("CONTAGEMRESULTADOQA_DATAHORA") ;
      this.A1990ContagemResultadoQA_UserPesNom=gx.fn.getControlValue("CONTAGEMRESULTADOQA_USERPESNOM") ;
      this.A1994ContagemResultadoQA_ParaPesNom=gx.fn.getControlValue("CONTAGEMRESULTADOQA_PARAPESNOM") ;
      this.A1987ContagemResultadoQA_Texto=gx.fn.getControlValue("CONTAGEMRESULTADOQA_TEXTO") ;
      this.AV23Respondida=gx.fn.getControlValue("vRESPONDIDA") ;
      this.AV44RContagemResultadoQA_Codigo=gx.fn.getIntegerValue("vRCONTAGEMRESULTADOQA_CODIGO",'.') ;
      this.AV45RContagemResultadoQA_RespostaDe=gx.fn.getIntegerValue("vRCONTAGEMRESULTADOQA_RESPOSTADE",'.') ;
      this.AV46RContagemResultadoQA_DataHora=gx.fn.getControlValue("vRCONTAGEMRESULTADOQA_DATAHORA") ;
      this.AV47RContagemResultadoQA_UserPesNom=gx.fn.getControlValue("vRCONTAGEMRESULTADOQA_USERPESNOM") ;
      this.AV48RContagemResultadoQA_ParaPesNom=gx.fn.getControlValue("vRCONTAGEMRESULTADOQA_PARAPESNOM") ;
      this.AV49RContagemResultadoQA_Texto=gx.fn.getControlValue("vRCONTAGEMRESULTADOQA_TEXTO") ;
      this.A1992ContagemResultadoQA_ParaCod=gx.fn.getIntegerValue("CONTAGEMRESULTADOQA_PARACOD",'.') ;
      this.AV51Finalizada=gx.fn.getControlValue("vFINALIZADA") ;
      this.AV32EhColega=gx.fn.getControlValue("vEHCOLEGA") ;
      this.AV20UserEhGestor=gx.fn.getControlValue("vUSEREHGESTOR") ;
      this.A66ContratadaUsuario_ContratadaCod=gx.fn.getIntegerValue("CONTRATADAUSUARIO_CONTRATADACOD",'.') ;
      this.A69ContratadaUsuario_UsuarioCod=gx.fn.getIntegerValue("CONTRATADAUSUARIO_USUARIOCOD",'.') ;
      this.AV50ContagemResultadoQA_ParaCod=gx.fn.getIntegerValue("vCONTAGEMRESULTADOQA_PARACOD",'.') ;
      this.A63ContratanteUsuario_ContratanteCod=gx.fn.getIntegerValue("CONTRATANTEUSUARIO_CONTRATANTECOD",'.') ;
      this.A60ContratanteUsuario_UsuarioCod=gx.fn.getIntegerValue("CONTRATANTEUSUARIO_USUARIOCOD",'.') ;
   };
   this.Validv_Contagemresultadoqa_codigo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADOQA_CODIGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e11r22_client=function()
   {
      this.executeServerEvent("'DONOVAQA'", true, null, false, false);
   };
   this.e15r22_client=function()
   {
      this.executeServerEvent("VRESPONDER.CLICK", true, arguments[0], false, false);
   };
   this.e16r22_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e17r22_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,8,12,13,14,15,16,17,18,20];
   this.GXLastCtrlId =20;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",11,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"contagemresultado_qawc",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",false,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addBitmap("&Warning","vWARNING",12,0,"px",17,"px",null,"","","Image","");
   GridContainer.addSingleLineEdit("Contagemresultadoqa_codigo",13,"vCONTAGEMRESULTADOQA_CODIGO","Contagem Resultado QA_Codigo","","ContagemResultadoQA_Codigo","int",0,"px",6,6,"right",null,[],"Contagemresultadoqa_codigo","ContagemResultadoQA_Codigo",false,0,false,false,"Attribute",1,"");
   GridContainer.addSingleLineEdit("Contagemresultadoqa_datahora",14,"vCONTAGEMRESULTADOQA_DATAHORA","Data","","ContagemResultadoQA_DataHora","char",0,"px",20,20,"left",null,[],"Contagemresultadoqa_datahora","ContagemResultadoQA_DataHora",true,0,false,false,"Attribute",1,"");
   GridContainer.addSingleLineEdit("Contagemresultadoqa_userpesnom",15,"vCONTAGEMRESULTADOQA_USERPESNOM","De","","ContagemResultadoQA_UserPesNom","char",0,"px",100,80,"left",null,[],"Contagemresultadoqa_userpesnom","ContagemResultadoQA_UserPesNom",true,0,false,false,"BootstrapAttribute100",1,"");
   GridContainer.addSingleLineEdit("Contagemresultadoqa_parapesnom",16,"vCONTAGEMRESULTADOQA_PARAPESNOM","Para","","ContagemResultadoQA_ParaPesNom","char",0,"px",100,80,"left",null,[],"Contagemresultadoqa_parapesnom","ContagemResultadoQA_ParaPesNom",true,0,false,false,"BootstrapAttribute100",1,"");
   GridContainer.addSingleLineEdit("Contagemresultadoqa_texto",17,"vCONTAGEMRESULTADOQA_TEXTO","Texto","","ContagemResultadoQA_Texto","vchar",0,"px",2097152,80,"left",null,[],"Contagemresultadoqa_texto","ContagemResultadoQA_Texto",true,0,false,false,"Attribute",1,"");
   GridContainer.addBitmap("&Responder","vRESPONDER",18,0,"px",17,"px","e15r22_client","","Responder","Image","");
   this.setGrid(GridContainer);
   GXValidFnc[2]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[5]={fld:"USRTABLE",grid:0};
   GXValidFnc[8]={fld:"GRIDTABLE",grid:0};
   GXValidFnc[12]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vWARNING",gxz:"ZV36Warning",gxold:"OV36Warning",gxvar:"AV36Warning",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV36Warning=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV36Warning=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vWARNING",row || gx.fn.currentGridRowImpl(11),gx.O.AV36Warning,gx.O.AV61Warning_GXI)},c2v:function(){gx.O.AV61Warning_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV36Warning=this.val()},val:function(row){return gx.fn.getGridControlValue("vWARNING",row || gx.fn.currentGridRowImpl(11))},val_GXI:function(row){return gx.fn.getGridControlValue("vWARNING_GXI",row || gx.fn.currentGridRowImpl(11))}, gxvar_GXI:'AV61Warning_GXI',nac:gx.falseFn};
   GXValidFnc[13]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:this.Validv_Contagemresultadoqa_codigo,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADOQA_CODIGO",gxz:"ZV38ContagemResultadoQA_Codigo",gxold:"OV38ContagemResultadoQA_Codigo",gxvar:"AV38ContagemResultadoQA_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV38ContagemResultadoQA_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV38ContagemResultadoQA_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("vCONTAGEMRESULTADOQA_CODIGO",row || gx.fn.currentGridRowImpl(11),gx.O.AV38ContagemResultadoQA_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV38ContagemResultadoQA_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("vCONTAGEMRESULTADOQA_CODIGO",row || gx.fn.currentGridRowImpl(11),'.')},nac:gx.falseFn};
   GXValidFnc[14]={lvl:2,type:"char",len:20,dec:0,sign:false,ro:0,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADOQA_DATAHORA",gxz:"ZV40ContagemResultadoQA_DataHora",gxold:"OV40ContagemResultadoQA_DataHora",gxvar:"AV40ContagemResultadoQA_DataHora",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.AV40ContagemResultadoQA_DataHora=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV40ContagemResultadoQA_DataHora=Value},v2c:function(row){gx.fn.setGridControlValue("vCONTAGEMRESULTADOQA_DATAHORA",row || gx.fn.currentGridRowImpl(11),gx.O.AV40ContagemResultadoQA_DataHora,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV40ContagemResultadoQA_DataHora=this.val()},val:function(row){return gx.fn.getGridControlValue("vCONTAGEMRESULTADOQA_DATAHORA",row || gx.fn.currentGridRowImpl(11))},nac:gx.falseFn};
   GXValidFnc[15]={lvl:2,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADOQA_USERPESNOM",gxz:"ZV41ContagemResultadoQA_UserPesNom",gxold:"OV41ContagemResultadoQA_UserPesNom",gxvar:"AV41ContagemResultadoQA_UserPesNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.AV41ContagemResultadoQA_UserPesNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV41ContagemResultadoQA_UserPesNom=Value},v2c:function(row){gx.fn.setGridControlValue("vCONTAGEMRESULTADOQA_USERPESNOM",row || gx.fn.currentGridRowImpl(11),gx.O.AV41ContagemResultadoQA_UserPesNom,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV41ContagemResultadoQA_UserPesNom=this.val()},val:function(row){return gx.fn.getGridControlValue("vCONTAGEMRESULTADOQA_USERPESNOM",row || gx.fn.currentGridRowImpl(11))},nac:gx.falseFn};
   GXValidFnc[16]={lvl:2,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADOQA_PARAPESNOM",gxz:"ZV42ContagemResultadoQA_ParaPesNom",gxold:"OV42ContagemResultadoQA_ParaPesNom",gxvar:"AV42ContagemResultadoQA_ParaPesNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.AV42ContagemResultadoQA_ParaPesNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV42ContagemResultadoQA_ParaPesNom=Value},v2c:function(row){gx.fn.setGridControlValue("vCONTAGEMRESULTADOQA_PARAPESNOM",row || gx.fn.currentGridRowImpl(11),gx.O.AV42ContagemResultadoQA_ParaPesNom,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV42ContagemResultadoQA_ParaPesNom=this.val()},val:function(row){return gx.fn.getGridControlValue("vCONTAGEMRESULTADOQA_PARAPESNOM",row || gx.fn.currentGridRowImpl(11))},nac:gx.falseFn};
   GXValidFnc[17]={lvl:2,type:"vchar",len:2097152,dec:0,sign:false,ro:0,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADOQA_TEXTO",gxz:"ZV43ContagemResultadoQA_Texto",gxold:"OV43ContagemResultadoQA_Texto",gxvar:"AV43ContagemResultadoQA_Texto",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.AV43ContagemResultadoQA_Texto=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV43ContagemResultadoQA_Texto=Value},v2c:function(row){gx.fn.setGridControlValue("vCONTAGEMRESULTADOQA_TEXTO",row || gx.fn.currentGridRowImpl(11),gx.O.AV43ContagemResultadoQA_Texto,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV43ContagemResultadoQA_Texto=this.val()},val:function(row){return gx.fn.getGridControlValue("vCONTAGEMRESULTADOQA_TEXTO",row || gx.fn.currentGridRowImpl(11))},nac:gx.falseFn};
   GXValidFnc[18]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vRESPONDER",gxz:"ZV19Responder",gxold:"OV19Responder",gxvar:"AV19Responder",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV19Responder=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV19Responder=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vRESPONDER",row || gx.fn.currentGridRowImpl(11),gx.O.AV19Responder,gx.O.AV59Responder_GXI)},c2v:function(){gx.O.AV59Responder_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV19Responder=this.val()},val:function(row){return gx.fn.getGridControlValue("vRESPONDER",row || gx.fn.currentGridRowImpl(11))},val_GXI:function(row){return gx.fn.getGridControlValue("vRESPONDER_GXI",row || gx.fn.currentGridRowImpl(11))}, gxvar_GXI:'AV59Responder_GXI',nac:gx.falseFn};
   GXValidFnc[20]={fld:"NOVAQA",grid:0};
   this.ZV36Warning = "" ;
   this.OV36Warning = "" ;
   this.ZV38ContagemResultadoQA_Codigo = 0 ;
   this.OV38ContagemResultadoQA_Codigo = 0 ;
   this.ZV40ContagemResultadoQA_DataHora = "" ;
   this.OV40ContagemResultadoQA_DataHora = "" ;
   this.ZV41ContagemResultadoQA_UserPesNom = "" ;
   this.OV41ContagemResultadoQA_UserPesNom = "" ;
   this.ZV42ContagemResultadoQA_ParaPesNom = "" ;
   this.OV42ContagemResultadoQA_ParaPesNom = "" ;
   this.ZV43ContagemResultadoQA_Texto = "" ;
   this.OV43ContagemResultadoQA_Texto = "" ;
   this.ZV19Responder = "" ;
   this.OV19Responder = "" ;
   this.AV53OS_Codigo = 0 ;
   this.AV36Warning = "" ;
   this.AV38ContagemResultadoQA_Codigo = 0 ;
   this.AV40ContagemResultadoQA_DataHora = "" ;
   this.AV41ContagemResultadoQA_UserPesNom = "" ;
   this.AV42ContagemResultadoQA_ParaPesNom = "" ;
   this.AV43ContagemResultadoQA_Texto = "" ;
   this.AV19Responder = "" ;
   this.A60ContratanteUsuario_UsuarioCod = 0 ;
   this.A63ContratanteUsuario_ContratanteCod = 0 ;
   this.A69ContratadaUsuario_UsuarioCod = 0 ;
   this.A66ContratadaUsuario_ContratadaCod = 0 ;
   this.A1996ContagemResultadoQA_RespostaDe = 0 ;
   this.A1984ContagemResultadoQA_Codigo = 0 ;
   this.A1986ContagemResultadoQA_DataHora = gx.date.nullDate() ;
   this.A1990ContagemResultadoQA_UserPesNom = "" ;
   this.A1994ContagemResultadoQA_ParaPesNom = "" ;
   this.A1987ContagemResultadoQA_Texto = "" ;
   this.A1988ContagemResultadoQA_UserCod = 0 ;
   this.A1989ContagemResultadoQA_UserPesCod = 0 ;
   this.A1992ContagemResultadoQA_ParaCod = 0 ;
   this.A1993ContagemResultadoQA_ParaPesCod = 0 ;
   this.A1079ContratoGestor_UsuarioCod = 0 ;
   this.A1078ContratoGestor_ContratoCod = 0 ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.A1603ContagemResultado_CntCod = 0 ;
   this.A1553ContagemResultado_CntSrvCod = 0 ;
   this.A484ContagemResultado_StatusDmn = "" ;
   this.A1985ContagemResultadoQA_OSCod = 0 ;
   this.AV22Contrato_Codigo = 0 ;
   this.AV6WWPContext = {} ;
   this.AV23Respondida = false ;
   this.AV44RContagemResultadoQA_Codigo = 0 ;
   this.AV45RContagemResultadoQA_RespostaDe = 0 ;
   this.AV46RContagemResultadoQA_DataHora = "" ;
   this.AV47RContagemResultadoQA_UserPesNom = "" ;
   this.AV48RContagemResultadoQA_ParaPesNom = "" ;
   this.AV49RContagemResultadoQA_Texto = "" ;
   this.AV51Finalizada = "" ;
   this.AV32EhColega = false ;
   this.AV20UserEhGestor = false ;
   this.AV50ContagemResultadoQA_ParaCod = 0 ;
   this.Events = {"e11r22_client": ["'DONOVAQA'", true] ,"e15r22_client": ["VRESPONDER.CLICK", true] ,"e16r22_client": ["ENTER", true] ,"e17r22_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A1985ContagemResultadoQA_OSCod',fld:'CONTAGEMRESULTADOQA_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1996ContagemResultadoQA_RespostaDe',fld:'CONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A1984ContagemResultadoQA_Codigo',fld:'CONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1986ContagemResultadoQA_DataHora',fld:'CONTAGEMRESULTADOQA_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'A1990ContagemResultadoQA_UserPesNom',fld:'CONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'A1994ContagemResultadoQA_ParaPesNom',fld:'CONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'A1987ContagemResultadoQA_Texto',fld:'CONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'AV23Respondida',fld:'vRESPONDIDA',pic:'',nv:false},{av:'AV44RContagemResultadoQA_Codigo',fld:'vRCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45RContagemResultadoQA_RespostaDe',fld:'vRCONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'AV46RContagemResultadoQA_DataHora',fld:'vRCONTAGEMRESULTADOQA_DATAHORA',pic:'',nv:''},{av:'AV47RContagemResultadoQA_UserPesNom',fld:'vRCONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'AV48RContagemResultadoQA_ParaPesNom',fld:'vRCONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'AV49RContagemResultadoQA_Texto',fld:'vRCONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'A1992ContagemResultadoQA_ParaCod',fld:'CONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Finalizada',fld:'vFINALIZADA',pic:'',nv:''},{av:'AV32EhColega',fld:'vEHCOLEGA',pic:'',nv:false},{av:'AV20UserEhGestor',fld:'vUSEREHGESTOR',pic:'',nv:false},{av:'AV38ContagemResultadoQA_Codigo',fld:'vCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV50ContagemResultadoQA_ParaCod',fld:'vCONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53OS_Codigo',fld:'vOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1603ContagemResultado_CntCod',fld:'CONTAGEMRESULTADO_CNTCOD',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'gx.fn.getCtrlProperty("NOVAQA","Visible")',ctrl:'NOVAQA',prop:'Visible'},{av:'AV19Responder',fld:'vRESPONDER',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vRESPONDER","Tooltiptext")',ctrl:'vRESPONDER',prop:'Tooltiptext'},{av:'AV22Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36Warning',fld:'vWARNING',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vWARNING","Tooltiptext")',ctrl:'vWARNING',prop:'Tooltiptext'},{av:'AV51Finalizada',fld:'vFINALIZADA',pic:'',nv:''},{av:'AV20UserEhGestor',fld:'vUSEREHGESTOR',pic:'',nv:false}]];
   this.EvtParms["LOAD"] = [[{av:'A1985ContagemResultadoQA_OSCod',fld:'CONTAGEMRESULTADOQA_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV53OS_Codigo',fld:'vOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1996ContagemResultadoQA_RespostaDe',fld:'CONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A1984ContagemResultadoQA_Codigo',fld:'CONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1986ContagemResultadoQA_DataHora',fld:'CONTAGEMRESULTADOQA_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'A1990ContagemResultadoQA_UserPesNom',fld:'CONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'A1994ContagemResultadoQA_ParaPesNom',fld:'CONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'A1987ContagemResultadoQA_Texto',fld:'CONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'AV23Respondida',fld:'vRESPONDIDA',pic:'',nv:false},{av:'AV44RContagemResultadoQA_Codigo',fld:'vRCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45RContagemResultadoQA_RespostaDe',fld:'vRCONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'AV46RContagemResultadoQA_DataHora',fld:'vRCONTAGEMRESULTADOQA_DATAHORA',pic:'',nv:''},{av:'AV47RContagemResultadoQA_UserPesNom',fld:'vRCONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'AV48RContagemResultadoQA_ParaPesNom',fld:'vRCONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'AV49RContagemResultadoQA_Texto',fld:'vRCONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'A1992ContagemResultadoQA_ParaCod',fld:'CONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Finalizada',fld:'vFINALIZADA',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV32EhColega',fld:'vEHCOLEGA',pic:'',nv:false},{av:'AV20UserEhGestor',fld:'vUSEREHGESTOR',pic:'',nv:false},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38ContagemResultadoQA_Codigo',fld:'vCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV50ContagemResultadoQA_ParaCod',fld:'vCONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0}],[{av:'AV38ContagemResultadoQA_Codigo',fld:'vCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV40ContagemResultadoQA_DataHora',fld:'vCONTAGEMRESULTADOQA_DATAHORA',pic:'',hsh:true,nv:''},{av:'AV41ContagemResultadoQA_UserPesNom',fld:'vCONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',hsh:true,nv:''},{av:'AV42ContagemResultadoQA_ParaPesNom',fld:'vCONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',hsh:true,nv:''},{av:'AV43ContagemResultadoQA_Texto',fld:'vCONTAGEMRESULTADOQA_TEXTO',pic:'',hsh:true,nv:''},{av:'gx.fn.getCtrlProperty("vWARNING","Visible")',ctrl:'vWARNING',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_DATAHORA","Forecolor")',ctrl:'vCONTAGEMRESULTADOQA_DATAHORA',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_USERPESNOM","Forecolor")',ctrl:'vCONTAGEMRESULTADOQA_USERPESNOM',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_PARAPESNOM","Forecolor")',ctrl:'vCONTAGEMRESULTADOQA_PARAPESNOM',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_TEXTO","Forecolor")',ctrl:'vCONTAGEMRESULTADOQA_TEXTO',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_DATAHORA","Fontbold")',ctrl:'vCONTAGEMRESULTADOQA_DATAHORA',prop:'Fontbold'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_USERPESNOM","Fontbold")',ctrl:'vCONTAGEMRESULTADOQA_USERPESNOM',prop:'Fontbold'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_PARAPESNOM","Fontbold")',ctrl:'vCONTAGEMRESULTADOQA_PARAPESNOM',prop:'Fontbold'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_TEXTO","Fontbold")',ctrl:'vCONTAGEMRESULTADOQA_TEXTO',prop:'Fontbold'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_DATAHORA","Fontitalic")',ctrl:'vCONTAGEMRESULTADOQA_DATAHORA',prop:'Fontitalic'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_USERPESNOM","Fontitalic")',ctrl:'vCONTAGEMRESULTADOQA_USERPESNOM',prop:'Fontitalic'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_PARAPESNOM","Fontitalic")',ctrl:'vCONTAGEMRESULTADOQA_PARAPESNOM',prop:'Fontitalic'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_TEXTO","Fontitalic")',ctrl:'vCONTAGEMRESULTADOQA_TEXTO',prop:'Fontitalic'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_DATAHORA","Fontsize")',ctrl:'vCONTAGEMRESULTADOQA_DATAHORA',prop:'Fontsize'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_USERPESNOM","Fontsize")',ctrl:'vCONTAGEMRESULTADOQA_USERPESNOM',prop:'Fontsize'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_PARAPESNOM","Fontsize")',ctrl:'vCONTAGEMRESULTADOQA_PARAPESNOM',prop:'Fontsize'},{av:'gx.fn.getCtrlProperty("vCONTAGEMRESULTADOQA_TEXTO","Fontsize")',ctrl:'vCONTAGEMRESULTADOQA_TEXTO',prop:'Fontsize'},{av:'AV50ContagemResultadoQA_ParaCod',fld:'vCONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("vRESPONDER","Visible")',ctrl:'vRESPONDER',prop:'Visible'},{av:'gx.fn.getCtrlProperty("NOVAQA","Visible")',ctrl:'NOVAQA',prop:'Visible'},{av:'AV44RContagemResultadoQA_Codigo',fld:'vRCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45RContagemResultadoQA_RespostaDe',fld:'vRCONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'AV46RContagemResultadoQA_DataHora',fld:'vRCONTAGEMRESULTADOQA_DATAHORA',pic:'',nv:''},{av:'AV47RContagemResultadoQA_UserPesNom',fld:'vRCONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'AV48RContagemResultadoQA_ParaPesNom',fld:'vRCONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'AV49RContagemResultadoQA_Texto',fld:'vRCONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'AV23Respondida',fld:'vRESPONDIDA',pic:'',nv:false},{av:'AV32EhColega',fld:'vEHCOLEGA',pic:'',nv:false}]];
   this.EvtParms["'DONOVAQA'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53OS_Codigo',fld:'vOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1603ContagemResultado_CntCod',fld:'CONTAGEMRESULTADO_CNTCOD',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1985ContagemResultadoQA_OSCod',fld:'CONTAGEMRESULTADOQA_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1996ContagemResultadoQA_RespostaDe',fld:'CONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A1984ContagemResultadoQA_Codigo',fld:'CONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1986ContagemResultadoQA_DataHora',fld:'CONTAGEMRESULTADOQA_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'A1990ContagemResultadoQA_UserPesNom',fld:'CONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'A1994ContagemResultadoQA_ParaPesNom',fld:'CONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'A1987ContagemResultadoQA_Texto',fld:'CONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'AV23Respondida',fld:'vRESPONDIDA',pic:'',nv:false},{av:'AV44RContagemResultadoQA_Codigo',fld:'vRCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45RContagemResultadoQA_RespostaDe',fld:'vRCONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'AV46RContagemResultadoQA_DataHora',fld:'vRCONTAGEMRESULTADOQA_DATAHORA',pic:'',nv:''},{av:'AV47RContagemResultadoQA_UserPesNom',fld:'vRCONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'AV48RContagemResultadoQA_ParaPesNom',fld:'vRCONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'AV49RContagemResultadoQA_Texto',fld:'vRCONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'A1992ContagemResultadoQA_ParaCod',fld:'CONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Finalizada',fld:'vFINALIZADA',pic:'',nv:''},{av:'AV32EhColega',fld:'vEHCOLEGA',pic:'',nv:false},{av:'AV20UserEhGestor',fld:'vUSEREHGESTOR',pic:'',nv:false},{av:'AV38ContagemResultadoQA_Codigo',fld:'vCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV50ContagemResultadoQA_ParaCod',fld:'vCONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],[]];
   this.EvtParms["VRESPONDER.CLICK"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53OS_Codigo',fld:'vOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1603ContagemResultado_CntCod',fld:'CONTAGEMRESULTADO_CNTCOD',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1985ContagemResultadoQA_OSCod',fld:'CONTAGEMRESULTADOQA_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1996ContagemResultadoQA_RespostaDe',fld:'CONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A1984ContagemResultadoQA_Codigo',fld:'CONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1986ContagemResultadoQA_DataHora',fld:'CONTAGEMRESULTADOQA_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'A1990ContagemResultadoQA_UserPesNom',fld:'CONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'A1994ContagemResultadoQA_ParaPesNom',fld:'CONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'A1987ContagemResultadoQA_Texto',fld:'CONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'AV23Respondida',fld:'vRESPONDIDA',pic:'',nv:false},{av:'AV44RContagemResultadoQA_Codigo',fld:'vRCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45RContagemResultadoQA_RespostaDe',fld:'vRCONTAGEMRESULTADOQA_RESPOSTADE',pic:'ZZZZZ9',nv:0},{av:'AV46RContagemResultadoQA_DataHora',fld:'vRCONTAGEMRESULTADOQA_DATAHORA',pic:'',nv:''},{av:'AV47RContagemResultadoQA_UserPesNom',fld:'vRCONTAGEMRESULTADOQA_USERPESNOM',pic:'@!',nv:''},{av:'AV48RContagemResultadoQA_ParaPesNom',fld:'vRCONTAGEMRESULTADOQA_PARAPESNOM',pic:'@!',nv:''},{av:'AV49RContagemResultadoQA_Texto',fld:'vRCONTAGEMRESULTADOQA_TEXTO',pic:'',nv:''},{av:'A1992ContagemResultadoQA_ParaCod',fld:'CONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Finalizada',fld:'vFINALIZADA',pic:'',nv:''},{av:'AV32EhColega',fld:'vEHCOLEGA',pic:'',nv:false},{av:'AV20UserEhGestor',fld:'vUSEREHGESTOR',pic:'',nv:false},{av:'AV38ContagemResultadoQA_Codigo',fld:'vCONTAGEMRESULTADOQA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV50ContagemResultadoQA_ParaCod',fld:'vCONTAGEMRESULTADOQA_PARACOD',pic:'ZZZZZ9',nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],[]];
   this.setVCMap("A456ContagemResultado_Codigo", "CONTAGEMRESULTADO_CODIGO", 0, "int");
   this.setVCMap("AV53OS_Codigo", "vOS_CODIGO", 0, "int");
   this.setVCMap("A1603ContagemResultado_CntCod", "CONTAGEMRESULTADO_CNTCOD", 0, "int");
   this.setVCMap("A1078ContratoGestor_ContratoCod", "CONTRATOGESTOR_CONTRATOCOD", 0, "int");
   this.setVCMap("AV22Contrato_Codigo", "vCONTRATO_CODIGO", 0, "int");
   this.setVCMap("A1079ContratoGestor_UsuarioCod", "CONTRATOGESTOR_USUARIOCOD", 0, "int");
   this.setVCMap("AV6WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("A1985ContagemResultadoQA_OSCod", "CONTAGEMRESULTADOQA_OSCOD", 0, "int");
   this.setVCMap("A1996ContagemResultadoQA_RespostaDe", "CONTAGEMRESULTADOQA_RESPOSTADE", 0, "int");
   this.setVCMap("A484ContagemResultado_StatusDmn", "CONTAGEMRESULTADO_STATUSDMN", 0, "char");
   this.setVCMap("A1984ContagemResultadoQA_Codigo", "CONTAGEMRESULTADOQA_CODIGO", 0, "int");
   this.setVCMap("A1986ContagemResultadoQA_DataHora", "CONTAGEMRESULTADOQA_DATAHORA", 0, "dtime");
   this.setVCMap("A1990ContagemResultadoQA_UserPesNom", "CONTAGEMRESULTADOQA_USERPESNOM", 0, "char");
   this.setVCMap("A1994ContagemResultadoQA_ParaPesNom", "CONTAGEMRESULTADOQA_PARAPESNOM", 0, "char");
   this.setVCMap("A1987ContagemResultadoQA_Texto", "CONTAGEMRESULTADOQA_TEXTO", 0, "vchar");
   this.setVCMap("AV23Respondida", "vRESPONDIDA", 0, "boolean");
   this.setVCMap("AV44RContagemResultadoQA_Codigo", "vRCONTAGEMRESULTADOQA_CODIGO", 0, "int");
   this.setVCMap("AV45RContagemResultadoQA_RespostaDe", "vRCONTAGEMRESULTADOQA_RESPOSTADE", 0, "int");
   this.setVCMap("AV46RContagemResultadoQA_DataHora", "vRCONTAGEMRESULTADOQA_DATAHORA", 0, "char");
   this.setVCMap("AV47RContagemResultadoQA_UserPesNom", "vRCONTAGEMRESULTADOQA_USERPESNOM", 0, "char");
   this.setVCMap("AV48RContagemResultadoQA_ParaPesNom", "vRCONTAGEMRESULTADOQA_PARAPESNOM", 0, "char");
   this.setVCMap("AV49RContagemResultadoQA_Texto", "vRCONTAGEMRESULTADOQA_TEXTO", 0, "vchar");
   this.setVCMap("A1992ContagemResultadoQA_ParaCod", "CONTAGEMRESULTADOQA_PARACOD", 0, "int");
   this.setVCMap("AV51Finalizada", "vFINALIZADA", 0, "char");
   this.setVCMap("AV32EhColega", "vEHCOLEGA", 0, "boolean");
   this.setVCMap("AV20UserEhGestor", "vUSEREHGESTOR", 0, "boolean");
   this.setVCMap("A66ContratadaUsuario_ContratadaCod", "CONTRATADAUSUARIO_CONTRATADACOD", 0, "int");
   this.setVCMap("A69ContratadaUsuario_UsuarioCod", "CONTRATADAUSUARIO_USUARIOCOD", 0, "int");
   this.setVCMap("AV50ContagemResultadoQA_ParaCod", "vCONTAGEMRESULTADOQA_PARACOD", 0, "int");
   this.setVCMap("A63ContratanteUsuario_ContratanteCod", "CONTRATANTEUSUARIO_CONTRATANTECOD", 0, "int");
   this.setVCMap("A60ContratanteUsuario_UsuarioCod", "CONTRATANTEUSUARIO_USUARIOCOD", 0, "int");
   this.setVCMap("A456ContagemResultado_Codigo", "CONTAGEMRESULTADO_CODIGO", 0, "int");
   this.setVCMap("AV53OS_Codigo", "vOS_CODIGO", 0, "int");
   this.setVCMap("A1603ContagemResultado_CntCod", "CONTAGEMRESULTADO_CNTCOD", 0, "int");
   this.setVCMap("A1078ContratoGestor_ContratoCod", "CONTRATOGESTOR_CONTRATOCOD", 0, "int");
   this.setVCMap("AV22Contrato_Codigo", "vCONTRATO_CODIGO", 0, "int");
   this.setVCMap("A1079ContratoGestor_UsuarioCod", "CONTRATOGESTOR_USUARIOCOD", 0, "int");
   this.setVCMap("AV6WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("A1985ContagemResultadoQA_OSCod", "CONTAGEMRESULTADOQA_OSCOD", 0, "int");
   this.setVCMap("A1996ContagemResultadoQA_RespostaDe", "CONTAGEMRESULTADOQA_RESPOSTADE", 0, "int");
   this.setVCMap("A484ContagemResultado_StatusDmn", "CONTAGEMRESULTADO_STATUSDMN", 0, "char");
   this.setVCMap("A1984ContagemResultadoQA_Codigo", "CONTAGEMRESULTADOQA_CODIGO", 0, "int");
   this.setVCMap("A1986ContagemResultadoQA_DataHora", "CONTAGEMRESULTADOQA_DATAHORA", 0, "dtime");
   this.setVCMap("A1990ContagemResultadoQA_UserPesNom", "CONTAGEMRESULTADOQA_USERPESNOM", 0, "char");
   this.setVCMap("A1994ContagemResultadoQA_ParaPesNom", "CONTAGEMRESULTADOQA_PARAPESNOM", 0, "char");
   this.setVCMap("A1987ContagemResultadoQA_Texto", "CONTAGEMRESULTADOQA_TEXTO", 0, "vchar");
   this.setVCMap("AV23Respondida", "vRESPONDIDA", 0, "boolean");
   this.setVCMap("AV44RContagemResultadoQA_Codigo", "vRCONTAGEMRESULTADOQA_CODIGO", 0, "int");
   this.setVCMap("AV45RContagemResultadoQA_RespostaDe", "vRCONTAGEMRESULTADOQA_RESPOSTADE", 0, "int");
   this.setVCMap("AV46RContagemResultadoQA_DataHora", "vRCONTAGEMRESULTADOQA_DATAHORA", 0, "char");
   this.setVCMap("AV47RContagemResultadoQA_UserPesNom", "vRCONTAGEMRESULTADOQA_USERPESNOM", 0, "char");
   this.setVCMap("AV48RContagemResultadoQA_ParaPesNom", "vRCONTAGEMRESULTADOQA_PARAPESNOM", 0, "char");
   this.setVCMap("AV49RContagemResultadoQA_Texto", "vRCONTAGEMRESULTADOQA_TEXTO", 0, "vchar");
   this.setVCMap("A1992ContagemResultadoQA_ParaCod", "CONTAGEMRESULTADOQA_PARACOD", 0, "int");
   this.setVCMap("AV51Finalizada", "vFINALIZADA", 0, "char");
   this.setVCMap("AV32EhColega", "vEHCOLEGA", 0, "boolean");
   this.setVCMap("AV20UserEhGestor", "vUSEREHGESTOR", 0, "boolean");
   this.setVCMap("A66ContratadaUsuario_ContratadaCod", "CONTRATADAUSUARIO_CONTRATADACOD", 0, "int");
   this.setVCMap("A69ContratadaUsuario_UsuarioCod", "CONTRATADAUSUARIO_USUARIOCOD", 0, "int");
   this.setVCMap("AV50ContagemResultadoQA_ParaCod", "vCONTAGEMRESULTADOQA_PARACOD", 0, "int");
   this.setVCMap("A63ContratanteUsuario_ContratanteCod", "CONTRATANTEUSUARIO_CONTRATANTECOD", 0, "int");
   this.setVCMap("A60ContratanteUsuario_UsuarioCod", "CONTRATANTEUSUARIO_USUARIOCOD", 0, "int");
   GridContainer.addRefreshingVar({rfrVar:"A456ContagemResultado_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"AV53OS_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"A1603ContagemResultado_CntCod"});
   GridContainer.addRefreshingVar({rfrVar:"A1078ContratoGestor_ContratoCod"});
   GridContainer.addRefreshingVar({rfrVar:"AV22Contrato_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"A1079ContratoGestor_UsuarioCod"});
   GridContainer.addRefreshingVar({rfrVar:"AV6WWPContext"});
   GridContainer.addRefreshingVar({rfrVar:"A1985ContagemResultadoQA_OSCod"});
   GridContainer.addRefreshingVar({rfrVar:"A1996ContagemResultadoQA_RespostaDe"});
   GridContainer.addRefreshingVar({rfrVar:"A484ContagemResultado_StatusDmn"});
   GridContainer.addRefreshingVar({rfrVar:"A1984ContagemResultadoQA_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"A1986ContagemResultadoQA_DataHora"});
   GridContainer.addRefreshingVar({rfrVar:"A1990ContagemResultadoQA_UserPesNom"});
   GridContainer.addRefreshingVar({rfrVar:"A1994ContagemResultadoQA_ParaPesNom"});
   GridContainer.addRefreshingVar({rfrVar:"A1987ContagemResultadoQA_Texto"});
   GridContainer.addRefreshingVar({rfrVar:"AV23Respondida"});
   GridContainer.addRefreshingVar({rfrVar:"AV44RContagemResultadoQA_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"AV45RContagemResultadoQA_RespostaDe"});
   GridContainer.addRefreshingVar({rfrVar:"AV46RContagemResultadoQA_DataHora"});
   GridContainer.addRefreshingVar({rfrVar:"AV47RContagemResultadoQA_UserPesNom"});
   GridContainer.addRefreshingVar({rfrVar:"AV48RContagemResultadoQA_ParaPesNom"});
   GridContainer.addRefreshingVar({rfrVar:"AV49RContagemResultadoQA_Texto"});
   GridContainer.addRefreshingVar({rfrVar:"A1992ContagemResultadoQA_ParaCod"});
   GridContainer.addRefreshingVar({rfrVar:"AV51Finalizada"});
   GridContainer.addRefreshingVar({rfrVar:"AV32EhColega"});
   GridContainer.addRefreshingVar({rfrVar:"AV20UserEhGestor"});
   GridContainer.addRefreshingVar({rfrVar:"AV38ContagemResultadoQA_Codigo", rfrProp:"Value", gxAttId:"Contagemresultadoqa_codigo"});
   GridContainer.addRefreshingVar({rfrVar:"A66ContratadaUsuario_ContratadaCod"});
   GridContainer.addRefreshingVar({rfrVar:"A69ContratadaUsuario_UsuarioCod"});
   GridContainer.addRefreshingVar({rfrVar:"AV50ContagemResultadoQA_ParaCod"});
   GridContainer.addRefreshingVar({rfrVar:"A63ContratanteUsuario_ContratanteCod"});
   GridContainer.addRefreshingVar({rfrVar:"A60ContratanteUsuario_UsuarioCod"});
   this.InitStandaloneVars( );
});
