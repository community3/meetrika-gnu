/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:24:4.69
*/
gx.evt.autoSkip = false;
gx.define('wp_prazoexecucao', false, function () {
   this.ServerClass =  "wp_prazoexecucao" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV26Codigos=gx.fn.getControlValue("vCODIGOS") ;
      this.AV7UserId=gx.fn.getIntegerValue("vUSERID",'.') ;
      this.AV28ContagemResultado_Codigo=gx.fn.getIntegerValue("vCONTAGEMRESULTADO_CODIGO",'.') ;
   };
   this.Validv_Prazoatual=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vPRAZOATUAL");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Prazo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vPRAZO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV5Prazo)==0) || new gx.date.gxdate( this.AV5Prazo ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Prazo fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e14ob1_client=function()
   {
      this.clearMessages();
      if ( ! (new gx.date.gxdate('').compare(this.AV5Prazo)==0) && new gx.date.gxdate( this.AV5Prazo ).compare( this.AV27PrazoAtual ) < 0 )
      {
         this.addMessage("A nova data e horas não pode ser inferior ao prazo mínimo!");
         gx.fn.usrSetFocus("vPRAZO") ;
      }
      this.refreshOutputs([]);
   };
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("TBJAVA","Caption", "<script language=\"javascript\" type=\"javascript\">parent.location.replace(parent.location.href); </script>" );
   };
   this.e12ob2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e15ob1_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,10,13,15,20];
   this.GXLastCtrlId =20;
   GXValidFnc[2]={fld:"TABLE1",grid:0};
   GXValidFnc[8]={fld:"TEXTBLOCK1", format:0,grid:0};
   GXValidFnc[10]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Prazoatual,isvalid:null,rgrid:[],fld:"vPRAZOATUAL",gxz:"ZV27PrazoAtual",gxold:"OV27PrazoAtual",gxvar:"AV27PrazoAtual",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV27PrazoAtual=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV27PrazoAtual=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vPRAZOATUAL",gx.O.AV27PrazoAtual,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV27PrazoAtual=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vPRAZOATUAL")},nac:gx.falseFn};
   GXValidFnc[13]={fld:"TEXTBLOCK2", format:0,grid:0};
   GXValidFnc[15]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Prazo,isvalid:'e14ob1_client',rgrid:[],fld:"vPRAZO",gxz:"ZV5Prazo",gxold:"OV5Prazo",gxvar:"AV5Prazo",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[15],ip:[15],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV5Prazo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV5Prazo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vPRAZO",gx.O.AV5Prazo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV5Prazo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vPRAZO")},nac:gx.falseFn};
   GXValidFnc[20]={fld:"TBJAVA", format:1,grid:0};
   this.AV27PrazoAtual = gx.date.nullDate() ;
   this.ZV27PrazoAtual = gx.date.nullDate() ;
   this.OV27PrazoAtual = gx.date.nullDate() ;
   this.AV5Prazo = gx.date.nullDate() ;
   this.ZV5Prazo = gx.date.nullDate() ;
   this.OV5Prazo = gx.date.nullDate() ;
   this.AV27PrazoAtual = gx.date.nullDate() ;
   this.AV5Prazo = gx.date.nullDate() ;
   this.AV28ContagemResultado_Codigo = 0 ;
   this.AV7UserId = 0 ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.A1351ContagemResultado_DataPrevista = gx.date.nullDate() ;
   this.A484ContagemResultado_StatusDmn = "" ;
   this.A508ContagemResultado_Owner = 0 ;
   this.AV26Codigos = [ ] ;
   this.Events = {"e12ob2_client": ["ENTER", true] ,"e15ob1_client": ["CANCEL", true] ,"e14ob1_client": ["VPRAZO.ISVALID", false]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["VPRAZO.ISVALID"] = [[{av:'AV5Prazo',fld:'vPRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV27PrazoAtual',fld:'vPRAZOATUAL',pic:'99/99/99 99:99',hsh:true,nv:''}],[]];
   this.EvtParms["ENTER"] = [[{av:'AV5Prazo',fld:'vPRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV27PrazoAtual',fld:'vPRAZOATUAL',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV26Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV7UserId',fld:'vUSERID',pic:'ZZZZZZZZZZZZZZZZZ9',hsh:true,nv:0}],[{av:'gx.fn.getCtrlProperty("TBJAVA","Caption")',ctrl:'TBJAVA',prop:'Caption'}]];
   this.EnterCtrl = ["BTNENTER"];
   this.setVCMap("AV26Codigos", "vCODIGOS", 0, "Collint");
   this.setVCMap("AV7UserId", "vUSERID", 0, "int");
   this.setVCMap("AV28ContagemResultado_Codigo", "vCONTAGEMRESULTADO_CODIGO", 0, "int");
   this.InitStandaloneVars( );
});
gx.createParentObj(wp_prazoexecucao);
