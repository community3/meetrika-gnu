/*
               File: ContagemResultadoNaoCnf
        Description: Contagem Resultado Nao Cnf
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:12:26.44
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadonaocnf : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTAGEMRESULTADONAOCNF_NAOCNFCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTAGEMRESULTADONAOCNF_NAOCNFCOD52224( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A2020ContagemResultadoNaoCnf_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2020ContagemResultadoNaoCnf_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2020ContagemResultadoNaoCnf_OSCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A2020ContagemResultadoNaoCnf_OSCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_6") == 0 )
         {
            A2026ContagemResultadoNaoCnf_NaoCnfCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2026ContagemResultadoNaoCnf_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_6( A2026ContagemResultadoNaoCnf_NaoCnfCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A2023ContagemResultadoNaoCnf_LogRspCod = (long)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2023ContagemResultadoNaoCnf_LogRspCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2023ContagemResultadoNaoCnf_LogRspCod), 10, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A2023ContagemResultadoNaoCnf_LogRspCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_7") == 0 )
         {
            A2060ContagemResultadoNaoCnf_IndCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2060ContagemResultadoNaoCnf_IndCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2060ContagemResultadoNaoCnf_IndCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2060ContagemResultadoNaoCnf_IndCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_7( A2060ContagemResultadoNaoCnf_IndCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynContagemResultadoNaoCnf_NaoCnfCod.Name = "CONTAGEMRESULTADONAOCNF_NAOCNFCOD";
         dynContagemResultadoNaoCnf_NaoCnfCod.WebTags = "";
         chkContagemResultadoNaoCnf_Glosavel.Name = "CONTAGEMRESULTADONAOCNF_GLOSAVEL";
         chkContagemResultadoNaoCnf_Glosavel.WebTags = "";
         chkContagemResultadoNaoCnf_Glosavel.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContagemResultadoNaoCnf_Glosavel_Internalname, "TitleCaption", chkContagemResultadoNaoCnf_Glosavel.Caption);
         chkContagemResultadoNaoCnf_Glosavel.CheckedValue = "false";
         cmbContagemResultadoNaoCnf_OSEntregue.Name = "CONTAGEMRESULTADONAOCNF_OSENTREGUE";
         cmbContagemResultadoNaoCnf_OSEntregue.WebTags = "";
         cmbContagemResultadoNaoCnf_OSEntregue.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbContagemResultadoNaoCnf_OSEntregue.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbContagemResultadoNaoCnf_OSEntregue.ItemCount > 0 )
         {
            if ( (false==A2053ContagemResultadoNaoCnf_OSEntregue) )
            {
               A2053ContagemResultadoNaoCnf_OSEntregue = false;
               n2053ContagemResultadoNaoCnf_OSEntregue = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2053ContagemResultadoNaoCnf_OSEntregue", A2053ContagemResultadoNaoCnf_OSEntregue);
            }
            A2053ContagemResultadoNaoCnf_OSEntregue = StringUtil.StrToBool( cmbContagemResultadoNaoCnf_OSEntregue.getValidValue(StringUtil.BoolToStr( A2053ContagemResultadoNaoCnf_OSEntregue)));
            n2053ContagemResultadoNaoCnf_OSEntregue = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2053ContagemResultadoNaoCnf_OSEntregue", A2053ContagemResultadoNaoCnf_OSEntregue);
         }
         chkContagemResultadoNaoCnf_Contestada.Name = "CONTAGEMRESULTADONAOCNF_CONTESTADA";
         chkContagemResultadoNaoCnf_Contestada.WebTags = "";
         chkContagemResultadoNaoCnf_Contestada.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContagemResultadoNaoCnf_Contestada_Internalname, "TitleCaption", chkContagemResultadoNaoCnf_Contestada.Caption);
         chkContagemResultadoNaoCnf_Contestada.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Resultado Nao Cnf", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultadoNaoCnf_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadonaocnf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadonaocnf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynContagemResultadoNaoCnf_NaoCnfCod = new GXCombobox();
         chkContagemResultadoNaoCnf_Glosavel = new GXCheckbox();
         cmbContagemResultadoNaoCnf_OSEntregue = new GXCombobox();
         chkContagemResultadoNaoCnf_Contestada = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynContagemResultadoNaoCnf_NaoCnfCod.ItemCount > 0 )
         {
            A2026ContagemResultadoNaoCnf_NaoCnfCod = (int)(NumberUtil.Val( dynContagemResultadoNaoCnf_NaoCnfCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2026ContagemResultadoNaoCnf_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0)));
         }
         if ( cmbContagemResultadoNaoCnf_OSEntregue.ItemCount > 0 )
         {
            A2053ContagemResultadoNaoCnf_OSEntregue = StringUtil.StrToBool( cmbContagemResultadoNaoCnf_OSEntregue.getValidValue(StringUtil.BoolToStr( A2053ContagemResultadoNaoCnf_OSEntregue)));
            n2053ContagemResultadoNaoCnf_OSEntregue = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2053ContagemResultadoNaoCnf_OSEntregue", A2053ContagemResultadoNaoCnf_OSEntregue);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_52224( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_52224e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_52224( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_52224( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_52224e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Contagem Resultado Nao Cnf", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContagemResultadoNaoCnf.htm");
            wb_table3_28_52224( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_52224e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_52224e( true) ;
         }
         else
         {
            wb_table1_2_52224e( false) ;
         }
      }

      protected void wb_table3_28_52224( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_52224( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_52224e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoNaoCnf.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoNaoCnf.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoNaoCnf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_52224e( true) ;
         }
         else
         {
            wb_table3_28_52224e( false) ;
         }
      }

      protected void wb_table4_34_52224( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonaocnf_codigo_Internalname, "Id", "", "", lblTextblockcontagemresultadonaocnf_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoNaoCnf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNaoCnf_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), 6, 0, ",", "")), ((edtContagemResultadoNaoCnf_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNaoCnf_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoNaoCnf_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoNaoCnf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonaocnf_oscod_Internalname, "id", "", "", lblTextblockcontagemresultadonaocnf_oscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoNaoCnf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNaoCnf_OSCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2020ContagemResultadoNaoCnf_OSCod), 6, 0, ",", "")), ((edtContagemResultadoNaoCnf_OSCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2020ContagemResultadoNaoCnf_OSCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A2020ContagemResultadoNaoCnf_OSCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNaoCnf_OSCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoNaoCnf_OSCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoNaoCnf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonaocnf_naocnfcod_Internalname, "Conformidade", "", "", lblTextblockcontagemresultadonaocnf_naocnfcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoNaoCnf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContagemResultadoNaoCnf_NaoCnfCod, dynContagemResultadoNaoCnf_NaoCnfCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0)), 1, dynContagemResultadoNaoCnf_NaoCnfCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContagemResultadoNaoCnf_NaoCnfCod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_ContagemResultadoNaoCnf.htm");
            dynContagemResultadoNaoCnf_NaoCnfCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultadoNaoCnf_NaoCnfCod_Internalname, "Values", (String)(dynContagemResultadoNaoCnf_NaoCnfCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonaocnf_glosavel_Internalname, "Nao Cnf_Glosavel", "", "", lblTextblockcontagemresultadonaocnf_glosavel_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoNaoCnf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Check box */
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContagemResultadoNaoCnf_Glosavel_Internalname, StringUtil.BoolToStr( A2031ContagemResultadoNaoCnf_Glosavel), "", "", 1, chkContagemResultadoNaoCnf_Glosavel.Enabled, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonaocnf_logrspcod_Internalname, "Respons�vel Id", "", "", lblTextblockcontagemresultadonaocnf_logrspcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoNaoCnf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNaoCnf_LogRspCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2023ContagemResultadoNaoCnf_LogRspCod), 10, 0, ",", "")), ((edtContagemResultadoNaoCnf_LogRspCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2023ContagemResultadoNaoCnf_LogRspCod), "ZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A2023ContagemResultadoNaoCnf_LogRspCod), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNaoCnf_LogRspCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoNaoCnf_LogRspCod_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "Codigo10", "right", false, "HLP_ContagemResultadoNaoCnf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonaocnf_osentregue_Internalname, "entregue", "", "", lblTextblockcontagemresultadonaocnf_osentregue_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoNaoCnf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultadoNaoCnf_OSEntregue, cmbContagemResultadoNaoCnf_OSEntregue_Internalname, StringUtil.BoolToStr( A2053ContagemResultadoNaoCnf_OSEntregue), 1, cmbContagemResultadoNaoCnf_OSEntregue_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContagemResultadoNaoCnf_OSEntregue.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "", true, "HLP_ContagemResultadoNaoCnf.htm");
            cmbContagemResultadoNaoCnf_OSEntregue.CurrentValue = StringUtil.BoolToStr( A2053ContagemResultadoNaoCnf_OSEntregue);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNaoCnf_OSEntregue_Internalname, "Values", (String)(cmbContagemResultadoNaoCnf_OSEntregue.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonaocnf_contestada_Internalname, "Contestada", "", "", lblTextblockcontagemresultadonaocnf_contestada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoNaoCnf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContagemResultadoNaoCnf_Contestada_Internalname, StringUtil.BoolToStr( A2057ContagemResultadoNaoCnf_Contestada), "", "", 1, chkContagemResultadoNaoCnf_Contestada.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(69, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonaocnf_indcod_Internalname, "Cnf_Ind Cod", "", "", lblTextblockcontagemresultadonaocnf_indcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoNaoCnf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNaoCnf_IndCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2060ContagemResultadoNaoCnf_IndCod), 6, 0, ",", "")), ((edtContagemResultadoNaoCnf_IndCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2060ContagemResultadoNaoCnf_IndCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A2060ContagemResultadoNaoCnf_IndCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNaoCnf_IndCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoNaoCnf_IndCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoNaoCnf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_52224e( true) ;
         }
         else
         {
            wb_table4_34_52224e( false) ;
         }
      }

      protected void wb_table2_5_52224( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNaoCnf.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_52224e( true) ;
         }
         else
         {
            wb_table2_5_52224e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNaoCnf_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNaoCnf_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADONAOCNF_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoNaoCnf_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2024ContagemResultadoNaoCnf_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2024ContagemResultadoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), 6, 0)));
               }
               else
               {
                  A2024ContagemResultadoNaoCnf_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNaoCnf_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2024ContagemResultadoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNaoCnf_OSCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNaoCnf_OSCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADONAOCNF_OSCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoNaoCnf_OSCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2020ContagemResultadoNaoCnf_OSCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2020ContagemResultadoNaoCnf_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2020ContagemResultadoNaoCnf_OSCod), 6, 0)));
               }
               else
               {
                  A2020ContagemResultadoNaoCnf_OSCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNaoCnf_OSCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2020ContagemResultadoNaoCnf_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2020ContagemResultadoNaoCnf_OSCod), 6, 0)));
               }
               dynContagemResultadoNaoCnf_NaoCnfCod.CurrentValue = cgiGet( dynContagemResultadoNaoCnf_NaoCnfCod_Internalname);
               A2026ContagemResultadoNaoCnf_NaoCnfCod = (int)(NumberUtil.Val( cgiGet( dynContagemResultadoNaoCnf_NaoCnfCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2026ContagemResultadoNaoCnf_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0)));
               A2031ContagemResultadoNaoCnf_Glosavel = StringUtil.StrToBool( cgiGet( chkContagemResultadoNaoCnf_Glosavel_Internalname));
               n2031ContagemResultadoNaoCnf_Glosavel = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2031ContagemResultadoNaoCnf_Glosavel", A2031ContagemResultadoNaoCnf_Glosavel);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNaoCnf_LogRspCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNaoCnf_LogRspCod_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADONAOCNF_LOGRSPCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoNaoCnf_LogRspCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2023ContagemResultadoNaoCnf_LogRspCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2023ContagemResultadoNaoCnf_LogRspCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2023ContagemResultadoNaoCnf_LogRspCod), 10, 0)));
               }
               else
               {
                  A2023ContagemResultadoNaoCnf_LogRspCod = (long)(context.localUtil.CToN( cgiGet( edtContagemResultadoNaoCnf_LogRspCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2023ContagemResultadoNaoCnf_LogRspCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2023ContagemResultadoNaoCnf_LogRspCod), 10, 0)));
               }
               cmbContagemResultadoNaoCnf_OSEntregue.CurrentValue = cgiGet( cmbContagemResultadoNaoCnf_OSEntregue_Internalname);
               A2053ContagemResultadoNaoCnf_OSEntregue = StringUtil.StrToBool( cgiGet( cmbContagemResultadoNaoCnf_OSEntregue_Internalname));
               n2053ContagemResultadoNaoCnf_OSEntregue = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2053ContagemResultadoNaoCnf_OSEntregue", A2053ContagemResultadoNaoCnf_OSEntregue);
               n2053ContagemResultadoNaoCnf_OSEntregue = ((false==A2053ContagemResultadoNaoCnf_OSEntregue) ? true : false);
               A2057ContagemResultadoNaoCnf_Contestada = StringUtil.StrToBool( cgiGet( chkContagemResultadoNaoCnf_Contestada_Internalname));
               n2057ContagemResultadoNaoCnf_Contestada = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2057ContagemResultadoNaoCnf_Contestada", A2057ContagemResultadoNaoCnf_Contestada);
               n2057ContagemResultadoNaoCnf_Contestada = ((false==A2057ContagemResultadoNaoCnf_Contestada) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNaoCnf_IndCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoNaoCnf_IndCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADONAOCNF_INDCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoNaoCnf_IndCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2060ContagemResultadoNaoCnf_IndCod = 0;
                  n2060ContagemResultadoNaoCnf_IndCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2060ContagemResultadoNaoCnf_IndCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2060ContagemResultadoNaoCnf_IndCod), 6, 0)));
               }
               else
               {
                  A2060ContagemResultadoNaoCnf_IndCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNaoCnf_IndCod_Internalname), ",", "."));
                  n2060ContagemResultadoNaoCnf_IndCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2060ContagemResultadoNaoCnf_IndCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2060ContagemResultadoNaoCnf_IndCod), 6, 0)));
               }
               n2060ContagemResultadoNaoCnf_IndCod = ((0==A2060ContagemResultadoNaoCnf_IndCod) ? true : false);
               /* Read saved values. */
               Z2024ContagemResultadoNaoCnf_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z2024ContagemResultadoNaoCnf_Codigo"), ",", "."));
               Z2053ContagemResultadoNaoCnf_OSEntregue = StringUtil.StrToBool( cgiGet( "Z2053ContagemResultadoNaoCnf_OSEntregue"));
               n2053ContagemResultadoNaoCnf_OSEntregue = ((false==A2053ContagemResultadoNaoCnf_OSEntregue) ? true : false);
               Z2057ContagemResultadoNaoCnf_Contestada = StringUtil.StrToBool( cgiGet( "Z2057ContagemResultadoNaoCnf_Contestada"));
               n2057ContagemResultadoNaoCnf_Contestada = ((false==A2057ContagemResultadoNaoCnf_Contestada) ? true : false);
               Z2020ContagemResultadoNaoCnf_OSCod = (int)(context.localUtil.CToN( cgiGet( "Z2020ContagemResultadoNaoCnf_OSCod"), ",", "."));
               Z2023ContagemResultadoNaoCnf_LogRspCod = (long)(context.localUtil.CToN( cgiGet( "Z2023ContagemResultadoNaoCnf_LogRspCod"), ",", "."));
               Z2026ContagemResultadoNaoCnf_NaoCnfCod = (int)(context.localUtil.CToN( cgiGet( "Z2026ContagemResultadoNaoCnf_NaoCnfCod"), ",", "."));
               Z2060ContagemResultadoNaoCnf_IndCod = (int)(context.localUtil.CToN( cgiGet( "Z2060ContagemResultadoNaoCnf_IndCod"), ",", "."));
               n2060ContagemResultadoNaoCnf_IndCod = ((0==A2060ContagemResultadoNaoCnf_IndCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A2024ContagemResultadoNaoCnf_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2024ContagemResultadoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll52224( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes52224( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption520( )
      {
      }

      protected void ZM52224( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2053ContagemResultadoNaoCnf_OSEntregue = T00523_A2053ContagemResultadoNaoCnf_OSEntregue[0];
               Z2057ContagemResultadoNaoCnf_Contestada = T00523_A2057ContagemResultadoNaoCnf_Contestada[0];
               Z2020ContagemResultadoNaoCnf_OSCod = T00523_A2020ContagemResultadoNaoCnf_OSCod[0];
               Z2023ContagemResultadoNaoCnf_LogRspCod = T00523_A2023ContagemResultadoNaoCnf_LogRspCod[0];
               Z2026ContagemResultadoNaoCnf_NaoCnfCod = T00523_A2026ContagemResultadoNaoCnf_NaoCnfCod[0];
               Z2060ContagemResultadoNaoCnf_IndCod = T00523_A2060ContagemResultadoNaoCnf_IndCod[0];
            }
            else
            {
               Z2053ContagemResultadoNaoCnf_OSEntregue = A2053ContagemResultadoNaoCnf_OSEntregue;
               Z2057ContagemResultadoNaoCnf_Contestada = A2057ContagemResultadoNaoCnf_Contestada;
               Z2020ContagemResultadoNaoCnf_OSCod = A2020ContagemResultadoNaoCnf_OSCod;
               Z2023ContagemResultadoNaoCnf_LogRspCod = A2023ContagemResultadoNaoCnf_LogRspCod;
               Z2026ContagemResultadoNaoCnf_NaoCnfCod = A2026ContagemResultadoNaoCnf_NaoCnfCod;
               Z2060ContagemResultadoNaoCnf_IndCod = A2060ContagemResultadoNaoCnf_IndCod;
            }
         }
         if ( GX_JID == -3 )
         {
            Z2024ContagemResultadoNaoCnf_Codigo = A2024ContagemResultadoNaoCnf_Codigo;
            Z2053ContagemResultadoNaoCnf_OSEntregue = A2053ContagemResultadoNaoCnf_OSEntregue;
            Z2057ContagemResultadoNaoCnf_Contestada = A2057ContagemResultadoNaoCnf_Contestada;
            Z2020ContagemResultadoNaoCnf_OSCod = A2020ContagemResultadoNaoCnf_OSCod;
            Z2023ContagemResultadoNaoCnf_LogRspCod = A2023ContagemResultadoNaoCnf_LogRspCod;
            Z2026ContagemResultadoNaoCnf_NaoCnfCod = A2026ContagemResultadoNaoCnf_NaoCnfCod;
            Z2060ContagemResultadoNaoCnf_IndCod = A2060ContagemResultadoNaoCnf_IndCod;
            Z2031ContagemResultadoNaoCnf_Glosavel = A2031ContagemResultadoNaoCnf_Glosavel;
         }
      }

      protected void standaloneNotModal( )
      {
         GXACONTAGEMRESULTADONAOCNF_NAOCNFCOD_html52224( ) ;
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A2053ContagemResultadoNaoCnf_OSEntregue) && ( Gx_BScreen == 0 ) )
         {
            A2053ContagemResultadoNaoCnf_OSEntregue = false;
            n2053ContagemResultadoNaoCnf_OSEntregue = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2053ContagemResultadoNaoCnf_OSEntregue", A2053ContagemResultadoNaoCnf_OSEntregue);
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00526 */
            pr_default.execute(4, new Object[] {A2026ContagemResultadoNaoCnf_NaoCnfCod});
            A2031ContagemResultadoNaoCnf_Glosavel = T00526_A2031ContagemResultadoNaoCnf_Glosavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2031ContagemResultadoNaoCnf_Glosavel", A2031ContagemResultadoNaoCnf_Glosavel);
            n2031ContagemResultadoNaoCnf_Glosavel = T00526_n2031ContagemResultadoNaoCnf_Glosavel[0];
            pr_default.close(4);
         }
      }

      protected void Load52224( )
      {
         /* Using cursor T00528 */
         pr_default.execute(6, new Object[] {A2024ContagemResultadoNaoCnf_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound224 = 1;
            A2031ContagemResultadoNaoCnf_Glosavel = T00528_A2031ContagemResultadoNaoCnf_Glosavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2031ContagemResultadoNaoCnf_Glosavel", A2031ContagemResultadoNaoCnf_Glosavel);
            n2031ContagemResultadoNaoCnf_Glosavel = T00528_n2031ContagemResultadoNaoCnf_Glosavel[0];
            A2053ContagemResultadoNaoCnf_OSEntregue = T00528_A2053ContagemResultadoNaoCnf_OSEntregue[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2053ContagemResultadoNaoCnf_OSEntregue", A2053ContagemResultadoNaoCnf_OSEntregue);
            n2053ContagemResultadoNaoCnf_OSEntregue = T00528_n2053ContagemResultadoNaoCnf_OSEntregue[0];
            A2057ContagemResultadoNaoCnf_Contestada = T00528_A2057ContagemResultadoNaoCnf_Contestada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2057ContagemResultadoNaoCnf_Contestada", A2057ContagemResultadoNaoCnf_Contestada);
            n2057ContagemResultadoNaoCnf_Contestada = T00528_n2057ContagemResultadoNaoCnf_Contestada[0];
            A2020ContagemResultadoNaoCnf_OSCod = T00528_A2020ContagemResultadoNaoCnf_OSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2020ContagemResultadoNaoCnf_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2020ContagemResultadoNaoCnf_OSCod), 6, 0)));
            A2023ContagemResultadoNaoCnf_LogRspCod = T00528_A2023ContagemResultadoNaoCnf_LogRspCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2023ContagemResultadoNaoCnf_LogRspCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2023ContagemResultadoNaoCnf_LogRspCod), 10, 0)));
            A2026ContagemResultadoNaoCnf_NaoCnfCod = T00528_A2026ContagemResultadoNaoCnf_NaoCnfCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2026ContagemResultadoNaoCnf_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0)));
            A2060ContagemResultadoNaoCnf_IndCod = T00528_A2060ContagemResultadoNaoCnf_IndCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2060ContagemResultadoNaoCnf_IndCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2060ContagemResultadoNaoCnf_IndCod), 6, 0)));
            n2060ContagemResultadoNaoCnf_IndCod = T00528_n2060ContagemResultadoNaoCnf_IndCod[0];
            ZM52224( -3) ;
         }
         pr_default.close(6);
         OnLoadActions52224( ) ;
      }

      protected void OnLoadActions52224( )
      {
      }

      protected void CheckExtendedTable52224( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T00524 */
         pr_default.execute(2, new Object[] {A2020ContagemResultadoNaoCnf_OSCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem ResultadoNaoCnf_OSCod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNaoCnf_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T00526 */
         pr_default.execute(4, new Object[] {A2026ContagemResultadoNaoCnf_NaoCnfCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Resultado Nao Cnf_Nao Cnf Cod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_NAOCNFCOD");
            AnyError = 1;
            GX_FocusControl = dynContagemResultadoNaoCnf_NaoCnfCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2031ContagemResultadoNaoCnf_Glosavel = T00526_A2031ContagemResultadoNaoCnf_Glosavel[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2031ContagemResultadoNaoCnf_Glosavel", A2031ContagemResultadoNaoCnf_Glosavel);
         n2031ContagemResultadoNaoCnf_Glosavel = T00526_n2031ContagemResultadoNaoCnf_Glosavel[0];
         pr_default.close(4);
         /* Using cursor T00525 */
         pr_default.execute(3, new Object[] {A2023ContagemResultadoNaoCnf_LogRspCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Nao Cnf_Log Rsp Cod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_LOGRSPCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNaoCnf_LogRspCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
         /* Using cursor T00527 */
         pr_default.execute(5, new Object[] {n2060ContagemResultadoNaoCnf_IndCod, A2060ContagemResultadoNaoCnf_IndCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A2060ContagemResultadoNaoCnf_IndCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Nao Cnf_Indicador'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_INDCOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultadoNaoCnf_IndCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(5);
      }

      protected void CloseExtendedTableCursors52224( )
      {
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(3);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_4( int A2020ContagemResultadoNaoCnf_OSCod )
      {
         /* Using cursor T00529 */
         pr_default.execute(7, new Object[] {A2020ContagemResultadoNaoCnf_OSCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem ResultadoNaoCnf_OSCod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNaoCnf_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_6( int A2026ContagemResultadoNaoCnf_NaoCnfCod )
      {
         /* Using cursor T005210 */
         pr_default.execute(8, new Object[] {A2026ContagemResultadoNaoCnf_NaoCnfCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Resultado Nao Cnf_Nao Cnf Cod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_NAOCNFCOD");
            AnyError = 1;
            GX_FocusControl = dynContagemResultadoNaoCnf_NaoCnfCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2031ContagemResultadoNaoCnf_Glosavel = T005210_A2031ContagemResultadoNaoCnf_Glosavel[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2031ContagemResultadoNaoCnf_Glosavel", A2031ContagemResultadoNaoCnf_Glosavel);
         n2031ContagemResultadoNaoCnf_Glosavel = T005210_n2031ContagemResultadoNaoCnf_Glosavel[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A2031ContagemResultadoNaoCnf_Glosavel))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_5( long A2023ContagemResultadoNaoCnf_LogRspCod )
      {
         /* Using cursor T005211 */
         pr_default.execute(9, new Object[] {A2023ContagemResultadoNaoCnf_LogRspCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Nao Cnf_Log Rsp Cod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_LOGRSPCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNaoCnf_LogRspCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_7( int A2060ContagemResultadoNaoCnf_IndCod )
      {
         /* Using cursor T005212 */
         pr_default.execute(10, new Object[] {n2060ContagemResultadoNaoCnf_IndCod, A2060ContagemResultadoNaoCnf_IndCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            if ( ! ( (0==A2060ContagemResultadoNaoCnf_IndCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Nao Cnf_Indicador'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_INDCOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultadoNaoCnf_IndCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void GetKey52224( )
      {
         /* Using cursor T005213 */
         pr_default.execute(11, new Object[] {A2024ContagemResultadoNaoCnf_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound224 = 1;
         }
         else
         {
            RcdFound224 = 0;
         }
         pr_default.close(11);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00523 */
         pr_default.execute(1, new Object[] {A2024ContagemResultadoNaoCnf_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM52224( 3) ;
            RcdFound224 = 1;
            A2024ContagemResultadoNaoCnf_Codigo = T00523_A2024ContagemResultadoNaoCnf_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2024ContagemResultadoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), 6, 0)));
            A2053ContagemResultadoNaoCnf_OSEntregue = T00523_A2053ContagemResultadoNaoCnf_OSEntregue[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2053ContagemResultadoNaoCnf_OSEntregue", A2053ContagemResultadoNaoCnf_OSEntregue);
            n2053ContagemResultadoNaoCnf_OSEntregue = T00523_n2053ContagemResultadoNaoCnf_OSEntregue[0];
            A2057ContagemResultadoNaoCnf_Contestada = T00523_A2057ContagemResultadoNaoCnf_Contestada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2057ContagemResultadoNaoCnf_Contestada", A2057ContagemResultadoNaoCnf_Contestada);
            n2057ContagemResultadoNaoCnf_Contestada = T00523_n2057ContagemResultadoNaoCnf_Contestada[0];
            A2020ContagemResultadoNaoCnf_OSCod = T00523_A2020ContagemResultadoNaoCnf_OSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2020ContagemResultadoNaoCnf_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2020ContagemResultadoNaoCnf_OSCod), 6, 0)));
            A2023ContagemResultadoNaoCnf_LogRspCod = T00523_A2023ContagemResultadoNaoCnf_LogRspCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2023ContagemResultadoNaoCnf_LogRspCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2023ContagemResultadoNaoCnf_LogRspCod), 10, 0)));
            A2026ContagemResultadoNaoCnf_NaoCnfCod = T00523_A2026ContagemResultadoNaoCnf_NaoCnfCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2026ContagemResultadoNaoCnf_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0)));
            A2060ContagemResultadoNaoCnf_IndCod = T00523_A2060ContagemResultadoNaoCnf_IndCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2060ContagemResultadoNaoCnf_IndCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2060ContagemResultadoNaoCnf_IndCod), 6, 0)));
            n2060ContagemResultadoNaoCnf_IndCod = T00523_n2060ContagemResultadoNaoCnf_IndCod[0];
            Z2024ContagemResultadoNaoCnf_Codigo = A2024ContagemResultadoNaoCnf_Codigo;
            sMode224 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load52224( ) ;
            if ( AnyError == 1 )
            {
               RcdFound224 = 0;
               InitializeNonKey52224( ) ;
            }
            Gx_mode = sMode224;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound224 = 0;
            InitializeNonKey52224( ) ;
            sMode224 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode224;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey52224( ) ;
         if ( RcdFound224 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound224 = 0;
         /* Using cursor T005214 */
         pr_default.execute(12, new Object[] {A2024ContagemResultadoNaoCnf_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T005214_A2024ContagemResultadoNaoCnf_Codigo[0] < A2024ContagemResultadoNaoCnf_Codigo ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T005214_A2024ContagemResultadoNaoCnf_Codigo[0] > A2024ContagemResultadoNaoCnf_Codigo ) ) )
            {
               A2024ContagemResultadoNaoCnf_Codigo = T005214_A2024ContagemResultadoNaoCnf_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2024ContagemResultadoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), 6, 0)));
               RcdFound224 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void move_previous( )
      {
         RcdFound224 = 0;
         /* Using cursor T005215 */
         pr_default.execute(13, new Object[] {A2024ContagemResultadoNaoCnf_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            while ( (pr_default.getStatus(13) != 101) && ( ( T005215_A2024ContagemResultadoNaoCnf_Codigo[0] > A2024ContagemResultadoNaoCnf_Codigo ) ) )
            {
               pr_default.readNext(13);
            }
            if ( (pr_default.getStatus(13) != 101) && ( ( T005215_A2024ContagemResultadoNaoCnf_Codigo[0] < A2024ContagemResultadoNaoCnf_Codigo ) ) )
            {
               A2024ContagemResultadoNaoCnf_Codigo = T005215_A2024ContagemResultadoNaoCnf_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2024ContagemResultadoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), 6, 0)));
               RcdFound224 = 1;
            }
         }
         pr_default.close(13);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey52224( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemResultadoNaoCnf_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert52224( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound224 == 1 )
            {
               if ( A2024ContagemResultadoNaoCnf_Codigo != Z2024ContagemResultadoNaoCnf_Codigo )
               {
                  A2024ContagemResultadoNaoCnf_Codigo = Z2024ContagemResultadoNaoCnf_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2024ContagemResultadoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoNaoCnf_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultadoNaoCnf_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update52224( ) ;
                  GX_FocusControl = edtContagemResultadoNaoCnf_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A2024ContagemResultadoNaoCnf_Codigo != Z2024ContagemResultadoNaoCnf_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContagemResultadoNaoCnf_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert52224( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADONAOCNF_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoNaoCnf_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContagemResultadoNaoCnf_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert52224( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A2024ContagemResultadoNaoCnf_Codigo != Z2024ContagemResultadoNaoCnf_Codigo )
         {
            A2024ContagemResultadoNaoCnf_Codigo = Z2024ContagemResultadoNaoCnf_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2024ContagemResultadoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADONAOCNF_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNaoCnf_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultadoNaoCnf_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound224 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNaoCnf_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtContagemResultadoNaoCnf_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart52224( ) ;
         if ( RcdFound224 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoNaoCnf_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd52224( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound224 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoNaoCnf_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound224 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoNaoCnf_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart52224( ) ;
         if ( RcdFound224 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound224 != 0 )
            {
               ScanNext52224( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoNaoCnf_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd52224( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency52224( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00522 */
            pr_default.execute(0, new Object[] {A2024ContagemResultadoNaoCnf_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNaoCnf"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z2053ContagemResultadoNaoCnf_OSEntregue != T00522_A2053ContagemResultadoNaoCnf_OSEntregue[0] ) || ( Z2057ContagemResultadoNaoCnf_Contestada != T00522_A2057ContagemResultadoNaoCnf_Contestada[0] ) || ( Z2020ContagemResultadoNaoCnf_OSCod != T00522_A2020ContagemResultadoNaoCnf_OSCod[0] ) || ( Z2023ContagemResultadoNaoCnf_LogRspCod != T00522_A2023ContagemResultadoNaoCnf_LogRspCod[0] ) || ( Z2026ContagemResultadoNaoCnf_NaoCnfCod != T00522_A2026ContagemResultadoNaoCnf_NaoCnfCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2060ContagemResultadoNaoCnf_IndCod != T00522_A2060ContagemResultadoNaoCnf_IndCod[0] ) )
            {
               if ( Z2053ContagemResultadoNaoCnf_OSEntregue != T00522_A2053ContagemResultadoNaoCnf_OSEntregue[0] )
               {
                  GXUtil.WriteLog("contagemresultadonaocnf:[seudo value changed for attri]"+"ContagemResultadoNaoCnf_OSEntregue");
                  GXUtil.WriteLogRaw("Old: ",Z2053ContagemResultadoNaoCnf_OSEntregue);
                  GXUtil.WriteLogRaw("Current: ",T00522_A2053ContagemResultadoNaoCnf_OSEntregue[0]);
               }
               if ( Z2057ContagemResultadoNaoCnf_Contestada != T00522_A2057ContagemResultadoNaoCnf_Contestada[0] )
               {
                  GXUtil.WriteLog("contagemresultadonaocnf:[seudo value changed for attri]"+"ContagemResultadoNaoCnf_Contestada");
                  GXUtil.WriteLogRaw("Old: ",Z2057ContagemResultadoNaoCnf_Contestada);
                  GXUtil.WriteLogRaw("Current: ",T00522_A2057ContagemResultadoNaoCnf_Contestada[0]);
               }
               if ( Z2020ContagemResultadoNaoCnf_OSCod != T00522_A2020ContagemResultadoNaoCnf_OSCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadonaocnf:[seudo value changed for attri]"+"ContagemResultadoNaoCnf_OSCod");
                  GXUtil.WriteLogRaw("Old: ",Z2020ContagemResultadoNaoCnf_OSCod);
                  GXUtil.WriteLogRaw("Current: ",T00522_A2020ContagemResultadoNaoCnf_OSCod[0]);
               }
               if ( Z2023ContagemResultadoNaoCnf_LogRspCod != T00522_A2023ContagemResultadoNaoCnf_LogRspCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadonaocnf:[seudo value changed for attri]"+"ContagemResultadoNaoCnf_LogRspCod");
                  GXUtil.WriteLogRaw("Old: ",Z2023ContagemResultadoNaoCnf_LogRspCod);
                  GXUtil.WriteLogRaw("Current: ",T00522_A2023ContagemResultadoNaoCnf_LogRspCod[0]);
               }
               if ( Z2026ContagemResultadoNaoCnf_NaoCnfCod != T00522_A2026ContagemResultadoNaoCnf_NaoCnfCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadonaocnf:[seudo value changed for attri]"+"ContagemResultadoNaoCnf_NaoCnfCod");
                  GXUtil.WriteLogRaw("Old: ",Z2026ContagemResultadoNaoCnf_NaoCnfCod);
                  GXUtil.WriteLogRaw("Current: ",T00522_A2026ContagemResultadoNaoCnf_NaoCnfCod[0]);
               }
               if ( Z2060ContagemResultadoNaoCnf_IndCod != T00522_A2060ContagemResultadoNaoCnf_IndCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadonaocnf:[seudo value changed for attri]"+"ContagemResultadoNaoCnf_IndCod");
                  GXUtil.WriteLogRaw("Old: ",Z2060ContagemResultadoNaoCnf_IndCod);
                  GXUtil.WriteLogRaw("Current: ",T00522_A2060ContagemResultadoNaoCnf_IndCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoNaoCnf"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert52224( )
      {
         BeforeValidate52224( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable52224( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM52224( 0) ;
            CheckOptimisticConcurrency52224( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm52224( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert52224( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005216 */
                     pr_default.execute(14, new Object[] {n2053ContagemResultadoNaoCnf_OSEntregue, A2053ContagemResultadoNaoCnf_OSEntregue, n2057ContagemResultadoNaoCnf_Contestada, A2057ContagemResultadoNaoCnf_Contestada, A2020ContagemResultadoNaoCnf_OSCod, A2023ContagemResultadoNaoCnf_LogRspCod, A2026ContagemResultadoNaoCnf_NaoCnfCod, n2060ContagemResultadoNaoCnf_IndCod, A2060ContagemResultadoNaoCnf_IndCod});
                     A2024ContagemResultadoNaoCnf_Codigo = T005216_A2024ContagemResultadoNaoCnf_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2024ContagemResultadoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), 6, 0)));
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNaoCnf") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption520( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load52224( ) ;
            }
            EndLevel52224( ) ;
         }
         CloseExtendedTableCursors52224( ) ;
      }

      protected void Update52224( )
      {
         BeforeValidate52224( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable52224( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency52224( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm52224( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate52224( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005217 */
                     pr_default.execute(15, new Object[] {n2053ContagemResultadoNaoCnf_OSEntregue, A2053ContagemResultadoNaoCnf_OSEntregue, n2057ContagemResultadoNaoCnf_Contestada, A2057ContagemResultadoNaoCnf_Contestada, A2020ContagemResultadoNaoCnf_OSCod, A2023ContagemResultadoNaoCnf_LogRspCod, A2026ContagemResultadoNaoCnf_NaoCnfCod, n2060ContagemResultadoNaoCnf_IndCod, A2060ContagemResultadoNaoCnf_IndCod, A2024ContagemResultadoNaoCnf_Codigo});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNaoCnf") ;
                     if ( (pr_default.getStatus(15) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNaoCnf"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate52224( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption520( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel52224( ) ;
         }
         CloseExtendedTableCursors52224( ) ;
      }

      protected void DeferredUpdate52224( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate52224( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency52224( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls52224( ) ;
            AfterConfirm52224( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete52224( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T005218 */
                  pr_default.execute(16, new Object[] {A2024ContagemResultadoNaoCnf_Codigo});
                  pr_default.close(16);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNaoCnf") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound224 == 0 )
                        {
                           InitAll52224( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption520( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode224 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel52224( ) ;
         Gx_mode = sMode224;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls52224( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T005219 */
            pr_default.execute(17, new Object[] {A2026ContagemResultadoNaoCnf_NaoCnfCod});
            A2031ContagemResultadoNaoCnf_Glosavel = T005219_A2031ContagemResultadoNaoCnf_Glosavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2031ContagemResultadoNaoCnf_Glosavel", A2031ContagemResultadoNaoCnf_Glosavel);
            n2031ContagemResultadoNaoCnf_Glosavel = T005219_n2031ContagemResultadoNaoCnf_Glosavel[0];
            pr_default.close(17);
         }
      }

      protected void EndLevel52224( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete52224( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(17);
            context.CommitDataStores( "ContagemResultadoNaoCnf");
            if ( AnyError == 0 )
            {
               ConfirmValues520( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(17);
            context.RollbackDataStores( "ContagemResultadoNaoCnf");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart52224( )
      {
         /* Using cursor T005220 */
         pr_default.execute(18);
         RcdFound224 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound224 = 1;
            A2024ContagemResultadoNaoCnf_Codigo = T005220_A2024ContagemResultadoNaoCnf_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2024ContagemResultadoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext52224( )
      {
         /* Scan next routine */
         pr_default.readNext(18);
         RcdFound224 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound224 = 1;
            A2024ContagemResultadoNaoCnf_Codigo = T005220_A2024ContagemResultadoNaoCnf_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2024ContagemResultadoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd52224( )
      {
         pr_default.close(18);
      }

      protected void AfterConfirm52224( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert52224( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate52224( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete52224( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete52224( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate52224( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes52224( )
      {
         edtContagemResultadoNaoCnf_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNaoCnf_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNaoCnf_Codigo_Enabled), 5, 0)));
         edtContagemResultadoNaoCnf_OSCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNaoCnf_OSCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNaoCnf_OSCod_Enabled), 5, 0)));
         dynContagemResultadoNaoCnf_NaoCnfCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultadoNaoCnf_NaoCnfCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultadoNaoCnf_NaoCnfCod.Enabled), 5, 0)));
         chkContagemResultadoNaoCnf_Glosavel.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContagemResultadoNaoCnf_Glosavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContagemResultadoNaoCnf_Glosavel.Enabled), 5, 0)));
         edtContagemResultadoNaoCnf_LogRspCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNaoCnf_LogRspCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNaoCnf_LogRspCod_Enabled), 5, 0)));
         cmbContagemResultadoNaoCnf_OSEntregue.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoNaoCnf_OSEntregue_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemResultadoNaoCnf_OSEntregue.Enabled), 5, 0)));
         chkContagemResultadoNaoCnf_Contestada.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContagemResultadoNaoCnf_Contestada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContagemResultadoNaoCnf_Contestada.Enabled), 5, 0)));
         edtContagemResultadoNaoCnf_IndCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoNaoCnf_IndCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNaoCnf_IndCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues520( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221122783");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadonaocnf.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z2024ContagemResultadoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2024ContagemResultadoNaoCnf_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z2053ContagemResultadoNaoCnf_OSEntregue", Z2053ContagemResultadoNaoCnf_OSEntregue);
         GxWebStd.gx_boolean_hidden_field( context, "Z2057ContagemResultadoNaoCnf_Contestada", Z2057ContagemResultadoNaoCnf_Contestada);
         GxWebStd.gx_hidden_field( context, "Z2020ContagemResultadoNaoCnf_OSCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2020ContagemResultadoNaoCnf_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2023ContagemResultadoNaoCnf_LogRspCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2023ContagemResultadoNaoCnf_LogRspCod), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2026ContagemResultadoNaoCnf_NaoCnfCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2060ContagemResultadoNaoCnf_IndCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2060ContagemResultadoNaoCnf_IndCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadonaocnf.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoNaoCnf" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Nao Cnf" ;
      }

      protected void InitializeNonKey52224( )
      {
         A2020ContagemResultadoNaoCnf_OSCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2020ContagemResultadoNaoCnf_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2020ContagemResultadoNaoCnf_OSCod), 6, 0)));
         A2026ContagemResultadoNaoCnf_NaoCnfCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2026ContagemResultadoNaoCnf_NaoCnfCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0)));
         A2031ContagemResultadoNaoCnf_Glosavel = false;
         n2031ContagemResultadoNaoCnf_Glosavel = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2031ContagemResultadoNaoCnf_Glosavel", A2031ContagemResultadoNaoCnf_Glosavel);
         A2023ContagemResultadoNaoCnf_LogRspCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2023ContagemResultadoNaoCnf_LogRspCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2023ContagemResultadoNaoCnf_LogRspCod), 10, 0)));
         A2057ContagemResultadoNaoCnf_Contestada = false;
         n2057ContagemResultadoNaoCnf_Contestada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2057ContagemResultadoNaoCnf_Contestada", A2057ContagemResultadoNaoCnf_Contestada);
         n2057ContagemResultadoNaoCnf_Contestada = ((false==A2057ContagemResultadoNaoCnf_Contestada) ? true : false);
         A2060ContagemResultadoNaoCnf_IndCod = 0;
         n2060ContagemResultadoNaoCnf_IndCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2060ContagemResultadoNaoCnf_IndCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2060ContagemResultadoNaoCnf_IndCod), 6, 0)));
         n2060ContagemResultadoNaoCnf_IndCod = ((0==A2060ContagemResultadoNaoCnf_IndCod) ? true : false);
         A2053ContagemResultadoNaoCnf_OSEntregue = false;
         n2053ContagemResultadoNaoCnf_OSEntregue = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2053ContagemResultadoNaoCnf_OSEntregue", A2053ContagemResultadoNaoCnf_OSEntregue);
         Z2053ContagemResultadoNaoCnf_OSEntregue = false;
         Z2057ContagemResultadoNaoCnf_Contestada = false;
         Z2020ContagemResultadoNaoCnf_OSCod = 0;
         Z2023ContagemResultadoNaoCnf_LogRspCod = 0;
         Z2026ContagemResultadoNaoCnf_NaoCnfCod = 0;
         Z2060ContagemResultadoNaoCnf_IndCod = 0;
      }

      protected void InitAll52224( )
      {
         A2024ContagemResultadoNaoCnf_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2024ContagemResultadoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2024ContagemResultadoNaoCnf_Codigo), 6, 0)));
         InitializeNonKey52224( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2053ContagemResultadoNaoCnf_OSEntregue = i2053ContagemResultadoNaoCnf_OSEntregue;
         n2053ContagemResultadoNaoCnf_OSEntregue = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2053ContagemResultadoNaoCnf_OSEntregue", A2053ContagemResultadoNaoCnf_OSEntregue);
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221122791");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadonaocnf.js", "?202031221122791");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontagemresultadonaocnf_codigo_Internalname = "TEXTBLOCKCONTAGEMRESULTADONAOCNF_CODIGO";
         edtContagemResultadoNaoCnf_Codigo_Internalname = "CONTAGEMRESULTADONAOCNF_CODIGO";
         lblTextblockcontagemresultadonaocnf_oscod_Internalname = "TEXTBLOCKCONTAGEMRESULTADONAOCNF_OSCOD";
         edtContagemResultadoNaoCnf_OSCod_Internalname = "CONTAGEMRESULTADONAOCNF_OSCOD";
         lblTextblockcontagemresultadonaocnf_naocnfcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADONAOCNF_NAOCNFCOD";
         dynContagemResultadoNaoCnf_NaoCnfCod_Internalname = "CONTAGEMRESULTADONAOCNF_NAOCNFCOD";
         lblTextblockcontagemresultadonaocnf_glosavel_Internalname = "TEXTBLOCKCONTAGEMRESULTADONAOCNF_GLOSAVEL";
         chkContagemResultadoNaoCnf_Glosavel_Internalname = "CONTAGEMRESULTADONAOCNF_GLOSAVEL";
         lblTextblockcontagemresultadonaocnf_logrspcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADONAOCNF_LOGRSPCOD";
         edtContagemResultadoNaoCnf_LogRspCod_Internalname = "CONTAGEMRESULTADONAOCNF_LOGRSPCOD";
         lblTextblockcontagemresultadonaocnf_osentregue_Internalname = "TEXTBLOCKCONTAGEMRESULTADONAOCNF_OSENTREGUE";
         cmbContagemResultadoNaoCnf_OSEntregue_Internalname = "CONTAGEMRESULTADONAOCNF_OSENTREGUE";
         lblTextblockcontagemresultadonaocnf_contestada_Internalname = "TEXTBLOCKCONTAGEMRESULTADONAOCNF_CONTESTADA";
         chkContagemResultadoNaoCnf_Contestada_Internalname = "CONTAGEMRESULTADONAOCNF_CONTESTADA";
         lblTextblockcontagemresultadonaocnf_indcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADONAOCNF_INDCOD";
         edtContagemResultadoNaoCnf_IndCod_Internalname = "CONTAGEMRESULTADONAOCNF_INDCOD";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Resultado Nao Cnf";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtContagemResultadoNaoCnf_IndCod_Jsonclick = "";
         edtContagemResultadoNaoCnf_IndCod_Enabled = 1;
         chkContagemResultadoNaoCnf_Contestada.Enabled = 1;
         cmbContagemResultadoNaoCnf_OSEntregue_Jsonclick = "";
         cmbContagemResultadoNaoCnf_OSEntregue.Enabled = 1;
         edtContagemResultadoNaoCnf_LogRspCod_Jsonclick = "";
         edtContagemResultadoNaoCnf_LogRspCod_Enabled = 1;
         chkContagemResultadoNaoCnf_Glosavel.Enabled = 0;
         dynContagemResultadoNaoCnf_NaoCnfCod_Jsonclick = "";
         dynContagemResultadoNaoCnf_NaoCnfCod.Enabled = 1;
         edtContagemResultadoNaoCnf_OSCod_Jsonclick = "";
         edtContagemResultadoNaoCnf_OSCod_Enabled = 1;
         edtContagemResultadoNaoCnf_Codigo_Jsonclick = "";
         edtContagemResultadoNaoCnf_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         chkContagemResultadoNaoCnf_Contestada.Caption = "";
         chkContagemResultadoNaoCnf_Glosavel.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACONTAGEMRESULTADONAOCNF_NAOCNFCOD52224( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEMRESULTADONAOCNF_NAOCNFCOD_data52224( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEMRESULTADONAOCNF_NAOCNFCOD_html52224( )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEMRESULTADONAOCNF_NAOCNFCOD_data52224( ) ;
         gxdynajaxindex = 1;
         dynContagemResultadoNaoCnf_NaoCnfCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagemResultadoNaoCnf_NaoCnfCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTAGEMRESULTADONAOCNF_NAOCNFCOD_data52224( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T005221 */
         pr_default.execute(19);
         while ( (pr_default.getStatus(19) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T005221_A2026ContagemResultadoNaoCnf_NaoCnfCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T005221_A427NaoConformidade_Nome[0]));
            pr_default.readNext(19);
         }
         pr_default.close(19);
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtContagemResultadoNaoCnf_OSCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemresultadonaocnf_codigo( int GX_Parm1 ,
                                                        GXCombobox cmbGX_Parm2 ,
                                                        bool GX_Parm3 ,
                                                        int GX_Parm4 ,
                                                        long GX_Parm5 ,
                                                        GXCombobox dynGX_Parm6 ,
                                                        int GX_Parm7 )
      {
         A2024ContagemResultadoNaoCnf_Codigo = GX_Parm1;
         cmbContagemResultadoNaoCnf_OSEntregue = cmbGX_Parm2;
         A2053ContagemResultadoNaoCnf_OSEntregue = StringUtil.StrToBool( cmbContagemResultadoNaoCnf_OSEntregue.CurrentValue);
         n2053ContagemResultadoNaoCnf_OSEntregue = false;
         cmbContagemResultadoNaoCnf_OSEntregue.CurrentValue = StringUtil.BoolToStr( A2053ContagemResultadoNaoCnf_OSEntregue);
         A2057ContagemResultadoNaoCnf_Contestada = GX_Parm3;
         n2057ContagemResultadoNaoCnf_Contestada = false;
         A2020ContagemResultadoNaoCnf_OSCod = GX_Parm4;
         A2023ContagemResultadoNaoCnf_LogRspCod = GX_Parm5;
         dynContagemResultadoNaoCnf_NaoCnfCod = dynGX_Parm6;
         A2026ContagemResultadoNaoCnf_NaoCnfCod = (int)(NumberUtil.Val( dynContagemResultadoNaoCnf_NaoCnfCod.CurrentValue, "."));
         A2060ContagemResultadoNaoCnf_IndCod = GX_Parm7;
         n2060ContagemResultadoNaoCnf_IndCod = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A2031ContagemResultadoNaoCnf_Glosavel = false;
            n2031ContagemResultadoNaoCnf_Glosavel = false;
         }
         GXACONTAGEMRESULTADONAOCNF_NAOCNFCOD_html52224( ) ;
         dynContagemResultadoNaoCnf_NaoCnfCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A2020ContagemResultadoNaoCnf_OSCod), 6, 0, ".", "")));
         if ( dynContagemResultadoNaoCnf_NaoCnfCod.ItemCount > 0 )
         {
            A2026ContagemResultadoNaoCnf_NaoCnfCod = (int)(NumberUtil.Val( dynContagemResultadoNaoCnf_NaoCnfCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0))), "."));
         }
         dynContagemResultadoNaoCnf_NaoCnfCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0));
         isValidOutput.Add(dynContagemResultadoNaoCnf_NaoCnfCod);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A2023ContagemResultadoNaoCnf_LogRspCod), 10, 0, ".", "")));
         cmbContagemResultadoNaoCnf_OSEntregue.CurrentValue = StringUtil.BoolToStr( A2053ContagemResultadoNaoCnf_OSEntregue);
         isValidOutput.Add(cmbContagemResultadoNaoCnf_OSEntregue);
         isValidOutput.Add(A2057ContagemResultadoNaoCnf_Contestada);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A2060ContagemResultadoNaoCnf_IndCod), 6, 0, ".", "")));
         isValidOutput.Add(A2031ContagemResultadoNaoCnf_Glosavel);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2024ContagemResultadoNaoCnf_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2020ContagemResultadoNaoCnf_OSCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2026ContagemResultadoNaoCnf_NaoCnfCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2023ContagemResultadoNaoCnf_LogRspCod), 10, 0, ",", "")));
         isValidOutput.Add(Z2053ContagemResultadoNaoCnf_OSEntregue);
         isValidOutput.Add(Z2057ContagemResultadoNaoCnf_Contestada);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2060ContagemResultadoNaoCnf_IndCod), 6, 0, ",", "")));
         isValidOutput.Add(Z2031ContagemResultadoNaoCnf_Glosavel);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadonaocnf_oscod( int GX_Parm1 )
      {
         A2020ContagemResultadoNaoCnf_OSCod = GX_Parm1;
         /* Using cursor T005222 */
         pr_default.execute(20, new Object[] {A2020ContagemResultadoNaoCnf_OSCod});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem ResultadoNaoCnf_OSCod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNaoCnf_OSCod_Internalname;
         }
         pr_default.close(20);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadonaocnf_naocnfcod( GXCombobox dynGX_Parm1 ,
                                                           bool GX_Parm2 )
      {
         dynContagemResultadoNaoCnf_NaoCnfCod = dynGX_Parm1;
         A2026ContagemResultadoNaoCnf_NaoCnfCod = (int)(NumberUtil.Val( dynContagemResultadoNaoCnf_NaoCnfCod.CurrentValue, "."));
         A2031ContagemResultadoNaoCnf_Glosavel = GX_Parm2;
         n2031ContagemResultadoNaoCnf_Glosavel = false;
         /* Using cursor T005223 */
         pr_default.execute(21, new Object[] {A2026ContagemResultadoNaoCnf_NaoCnfCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Resultado Nao Cnf_Nao Cnf Cod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_NAOCNFCOD");
            AnyError = 1;
            GX_FocusControl = dynContagemResultadoNaoCnf_NaoCnfCod_Internalname;
         }
         A2031ContagemResultadoNaoCnf_Glosavel = T005223_A2031ContagemResultadoNaoCnf_Glosavel[0];
         n2031ContagemResultadoNaoCnf_Glosavel = T005223_n2031ContagemResultadoNaoCnf_Glosavel[0];
         pr_default.close(21);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A2031ContagemResultadoNaoCnf_Glosavel = false;
            n2031ContagemResultadoNaoCnf_Glosavel = false;
         }
         isValidOutput.Add(A2031ContagemResultadoNaoCnf_Glosavel);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadonaocnf_logrspcod( long GX_Parm1 )
      {
         A2023ContagemResultadoNaoCnf_LogRspCod = GX_Parm1;
         /* Using cursor T005224 */
         pr_default.execute(22, new Object[] {A2023ContagemResultadoNaoCnf_LogRspCod});
         if ( (pr_default.getStatus(22) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Nao Cnf_Log Rsp Cod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_LOGRSPCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoNaoCnf_LogRspCod_Internalname;
         }
         pr_default.close(22);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadonaocnf_indcod( int GX_Parm1 )
      {
         A2060ContagemResultadoNaoCnf_IndCod = GX_Parm1;
         n2060ContagemResultadoNaoCnf_IndCod = false;
         /* Using cursor T005225 */
         pr_default.execute(23, new Object[] {n2060ContagemResultadoNaoCnf_IndCod, A2060ContagemResultadoNaoCnf_IndCod});
         if ( (pr_default.getStatus(23) == 101) )
         {
            if ( ! ( (0==A2060ContagemResultadoNaoCnf_IndCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Nao Cnf_Indicador'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONAOCNF_INDCOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultadoNaoCnf_IndCod_Internalname;
            }
         }
         pr_default.close(23);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(20);
         pr_default.close(22);
         pr_default.close(21);
         pr_default.close(17);
         pr_default.close(23);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontagemresultadonaocnf_codigo_Jsonclick = "";
         lblTextblockcontagemresultadonaocnf_oscod_Jsonclick = "";
         lblTextblockcontagemresultadonaocnf_naocnfcod_Jsonclick = "";
         lblTextblockcontagemresultadonaocnf_glosavel_Jsonclick = "";
         lblTextblockcontagemresultadonaocnf_logrspcod_Jsonclick = "";
         lblTextblockcontagemresultadonaocnf_osentregue_Jsonclick = "";
         lblTextblockcontagemresultadonaocnf_contestada_Jsonclick = "";
         lblTextblockcontagemresultadonaocnf_indcod_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T00526_A2031ContagemResultadoNaoCnf_Glosavel = new bool[] {false} ;
         T00526_n2031ContagemResultadoNaoCnf_Glosavel = new bool[] {false} ;
         T00528_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         T00528_A2031ContagemResultadoNaoCnf_Glosavel = new bool[] {false} ;
         T00528_n2031ContagemResultadoNaoCnf_Glosavel = new bool[] {false} ;
         T00528_A2053ContagemResultadoNaoCnf_OSEntregue = new bool[] {false} ;
         T00528_n2053ContagemResultadoNaoCnf_OSEntregue = new bool[] {false} ;
         T00528_A2057ContagemResultadoNaoCnf_Contestada = new bool[] {false} ;
         T00528_n2057ContagemResultadoNaoCnf_Contestada = new bool[] {false} ;
         T00528_A2020ContagemResultadoNaoCnf_OSCod = new int[1] ;
         T00528_A2023ContagemResultadoNaoCnf_LogRspCod = new long[1] ;
         T00528_A2026ContagemResultadoNaoCnf_NaoCnfCod = new int[1] ;
         T00528_A2060ContagemResultadoNaoCnf_IndCod = new int[1] ;
         T00528_n2060ContagemResultadoNaoCnf_IndCod = new bool[] {false} ;
         T00524_A2020ContagemResultadoNaoCnf_OSCod = new int[1] ;
         T00525_A2023ContagemResultadoNaoCnf_LogRspCod = new long[1] ;
         T00527_A2060ContagemResultadoNaoCnf_IndCod = new int[1] ;
         T00527_n2060ContagemResultadoNaoCnf_IndCod = new bool[] {false} ;
         T00529_A2020ContagemResultadoNaoCnf_OSCod = new int[1] ;
         T005210_A2031ContagemResultadoNaoCnf_Glosavel = new bool[] {false} ;
         T005210_n2031ContagemResultadoNaoCnf_Glosavel = new bool[] {false} ;
         T005211_A2023ContagemResultadoNaoCnf_LogRspCod = new long[1] ;
         T005212_A2060ContagemResultadoNaoCnf_IndCod = new int[1] ;
         T005212_n2060ContagemResultadoNaoCnf_IndCod = new bool[] {false} ;
         T005213_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         T00523_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         T00523_A2053ContagemResultadoNaoCnf_OSEntregue = new bool[] {false} ;
         T00523_n2053ContagemResultadoNaoCnf_OSEntregue = new bool[] {false} ;
         T00523_A2057ContagemResultadoNaoCnf_Contestada = new bool[] {false} ;
         T00523_n2057ContagemResultadoNaoCnf_Contestada = new bool[] {false} ;
         T00523_A2020ContagemResultadoNaoCnf_OSCod = new int[1] ;
         T00523_A2023ContagemResultadoNaoCnf_LogRspCod = new long[1] ;
         T00523_A2026ContagemResultadoNaoCnf_NaoCnfCod = new int[1] ;
         T00523_A2060ContagemResultadoNaoCnf_IndCod = new int[1] ;
         T00523_n2060ContagemResultadoNaoCnf_IndCod = new bool[] {false} ;
         sMode224 = "";
         T005214_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         T005215_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         T00522_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         T00522_A2053ContagemResultadoNaoCnf_OSEntregue = new bool[] {false} ;
         T00522_n2053ContagemResultadoNaoCnf_OSEntregue = new bool[] {false} ;
         T00522_A2057ContagemResultadoNaoCnf_Contestada = new bool[] {false} ;
         T00522_n2057ContagemResultadoNaoCnf_Contestada = new bool[] {false} ;
         T00522_A2020ContagemResultadoNaoCnf_OSCod = new int[1] ;
         T00522_A2023ContagemResultadoNaoCnf_LogRspCod = new long[1] ;
         T00522_A2026ContagemResultadoNaoCnf_NaoCnfCod = new int[1] ;
         T00522_A2060ContagemResultadoNaoCnf_IndCod = new int[1] ;
         T00522_n2060ContagemResultadoNaoCnf_IndCod = new bool[] {false} ;
         T005216_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         T005219_A2031ContagemResultadoNaoCnf_Glosavel = new bool[] {false} ;
         T005219_n2031ContagemResultadoNaoCnf_Glosavel = new bool[] {false} ;
         T005220_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T005221_A2026ContagemResultadoNaoCnf_NaoCnfCod = new int[1] ;
         T005221_A427NaoConformidade_Nome = new String[] {""} ;
         T005221_n427NaoConformidade_Nome = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T005222_A2020ContagemResultadoNaoCnf_OSCod = new int[1] ;
         T005223_A2031ContagemResultadoNaoCnf_Glosavel = new bool[] {false} ;
         T005223_n2031ContagemResultadoNaoCnf_Glosavel = new bool[] {false} ;
         T005224_A2023ContagemResultadoNaoCnf_LogRspCod = new long[1] ;
         T005225_A2060ContagemResultadoNaoCnf_IndCod = new int[1] ;
         T005225_n2060ContagemResultadoNaoCnf_IndCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadonaocnf__default(),
            new Object[][] {
                new Object[] {
               T00522_A2024ContagemResultadoNaoCnf_Codigo, T00522_A2053ContagemResultadoNaoCnf_OSEntregue, T00522_n2053ContagemResultadoNaoCnf_OSEntregue, T00522_A2057ContagemResultadoNaoCnf_Contestada, T00522_n2057ContagemResultadoNaoCnf_Contestada, T00522_A2020ContagemResultadoNaoCnf_OSCod, T00522_A2023ContagemResultadoNaoCnf_LogRspCod, T00522_A2026ContagemResultadoNaoCnf_NaoCnfCod, T00522_A2060ContagemResultadoNaoCnf_IndCod, T00522_n2060ContagemResultadoNaoCnf_IndCod
               }
               , new Object[] {
               T00523_A2024ContagemResultadoNaoCnf_Codigo, T00523_A2053ContagemResultadoNaoCnf_OSEntregue, T00523_n2053ContagemResultadoNaoCnf_OSEntregue, T00523_A2057ContagemResultadoNaoCnf_Contestada, T00523_n2057ContagemResultadoNaoCnf_Contestada, T00523_A2020ContagemResultadoNaoCnf_OSCod, T00523_A2023ContagemResultadoNaoCnf_LogRspCod, T00523_A2026ContagemResultadoNaoCnf_NaoCnfCod, T00523_A2060ContagemResultadoNaoCnf_IndCod, T00523_n2060ContagemResultadoNaoCnf_IndCod
               }
               , new Object[] {
               T00524_A2020ContagemResultadoNaoCnf_OSCod
               }
               , new Object[] {
               T00525_A2023ContagemResultadoNaoCnf_LogRspCod
               }
               , new Object[] {
               T00526_A2031ContagemResultadoNaoCnf_Glosavel, T00526_n2031ContagemResultadoNaoCnf_Glosavel
               }
               , new Object[] {
               T00527_A2060ContagemResultadoNaoCnf_IndCod
               }
               , new Object[] {
               T00528_A2024ContagemResultadoNaoCnf_Codigo, T00528_A2031ContagemResultadoNaoCnf_Glosavel, T00528_n2031ContagemResultadoNaoCnf_Glosavel, T00528_A2053ContagemResultadoNaoCnf_OSEntregue, T00528_n2053ContagemResultadoNaoCnf_OSEntregue, T00528_A2057ContagemResultadoNaoCnf_Contestada, T00528_n2057ContagemResultadoNaoCnf_Contestada, T00528_A2020ContagemResultadoNaoCnf_OSCod, T00528_A2023ContagemResultadoNaoCnf_LogRspCod, T00528_A2026ContagemResultadoNaoCnf_NaoCnfCod,
               T00528_A2060ContagemResultadoNaoCnf_IndCod, T00528_n2060ContagemResultadoNaoCnf_IndCod
               }
               , new Object[] {
               T00529_A2020ContagemResultadoNaoCnf_OSCod
               }
               , new Object[] {
               T005210_A2031ContagemResultadoNaoCnf_Glosavel, T005210_n2031ContagemResultadoNaoCnf_Glosavel
               }
               , new Object[] {
               T005211_A2023ContagemResultadoNaoCnf_LogRspCod
               }
               , new Object[] {
               T005212_A2060ContagemResultadoNaoCnf_IndCod
               }
               , new Object[] {
               T005213_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               T005214_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               T005215_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               T005216_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T005219_A2031ContagemResultadoNaoCnf_Glosavel, T005219_n2031ContagemResultadoNaoCnf_Glosavel
               }
               , new Object[] {
               T005220_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               T005221_A2026ContagemResultadoNaoCnf_NaoCnfCod, T005221_A427NaoConformidade_Nome, T005221_n427NaoConformidade_Nome
               }
               , new Object[] {
               T005222_A2020ContagemResultadoNaoCnf_OSCod
               }
               , new Object[] {
               T005223_A2031ContagemResultadoNaoCnf_Glosavel, T005223_n2031ContagemResultadoNaoCnf_Glosavel
               }
               , new Object[] {
               T005224_A2023ContagemResultadoNaoCnf_LogRspCod
               }
               , new Object[] {
               T005225_A2060ContagemResultadoNaoCnf_IndCod
               }
            }
         );
         Z2053ContagemResultadoNaoCnf_OSEntregue = false;
         n2053ContagemResultadoNaoCnf_OSEntregue = false;
         A2053ContagemResultadoNaoCnf_OSEntregue = false;
         n2053ContagemResultadoNaoCnf_OSEntregue = false;
         i2053ContagemResultadoNaoCnf_OSEntregue = false;
         n2053ContagemResultadoNaoCnf_OSEntregue = false;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound224 ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z2024ContagemResultadoNaoCnf_Codigo ;
      private int Z2020ContagemResultadoNaoCnf_OSCod ;
      private int Z2026ContagemResultadoNaoCnf_NaoCnfCod ;
      private int Z2060ContagemResultadoNaoCnf_IndCod ;
      private int A2020ContagemResultadoNaoCnf_OSCod ;
      private int A2026ContagemResultadoNaoCnf_NaoCnfCod ;
      private int A2060ContagemResultadoNaoCnf_IndCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A2024ContagemResultadoNaoCnf_Codigo ;
      private int edtContagemResultadoNaoCnf_Codigo_Enabled ;
      private int edtContagemResultadoNaoCnf_OSCod_Enabled ;
      private int edtContagemResultadoNaoCnf_LogRspCod_Enabled ;
      private int edtContagemResultadoNaoCnf_IndCod_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private long Z2023ContagemResultadoNaoCnf_LogRspCod ;
      private long A2023ContagemResultadoNaoCnf_LogRspCod ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String chkContagemResultadoNaoCnf_Glosavel_Internalname ;
      private String chkContagemResultadoNaoCnf_Contestada_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoNaoCnf_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemresultadonaocnf_codigo_Internalname ;
      private String lblTextblockcontagemresultadonaocnf_codigo_Jsonclick ;
      private String edtContagemResultadoNaoCnf_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultadonaocnf_oscod_Internalname ;
      private String lblTextblockcontagemresultadonaocnf_oscod_Jsonclick ;
      private String edtContagemResultadoNaoCnf_OSCod_Internalname ;
      private String edtContagemResultadoNaoCnf_OSCod_Jsonclick ;
      private String lblTextblockcontagemresultadonaocnf_naocnfcod_Internalname ;
      private String lblTextblockcontagemresultadonaocnf_naocnfcod_Jsonclick ;
      private String dynContagemResultadoNaoCnf_NaoCnfCod_Internalname ;
      private String dynContagemResultadoNaoCnf_NaoCnfCod_Jsonclick ;
      private String lblTextblockcontagemresultadonaocnf_glosavel_Internalname ;
      private String lblTextblockcontagemresultadonaocnf_glosavel_Jsonclick ;
      private String lblTextblockcontagemresultadonaocnf_logrspcod_Internalname ;
      private String lblTextblockcontagemresultadonaocnf_logrspcod_Jsonclick ;
      private String edtContagemResultadoNaoCnf_LogRspCod_Internalname ;
      private String edtContagemResultadoNaoCnf_LogRspCod_Jsonclick ;
      private String lblTextblockcontagemresultadonaocnf_osentregue_Internalname ;
      private String lblTextblockcontagemresultadonaocnf_osentregue_Jsonclick ;
      private String cmbContagemResultadoNaoCnf_OSEntregue_Internalname ;
      private String cmbContagemResultadoNaoCnf_OSEntregue_Jsonclick ;
      private String lblTextblockcontagemresultadonaocnf_contestada_Internalname ;
      private String lblTextblockcontagemresultadonaocnf_contestada_Jsonclick ;
      private String lblTextblockcontagemresultadonaocnf_indcod_Internalname ;
      private String lblTextblockcontagemresultadonaocnf_indcod_Jsonclick ;
      private String edtContagemResultadoNaoCnf_IndCod_Internalname ;
      private String edtContagemResultadoNaoCnf_IndCod_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode224 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool Z2053ContagemResultadoNaoCnf_OSEntregue ;
      private bool Z2057ContagemResultadoNaoCnf_Contestada ;
      private bool entryPointCalled ;
      private bool n2060ContagemResultadoNaoCnf_IndCod ;
      private bool toggleJsOutput ;
      private bool A2053ContagemResultadoNaoCnf_OSEntregue ;
      private bool n2053ContagemResultadoNaoCnf_OSEntregue ;
      private bool wbErr ;
      private bool A2031ContagemResultadoNaoCnf_Glosavel ;
      private bool A2057ContagemResultadoNaoCnf_Contestada ;
      private bool n2031ContagemResultadoNaoCnf_Glosavel ;
      private bool n2057ContagemResultadoNaoCnf_Contestada ;
      private bool Z2031ContagemResultadoNaoCnf_Glosavel ;
      private bool Gx_longc ;
      private bool i2053ContagemResultadoNaoCnf_OSEntregue ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynContagemResultadoNaoCnf_NaoCnfCod ;
      private GXCheckbox chkContagemResultadoNaoCnf_Glosavel ;
      private GXCombobox cmbContagemResultadoNaoCnf_OSEntregue ;
      private GXCheckbox chkContagemResultadoNaoCnf_Contestada ;
      private IDataStoreProvider pr_default ;
      private bool[] T00526_A2031ContagemResultadoNaoCnf_Glosavel ;
      private bool[] T00526_n2031ContagemResultadoNaoCnf_Glosavel ;
      private int[] T00528_A2024ContagemResultadoNaoCnf_Codigo ;
      private bool[] T00528_A2031ContagemResultadoNaoCnf_Glosavel ;
      private bool[] T00528_n2031ContagemResultadoNaoCnf_Glosavel ;
      private bool[] T00528_A2053ContagemResultadoNaoCnf_OSEntregue ;
      private bool[] T00528_n2053ContagemResultadoNaoCnf_OSEntregue ;
      private bool[] T00528_A2057ContagemResultadoNaoCnf_Contestada ;
      private bool[] T00528_n2057ContagemResultadoNaoCnf_Contestada ;
      private int[] T00528_A2020ContagemResultadoNaoCnf_OSCod ;
      private long[] T00528_A2023ContagemResultadoNaoCnf_LogRspCod ;
      private int[] T00528_A2026ContagemResultadoNaoCnf_NaoCnfCod ;
      private int[] T00528_A2060ContagemResultadoNaoCnf_IndCod ;
      private bool[] T00528_n2060ContagemResultadoNaoCnf_IndCod ;
      private int[] T00524_A2020ContagemResultadoNaoCnf_OSCod ;
      private long[] T00525_A2023ContagemResultadoNaoCnf_LogRspCod ;
      private int[] T00527_A2060ContagemResultadoNaoCnf_IndCod ;
      private bool[] T00527_n2060ContagemResultadoNaoCnf_IndCod ;
      private int[] T00529_A2020ContagemResultadoNaoCnf_OSCod ;
      private bool[] T005210_A2031ContagemResultadoNaoCnf_Glosavel ;
      private bool[] T005210_n2031ContagemResultadoNaoCnf_Glosavel ;
      private long[] T005211_A2023ContagemResultadoNaoCnf_LogRspCod ;
      private int[] T005212_A2060ContagemResultadoNaoCnf_IndCod ;
      private bool[] T005212_n2060ContagemResultadoNaoCnf_IndCod ;
      private int[] T005213_A2024ContagemResultadoNaoCnf_Codigo ;
      private int[] T00523_A2024ContagemResultadoNaoCnf_Codigo ;
      private bool[] T00523_A2053ContagemResultadoNaoCnf_OSEntregue ;
      private bool[] T00523_n2053ContagemResultadoNaoCnf_OSEntregue ;
      private bool[] T00523_A2057ContagemResultadoNaoCnf_Contestada ;
      private bool[] T00523_n2057ContagemResultadoNaoCnf_Contestada ;
      private int[] T00523_A2020ContagemResultadoNaoCnf_OSCod ;
      private long[] T00523_A2023ContagemResultadoNaoCnf_LogRspCod ;
      private int[] T00523_A2026ContagemResultadoNaoCnf_NaoCnfCod ;
      private int[] T00523_A2060ContagemResultadoNaoCnf_IndCod ;
      private bool[] T00523_n2060ContagemResultadoNaoCnf_IndCod ;
      private int[] T005214_A2024ContagemResultadoNaoCnf_Codigo ;
      private int[] T005215_A2024ContagemResultadoNaoCnf_Codigo ;
      private int[] T00522_A2024ContagemResultadoNaoCnf_Codigo ;
      private bool[] T00522_A2053ContagemResultadoNaoCnf_OSEntregue ;
      private bool[] T00522_n2053ContagemResultadoNaoCnf_OSEntregue ;
      private bool[] T00522_A2057ContagemResultadoNaoCnf_Contestada ;
      private bool[] T00522_n2057ContagemResultadoNaoCnf_Contestada ;
      private int[] T00522_A2020ContagemResultadoNaoCnf_OSCod ;
      private long[] T00522_A2023ContagemResultadoNaoCnf_LogRspCod ;
      private int[] T00522_A2026ContagemResultadoNaoCnf_NaoCnfCod ;
      private int[] T00522_A2060ContagemResultadoNaoCnf_IndCod ;
      private bool[] T00522_n2060ContagemResultadoNaoCnf_IndCod ;
      private int[] T005216_A2024ContagemResultadoNaoCnf_Codigo ;
      private bool[] T005219_A2031ContagemResultadoNaoCnf_Glosavel ;
      private bool[] T005219_n2031ContagemResultadoNaoCnf_Glosavel ;
      private int[] T005220_A2024ContagemResultadoNaoCnf_Codigo ;
      private int[] T005221_A2026ContagemResultadoNaoCnf_NaoCnfCod ;
      private String[] T005221_A427NaoConformidade_Nome ;
      private bool[] T005221_n427NaoConformidade_Nome ;
      private int[] T005222_A2020ContagemResultadoNaoCnf_OSCod ;
      private bool[] T005223_A2031ContagemResultadoNaoCnf_Glosavel ;
      private bool[] T005223_n2031ContagemResultadoNaoCnf_Glosavel ;
      private long[] T005224_A2023ContagemResultadoNaoCnf_LogRspCod ;
      private int[] T005225_A2060ContagemResultadoNaoCnf_IndCod ;
      private bool[] T005225_n2060ContagemResultadoNaoCnf_IndCod ;
      private GXWebForm Form ;
   }

   public class contagemresultadonaocnf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00528 ;
          prmT00528 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00524 ;
          prmT00524 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00526 ;
          prmT00526 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00525 ;
          prmT00525 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_LogRspCod",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT00527 ;
          prmT00527 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_IndCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00529 ;
          prmT00529 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005210 ;
          prmT005210 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005211 ;
          prmT005211 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_LogRspCod",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT005212 ;
          prmT005212 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_IndCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005213 ;
          prmT005213 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00523 ;
          prmT00523 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005214 ;
          prmT005214 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005215 ;
          prmT005215 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00522 ;
          prmT00522 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005216 ;
          prmT005216 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_OSEntregue",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_Contestada",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_LogRspCod",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_NaoCnfCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_IndCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005217 ;
          prmT005217 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_OSEntregue",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_Contestada",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_LogRspCod",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_NaoCnfCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_IndCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005218 ;
          prmT005218 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005219 ;
          prmT005219 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005220 ;
          prmT005220 = new Object[] {
          } ;
          Object[] prmT005221 ;
          prmT005221 = new Object[] {
          } ;
          Object[] prmT005222 ;
          prmT005222 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005223 ;
          prmT005223 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_NaoCnfCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005224 ;
          prmT005224 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_LogRspCod",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT005225 ;
          prmT005225 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_IndCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00522", "SELECT [ContagemResultadoNaoCnf_Codigo], [ContagemResultadoNaoCnf_OSEntregue], [ContagemResultadoNaoCnf_Contestada], [ContagemResultadoNaoCnf_OSCod] AS ContagemResultadoNaoCnf_OSCod, [ContagemResultadoNaoCnf_LogRspCod] AS ContagemResultadoNaoCnf_LogRspCod, [ContagemResultadoNaoCnf_NaoCnfCod] AS ContagemResultadoNaoCnf_NaoCnfCod, [ContagemResultadoNaoCnf_IndCod] AS ContagemResultadoNaoCnf_IndCod FROM [ContagemResultadoNaoCnf] WITH (UPDLOCK) WHERE [ContagemResultadoNaoCnf_Codigo] = @ContagemResultadoNaoCnf_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00522,1,0,true,false )
             ,new CursorDef("T00523", "SELECT [ContagemResultadoNaoCnf_Codigo], [ContagemResultadoNaoCnf_OSEntregue], [ContagemResultadoNaoCnf_Contestada], [ContagemResultadoNaoCnf_OSCod] AS ContagemResultadoNaoCnf_OSCod, [ContagemResultadoNaoCnf_LogRspCod] AS ContagemResultadoNaoCnf_LogRspCod, [ContagemResultadoNaoCnf_NaoCnfCod] AS ContagemResultadoNaoCnf_NaoCnfCod, [ContagemResultadoNaoCnf_IndCod] AS ContagemResultadoNaoCnf_IndCod FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) WHERE [ContagemResultadoNaoCnf_Codigo] = @ContagemResultadoNaoCnf_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00523,1,0,true,false )
             ,new CursorDef("T00524", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoNaoCnf_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoNaoCnf_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00524,1,0,true,false )
             ,new CursorDef("T00525", "SELECT [LogResponsavel_Codigo] AS ContagemResultadoNaoCnf_LogRspCod FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_Codigo] = @ContagemResultadoNaoCnf_LogRspCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00525,1,0,true,false )
             ,new CursorDef("T00526", "SELECT [NaoConformidade_Glosavel] AS ContagemResultadoNaoCnf_Glosavel FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultadoNaoCnf_NaoCnfCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00526,1,0,true,false )
             ,new CursorDef("T00527", "SELECT [Indicador_Codigo] AS ContagemResultadoNaoCnf_IndCod FROM [Indicador] WITH (NOLOCK) WHERE [Indicador_Codigo] = @ContagemResultadoNaoCnf_IndCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00527,1,0,true,false )
             ,new CursorDef("T00528", "SELECT TM1.[ContagemResultadoNaoCnf_Codigo], T2.[NaoConformidade_Glosavel] AS ContagemResultadoNaoCnf_Glosavel, TM1.[ContagemResultadoNaoCnf_OSEntregue], TM1.[ContagemResultadoNaoCnf_Contestada], TM1.[ContagemResultadoNaoCnf_OSCod] AS ContagemResultadoNaoCnf_OSCod, TM1.[ContagemResultadoNaoCnf_LogRspCod] AS ContagemResultadoNaoCnf_LogRspCod, TM1.[ContagemResultadoNaoCnf_NaoCnfCod] AS ContagemResultadoNaoCnf_NaoCnfCod, TM1.[ContagemResultadoNaoCnf_IndCod] AS ContagemResultadoNaoCnf_IndCod FROM ([ContagemResultadoNaoCnf] TM1 WITH (NOLOCK) INNER JOIN [NaoConformidade] T2 WITH (NOLOCK) ON T2.[NaoConformidade_Codigo] = TM1.[ContagemResultadoNaoCnf_NaoCnfCod]) WHERE TM1.[ContagemResultadoNaoCnf_Codigo] = @ContagemResultadoNaoCnf_Codigo ORDER BY TM1.[ContagemResultadoNaoCnf_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00528,100,0,true,false )
             ,new CursorDef("T00529", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoNaoCnf_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoNaoCnf_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00529,1,0,true,false )
             ,new CursorDef("T005210", "SELECT [NaoConformidade_Glosavel] AS ContagemResultadoNaoCnf_Glosavel FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultadoNaoCnf_NaoCnfCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005210,1,0,true,false )
             ,new CursorDef("T005211", "SELECT [LogResponsavel_Codigo] AS ContagemResultadoNaoCnf_LogRspCod FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_Codigo] = @ContagemResultadoNaoCnf_LogRspCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005211,1,0,true,false )
             ,new CursorDef("T005212", "SELECT [Indicador_Codigo] AS ContagemResultadoNaoCnf_IndCod FROM [Indicador] WITH (NOLOCK) WHERE [Indicador_Codigo] = @ContagemResultadoNaoCnf_IndCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005212,1,0,true,false )
             ,new CursorDef("T005213", "SELECT [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) WHERE [ContagemResultadoNaoCnf_Codigo] = @ContagemResultadoNaoCnf_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005213,1,0,true,false )
             ,new CursorDef("T005214", "SELECT TOP 1 [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) WHERE ( [ContagemResultadoNaoCnf_Codigo] > @ContagemResultadoNaoCnf_Codigo) ORDER BY [ContagemResultadoNaoCnf_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005214,1,0,true,true )
             ,new CursorDef("T005215", "SELECT TOP 1 [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) WHERE ( [ContagemResultadoNaoCnf_Codigo] < @ContagemResultadoNaoCnf_Codigo) ORDER BY [ContagemResultadoNaoCnf_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT005215,1,0,true,true )
             ,new CursorDef("T005216", "INSERT INTO [ContagemResultadoNaoCnf]([ContagemResultadoNaoCnf_OSEntregue], [ContagemResultadoNaoCnf_Contestada], [ContagemResultadoNaoCnf_OSCod], [ContagemResultadoNaoCnf_LogRspCod], [ContagemResultadoNaoCnf_NaoCnfCod], [ContagemResultadoNaoCnf_IndCod]) VALUES(@ContagemResultadoNaoCnf_OSEntregue, @ContagemResultadoNaoCnf_Contestada, @ContagemResultadoNaoCnf_OSCod, @ContagemResultadoNaoCnf_LogRspCod, @ContagemResultadoNaoCnf_NaoCnfCod, @ContagemResultadoNaoCnf_IndCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT005216)
             ,new CursorDef("T005217", "UPDATE [ContagemResultadoNaoCnf] SET [ContagemResultadoNaoCnf_OSEntregue]=@ContagemResultadoNaoCnf_OSEntregue, [ContagemResultadoNaoCnf_Contestada]=@ContagemResultadoNaoCnf_Contestada, [ContagemResultadoNaoCnf_OSCod]=@ContagemResultadoNaoCnf_OSCod, [ContagemResultadoNaoCnf_LogRspCod]=@ContagemResultadoNaoCnf_LogRspCod, [ContagemResultadoNaoCnf_NaoCnfCod]=@ContagemResultadoNaoCnf_NaoCnfCod, [ContagemResultadoNaoCnf_IndCod]=@ContagemResultadoNaoCnf_IndCod  WHERE [ContagemResultadoNaoCnf_Codigo] = @ContagemResultadoNaoCnf_Codigo", GxErrorMask.GX_NOMASK,prmT005217)
             ,new CursorDef("T005218", "DELETE FROM [ContagemResultadoNaoCnf]  WHERE [ContagemResultadoNaoCnf_Codigo] = @ContagemResultadoNaoCnf_Codigo", GxErrorMask.GX_NOMASK,prmT005218)
             ,new CursorDef("T005219", "SELECT [NaoConformidade_Glosavel] AS ContagemResultadoNaoCnf_Glosavel FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultadoNaoCnf_NaoCnfCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005219,1,0,true,false )
             ,new CursorDef("T005220", "SELECT [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) ORDER BY [ContagemResultadoNaoCnf_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005220,100,0,true,false )
             ,new CursorDef("T005221", "SELECT [NaoConformidade_Codigo] AS ContagemResultadoNaoCnf_NaoCnfCod, [NaoConformidade_Nome] FROM [NaoConformidade] WITH (NOLOCK) ORDER BY [NaoConformidade_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005221,0,0,true,false )
             ,new CursorDef("T005222", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoNaoCnf_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoNaoCnf_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005222,1,0,true,false )
             ,new CursorDef("T005223", "SELECT [NaoConformidade_Glosavel] AS ContagemResultadoNaoCnf_Glosavel FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultadoNaoCnf_NaoCnfCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005223,1,0,true,false )
             ,new CursorDef("T005224", "SELECT [LogResponsavel_Codigo] AS ContagemResultadoNaoCnf_LogRspCod FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_Codigo] = @ContagemResultadoNaoCnf_LogRspCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005224,1,0,true,false )
             ,new CursorDef("T005225", "SELECT [Indicador_Codigo] AS ContagemResultadoNaoCnf_IndCod FROM [Indicador] WITH (NOLOCK) WHERE [Indicador_Codigo] = @ContagemResultadoNaoCnf_IndCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005225,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((long[]) buf[6])[0] = rslt.getLong(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((long[]) buf[6])[0] = rslt.getLong(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 4 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((long[]) buf[8])[0] = rslt.getLong(6) ;
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (long)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[8]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (long)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[8]);
                }
                stmt.SetParameter(7, (int)parms[9]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
