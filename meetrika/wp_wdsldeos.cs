/*
               File: WP_WDSLdeOS
        Description: WDSL de  OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:19:39.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_wdsldeos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_wdsldeos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_wdsldeos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavSecure = new GXCombobox();
         cmbavProjeto = new GXCombobox();
         dynavContratada_codigo = new GXCombobox();
         cmbavTracker = new GXCombobox();
         dynavContrato = new GXCombobox();
         dynavServico_codigo = new GXCombobox();
         cmbavStatus = new GXCombobox();
         dynavContratada_codigo = new GXCombobox();
         dynavServico_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV50WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGOKD2( AV50WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATO") == 0 )
            {
               AV33Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATOKD2( AV33Contratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSERVICO_CODIGO") == 0 )
            {
               AV68Contrato = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contrato), 6, 0)));
               AV33Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSERVICO_CODIGOKD2( AV68Contrato, AV33Contratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_202 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_202_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_202_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( ) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid2") == 0 )
            {
               nRC_GXsfl_120 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_120_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_120_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid2_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid2") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid2_refresh( ) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAKD2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTKD2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221194028");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_wdsldeos.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_issues", AV7SDT_Issues);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_issues", AV7SDT_Issues);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_120", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_120), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vVERSAO", StringUtil.RTrim( AV44Versao));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ISSUES", AV7SDT_Issues);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ISSUES", AV7SDT_Issues);
         }
         GxWebStd.gx_hidden_field( context, "vEXECUTE", StringUtil.RTrim( AV9Execute));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_RDMNISSUEID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1389ContagemResultado_RdmnIssueId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_RDMNUPDATED", context.localUtil.TToC( A1392ContagemResultado_RdmnUpdated, 10, 8, 0, 0, "/", ":", " "));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_CONSULTA", AV59SDT_Consulta);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_CONSULTA", AV59SDT_Consulta);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROJETOSEMDEPARA", AV51ProjetoSemDePara);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROJETOSEMDEPARA", AV51ProjetoSemDePara);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRACKERSSEMDEPARA", AV56TrackersSemDePara);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRACKERSSEMDEPARA", AV56TrackersSemDePara);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSTATUSSEMDEPARA", AV57StatusSemDePara);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSTATUSSEMDEPARA", AV57StatusSemDePara);
         }
         GxWebStd.gx_hidden_field( context, "vOFFSET", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71offset), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPAGINA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58pagina), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMADEPARA_ORIGEM", StringUtil.RTrim( A1461SistemaDePara_Origem));
         GxWebStd.gx_hidden_field( context, "SISTEMADEPARA_ORIGEMID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1462SistemaDePara_OrigemId), 18, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROJECTITEM", AV14ProjectItem);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROJECTITEM", AV14ProjectItem);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSDEPARA_ORIGEM", StringUtil.RTrim( A1466ContratoServicosDePara_Origem));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSDEPARA_ORIGENID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1467ContratoServicosDePara_OrigenId), 18, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSDEPARA_ORIGENID2", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1469ContratoServicosDePara_OrigenId2), 18, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vSEMDEPARA", AV54SemDePara);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRACKERSITEM", AV25TrackersItem);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRACKERSITEM", AV25TrackersItem);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSTATUSITEM", AV16StatusItem);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSTATUSITEM", AV16StatusItem);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vSISTEMASVALIDADOS", AV70SistemasValidados);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_PARAMETROS", AV64SDT_Parametros);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_PARAMETROS", AV64SDT_Parametros);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV50WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV50WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vWWPCONTEXT_Areatrabalho_codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50WWPContext.gxTpr_Areatrabalho_codigo), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEKD2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTKD2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_wdsldeos.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_WDSLdeOS" ;
      }

      public override String GetPgmdesc( )
      {
         return "WDSL de  OS" ;
      }

      protected void WBKD0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_KD2( true) ;
         }
         else
         {
            wb_table1_2_KD2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_KD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "<p></p>") ;
            wb_table2_219_KD2( true) ;
         }
         else
         {
            wb_table2_219_KD2( false) ;
         }
         return  ;
      }

      protected void wb_table2_219_KD2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTKD2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "WDSL de  OS", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPKD0( ) ;
      }

      protected void WSKD2( )
      {
         STARTKD2( ) ;
         EVTKD2( ) ;
      }

      protected void EVTKD2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CARREGARFILTROS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11KD2 */
                              E11KD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'BTNCONSULTA'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12KD2 */
                              E12KD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'IMPORTAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13KD2 */
                              E13KD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'VERIFICARSISTEMAS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14KD2 */
                              E14KD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 19), "'VERIFICARSERVICOS'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID2.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID1.LOAD") == 0 ) )
                           {
                              nGXsfl_120_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_120_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_120_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1202( ) ;
                              GXCCtl = "GRID1_Collapsed_" + sGXsfl_120_idx;
                              subGrid1_Collapsed = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
                              AV76GXV3 = nGXsfl_120_idx;
                              if ( ( AV7SDT_Issues.gxTpr_Issues.Count >= AV76GXV3 ) && ( AV76GXV3 > 0 ) )
                              {
                                 AV7SDT_Issues.gxTpr_Issues.CurrentItem = ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3));
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid1_Internalname), ",", "."));
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Project.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid2_Internalname), ",", "."));
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Project.gxTpr_Name = cgiGet( edtavCtlname1_Internalname);
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Tracker.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid3_Internalname), ",", "."));
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Tracker.gxTpr_Name = cgiGet( edtavCtlname2_Internalname);
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Status.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlname3_Internalname), ",", "."));
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Status.gxTpr_Name = cgiGet( edtavCtlname7_Internalname);
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Start_date = cgiGet( edtavCtlstart_date_Internalname);
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Assigned_to.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid7_Internalname), ",", "."));
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Priority.gxTpr_Name = cgiGet( edtavCtlname4_Internalname);
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Due_date = cgiGet( edtavCtldue_date_Internalname);
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Assigned_to.gxTpr_Name = cgiGet( edtavCtlname6_Internalname);
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Author.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid6_Internalname), ",", "."));
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Author.gxTpr_Name = cgiGet( edtavCtlname5_Internalname);
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Subject = cgiGet( edtavCtlsubject_Internalname);
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Description = cgiGet( edtavCtldescription_Internalname);
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Done_ratio = (short)(context.localUtil.CToN( cgiGet( edtavCtldone_ratio_Internalname), ",", "."));
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Estimated_hours = cgiGet( edtavCtlestimated_hours_Internalname);
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Created_on = cgiGet( edtavCtlcreated_on_Internalname);
                                 ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Updated_on = cgiGet( edtavCtlupdated_on_Internalname);
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15KD2 */
                                    E15KD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'VERIFICARSERVICOS'") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E16KD2 */
                                    E16KD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID2.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E17KD2 */
                                    E17KD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                                 sEvtType = StringUtil.Right( sEvt, 4);
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                                 if ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID1.LOAD") == 0 )
                                 {
                                    nGXsfl_202_idx = (short)(NumberUtil.Val( sEvtType, "."));
                                    sGXsfl_202_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_202_idx), 4, 0)), 4, "0") + sGXsfl_120_idx;
                                    SubsflControlProps_20210( ) ;
                                    AV95GXV22 = nGXsfl_202_idx;
                                    if ( ( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Count >= AV95GXV22 ) && ( AV95GXV22 > 0 ) )
                                    {
                                       ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.CurrentItem = ((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV95GXV22));
                                       ((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV95GXV22)).gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid_Internalname), ",", "."));
                                       ((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV95GXV22)).gxTpr_Name = cgiGet( edtavCtlname_Internalname);
                                       ((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV95GXV22)).gxTpr_Value = cgiGet( edtavCtlvalue_Internalname);
                                    }
                                    sEvtType = StringUtil.Right( sEvt, 1);
                                    if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                                    {
                                       sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                       if ( StringUtil.StrCmp(sEvt, "GRID1.LOAD") == 0 )
                                       {
                                          context.wbHandled = 1;
                                          dynload_actions( ) ;
                                          /* Execute user event: E18KD10 */
                                          E18KD10 ();
                                       }
                                       else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                       {
                                          context.wbHandled = 1;
                                          dynload_actions( ) ;
                                       }
                                    }
                                    else
                                    {
                                    }
                                 }
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKD2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAKD2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavSecure.Name = "vSECURE";
            cmbavSecure.WebTags = "";
            cmbavSecure.addItem("0", "http://", 0);
            cmbavSecure.addItem("1", "https://", 0);
            if ( cmbavSecure.ItemCount > 0 )
            {
               AV63Secure = (short)(NumberUtil.Val( cmbavSecure.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV63Secure), 1, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Secure", StringUtil.Str( (decimal)(AV63Secure), 1, 0));
            }
            cmbavProjeto.Name = "vPROJETO";
            cmbavProjeto.WebTags = "";
            cmbavProjeto.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 18, 0)), "(Todos)", 0);
            if ( cmbavProjeto.ItemCount > 0 )
            {
               AV6Projeto = (long)(NumberUtil.Val( cmbavProjeto.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6Projeto), 18, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Projeto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Projeto), 18, 0)));
            }
            dynavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            dynavContratada_codigo.WebTags = "";
            cmbavTracker.Name = "vTRACKER";
            cmbavTracker.WebTags = "";
            cmbavTracker.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Todos)", 0);
            if ( cmbavTracker.ItemCount > 0 )
            {
               AV26Tracker = (short)(NumberUtil.Val( cmbavTracker.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26Tracker), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Tracker", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Tracker), 4, 0)));
            }
            dynavContrato.Name = "vCONTRATO";
            dynavContrato.WebTags = "";
            dynavServico_codigo.Name = "vSERVICO_CODIGO";
            dynavServico_codigo.WebTags = "";
            cmbavStatus.Name = "vSTATUS";
            cmbavStatus.WebTags = "";
            cmbavStatus.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Todos", 0);
            if ( cmbavStatus.ItemCount > 0 )
            {
               AV8Status = (short)(NumberUtil.Val( cmbavStatus.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8Status), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Status), 4, 0)));
            }
            dynavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            dynavContratada_codigo.WebTags = "";
            dynavServico_codigo.Name = "vSERVICO_CODIGO";
            dynavServico_codigo.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavUsr_nome_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvSERVICO_CODIGO_htmlKD2( AV68Contrato, AV33Contratada_Codigo) ;
         GXVvSERVICO_CODIGO_htmlKD2( AV68Contrato, AV33Contratada_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADA_CODIGOKD2( wwpbaseobjects.SdtWWPContext AV50WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO_dataKD2( AV50WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO_htmlKD2( wwpbaseobjects.SdtWWPContext AV50WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO_dataKD2( AV50WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV33Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO_dataKD2( wwpbaseobjects.SdtWWPContext AV50WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H00KD2 */
         pr_default.execute(0, new Object[] {AV50WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00KD2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00KD2_A41Contratada_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTRATOKD2( int AV33Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATO_dataKD2( AV33Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATO_htmlKD2( int AV33Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATO_dataKD2( AV33Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavContrato.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContrato.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContrato.ItemCount > 0 )
         {
            AV68Contrato = (int)(NumberUtil.Val( dynavContrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV68Contrato), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contrato), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATO_dataKD2( int AV33Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00KD3 */
         pr_default.execute(1, new Object[] {AV33Contratada_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00KD3_A74Contrato_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00KD3_A77Contrato_Numero[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvSERVICO_CODIGOKD2( int AV68Contrato ,
                                              int AV33Contratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICO_CODIGO_dataKD2( AV68Contrato, AV33Contratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICO_CODIGO_htmlKD2( int AV68Contrato ,
                                                 int AV33Contratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICO_CODIGO_dataKD2( AV68Contrato, AV33Contratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavServico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV34Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSERVICO_CODIGO_dataKD2( int AV68Contrato ,
                                                   int AV33Contratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00KD4 */
         pr_default.execute(2, new Object[] {AV68Contrato, AV33Contratada_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00KD4_A160ContratoServicos_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00KD4_A826ContratoServicos_ServicoSigla[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void gxnrGrid2_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1202( ) ;
         while ( nGXsfl_120_idx <= nRC_GXsfl_120 )
         {
            sendrow_1202( ) ;
            nGXsfl_120_idx = (short)(((subGrid2_Islastpage==1)&&(nGXsfl_120_idx+1>subGrid2_Recordsperpage( )) ? 1 : nGXsfl_120_idx+1));
            sGXsfl_120_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_120_idx), 4, 0)), 4, "0");
            SubsflControlProps_1202( ) ;
         }
         context.GX_webresponse.AddString(Grid2Container.ToJavascriptSource());
         /* End function gxnrGrid2_newrow */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_20210( ) ;
         while ( nGXsfl_202_idx <= nRC_GXsfl_202 )
         {
            sendrow_20210( ) ;
            nGXsfl_202_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_202_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_202_idx+1));
            sGXsfl_202_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_202_idx), 4, 0)), 4, "0") + sGXsfl_120_idx;
            SubsflControlProps_20210( ) ;
         }
         context.GX_webresponse.AddString(Grid1Container.ToJavascriptSource());
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID1_nCurrentRecord = 0;
         RFKD10( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void gxgrGrid2_refresh( )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID2_nCurrentRecord = 0;
         RFKD2( ) ;
         /* End function gxgrGrid2_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavSecure.ItemCount > 0 )
         {
            AV63Secure = (short)(NumberUtil.Val( cmbavSecure.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV63Secure), 1, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Secure", StringUtil.Str( (decimal)(AV63Secure), 1, 0));
         }
         if ( cmbavProjeto.ItemCount > 0 )
         {
            AV6Projeto = (long)(NumberUtil.Val( cmbavProjeto.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6Projeto), 18, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Projeto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Projeto), 18, 0)));
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV33Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0)));
         }
         if ( cmbavTracker.ItemCount > 0 )
         {
            AV26Tracker = (short)(NumberUtil.Val( cmbavTracker.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26Tracker), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Tracker", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Tracker), 4, 0)));
         }
         if ( dynavContrato.ItemCount > 0 )
         {
            AV68Contrato = (int)(NumberUtil.Val( dynavContrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV68Contrato), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contrato), 6, 0)));
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV34Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0)));
         }
         if ( cmbavStatus.ItemCount > 0 )
         {
            AV8Status = (short)(NumberUtil.Val( cmbavStatus.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8Status), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Status), 4, 0)));
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV33Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0)));
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV34Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKD2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCount_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCount_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCount_Enabled), 5, 0)));
         edtavCtloffset_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtloffset_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtloffset_Enabled), 5, 0)));
         edtavCtllimit_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtllimit_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtllimit_Enabled), 5, 0)));
      }

      protected void RFKD2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid2Container.ClearRows();
         }
         wbStart = 120;
         nGXsfl_120_idx = 1;
         sGXsfl_120_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_120_idx), 4, 0)), 4, "0");
         SubsflControlProps_1202( ) ;
         nGXsfl_120_Refreshing = 1;
         Grid2Container.AddObjectProperty("GridName", "Grid2");
         Grid2Container.AddObjectProperty("CmpContext", "");
         Grid2Container.AddObjectProperty("InMasterPage", "false");
         Grid2Container.AddObjectProperty("Bordercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)((int)(0x000000)), 9, 0, ".", "")));
         Grid2Container.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Rules", StringUtil.RTrim( "none"));
         Grid2Container.AddObjectProperty("Class", "FreeStyleGrid");
         Grid2Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Backcolorstyle), 1, 0, ".", "")));
         Grid2Container.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Borderwidth), 4, 0, ".", "")));
         Grid2Container.AddObjectProperty("Bordercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Bordercolor), 9, 0, ".", "")));
         Grid2Container.PageSize = subGrid2_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1202( ) ;
            /* Execute user event: E17KD2 */
            E17KD2 ();
            wbEnd = 120;
            WBKD0( ) ;
         }
         nGXsfl_120_Refreshing = 0;
      }

      protected void RFKD10( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 202;
         nGXsfl_202_idx = 1;
         sGXsfl_202_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_202_idx), 4, 0)), 4, "0") + sGXsfl_120_idx;
         SubsflControlProps_20210( ) ;
         nGXsfl_202_Refreshing = 1;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "Grid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcoloreven", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcoloreven), 9, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_20210( ) ;
            /* Execute user event: E18KD10 */
            E18KD10 ();
            wbEnd = 202;
            WBKD0( ) ;
         }
         nGXsfl_202_Refreshing = 0;
      }

      protected int subGrid2_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid2_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPKD0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCount_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCount_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCount_Enabled), 5, 0)));
         edtavCtloffset_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtloffset_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtloffset_Enabled), 5, 0)));
         edtavCtllimit_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtllimit_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtllimit_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E15KD2 */
         E15KD2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTRATADA_CODIGO_htmlKD2( AV50WWPContext) ;
         GXVvCONTRATO_htmlKD2( AV33Contratada_Codigo) ;
         GXVvSERVICO_CODIGO_htmlKD2( AV68Contrato, AV33Contratada_Codigo) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_issues"), AV7SDT_Issues);
            ajax_req_read_hidden_sdt(cgiGet( "vSDT_ISSUES"), AV7SDT_Issues);
            /* Read variables values. */
            AV47Usr_Nome = cgiGet( edtavUsr_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Usr_Nome", AV47Usr_Nome);
            AV48Usr_Senha = cgiGet( edtavUsr_senha_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Usr_Senha", AV48Usr_Senha);
            cmbavSecure.Name = cmbavSecure_Internalname;
            cmbavSecure.CurrentValue = cgiGet( cmbavSecure_Internalname);
            AV63Secure = (short)(NumberUtil.Val( cgiGet( cmbavSecure_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Secure", StringUtil.Str( (decimal)(AV63Secure), 1, 0));
            AV27Host = cgiGet( edtavHost_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Host", AV27Host);
            AV28Url = cgiGet( edtavUrl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Url", AV28Url);
            AV10Key = cgiGet( edtavKey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Key", AV10Key);
            cmbavProjeto.Name = cmbavProjeto_Internalname;
            cmbavProjeto.CurrentValue = cgiGet( cmbavProjeto_Internalname);
            AV6Projeto = (long)(NumberUtil.Val( cgiGet( cmbavProjeto_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Projeto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Projeto), 18, 0)));
            dynavContratada_codigo.Name = dynavContratada_codigo_Internalname;
            dynavContratada_codigo.CurrentValue = cgiGet( dynavContratada_codigo_Internalname);
            AV33Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0)));
            cmbavTracker.Name = cmbavTracker_Internalname;
            cmbavTracker.CurrentValue = cgiGet( cmbavTracker_Internalname);
            AV26Tracker = (short)(NumberUtil.Val( cgiGet( cmbavTracker_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Tracker", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Tracker), 4, 0)));
            dynavContrato.Name = dynavContrato_Internalname;
            dynavContrato.CurrentValue = cgiGet( dynavContrato_Internalname);
            AV68Contrato = (int)(NumberUtil.Val( cgiGet( dynavContrato_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Contrato), 6, 0)));
            dynavServico_codigo.Name = dynavServico_codigo_Internalname;
            dynavServico_codigo.CurrentValue = cgiGet( dynavServico_codigo_Internalname);
            AV34Servico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0)));
            cmbavStatus.Name = cmbavStatus_Internalname;
            cmbavStatus.CurrentValue = cgiGet( cmbavStatus_Internalname);
            AV8Status = (short)(NumberUtil.Val( cgiGet( cmbavStatus_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Status", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Status), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavCreated_from_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Created_From"}), 1, "vCREATED_FROM");
               GX_FocusControl = edtavCreated_from_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40Created_From = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Created_From", context.localUtil.TToC( AV40Created_From, 10, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV40Created_From = context.localUtil.CToT( cgiGet( edtavCreated_from_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Created_From", context.localUtil.TToC( AV40Created_From, 10, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavCreated_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Created_To"}), 1, "vCREATED_TO");
               GX_FocusControl = edtavCreated_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41Created_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Created_To", context.localUtil.TToC( AV41Created_To, 10, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV41Created_To = context.localUtil.CToT( cgiGet( edtavCreated_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Created_To", context.localUtil.TToC( AV41Created_To, 10, 5, 0, 3, "/", ":", " "));
            }
            AV66Campo_Sistemas = cgiGet( edtavCampo_sistemas_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Campo_Sistemas", AV66Campo_Sistemas);
            AV65Campo_Servicos = cgiGet( edtavCampo_servicos_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Campo_Servicos", AV65Campo_Servicos);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCount_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCount_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCOUNT");
               GX_FocusControl = edtavCount_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60Count = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Count", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Count), 4, 0)));
            }
            else
            {
               AV60Count = (short)(context.localUtil.CToN( cgiGet( edtavCount_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Count", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Count), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCtloffset_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtloffset_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CTLOFFSET");
               GX_FocusControl = edtavCtloffset_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7SDT_Issues.gxTpr_Offset = 0;
            }
            else
            {
               AV7SDT_Issues.gxTpr_Offset = (short)(context.localUtil.CToN( cgiGet( edtavCtloffset_Internalname), ",", "."));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCtllimit_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtllimit_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CTLLIMIT");
               GX_FocusControl = edtavCtllimit_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7SDT_Issues.gxTpr_Limit = 0;
            }
            else
            {
               AV7SDT_Issues.gxTpr_Limit = (short)(context.localUtil.CToN( cgiGet( edtavCtllimit_Internalname), ",", "."));
            }
            dynavContratada_codigo.Name = dynavContratada_codigo_Internalname;
            dynavContratada_codigo.CurrentValue = cgiGet( dynavContratada_codigo_Internalname);
            AV33Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0)));
            dynavServico_codigo.Name = dynavServico_codigo_Internalname;
            dynavServico_codigo.CurrentValue = cgiGet( dynavServico_codigo_Internalname);
            AV34Servico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0)));
            /* Read saved values. */
            nRC_GXsfl_120 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_120"), ",", "."));
            AV70SistemasValidados = StringUtil.StrToBool( cgiGet( "vSISTEMASVALIDADOS"));
            nRC_GXsfl_120 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_120"), ",", "."));
            nGXsfl_120_fel_idx = 0;
            while ( nGXsfl_120_fel_idx < nRC_GXsfl_120 )
            {
               nGXsfl_120_fel_idx = (short)(((subGrid2_Islastpage==1)&&(nGXsfl_120_fel_idx+1>subGrid2_Recordsperpage( )) ? 1 : nGXsfl_120_fel_idx+1));
               sGXsfl_120_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_120_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_1202( ) ;
               GXCCtl = "GRID1_Collapsed_" + sGXsfl_120_fel_idx;
               subGrid1_Collapsed = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
               AV76GXV3 = nGXsfl_120_fel_idx;
               if ( ( AV7SDT_Issues.gxTpr_Issues.Count >= AV76GXV3 ) && ( AV76GXV3 > 0 ) )
               {
                  AV7SDT_Issues.gxTpr_Issues.CurrentItem = ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3));
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid1_Internalname), ",", "."));
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Project.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid2_Internalname), ",", "."));
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Project.gxTpr_Name = cgiGet( edtavCtlname1_Internalname);
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Tracker.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid3_Internalname), ",", "."));
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Tracker.gxTpr_Name = cgiGet( edtavCtlname2_Internalname);
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Status.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlname3_Internalname), ",", "."));
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Status.gxTpr_Name = cgiGet( edtavCtlname7_Internalname);
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Start_date = cgiGet( edtavCtlstart_date_Internalname);
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Assigned_to.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid7_Internalname), ",", "."));
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Priority.gxTpr_Name = cgiGet( edtavCtlname4_Internalname);
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Due_date = cgiGet( edtavCtldue_date_Internalname);
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Assigned_to.gxTpr_Name = cgiGet( edtavCtlname6_Internalname);
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Author.gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid6_Internalname), ",", "."));
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Author.gxTpr_Name = cgiGet( edtavCtlname5_Internalname);
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Subject = cgiGet( edtavCtlsubject_Internalname);
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Description = cgiGet( edtavCtldescription_Internalname);
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Done_ratio = (short)(context.localUtil.CToN( cgiGet( edtavCtldone_ratio_Internalname), ",", "."));
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Estimated_hours = cgiGet( edtavCtlestimated_hours_Internalname);
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Created_on = cgiGet( edtavCtlcreated_on_Internalname);
                  ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Updated_on = cgiGet( edtavCtlupdated_on_Internalname);
               }
               GXCCtl = "nRC_GXsfl_202_" + sGXsfl_120_fel_idx;
               nRC_GXsfl_202 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
               nGXsfl_202_fel_idx = 0;
               while ( nGXsfl_202_fel_idx < nRC_GXsfl_202 )
               {
                  nGXsfl_202_fel_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_202_fel_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_202_fel_idx+1));
                  sGXsfl_202_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_202_fel_idx), 4, 0)), 4, "0") + sGXsfl_120_fel_idx;
                  SubsflControlProps_fel_20210( ) ;
                  AV95GXV22 = nGXsfl_202_fel_idx;
                  if ( ( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Count >= AV95GXV22 ) && ( AV95GXV22 > 0 ) )
                  {
                     ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.CurrentItem = ((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV95GXV22));
                     ((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV95GXV22)).gxTpr_Id = (short)(context.localUtil.CToN( cgiGet( edtavCtlid_Internalname), ",", "."));
                     ((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV95GXV22)).gxTpr_Name = cgiGet( edtavCtlname_Internalname);
                     ((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV95GXV22)).gxTpr_Value = cgiGet( edtavCtlvalue_Internalname);
                  }
               }
               if ( nGXsfl_202_fel_idx == 0 )
               {
                  nGXsfl_202_idx = 1;
                  sGXsfl_202_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_202_idx), 4, 0)), 4, "0") + sGXsfl_120_fel_idx;
                  SubsflControlProps_20210( ) ;
               }
               nGXsfl_202_fel_idx = 1;
            }
            if ( nGXsfl_120_fel_idx == 0 )
            {
               nGXsfl_120_idx = 1;
               sGXsfl_120_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_120_idx), 4, 0)), 4, "0");
               SubsflControlProps_1202( ) ;
            }
            nGXsfl_120_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E15KD2 */
         E15KD2 ();
         if (returnInSub) return;
      }

      protected void E15KD2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV50WWPContext) ;
         /* Using cursor H00KD5 */
         pr_default.execute(3, new Object[] {AV50WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A830AreaTrabalho_ServicoPadrao = H00KD5_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = H00KD5_n830AreaTrabalho_ServicoPadrao[0];
            A29Contratante_Codigo = H00KD5_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00KD5_n29Contratante_Codigo[0];
            A632Servico_Ativo = H00KD5_A632Servico_Ativo[0];
            n632Servico_Ativo = H00KD5_n632Servico_Ativo[0];
            A5AreaTrabalho_Codigo = H00KD5_A5AreaTrabalho_Codigo[0];
            A632Servico_Ativo = H00KD5_A632Servico_Ativo[0];
            n632Servico_Ativo = H00KD5_n632Servico_Ativo[0];
            AV67Contratante_Codigo = A29Contratante_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67Contratante_Codigo), 6, 0)));
            /* Using cursor H00KD6 */
            pr_default.execute(4, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A1384Redmine_ContratanteCod = H00KD6_A1384Redmine_ContratanteCod[0];
               A1905Redmine_User = H00KD6_A1905Redmine_User[0];
               n1905Redmine_User = H00KD6_n1905Redmine_User[0];
               A1904Redmine_Secure = H00KD6_A1904Redmine_Secure[0];
               A1381Redmine_Host = H00KD6_A1381Redmine_Host[0];
               A1382Redmine_Url = H00KD6_A1382Redmine_Url[0];
               A1383Redmine_Key = H00KD6_A1383Redmine_Key[0];
               A1391Redmine_Versao = H00KD6_A1391Redmine_Versao[0];
               n1391Redmine_Versao = H00KD6_n1391Redmine_Versao[0];
               A1906Redmine_CampoSistema = H00KD6_A1906Redmine_CampoSistema[0];
               n1906Redmine_CampoSistema = H00KD6_n1906Redmine_CampoSistema[0];
               A1907Redmine_CampoServico = H00KD6_A1907Redmine_CampoServico[0];
               n1907Redmine_CampoServico = H00KD6_n1907Redmine_CampoServico[0];
               AV47Usr_Nome = A1905Redmine_User;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Usr_Nome", AV47Usr_Nome);
               AV63Secure = A1904Redmine_Secure;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Secure", StringUtil.Str( (decimal)(AV63Secure), 1, 0));
               AV27Host = A1381Redmine_Host;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Host", AV27Host);
               AV28Url = A1382Redmine_Url;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Url", AV28Url);
               AV10Key = A1383Redmine_Key;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Key", AV10Key);
               AV44Versao = A1391Redmine_Versao;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Versao", AV44Versao);
               AV66Campo_Sistemas = A1906Redmine_CampoSistema;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Campo_Sistemas", AV66Campo_Sistemas);
               AV65Campo_Servicos = A1907Redmine_CampoServico;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Campo_Servicos", AV65Campo_Servicos);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(4);
            }
            pr_default.close(4);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV44Versao)) )
         {
            AV63Secure = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Secure", StringUtil.Str( (decimal)(AV63Secure), 1, 0));
            AV44Versao = "223";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Versao", AV44Versao);
         }
         else
         {
            AV44Versao = StringUtil.StringReplace( AV44Versao, ".", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Versao", AV44Versao);
         }
         tblTable2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTable2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTable2_Visible), 5, 0)));
         bttBtnimportar_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnimportar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnimportar_Visible), 5, 0)));
         bttBtnconsultar_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconsultar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconsultar_Visible), 5, 0)));
         bttBtndeparasistemas_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtndeparasistemas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndeparasistemas_Visible), 5, 0)));
         if ( AV50WWPContext.gxTpr_Contratada_codigo > 0 )
         {
            AV33Contratada_Codigo = AV50WWPContext.gxTpr_Contratada_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0)));
         }
         AV48Usr_Senha = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Usr_Senha", AV48Usr_Senha);
      }

      protected void E11KD2( )
      {
         /* 'CarregarFiltros' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV47Usr_Nome)) && String.IsNullOrEmpty(StringUtil.RTrim( AV48Usr_Senha)) )
         {
            GX_msglist.addItem("Informe Usu�rio e Senha para o acesso!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV47Usr_Nome)) )
         {
            GX_msglist.addItem("Informe o Usu�rio para o acesso!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48Usr_Senha)) )
         {
            GX_msglist.addItem("Informe a Senha para o acesso!");
         }
         else
         {
            /* Execute user subroutine: 'UPDATECADASTRO' */
            S112 ();
            if (returnInSub) return;
            /* Execute user subroutine: 'CARREGARFILTROS' */
            S122 ();
            if (returnInSub) return;
         }
         cmbavStatus.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8Status), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatus_Internalname, "Values", cmbavStatus.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV16StatusItem", AV16StatusItem);
         cmbavProjeto.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6Projeto), 18, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_Internalname, "Values", cmbavProjeto.ToJavascriptSource());
         cmbavTracker.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26Tracker), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTracker_Internalname, "Values", cmbavTracker.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25TrackersItem", AV25TrackersItem);
      }

      protected void E12KD2( )
      {
         AV76GXV3 = nGXsfl_120_idx;
         if ( AV7SDT_Issues.gxTpr_Issues.Count >= AV76GXV3 )
         {
            AV7SDT_Issues.gxTpr_Issues.CurrentItem = ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3));
         }
         /* 'btnConsulta' Routine */
         if ( (0==AV6Projeto) && (0==AV26Tracker) && (0==AV8Status) )
         {
            GX_msglist.addItem("Informe projeto e/ou tracker e/ou estado para melhorar a sua consulta!");
         }
         else
         {
            bttBtnimportar_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnimportar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnimportar_Visible), 5, 0)));
            AV7SDT_Issues.gxTpr_Issues.Clear();
            gx_BV120 = true;
            AV58pagina = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58pagina", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58pagina), 4, 0)));
            AV71offset = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71offset", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71offset), 4, 0)));
            AV53i = 1;
            AV60Count = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Count", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Count), 4, 0)));
            while ( true )
            {
               /* Execute user subroutine: 'EXECUTEDACONSULTA' */
               S132 ();
               if (returnInSub) return;
               AV5httpclient.Execute("GET", AV9Execute);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Execute", AV9Execute);
               if ( AV5httpclient.ErrCode == 0 )
               {
                  AV59SDT_Consulta.FromXml(AV5httpclient.ToString(), "");
                  if ( AV59SDT_Consulta.gxTpr_Issues.Count > 0 )
                  {
                     AV61c = 0;
                     AV103GXV25 = 1;
                     while ( AV103GXV25 <= AV59SDT_Consulta.gxTpr_Issues.Count )
                     {
                        AV55Issue = ((SdtSDT_RedmineIssues_issue)AV59SDT_Consulta.gxTpr_Issues.Item(AV103GXV25));
                        AV104GXLvl81 = 0;
                        /* Using cursor H00KD7 */
                        pr_default.execute(5, new Object[] {AV55Issue.gxTpr_Id});
                        while ( (pr_default.getStatus(5) != 101) )
                        {
                           A1389ContagemResultado_RdmnIssueId = H00KD7_A1389ContagemResultado_RdmnIssueId[0];
                           n1389ContagemResultado_RdmnIssueId = H00KD7_n1389ContagemResultado_RdmnIssueId[0];
                           A1392ContagemResultado_RdmnUpdated = H00KD7_A1392ContagemResultado_RdmnUpdated[0];
                           n1392ContagemResultado_RdmnUpdated = H00KD7_n1392ContagemResultado_RdmnUpdated[0];
                           AV104GXLvl81 = 1;
                           AV62Updated = context.localUtil.CToT( AV55Issue.gxTpr_Updated_on, 2);
                           if ( AV62Updated <= A1392ContagemResultado_RdmnUpdated )
                           {
                              /* Exit For each command. Update data (if necessary), close cursors & exit. */
                              if (true) break;
                           }
                           else
                           {
                              AV7SDT_Issues.gxTpr_Issues.Add(AV55Issue, 0);
                              gx_BV120 = true;
                              AV61c = (short)(AV61c+1);
                           }
                           pr_default.readNext(5);
                        }
                        pr_default.close(5);
                        if ( AV104GXLvl81 == 0 )
                        {
                           AV7SDT_Issues.gxTpr_Issues.Add(AV55Issue, 0);
                           gx_BV120 = true;
                           AV61c = (short)(AV61c+1);
                        }
                        AV103GXV25 = (int)(AV103GXV25+1);
                     }
                     AV60Count = (short)(AV60Count+AV61c);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Count", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Count), 4, 0)));
                  }
                  else
                  {
                     if (true) break;
                  }
               }
               else
               {
                  if (true) break;
               }
               if ( AV53i * 100 > AV59SDT_Consulta.gxTpr_Issues.Count )
               {
                  if (true) break;
               }
               else
               {
                  AV71offset = (short)(AV71offset+100);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71offset", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71offset), 4, 0)));
                  AV53i = (short)(AV53i+1);
               }
            }
            if ( AV5httpclient.ErrCode == 0 )
            {
               GX_msglist.addItem(StringUtil.StringReplace( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0), StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0), ""));
               /* Execute user subroutine: 'PROJETOSEMDEPARA' */
               S142 ();
               if (returnInSub) return;
               /* Execute user subroutine: 'SERVICOSEMDEPARA' */
               S152 ();
               if (returnInSub) return;
               AV54SemDePara = (bool)((AV51ProjetoSemDePara.Count>0)||((AV56TrackersSemDePara.Count>0)&&(AV57StatusSemDePara.Count>0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54SemDePara", AV54SemDePara);
               bttBtndeparasistemas_Visible = ((AV51ProjetoSemDePara.Count>0) ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtndeparasistemas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndeparasistemas_Visible), 5, 0)));
               bttBtnimportar_Caption = "Importar "+StringUtil.Trim( StringUtil.Str( (decimal)(AV60Count), 4, 0))+" demandas";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnimportar_Internalname, "Caption", bttBtnimportar_Caption);
            }
            else
            {
               GX_msglist.addItem(AV5httpclient.ErrDescription);
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7SDT_Issues", AV7SDT_Issues);
         nGXsfl_120_bak_idx = nGXsfl_120_idx;
         gxgrGrid2_refresh( ) ;
         nGXsfl_120_idx = nGXsfl_120_bak_idx;
         sGXsfl_120_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_120_idx), 4, 0)), 4, "0");
         SubsflControlProps_1202( ) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV59SDT_Consulta", AV59SDT_Consulta);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV55Issue", AV55Issue);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51ProjetoSemDePara", AV51ProjetoSemDePara);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14ProjectItem", AV14ProjectItem);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV56TrackersSemDePara", AV56TrackersSemDePara);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57StatusSemDePara", AV57StatusSemDePara);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25TrackersItem", AV25TrackersItem);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV16StatusItem", AV16StatusItem);
      }

      protected void E13KD2( )
      {
         AV76GXV3 = nGXsfl_120_idx;
         if ( AV7SDT_Issues.gxTpr_Issues.Count >= AV76GXV3 )
         {
            AV7SDT_Issues.gxTpr_Issues.CurrentItem = ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3));
         }
         /* 'Importar' Routine */
         if ( (0==AV8Status) )
         {
            GX_msglist.addItem("Informe o Estado das OS a importar!");
         }
         else if ( (0==AV33Contratada_Codigo) )
         {
            GX_msglist.addItem("Informe a Prestadora dos servi�os a importar!");
         }
         else if ( (0==AV34Servico_Codigo) )
         {
            GX_msglist.addItem("Informe o Servi�os das demandas a importar!");
         }
         else
         {
            AV70SistemasValidados = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70SistemasValidados", AV70SistemasValidados);
            AV69ServicosValidados = false;
            AV45WebSession.Set("Issues", AV7SDT_Issues.ToXml(false, true, "issues", ""));
            if ( StringUtil.StrCmp(AV44Versao, "223") == 0 )
            {
               context.PopUp(formatLink("aprc_processarissuesvs223.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV27Host)) + "," + UrlEncode(StringUtil.RTrim(AV28Url)) + "," + UrlEncode(StringUtil.RTrim(AV10Key)) + "," + UrlEncode("" +AV33Contratada_Codigo) + "," + UrlEncode("" +AV34Servico_Codigo) + "," + UrlEncode("" +AV50WWPContext.gxTpr_Userid), new Object[] {""});
            }
            else
            {
               GX_msglist.addItem("Vers�o do Redmine n�o liberada!");
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50WWPContext", AV50WWPContext);
      }

      protected void S162( )
      {
         /* 'GETSTATUS' Routine */
         AV5httpclient.Host = StringUtil.Trim( AV27Host);
         AV5httpclient.BaseURL = StringUtil.Trim( AV28Url);
         AV9Execute = "issue_statuses.xml";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Execute", AV9Execute);
         AV5httpclient.Secure = AV63Secure;
         AV5httpclient.AddAuthentication(0, "", AV47Usr_Nome, AV48Usr_Senha);
         AV5httpclient.Execute("GET", AV9Execute);
         if ( AV5httpclient.ErrCode == 0 )
         {
            AV15SDT_Status.FromXml(AV5httpclient.ToString(), "");
            AV15SDT_Status.gxTpr_Issue_statuss.Sort("name") ;
            cmbavStatus.removeAllItems();
            cmbavStatus.addItem("0", "(Todos)", 0);
            AV105GXV26 = 1;
            while ( AV105GXV26 <= AV15SDT_Status.gxTpr_Issue_statuss.Count )
            {
               AV16StatusItem = ((SdtSDT_RedmineStatus_issue_status)AV15SDT_Status.gxTpr_Issue_statuss.Item(AV105GXV26));
               cmbavStatus.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV16StatusItem.gxTpr_Id), 4, 0)), AV16StatusItem.gxTpr_Name, 0);
               AV105GXV26 = (int)(AV105GXV26+1);
            }
         }
         else
         {
            GX_msglist.addItem(AV5httpclient.ErrDescription);
         }
      }

      protected void S172( )
      {
         /* 'GETTRACKERS' Routine */
         AV5httpclient.Host = StringUtil.Trim( AV27Host);
         AV5httpclient.BaseURL = StringUtil.Trim( AV28Url);
         AV9Execute = "trackers.xml";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Execute", AV9Execute);
         AV5httpclient.Secure = AV63Secure;
         AV5httpclient.AddAuthentication(0, "", AV47Usr_Nome, AV48Usr_Senha);
         AV5httpclient.Execute("GET", AV9Execute);
         if ( AV5httpclient.ErrCode == 0 )
         {
            AV17SDT_Trackers.FromXml(AV5httpclient.ToString(), "");
            AV17SDT_Trackers.gxTpr_Trackers.Sort("name") ;
            cmbavTracker.removeAllItems();
            cmbavTracker.addItem("0", "(Todos)", 0);
            AV106GXV27 = 1;
            while ( AV106GXV27 <= AV17SDT_Trackers.gxTpr_Trackers.Count )
            {
               AV25TrackersItem = ((SdtSDT_RedmineTrackers_tracker)AV17SDT_Trackers.gxTpr_Trackers.Item(AV106GXV27));
               cmbavTracker.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV25TrackersItem.gxTpr_Id), 4, 0)), AV25TrackersItem.gxTpr_Name, 0);
               AV106GXV27 = (int)(AV106GXV27+1);
            }
         }
         else
         {
            GX_msglist.addItem(AV5httpclient.ErrDescription);
         }
      }

      protected void S122( )
      {
         /* 'CARREGARFILTROS' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Host)) )
         {
            /* Execute user subroutine: 'GETSTATUS' */
            S162 ();
            if (returnInSub) return;
            if ( AV5httpclient.ErrCode == 0 )
            {
               /* Execute user subroutine: 'CARREGARUSUARIO' */
               S182 ();
               if (returnInSub) return;
               if ( AV5httpclient.ErrCode == 0 )
               {
                  /* Execute user subroutine: 'GETTRACKERS' */
                  S172 ();
                  if (returnInSub) return;
               }
            }
            else
            {
               GX_msglist.addItem(AV5httpclient.ErrDescription);
            }
         }
      }

      protected void S182( )
      {
         /* 'CARREGARUSUARIO' Routine */
         AV5httpclient.Host = StringUtil.Trim( AV27Host);
         AV5httpclient.BaseURL = StringUtil.Trim( AV28Url);
         AV9Execute = "users/current.xml?&include=memberships,groups";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Execute", AV9Execute);
         AV5httpclient.Secure = AV63Secure;
         AV5httpclient.AddAuthentication(0, "", AV47Usr_Nome, AV48Usr_Senha);
         AV5httpclient.Execute("GET", AV9Execute);
         if ( AV5httpclient.ErrCode == 0 )
         {
            AV39SDT_User.FromXml(AV5httpclient.ToString(), "");
            AV39SDT_User.gxTpr_Memberships.gxTpr_Memberships.Sort("Project.Name") ;
            cmbavProjeto.removeAllItems();
            cmbavProjeto.addItem("0", "(Todos)", 0);
            AV107GXV28 = 1;
            while ( AV107GXV28 <= AV39SDT_User.gxTpr_Memberships.gxTpr_Memberships.Count )
            {
               AV49User = ((SdtSDT_Redmineuser_memberships_membership)AV39SDT_User.gxTpr_Memberships.gxTpr_Memberships.Item(AV107GXV28));
               cmbavProjeto.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV49User.gxTpr_Project.gxTpr_Id), 18, 0)), AV49User.gxTpr_Project.gxTpr_Name, 0);
               AV107GXV28 = (int)(AV107GXV28+1);
            }
         }
         else
         {
            GX_msglist.addItem(AV5httpclient.ErrDescription);
         }
         bttBtnconsultar_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconsultar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconsultar_Visible), 5, 0)));
      }

      protected void S142( )
      {
         /* 'PROJETOSEMDEPARA' Routine */
         AV51ProjetoSemDePara.Clear();
         AV108GXV29 = 1;
         while ( AV108GXV29 <= AV7SDT_Issues.gxTpr_Issues.Count )
         {
            AV55Issue = ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV108GXV29));
            AV109GXLvl292 = 0;
            /* Using cursor H00KD8 */
            pr_default.execute(6, new Object[] {AV55Issue.gxTpr_Project.gxTpr_Id});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A1462SistemaDePara_OrigemId = H00KD8_A1462SistemaDePara_OrigemId[0];
               A1461SistemaDePara_Origem = H00KD8_A1461SistemaDePara_Origem[0];
               AV109GXLvl292 = 1;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(6);
            }
            pr_default.close(6);
            if ( AV109GXLvl292 == 0 )
            {
               AV14ProjectItem.gxTpr_Project.gxTpr_Id = AV55Issue.gxTpr_Project.gxTpr_Id;
               AV14ProjectItem.gxTpr_Project.gxTpr_Name = AV55Issue.gxTpr_Project.gxTpr_Name;
               AV51ProjetoSemDePara.Add(AV14ProjectItem, 0);
               AV14ProjectItem = new SdtSDT_Redmineuser_memberships_membership(context);
            }
            AV108GXV29 = (int)(AV108GXV29+1);
         }
      }

      protected void S152( )
      {
         /* 'SERVICOSEMDEPARA' Routine */
         AV56TrackersSemDePara.Clear();
         AV57StatusSemDePara.Clear();
         AV110GXV30 = 1;
         while ( AV110GXV30 <= AV7SDT_Issues.gxTpr_Issues.Count )
         {
            AV55Issue = ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV110GXV30));
            AV111GXLvl314 = 0;
            /* Using cursor H00KD9 */
            pr_default.execute(7, new Object[] {AV55Issue.gxTpr_Tracker.gxTpr_Id, AV55Issue.gxTpr_Status.gxTpr_Id});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A1469ContratoServicosDePara_OrigenId2 = H00KD9_A1469ContratoServicosDePara_OrigenId2[0];
               n1469ContratoServicosDePara_OrigenId2 = H00KD9_n1469ContratoServicosDePara_OrigenId2[0];
               A1467ContratoServicosDePara_OrigenId = H00KD9_A1467ContratoServicosDePara_OrigenId[0];
               n1467ContratoServicosDePara_OrigenId = H00KD9_n1467ContratoServicosDePara_OrigenId[0];
               A1466ContratoServicosDePara_Origem = H00KD9_A1466ContratoServicosDePara_Origem[0];
               AV111GXLvl314 = 1;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(7);
            }
            pr_default.close(7);
            if ( AV111GXLvl314 == 0 )
            {
               AV112GXLvl320 = 0;
               /* Using cursor H00KD10 */
               pr_default.execute(8, new Object[] {AV55Issue.gxTpr_Tracker.gxTpr_Id});
               while ( (pr_default.getStatus(8) != 101) )
               {
                  A1469ContratoServicosDePara_OrigenId2 = H00KD10_A1469ContratoServicosDePara_OrigenId2[0];
                  n1469ContratoServicosDePara_OrigenId2 = H00KD10_n1469ContratoServicosDePara_OrigenId2[0];
                  A1467ContratoServicosDePara_OrigenId = H00KD10_A1467ContratoServicosDePara_OrigenId[0];
                  n1467ContratoServicosDePara_OrigenId = H00KD10_n1467ContratoServicosDePara_OrigenId[0];
                  A1466ContratoServicosDePara_Origem = H00KD10_A1466ContratoServicosDePara_Origem[0];
                  AV112GXLvl320 = 1;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(8);
               }
               pr_default.close(8);
               if ( AV112GXLvl320 == 0 )
               {
                  AV54SemDePara = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54SemDePara", AV54SemDePara);
                  AV113GXLvl327 = 0;
                  /* Using cursor H00KD11 */
                  pr_default.execute(9, new Object[] {AV55Issue.gxTpr_Status.gxTpr_Id});
                  while ( (pr_default.getStatus(9) != 101) )
                  {
                     A1469ContratoServicosDePara_OrigenId2 = H00KD11_A1469ContratoServicosDePara_OrigenId2[0];
                     n1469ContratoServicosDePara_OrigenId2 = H00KD11_n1469ContratoServicosDePara_OrigenId2[0];
                     A1467ContratoServicosDePara_OrigenId = H00KD11_A1467ContratoServicosDePara_OrigenId[0];
                     n1467ContratoServicosDePara_OrigenId = H00KD11_n1467ContratoServicosDePara_OrigenId[0];
                     A1466ContratoServicosDePara_Origem = H00KD11_A1466ContratoServicosDePara_Origem[0];
                     AV113GXLvl327 = 1;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(9);
                  }
                  pr_default.close(9);
                  if ( AV113GXLvl327 == 0 )
                  {
                     AV54SemDePara = true;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54SemDePara", AV54SemDePara);
                  }
               }
            }
            if ( AV54SemDePara )
            {
               AV54SemDePara = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54SemDePara", AV54SemDePara);
               AV25TrackersItem.gxTpr_Id = AV55Issue.gxTpr_Tracker.gxTpr_Id;
               AV25TrackersItem.gxTpr_Name = AV55Issue.gxTpr_Tracker.gxTpr_Name;
               AV56TrackersSemDePara.Add(AV25TrackersItem, 0);
               AV25TrackersItem = new SdtSDT_RedmineTrackers_tracker(context);
               AV16StatusItem.gxTpr_Id = AV55Issue.gxTpr_Status.gxTpr_Id;
               AV16StatusItem.gxTpr_Name = AV55Issue.gxTpr_Status.gxTpr_Name;
               AV57StatusSemDePara.Add(AV16StatusItem, 0);
               AV16StatusItem = new SdtSDT_RedmineStatus_issue_status(context);
            }
            AV110GXV30 = (int)(AV110GXV30+1);
         }
      }

      protected void E14KD2( )
      {
         AV76GXV3 = nGXsfl_120_idx;
         if ( AV7SDT_Issues.gxTpr_Issues.Count >= AV76GXV3 )
         {
            AV7SDT_Issues.gxTpr_Issues.CurrentItem = ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3));
         }
         /* 'VerificarSistemas' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66Campo_Sistemas)) )
         {
            GX_msglist.addItem("Informe o campo de Sistemas para poder fazer a verifica��o solicitada!");
         }
         else
         {
            /* Execute user subroutine: 'UPDATECADASTRO' */
            S112 ();
            if (returnInSub) return;
            AV70SistemasValidados = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70SistemasValidados", AV70SistemasValidados);
            bttBtnimportar_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnimportar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnimportar_Visible), 5, 0)));
            AV64SDT_Parametros.gxTpr_Numeric.Clear();
            AV64SDT_Parametros.gxTpr_Character.Clear();
            AV64SDT_Parametros.gxTpr_Numeric.Add((decimal)(AV50WWPContext.gxTpr_Areatrabalho_codigo), 0);
            AV64SDT_Parametros.gxTpr_Character.Add(AV66Campo_Sistemas, 0);
            AV45WebSession.Remove("Sistemas");
            AV45WebSession.Set("Issues", AV7SDT_Issues.ToXml(false, true, "issues", ""));
            AV45WebSession.Set("Parametros", AV64SDT_Parametros.ToXml(false, true, "SDT_Parametros", "GxEv3Up14_Meetrika"));
            context.PopUp(formatLink("arel_redminesistemas.aspx") , new Object[] {});
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64SDT_Parametros", AV64SDT_Parametros);
      }

      protected void E16KD2( )
      {
         AV76GXV3 = nGXsfl_120_idx;
         if ( AV7SDT_Issues.gxTpr_Issues.Count >= AV76GXV3 )
         {
            AV7SDT_Issues.gxTpr_Issues.CurrentItem = ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3));
         }
         /* 'VerificarServicos' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65Campo_Servicos)) )
         {
            GX_msglist.addItem("Informe o campo de Servicos para poder fazer a verifica��o solicitada!");
         }
         else
         {
            /* Execute user subroutine: 'UPDATECADASTRO' */
            S112 ();
            if (returnInSub) return;
            AV69ServicosValidados = true;
            bttBtnimportar_Visible = (AV70SistemasValidados ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnimportar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnimportar_Visible), 5, 0)));
            AV64SDT_Parametros.gxTpr_Numeric.Clear();
            AV64SDT_Parametros.gxTpr_Character.Clear();
            AV64SDT_Parametros.gxTpr_Numeric.Add((decimal)(AV50WWPContext.gxTpr_Areatrabalho_codigo), 0);
            AV64SDT_Parametros.gxTpr_Numeric.Add((decimal)(AV68Contrato), 0);
            AV64SDT_Parametros.gxTpr_Character.Add(AV65Campo_Servicos, 0);
            AV45WebSession.Remove("Servicos");
            AV45WebSession.Set("Issues", AV7SDT_Issues.ToXml(false, true, "issues", ""));
            AV45WebSession.Set("Parametros", AV64SDT_Parametros.ToXml(false, true, "SDT_Parametros", "GxEv3Up14_Meetrika"));
            context.PopUp(formatLink("arel_redmineservicos.aspx") , new Object[] {});
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64SDT_Parametros", AV64SDT_Parametros);
      }

      protected void S132( )
      {
         /* 'EXECUTEDACONSULTA' Routine */
         AV5httpclient.Host = StringUtil.Trim( AV27Host);
         AV5httpclient.BaseURL = StringUtil.Trim( AV28Url);
         AV9Execute = "issues.xml?&include=journals&sort=&offset=" + StringUtil.Trim( StringUtil.Str( (decimal)(AV71offset), 4, 0)) + "&limit=100&page=" + StringUtil.Trim( StringUtil.Str( (decimal)(AV58pagina), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Execute", AV9Execute);
         if ( ! AV50WWPContext.gxTpr_Userehadministradorgam )
         {
            AV9Execute = AV9Execute + "&assigned_to_id=me";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Execute", AV9Execute);
         }
         if ( ! (0==AV6Projeto) )
         {
            AV9Execute = AV9Execute + "&project_id=" + StringUtil.Trim( StringUtil.Str( (decimal)(AV6Projeto), 18, 0));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Execute", AV9Execute);
         }
         if ( ! (0==AV8Status) )
         {
            AV9Execute = AV9Execute + "&status_id=" + StringUtil.Trim( StringUtil.Str( (decimal)(AV8Status), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Execute", AV9Execute);
         }
         if ( ! (0==AV26Tracker) )
         {
            AV9Execute = AV9Execute + "&tracker_id=" + StringUtil.Trim( StringUtil.Str( (decimal)(AV26Tracker), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Execute", AV9Execute);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10Key)) )
         {
            AV9Execute = AV9Execute + "&key=" + StringUtil.Trim( AV10Key);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Execute", AV9Execute);
         }
         if ( ! (DateTime.MinValue==AV40Created_From) )
         {
            AV9Execute = AV9Execute + "&created_on=" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( AV40Created_From)), 10, 0)) + "-" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( AV40Created_From)), 10, 0)) + "-" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( AV40Created_From)), 10, 0)) + "|";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Execute", AV9Execute);
         }
         if ( ! (DateTime.MinValue==AV41Created_To) )
         {
            if ( (DateTime.MinValue==AV40Created_From) )
            {
               AV9Execute = AV9Execute + "|";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Execute", AV9Execute);
            }
            AV9Execute = AV9Execute + "&created_on=" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( AV41Created_To)), 10, 0)) + "-" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( AV41Created_To)), 10, 0)) + "-" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( AV41Created_To)), 10, 0)) + "|";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Execute", AV9Execute);
         }
         AV5httpclient.Secure = AV63Secure;
         AV5httpclient.AddAuthentication(0, "", AV47Usr_Nome, AV48Usr_Senha);
      }

      protected void S112( )
      {
         /* 'UPDATECADASTRO' Routine */
         new prc_updredmine(context ).execute( ref  AV67Contratante_Codigo,  AV47Usr_Nome,  AV63Secure,  AV27Host,  AV28Url,  AV10Key,  AV44Versao,  AV66Campo_Sistemas,  AV65Campo_Servicos) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67Contratante_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Usr_Nome", AV47Usr_Nome);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Secure", StringUtil.Str( (decimal)(AV63Secure), 1, 0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Host", AV27Host);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Url", AV28Url);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Key", AV10Key);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Versao", AV44Versao);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Campo_Sistemas", AV66Campo_Sistemas);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Campo_Servicos", AV65Campo_Servicos);
      }

      private void E17KD2( )
      {
         /* Grid2_Load Routine */
         AV76GXV3 = 1;
         while ( AV76GXV3 <= AV7SDT_Issues.gxTpr_Issues.Count )
         {
            AV7SDT_Issues.gxTpr_Issues.CurrentItem = ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 120;
            }
            sendrow_1202( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_120_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(120, Grid2Row);
            }
            AV76GXV3 = (short)(AV76GXV3+1);
         }
      }

      private void E18KD10( )
      {
         /* Grid1_Load Routine */
         AV95GXV22 = 1;
         while ( AV95GXV22 <= ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Count )
         {
            ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.CurrentItem = ((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV95GXV22));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 202;
            }
            sendrow_20210( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_202_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(202, Grid1Row);
            }
            AV95GXV22 = (short)(AV95GXV22+1);
         }
      }

      protected void wb_table2_219_KD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTable2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Contratada", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 224,'',false,'" + sGXsfl_120_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo, dynavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0)), 1, dynavContratada_codigo_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e19kd1_client"+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,224);\"", "", true, "HLP_WP_WDSLdeOS.htm");
            dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", (String)(dynavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock7_Internalname, "Servi�o", "", "", lblTextblock7_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 229,'',false,'" + sGXsfl_120_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServico_codigo, dynavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0)), 1, dynavServico_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,229);\"", "", true, "HLP_WP_WDSLdeOS.htm");
            dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_codigo_Internalname, "Values", (String)(dynavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_219_KD2e( true) ;
         }
         else
         {
            wb_table2_219_KD2e( false) ;
         }
      }

      protected void wb_table1_2_KD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(157), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"5\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  style=\""+CSSHelper.Prettify( "height:30px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"5\" >") ;
            wb_table3_10_KD2( true) ;
         }
         else
         {
            wb_table3_10_KD2( false) ;
         }
         return  ;
      }

      protected void wb_table3_10_KD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock37_Internalname, "Host:", "", "", lblTextblock37_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'" + sGXsfl_120_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSecure, cmbavSecure_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV63Secure), 1, 0)), 1, cmbavSecure_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "", true, "HLP_WP_WDSLdeOS.htm");
            cmbavSecure.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV63Secure), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSecure_Internalname, "Values", (String)(cmbavSecure.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_120_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavHost_Internalname, StringUtil.RTrim( AV27Host), StringUtil.RTrim( context.localUtil.Format( AV27Host, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavHost_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock39_Internalname, "Url:", "", "", lblTextblock39_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_120_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUrl_Internalname, StringUtil.RTrim( AV28Url), StringUtil.RTrim( context.localUtil.Format( AV28Url, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUrl_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock38_Internalname, "Key:", "", "", lblTextblock38_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_120_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavKey_Internalname, StringUtil.RTrim( AV10Key), StringUtil.RTrim( context.localUtil.Format( AV10Key, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavKey_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  style=\""+CSSHelper.Prettify( "height:20px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(120), 3, 0)+","+"null"+");", "1) Carregar Filtros do Redmine", bttButton2_Jsonclick, 5, "1) Carregar Filtros do Redmine", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CARREGARFILTROS\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  style=\""+CSSHelper.Prettify( "height:25px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock34_Internalname, "Projeto:", "", "", lblTextblock34_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_120_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavProjeto, cmbavProjeto_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV6Projeto), 18, 0)), 1, cmbavProjeto_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e20kd1_client"+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_WP_WDSLdeOS.htm");
            cmbavProjeto.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6Projeto), 18, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavProjeto_Internalname, "Values", (String)(cmbavProjeto.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock41_Internalname, "Para a Prestadora:", "", "", lblTextblock41_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_120_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo, dynavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0)), 1, dynavContratada_codigo_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e19kd1_client"+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_WP_WDSLdeOS.htm");
            dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV33Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", (String)(dynavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock36_Internalname, "Tracker:", "", "", lblTextblock36_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_120_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavTracker, cmbavTracker_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26Tracker), 4, 0)), 1, cmbavTracker_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e21kd1_client"+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_WP_WDSLdeOS.htm");
            cmbavTracker.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26Tracker), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTracker_Internalname, "Values", (String)(cmbavTracker.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock44_Internalname, "Contrato:", "", "", lblTextblock44_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_120_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContrato, dynavContrato_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV68Contrato), 6, 0)), 1, dynavContrato_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_WP_WDSLdeOS.htm");
            dynavContrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV68Contrato), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContrato_Internalname, "Values", (String)(dynavContrato.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock47_Internalname, "Servi�o:", "", "", lblTextblock47_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_120_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServico_codigo, dynavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0)), 1, dynavServico_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_WP_WDSLdeOS.htm");
            dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_codigo_Internalname, "Values", (String)(dynavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock35_Internalname, "Estado:", "", "", lblTextblock35_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_120_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavStatus, cmbavStatus_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV8Status), 4, 0)), 1, cmbavStatus_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e22kd1_client"+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", "", true, "HLP_WP_WDSLdeOS.htm");
            cmbavStatus.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8Status), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatus_Internalname, "Values", (String)(cmbavStatus.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnimportar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(120), 3, 0)+","+"null"+");", bttBtnimportar_Caption, bttBtnimportar_Jsonclick, 5, "4) Importar demandas", "", StyleString, ClassString, bttBtnimportar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'IMPORTAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock42_Internalname, "Periodo:", "", "", lblTextblock42_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_120_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavCreated_from_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavCreated_from_Internalname, context.localUtil.TToC( AV40Created_From, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV40Created_From, "99/99/9999 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCreated_from_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 16, "chr", 1, "row", 16, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_WDSLdeOS.htm");
            GxWebStd.gx_bitmap( context, edtavCreated_from_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_120_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavCreated_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavCreated_to_Internalname, context.localUtil.TToC( AV41Created_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV41Created_To, "99/99/9999 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCreated_to_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 16, "chr", 1, "row", 16, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_WDSLdeOS.htm");
            GxWebStd.gx_bitmap( context, edtavCreated_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock45_Internalname, "Campo de Sistemas:", "", "", lblTextblock45_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_120_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCampo_sistemas_Internalname, StringUtil.RTrim( AV66Campo_Sistemas), StringUtil.RTrim( context.localUtil.Format( AV66Campo_Sistemas, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCampo_sistemas_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            context.WriteHtmlText( "<p>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconsultar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(120), 3, 0)+","+"null"+");", "2) Consultar", bttBtnconsultar_Jsonclick, 5, "2) Consultar", "", StyleString, ClassString, bttBtnconsultar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'BTNCONSULTA\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "&nbsp;") ;
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock46_Internalname, "Campo de Servi�os:", "", "", lblTextblock46_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_120_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCampo_servicos_Internalname, StringUtil.RTrim( AV65Campo_Servicos), StringUtil.RTrim( context.localUtil.Format( AV65Campo_Servicos, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCampo_servicos_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp; ") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndeparasistemas_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(120), 3, 0)+","+"null"+");", "3) Verificar Sistemas", bttBtndeparasistemas_Jsonclick, 5, "3) Verificar Sistemas", "", StyleString, ClassString, bttBtndeparasistemas_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'VERIFICARSISTEMAS\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  style=\""+CSSHelper.Prettify( "height:25px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"4\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:16px")+"\">") ;
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            wb_table4_96_KD2( true) ;
         }
         else
         {
            wb_table4_96_KD2( false) ;
         }
         return  ;
      }

      protected void wb_table4_96_KD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_114_KD2( true) ;
         }
         else
         {
            wb_table5_114_KD2( false) ;
         }
         return  ;
      }

      protected void wb_table5_114_KD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_KD2e( true) ;
         }
         else
         {
            wb_table1_2_KD2e( false) ;
         }
      }

      protected void wb_table5_114_KD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable9_Internalname, tblTable9_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /*  Grid Control  */
            Grid2Container.SetIsFreestyle(true);
            Grid2Container.SetWrapped(nGXWrapped);
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid2Container"+"DivS\" data-gxgridid=\"120\">") ;
               sStyleString = "";
               sStyleString = sStyleString + "border:" + StringUtil.Str( (decimal)(1), 3, 0) + "px solid;";
               if ( StringUtil.StrCmp(context.BuildHTMLColor( (int)(0x000000))+";", "") != 0 )
               {
                  sStyleString = sStyleString + " border-color: " + context.BuildHTMLColor( (int)(0x000000)) + ";";
               }
               GxWebStd.gx_table_start( context, subGrid2_Internalname, subGrid2_Internalname, "", "FreeStyleGrid", 0, "", "", 1, 2, sStyleString, "none", 0);
               Grid2Container.AddObjectProperty("GridName", "Grid2");
            }
            else
            {
               Grid2Container.AddObjectProperty("GridName", "Grid2");
               Grid2Container.AddObjectProperty("Bordercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)((int)(0x000000)), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Rules", StringUtil.RTrim( "none"));
               Grid2Container.AddObjectProperty("Class", "FreeStyleGrid");
               Grid2Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Backcolorstyle), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Borderwidth), 4, 0, ".", "")));
               Grid2Container.AddObjectProperty("Bordercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Bordercolor), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("CmpContext", "");
               Grid2Container.AddObjectProperty("InMasterPage", "false");
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock5_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock6_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock9_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock12_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock26_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock15_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock27_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock21_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock18_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock24_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock25_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock28_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock29_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock30_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock32_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Column.AddObjectProperty("Value", lblTextblock33_Caption);
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid2Container.AddColumnProperties(Grid2Column);
               Grid2Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowselection), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Selectioncolor), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowhovering), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Hoveringcolor), 9, 0, ".", "")));
               Grid2Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowcollapsing), 1, 0, ".", "")));
               Grid2Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 120 )
         {
            wbEnd = 0;
            nRC_GXsfl_120 = (short)(nGXsfl_120_idx-1);
            if ( Grid2Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV76GXV3 = nGXsfl_120_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid2Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid2", Grid2Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid2ContainerData", Grid2Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid2ContainerData"+"V", Grid2Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid2ContainerData"+"V"+"\" value='"+Grid2Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_114_KD2e( true) ;
         }
         else
         {
            wb_table5_114_KD2e( false) ;
         }
      }

      protected void wb_table4_96_KD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable10_Internalname, tblTable10_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock14_Internalname, "Tarefas", "", "", lblTextblock14_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_120_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCount_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60Count), 4, 0, ",", "")), ((edtavCount_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV60Count), "ZZZ9")) : context.localUtil.Format( (decimal)(AV60Count), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCount_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCount_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "offset", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavCtloffset_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7SDT_Issues.gxTpr_Offset), 2, 0, ",", "")), ((edtavCtloffset_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV7SDT_Issues.gxTpr_Offset), "Z9")) : context.localUtil.Format( (decimal)(AV7SDT_Issues.gxTpr_Offset), "Z9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtloffset_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCtloffset_Enabled, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "limit", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavCtllimit_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7SDT_Issues.gxTpr_Limit), 2, 0, ",", "")), ((edtavCtllimit_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV7SDT_Issues.gxTpr_Limit), "Z9")) : context.localUtil.Format( (decimal)(AV7SDT_Issues.gxTpr_Limit), "Z9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtllimit_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCtllimit_Enabled, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_96_KD2e( true) ;
         }
         else
         {
            wb_table4_96_KD2e( false) ;
         }
      }

      protected void wb_table3_10_KD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable5_Internalname, tblTable5_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock8_Internalname, "Usu�rio", "", "", lblTextblock8_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'" + sGXsfl_120_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsr_nome_Internalname, AV47Usr_Nome, StringUtil.RTrim( context.localUtil.Format( AV47Usr_Nome, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsr_nome_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock10_Internalname, "Senha", "", "", lblTextblock10_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_120_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsr_senha_Internalname, AV48Usr_Senha, StringUtil.RTrim( context.localUtil.Format( AV48Usr_Senha, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsr_senha_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 40, "chr", 1, "row", 40, -1, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_WDSLdeOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_10_KD2e( true) ;
         }
         else
         {
            wb_table3_10_KD2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKD2( ) ;
         WSKD2( ) ;
         WEKD2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221194374");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_wdsldeos.js", "?202031221194375");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_20210( )
      {
         edtavCtlid_Internalname = "CTLID_"+sGXsfl_202_idx;
         edtavCtlname_Internalname = "CTLNAME_"+sGXsfl_202_idx;
         edtavCtlvalue_Internalname = "CTLVALUE_"+sGXsfl_202_idx;
      }

      protected void SubsflControlProps_fel_20210( )
      {
         edtavCtlid_Internalname = "CTLID_"+sGXsfl_202_fel_idx;
         edtavCtlname_Internalname = "CTLNAME_"+sGXsfl_202_fel_idx;
         edtavCtlvalue_Internalname = "CTLVALUE_"+sGXsfl_202_fel_idx;
      }

      protected void sendrow_20210( )
      {
         SubsflControlProps_20210( ) ;
         WBKD0( ) ;
         Grid1Row = GXWebRow.GetNew(context,Grid1Container);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0xF0F0F0);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_202_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0xF5F5F5);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0xF0F0F0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid1_Linesclass+"\" style=\""+((subGrid1_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid1_Backcolor)+";")+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_202_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCtlid_Enabled!=0)&&(edtavCtlid_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 203,'',false,'"+sGXsfl_202_idx+"',202)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlid_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV95GXV22)).gxTpr_Id), 2, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV95GXV22)).gxTpr_Id), "Z9")),TempTags+((edtavCtlid_Enabled!=0)&&(edtavCtlid_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlid_Enabled!=0)&&(edtavCtlid_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,203);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlid_Jsonclick,(short)0,(String)"Attribute",((subGrid1_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid1_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(short)1,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)202,(short)1,(short)-1,(short)0,(bool)false,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCtlname_Enabled!=0)&&(edtavCtlname_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 204,'',false,'"+sGXsfl_202_idx+"',202)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV95GXV22)).gxTpr_Name),(String)"",TempTags+((edtavCtlname_Enabled!=0)&&(edtavCtlname_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlname_Enabled!=0)&&(edtavCtlname_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,204);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlname_Jsonclick,(short)0,(String)"Attribute",((subGrid1_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid1_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(short)1,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)9999,(short)0,(short)0,(short)202,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCtlvalue_Enabled!=0)&&(edtavCtlvalue_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 205,'',false,'"+sGXsfl_202_idx+"',202)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlvalue_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV95GXV22)).gxTpr_Value),(String)"",TempTags+((edtavCtlvalue_Enabled!=0)&&(edtavCtlvalue_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlvalue_Enabled!=0)&&(edtavCtlvalue_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,205);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlvalue_Jsonclick,(short)0,(String)"Attribute",((subGrid1_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid1_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(short)1,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)9999,(short)0,(short)0,(short)202,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_202_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_202_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_202_idx+1));
         sGXsfl_202_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_202_idx), 4, 0)), 4, "0") + sGXsfl_120_idx;
         SubsflControlProps_20210( ) ;
         /* End function sendrow_20210 */
      }

      protected void SubsflControlProps_1202( )
      {
         lblTextblock5_Internalname = "TEXTBLOCK5_"+sGXsfl_120_idx;
         edtavCtlid1_Internalname = "CTLID1_"+sGXsfl_120_idx;
         lblTextblock6_Internalname = "TEXTBLOCK6_"+sGXsfl_120_idx;
         edtavCtlid2_Internalname = "CTLID2_"+sGXsfl_120_idx;
         edtavCtlname1_Internalname = "CTLNAME1_"+sGXsfl_120_idx;
         lblTextblock9_Internalname = "TEXTBLOCK9_"+sGXsfl_120_idx;
         edtavCtlid3_Internalname = "CTLID3_"+sGXsfl_120_idx;
         edtavCtlname2_Internalname = "CTLNAME2_"+sGXsfl_120_idx;
         lblTextblock12_Internalname = "TEXTBLOCK12_"+sGXsfl_120_idx;
         edtavCtlname3_Internalname = "CTLNAME3_"+sGXsfl_120_idx;
         edtavCtlname7_Internalname = "CTLNAME7_"+sGXsfl_120_idx;
         lblTextblock26_Internalname = "TEXTBLOCK26_"+sGXsfl_120_idx;
         edtavCtlstart_date_Internalname = "CTLSTART_DATE_"+sGXsfl_120_idx;
         lblTextblock15_Internalname = "TEXTBLOCK15_"+sGXsfl_120_idx;
         edtavCtlid7_Internalname = "CTLID7_"+sGXsfl_120_idx;
         edtavCtlname4_Internalname = "CTLNAME4_"+sGXsfl_120_idx;
         lblTextblock27_Internalname = "TEXTBLOCK27_"+sGXsfl_120_idx;
         edtavCtldue_date_Internalname = "CTLDUE_DATE_"+sGXsfl_120_idx;
         lblTextblock21_Internalname = "TEXTBLOCK21_"+sGXsfl_120_idx;
         edtavCtlname6_Internalname = "CTLNAME6_"+sGXsfl_120_idx;
         lblTextblock18_Internalname = "TEXTBLOCK18_"+sGXsfl_120_idx;
         edtavCtlid6_Internalname = "CTLID6_"+sGXsfl_120_idx;
         edtavCtlname5_Internalname = "CTLNAME5_"+sGXsfl_120_idx;
         lblTextblock24_Internalname = "TEXTBLOCK24_"+sGXsfl_120_idx;
         edtavCtlsubject_Internalname = "CTLSUBJECT_"+sGXsfl_120_idx;
         lblTextblock25_Internalname = "TEXTBLOCK25_"+sGXsfl_120_idx;
         edtavCtldescription_Internalname = "CTLDESCRIPTION_"+sGXsfl_120_idx;
         lblTextblock28_Internalname = "TEXTBLOCK28_"+sGXsfl_120_idx;
         edtavCtldone_ratio_Internalname = "CTLDONE_RATIO_"+sGXsfl_120_idx;
         lblTextblock29_Internalname = "TEXTBLOCK29_"+sGXsfl_120_idx;
         edtavCtlestimated_hours_Internalname = "CTLESTIMATED_HOURS_"+sGXsfl_120_idx;
         lblTextblock30_Internalname = "TEXTBLOCK30_"+sGXsfl_120_idx;
         lblTextblock32_Internalname = "TEXTBLOCK32_"+sGXsfl_120_idx;
         edtavCtlcreated_on_Internalname = "CTLCREATED_ON_"+sGXsfl_120_idx;
         lblTextblock33_Internalname = "TEXTBLOCK33_"+sGXsfl_120_idx;
         edtavCtlupdated_on_Internalname = "CTLUPDATED_ON_"+sGXsfl_120_idx;
         subGrid1_Internalname = "GRID1_"+sGXsfl_120_idx;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            GXCCtl = "GRID1_Collapsed_" + sGXsfl_120_idx;
            subGrid1_Collapsed = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         }
      }

      protected void SubsflControlProps_fel_1202( )
      {
         lblTextblock5_Internalname = "TEXTBLOCK5_"+sGXsfl_120_fel_idx;
         edtavCtlid1_Internalname = "CTLID1_"+sGXsfl_120_fel_idx;
         lblTextblock6_Internalname = "TEXTBLOCK6_"+sGXsfl_120_fel_idx;
         edtavCtlid2_Internalname = "CTLID2_"+sGXsfl_120_fel_idx;
         edtavCtlname1_Internalname = "CTLNAME1_"+sGXsfl_120_fel_idx;
         lblTextblock9_Internalname = "TEXTBLOCK9_"+sGXsfl_120_fel_idx;
         edtavCtlid3_Internalname = "CTLID3_"+sGXsfl_120_fel_idx;
         edtavCtlname2_Internalname = "CTLNAME2_"+sGXsfl_120_fel_idx;
         lblTextblock12_Internalname = "TEXTBLOCK12_"+sGXsfl_120_fel_idx;
         edtavCtlname3_Internalname = "CTLNAME3_"+sGXsfl_120_fel_idx;
         edtavCtlname7_Internalname = "CTLNAME7_"+sGXsfl_120_fel_idx;
         lblTextblock26_Internalname = "TEXTBLOCK26_"+sGXsfl_120_fel_idx;
         edtavCtlstart_date_Internalname = "CTLSTART_DATE_"+sGXsfl_120_fel_idx;
         lblTextblock15_Internalname = "TEXTBLOCK15_"+sGXsfl_120_fel_idx;
         edtavCtlid7_Internalname = "CTLID7_"+sGXsfl_120_fel_idx;
         edtavCtlname4_Internalname = "CTLNAME4_"+sGXsfl_120_fel_idx;
         lblTextblock27_Internalname = "TEXTBLOCK27_"+sGXsfl_120_fel_idx;
         edtavCtldue_date_Internalname = "CTLDUE_DATE_"+sGXsfl_120_fel_idx;
         lblTextblock21_Internalname = "TEXTBLOCK21_"+sGXsfl_120_fel_idx;
         edtavCtlname6_Internalname = "CTLNAME6_"+sGXsfl_120_fel_idx;
         lblTextblock18_Internalname = "TEXTBLOCK18_"+sGXsfl_120_fel_idx;
         edtavCtlid6_Internalname = "CTLID6_"+sGXsfl_120_fel_idx;
         edtavCtlname5_Internalname = "CTLNAME5_"+sGXsfl_120_fel_idx;
         lblTextblock24_Internalname = "TEXTBLOCK24_"+sGXsfl_120_fel_idx;
         edtavCtlsubject_Internalname = "CTLSUBJECT_"+sGXsfl_120_fel_idx;
         lblTextblock25_Internalname = "TEXTBLOCK25_"+sGXsfl_120_fel_idx;
         edtavCtldescription_Internalname = "CTLDESCRIPTION_"+sGXsfl_120_fel_idx;
         lblTextblock28_Internalname = "TEXTBLOCK28_"+sGXsfl_120_fel_idx;
         edtavCtldone_ratio_Internalname = "CTLDONE_RATIO_"+sGXsfl_120_fel_idx;
         lblTextblock29_Internalname = "TEXTBLOCK29_"+sGXsfl_120_fel_idx;
         edtavCtlestimated_hours_Internalname = "CTLESTIMATED_HOURS_"+sGXsfl_120_fel_idx;
         lblTextblock30_Internalname = "TEXTBLOCK30_"+sGXsfl_120_fel_idx;
         lblTextblock32_Internalname = "TEXTBLOCK32_"+sGXsfl_120_fel_idx;
         edtavCtlcreated_on_Internalname = "CTLCREATED_ON_"+sGXsfl_120_fel_idx;
         lblTextblock33_Internalname = "TEXTBLOCK33_"+sGXsfl_120_fel_idx;
         edtavCtlupdated_on_Internalname = "CTLUPDATED_ON_"+sGXsfl_120_fel_idx;
         subGrid1_Internalname = "GRID1_"+sGXsfl_120_fel_idx;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            GXCCtl = "GRID1_Collapsed_" + sGXsfl_120_fel_idx;
            subGrid1_Collapsed = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         }
      }

      protected void sendrow_1202( )
      {
         SubsflControlProps_1202( ) ;
         WBKD0( ) ;
         Grid2Row = GXWebRow.GetNew(context,Grid2Container);
         if ( subGrid2_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid2_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Odd";
            }
         }
         else if ( subGrid2_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid2_Backstyle = 0;
            subGrid2_Backcolor = subGrid2_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Uniform";
            }
         }
         else if ( subGrid2_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid2_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Odd";
            }
            subGrid2_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid2_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid2_Backstyle = 1;
            if ( ((int)(((nGXsfl_120_idx-1)/ (decimal)(1)) % (2))) == 0 )
            {
               subGrid2_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
               {
                  subGrid2_Linesclass = subGrid2_Class+"Odd";
               }
            }
            else
            {
               subGrid2_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
               {
                  subGrid2_Linesclass = subGrid2_Class+"Even";
               }
            }
         }
         /* Start of Columns property logic. */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            if ( ( 1 == 0 ) && ( nGXsfl_120_idx == 1 ) )
            {
               context.WriteHtmlText( "<tr"+" class=\""+subGrid2_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_120_idx+"\">") ;
            }
            if ( 1 > 0 )
            {
               if ( ( 1 == 1 ) || ( ((int)((nGXsfl_120_idx) % (1))) - 1 == 0 ) )
               {
                  context.WriteHtmlText( "<tr"+" class=\""+subGrid2_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_120_idx+"\">") ;
               }
            }
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock5_Internalname,(String)"id",(String)"",(String)"",(String)lblTextblock5_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         TempTags = " " + ((edtavCtlid1_Enabled!=0)&&(edtavCtlid1_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 125,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlid1_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Id), 4, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Id), "ZZZ9")),TempTags+((edtavCtlid1_Enabled!=0)&&(edtavCtlid1_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlid1_Enabled!=0)&&(edtavCtlid1_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,125);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlid1_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)4,(String)"chr",(short)1,(String)"row",(short)4,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock6_Internalname,(String)"project",(String)"",(String)"",(String)lblTextblock6_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Single line edit */
         TempTags = " " + ((edtavCtlid2_Enabled!=0)&&(edtavCtlid2_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 129,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlid2_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Project.gxTpr_Id), 2, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Project.gxTpr_Id), "Z9")),TempTags+((edtavCtlid2_Enabled!=0)&&(edtavCtlid2_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlid2_Enabled!=0)&&(edtavCtlid2_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,129);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlid2_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)2,(String)"chr",(short)1,(String)"row",(short)2,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Single line edit */
         TempTags = " " + ((edtavCtlname1_Enabled!=0)&&(edtavCtlname1_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 130,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname1_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Project.gxTpr_Name),(String)"",TempTags+((edtavCtlname1_Enabled!=0)&&(edtavCtlname1_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlname1_Enabled!=0)&&(edtavCtlname1_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,130);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlname1_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)80,(String)"chr",(short)1,(String)"row",(short)9999,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock9_Internalname,(String)"tracker",(String)"",(String)"",(String)lblTextblock9_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Single line edit */
         TempTags = " " + ((edtavCtlid3_Enabled!=0)&&(edtavCtlid3_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 134,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlid3_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Tracker.gxTpr_Id), 2, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Tracker.gxTpr_Id), "Z9")),TempTags+((edtavCtlid3_Enabled!=0)&&(edtavCtlid3_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlid3_Enabled!=0)&&(edtavCtlid3_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,134);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlid3_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)2,(String)"chr",(short)1,(String)"row",(short)2,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Single line edit */
         TempTags = " " + ((edtavCtlname2_Enabled!=0)&&(edtavCtlname2_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 135,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname2_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Tracker.gxTpr_Name),(String)"",TempTags+((edtavCtlname2_Enabled!=0)&&(edtavCtlname2_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlname2_Enabled!=0)&&(edtavCtlname2_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,135);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlname2_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)80,(String)"chr",(short)1,(String)"row",(short)9999,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "width:35px")+"\""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock12_Internalname,(String)"Estado",(String)"",(String)"",(String)lblTextblock12_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Single line edit */
         TempTags = " " + ((edtavCtlname3_Enabled!=0)&&(edtavCtlname3_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 139,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname3_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Status.gxTpr_Id), 2, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Status.gxTpr_Id), "Z9")),TempTags+((edtavCtlname3_Enabled!=0)&&(edtavCtlname3_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlname3_Enabled!=0)&&(edtavCtlname3_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,139);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlname3_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)2,(String)"chr",(short)1,(String)"row",(short)2,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Single line edit */
         TempTags = " " + ((edtavCtlname7_Enabled!=0)&&(edtavCtlname7_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 140,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname7_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Status.gxTpr_Name),(String)"",TempTags+((edtavCtlname7_Enabled!=0)&&(edtavCtlname7_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlname7_Enabled!=0)&&(edtavCtlname7_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,140);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlname7_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)80,(String)"chr",(short)1,(String)"row",(short)9999,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock26_Internalname,(String)"Inicio",(String)"",(String)"",(String)lblTextblock26_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         TempTags = " " + ((edtavCtlstart_date_Enabled!=0)&&(edtavCtlstart_date_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 144,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlstart_date_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Start_date),(String)"",TempTags+((edtavCtlstart_date_Enabled!=0)&&(edtavCtlstart_date_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlstart_date_Enabled!=0)&&(edtavCtlstart_date_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,144);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlstart_date_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)20,(String)"chr",(short)1,(String)"row",(short)20,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock15_Internalname,(String)"Prioridade",(String)"",(String)"",(String)lblTextblock15_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Single line edit */
         TempTags = " " + ((edtavCtlid7_Enabled!=0)&&(edtavCtlid7_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 148,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlid7_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Assigned_to.gxTpr_Id), 2, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Assigned_to.gxTpr_Id), "Z9")),TempTags+((edtavCtlid7_Enabled!=0)&&(edtavCtlid7_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlid7_Enabled!=0)&&(edtavCtlid7_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,148);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlid7_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)2,(String)"chr",(short)1,(String)"row",(short)2,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Single line edit */
         TempTags = " " + ((edtavCtlname4_Enabled!=0)&&(edtavCtlname4_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 149,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname4_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Priority.gxTpr_Name),(String)"",TempTags+((edtavCtlname4_Enabled!=0)&&(edtavCtlname4_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlname4_Enabled!=0)&&(edtavCtlname4_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,149);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlname4_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)80,(String)"chr",(short)1,(String)"row",(short)9999,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock27_Internalname,(String)"Data prevista",(String)"",(String)"",(String)lblTextblock27_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         TempTags = " " + ((edtavCtldue_date_Enabled!=0)&&(edtavCtldue_date_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 153,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldue_date_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Due_date),(String)"",TempTags+((edtavCtldue_date_Enabled!=0)&&(edtavCtldue_date_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtldue_date_Enabled!=0)&&(edtavCtldue_date_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,153);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtldue_date_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)20,(String)"chr",(short)1,(String)"row",(short)20,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock21_Internalname,(String)"assigned_to",(String)"",(String)"",(String)lblTextblock21_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Single line edit */
         TempTags = " " + ((edtavCtlname6_Enabled!=0)&&(edtavCtlname6_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 157,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname6_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Assigned_to.gxTpr_Name),(String)"",TempTags+((edtavCtlname6_Enabled!=0)&&(edtavCtlname6_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlname6_Enabled!=0)&&(edtavCtlname6_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,157);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlname6_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)80,(String)"chr",(short)1,(String)"row",(short)9999,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock18_Internalname,(String)"author",(String)"",(String)"",(String)lblTextblock18_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Single line edit */
         TempTags = " " + ((edtavCtlid6_Enabled!=0)&&(edtavCtlid6_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 163,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlid6_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Author.gxTpr_Id), 2, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Author.gxTpr_Id), "Z9")),TempTags+((edtavCtlid6_Enabled!=0)&&(edtavCtlid6_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlid6_Enabled!=0)&&(edtavCtlid6_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,163);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlid6_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)2,(String)"chr",(short)1,(String)"row",(short)2,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Single line edit */
         TempTags = " " + ((edtavCtlname5_Enabled!=0)&&(edtavCtlname5_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 164,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlname5_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Author.gxTpr_Name),(String)"",TempTags+((edtavCtlname5_Enabled!=0)&&(edtavCtlname5_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlname5_Enabled!=0)&&(edtavCtlname5_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,164);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlname5_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)80,(String)"chr",(short)1,(String)"row",(short)9999,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Table start */
         Grid2Row.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblTable6_Internalname+"_"+sGXsfl_120_idx,(short)1,(String)"Table",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(short)2,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("table");
         }
         /* End of table */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock24_Internalname,(String)"subject",(String)"",(String)"",(String)lblTextblock24_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         TempTags = " " + ((edtavCtlsubject_Enabled!=0)&&(edtavCtlsubject_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 172,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlsubject_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Subject),(String)"",TempTags+((edtavCtlsubject_Enabled!=0)&&(edtavCtlsubject_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlsubject_Enabled!=0)&&(edtavCtlsubject_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,172);\"" : " "),(short)0,(short)1,(short)1,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock25_Internalname,(String)"description",(String)"",(String)"",(String)lblTextblock25_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         TempTags = " " + ((edtavCtldescription_Enabled!=0)&&(edtavCtldescription_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 177,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         Grid2Row.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldescription_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Description),(String)"",TempTags+((edtavCtldescription_Enabled!=0)&&(edtavCtldescription_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtldescription_Enabled!=0)&&(edtavCtldescription_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,177);\"" : " "),(short)0,(short)1,(short)1,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"9999",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock28_Internalname,(String)"done_ratio",(String)"",(String)"",(String)lblTextblock28_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         TempTags = " " + ((edtavCtldone_ratio_Enabled!=0)&&(edtavCtldone_ratio_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 190,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtldone_ratio_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Done_ratio), 2, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Done_ratio), "Z9")),TempTags+((edtavCtldone_ratio_Enabled!=0)&&(edtavCtldone_ratio_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtldone_ratio_Enabled!=0)&&(edtavCtldone_ratio_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,190);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtldone_ratio_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)2,(String)"chr",(short)1,(String)"row",(short)2,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock29_Internalname,(String)"estimated_hours",(String)"",(String)"",(String)lblTextblock29_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         TempTags = " " + ((edtavCtlestimated_hours_Enabled!=0)&&(edtavCtlestimated_hours_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 195,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlestimated_hours_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Estimated_hours),(String)"",TempTags+((edtavCtlestimated_hours_Enabled!=0)&&(edtavCtlestimated_hours_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlestimated_hours_Enabled!=0)&&(edtavCtlestimated_hours_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,195);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlestimated_hours_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)80,(String)"chr",(short)1,(String)"row",(short)9999,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock30_Internalname,(String)"custom_fields",(String)"",(String)"",(String)lblTextblock30_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'","font-family:'Arial'; font-size:10.0pt; font-weight:bold; font-style:normal;",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Table start */
         Grid2Row.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblTable8_Internalname+"_"+sGXsfl_120_idx,(short)1,(String)"Table",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(short)2,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)"",(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /*  Child Grid Control  */
         Grid2Row.AddColumnProperties("subfile", -1, isAjaxCallMode( ), new Object[] {(String)"Grid1Container"});
         if ( isAjaxCallMode( ) )
         {
            Grid1Container = new GXWebGrid( context);
         }
         else
         {
            Grid1Container.Clear();
         }
         Grid1Container.SetWrapped(nGXWrapped);
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"202\">") ;
            sStyleString = "";
            if ( subGrid1_Collapsed != 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
            /* Subfile titles */
            context.WriteHtmlText( "<tr") ;
            context.WriteHtmlTextNl( ">") ;
            if ( subGrid1_Backcolorstyle == 0 )
            {
               subGrid1_Titlebackstyle = 0;
               if ( StringUtil.Len( subGrid1_Class) > 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Title";
               }
            }
            else
            {
               subGrid1_Titlebackstyle = 1;
               if ( subGrid1_Backcolorstyle == 1 )
               {
                  subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                  }
               }
               else
               {
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
            }
            context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
            context.SendWebValue( "id") ;
            context.WriteHtmlTextNl( "</th>") ;
            context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
            context.SendWebValue( "name") ;
            context.WriteHtmlTextNl( "</th>") ;
            context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
            context.SendWebValue( "value") ;
            context.WriteHtmlTextNl( "</th>") ;
            context.WriteHtmlTextNl( "</tr>") ;
            Grid1Container.AddObjectProperty("GridName", "Grid1");
         }
         else
         {
            Grid1Container.AddObjectProperty("GridName", "Grid1");
            Grid1Container.AddObjectProperty("Class", "Grid");
            Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
            Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
            Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
            Grid1Container.AddObjectProperty("Backcoloreven", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcoloreven), 9, 0, ".", "")));
            Grid1Container.AddObjectProperty("CmpContext", "");
            Grid1Container.AddObjectProperty("InMasterPage", "false");
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
            Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
            Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
            Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
            Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
            Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
         }
         RFKD10( ) ;
         nRC_GXsfl_202 = (short)(nGXsfl_202_idx-1);
         GXCCtl = "nRC_GXsfl_202_" + sGXsfl_120_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_202), 4, 0, ",", "")));
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "</table>") ;
         }
         else
         {
            if ( ! isAjaxCallMode( ) )
            {
               GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"_"+sGXsfl_120_idx, Grid1Container.ToJavascriptSource());
            }
            if ( isAjaxCallMode( ) )
            {
               Grid2Row.AddGrid("Grid1", Grid1Container);
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V_"+sGXsfl_120_idx, Grid1Container.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V_"+sGXsfl_120_idx+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
            }
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("table");
         }
         /* End of table */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock32_Internalname,(String)"created_on",(String)"",(String)"",(String)lblTextblock32_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         TempTags = " " + ((edtavCtlcreated_on_Enabled!=0)&&(edtavCtlcreated_on_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 210,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlcreated_on_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Created_on),(String)"",TempTags+((edtavCtlcreated_on_Enabled!=0)&&(edtavCtlcreated_on_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlcreated_on_Enabled!=0)&&(edtavCtlcreated_on_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,210);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlcreated_on_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)20,(String)"chr",(short)1,(String)"row",(short)20,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         Grid2Row.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblock33_Internalname,(String)"updated_on",(String)"",(String)"",(String)lblTextblock33_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         TempTags = " " + ((edtavCtlupdated_on_Enabled!=0)&&(edtavCtlupdated_on_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 214,'',false,'"+sGXsfl_120_idx+"',120)\"" : " ");
         ROClassString = "Attribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlupdated_on_Internalname,StringUtil.RTrim( ((SdtSDT_RedmineIssues_issue)AV7SDT_Issues.gxTpr_Issues.Item(AV76GXV3)).gxTpr_Updated_on),(String)"",TempTags+((edtavCtlupdated_on_Enabled!=0)&&(edtavCtlupdated_on_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlupdated_on_Enabled!=0)&&(edtavCtlupdated_on_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,214);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlupdated_on_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)1,(short)0,(String)"text",(String)"",(short)20,(String)"chr",(short)1,(String)"row",(short)20,(short)0,(short)0,(short)120,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         Grid2Row.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGrid2_Linesclass,(String)""});
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         Grid2Row.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("cell");
         }
         if ( Grid2Container.GetWrapped() == 1 )
         {
            Grid2Container.CloseTag("row");
         }
         GXCCtl = "GRID1_Collapsed_" + sGXsfl_120_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
         /* End of Columns property logic. */
         if ( Grid2Container.GetWrapped() == 1 )
         {
            if ( 1 > 0 )
            {
               if ( ((int)((nGXsfl_120_idx) % (1))) == 0 )
               {
                  context.WriteHtmlTextNl( "</tr>") ;
               }
            }
         }
         Grid2Container.AddRow(Grid2Row);
         nGXsfl_120_idx = (short)(((subGrid2_Islastpage==1)&&(nGXsfl_120_idx+1>subGrid2_Recordsperpage( )) ? 1 : nGXsfl_120_idx+1));
         sGXsfl_120_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_120_idx), 4, 0)), 4, "0");
         SubsflControlProps_1202( ) ;
         /* End function sendrow_1202 */
      }

      protected void init_default_properties( )
      {
         lblTextblock8_Internalname = "TEXTBLOCK8";
         edtavUsr_nome_Internalname = "vUSR_NOME";
         lblTextblock10_Internalname = "TEXTBLOCK10";
         edtavUsr_senha_Internalname = "vUSR_SENHA";
         tblTable5_Internalname = "TABLE5";
         lblTextblock37_Internalname = "TEXTBLOCK37";
         cmbavSecure_Internalname = "vSECURE";
         edtavHost_Internalname = "vHOST";
         lblTextblock39_Internalname = "TEXTBLOCK39";
         edtavUrl_Internalname = "vURL";
         lblTextblock38_Internalname = "TEXTBLOCK38";
         edtavKey_Internalname = "vKEY";
         bttButton2_Internalname = "BUTTON2";
         lblTextblock34_Internalname = "TEXTBLOCK34";
         cmbavProjeto_Internalname = "vPROJETO";
         lblTextblock41_Internalname = "TEXTBLOCK41";
         dynavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         lblTextblock36_Internalname = "TEXTBLOCK36";
         cmbavTracker_Internalname = "vTRACKER";
         lblTextblock44_Internalname = "TEXTBLOCK44";
         dynavContrato_Internalname = "vCONTRATO";
         lblTextblock47_Internalname = "TEXTBLOCK47";
         dynavServico_codigo_Internalname = "vSERVICO_CODIGO";
         lblTextblock35_Internalname = "TEXTBLOCK35";
         cmbavStatus_Internalname = "vSTATUS";
         bttBtnimportar_Internalname = "BTNIMPORTAR";
         lblTextblock42_Internalname = "TEXTBLOCK42";
         edtavCreated_from_Internalname = "vCREATED_FROM";
         edtavCreated_to_Internalname = "vCREATED_TO";
         lblTextblock45_Internalname = "TEXTBLOCK45";
         edtavCampo_sistemas_Internalname = "vCAMPO_SISTEMAS";
         bttBtnconsultar_Internalname = "BTNCONSULTAR";
         lblTextblock46_Internalname = "TEXTBLOCK46";
         edtavCampo_servicos_Internalname = "vCAMPO_SERVICOS";
         bttBtndeparasistemas_Internalname = "BTNDEPARASISTEMAS";
         lblTextblock14_Internalname = "TEXTBLOCK14";
         edtavCount_Internalname = "vCOUNT";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavCtloffset_Internalname = "CTLOFFSET";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavCtllimit_Internalname = "CTLLIMIT";
         tblTable10_Internalname = "TABLE10";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavCtlid1_Internalname = "CTLID1";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         edtavCtlid2_Internalname = "CTLID2";
         edtavCtlname1_Internalname = "CTLNAME1";
         lblTextblock9_Internalname = "TEXTBLOCK9";
         edtavCtlid3_Internalname = "CTLID3";
         edtavCtlname2_Internalname = "CTLNAME2";
         lblTextblock12_Internalname = "TEXTBLOCK12";
         edtavCtlname3_Internalname = "CTLNAME3";
         edtavCtlname7_Internalname = "CTLNAME7";
         lblTextblock26_Internalname = "TEXTBLOCK26";
         edtavCtlstart_date_Internalname = "CTLSTART_DATE";
         lblTextblock15_Internalname = "TEXTBLOCK15";
         edtavCtlid7_Internalname = "CTLID7";
         edtavCtlname4_Internalname = "CTLNAME4";
         lblTextblock27_Internalname = "TEXTBLOCK27";
         edtavCtldue_date_Internalname = "CTLDUE_DATE";
         lblTextblock21_Internalname = "TEXTBLOCK21";
         edtavCtlname6_Internalname = "CTLNAME6";
         lblTextblock18_Internalname = "TEXTBLOCK18";
         edtavCtlid6_Internalname = "CTLID6";
         edtavCtlname5_Internalname = "CTLNAME5";
         tblTable6_Internalname = "TABLE6";
         lblTextblock24_Internalname = "TEXTBLOCK24";
         edtavCtlsubject_Internalname = "CTLSUBJECT";
         lblTextblock25_Internalname = "TEXTBLOCK25";
         edtavCtldescription_Internalname = "CTLDESCRIPTION";
         lblTextblock28_Internalname = "TEXTBLOCK28";
         edtavCtldone_ratio_Internalname = "CTLDONE_RATIO";
         lblTextblock29_Internalname = "TEXTBLOCK29";
         edtavCtlestimated_hours_Internalname = "CTLESTIMATED_HOURS";
         lblTextblock30_Internalname = "TEXTBLOCK30";
         edtavCtlid_Internalname = "CTLID";
         edtavCtlname_Internalname = "CTLNAME";
         edtavCtlvalue_Internalname = "CTLVALUE";
         tblTable8_Internalname = "TABLE8";
         lblTextblock32_Internalname = "TEXTBLOCK32";
         edtavCtlcreated_on_Internalname = "CTLCREATED_ON";
         lblTextblock33_Internalname = "TEXTBLOCK33";
         edtavCtlupdated_on_Internalname = "CTLUPDATED_ON";
         tblTable9_Internalname = "TABLE9";
         tblTable1_Internalname = "TABLE1";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         dynavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         lblTextblock7_Internalname = "TEXTBLOCK7";
         dynavServico_codigo_Internalname = "vSERVICO_CODIGO";
         tblTable2_Internalname = "TABLE2";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
         subGrid2_Internalname = "GRID2";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavCtlupdated_on_Jsonclick = "";
         edtavCtlupdated_on_Visible = 1;
         edtavCtlupdated_on_Enabled = 1;
         edtavCtlcreated_on_Jsonclick = "";
         edtavCtlcreated_on_Visible = 1;
         edtavCtlcreated_on_Enabled = 1;
         subGrid1_Allowcollapsing = 1;
         subGrid1_Allowselection = 0;
         edtavCtlestimated_hours_Jsonclick = "";
         edtavCtlestimated_hours_Visible = 1;
         edtavCtlestimated_hours_Enabled = 1;
         edtavCtldone_ratio_Jsonclick = "";
         edtavCtldone_ratio_Visible = 1;
         edtavCtldone_ratio_Enabled = 1;
         edtavCtldescription_Visible = 1;
         edtavCtldescription_Enabled = 1;
         edtavCtlsubject_Visible = 1;
         edtavCtlsubject_Enabled = 1;
         edtavCtlname5_Jsonclick = "";
         edtavCtlname5_Visible = 1;
         edtavCtlname5_Enabled = 1;
         edtavCtlid6_Jsonclick = "";
         edtavCtlid6_Visible = 1;
         edtavCtlid6_Enabled = 1;
         edtavCtlname6_Jsonclick = "";
         edtavCtlname6_Visible = 1;
         edtavCtlname6_Enabled = 1;
         edtavCtldue_date_Jsonclick = "";
         edtavCtldue_date_Visible = 1;
         edtavCtldue_date_Enabled = 1;
         edtavCtlname4_Jsonclick = "";
         edtavCtlname4_Visible = 1;
         edtavCtlname4_Enabled = 1;
         edtavCtlid7_Jsonclick = "";
         edtavCtlid7_Visible = 1;
         edtavCtlid7_Enabled = 1;
         edtavCtlstart_date_Jsonclick = "";
         edtavCtlstart_date_Visible = 1;
         edtavCtlstart_date_Enabled = 1;
         edtavCtlname7_Jsonclick = "";
         edtavCtlname7_Visible = 1;
         edtavCtlname7_Enabled = 1;
         edtavCtlname3_Jsonclick = "";
         edtavCtlname3_Visible = 1;
         edtavCtlname3_Enabled = 1;
         edtavCtlname2_Jsonclick = "";
         edtavCtlname2_Visible = 1;
         edtavCtlname2_Enabled = 1;
         edtavCtlid3_Jsonclick = "";
         edtavCtlid3_Visible = 1;
         edtavCtlid3_Enabled = 1;
         edtavCtlname1_Jsonclick = "";
         edtavCtlname1_Visible = 1;
         edtavCtlname1_Enabled = 1;
         edtavCtlid2_Jsonclick = "";
         edtavCtlid2_Visible = 1;
         edtavCtlid2_Enabled = 1;
         edtavCtlid1_Jsonclick = "";
         edtavCtlid1_Visible = 1;
         edtavCtlid1_Enabled = 1;
         subGrid2_Class = "FreeStyleGrid";
         edtavCtlvalue_Jsonclick = "";
         edtavCtlvalue_Visible = -1;
         edtavCtlvalue_Enabled = 1;
         edtavCtlname_Jsonclick = "";
         edtavCtlname_Visible = -1;
         edtavCtlname_Enabled = 1;
         edtavCtlid_Jsonclick = "";
         edtavCtlid_Visible = -1;
         edtavCtlid_Enabled = 1;
         subGrid1_Class = "Grid";
         edtavUsr_senha_Jsonclick = "";
         edtavUsr_nome_Jsonclick = "";
         edtavCtllimit_Jsonclick = "";
         edtavCtllimit_Enabled = 0;
         edtavCtloffset_Jsonclick = "";
         edtavCtloffset_Enabled = 0;
         edtavCount_Jsonclick = "";
         edtavCount_Enabled = 1;
         subGrid2_Allowcollapsing = 0;
         lblTextblock33_Caption = "updated_on";
         lblTextblock32_Caption = "created_on";
         lblTextblock30_Caption = "custom_fields";
         lblTextblock29_Caption = "estimated_hours";
         lblTextblock28_Caption = "done_ratio";
         lblTextblock25_Caption = "description";
         lblTextblock24_Caption = "subject";
         lblTextblock18_Caption = "author";
         lblTextblock21_Caption = "assigned_to";
         lblTextblock27_Caption = "Data prevista";
         lblTextblock15_Caption = "Prioridade";
         lblTextblock26_Caption = "Inicio";
         lblTextblock12_Caption = "Estado";
         lblTextblock9_Caption = "tracker";
         lblTextblock6_Caption = "project";
         lblTextblock5_Caption = "id";
         bttBtndeparasistemas_Visible = 1;
         edtavCampo_servicos_Jsonclick = "";
         bttBtnconsultar_Visible = 1;
         edtavCampo_sistemas_Jsonclick = "";
         edtavCreated_to_Jsonclick = "";
         edtavCreated_from_Jsonclick = "";
         bttBtnimportar_Visible = 1;
         cmbavStatus_Jsonclick = "";
         dynavContrato_Jsonclick = "";
         cmbavTracker_Jsonclick = "";
         cmbavProjeto_Jsonclick = "";
         edtavKey_Jsonclick = "";
         edtavUrl_Jsonclick = "";
         edtavHost_Jsonclick = "";
         cmbavSecure_Jsonclick = "";
         dynavServico_codigo_Jsonclick = "";
         dynavContratada_codigo_Jsonclick = "";
         bttBtnimportar_Caption = "4) Importar demandas";
         tblTable2_Visible = 1;
         subGrid1_Backcoloreven = (int)(0xF5F5F5);
         subGrid1_Backcolorstyle = 3;
         subGrid2_Bordercolor = (int)(0x000000);
         subGrid2_Borderwidth = 1;
         subGrid2_Backcolorstyle = 0;
         edtavCtllimit_Enabled = -1;
         edtavCtloffset_Enabled = -1;
         subGrid1_Collapsed = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "WDSL de  OS";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Contratada_codigo( GXCombobox dynGX_Parm1 ,
                                            GXCombobox dynGX_Parm2 ,
                                            GXCombobox dynGX_Parm3 )
      {
         dynavContratada_codigo = dynGX_Parm1;
         AV33Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.CurrentValue, "."));
         dynavContrato = dynGX_Parm2;
         AV68Contrato = (int)(NumberUtil.Val( dynavContrato.CurrentValue, "."));
         dynavServico_codigo = dynGX_Parm3;
         AV34Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.CurrentValue, "."));
         GXVvCONTRATO_htmlKD2( AV33Contratada_Codigo) ;
         GXVvSERVICO_CODIGO_htmlKD2( AV68Contrato, AV33Contratada_Codigo) ;
         dynload_actions( ) ;
         if ( dynavContrato.ItemCount > 0 )
         {
            AV68Contrato = (int)(NumberUtil.Val( dynavContrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV68Contrato), 6, 0))), "."));
         }
         dynavContrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV68Contrato), 6, 0));
         isValidOutput.Add(dynavContrato);
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV34Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0))), "."));
         }
         dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0));
         isValidOutput.Add(dynavServico_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contrato( GXCombobox dynGX_Parm1 ,
                                   GXCombobox dynGX_Parm2 ,
                                   GXCombobox dynGX_Parm3 )
      {
         dynavContrato = dynGX_Parm1;
         AV68Contrato = (int)(NumberUtil.Val( dynavContrato.CurrentValue, "."));
         dynavContratada_codigo = dynGX_Parm2;
         AV33Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.CurrentValue, "."));
         dynavServico_codigo = dynGX_Parm3;
         AV34Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.CurrentValue, "."));
         GXVvSERVICO_CODIGO_htmlKD2( AV68Contrato, AV33Contratada_Codigo) ;
         dynload_actions( ) ;
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV34Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0))), "."));
         }
         dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Servico_Codigo), 6, 0));
         isValidOutput.Add(dynavServico_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID2_nFirstRecordOnPage',nv:0},{av:'GRID2_nEOF',nv:0},{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'AV7SDT_Issues',fld:'vSDT_ISSUES',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'CARREGARFILTROS'","{handler:'E11KD2',iparms:[{av:'AV47Usr_Nome',fld:'vUSR_NOME',pic:'',nv:''},{av:'AV48Usr_Senha',fld:'vUSR_SENHA',pic:'',nv:''},{av:'AV67Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV63Secure',fld:'vSECURE',pic:'9',nv:0},{av:'AV27Host',fld:'vHOST',pic:'',nv:''},{av:'AV28Url',fld:'vURL',pic:'',nv:''},{av:'AV10Key',fld:'vKEY',pic:'',nv:''},{av:'AV44Versao',fld:'vVERSAO',pic:'',nv:''},{av:'AV66Campo_Sistemas',fld:'vCAMPO_SISTEMAS',pic:'',nv:''},{av:'AV65Campo_Servicos',fld:'vCAMPO_SERVICOS',pic:'',nv:''}],oparms:[{av:'AV67Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9Execute',fld:'vEXECUTE',pic:'',nv:''},{av:'AV8Status',fld:'vSTATUS',pic:'ZZZ9',nv:0},{av:'AV16StatusItem',fld:'vSTATUSITEM',pic:'',nv:null},{av:'AV6Projeto',fld:'vPROJETO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{ctrl:'BTNCONSULTAR',prop:'Visible'},{av:'AV26Tracker',fld:'vTRACKER',pic:'ZZZ9',nv:0},{av:'AV25TrackersItem',fld:'vTRACKERSITEM',pic:'',nv:null}]}");
         setEventMetadata("'BTNCONSULTA'","{handler:'E12KD2',iparms:[{av:'AV6Projeto',fld:'vPROJETO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV26Tracker',fld:'vTRACKER',pic:'ZZZ9',nv:0},{av:'AV8Status',fld:'vSTATUS',pic:'ZZZ9',nv:0},{av:'AV7SDT_Issues',fld:'vSDT_ISSUES',pic:'',nv:null},{av:'AV9Execute',fld:'vEXECUTE',pic:'',nv:''},{av:'A1389ContagemResultado_RdmnIssueId',fld:'CONTAGEMRESULTADO_RDMNISSUEID',pic:'ZZZZZ9',nv:0},{av:'A1392ContagemResultado_RdmnUpdated',fld:'CONTAGEMRESULTADO_RDMNUPDATED',pic:'99/99/99 99:99',nv:''},{av:'AV59SDT_Consulta',fld:'vSDT_CONSULTA',pic:'',nv:null},{av:'AV33Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV51ProjetoSemDePara',fld:'vPROJETOSEMDEPARA',pic:'',nv:null},{av:'AV56TrackersSemDePara',fld:'vTRACKERSSEMDEPARA',pic:'',nv:null},{av:'AV57StatusSemDePara',fld:'vSTATUSSEMDEPARA',pic:'',nv:null},{av:'AV27Host',fld:'vHOST',pic:'',nv:''},{av:'AV28Url',fld:'vURL',pic:'',nv:''},{av:'AV71offset',fld:'vOFFSET',pic:'ZZZ9',nv:0},{av:'AV58pagina',fld:'vPAGINA',pic:'ZZZ9',nv:0},{av:'AV50WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV10Key',fld:'vKEY',pic:'',nv:''},{av:'AV40Created_From',fld:'vCREATED_FROM',pic:'99/99/9999 99:99',nv:''},{av:'AV41Created_To',fld:'vCREATED_TO',pic:'99/99/9999 99:99',nv:''},{av:'AV63Secure',fld:'vSECURE',pic:'9',nv:0},{av:'AV47Usr_Nome',fld:'vUSR_NOME',pic:'',nv:''},{av:'AV48Usr_Senha',fld:'vUSR_SENHA',pic:'',nv:''},{av:'A1461SistemaDePara_Origem',fld:'SISTEMADEPARA_ORIGEM',pic:'',nv:''},{av:'A1462SistemaDePara_OrigemId',fld:'SISTEMADEPARA_ORIGEMID',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV14ProjectItem',fld:'vPROJECTITEM',pic:'',nv:null},{av:'A1466ContratoServicosDePara_Origem',fld:'CONTRATOSERVICOSDEPARA_ORIGEM',pic:'',nv:''},{av:'A1467ContratoServicosDePara_OrigenId',fld:'CONTRATOSERVICOSDEPARA_ORIGENID',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'A1469ContratoServicosDePara_OrigenId2',fld:'CONTRATOSERVICOSDEPARA_ORIGENID2',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV54SemDePara',fld:'vSEMDEPARA',pic:'',nv:false},{av:'AV25TrackersItem',fld:'vTRACKERSITEM',pic:'',nv:null},{av:'AV16StatusItem',fld:'vSTATUSITEM',pic:'',nv:null},{av:'GRID2_nFirstRecordOnPage',nv:0},{av:'GRID2_nEOF',nv:0}],oparms:[{ctrl:'BTNIMPORTAR',prop:'Visible'},{av:'AV7SDT_Issues',fld:'vSDT_ISSUES',pic:'',nv:null},{av:'AV58pagina',fld:'vPAGINA',pic:'ZZZ9',nv:0},{av:'AV71offset',fld:'vOFFSET',pic:'ZZZ9',nv:0},{av:'AV60Count',fld:'vCOUNT',pic:'ZZZ9',nv:0},{av:'AV59SDT_Consulta',fld:'vSDT_CONSULTA',pic:'',nv:null},{av:'AV55Issue',fld:'vISSUE',pic:'',nv:null},{av:'AV54SemDePara',fld:'vSEMDEPARA',pic:'',nv:false},{ctrl:'BTNDEPARASISTEMAS',prop:'Visible'},{ctrl:'BTNIMPORTAR',prop:'Caption'},{av:'AV9Execute',fld:'vEXECUTE',pic:'',nv:''},{av:'AV51ProjetoSemDePara',fld:'vPROJETOSEMDEPARA',pic:'',nv:null},{av:'AV14ProjectItem',fld:'vPROJECTITEM',pic:'',nv:null},{av:'AV56TrackersSemDePara',fld:'vTRACKERSSEMDEPARA',pic:'',nv:null},{av:'AV57StatusSemDePara',fld:'vSTATUSSEMDEPARA',pic:'',nv:null},{av:'AV25TrackersItem',fld:'vTRACKERSITEM',pic:'',nv:null},{av:'AV16StatusItem',fld:'vSTATUSITEM',pic:'',nv:null}]}");
         setEventMetadata("'IMPORTAR'","{handler:'E13KD2',iparms:[{av:'AV8Status',fld:'vSTATUS',pic:'ZZZ9',nv:0},{av:'AV33Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV34Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7SDT_Issues',fld:'vSDT_ISSUES',pic:'',nv:null},{av:'AV44Versao',fld:'vVERSAO',pic:'',nv:''},{av:'AV27Host',fld:'vHOST',pic:'',nv:''},{av:'AV28Url',fld:'vURL',pic:'',nv:''},{av:'AV10Key',fld:'vKEY',pic:'',nv:''},{av:'AV50WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV70SistemasValidados',fld:'vSISTEMASVALIDADOS',pic:'',nv:false},{av:'AV50WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}]}");
         setEventMetadata("VCONTRATADA_CODIGO.CLICK","{handler:'E19KD1',iparms:[{av:'AV33Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7SDT_Issues',fld:'vSDT_ISSUES',pic:'',nv:null},{av:'AV70SistemasValidados',fld:'vSISTEMASVALIDADOS',pic:'',nv:false}],oparms:[{ctrl:'BTNIMPORTAR',prop:'Visible'}]}");
         setEventMetadata("VPROJETO.CLICK","{handler:'E20KD1',iparms:[],oparms:[{ctrl:'BTNIMPORTAR',prop:'Visible'},{ctrl:'BTNDEPARASISTEMAS',prop:'Visible'}]}");
         setEventMetadata("VTRACKER.CLICK","{handler:'E21KD1',iparms:[],oparms:[{ctrl:'BTNIMPORTAR',prop:'Visible'},{ctrl:'BTNDEPARASISTEMAS',prop:'Visible'}]}");
         setEventMetadata("VSTATUS.CLICK","{handler:'E22KD1',iparms:[],oparms:[{ctrl:'BTNIMPORTAR',prop:'Visible'},{ctrl:'BTNDEPARASISTEMAS',prop:'Visible'}]}");
         setEventMetadata("'VERIFICARSISTEMAS'","{handler:'E14KD2',iparms:[{av:'AV66Campo_Sistemas',fld:'vCAMPO_SISTEMAS',pic:'',nv:''},{av:'AV64SDT_Parametros',fld:'vSDT_PARAMETROS',pic:'',nv:null},{av:'AV50WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV7SDT_Issues',fld:'vSDT_ISSUES',pic:'',nv:null},{av:'AV67Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV47Usr_Nome',fld:'vUSR_NOME',pic:'',nv:''},{av:'AV63Secure',fld:'vSECURE',pic:'9',nv:0},{av:'AV27Host',fld:'vHOST',pic:'',nv:''},{av:'AV28Url',fld:'vURL',pic:'',nv:''},{av:'AV10Key',fld:'vKEY',pic:'',nv:''},{av:'AV44Versao',fld:'vVERSAO',pic:'',nv:''},{av:'AV65Campo_Servicos',fld:'vCAMPO_SERVICOS',pic:'',nv:''}],oparms:[{av:'AV70SistemasValidados',fld:'vSISTEMASVALIDADOS',pic:'',nv:false},{ctrl:'BTNIMPORTAR',prop:'Visible'},{av:'AV64SDT_Parametros',fld:'vSDT_PARAMETROS',pic:'',nv:null},{av:'AV67Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'VERIFICARSERVICOS'","{handler:'E16KD2',iparms:[{av:'AV65Campo_Servicos',fld:'vCAMPO_SERVICOS',pic:'',nv:''},{av:'AV70SistemasValidados',fld:'vSISTEMASVALIDADOS',pic:'',nv:false},{av:'AV64SDT_Parametros',fld:'vSDT_PARAMETROS',pic:'',nv:null},{av:'AV50WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV68Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV7SDT_Issues',fld:'vSDT_ISSUES',pic:'',nv:null},{av:'AV67Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV47Usr_Nome',fld:'vUSR_NOME',pic:'',nv:''},{av:'AV63Secure',fld:'vSECURE',pic:'9',nv:0},{av:'AV27Host',fld:'vHOST',pic:'',nv:''},{av:'AV28Url',fld:'vURL',pic:'',nv:''},{av:'AV10Key',fld:'vKEY',pic:'',nv:''},{av:'AV44Versao',fld:'vVERSAO',pic:'',nv:''},{av:'AV66Campo_Sistemas',fld:'vCAMPO_SISTEMAS',pic:'',nv:''}],oparms:[{ctrl:'BTNIMPORTAR',prop:'Visible'},{av:'AV64SDT_Parametros',fld:'vSDT_PARAMETROS',pic:'',nv:null},{av:'AV67Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV50WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV7SDT_Issues = new SdtSDT_RedmineIssues(context);
         AV44Versao = "";
         AV9Execute = "";
         A1392ContagemResultado_RdmnUpdated = (DateTime)(DateTime.MinValue);
         AV59SDT_Consulta = new SdtSDT_RedmineIssues(context);
         AV51ProjetoSemDePara = new GxObjectCollection( context, "SDT_Redmineuser.memberships.membership", "", "SdtSDT_Redmineuser_memberships_membership", "GeneXus.Programs");
         AV56TrackersSemDePara = new GxObjectCollection( context, "SDT_RedmineTrackers.tracker", "", "SdtSDT_RedmineTrackers_tracker", "GeneXus.Programs");
         AV57StatusSemDePara = new GxObjectCollection( context, "SDT_RedmineStatus.issue_status", "", "SdtSDT_RedmineStatus_issue_status", "GeneXus.Programs");
         A1461SistemaDePara_Origem = "";
         AV14ProjectItem = new SdtSDT_Redmineuser_memberships_membership(context);
         A1466ContratoServicosDePara_Origem = "";
         AV25TrackersItem = new SdtSDT_RedmineTrackers_tracker(context);
         AV16StatusItem = new SdtSDT_RedmineStatus_issue_status(context);
         AV64SDT_Parametros = new SdtSDT_Parametros(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         AV63Secure = 0;
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00KD2_A40Contratada_PessoaCod = new int[1] ;
         H00KD2_A39Contratada_Codigo = new int[1] ;
         H00KD2_A41Contratada_PessoaNom = new String[] {""} ;
         H00KD2_n41Contratada_PessoaNom = new bool[] {false} ;
         H00KD2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00KD3_A74Contrato_Codigo = new int[1] ;
         H00KD3_A77Contrato_Numero = new String[] {""} ;
         H00KD3_A39Contratada_Codigo = new int[1] ;
         H00KD3_A92Contrato_Ativo = new bool[] {false} ;
         H00KD4_A155Servico_Codigo = new int[1] ;
         H00KD4_A160ContratoServicos_Codigo = new int[1] ;
         H00KD4_A74Contrato_Codigo = new int[1] ;
         H00KD4_A39Contratada_Codigo = new int[1] ;
         H00KD4_A632Servico_Ativo = new bool[] {false} ;
         H00KD4_n632Servico_Ativo = new bool[] {false} ;
         H00KD4_A826ContratoServicos_ServicoSigla = new String[] {""} ;
         Grid2Container = new GXWebGrid( context);
         Grid1Container = new GXWebGrid( context);
         AV47Usr_Nome = "";
         AV48Usr_Senha = "";
         AV27Host = "";
         AV28Url = "";
         AV10Key = "";
         AV40Created_From = (DateTime)(DateTime.MinValue);
         AV41Created_To = (DateTime)(DateTime.MinValue);
         AV66Campo_Sistemas = "";
         AV65Campo_Servicos = "";
         H00KD5_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         H00KD5_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         H00KD5_A29Contratante_Codigo = new int[1] ;
         H00KD5_n29Contratante_Codigo = new bool[] {false} ;
         H00KD5_A632Servico_Ativo = new bool[] {false} ;
         H00KD5_n632Servico_Ativo = new bool[] {false} ;
         H00KD5_A5AreaTrabalho_Codigo = new int[1] ;
         H00KD6_A1380Redmine_Codigo = new int[1] ;
         H00KD6_A1384Redmine_ContratanteCod = new int[1] ;
         H00KD6_A1905Redmine_User = new String[] {""} ;
         H00KD6_n1905Redmine_User = new bool[] {false} ;
         H00KD6_A1904Redmine_Secure = new short[1] ;
         H00KD6_A1381Redmine_Host = new String[] {""} ;
         H00KD6_A1382Redmine_Url = new String[] {""} ;
         H00KD6_A1383Redmine_Key = new String[] {""} ;
         H00KD6_A1391Redmine_Versao = new String[] {""} ;
         H00KD6_n1391Redmine_Versao = new bool[] {false} ;
         H00KD6_A1906Redmine_CampoSistema = new String[] {""} ;
         H00KD6_n1906Redmine_CampoSistema = new bool[] {false} ;
         H00KD6_A1907Redmine_CampoServico = new String[] {""} ;
         H00KD6_n1907Redmine_CampoServico = new bool[] {false} ;
         A1905Redmine_User = "";
         A1381Redmine_Host = "";
         A1382Redmine_Url = "";
         A1383Redmine_Key = "";
         A1391Redmine_Versao = "";
         A1906Redmine_CampoSistema = "";
         A1907Redmine_CampoServico = "";
         AV5httpclient = new GxHttpClient( context);
         AV55Issue = new SdtSDT_RedmineIssues_issue(context);
         H00KD7_A456ContagemResultado_Codigo = new int[1] ;
         H00KD7_A1389ContagemResultado_RdmnIssueId = new int[1] ;
         H00KD7_n1389ContagemResultado_RdmnIssueId = new bool[] {false} ;
         H00KD7_A1392ContagemResultado_RdmnUpdated = new DateTime[] {DateTime.MinValue} ;
         H00KD7_n1392ContagemResultado_RdmnUpdated = new bool[] {false} ;
         AV62Updated = (DateTime)(DateTime.MinValue);
         AV45WebSession = context.GetSession();
         AV15SDT_Status = new SdtSDT_RedmineStatus(context);
         AV17SDT_Trackers = new SdtSDT_RedmineTrackers(context);
         AV39SDT_User = new SdtSDT_Redmineuser(context);
         AV49User = new SdtSDT_Redmineuser_memberships_membership(context);
         H00KD8_A127Sistema_Codigo = new int[1] ;
         H00KD8_A1460SistemaDePara_Codigo = new int[1] ;
         H00KD8_A1462SistemaDePara_OrigemId = new long[1] ;
         H00KD8_A1461SistemaDePara_Origem = new String[] {""} ;
         H00KD9_A160ContratoServicos_Codigo = new int[1] ;
         H00KD9_A1465ContratoServicosDePara_Codigo = new int[1] ;
         H00KD9_A1469ContratoServicosDePara_OrigenId2 = new long[1] ;
         H00KD9_n1469ContratoServicosDePara_OrigenId2 = new bool[] {false} ;
         H00KD9_A1467ContratoServicosDePara_OrigenId = new long[1] ;
         H00KD9_n1467ContratoServicosDePara_OrigenId = new bool[] {false} ;
         H00KD9_A1466ContratoServicosDePara_Origem = new String[] {""} ;
         H00KD10_A160ContratoServicos_Codigo = new int[1] ;
         H00KD10_A1465ContratoServicosDePara_Codigo = new int[1] ;
         H00KD10_A1469ContratoServicosDePara_OrigenId2 = new long[1] ;
         H00KD10_n1469ContratoServicosDePara_OrigenId2 = new bool[] {false} ;
         H00KD10_A1467ContratoServicosDePara_OrigenId = new long[1] ;
         H00KD10_n1467ContratoServicosDePara_OrigenId = new bool[] {false} ;
         H00KD10_A1466ContratoServicosDePara_Origem = new String[] {""} ;
         H00KD11_A160ContratoServicos_Codigo = new int[1] ;
         H00KD11_A1465ContratoServicosDePara_Codigo = new int[1] ;
         H00KD11_A1469ContratoServicosDePara_OrigenId2 = new long[1] ;
         H00KD11_n1469ContratoServicosDePara_OrigenId2 = new bool[] {false} ;
         H00KD11_A1467ContratoServicosDePara_OrigenId = new long[1] ;
         H00KD11_n1467ContratoServicosDePara_OrigenId = new bool[] {false} ;
         H00KD11_A1466ContratoServicosDePara_Origem = new String[] {""} ;
         Grid2Row = new GXWebRow();
         Grid1Row = new GXWebRow();
         sStyleString = "";
         lblTextblock4_Jsonclick = "";
         TempTags = "";
         lblTextblock7_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         lblTextblock37_Jsonclick = "";
         lblTextblock39_Jsonclick = "";
         lblTextblock38_Jsonclick = "";
         bttButton2_Jsonclick = "";
         lblTextblock34_Jsonclick = "";
         lblTextblock41_Jsonclick = "";
         lblTextblock36_Jsonclick = "";
         lblTextblock44_Jsonclick = "";
         lblTextblock47_Jsonclick = "";
         lblTextblock35_Jsonclick = "";
         bttBtnimportar_Jsonclick = "";
         lblTextblock42_Jsonclick = "";
         lblTextblock45_Jsonclick = "";
         bttBtnconsultar_Jsonclick = "";
         lblTextblock46_Jsonclick = "";
         bttBtndeparasistemas_Jsonclick = "";
         Grid2Column = new GXWebColumn();
         lblTextblock14_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock8_Jsonclick = "";
         lblTextblock10_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         subGrid1_Linesclass = "";
         ROClassString = "";
         subGrid2_Linesclass = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock9_Jsonclick = "";
         lblTextblock12_Jsonclick = "";
         lblTextblock26_Jsonclick = "";
         lblTextblock15_Jsonclick = "";
         lblTextblock27_Jsonclick = "";
         lblTextblock21_Jsonclick = "";
         lblTextblock18_Jsonclick = "";
         lblTextblock24_Jsonclick = "";
         lblTextblock25_Jsonclick = "";
         lblTextblock28_Jsonclick = "";
         lblTextblock29_Jsonclick = "";
         lblTextblock30_Jsonclick = "";
         Grid1Column = new GXWebColumn();
         lblTextblock32_Jsonclick = "";
         lblTextblock33_Jsonclick = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_wdsldeos__default(),
            new Object[][] {
                new Object[] {
               H00KD2_A40Contratada_PessoaCod, H00KD2_A39Contratada_Codigo, H00KD2_A41Contratada_PessoaNom, H00KD2_n41Contratada_PessoaNom, H00KD2_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00KD3_A74Contrato_Codigo, H00KD3_A77Contrato_Numero, H00KD3_A39Contratada_Codigo, H00KD3_A92Contrato_Ativo
               }
               , new Object[] {
               H00KD4_A155Servico_Codigo, H00KD4_A160ContratoServicos_Codigo, H00KD4_A74Contrato_Codigo, H00KD4_A39Contratada_Codigo, H00KD4_A632Servico_Ativo, H00KD4_A826ContratoServicos_ServicoSigla
               }
               , new Object[] {
               H00KD5_A830AreaTrabalho_ServicoPadrao, H00KD5_n830AreaTrabalho_ServicoPadrao, H00KD5_A29Contratante_Codigo, H00KD5_n29Contratante_Codigo, H00KD5_A632Servico_Ativo, H00KD5_n632Servico_Ativo, H00KD5_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               H00KD6_A1380Redmine_Codigo, H00KD6_A1384Redmine_ContratanteCod, H00KD6_A1905Redmine_User, H00KD6_n1905Redmine_User, H00KD6_A1904Redmine_Secure, H00KD6_A1381Redmine_Host, H00KD6_A1382Redmine_Url, H00KD6_A1383Redmine_Key, H00KD6_A1391Redmine_Versao, H00KD6_n1391Redmine_Versao,
               H00KD6_A1906Redmine_CampoSistema, H00KD6_n1906Redmine_CampoSistema, H00KD6_A1907Redmine_CampoServico, H00KD6_n1907Redmine_CampoServico
               }
               , new Object[] {
               H00KD7_A456ContagemResultado_Codigo, H00KD7_A1389ContagemResultado_RdmnIssueId, H00KD7_n1389ContagemResultado_RdmnIssueId, H00KD7_A1392ContagemResultado_RdmnUpdated, H00KD7_n1392ContagemResultado_RdmnUpdated
               }
               , new Object[] {
               H00KD8_A127Sistema_Codigo, H00KD8_A1460SistemaDePara_Codigo, H00KD8_A1462SistemaDePara_OrigemId, H00KD8_A1461SistemaDePara_Origem
               }
               , new Object[] {
               H00KD9_A160ContratoServicos_Codigo, H00KD9_A1465ContratoServicosDePara_Codigo, H00KD9_A1469ContratoServicosDePara_OrigenId2, H00KD9_n1469ContratoServicosDePara_OrigenId2, H00KD9_A1467ContratoServicosDePara_OrigenId, H00KD9_n1467ContratoServicosDePara_OrigenId, H00KD9_A1466ContratoServicosDePara_Origem
               }
               , new Object[] {
               H00KD10_A160ContratoServicos_Codigo, H00KD10_A1465ContratoServicosDePara_Codigo, H00KD10_A1469ContratoServicosDePara_OrigenId2, H00KD10_n1469ContratoServicosDePara_OrigenId2, H00KD10_A1467ContratoServicosDePara_OrigenId, H00KD10_n1467ContratoServicosDePara_OrigenId, H00KD10_A1466ContratoServicosDePara_Origem
               }
               , new Object[] {
               H00KD11_A160ContratoServicos_Codigo, H00KD11_A1465ContratoServicosDePara_Codigo, H00KD11_A1469ContratoServicosDePara_OrigenId2, H00KD11_n1469ContratoServicosDePara_OrigenId2, H00KD11_A1467ContratoServicosDePara_OrigenId, H00KD11_n1467ContratoServicosDePara_OrigenId, H00KD11_A1466ContratoServicosDePara_Origem
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCount_Enabled = 0;
         edtavCtloffset_Enabled = 0;
         edtavCtllimit_Enabled = 0;
      }

      private short nRC_GXsfl_202 ;
      private short nGXsfl_202_idx=1 ;
      private short nRcdExists_10 ;
      private short nIsMod_10 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_120 ;
      private short nGXsfl_120_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV71offset ;
      private short AV58pagina ;
      private short wbEnd ;
      private short wbStart ;
      private short subGrid1_Collapsed ;
      private short AV76GXV3 ;
      private short AV95GXV22 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV63Secure ;
      private short AV26Tracker ;
      private short AV8Status ;
      private short nGXsfl_120_Refreshing=0 ;
      private short subGrid2_Backcolorstyle ;
      private short subGrid2_Borderwidth ;
      private short nGXsfl_202_Refreshing=0 ;
      private short subGrid1_Backcolorstyle ;
      private short AV60Count ;
      private short nGXsfl_120_fel_idx=1 ;
      private short nGXsfl_202_fel_idx=1 ;
      private short A1904Redmine_Secure ;
      private short AV53i ;
      private short AV61c ;
      private short AV104GXLvl81 ;
      private short GRID2_nEOF ;
      private short nGXsfl_120_bak_idx=1 ;
      private short AV109GXLvl292 ;
      private short AV111GXLvl314 ;
      private short AV112GXLvl320 ;
      private short AV113GXLvl327 ;
      private short subGrid2_Allowselection ;
      private short subGrid2_Allowhovering ;
      private short subGrid2_Allowcollapsing ;
      private short subGrid2_Collapsed ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private short subGrid2_Backstyle ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short wbTemp ;
      private short GRID1_nEOF ;
      private int AV33Contratada_Codigo ;
      private int AV68Contrato ;
      private int AV67Contratante_Codigo ;
      private int A1389ContagemResultado_RdmnIssueId ;
      private int gxdynajaxindex ;
      private int AV34Servico_Codigo ;
      private int subGrid2_Islastpage ;
      private int subGrid1_Islastpage ;
      private int edtavCount_Enabled ;
      private int edtavCtloffset_Enabled ;
      private int edtavCtllimit_Enabled ;
      private int subGrid2_Bordercolor ;
      private int subGrid1_Backcoloreven ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A1384Redmine_ContratanteCod ;
      private int tblTable2_Visible ;
      private int bttBtnimportar_Visible ;
      private int bttBtnconsultar_Visible ;
      private int bttBtndeparasistemas_Visible ;
      private int AV103GXV25 ;
      private int AV105GXV26 ;
      private int AV106GXV27 ;
      private int AV107GXV28 ;
      private int AV108GXV29 ;
      private int AV110GXV30 ;
      private int subGrid2_Selectioncolor ;
      private int subGrid2_Hoveringcolor ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private int subGrid1_Allbackcolor ;
      private int edtavCtlid_Enabled ;
      private int edtavCtlid_Visible ;
      private int edtavCtlname_Enabled ;
      private int edtavCtlname_Visible ;
      private int edtavCtlvalue_Enabled ;
      private int edtavCtlvalue_Visible ;
      private int subGrid2_Backcolor ;
      private int subGrid2_Allbackcolor ;
      private int edtavCtlid1_Enabled ;
      private int edtavCtlid1_Visible ;
      private int edtavCtlid2_Enabled ;
      private int edtavCtlid2_Visible ;
      private int edtavCtlname1_Enabled ;
      private int edtavCtlname1_Visible ;
      private int edtavCtlid3_Enabled ;
      private int edtavCtlid3_Visible ;
      private int edtavCtlname2_Enabled ;
      private int edtavCtlname2_Visible ;
      private int edtavCtlname3_Enabled ;
      private int edtavCtlname3_Visible ;
      private int edtavCtlname7_Enabled ;
      private int edtavCtlname7_Visible ;
      private int edtavCtlstart_date_Enabled ;
      private int edtavCtlstart_date_Visible ;
      private int edtavCtlid7_Enabled ;
      private int edtavCtlid7_Visible ;
      private int edtavCtlname4_Enabled ;
      private int edtavCtlname4_Visible ;
      private int edtavCtldue_date_Enabled ;
      private int edtavCtldue_date_Visible ;
      private int edtavCtlname6_Enabled ;
      private int edtavCtlname6_Visible ;
      private int edtavCtlid6_Enabled ;
      private int edtavCtlid6_Visible ;
      private int edtavCtlname5_Enabled ;
      private int edtavCtlname5_Visible ;
      private int edtavCtlsubject_Enabled ;
      private int edtavCtlsubject_Visible ;
      private int edtavCtldescription_Enabled ;
      private int edtavCtldescription_Visible ;
      private int edtavCtldone_ratio_Enabled ;
      private int edtavCtldone_ratio_Visible ;
      private int edtavCtlestimated_hours_Enabled ;
      private int edtavCtlestimated_hours_Visible ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int edtavCtlcreated_on_Enabled ;
      private int edtavCtlcreated_on_Visible ;
      private int edtavCtlupdated_on_Enabled ;
      private int edtavCtlupdated_on_Visible ;
      private long A1462SistemaDePara_OrigemId ;
      private long A1467ContratoServicosDePara_OrigenId ;
      private long A1469ContratoServicosDePara_OrigenId2 ;
      private long AV6Projeto ;
      private long GRID1_nCurrentRecord ;
      private long GRID2_nCurrentRecord ;
      private long GRID2_nFirstRecordOnPage ;
      private long GRID1_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_202_idx="0001" ;
      private String GXKey ;
      private String sGXsfl_120_idx="0001" ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV44Versao ;
      private String AV9Execute ;
      private String A1461SistemaDePara_Origem ;
      private String A1466ContratoServicosDePara_Origem ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String edtavCtlid1_Internalname ;
      private String edtavCtlid2_Internalname ;
      private String edtavCtlname1_Internalname ;
      private String edtavCtlid3_Internalname ;
      private String edtavCtlname2_Internalname ;
      private String edtavCtlname3_Internalname ;
      private String edtavCtlname7_Internalname ;
      private String edtavCtlstart_date_Internalname ;
      private String edtavCtlid7_Internalname ;
      private String edtavCtlname4_Internalname ;
      private String edtavCtldue_date_Internalname ;
      private String edtavCtlname6_Internalname ;
      private String edtavCtlid6_Internalname ;
      private String edtavCtlname5_Internalname ;
      private String edtavCtlsubject_Internalname ;
      private String edtavCtldescription_Internalname ;
      private String edtavCtldone_ratio_Internalname ;
      private String edtavCtlestimated_hours_Internalname ;
      private String edtavCtlcreated_on_Internalname ;
      private String edtavCtlupdated_on_Internalname ;
      private String edtavCtlid_Internalname ;
      private String edtavCtlname_Internalname ;
      private String edtavCtlvalue_Internalname ;
      private String edtavUsr_nome_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavCount_Internalname ;
      private String edtavCtloffset_Internalname ;
      private String edtavCtllimit_Internalname ;
      private String edtavUsr_senha_Internalname ;
      private String cmbavSecure_Internalname ;
      private String AV27Host ;
      private String edtavHost_Internalname ;
      private String AV28Url ;
      private String edtavUrl_Internalname ;
      private String AV10Key ;
      private String edtavKey_Internalname ;
      private String cmbavProjeto_Internalname ;
      private String dynavContratada_codigo_Internalname ;
      private String cmbavTracker_Internalname ;
      private String dynavContrato_Internalname ;
      private String dynavServico_codigo_Internalname ;
      private String cmbavStatus_Internalname ;
      private String edtavCreated_from_Internalname ;
      private String edtavCreated_to_Internalname ;
      private String AV66Campo_Sistemas ;
      private String edtavCampo_sistemas_Internalname ;
      private String AV65Campo_Servicos ;
      private String edtavCampo_servicos_Internalname ;
      private String sGXsfl_120_fel_idx="0001" ;
      private String sGXsfl_202_fel_idx="0001" ;
      private String A1905Redmine_User ;
      private String A1381Redmine_Host ;
      private String A1382Redmine_Url ;
      private String A1383Redmine_Key ;
      private String A1391Redmine_Versao ;
      private String A1906Redmine_CampoSistema ;
      private String A1907Redmine_CampoServico ;
      private String tblTable2_Internalname ;
      private String bttBtnimportar_Internalname ;
      private String bttBtnconsultar_Internalname ;
      private String bttBtndeparasistemas_Internalname ;
      private String bttBtnimportar_Caption ;
      private String sStyleString ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String TempTags ;
      private String dynavContratada_codigo_Jsonclick ;
      private String lblTextblock7_Internalname ;
      private String lblTextblock7_Jsonclick ;
      private String dynavServico_codigo_Jsonclick ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock37_Internalname ;
      private String lblTextblock37_Jsonclick ;
      private String cmbavSecure_Jsonclick ;
      private String edtavHost_Jsonclick ;
      private String lblTextblock39_Internalname ;
      private String lblTextblock39_Jsonclick ;
      private String edtavUrl_Jsonclick ;
      private String lblTextblock38_Internalname ;
      private String lblTextblock38_Jsonclick ;
      private String edtavKey_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private String lblTextblock34_Internalname ;
      private String lblTextblock34_Jsonclick ;
      private String cmbavProjeto_Jsonclick ;
      private String lblTextblock41_Internalname ;
      private String lblTextblock41_Jsonclick ;
      private String lblTextblock36_Internalname ;
      private String lblTextblock36_Jsonclick ;
      private String cmbavTracker_Jsonclick ;
      private String lblTextblock44_Internalname ;
      private String lblTextblock44_Jsonclick ;
      private String dynavContrato_Jsonclick ;
      private String lblTextblock47_Internalname ;
      private String lblTextblock47_Jsonclick ;
      private String lblTextblock35_Internalname ;
      private String lblTextblock35_Jsonclick ;
      private String cmbavStatus_Jsonclick ;
      private String bttBtnimportar_Jsonclick ;
      private String lblTextblock42_Internalname ;
      private String lblTextblock42_Jsonclick ;
      private String edtavCreated_from_Jsonclick ;
      private String edtavCreated_to_Jsonclick ;
      private String lblTextblock45_Internalname ;
      private String lblTextblock45_Jsonclick ;
      private String edtavCampo_sistemas_Jsonclick ;
      private String bttBtnconsultar_Jsonclick ;
      private String lblTextblock46_Internalname ;
      private String lblTextblock46_Jsonclick ;
      private String edtavCampo_servicos_Jsonclick ;
      private String bttBtndeparasistemas_Jsonclick ;
      private String tblTable9_Internalname ;
      private String subGrid2_Internalname ;
      private String lblTextblock5_Caption ;
      private String lblTextblock6_Caption ;
      private String lblTextblock9_Caption ;
      private String lblTextblock12_Caption ;
      private String lblTextblock26_Caption ;
      private String lblTextblock15_Caption ;
      private String lblTextblock27_Caption ;
      private String lblTextblock21_Caption ;
      private String lblTextblock18_Caption ;
      private String lblTextblock24_Caption ;
      private String lblTextblock25_Caption ;
      private String lblTextblock28_Caption ;
      private String lblTextblock29_Caption ;
      private String lblTextblock30_Caption ;
      private String lblTextblock32_Caption ;
      private String lblTextblock33_Caption ;
      private String tblTable10_Internalname ;
      private String lblTextblock14_Internalname ;
      private String lblTextblock14_Jsonclick ;
      private String edtavCount_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavCtloffset_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavCtllimit_Jsonclick ;
      private String tblTable5_Internalname ;
      private String lblTextblock8_Internalname ;
      private String lblTextblock8_Jsonclick ;
      private String edtavUsr_nome_Jsonclick ;
      private String lblTextblock10_Internalname ;
      private String lblTextblock10_Jsonclick ;
      private String edtavUsr_senha_Jsonclick ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String ROClassString ;
      private String edtavCtlid_Jsonclick ;
      private String edtavCtlname_Jsonclick ;
      private String edtavCtlvalue_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock9_Internalname ;
      private String lblTextblock12_Internalname ;
      private String lblTextblock26_Internalname ;
      private String lblTextblock15_Internalname ;
      private String lblTextblock27_Internalname ;
      private String lblTextblock21_Internalname ;
      private String lblTextblock18_Internalname ;
      private String lblTextblock24_Internalname ;
      private String lblTextblock25_Internalname ;
      private String lblTextblock28_Internalname ;
      private String lblTextblock29_Internalname ;
      private String lblTextblock30_Internalname ;
      private String lblTextblock32_Internalname ;
      private String lblTextblock33_Internalname ;
      private String subGrid1_Internalname ;
      private String subGrid2_Class ;
      private String subGrid2_Linesclass ;
      private String lblTextblock5_Jsonclick ;
      private String edtavCtlid1_Jsonclick ;
      private String lblTextblock6_Jsonclick ;
      private String edtavCtlid2_Jsonclick ;
      private String edtavCtlname1_Jsonclick ;
      private String lblTextblock9_Jsonclick ;
      private String edtavCtlid3_Jsonclick ;
      private String edtavCtlname2_Jsonclick ;
      private String lblTextblock12_Jsonclick ;
      private String edtavCtlname3_Jsonclick ;
      private String edtavCtlname7_Jsonclick ;
      private String lblTextblock26_Jsonclick ;
      private String edtavCtlstart_date_Jsonclick ;
      private String lblTextblock15_Jsonclick ;
      private String edtavCtlid7_Jsonclick ;
      private String edtavCtlname4_Jsonclick ;
      private String lblTextblock27_Jsonclick ;
      private String edtavCtldue_date_Jsonclick ;
      private String lblTextblock21_Jsonclick ;
      private String edtavCtlname6_Jsonclick ;
      private String lblTextblock18_Jsonclick ;
      private String edtavCtlid6_Jsonclick ;
      private String edtavCtlname5_Jsonclick ;
      private String tblTable6_Internalname ;
      private String lblTextblock24_Jsonclick ;
      private String lblTextblock25_Jsonclick ;
      private String lblTextblock28_Jsonclick ;
      private String edtavCtldone_ratio_Jsonclick ;
      private String lblTextblock29_Jsonclick ;
      private String edtavCtlestimated_hours_Jsonclick ;
      private String lblTextblock30_Jsonclick ;
      private String tblTable8_Internalname ;
      private String lblTextblock32_Jsonclick ;
      private String edtavCtlcreated_on_Jsonclick ;
      private String lblTextblock33_Jsonclick ;
      private String edtavCtlupdated_on_Jsonclick ;
      private DateTime A1392ContagemResultado_RdmnUpdated ;
      private DateTime AV40Created_From ;
      private DateTime AV41Created_To ;
      private DateTime AV62Updated ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV54SemDePara ;
      private bool AV70SistemasValidados ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool n29Contratante_Codigo ;
      private bool A632Servico_Ativo ;
      private bool n632Servico_Ativo ;
      private bool n1905Redmine_User ;
      private bool n1391Redmine_Versao ;
      private bool n1906Redmine_CampoSistema ;
      private bool n1907Redmine_CampoServico ;
      private bool gx_BV120 ;
      private bool n1389ContagemResultado_RdmnIssueId ;
      private bool n1392ContagemResultado_RdmnUpdated ;
      private bool AV69ServicosValidados ;
      private bool n1469ContratoServicosDePara_OrigenId2 ;
      private bool n1467ContratoServicosDePara_OrigenId ;
      private String AV47Usr_Nome ;
      private String AV48Usr_Senha ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid Grid2Container ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid2Row ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid2Column ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavSecure ;
      private GXCombobox cmbavProjeto ;
      private GXCombobox dynavContratada_codigo ;
      private GXCombobox cmbavTracker ;
      private GXCombobox dynavContrato ;
      private GXCombobox dynavServico_codigo ;
      private GXCombobox cmbavStatus ;
      private IDataStoreProvider pr_default ;
      private int[] H00KD2_A40Contratada_PessoaCod ;
      private int[] H00KD2_A39Contratada_Codigo ;
      private String[] H00KD2_A41Contratada_PessoaNom ;
      private bool[] H00KD2_n41Contratada_PessoaNom ;
      private int[] H00KD2_A52Contratada_AreaTrabalhoCod ;
      private int[] H00KD3_A74Contrato_Codigo ;
      private String[] H00KD3_A77Contrato_Numero ;
      private int[] H00KD3_A39Contratada_Codigo ;
      private bool[] H00KD3_A92Contrato_Ativo ;
      private int[] H00KD4_A155Servico_Codigo ;
      private int[] H00KD4_A160ContratoServicos_Codigo ;
      private int[] H00KD4_A74Contrato_Codigo ;
      private int[] H00KD4_A39Contratada_Codigo ;
      private bool[] H00KD4_A632Servico_Ativo ;
      private bool[] H00KD4_n632Servico_Ativo ;
      private String[] H00KD4_A826ContratoServicos_ServicoSigla ;
      private int[] H00KD5_A830AreaTrabalho_ServicoPadrao ;
      private bool[] H00KD5_n830AreaTrabalho_ServicoPadrao ;
      private int[] H00KD5_A29Contratante_Codigo ;
      private bool[] H00KD5_n29Contratante_Codigo ;
      private bool[] H00KD5_A632Servico_Ativo ;
      private bool[] H00KD5_n632Servico_Ativo ;
      private int[] H00KD5_A5AreaTrabalho_Codigo ;
      private int[] H00KD6_A1380Redmine_Codigo ;
      private int[] H00KD6_A1384Redmine_ContratanteCod ;
      private String[] H00KD6_A1905Redmine_User ;
      private bool[] H00KD6_n1905Redmine_User ;
      private short[] H00KD6_A1904Redmine_Secure ;
      private String[] H00KD6_A1381Redmine_Host ;
      private String[] H00KD6_A1382Redmine_Url ;
      private String[] H00KD6_A1383Redmine_Key ;
      private String[] H00KD6_A1391Redmine_Versao ;
      private bool[] H00KD6_n1391Redmine_Versao ;
      private String[] H00KD6_A1906Redmine_CampoSistema ;
      private bool[] H00KD6_n1906Redmine_CampoSistema ;
      private String[] H00KD6_A1907Redmine_CampoServico ;
      private bool[] H00KD6_n1907Redmine_CampoServico ;
      private int[] H00KD7_A456ContagemResultado_Codigo ;
      private int[] H00KD7_A1389ContagemResultado_RdmnIssueId ;
      private bool[] H00KD7_n1389ContagemResultado_RdmnIssueId ;
      private DateTime[] H00KD7_A1392ContagemResultado_RdmnUpdated ;
      private bool[] H00KD7_n1392ContagemResultado_RdmnUpdated ;
      private int[] H00KD8_A127Sistema_Codigo ;
      private int[] H00KD8_A1460SistemaDePara_Codigo ;
      private long[] H00KD8_A1462SistemaDePara_OrigemId ;
      private String[] H00KD8_A1461SistemaDePara_Origem ;
      private int[] H00KD9_A160ContratoServicos_Codigo ;
      private int[] H00KD9_A1465ContratoServicosDePara_Codigo ;
      private long[] H00KD9_A1469ContratoServicosDePara_OrigenId2 ;
      private bool[] H00KD9_n1469ContratoServicosDePara_OrigenId2 ;
      private long[] H00KD9_A1467ContratoServicosDePara_OrigenId ;
      private bool[] H00KD9_n1467ContratoServicosDePara_OrigenId ;
      private String[] H00KD9_A1466ContratoServicosDePara_Origem ;
      private int[] H00KD10_A160ContratoServicos_Codigo ;
      private int[] H00KD10_A1465ContratoServicosDePara_Codigo ;
      private long[] H00KD10_A1469ContratoServicosDePara_OrigenId2 ;
      private bool[] H00KD10_n1469ContratoServicosDePara_OrigenId2 ;
      private long[] H00KD10_A1467ContratoServicosDePara_OrigenId ;
      private bool[] H00KD10_n1467ContratoServicosDePara_OrigenId ;
      private String[] H00KD10_A1466ContratoServicosDePara_Origem ;
      private int[] H00KD11_A160ContratoServicos_Codigo ;
      private int[] H00KD11_A1465ContratoServicosDePara_Codigo ;
      private long[] H00KD11_A1469ContratoServicosDePara_OrigenId2 ;
      private bool[] H00KD11_n1469ContratoServicosDePara_OrigenId2 ;
      private long[] H00KD11_A1467ContratoServicosDePara_OrigenId ;
      private bool[] H00KD11_n1467ContratoServicosDePara_OrigenId ;
      private String[] H00KD11_A1466ContratoServicosDePara_Origem ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpClient AV5httpclient ;
      private IGxSession AV45WebSession ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Redmineuser_memberships_membership ))]
      private IGxCollection AV51ProjetoSemDePara ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RedmineStatus_issue_status ))]
      private IGxCollection AV57StatusSemDePara ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RedmineTrackers_tracker ))]
      private IGxCollection AV56TrackersSemDePara ;
      private GXWebForm Form ;
      private SdtSDT_Parametros AV64SDT_Parametros ;
      private wwpbaseobjects.SdtWWPContext AV50WWPContext ;
      private SdtSDT_RedmineIssues AV7SDT_Issues ;
      private SdtSDT_RedmineIssues AV59SDT_Consulta ;
      private SdtSDT_RedmineIssues_issue AV55Issue ;
      private SdtSDT_Redmineuser AV39SDT_User ;
      private SdtSDT_Redmineuser_memberships_membership AV14ProjectItem ;
      private SdtSDT_Redmineuser_memberships_membership AV49User ;
      private SdtSDT_RedmineStatus AV15SDT_Status ;
      private SdtSDT_RedmineStatus_issue_status AV16StatusItem ;
      private SdtSDT_RedmineTrackers AV17SDT_Trackers ;
      private SdtSDT_RedmineTrackers_tracker AV25TrackersItem ;
   }

   public class wp_wdsldeos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KD2 ;
          prmH00KD2 = new Object[] {
          new Object[] {"@AV50WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KD3 ;
          prmH00KD3 = new Object[] {
          new Object[] {"@AV33Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KD4 ;
          prmH00KD4 = new Object[] {
          new Object[] {"@AV68Contrato",SqlDbType.Int,6,0} ,
          new Object[] {"@AV33Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KD5 ;
          prmH00KD5 = new Object[] {
          new Object[] {"@AV50WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KD6 ;
          prmH00KD6 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KD7 ;
          prmH00KD7 = new Object[] {
          new Object[] {"@AV55Issue__Id",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00KD8 ;
          prmH00KD8 = new Object[] {
          new Object[] {"@AV55Issue__Project__Id",SqlDbType.SmallInt,2,0}
          } ;
          Object[] prmH00KD9 ;
          prmH00KD9 = new Object[] {
          new Object[] {"@AV55Issue__Tracker__Id",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV55Issue__Status__Id",SqlDbType.SmallInt,2,0}
          } ;
          Object[] prmH00KD10 ;
          prmH00KD10 = new Object[] {
          new Object[] {"@AV55Issue__Tracker__Id",SqlDbType.SmallInt,2,0}
          } ;
          Object[] prmH00KD11 ;
          prmH00KD11 = new Object[] {
          new Object[] {"@AV55Issue__Status__Id",SqlDbType.SmallInt,2,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KD2", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV50WWPC_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KD2,0,0,true,false )
             ,new CursorDef("H00KD3", "SELECT [Contrato_Codigo], [Contrato_Numero], [Contratada_Codigo], [Contrato_Ativo] FROM [Contrato] WITH (NOLOCK) WHERE ([Contrato_Ativo] = 1) AND ([Contratada_Codigo] = @AV33Contratada_Codigo) ORDER BY [Contrato_Numero] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KD3,0,0,true,false )
             ,new CursorDef("H00KD4", "SELECT T2.[Servico_Codigo], T1.[ContratoServicos_Codigo], T1.[Contrato_Codigo], T3.[Contratada_Codigo], T2.[Servico_Ativo], T2.[Servico_Sigla] AS ContratoServicos_ServicoSigla FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T2.[Servico_Ativo] = 1) AND (T1.[Contrato_Codigo] = @AV68Contrato) AND (T3.[Contratada_Codigo] = @AV33Contratada_Codigo) ORDER BY [ContratoServicos_ServicoSigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KD4,0,0,true,false )
             ,new CursorDef("H00KD5", "SELECT TOP 1 T1.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, T1.[Contratante_Codigo], T2.[Servico_Ativo], T1.[AreaTrabalho_Codigo] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[AreaTrabalho_ServicoPadrao]) WHERE T1.[AreaTrabalho_Codigo] = @AV50WWPC_1Areatrabalho_codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KD5,1,0,true,true )
             ,new CursorDef("H00KD6", "SELECT TOP 1 [Redmine_Codigo], [Redmine_ContratanteCod], [Redmine_User], [Redmine_Secure], [Redmine_Host], [Redmine_Url], [Redmine_Key], [Redmine_Versao], [Redmine_CampoSistema], [Redmine_CampoServico] FROM [Redmine] WITH (NOLOCK) WHERE [Redmine_ContratanteCod] = @Contratante_Codigo ORDER BY [Redmine_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KD6,1,0,false,true )
             ,new CursorDef("H00KD7", "SELECT [ContagemResultado_Codigo], [ContagemResultado_RdmnIssueId], [ContagemResultado_RdmnUpdated] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_RdmnIssueId] = @AV55Issue__Id ORDER BY [ContagemResultado_RdmnIssueId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KD7,100,0,false,false )
             ,new CursorDef("H00KD8", "SELECT TOP 1 [Sistema_Codigo], [SistemaDePara_Codigo], [SistemaDePara_OrigemId], [SistemaDePara_Origem] FROM [SistemaDePara] WITH (NOLOCK) WHERE [SistemaDePara_Origem] = 'R' and [SistemaDePara_OrigemId] = @AV55Issue__Project__Id ORDER BY [SistemaDePara_Origem] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KD8,1,0,false,true )
             ,new CursorDef("H00KD9", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo], [ContratoServicosDePara_OrigenId2], [ContratoServicosDePara_OrigenId], [ContratoServicosDePara_Origem] FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE ([ContratoServicosDePara_OrigenId] = @AV55Issue__Tracker__Id) AND ([ContratoServicosDePara_OrigenId2] = @AV55Issue__Status__Id) AND ([ContratoServicosDePara_Origem] = 'R') ORDER BY [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KD9,1,0,false,true )
             ,new CursorDef("H00KD10", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo], [ContratoServicosDePara_OrigenId2], [ContratoServicosDePara_OrigenId], [ContratoServicosDePara_Origem] FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE ([ContratoServicosDePara_OrigenId] = @AV55Issue__Tracker__Id) AND ([ContratoServicosDePara_OrigenId2] IS NULL) AND ([ContratoServicosDePara_Origem] = 'R') ORDER BY [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KD10,1,0,false,true )
             ,new CursorDef("H00KD11", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo], [ContratoServicosDePara_OrigenId2], [ContratoServicosDePara_OrigenId], [ContratoServicosDePara_Origem] FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE ([ContratoServicosDePara_OrigenId] IS NULL) AND ([ContratoServicosDePara_OrigenId2] = @AV55Issue__Status__Id) AND ([ContratoServicosDePara_Origem] = 'R') ORDER BY [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KD11,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 15) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 100) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 50) ;
                ((String[]) buf[8])[0] = rslt.getString(8, 20) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((String[]) buf[10])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((String[]) buf[12])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((long[]) buf[4])[0] = rslt.getLong(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((long[]) buf[4])[0] = rslt.getLong(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((long[]) buf[4])[0] = rslt.getLong(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
       }
    }

 }

}
