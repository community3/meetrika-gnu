/*
               File: FuncaoDadosPreImp
        Description: Funcao Dados Pre Importa��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:27:45.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaodadospreimp : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"FUNCAODADOSPREIMP_FNDADOSCODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAFUNCAODADOSPREIMP_FNDADOSCODIGO3G152( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A1261FuncaoDadosPreImp_FnDadosCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1261FuncaoDadosPreImp_FnDadosCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A1261FuncaoDadosPreImp_FnDadosCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynFuncaoDadosPreImp_FnDadosCodigo.Name = "FUNCAODADOSPREIMP_FNDADOSCODIGO";
         dynFuncaoDadosPreImp_FnDadosCodigo.WebTags = "";
         cmbFuncaoDadosPreImp_Tipo.Name = "FUNCAODADOSPREIMP_TIPO";
         cmbFuncaoDadosPreImp_Tipo.WebTags = "";
         cmbFuncaoDadosPreImp_Tipo.addItem("ALI", "ALI", 0);
         cmbFuncaoDadosPreImp_Tipo.addItem("AIE", "AIE", 0);
         cmbFuncaoDadosPreImp_Tipo.addItem("DC", "DC", 0);
         if ( cmbFuncaoDadosPreImp_Tipo.ItemCount > 0 )
         {
            A1253FuncaoDadosPreImp_Tipo = cmbFuncaoDadosPreImp_Tipo.getValidValue(A1253FuncaoDadosPreImp_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1253FuncaoDadosPreImp_Tipo", A1253FuncaoDadosPreImp_Tipo);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Funcao Dados Pre Importa��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public funcaodadospreimp( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public funcaodadospreimp( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynFuncaoDadosPreImp_FnDadosCodigo = new GXCombobox();
         cmbFuncaoDadosPreImp_Tipo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynFuncaoDadosPreImp_FnDadosCodigo.ItemCount > 0 )
         {
            A1261FuncaoDadosPreImp_FnDadosCodigo = (int)(NumberUtil.Val( dynFuncaoDadosPreImp_FnDadosCodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1261FuncaoDadosPreImp_FnDadosCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0)));
         }
         if ( cmbFuncaoDadosPreImp_Tipo.ItemCount > 0 )
         {
            A1253FuncaoDadosPreImp_Tipo = cmbFuncaoDadosPreImp_Tipo.getValidValue(A1253FuncaoDadosPreImp_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1253FuncaoDadosPreImp_Tipo", A1253FuncaoDadosPreImp_Tipo);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3G152( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3G152e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3G152( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3G152( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3G152e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Funcao Dados Pre Importa��o", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_FuncaoDadosPreImp.htm");
            wb_table3_28_3G152( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_3G152e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3G152e( true) ;
         }
         else
         {
            wb_table1_2_3G152e( false) ;
         }
      }

      protected void wb_table3_28_3G152( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_3G152( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_3G152e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoDadosPreImp.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoDadosPreImp.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoDadosPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_3G152e( true) ;
         }
         else
         {
            wb_table3_28_3G152e( false) ;
         }
      }

      protected void wb_table4_34_3G152( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodadospreimp_fndadoscodigo_Internalname, "Dados", "", "", lblTextblockfuncaodadospreimp_fndadoscodigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynFuncaoDadosPreImp_FnDadosCodigo, dynFuncaoDadosPreImp_FnDadosCodigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0)), 1, dynFuncaoDadosPreImp_FnDadosCodigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynFuncaoDadosPreImp_FnDadosCodigo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_FuncaoDadosPreImp.htm");
            dynFuncaoDadosPreImp_FnDadosCodigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoDadosPreImp_FnDadosCodigo_Internalname, "Values", (String)(dynFuncaoDadosPreImp_FnDadosCodigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodadospreimp_sequencial_Internalname, "Pre Imp_Sequencial", "", "", lblTextblockfuncaodadospreimp_sequencial_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoDadosPreImp_Sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1259FuncaoDadosPreImp_Sequencial), 3, 0, ",", "")), ((edtFuncaoDadosPreImp_Sequencial_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1259FuncaoDadosPreImp_Sequencial), "ZZ9")) : context.localUtil.Format( (decimal)(A1259FuncaoDadosPreImp_Sequencial), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDadosPreImp_Sequencial_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDadosPreImp_Sequencial_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Sequencial", "right", false, "HLP_FuncaoDadosPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodadospreimp_tipo_Internalname, "Pre Imp_Tipo", "", "", lblTextblockfuncaodadospreimp_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoDadosPreImp_Tipo, cmbFuncaoDadosPreImp_Tipo_Internalname, StringUtil.RTrim( A1253FuncaoDadosPreImp_Tipo), 1, cmbFuncaoDadosPreImp_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbFuncaoDadosPreImp_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_FuncaoDadosPreImp.htm");
            cmbFuncaoDadosPreImp_Tipo.CurrentValue = StringUtil.RTrim( A1253FuncaoDadosPreImp_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDadosPreImp_Tipo_Internalname, "Values", (String)(cmbFuncaoDadosPreImp_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodadospreimp_derimp_Internalname, "Pre Imp_DERImp", "", "", lblTextblockfuncaodadospreimp_derimp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoDadosPreImp_DERImp_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1254FuncaoDadosPreImp_DERImp), 4, 0, ",", "")), ((edtFuncaoDadosPreImp_DERImp_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1254FuncaoDadosPreImp_DERImp), "ZZZ9")) : context.localUtil.Format( (decimal)(A1254FuncaoDadosPreImp_DERImp), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDadosPreImp_DERImp_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDadosPreImp_DERImp_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoDadosPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodadospreimp_raimp_Internalname, "Pre Imp_RAImp", "", "", lblTextblockfuncaodadospreimp_raimp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoDadosPreImp_RAImp_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1255FuncaoDadosPreImp_RAImp), 4, 0, ",", "")), ((edtFuncaoDadosPreImp_RAImp_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1255FuncaoDadosPreImp_RAImp), "ZZZ9")) : context.localUtil.Format( (decimal)(A1255FuncaoDadosPreImp_RAImp), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoDadosPreImp_RAImp_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoDadosPreImp_RAImp_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoDadosPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodadospreimp_observacao_Internalname, "Pre Imp_Observacao", "", "", lblTextblockfuncaodadospreimp_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoDadosPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"FUNCAODADOSPREIMP_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_3G152e( true) ;
         }
         else
         {
            wb_table4_34_3G152e( false) ;
         }
      }

      protected void wb_table2_5_3G152( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoDadosPreImp.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3G152e( true) ;
         }
         else
         {
            wb_table2_5_3G152e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynFuncaoDadosPreImp_FnDadosCodigo.CurrentValue = cgiGet( dynFuncaoDadosPreImp_FnDadosCodigo_Internalname);
               A1261FuncaoDadosPreImp_FnDadosCodigo = (int)(NumberUtil.Val( cgiGet( dynFuncaoDadosPreImp_FnDadosCodigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1261FuncaoDadosPreImp_FnDadosCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoDadosPreImp_Sequencial_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoDadosPreImp_Sequencial_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAODADOSPREIMP_SEQUENCIAL");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoDadosPreImp_Sequencial_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1259FuncaoDadosPreImp_Sequencial = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1259FuncaoDadosPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1259FuncaoDadosPreImp_Sequencial), 3, 0)));
               }
               else
               {
                  A1259FuncaoDadosPreImp_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDadosPreImp_Sequencial_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1259FuncaoDadosPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1259FuncaoDadosPreImp_Sequencial), 3, 0)));
               }
               cmbFuncaoDadosPreImp_Tipo.CurrentValue = cgiGet( cmbFuncaoDadosPreImp_Tipo_Internalname);
               A1253FuncaoDadosPreImp_Tipo = cgiGet( cmbFuncaoDadosPreImp_Tipo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1253FuncaoDadosPreImp_Tipo", A1253FuncaoDadosPreImp_Tipo);
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoDadosPreImp_DERImp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoDadosPreImp_DERImp_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAODADOSPREIMP_DERIMP");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoDadosPreImp_DERImp_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1254FuncaoDadosPreImp_DERImp = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1254FuncaoDadosPreImp_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1254FuncaoDadosPreImp_DERImp), 4, 0)));
               }
               else
               {
                  A1254FuncaoDadosPreImp_DERImp = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDadosPreImp_DERImp_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1254FuncaoDadosPreImp_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1254FuncaoDadosPreImp_DERImp), 4, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoDadosPreImp_RAImp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoDadosPreImp_RAImp_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAODADOSPREIMP_RAIMP");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoDadosPreImp_RAImp_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1255FuncaoDadosPreImp_RAImp = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1255FuncaoDadosPreImp_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1255FuncaoDadosPreImp_RAImp), 4, 0)));
               }
               else
               {
                  A1255FuncaoDadosPreImp_RAImp = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDadosPreImp_RAImp_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1255FuncaoDadosPreImp_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1255FuncaoDadosPreImp_RAImp), 4, 0)));
               }
               /* Read saved values. */
               Z1261FuncaoDadosPreImp_FnDadosCodigo = (int)(context.localUtil.CToN( cgiGet( "Z1261FuncaoDadosPreImp_FnDadosCodigo"), ",", "."));
               Z1259FuncaoDadosPreImp_Sequencial = (short)(context.localUtil.CToN( cgiGet( "Z1259FuncaoDadosPreImp_Sequencial"), ",", "."));
               Z1253FuncaoDadosPreImp_Tipo = cgiGet( "Z1253FuncaoDadosPreImp_Tipo");
               Z1254FuncaoDadosPreImp_DERImp = (short)(context.localUtil.CToN( cgiGet( "Z1254FuncaoDadosPreImp_DERImp"), ",", "."));
               Z1255FuncaoDadosPreImp_RAImp = (short)(context.localUtil.CToN( cgiGet( "Z1255FuncaoDadosPreImp_RAImp"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               A1256FuncaoDadosPreImp_Observacao = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO");
               Gx_mode = cgiGet( "vMODE");
               Funcaodadospreimp_observacao_Width = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Width");
               Funcaodadospreimp_observacao_Height = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Height");
               Funcaodadospreimp_observacao_Skin = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Skin");
               Funcaodadospreimp_observacao_Toolbar = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Toolbar");
               Funcaodadospreimp_observacao_Color = (int)(context.localUtil.CToN( cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Color"), ",", "."));
               Funcaodadospreimp_observacao_Enabled = StringUtil.StrToBool( cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Enabled"));
               Funcaodadospreimp_observacao_Class = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Class");
               Funcaodadospreimp_observacao_Customtoolbar = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Customtoolbar");
               Funcaodadospreimp_observacao_Customconfiguration = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Customconfiguration");
               Funcaodadospreimp_observacao_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Toolbarcancollapse"));
               Funcaodadospreimp_observacao_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Toolbarexpanded"));
               Funcaodadospreimp_observacao_Buttonpressedid = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Buttonpressedid");
               Funcaodadospreimp_observacao_Captionvalue = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Captionvalue");
               Funcaodadospreimp_observacao_Captionclass = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Captionclass");
               Funcaodadospreimp_observacao_Captionposition = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Captionposition");
               Funcaodadospreimp_observacao_Coltitle = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Coltitle");
               Funcaodadospreimp_observacao_Coltitlefont = cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Coltitlefont");
               Funcaodadospreimp_observacao_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Coltitlecolor"), ",", "."));
               Funcaodadospreimp_observacao_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Usercontroliscolumn"));
               Funcaodadospreimp_observacao_Visible = StringUtil.StrToBool( cgiGet( "FUNCAODADOSPREIMP_OBSERVACAO_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1261FuncaoDadosPreImp_FnDadosCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1261FuncaoDadosPreImp_FnDadosCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0)));
                  A1259FuncaoDadosPreImp_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1259FuncaoDadosPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1259FuncaoDadosPreImp_Sequencial), 3, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3G152( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes3G152( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption3G0( )
      {
      }

      protected void ZM3G152( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1253FuncaoDadosPreImp_Tipo = T003G3_A1253FuncaoDadosPreImp_Tipo[0];
               Z1254FuncaoDadosPreImp_DERImp = T003G3_A1254FuncaoDadosPreImp_DERImp[0];
               Z1255FuncaoDadosPreImp_RAImp = T003G3_A1255FuncaoDadosPreImp_RAImp[0];
            }
            else
            {
               Z1253FuncaoDadosPreImp_Tipo = A1253FuncaoDadosPreImp_Tipo;
               Z1254FuncaoDadosPreImp_DERImp = A1254FuncaoDadosPreImp_DERImp;
               Z1255FuncaoDadosPreImp_RAImp = A1255FuncaoDadosPreImp_RAImp;
            }
         }
         if ( GX_JID == -3 )
         {
            Z1259FuncaoDadosPreImp_Sequencial = A1259FuncaoDadosPreImp_Sequencial;
            Z1253FuncaoDadosPreImp_Tipo = A1253FuncaoDadosPreImp_Tipo;
            Z1254FuncaoDadosPreImp_DERImp = A1254FuncaoDadosPreImp_DERImp;
            Z1255FuncaoDadosPreImp_RAImp = A1255FuncaoDadosPreImp_RAImp;
            Z1256FuncaoDadosPreImp_Observacao = A1256FuncaoDadosPreImp_Observacao;
            Z1261FuncaoDadosPreImp_FnDadosCodigo = A1261FuncaoDadosPreImp_FnDadosCodigo;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAFUNCAODADOSPREIMP_FNDADOSCODIGO_html3G152( ) ;
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load3G152( )
      {
         /* Using cursor T003G5 */
         pr_default.execute(3, new Object[] {A1261FuncaoDadosPreImp_FnDadosCodigo, A1259FuncaoDadosPreImp_Sequencial});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound152 = 1;
            A1253FuncaoDadosPreImp_Tipo = T003G5_A1253FuncaoDadosPreImp_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1253FuncaoDadosPreImp_Tipo", A1253FuncaoDadosPreImp_Tipo);
            A1254FuncaoDadosPreImp_DERImp = T003G5_A1254FuncaoDadosPreImp_DERImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1254FuncaoDadosPreImp_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1254FuncaoDadosPreImp_DERImp), 4, 0)));
            A1255FuncaoDadosPreImp_RAImp = T003G5_A1255FuncaoDadosPreImp_RAImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1255FuncaoDadosPreImp_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1255FuncaoDadosPreImp_RAImp), 4, 0)));
            A1256FuncaoDadosPreImp_Observacao = T003G5_A1256FuncaoDadosPreImp_Observacao[0];
            ZM3G152( -3) ;
         }
         pr_default.close(3);
         OnLoadActions3G152( ) ;
      }

      protected void OnLoadActions3G152( )
      {
      }

      protected void CheckExtendedTable3G152( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T003G4 */
         pr_default.execute(2, new Object[] {A1261FuncaoDadosPreImp_FnDadosCodigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao Dados Pre Imp_Funcao Dados'.", "ForeignKeyNotFound", 1, "FUNCAODADOSPREIMP_FNDADOSCODIGO");
            AnyError = 1;
            GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         if ( ! ( ( StringUtil.StrCmp(A1253FuncaoDadosPreImp_Tipo, "ALI") == 0 ) || ( StringUtil.StrCmp(A1253FuncaoDadosPreImp_Tipo, "AIE") == 0 ) || ( StringUtil.StrCmp(A1253FuncaoDadosPreImp_Tipo, "DC") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Funcao Dados Pre Imp_Tipo fora do intervalo", "OutOfRange", 1, "FUNCAODADOSPREIMP_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbFuncaoDadosPreImp_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors3G152( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_4( int A1261FuncaoDadosPreImp_FnDadosCodigo )
      {
         /* Using cursor T003G6 */
         pr_default.execute(4, new Object[] {A1261FuncaoDadosPreImp_FnDadosCodigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao Dados Pre Imp_Funcao Dados'.", "ForeignKeyNotFound", 1, "FUNCAODADOSPREIMP_FNDADOSCODIGO");
            AnyError = 1;
            GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey3G152( )
      {
         /* Using cursor T003G7 */
         pr_default.execute(5, new Object[] {A1261FuncaoDadosPreImp_FnDadosCodigo, A1259FuncaoDadosPreImp_Sequencial});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound152 = 1;
         }
         else
         {
            RcdFound152 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003G3 */
         pr_default.execute(1, new Object[] {A1261FuncaoDadosPreImp_FnDadosCodigo, A1259FuncaoDadosPreImp_Sequencial});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3G152( 3) ;
            RcdFound152 = 1;
            A1259FuncaoDadosPreImp_Sequencial = T003G3_A1259FuncaoDadosPreImp_Sequencial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1259FuncaoDadosPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1259FuncaoDadosPreImp_Sequencial), 3, 0)));
            A1253FuncaoDadosPreImp_Tipo = T003G3_A1253FuncaoDadosPreImp_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1253FuncaoDadosPreImp_Tipo", A1253FuncaoDadosPreImp_Tipo);
            A1254FuncaoDadosPreImp_DERImp = T003G3_A1254FuncaoDadosPreImp_DERImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1254FuncaoDadosPreImp_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1254FuncaoDadosPreImp_DERImp), 4, 0)));
            A1255FuncaoDadosPreImp_RAImp = T003G3_A1255FuncaoDadosPreImp_RAImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1255FuncaoDadosPreImp_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1255FuncaoDadosPreImp_RAImp), 4, 0)));
            A1256FuncaoDadosPreImp_Observacao = T003G3_A1256FuncaoDadosPreImp_Observacao[0];
            A1261FuncaoDadosPreImp_FnDadosCodigo = T003G3_A1261FuncaoDadosPreImp_FnDadosCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1261FuncaoDadosPreImp_FnDadosCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0)));
            Z1261FuncaoDadosPreImp_FnDadosCodigo = A1261FuncaoDadosPreImp_FnDadosCodigo;
            Z1259FuncaoDadosPreImp_Sequencial = A1259FuncaoDadosPreImp_Sequencial;
            sMode152 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load3G152( ) ;
            if ( AnyError == 1 )
            {
               RcdFound152 = 0;
               InitializeNonKey3G152( ) ;
            }
            Gx_mode = sMode152;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound152 = 0;
            InitializeNonKey3G152( ) ;
            sMode152 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode152;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3G152( ) ;
         if ( RcdFound152 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound152 = 0;
         /* Using cursor T003G8 */
         pr_default.execute(6, new Object[] {A1261FuncaoDadosPreImp_FnDadosCodigo, A1259FuncaoDadosPreImp_Sequencial});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T003G8_A1261FuncaoDadosPreImp_FnDadosCodigo[0] < A1261FuncaoDadosPreImp_FnDadosCodigo ) || ( T003G8_A1261FuncaoDadosPreImp_FnDadosCodigo[0] == A1261FuncaoDadosPreImp_FnDadosCodigo ) && ( T003G8_A1259FuncaoDadosPreImp_Sequencial[0] < A1259FuncaoDadosPreImp_Sequencial ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T003G8_A1261FuncaoDadosPreImp_FnDadosCodigo[0] > A1261FuncaoDadosPreImp_FnDadosCodigo ) || ( T003G8_A1261FuncaoDadosPreImp_FnDadosCodigo[0] == A1261FuncaoDadosPreImp_FnDadosCodigo ) && ( T003G8_A1259FuncaoDadosPreImp_Sequencial[0] > A1259FuncaoDadosPreImp_Sequencial ) ) )
            {
               A1261FuncaoDadosPreImp_FnDadosCodigo = T003G8_A1261FuncaoDadosPreImp_FnDadosCodigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1261FuncaoDadosPreImp_FnDadosCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0)));
               A1259FuncaoDadosPreImp_Sequencial = T003G8_A1259FuncaoDadosPreImp_Sequencial[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1259FuncaoDadosPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1259FuncaoDadosPreImp_Sequencial), 3, 0)));
               RcdFound152 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound152 = 0;
         /* Using cursor T003G9 */
         pr_default.execute(7, new Object[] {A1261FuncaoDadosPreImp_FnDadosCodigo, A1259FuncaoDadosPreImp_Sequencial});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T003G9_A1261FuncaoDadosPreImp_FnDadosCodigo[0] > A1261FuncaoDadosPreImp_FnDadosCodigo ) || ( T003G9_A1261FuncaoDadosPreImp_FnDadosCodigo[0] == A1261FuncaoDadosPreImp_FnDadosCodigo ) && ( T003G9_A1259FuncaoDadosPreImp_Sequencial[0] > A1259FuncaoDadosPreImp_Sequencial ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T003G9_A1261FuncaoDadosPreImp_FnDadosCodigo[0] < A1261FuncaoDadosPreImp_FnDadosCodigo ) || ( T003G9_A1261FuncaoDadosPreImp_FnDadosCodigo[0] == A1261FuncaoDadosPreImp_FnDadosCodigo ) && ( T003G9_A1259FuncaoDadosPreImp_Sequencial[0] < A1259FuncaoDadosPreImp_Sequencial ) ) )
            {
               A1261FuncaoDadosPreImp_FnDadosCodigo = T003G9_A1261FuncaoDadosPreImp_FnDadosCodigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1261FuncaoDadosPreImp_FnDadosCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0)));
               A1259FuncaoDadosPreImp_Sequencial = T003G9_A1259FuncaoDadosPreImp_Sequencial[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1259FuncaoDadosPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1259FuncaoDadosPreImp_Sequencial), 3, 0)));
               RcdFound152 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3G152( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3G152( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound152 == 1 )
            {
               if ( ( A1261FuncaoDadosPreImp_FnDadosCodigo != Z1261FuncaoDadosPreImp_FnDadosCodigo ) || ( A1259FuncaoDadosPreImp_Sequencial != Z1259FuncaoDadosPreImp_Sequencial ) )
               {
                  A1261FuncaoDadosPreImp_FnDadosCodigo = Z1261FuncaoDadosPreImp_FnDadosCodigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1261FuncaoDadosPreImp_FnDadosCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0)));
                  A1259FuncaoDadosPreImp_Sequencial = Z1259FuncaoDadosPreImp_Sequencial;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1259FuncaoDadosPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1259FuncaoDadosPreImp_Sequencial), 3, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "FUNCAODADOSPREIMP_FNDADOSCODIGO");
                  AnyError = 1;
                  GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update3G152( ) ;
                  GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A1261FuncaoDadosPreImp_FnDadosCodigo != Z1261FuncaoDadosPreImp_FnDadosCodigo ) || ( A1259FuncaoDadosPreImp_Sequencial != Z1259FuncaoDadosPreImp_Sequencial ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3G152( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "FUNCAODADOSPREIMP_FNDADOSCODIGO");
                     AnyError = 1;
                     GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3G152( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A1261FuncaoDadosPreImp_FnDadosCodigo != Z1261FuncaoDadosPreImp_FnDadosCodigo ) || ( A1259FuncaoDadosPreImp_Sequencial != Z1259FuncaoDadosPreImp_Sequencial ) )
         {
            A1261FuncaoDadosPreImp_FnDadosCodigo = Z1261FuncaoDadosPreImp_FnDadosCodigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1261FuncaoDadosPreImp_FnDadosCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0)));
            A1259FuncaoDadosPreImp_Sequencial = Z1259FuncaoDadosPreImp_Sequencial;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1259FuncaoDadosPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1259FuncaoDadosPreImp_Sequencial), 3, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "FUNCAODADOSPREIMP_FNDADOSCODIGO");
            AnyError = 1;
            GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound152 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "FUNCAODADOSPREIMP_FNDADOSCODIGO");
            AnyError = 1;
            GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = cmbFuncaoDadosPreImp_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3G152( ) ;
         if ( RcdFound152 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbFuncaoDadosPreImp_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3G152( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound152 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbFuncaoDadosPreImp_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound152 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbFuncaoDadosPreImp_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3G152( ) ;
         if ( RcdFound152 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound152 != 0 )
            {
               ScanNext3G152( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbFuncaoDadosPreImp_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3G152( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency3G152( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003G2 */
            pr_default.execute(0, new Object[] {A1261FuncaoDadosPreImp_FnDadosCodigo, A1259FuncaoDadosPreImp_Sequencial});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncaoDadosPreImp"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1253FuncaoDadosPreImp_Tipo, T003G2_A1253FuncaoDadosPreImp_Tipo[0]) != 0 ) || ( Z1254FuncaoDadosPreImp_DERImp != T003G2_A1254FuncaoDadosPreImp_DERImp[0] ) || ( Z1255FuncaoDadosPreImp_RAImp != T003G2_A1255FuncaoDadosPreImp_RAImp[0] ) )
            {
               if ( StringUtil.StrCmp(Z1253FuncaoDadosPreImp_Tipo, T003G2_A1253FuncaoDadosPreImp_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaodadospreimp:[seudo value changed for attri]"+"FuncaoDadosPreImp_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z1253FuncaoDadosPreImp_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T003G2_A1253FuncaoDadosPreImp_Tipo[0]);
               }
               if ( Z1254FuncaoDadosPreImp_DERImp != T003G2_A1254FuncaoDadosPreImp_DERImp[0] )
               {
                  GXUtil.WriteLog("funcaodadospreimp:[seudo value changed for attri]"+"FuncaoDadosPreImp_DERImp");
                  GXUtil.WriteLogRaw("Old: ",Z1254FuncaoDadosPreImp_DERImp);
                  GXUtil.WriteLogRaw("Current: ",T003G2_A1254FuncaoDadosPreImp_DERImp[0]);
               }
               if ( Z1255FuncaoDadosPreImp_RAImp != T003G2_A1255FuncaoDadosPreImp_RAImp[0] )
               {
                  GXUtil.WriteLog("funcaodadospreimp:[seudo value changed for attri]"+"FuncaoDadosPreImp_RAImp");
                  GXUtil.WriteLogRaw("Old: ",Z1255FuncaoDadosPreImp_RAImp);
                  GXUtil.WriteLogRaw("Current: ",T003G2_A1255FuncaoDadosPreImp_RAImp[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"FuncaoDadosPreImp"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3G152( )
      {
         BeforeValidate3G152( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3G152( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3G152( 0) ;
            CheckOptimisticConcurrency3G152( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3G152( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3G152( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003G10 */
                     pr_default.execute(8, new Object[] {A1259FuncaoDadosPreImp_Sequencial, A1253FuncaoDadosPreImp_Tipo, A1254FuncaoDadosPreImp_DERImp, A1255FuncaoDadosPreImp_RAImp, A1256FuncaoDadosPreImp_Observacao, A1261FuncaoDadosPreImp_FnDadosCodigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncaoDadosPreImp") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3G0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3G152( ) ;
            }
            EndLevel3G152( ) ;
         }
         CloseExtendedTableCursors3G152( ) ;
      }

      protected void Update3G152( )
      {
         BeforeValidate3G152( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3G152( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3G152( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3G152( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3G152( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003G11 */
                     pr_default.execute(9, new Object[] {A1253FuncaoDadosPreImp_Tipo, A1254FuncaoDadosPreImp_DERImp, A1255FuncaoDadosPreImp_RAImp, A1256FuncaoDadosPreImp_Observacao, A1261FuncaoDadosPreImp_FnDadosCodigo, A1259FuncaoDadosPreImp_Sequencial});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncaoDadosPreImp") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncaoDadosPreImp"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3G152( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption3G0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3G152( ) ;
         }
         CloseExtendedTableCursors3G152( ) ;
      }

      protected void DeferredUpdate3G152( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate3G152( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3G152( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3G152( ) ;
            AfterConfirm3G152( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3G152( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003G12 */
                  pr_default.execute(10, new Object[] {A1261FuncaoDadosPreImp_FnDadosCodigo, A1259FuncaoDadosPreImp_Sequencial});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncaoDadosPreImp") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound152 == 0 )
                        {
                           InitAll3G152( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption3G0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode152 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel3G152( ) ;
         Gx_mode = sMode152;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls3G152( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel3G152( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3G152( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "FuncaoDadosPreImp");
            if ( AnyError == 0 )
            {
               ConfirmValues3G0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "FuncaoDadosPreImp");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3G152( )
      {
         /* Using cursor T003G13 */
         pr_default.execute(11);
         RcdFound152 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound152 = 1;
            A1261FuncaoDadosPreImp_FnDadosCodigo = T003G13_A1261FuncaoDadosPreImp_FnDadosCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1261FuncaoDadosPreImp_FnDadosCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0)));
            A1259FuncaoDadosPreImp_Sequencial = T003G13_A1259FuncaoDadosPreImp_Sequencial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1259FuncaoDadosPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1259FuncaoDadosPreImp_Sequencial), 3, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3G152( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound152 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound152 = 1;
            A1261FuncaoDadosPreImp_FnDadosCodigo = T003G13_A1261FuncaoDadosPreImp_FnDadosCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1261FuncaoDadosPreImp_FnDadosCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0)));
            A1259FuncaoDadosPreImp_Sequencial = T003G13_A1259FuncaoDadosPreImp_Sequencial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1259FuncaoDadosPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1259FuncaoDadosPreImp_Sequencial), 3, 0)));
         }
      }

      protected void ScanEnd3G152( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm3G152( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3G152( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3G152( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3G152( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3G152( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3G152( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3G152( )
      {
         dynFuncaoDadosPreImp_FnDadosCodigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynFuncaoDadosPreImp_FnDadosCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynFuncaoDadosPreImp_FnDadosCodigo.Enabled), 5, 0)));
         edtFuncaoDadosPreImp_Sequencial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDadosPreImp_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDadosPreImp_Sequencial_Enabled), 5, 0)));
         cmbFuncaoDadosPreImp_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDadosPreImp_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoDadosPreImp_Tipo.Enabled), 5, 0)));
         edtFuncaoDadosPreImp_DERImp_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDadosPreImp_DERImp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDadosPreImp_DERImp_Enabled), 5, 0)));
         edtFuncaoDadosPreImp_RAImp_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDadosPreImp_RAImp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDadosPreImp_RAImp_Enabled), 5, 0)));
         Funcaodadospreimp_observacao_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Funcaodadospreimp_observacao_Internalname, "Enabled", StringUtil.BoolToStr( Funcaodadospreimp_observacao_Enabled));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3G0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117274687");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaodadospreimp.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1261FuncaoDadosPreImp_FnDadosCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1259FuncaoDadosPreImp_Sequencial", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1259FuncaoDadosPreImp_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1253FuncaoDadosPreImp_Tipo", StringUtil.RTrim( Z1253FuncaoDadosPreImp_Tipo));
         GxWebStd.gx_hidden_field( context, "Z1254FuncaoDadosPreImp_DERImp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1254FuncaoDadosPreImp_DERImp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1255FuncaoDadosPreImp_RAImp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1255FuncaoDadosPreImp_RAImp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOSPREIMP_OBSERVACAO", A1256FuncaoDadosPreImp_Observacao);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOSPREIMP_OBSERVACAO_Enabled", StringUtil.BoolToStr( Funcaodadospreimp_observacao_Enabled));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("funcaodadospreimp.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "FuncaoDadosPreImp" ;
      }

      public override String GetPgmdesc( )
      {
         return "Funcao Dados Pre Importa��o" ;
      }

      protected void InitializeNonKey3G152( )
      {
         A1253FuncaoDadosPreImp_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1253FuncaoDadosPreImp_Tipo", A1253FuncaoDadosPreImp_Tipo);
         A1254FuncaoDadosPreImp_DERImp = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1254FuncaoDadosPreImp_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1254FuncaoDadosPreImp_DERImp), 4, 0)));
         A1255FuncaoDadosPreImp_RAImp = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1255FuncaoDadosPreImp_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1255FuncaoDadosPreImp_RAImp), 4, 0)));
         A1256FuncaoDadosPreImp_Observacao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1256FuncaoDadosPreImp_Observacao", A1256FuncaoDadosPreImp_Observacao);
         Z1253FuncaoDadosPreImp_Tipo = "";
         Z1254FuncaoDadosPreImp_DERImp = 0;
         Z1255FuncaoDadosPreImp_RAImp = 0;
      }

      protected void InitAll3G152( )
      {
         A1261FuncaoDadosPreImp_FnDadosCodigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1261FuncaoDadosPreImp_FnDadosCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0)));
         A1259FuncaoDadosPreImp_Sequencial = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1259FuncaoDadosPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1259FuncaoDadosPreImp_Sequencial), 3, 0)));
         InitializeNonKey3G152( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117274692");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("funcaodadospreimp.js", "?20203117274692");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockfuncaodadospreimp_fndadoscodigo_Internalname = "TEXTBLOCKFUNCAODADOSPREIMP_FNDADOSCODIGO";
         dynFuncaoDadosPreImp_FnDadosCodigo_Internalname = "FUNCAODADOSPREIMP_FNDADOSCODIGO";
         lblTextblockfuncaodadospreimp_sequencial_Internalname = "TEXTBLOCKFUNCAODADOSPREIMP_SEQUENCIAL";
         edtFuncaoDadosPreImp_Sequencial_Internalname = "FUNCAODADOSPREIMP_SEQUENCIAL";
         lblTextblockfuncaodadospreimp_tipo_Internalname = "TEXTBLOCKFUNCAODADOSPREIMP_TIPO";
         cmbFuncaoDadosPreImp_Tipo_Internalname = "FUNCAODADOSPREIMP_TIPO";
         lblTextblockfuncaodadospreimp_derimp_Internalname = "TEXTBLOCKFUNCAODADOSPREIMP_DERIMP";
         edtFuncaoDadosPreImp_DERImp_Internalname = "FUNCAODADOSPREIMP_DERIMP";
         lblTextblockfuncaodadospreimp_raimp_Internalname = "TEXTBLOCKFUNCAODADOSPREIMP_RAIMP";
         edtFuncaoDadosPreImp_RAImp_Internalname = "FUNCAODADOSPREIMP_RAIMP";
         lblTextblockfuncaodadospreimp_observacao_Internalname = "TEXTBLOCKFUNCAODADOSPREIMP_OBSERVACAO";
         Funcaodadospreimp_observacao_Internalname = "FUNCAODADOSPREIMP_OBSERVACAO";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Funcao Dados Pre Importa��o";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         Funcaodadospreimp_observacao_Enabled = Convert.ToBoolean( 1);
         edtFuncaoDadosPreImp_RAImp_Jsonclick = "";
         edtFuncaoDadosPreImp_RAImp_Enabled = 1;
         edtFuncaoDadosPreImp_DERImp_Jsonclick = "";
         edtFuncaoDadosPreImp_DERImp_Enabled = 1;
         cmbFuncaoDadosPreImp_Tipo_Jsonclick = "";
         cmbFuncaoDadosPreImp_Tipo.Enabled = 1;
         edtFuncaoDadosPreImp_Sequencial_Jsonclick = "";
         edtFuncaoDadosPreImp_Sequencial_Enabled = 1;
         dynFuncaoDadosPreImp_FnDadosCodigo_Jsonclick = "";
         dynFuncaoDadosPreImp_FnDadosCodigo.Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAFUNCAODADOSPREIMP_FNDADOSCODIGO3G152( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAFUNCAODADOSPREIMP_FNDADOSCODIGO_data3G152( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAFUNCAODADOSPREIMP_FNDADOSCODIGO_html3G152( )
      {
         int gxdynajaxvalue ;
         GXDLAFUNCAODADOSPREIMP_FNDADOSCODIGO_data3G152( ) ;
         gxdynajaxindex = 1;
         dynFuncaoDadosPreImp_FnDadosCodigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynFuncaoDadosPreImp_FnDadosCodigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAFUNCAODADOSPREIMP_FNDADOSCODIGO_data3G152( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor T003G14 */
         pr_default.execute(12);
         while ( (pr_default.getStatus(12) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T003G14_A1261FuncaoDadosPreImp_FnDadosCodigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T003G14_A369FuncaoDados_Nome[0]);
            pr_default.readNext(12);
         }
         pr_default.close(12);
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T003G15 */
         pr_default.execute(13, new Object[] {A1261FuncaoDadosPreImp_FnDadosCodigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao Dados Pre Imp_Funcao Dados'.", "ForeignKeyNotFound", 1, "FUNCAODADOSPREIMP_FNDADOSCODIGO");
            AnyError = 1;
            GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(13);
         GX_FocusControl = cmbFuncaoDadosPreImp_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Funcaodadospreimp_fndadoscodigo( GXCombobox dynGX_Parm1 )
      {
         dynFuncaoDadosPreImp_FnDadosCodigo = dynGX_Parm1;
         A1261FuncaoDadosPreImp_FnDadosCodigo = (int)(NumberUtil.Val( dynFuncaoDadosPreImp_FnDadosCodigo.CurrentValue, "."));
         /* Using cursor T003G16 */
         pr_default.execute(14, new Object[] {A1261FuncaoDadosPreImp_FnDadosCodigo});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao Dados Pre Imp_Funcao Dados'.", "ForeignKeyNotFound", 1, "FUNCAODADOSPREIMP_FNDADOSCODIGO");
            AnyError = 1;
            GX_FocusControl = dynFuncaoDadosPreImp_FnDadosCodigo_Internalname;
         }
         pr_default.close(14);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Funcaodadospreimp_sequencial( GXCombobox dynGX_Parm1 ,
                                                      short GX_Parm2 ,
                                                      GXCombobox cmbGX_Parm3 ,
                                                      short GX_Parm4 ,
                                                      short GX_Parm5 ,
                                                      String GX_Parm6 )
      {
         dynFuncaoDadosPreImp_FnDadosCodigo = dynGX_Parm1;
         A1261FuncaoDadosPreImp_FnDadosCodigo = (int)(NumberUtil.Val( dynFuncaoDadosPreImp_FnDadosCodigo.CurrentValue, "."));
         A1259FuncaoDadosPreImp_Sequencial = GX_Parm2;
         cmbFuncaoDadosPreImp_Tipo = cmbGX_Parm3;
         A1253FuncaoDadosPreImp_Tipo = cmbFuncaoDadosPreImp_Tipo.CurrentValue;
         cmbFuncaoDadosPreImp_Tipo.CurrentValue = A1253FuncaoDadosPreImp_Tipo;
         A1254FuncaoDadosPreImp_DERImp = GX_Parm4;
         A1255FuncaoDadosPreImp_RAImp = GX_Parm5;
         A1256FuncaoDadosPreImp_Observacao = GX_Parm6;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         cmbFuncaoDadosPreImp_Tipo.CurrentValue = A1253FuncaoDadosPreImp_Tipo;
         isValidOutput.Add(cmbFuncaoDadosPreImp_Tipo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1254FuncaoDadosPreImp_DERImp), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1255FuncaoDadosPreImp_RAImp), 4, 0, ".", "")));
         isValidOutput.Add(A1256FuncaoDadosPreImp_Observacao);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1261FuncaoDadosPreImp_FnDadosCodigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1259FuncaoDadosPreImp_Sequencial), 3, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1253FuncaoDadosPreImp_Tipo));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1254FuncaoDadosPreImp_DERImp), 4, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1255FuncaoDadosPreImp_RAImp), 4, 0, ",", "")));
         isValidOutput.Add(Z1256FuncaoDadosPreImp_Observacao);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(14);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1253FuncaoDadosPreImp_Tipo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A1253FuncaoDadosPreImp_Tipo = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockfuncaodadospreimp_fndadoscodigo_Jsonclick = "";
         lblTextblockfuncaodadospreimp_sequencial_Jsonclick = "";
         lblTextblockfuncaodadospreimp_tipo_Jsonclick = "";
         lblTextblockfuncaodadospreimp_derimp_Jsonclick = "";
         lblTextblockfuncaodadospreimp_raimp_Jsonclick = "";
         lblTextblockfuncaodadospreimp_observacao_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         A1256FuncaoDadosPreImp_Observacao = "";
         Funcaodadospreimp_observacao_Width = "";
         Funcaodadospreimp_observacao_Height = "";
         Funcaodadospreimp_observacao_Skin = "";
         Funcaodadospreimp_observacao_Toolbar = "";
         Funcaodadospreimp_observacao_Class = "";
         Funcaodadospreimp_observacao_Customtoolbar = "";
         Funcaodadospreimp_observacao_Customconfiguration = "";
         Funcaodadospreimp_observacao_Buttonpressedid = "";
         Funcaodadospreimp_observacao_Captionvalue = "";
         Funcaodadospreimp_observacao_Captionclass = "";
         Funcaodadospreimp_observacao_Captionposition = "";
         Funcaodadospreimp_observacao_Coltitle = "";
         Funcaodadospreimp_observacao_Coltitlefont = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1256FuncaoDadosPreImp_Observacao = "";
         T003G5_A1259FuncaoDadosPreImp_Sequencial = new short[1] ;
         T003G5_A1253FuncaoDadosPreImp_Tipo = new String[] {""} ;
         T003G5_A1254FuncaoDadosPreImp_DERImp = new short[1] ;
         T003G5_A1255FuncaoDadosPreImp_RAImp = new short[1] ;
         T003G5_A1256FuncaoDadosPreImp_Observacao = new String[] {""} ;
         T003G5_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         T003G4_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         T003G6_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         T003G7_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         T003G7_A1259FuncaoDadosPreImp_Sequencial = new short[1] ;
         T003G3_A1259FuncaoDadosPreImp_Sequencial = new short[1] ;
         T003G3_A1253FuncaoDadosPreImp_Tipo = new String[] {""} ;
         T003G3_A1254FuncaoDadosPreImp_DERImp = new short[1] ;
         T003G3_A1255FuncaoDadosPreImp_RAImp = new short[1] ;
         T003G3_A1256FuncaoDadosPreImp_Observacao = new String[] {""} ;
         T003G3_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         sMode152 = "";
         T003G8_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         T003G8_A1259FuncaoDadosPreImp_Sequencial = new short[1] ;
         T003G9_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         T003G9_A1259FuncaoDadosPreImp_Sequencial = new short[1] ;
         T003G2_A1259FuncaoDadosPreImp_Sequencial = new short[1] ;
         T003G2_A1253FuncaoDadosPreImp_Tipo = new String[] {""} ;
         T003G2_A1254FuncaoDadosPreImp_DERImp = new short[1] ;
         T003G2_A1255FuncaoDadosPreImp_RAImp = new short[1] ;
         T003G2_A1256FuncaoDadosPreImp_Observacao = new String[] {""} ;
         T003G2_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         T003G13_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         T003G13_A1259FuncaoDadosPreImp_Sequencial = new short[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T003G14_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         T003G14_A369FuncaoDados_Nome = new String[] {""} ;
         T003G14_n369FuncaoDados_Nome = new bool[] {false} ;
         T003G15_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         T003G16_A1261FuncaoDadosPreImp_FnDadosCodigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaodadospreimp__default(),
            new Object[][] {
                new Object[] {
               T003G2_A1259FuncaoDadosPreImp_Sequencial, T003G2_A1253FuncaoDadosPreImp_Tipo, T003G2_A1254FuncaoDadosPreImp_DERImp, T003G2_A1255FuncaoDadosPreImp_RAImp, T003G2_A1256FuncaoDadosPreImp_Observacao, T003G2_A1261FuncaoDadosPreImp_FnDadosCodigo
               }
               , new Object[] {
               T003G3_A1259FuncaoDadosPreImp_Sequencial, T003G3_A1253FuncaoDadosPreImp_Tipo, T003G3_A1254FuncaoDadosPreImp_DERImp, T003G3_A1255FuncaoDadosPreImp_RAImp, T003G3_A1256FuncaoDadosPreImp_Observacao, T003G3_A1261FuncaoDadosPreImp_FnDadosCodigo
               }
               , new Object[] {
               T003G4_A1261FuncaoDadosPreImp_FnDadosCodigo
               }
               , new Object[] {
               T003G5_A1259FuncaoDadosPreImp_Sequencial, T003G5_A1253FuncaoDadosPreImp_Tipo, T003G5_A1254FuncaoDadosPreImp_DERImp, T003G5_A1255FuncaoDadosPreImp_RAImp, T003G5_A1256FuncaoDadosPreImp_Observacao, T003G5_A1261FuncaoDadosPreImp_FnDadosCodigo
               }
               , new Object[] {
               T003G6_A1261FuncaoDadosPreImp_FnDadosCodigo
               }
               , new Object[] {
               T003G7_A1261FuncaoDadosPreImp_FnDadosCodigo, T003G7_A1259FuncaoDadosPreImp_Sequencial
               }
               , new Object[] {
               T003G8_A1261FuncaoDadosPreImp_FnDadosCodigo, T003G8_A1259FuncaoDadosPreImp_Sequencial
               }
               , new Object[] {
               T003G9_A1261FuncaoDadosPreImp_FnDadosCodigo, T003G9_A1259FuncaoDadosPreImp_Sequencial
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003G13_A1261FuncaoDadosPreImp_FnDadosCodigo, T003G13_A1259FuncaoDadosPreImp_Sequencial
               }
               , new Object[] {
               T003G14_A1261FuncaoDadosPreImp_FnDadosCodigo, T003G14_A369FuncaoDados_Nome, T003G14_n369FuncaoDados_Nome
               }
               , new Object[] {
               T003G15_A1261FuncaoDadosPreImp_FnDadosCodigo
               }
               , new Object[] {
               T003G16_A1261FuncaoDadosPreImp_FnDadosCodigo
               }
            }
         );
      }

      private short Z1259FuncaoDadosPreImp_Sequencial ;
      private short Z1254FuncaoDadosPreImp_DERImp ;
      private short Z1255FuncaoDadosPreImp_RAImp ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1259FuncaoDadosPreImp_Sequencial ;
      private short A1254FuncaoDadosPreImp_DERImp ;
      private short A1255FuncaoDadosPreImp_RAImp ;
      private short GX_JID ;
      private short RcdFound152 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1261FuncaoDadosPreImp_FnDadosCodigo ;
      private int A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtFuncaoDadosPreImp_Sequencial_Enabled ;
      private int edtFuncaoDadosPreImp_DERImp_Enabled ;
      private int edtFuncaoDadosPreImp_RAImp_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Funcaodadospreimp_observacao_Color ;
      private int Funcaodadospreimp_observacao_Coltitlecolor ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String Z1253FuncaoDadosPreImp_Tipo ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String A1253FuncaoDadosPreImp_Tipo ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynFuncaoDadosPreImp_FnDadosCodigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockfuncaodadospreimp_fndadoscodigo_Internalname ;
      private String lblTextblockfuncaodadospreimp_fndadoscodigo_Jsonclick ;
      private String dynFuncaoDadosPreImp_FnDadosCodigo_Jsonclick ;
      private String lblTextblockfuncaodadospreimp_sequencial_Internalname ;
      private String lblTextblockfuncaodadospreimp_sequencial_Jsonclick ;
      private String edtFuncaoDadosPreImp_Sequencial_Internalname ;
      private String edtFuncaoDadosPreImp_Sequencial_Jsonclick ;
      private String lblTextblockfuncaodadospreimp_tipo_Internalname ;
      private String lblTextblockfuncaodadospreimp_tipo_Jsonclick ;
      private String cmbFuncaoDadosPreImp_Tipo_Internalname ;
      private String cmbFuncaoDadosPreImp_Tipo_Jsonclick ;
      private String lblTextblockfuncaodadospreimp_derimp_Internalname ;
      private String lblTextblockfuncaodadospreimp_derimp_Jsonclick ;
      private String edtFuncaoDadosPreImp_DERImp_Internalname ;
      private String edtFuncaoDadosPreImp_DERImp_Jsonclick ;
      private String lblTextblockfuncaodadospreimp_raimp_Internalname ;
      private String lblTextblockfuncaodadospreimp_raimp_Jsonclick ;
      private String edtFuncaoDadosPreImp_RAImp_Internalname ;
      private String edtFuncaoDadosPreImp_RAImp_Jsonclick ;
      private String lblTextblockfuncaodadospreimp_observacao_Internalname ;
      private String lblTextblockfuncaodadospreimp_observacao_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String Funcaodadospreimp_observacao_Width ;
      private String Funcaodadospreimp_observacao_Height ;
      private String Funcaodadospreimp_observacao_Skin ;
      private String Funcaodadospreimp_observacao_Toolbar ;
      private String Funcaodadospreimp_observacao_Class ;
      private String Funcaodadospreimp_observacao_Customtoolbar ;
      private String Funcaodadospreimp_observacao_Customconfiguration ;
      private String Funcaodadospreimp_observacao_Buttonpressedid ;
      private String Funcaodadospreimp_observacao_Captionvalue ;
      private String Funcaodadospreimp_observacao_Captionclass ;
      private String Funcaodadospreimp_observacao_Captionposition ;
      private String Funcaodadospreimp_observacao_Coltitle ;
      private String Funcaodadospreimp_observacao_Coltitlefont ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode152 ;
      private String Funcaodadospreimp_observacao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Funcaodadospreimp_observacao_Enabled ;
      private bool Funcaodadospreimp_observacao_Toolbarcancollapse ;
      private bool Funcaodadospreimp_observacao_Toolbarexpanded ;
      private bool Funcaodadospreimp_observacao_Usercontroliscolumn ;
      private bool Funcaodadospreimp_observacao_Visible ;
      private String A1256FuncaoDadosPreImp_Observacao ;
      private String Z1256FuncaoDadosPreImp_Observacao ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynFuncaoDadosPreImp_FnDadosCodigo ;
      private GXCombobox cmbFuncaoDadosPreImp_Tipo ;
      private IDataStoreProvider pr_default ;
      private short[] T003G5_A1259FuncaoDadosPreImp_Sequencial ;
      private String[] T003G5_A1253FuncaoDadosPreImp_Tipo ;
      private short[] T003G5_A1254FuncaoDadosPreImp_DERImp ;
      private short[] T003G5_A1255FuncaoDadosPreImp_RAImp ;
      private String[] T003G5_A1256FuncaoDadosPreImp_Observacao ;
      private int[] T003G5_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private int[] T003G4_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private int[] T003G6_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private int[] T003G7_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private short[] T003G7_A1259FuncaoDadosPreImp_Sequencial ;
      private short[] T003G3_A1259FuncaoDadosPreImp_Sequencial ;
      private String[] T003G3_A1253FuncaoDadosPreImp_Tipo ;
      private short[] T003G3_A1254FuncaoDadosPreImp_DERImp ;
      private short[] T003G3_A1255FuncaoDadosPreImp_RAImp ;
      private String[] T003G3_A1256FuncaoDadosPreImp_Observacao ;
      private int[] T003G3_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private int[] T003G8_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private short[] T003G8_A1259FuncaoDadosPreImp_Sequencial ;
      private int[] T003G9_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private short[] T003G9_A1259FuncaoDadosPreImp_Sequencial ;
      private short[] T003G2_A1259FuncaoDadosPreImp_Sequencial ;
      private String[] T003G2_A1253FuncaoDadosPreImp_Tipo ;
      private short[] T003G2_A1254FuncaoDadosPreImp_DERImp ;
      private short[] T003G2_A1255FuncaoDadosPreImp_RAImp ;
      private String[] T003G2_A1256FuncaoDadosPreImp_Observacao ;
      private int[] T003G2_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private int[] T003G13_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private short[] T003G13_A1259FuncaoDadosPreImp_Sequencial ;
      private int[] T003G14_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private String[] T003G14_A369FuncaoDados_Nome ;
      private bool[] T003G14_n369FuncaoDados_Nome ;
      private int[] T003G15_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private int[] T003G16_A1261FuncaoDadosPreImp_FnDadosCodigo ;
      private GXWebForm Form ;
   }

   public class funcaodadospreimp__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003G5 ;
          prmT003G5 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDadosPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003G4 ;
          prmT003G4 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003G6 ;
          prmT003G6 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003G7 ;
          prmT003G7 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDadosPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003G3 ;
          prmT003G3 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDadosPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003G8 ;
          prmT003G8 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDadosPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003G9 ;
          prmT003G9 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDadosPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003G2 ;
          prmT003G2 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDadosPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003G10 ;
          prmT003G10 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@FuncaoDadosPreImp_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDadosPreImp_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDadosPreImp_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDadosPreImp_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003G11 ;
          prmT003G11 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoDadosPreImp_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDadosPreImp_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoDadosPreImp_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDadosPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003G12 ;
          prmT003G12 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoDadosPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003G13 ;
          prmT003G13 = new Object[] {
          } ;
          Object[] prmT003G14 ;
          prmT003G14 = new Object[] {
          } ;
          Object[] prmT003G15 ;
          prmT003G15 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003G16 ;
          prmT003G16 = new Object[] {
          new Object[] {"@FuncaoDadosPreImp_FnDadosCodigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003G2", "SELECT [FuncaoDadosPreImp_Sequencial], [FuncaoDadosPreImp_Tipo], [FuncaoDadosPreImp_DERImp], [FuncaoDadosPreImp_RAImp], [FuncaoDadosPreImp_Observacao], [FuncaoDadosPreImp_FnDadosCodigo] AS FuncaoDadosPreImp_FnDadosCodigo FROM [FuncaoDadosPreImp] WITH (UPDLOCK) WHERE [FuncaoDadosPreImp_FnDadosCodigo] = @FuncaoDadosPreImp_FnDadosCodigo AND [FuncaoDadosPreImp_Sequencial] = @FuncaoDadosPreImp_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT003G2,1,0,true,false )
             ,new CursorDef("T003G3", "SELECT [FuncaoDadosPreImp_Sequencial], [FuncaoDadosPreImp_Tipo], [FuncaoDadosPreImp_DERImp], [FuncaoDadosPreImp_RAImp], [FuncaoDadosPreImp_Observacao], [FuncaoDadosPreImp_FnDadosCodigo] AS FuncaoDadosPreImp_FnDadosCodigo FROM [FuncaoDadosPreImp] WITH (NOLOCK) WHERE [FuncaoDadosPreImp_FnDadosCodigo] = @FuncaoDadosPreImp_FnDadosCodigo AND [FuncaoDadosPreImp_Sequencial] = @FuncaoDadosPreImp_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT003G3,1,0,true,false )
             ,new CursorDef("T003G4", "SELECT [FuncaoDados_Codigo] AS FuncaoDadosPreImp_FnDadosCodigo FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDadosPreImp_FnDadosCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003G4,1,0,true,false )
             ,new CursorDef("T003G5", "SELECT TM1.[FuncaoDadosPreImp_Sequencial], TM1.[FuncaoDadosPreImp_Tipo], TM1.[FuncaoDadosPreImp_DERImp], TM1.[FuncaoDadosPreImp_RAImp], TM1.[FuncaoDadosPreImp_Observacao], TM1.[FuncaoDadosPreImp_FnDadosCodigo] AS FuncaoDadosPreImp_FnDadosCodigo FROM [FuncaoDadosPreImp] TM1 WITH (NOLOCK) WHERE TM1.[FuncaoDadosPreImp_FnDadosCodigo] = @FuncaoDadosPreImp_FnDadosCodigo and TM1.[FuncaoDadosPreImp_Sequencial] = @FuncaoDadosPreImp_Sequencial ORDER BY TM1.[FuncaoDadosPreImp_FnDadosCodigo], TM1.[FuncaoDadosPreImp_Sequencial]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003G5,100,0,true,false )
             ,new CursorDef("T003G6", "SELECT [FuncaoDados_Codigo] AS FuncaoDadosPreImp_FnDadosCodigo FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDadosPreImp_FnDadosCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003G6,1,0,true,false )
             ,new CursorDef("T003G7", "SELECT [FuncaoDadosPreImp_FnDadosCodigo] AS FuncaoDadosPreImp_FnDadosCodigo, [FuncaoDadosPreImp_Sequencial] FROM [FuncaoDadosPreImp] WITH (NOLOCK) WHERE [FuncaoDadosPreImp_FnDadosCodigo] = @FuncaoDadosPreImp_FnDadosCodigo AND [FuncaoDadosPreImp_Sequencial] = @FuncaoDadosPreImp_Sequencial  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003G7,1,0,true,false )
             ,new CursorDef("T003G8", "SELECT TOP 1 [FuncaoDadosPreImp_FnDadosCodigo] AS FuncaoDadosPreImp_FnDadosCodigo, [FuncaoDadosPreImp_Sequencial] FROM [FuncaoDadosPreImp] WITH (NOLOCK) WHERE ( [FuncaoDadosPreImp_FnDadosCodigo] > @FuncaoDadosPreImp_FnDadosCodigo or [FuncaoDadosPreImp_FnDadosCodigo] = @FuncaoDadosPreImp_FnDadosCodigo and [FuncaoDadosPreImp_Sequencial] > @FuncaoDadosPreImp_Sequencial) ORDER BY [FuncaoDadosPreImp_FnDadosCodigo], [FuncaoDadosPreImp_Sequencial]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003G8,1,0,true,true )
             ,new CursorDef("T003G9", "SELECT TOP 1 [FuncaoDadosPreImp_FnDadosCodigo] AS FuncaoDadosPreImp_FnDadosCodigo, [FuncaoDadosPreImp_Sequencial] FROM [FuncaoDadosPreImp] WITH (NOLOCK) WHERE ( [FuncaoDadosPreImp_FnDadosCodigo] < @FuncaoDadosPreImp_FnDadosCodigo or [FuncaoDadosPreImp_FnDadosCodigo] = @FuncaoDadosPreImp_FnDadosCodigo and [FuncaoDadosPreImp_Sequencial] < @FuncaoDadosPreImp_Sequencial) ORDER BY [FuncaoDadosPreImp_FnDadosCodigo] DESC, [FuncaoDadosPreImp_Sequencial] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003G9,1,0,true,true )
             ,new CursorDef("T003G10", "INSERT INTO [FuncaoDadosPreImp]([FuncaoDadosPreImp_Sequencial], [FuncaoDadosPreImp_Tipo], [FuncaoDadosPreImp_DERImp], [FuncaoDadosPreImp_RAImp], [FuncaoDadosPreImp_Observacao], [FuncaoDadosPreImp_FnDadosCodigo]) VALUES(@FuncaoDadosPreImp_Sequencial, @FuncaoDadosPreImp_Tipo, @FuncaoDadosPreImp_DERImp, @FuncaoDadosPreImp_RAImp, @FuncaoDadosPreImp_Observacao, @FuncaoDadosPreImp_FnDadosCodigo)", GxErrorMask.GX_NOMASK,prmT003G10)
             ,new CursorDef("T003G11", "UPDATE [FuncaoDadosPreImp] SET [FuncaoDadosPreImp_Tipo]=@FuncaoDadosPreImp_Tipo, [FuncaoDadosPreImp_DERImp]=@FuncaoDadosPreImp_DERImp, [FuncaoDadosPreImp_RAImp]=@FuncaoDadosPreImp_RAImp, [FuncaoDadosPreImp_Observacao]=@FuncaoDadosPreImp_Observacao  WHERE [FuncaoDadosPreImp_FnDadosCodigo] = @FuncaoDadosPreImp_FnDadosCodigo AND [FuncaoDadosPreImp_Sequencial] = @FuncaoDadosPreImp_Sequencial", GxErrorMask.GX_NOMASK,prmT003G11)
             ,new CursorDef("T003G12", "DELETE FROM [FuncaoDadosPreImp]  WHERE [FuncaoDadosPreImp_FnDadosCodigo] = @FuncaoDadosPreImp_FnDadosCodigo AND [FuncaoDadosPreImp_Sequencial] = @FuncaoDadosPreImp_Sequencial", GxErrorMask.GX_NOMASK,prmT003G12)
             ,new CursorDef("T003G13", "SELECT [FuncaoDadosPreImp_FnDadosCodigo] AS FuncaoDadosPreImp_FnDadosCodigo, [FuncaoDadosPreImp_Sequencial] FROM [FuncaoDadosPreImp] WITH (NOLOCK) ORDER BY [FuncaoDadosPreImp_FnDadosCodigo], [FuncaoDadosPreImp_Sequencial]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003G13,100,0,true,false )
             ,new CursorDef("T003G14", "SELECT [FuncaoDados_Codigo] AS FuncaoDadosPreImp_FnDadosCodigo, [FuncaoDados_Nome] FROM [FuncaoDados] WITH (NOLOCK) ORDER BY [FuncaoDados_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003G14,0,0,true,false )
             ,new CursorDef("T003G15", "SELECT [FuncaoDados_Codigo] AS FuncaoDadosPreImp_FnDadosCodigo FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDadosPreImp_FnDadosCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003G15,1,0,true,false )
             ,new CursorDef("T003G16", "SELECT [FuncaoDados_Codigo] AS FuncaoDadosPreImp_FnDadosCodigo FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @FuncaoDadosPreImp_FnDadosCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003G16,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (int)parms[5]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                stmt.SetParameter(6, (short)parms[5]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
