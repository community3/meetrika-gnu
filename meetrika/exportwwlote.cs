/*
               File: ExportWWLote
        Description: Export WWLote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 22:59:12.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportwwlote : GXProcedure
   {
      public exportwwlote( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public exportwwlote( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Lote_AreaTrabalhoCod ,
                           int aP1_Lote_ContratadaCod ,
                           int aP2_TFLote_NFe ,
                           int aP3_TFLote_NFe_To ,
                           DateTime aP4_TFLote_DataNfe ,
                           DateTime aP5_TFLote_DataNfe_To ,
                           DateTime aP6_TFLote_PrevPagamento ,
                           DateTime aP7_TFLote_PrevPagamento_To ,
                           short aP8_OrderedBy ,
                           bool aP9_OrderedDsc ,
                           String aP10_GridStateXML ,
                           out String aP11_Filename ,
                           out String aP12_ErrorMessage )
      {
         this.AV18Lote_AreaTrabalhoCod = aP0_Lote_AreaTrabalhoCod;
         this.AV81Lote_ContratadaCod = aP1_Lote_ContratadaCod;
         this.AV109TFLote_NFe = aP2_TFLote_NFe;
         this.AV110TFLote_NFe_To = aP3_TFLote_NFe_To;
         this.AV111TFLote_DataNfe = aP4_TFLote_DataNfe;
         this.AV112TFLote_DataNfe_To = aP5_TFLote_DataNfe_To;
         this.AV113TFLote_PrevPagamento = aP6_TFLote_PrevPagamento;
         this.AV114TFLote_PrevPagamento_To = aP7_TFLote_PrevPagamento_To;
         this.AV16OrderedBy = aP8_OrderedBy;
         this.AV17OrderedDsc = aP9_OrderedDsc;
         this.AV78GridStateXML = aP10_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP11_Filename=this.AV11Filename;
         aP12_ErrorMessage=this.AV12ErrorMessage;
      }

      public String executeUdp( int aP0_Lote_AreaTrabalhoCod ,
                                int aP1_Lote_ContratadaCod ,
                                int aP2_TFLote_NFe ,
                                int aP3_TFLote_NFe_To ,
                                DateTime aP4_TFLote_DataNfe ,
                                DateTime aP5_TFLote_DataNfe_To ,
                                DateTime aP6_TFLote_PrevPagamento ,
                                DateTime aP7_TFLote_PrevPagamento_To ,
                                short aP8_OrderedBy ,
                                bool aP9_OrderedDsc ,
                                String aP10_GridStateXML ,
                                out String aP11_Filename )
      {
         this.AV18Lote_AreaTrabalhoCod = aP0_Lote_AreaTrabalhoCod;
         this.AV81Lote_ContratadaCod = aP1_Lote_ContratadaCod;
         this.AV109TFLote_NFe = aP2_TFLote_NFe;
         this.AV110TFLote_NFe_To = aP3_TFLote_NFe_To;
         this.AV111TFLote_DataNfe = aP4_TFLote_DataNfe;
         this.AV112TFLote_DataNfe_To = aP5_TFLote_DataNfe_To;
         this.AV113TFLote_PrevPagamento = aP6_TFLote_PrevPagamento;
         this.AV114TFLote_PrevPagamento_To = aP7_TFLote_PrevPagamento_To;
         this.AV16OrderedBy = aP8_OrderedBy;
         this.AV17OrderedDsc = aP9_OrderedDsc;
         this.AV78GridStateXML = aP10_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP11_Filename=this.AV11Filename;
         aP12_ErrorMessage=this.AV12ErrorMessage;
         return AV12ErrorMessage ;
      }

      public void executeSubmit( int aP0_Lote_AreaTrabalhoCod ,
                                 int aP1_Lote_ContratadaCod ,
                                 int aP2_TFLote_NFe ,
                                 int aP3_TFLote_NFe_To ,
                                 DateTime aP4_TFLote_DataNfe ,
                                 DateTime aP5_TFLote_DataNfe_To ,
                                 DateTime aP6_TFLote_PrevPagamento ,
                                 DateTime aP7_TFLote_PrevPagamento_To ,
                                 short aP8_OrderedBy ,
                                 bool aP9_OrderedDsc ,
                                 String aP10_GridStateXML ,
                                 out String aP11_Filename ,
                                 out String aP12_ErrorMessage )
      {
         exportwwlote objexportwwlote;
         objexportwwlote = new exportwwlote();
         objexportwwlote.AV18Lote_AreaTrabalhoCod = aP0_Lote_AreaTrabalhoCod;
         objexportwwlote.AV81Lote_ContratadaCod = aP1_Lote_ContratadaCod;
         objexportwwlote.AV109TFLote_NFe = aP2_TFLote_NFe;
         objexportwwlote.AV110TFLote_NFe_To = aP3_TFLote_NFe_To;
         objexportwwlote.AV111TFLote_DataNfe = aP4_TFLote_DataNfe;
         objexportwwlote.AV112TFLote_DataNfe_To = aP5_TFLote_DataNfe_To;
         objexportwwlote.AV113TFLote_PrevPagamento = aP6_TFLote_PrevPagamento;
         objexportwwlote.AV114TFLote_PrevPagamento_To = aP7_TFLote_PrevPagamento_To;
         objexportwwlote.AV16OrderedBy = aP8_OrderedBy;
         objexportwwlote.AV17OrderedDsc = aP9_OrderedDsc;
         objexportwwlote.AV78GridStateXML = aP10_GridStateXML;
         objexportwwlote.AV11Filename = "" ;
         objexportwwlote.AV12ErrorMessage = "" ;
         objexportwwlote.context.SetSubmitInitialConfig(context);
         objexportwwlote.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportwwlote);
         aP11_Filename=this.AV11Filename;
         aP12_ErrorMessage=this.AV12ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportwwlote)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'OPENDOCUMENT' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13CellRow = 1;
         AV14FirstColumn = 1;
         /* Execute user subroutine: 'WRITEMAINTITLE' */
         S131 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEFILTERS' */
         S141 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITECOLUMNTITLES' */
         S151 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEDATA' */
         S161 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'CLOSEDOCUMENT' */
         S191 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'OPENDOCUMENT' Routine */
         if ( false )
         {
            AV15Random = (int)(NumberUtil.Random( )*10000);
            AV11Filename = "ExportWWLote-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Random), 8, 0)) + ".xlsx";
            AV10ExcelDocument.Open(AV11Filename);
            /* Execute user subroutine: 'CHECKSTATUS' */
            S121 ();
            if (returnInSub) return;
            AV10ExcelDocument.Clear();
         }
         AV15Random = (int)(NumberUtil.Random( )*10000);
         AV11Filename = "PublicTempStorage\\ExportWWLote-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Random), 8, 0)) + ".xlsx";
         AV10ExcelDocument.Open(AV11Filename);
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Clear();
      }

      protected void S131( )
      {
         /* 'WRITEMAINTITLE' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Consulta de Lotes";
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S141( )
      {
         /* 'WRITEFILTERS' Routine */
         if ( ! ( (0==AV18Lote_AreaTrabalhoCod) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Area de Trabalho";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV18Lote_AreaTrabalhoCod;
         }
         if ( ! ( (0==AV81Lote_ContratadaCod) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV81Lote_ContratadaCod;
         }
         AV79GridState.gxTpr_Dynamicfilters.FromXml(AV78GridStateXML, "");
         if ( AV79GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV80GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV79GridState.gxTpr_Dynamicfilters.Item(1));
            AV19DynamicFiltersSelector1 = AV80GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "LOTE_NUMERO") == 0 )
            {
               AV20Lote_Numero1 = AV80GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20Lote_Numero1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Lote";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV20Lote_Numero1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "LOTE_NOME") == 0 )
            {
               AV21Lote_Nome1 = AV80GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Lote_Nome1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Nome";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV21Lote_Nome1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "LOTE_DATA") == 0 )
            {
               AV22Lote_Data1 = context.localUtil.CToT( AV80GridStateDynamicFilter.gxTpr_Value, 2);
               AV23Lote_Data_To1 = context.localUtil.CToT( AV80GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV22Lote_Data1) || ! (DateTime.MinValue==AV23Lote_Data_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data do Lote";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = AV22Lote_Data1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = AV23Lote_Data_To1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "LOTE_USERNOM") == 0 )
            {
               AV24Lote_UserNom1 = AV80GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Lote_UserNom1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV24Lote_UserNom1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "LOTE_DATAINI") == 0 )
            {
               AV25Lote_DataIni1 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Value, 2);
               AV26Lote_DataIni_To1 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV25Lote_DataIni1) || ! (DateTime.MinValue==AV26Lote_DataIni_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Ini";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV25Lote_DataIni1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV26Lote_DataIni_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "LOTE_DATAFIM") == 0 )
            {
               AV27Lote_DataFim1 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Value, 2);
               AV28Lote_DataFim_To1 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV27Lote_DataFim1) || ! (DateTime.MinValue==AV28Lote_DataFim_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Fim";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV27Lote_DataFim1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV28Lote_DataFim_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "LOTE_NFE") == 0 )
            {
               AV29Lote_NFe1 = (int)(NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV29Lote_NFe1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "NFe";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV29Lote_NFe1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "LOTE_DATANFE") == 0 )
            {
               AV30Lote_DataNfe1 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Value, 2);
               AV31Lote_DataNfe_To1 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV30Lote_DataNfe1) || ! (DateTime.MinValue==AV31Lote_DataNfe_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Nfe";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV30Lote_DataNfe1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV31Lote_DataNfe_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "LOTE_QTDEDMN") == 0 )
            {
               AV32Lote_QtdeDmn1 = (short)(NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Value, "."));
               AV33Lote_QtdeDmn_To1 = (short)(NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Valueto, "."));
               if ( ! (0==AV32Lote_QtdeDmn1) || ! (0==AV33Lote_QtdeDmn_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Qtde Dmn";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV32Lote_QtdeDmn1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = AV33Lote_QtdeDmn_To1;
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "LOTE_VALOR") == 0 )
            {
               AV34Lote_Valor1 = NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Value, ".");
               AV35Lote_Valor_To1 = NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Valueto, ".");
               if ( ! (Convert.ToDecimal(0)==AV34Lote_Valor1) || ! (Convert.ToDecimal(0)==AV35Lote_Valor_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Valor R$";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV34Lote_Valor1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV35Lote_Valor_To1);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "LOTE_VALORPF") == 0 )
            {
               AV36Lote_ValorPF1 = NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Value, ".");
               AV37Lote_ValorPF_To1 = NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Valueto, ".");
               if ( ! (Convert.ToDecimal(0)==AV36Lote_ValorPF1) || ! (Convert.ToDecimal(0)==AV37Lote_ValorPF_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Valor PF R$";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV36Lote_ValorPF1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV37Lote_ValorPF_To1);
               }
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector1, "LOTE_CONTRATADANOM") == 0 )
            {
               AV82Lote_ContratadaNom1 = AV80GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82Lote_ContratadaNom1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contratada";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV82Lote_ContratadaNom1;
               }
            }
            if ( AV79GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV38DynamicFiltersEnabled2 = true;
               AV80GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV79GridState.gxTpr_Dynamicfilters.Item(2));
               AV39DynamicFiltersSelector2 = AV80GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "LOTE_NUMERO") == 0 )
               {
                  AV40Lote_Numero2 = AV80GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Lote_Numero2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Lote";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV40Lote_Numero2;
                  }
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "LOTE_NOME") == 0 )
               {
                  AV41Lote_Nome2 = AV80GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Lote_Nome2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Nome";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV41Lote_Nome2;
                  }
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "LOTE_DATA") == 0 )
               {
                  AV42Lote_Data2 = context.localUtil.CToT( AV80GridStateDynamicFilter.gxTpr_Value, 2);
                  AV43Lote_Data_To2 = context.localUtil.CToT( AV80GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV42Lote_Data2) || ! (DateTime.MinValue==AV43Lote_Data_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data do Lote";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = AV42Lote_Data2;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = AV43Lote_Data_To2;
                  }
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "LOTE_USERNOM") == 0 )
               {
                  AV44Lote_UserNom2 = AV80GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Lote_UserNom2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV44Lote_UserNom2;
                  }
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "LOTE_DATAINI") == 0 )
               {
                  AV45Lote_DataIni2 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Value, 2);
                  AV46Lote_DataIni_To2 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV45Lote_DataIni2) || ! (DateTime.MinValue==AV46Lote_DataIni_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Ini";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV45Lote_DataIni2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV46Lote_DataIni_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "LOTE_DATAFIM") == 0 )
               {
                  AV47Lote_DataFim2 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Value, 2);
                  AV48Lote_DataFim_To2 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV47Lote_DataFim2) || ! (DateTime.MinValue==AV48Lote_DataFim_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Fim";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV47Lote_DataFim2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV48Lote_DataFim_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "LOTE_NFE") == 0 )
               {
                  AV49Lote_NFe2 = (int)(NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV49Lote_NFe2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "NFe";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV49Lote_NFe2;
                  }
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "LOTE_DATANFE") == 0 )
               {
                  AV50Lote_DataNfe2 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Value, 2);
                  AV51Lote_DataNfe_To2 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV50Lote_DataNfe2) || ! (DateTime.MinValue==AV51Lote_DataNfe_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Nfe";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV50Lote_DataNfe2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV51Lote_DataNfe_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "LOTE_QTDEDMN") == 0 )
               {
                  AV52Lote_QtdeDmn2 = (short)(NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Value, "."));
                  AV53Lote_QtdeDmn_To2 = (short)(NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Valueto, "."));
                  if ( ! (0==AV52Lote_QtdeDmn2) || ! (0==AV53Lote_QtdeDmn_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Qtde Dmn";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV52Lote_QtdeDmn2;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = AV53Lote_QtdeDmn_To2;
                  }
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "LOTE_VALOR") == 0 )
               {
                  AV54Lote_Valor2 = NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Value, ".");
                  AV55Lote_Valor_To2 = NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Valueto, ".");
                  if ( ! (Convert.ToDecimal(0)==AV54Lote_Valor2) || ! (Convert.ToDecimal(0)==AV55Lote_Valor_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Valor R$";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV54Lote_Valor2);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV55Lote_Valor_To2);
                  }
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "LOTE_VALORPF") == 0 )
               {
                  AV56Lote_ValorPF2 = NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Value, ".");
                  AV57Lote_ValorPF_To2 = NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Valueto, ".");
                  if ( ! (Convert.ToDecimal(0)==AV56Lote_ValorPF2) || ! (Convert.ToDecimal(0)==AV57Lote_ValorPF_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Valor PF R$";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV56Lote_ValorPF2);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV57Lote_ValorPF_To2);
                  }
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "LOTE_CONTRATADANOM") == 0 )
               {
                  AV83Lote_ContratadaNom2 = AV80GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83Lote_ContratadaNom2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contratada";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV83Lote_ContratadaNom2;
                  }
               }
               if ( AV79GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV58DynamicFiltersEnabled3 = true;
                  AV80GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV79GridState.gxTpr_Dynamicfilters.Item(3));
                  AV59DynamicFiltersSelector3 = AV80GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "LOTE_NUMERO") == 0 )
                  {
                     AV60Lote_Numero3 = AV80GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60Lote_Numero3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Lote";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV60Lote_Numero3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "LOTE_NOME") == 0 )
                  {
                     AV61Lote_Nome3 = AV80GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61Lote_Nome3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Nome";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV61Lote_Nome3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "LOTE_DATA") == 0 )
                  {
                     AV62Lote_Data3 = context.localUtil.CToT( AV80GridStateDynamicFilter.gxTpr_Value, 2);
                     AV63Lote_Data_To3 = context.localUtil.CToT( AV80GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV62Lote_Data3) || ! (DateTime.MinValue==AV63Lote_Data_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data do Lote";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = AV62Lote_Data3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = AV63Lote_Data_To3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "LOTE_USERNOM") == 0 )
                  {
                     AV64Lote_UserNom3 = AV80GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64Lote_UserNom3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV64Lote_UserNom3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "LOTE_DATAINI") == 0 )
                  {
                     AV65Lote_DataIni3 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Value, 2);
                     AV66Lote_DataIni_To3 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV65Lote_DataIni3) || ! (DateTime.MinValue==AV66Lote_DataIni_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Ini";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV65Lote_DataIni3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV66Lote_DataIni_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "LOTE_DATAFIM") == 0 )
                  {
                     AV67Lote_DataFim3 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Value, 2);
                     AV68Lote_DataFim_To3 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV67Lote_DataFim3) || ! (DateTime.MinValue==AV68Lote_DataFim_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Fim";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV67Lote_DataFim3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV68Lote_DataFim_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "LOTE_NFE") == 0 )
                  {
                     AV69Lote_NFe3 = (int)(NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV69Lote_NFe3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "NFe";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV69Lote_NFe3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "LOTE_DATANFE") == 0 )
                  {
                     AV70Lote_DataNfe3 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Value, 2);
                     AV71Lote_DataNfe_To3 = context.localUtil.CToD( AV80GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV70Lote_DataNfe3) || ! (DateTime.MinValue==AV71Lote_DataNfe_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Nfe";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV70Lote_DataNfe3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV71Lote_DataNfe_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "LOTE_QTDEDMN") == 0 )
                  {
                     AV72Lote_QtdeDmn3 = (short)(NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Value, "."));
                     AV73Lote_QtdeDmn_To3 = (short)(NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Valueto, "."));
                     if ( ! (0==AV72Lote_QtdeDmn3) || ! (0==AV73Lote_QtdeDmn_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Qtde Dmn";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV72Lote_QtdeDmn3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = AV73Lote_QtdeDmn_To3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "LOTE_VALOR") == 0 )
                  {
                     AV74Lote_Valor3 = NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Value, ".");
                     AV75Lote_Valor_To3 = NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Valueto, ".");
                     if ( ! (Convert.ToDecimal(0)==AV74Lote_Valor3) || ! (Convert.ToDecimal(0)==AV75Lote_Valor_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Valor R$";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV74Lote_Valor3);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV75Lote_Valor_To3);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "LOTE_VALORPF") == 0 )
                  {
                     AV76Lote_ValorPF3 = NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Value, ".");
                     AV77Lote_ValorPF_To3 = NumberUtil.Val( AV80GridStateDynamicFilter.gxTpr_Valueto, ".");
                     if ( ! (Convert.ToDecimal(0)==AV76Lote_ValorPF3) || ! (Convert.ToDecimal(0)==AV77Lote_ValorPF_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Valor PF R$";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV76Lote_ValorPF3);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV77Lote_ValorPF_To3);
                     }
                  }
                  else if ( StringUtil.StrCmp(AV59DynamicFiltersSelector3, "LOTE_CONTRATADANOM") == 0 )
                  {
                     AV84Lote_ContratadaNom3 = AV80GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84Lote_ContratadaNom3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contratada";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV84Lote_ContratadaNom3;
                     }
                  }
               }
            }
         }
         if ( ! ( (0==AV109TFLote_NFe) && (0==AV110TFLote_NFe_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "NFe";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV109TFLote_NFe;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = AV110TFLote_NFe_To;
         }
         if ( ! ( (DateTime.MinValue==AV111TFLote_DataNfe) && (DateTime.MinValue==AV112TFLote_DataNfe_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Emiss�o";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV111TFLote_DataNfe ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV112TFLote_DataNfe_To ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
         }
         if ( ! ( (DateTime.MinValue==AV113TFLote_PrevPagamento) && (DateTime.MinValue==AV114TFLote_PrevPagamento_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Previss�o";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV113TFLote_PrevPagamento ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV114TFLote_PrevPagamento_To ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
         }
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S151( )
      {
         /* 'WRITECOLUMNTITLES' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Lote";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Nome";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "Contratada";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Text = "Data do Lote";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Text = "Usu�rio";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Text = "Per�odo";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Text = "";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Text = "Status";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = "Qtde Dmn";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Text = "Valor R$";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Text = "NFe";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Text = "Emiss�o";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Text = "Previss�o";
      }

      protected void S161( )
      {
         /* 'WRITEDATA' Routine */
         AV118WWLoteDS_1_Lote_areatrabalhocod = AV18Lote_AreaTrabalhoCod;
         AV119WWLoteDS_2_Lote_contratadacod = AV81Lote_ContratadaCod;
         AV120WWLoteDS_3_Dynamicfiltersselector1 = AV19DynamicFiltersSelector1;
         AV121WWLoteDS_4_Lote_numero1 = AV20Lote_Numero1;
         AV122WWLoteDS_5_Lote_nome1 = AV21Lote_Nome1;
         AV123WWLoteDS_6_Lote_data1 = AV22Lote_Data1;
         AV124WWLoteDS_7_Lote_data_to1 = AV23Lote_Data_To1;
         AV125WWLoteDS_8_Lote_usernom1 = AV24Lote_UserNom1;
         AV126WWLoteDS_9_Lote_dataini1 = AV25Lote_DataIni1;
         AV127WWLoteDS_10_Lote_dataini_to1 = AV26Lote_DataIni_To1;
         AV128WWLoteDS_11_Lote_datafim1 = AV27Lote_DataFim1;
         AV129WWLoteDS_12_Lote_datafim_to1 = AV28Lote_DataFim_To1;
         AV130WWLoteDS_13_Lote_nfe1 = AV29Lote_NFe1;
         AV131WWLoteDS_14_Lote_datanfe1 = AV30Lote_DataNfe1;
         AV132WWLoteDS_15_Lote_datanfe_to1 = AV31Lote_DataNfe_To1;
         AV133WWLoteDS_16_Lote_qtdedmn1 = AV32Lote_QtdeDmn1;
         AV134WWLoteDS_17_Lote_qtdedmn_to1 = AV33Lote_QtdeDmn_To1;
         AV135WWLoteDS_18_Lote_valor1 = AV34Lote_Valor1;
         AV136WWLoteDS_19_Lote_valor_to1 = AV35Lote_Valor_To1;
         AV137WWLoteDS_20_Lote_valorpf1 = AV36Lote_ValorPF1;
         AV138WWLoteDS_21_Lote_valorpf_to1 = AV37Lote_ValorPF_To1;
         AV139WWLoteDS_22_Lote_contratadanom1 = AV82Lote_ContratadaNom1;
         AV140WWLoteDS_23_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV141WWLoteDS_24_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV142WWLoteDS_25_Lote_numero2 = AV40Lote_Numero2;
         AV143WWLoteDS_26_Lote_nome2 = AV41Lote_Nome2;
         AV144WWLoteDS_27_Lote_data2 = AV42Lote_Data2;
         AV145WWLoteDS_28_Lote_data_to2 = AV43Lote_Data_To2;
         AV146WWLoteDS_29_Lote_usernom2 = AV44Lote_UserNom2;
         AV147WWLoteDS_30_Lote_dataini2 = AV45Lote_DataIni2;
         AV148WWLoteDS_31_Lote_dataini_to2 = AV46Lote_DataIni_To2;
         AV149WWLoteDS_32_Lote_datafim2 = AV47Lote_DataFim2;
         AV150WWLoteDS_33_Lote_datafim_to2 = AV48Lote_DataFim_To2;
         AV151WWLoteDS_34_Lote_nfe2 = AV49Lote_NFe2;
         AV152WWLoteDS_35_Lote_datanfe2 = AV50Lote_DataNfe2;
         AV153WWLoteDS_36_Lote_datanfe_to2 = AV51Lote_DataNfe_To2;
         AV154WWLoteDS_37_Lote_qtdedmn2 = AV52Lote_QtdeDmn2;
         AV155WWLoteDS_38_Lote_qtdedmn_to2 = AV53Lote_QtdeDmn_To2;
         AV156WWLoteDS_39_Lote_valor2 = AV54Lote_Valor2;
         AV157WWLoteDS_40_Lote_valor_to2 = AV55Lote_Valor_To2;
         AV158WWLoteDS_41_Lote_valorpf2 = AV56Lote_ValorPF2;
         AV159WWLoteDS_42_Lote_valorpf_to2 = AV57Lote_ValorPF_To2;
         AV160WWLoteDS_43_Lote_contratadanom2 = AV83Lote_ContratadaNom2;
         AV161WWLoteDS_44_Dynamicfiltersenabled3 = AV58DynamicFiltersEnabled3;
         AV162WWLoteDS_45_Dynamicfiltersselector3 = AV59DynamicFiltersSelector3;
         AV163WWLoteDS_46_Lote_numero3 = AV60Lote_Numero3;
         AV164WWLoteDS_47_Lote_nome3 = AV61Lote_Nome3;
         AV165WWLoteDS_48_Lote_data3 = AV62Lote_Data3;
         AV166WWLoteDS_49_Lote_data_to3 = AV63Lote_Data_To3;
         AV167WWLoteDS_50_Lote_usernom3 = AV64Lote_UserNom3;
         AV168WWLoteDS_51_Lote_dataini3 = AV65Lote_DataIni3;
         AV169WWLoteDS_52_Lote_dataini_to3 = AV66Lote_DataIni_To3;
         AV170WWLoteDS_53_Lote_datafim3 = AV67Lote_DataFim3;
         AV171WWLoteDS_54_Lote_datafim_to3 = AV68Lote_DataFim_To3;
         AV172WWLoteDS_55_Lote_nfe3 = AV69Lote_NFe3;
         AV173WWLoteDS_56_Lote_datanfe3 = AV70Lote_DataNfe3;
         AV174WWLoteDS_57_Lote_datanfe_to3 = AV71Lote_DataNfe_To3;
         AV175WWLoteDS_58_Lote_qtdedmn3 = AV72Lote_QtdeDmn3;
         AV176WWLoteDS_59_Lote_qtdedmn_to3 = AV73Lote_QtdeDmn_To3;
         AV177WWLoteDS_60_Lote_valor3 = AV74Lote_Valor3;
         AV178WWLoteDS_61_Lote_valor_to3 = AV75Lote_Valor_To3;
         AV179WWLoteDS_62_Lote_valorpf3 = AV76Lote_ValorPF3;
         AV180WWLoteDS_63_Lote_valorpf_to3 = AV77Lote_ValorPF_To3;
         AV181WWLoteDS_64_Lote_contratadanom3 = AV84Lote_ContratadaNom3;
         AV182WWLoteDS_65_Tflote_nfe = AV109TFLote_NFe;
         AV183WWLoteDS_66_Tflote_nfe_to = AV110TFLote_NFe_To;
         AV184WWLoteDS_67_Tflote_datanfe = AV111TFLote_DataNfe;
         AV185WWLoteDS_68_Tflote_datanfe_to = AV112TFLote_DataNfe_To;
         AV186WWLoteDS_69_Tflote_prevpagamento = AV113TFLote_PrevPagamento;
         AV187WWLoteDS_70_Tflote_prevpagamento_to = AV114TFLote_PrevPagamento_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV120WWLoteDS_3_Dynamicfiltersselector1 ,
                                              AV121WWLoteDS_4_Lote_numero1 ,
                                              AV122WWLoteDS_5_Lote_nome1 ,
                                              AV123WWLoteDS_6_Lote_data1 ,
                                              AV124WWLoteDS_7_Lote_data_to1 ,
                                              AV125WWLoteDS_8_Lote_usernom1 ,
                                              AV130WWLoteDS_13_Lote_nfe1 ,
                                              AV131WWLoteDS_14_Lote_datanfe1 ,
                                              AV132WWLoteDS_15_Lote_datanfe_to1 ,
                                              AV133WWLoteDS_16_Lote_qtdedmn1 ,
                                              AV134WWLoteDS_17_Lote_qtdedmn_to1 ,
                                              AV137WWLoteDS_20_Lote_valorpf1 ,
                                              AV138WWLoteDS_21_Lote_valorpf_to1 ,
                                              AV140WWLoteDS_23_Dynamicfiltersenabled2 ,
                                              AV141WWLoteDS_24_Dynamicfiltersselector2 ,
                                              AV142WWLoteDS_25_Lote_numero2 ,
                                              AV143WWLoteDS_26_Lote_nome2 ,
                                              AV144WWLoteDS_27_Lote_data2 ,
                                              AV145WWLoteDS_28_Lote_data_to2 ,
                                              AV146WWLoteDS_29_Lote_usernom2 ,
                                              AV151WWLoteDS_34_Lote_nfe2 ,
                                              AV152WWLoteDS_35_Lote_datanfe2 ,
                                              AV153WWLoteDS_36_Lote_datanfe_to2 ,
                                              AV154WWLoteDS_37_Lote_qtdedmn2 ,
                                              AV155WWLoteDS_38_Lote_qtdedmn_to2 ,
                                              AV158WWLoteDS_41_Lote_valorpf2 ,
                                              AV159WWLoteDS_42_Lote_valorpf_to2 ,
                                              AV161WWLoteDS_44_Dynamicfiltersenabled3 ,
                                              AV162WWLoteDS_45_Dynamicfiltersselector3 ,
                                              AV163WWLoteDS_46_Lote_numero3 ,
                                              AV164WWLoteDS_47_Lote_nome3 ,
                                              AV165WWLoteDS_48_Lote_data3 ,
                                              AV166WWLoteDS_49_Lote_data_to3 ,
                                              AV167WWLoteDS_50_Lote_usernom3 ,
                                              AV172WWLoteDS_55_Lote_nfe3 ,
                                              AV173WWLoteDS_56_Lote_datanfe3 ,
                                              AV174WWLoteDS_57_Lote_datanfe_to3 ,
                                              AV175WWLoteDS_58_Lote_qtdedmn3 ,
                                              AV176WWLoteDS_59_Lote_qtdedmn_to3 ,
                                              AV179WWLoteDS_62_Lote_valorpf3 ,
                                              AV180WWLoteDS_63_Lote_valorpf_to3 ,
                                              AV182WWLoteDS_65_Tflote_nfe ,
                                              AV183WWLoteDS_66_Tflote_nfe_to ,
                                              AV184WWLoteDS_67_Tflote_datanfe ,
                                              AV185WWLoteDS_68_Tflote_datanfe_to ,
                                              AV186WWLoteDS_69_Tflote_prevpagamento ,
                                              AV187WWLoteDS_70_Tflote_prevpagamento_to ,
                                              A562Lote_Numero ,
                                              A563Lote_Nome ,
                                              A564Lote_Data ,
                                              A561Lote_UserNom ,
                                              A673Lote_NFe ,
                                              A674Lote_DataNfe ,
                                              A569Lote_QtdeDmn ,
                                              A565Lote_ValorPF ,
                                              A857Lote_PrevPagamento ,
                                              AV16OrderedBy ,
                                              AV17OrderedDsc ,
                                              AV9WWPContext.gxTpr_Userehcontratante ,
                                              A1231Lote_ContratadaCod ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV126WWLoteDS_9_Lote_dataini1 ,
                                              A567Lote_DataIni ,
                                              AV127WWLoteDS_10_Lote_dataini_to1 ,
                                              AV128WWLoteDS_11_Lote_datafim1 ,
                                              A568Lote_DataFim ,
                                              AV129WWLoteDS_12_Lote_datafim_to1 ,
                                              AV135WWLoteDS_18_Lote_valor1 ,
                                              A572Lote_Valor ,
                                              AV136WWLoteDS_19_Lote_valor_to1 ,
                                              AV139WWLoteDS_22_Lote_contratadanom1 ,
                                              A1748Lote_ContratadaNom ,
                                              AV147WWLoteDS_30_Lote_dataini2 ,
                                              AV148WWLoteDS_31_Lote_dataini_to2 ,
                                              AV149WWLoteDS_32_Lote_datafim2 ,
                                              AV150WWLoteDS_33_Lote_datafim_to2 ,
                                              AV156WWLoteDS_39_Lote_valor2 ,
                                              AV157WWLoteDS_40_Lote_valor_to2 ,
                                              AV160WWLoteDS_43_Lote_contratadanom2 ,
                                              AV168WWLoteDS_51_Lote_dataini3 ,
                                              AV169WWLoteDS_52_Lote_dataini_to3 ,
                                              AV170WWLoteDS_53_Lote_datafim3 ,
                                              AV171WWLoteDS_54_Lote_datafim_to3 ,
                                              AV177WWLoteDS_60_Lote_valor3 ,
                                              AV178WWLoteDS_61_Lote_valor_to3 ,
                                              AV181WWLoteDS_64_Lote_contratadanom3 ,
                                              AV79GridState.gxTpr_Dynamicfilters.Count ,
                                              Gx_date ,
                                              A595Lote_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV139WWLoteDS_22_Lote_contratadanom1 = StringUtil.PadR( StringUtil.RTrim( AV139WWLoteDS_22_Lote_contratadanom1), 50, "%");
         lV160WWLoteDS_43_Lote_contratadanom2 = StringUtil.PadR( StringUtil.RTrim( AV160WWLoteDS_43_Lote_contratadanom2), 50, "%");
         lV181WWLoteDS_64_Lote_contratadanom3 = StringUtil.PadR( StringUtil.RTrim( AV181WWLoteDS_64_Lote_contratadanom3), 50, "%");
         lV121WWLoteDS_4_Lote_numero1 = StringUtil.PadR( StringUtil.RTrim( AV121WWLoteDS_4_Lote_numero1), 10, "%");
         lV122WWLoteDS_5_Lote_nome1 = StringUtil.PadR( StringUtil.RTrim( AV122WWLoteDS_5_Lote_nome1), 50, "%");
         lV125WWLoteDS_8_Lote_usernom1 = StringUtil.PadR( StringUtil.RTrim( AV125WWLoteDS_8_Lote_usernom1), 100, "%");
         lV142WWLoteDS_25_Lote_numero2 = StringUtil.PadR( StringUtil.RTrim( AV142WWLoteDS_25_Lote_numero2), 10, "%");
         lV143WWLoteDS_26_Lote_nome2 = StringUtil.PadR( StringUtil.RTrim( AV143WWLoteDS_26_Lote_nome2), 50, "%");
         lV146WWLoteDS_29_Lote_usernom2 = StringUtil.PadR( StringUtil.RTrim( AV146WWLoteDS_29_Lote_usernom2), 100, "%");
         lV163WWLoteDS_46_Lote_numero3 = StringUtil.PadR( StringUtil.RTrim( AV163WWLoteDS_46_Lote_numero3), 10, "%");
         lV164WWLoteDS_47_Lote_nome3 = StringUtil.PadR( StringUtil.RTrim( AV164WWLoteDS_47_Lote_nome3), 50, "%");
         lV167WWLoteDS_50_Lote_usernom3 = StringUtil.PadR( StringUtil.RTrim( AV167WWLoteDS_50_Lote_usernom3), 100, "%");
         /* Using cursor P00689 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Userehcontratante, AV9WWPContext.gxTpr_Contratada_codigo, AV120WWLoteDS_3_Dynamicfiltersselector1, AV126WWLoteDS_9_Lote_dataini1, AV126WWLoteDS_9_Lote_dataini1, AV120WWLoteDS_3_Dynamicfiltersselector1, AV127WWLoteDS_10_Lote_dataini_to1, AV127WWLoteDS_10_Lote_dataini_to1, AV120WWLoteDS_3_Dynamicfiltersselector1, AV128WWLoteDS_11_Lote_datafim1, AV128WWLoteDS_11_Lote_datafim1, AV120WWLoteDS_3_Dynamicfiltersselector1, AV129WWLoteDS_12_Lote_datafim_to1, AV129WWLoteDS_12_Lote_datafim_to1, AV120WWLoteDS_3_Dynamicfiltersselector1, AV139WWLoteDS_22_Lote_contratadanom1, lV139WWLoteDS_22_Lote_contratadanom1, AV140WWLoteDS_23_Dynamicfiltersenabled2, AV141WWLoteDS_24_Dynamicfiltersselector2, AV147WWLoteDS_30_Lote_dataini2, AV147WWLoteDS_30_Lote_dataini2, AV140WWLoteDS_23_Dynamicfiltersenabled2, AV141WWLoteDS_24_Dynamicfiltersselector2, AV148WWLoteDS_31_Lote_dataini_to2, AV148WWLoteDS_31_Lote_dataini_to2, AV140WWLoteDS_23_Dynamicfiltersenabled2, AV141WWLoteDS_24_Dynamicfiltersselector2, AV149WWLoteDS_32_Lote_datafim2, AV149WWLoteDS_32_Lote_datafim2, AV140WWLoteDS_23_Dynamicfiltersenabled2, AV141WWLoteDS_24_Dynamicfiltersselector2, AV150WWLoteDS_33_Lote_datafim_to2, AV150WWLoteDS_33_Lote_datafim_to2, AV140WWLoteDS_23_Dynamicfiltersenabled2, AV141WWLoteDS_24_Dynamicfiltersselector2, AV160WWLoteDS_43_Lote_contratadanom2, lV160WWLoteDS_43_Lote_contratadanom2, AV161WWLoteDS_44_Dynamicfiltersenabled3, AV162WWLoteDS_45_Dynamicfiltersselector3, AV168WWLoteDS_51_Lote_dataini3, AV168WWLoteDS_51_Lote_dataini3, AV161WWLoteDS_44_Dynamicfiltersenabled3, AV162WWLoteDS_45_Dynamicfiltersselector3, AV169WWLoteDS_52_Lote_dataini_to3, AV169WWLoteDS_52_Lote_dataini_to3, AV161WWLoteDS_44_Dynamicfiltersenabled3, AV162WWLoteDS_45_Dynamicfiltersselector3, AV170WWLoteDS_53_Lote_datafim3, AV170WWLoteDS_53_Lote_datafim3, AV161WWLoteDS_44_Dynamicfiltersenabled3, AV162WWLoteDS_45_Dynamicfiltersselector3, AV171WWLoteDS_54_Lote_datafim_to3, AV171WWLoteDS_54_Lote_datafim_to3, AV161WWLoteDS_44_Dynamicfiltersenabled3, AV162WWLoteDS_45_Dynamicfiltersselector3, AV181WWLoteDS_64_Lote_contratadanom3, lV181WWLoteDS_64_Lote_contratadanom3, AV9WWPContext.gxTpr_Areatrabalho_codigo, lV121WWLoteDS_4_Lote_numero1, lV122WWLoteDS_5_Lote_nome1, AV123WWLoteDS_6_Lote_data1, AV124WWLoteDS_7_Lote_data_to1, lV125WWLoteDS_8_Lote_usernom1, AV130WWLoteDS_13_Lote_nfe1, AV131WWLoteDS_14_Lote_datanfe1, AV132WWLoteDS_15_Lote_datanfe_to1, AV133WWLoteDS_16_Lote_qtdedmn1, AV134WWLoteDS_17_Lote_qtdedmn_to1, AV137WWLoteDS_20_Lote_valorpf1, AV138WWLoteDS_21_Lote_valorpf_to1, lV142WWLoteDS_25_Lote_numero2, lV143WWLoteDS_26_Lote_nome2, AV144WWLoteDS_27_Lote_data2, AV145WWLoteDS_28_Lote_data_to2, lV146WWLoteDS_29_Lote_usernom2, AV151WWLoteDS_34_Lote_nfe2, AV152WWLoteDS_35_Lote_datanfe2, AV153WWLoteDS_36_Lote_datanfe_to2, AV154WWLoteDS_37_Lote_qtdedmn2, AV155WWLoteDS_38_Lote_qtdedmn_to2, AV158WWLoteDS_41_Lote_valorpf2, AV159WWLoteDS_42_Lote_valorpf_to2, lV163WWLoteDS_46_Lote_numero3, lV164WWLoteDS_47_Lote_nome3, AV165WWLoteDS_48_Lote_data3, AV166WWLoteDS_49_Lote_data_to3, lV167WWLoteDS_50_Lote_usernom3, AV172WWLoteDS_55_Lote_nfe3, AV173WWLoteDS_56_Lote_datanfe3, AV174WWLoteDS_57_Lote_datanfe_to3, AV175WWLoteDS_58_Lote_qtdedmn3, AV176WWLoteDS_59_Lote_qtdedmn_to3, AV179WWLoteDS_62_Lote_valorpf3, AV180WWLoteDS_63_Lote_valorpf_to3, AV182WWLoteDS_65_Tflote_nfe, AV183WWLoteDS_66_Tflote_nfe_to, AV184WWLoteDS_67_Tflote_datanfe, AV185WWLoteDS_68_Tflote_datanfe_to, AV186WWLoteDS_69_Tflote_prevpagamento, AV187WWLoteDS_70_Tflote_prevpagamento_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A559Lote_UserCod = P00689_A559Lote_UserCod[0];
            A560Lote_PessoaCod = P00689_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00689_n560Lote_PessoaCod[0];
            A857Lote_PrevPagamento = P00689_A857Lote_PrevPagamento[0];
            n857Lote_PrevPagamento = P00689_n857Lote_PrevPagamento[0];
            A565Lote_ValorPF = P00689_A565Lote_ValorPF[0];
            A674Lote_DataNfe = P00689_A674Lote_DataNfe[0];
            n674Lote_DataNfe = P00689_n674Lote_DataNfe[0];
            A673Lote_NFe = P00689_A673Lote_NFe[0];
            n673Lote_NFe = P00689_n673Lote_NFe[0];
            A561Lote_UserNom = P00689_A561Lote_UserNom[0];
            n561Lote_UserNom = P00689_n561Lote_UserNom[0];
            A564Lote_Data = P00689_A564Lote_Data[0];
            A563Lote_Nome = P00689_A563Lote_Nome[0];
            A562Lote_Numero = P00689_A562Lote_Numero[0];
            A595Lote_AreaTrabalhoCod = P00689_A595Lote_AreaTrabalhoCod[0];
            A596Lote_Codigo = P00689_A596Lote_Codigo[0];
            A1748Lote_ContratadaNom = P00689_A1748Lote_ContratadaNom[0];
            n1748Lote_ContratadaNom = P00689_n1748Lote_ContratadaNom[0];
            A569Lote_QtdeDmn = P00689_A569Lote_QtdeDmn[0];
            A568Lote_DataFim = P00689_A568Lote_DataFim[0];
            A567Lote_DataIni = P00689_A567Lote_DataIni[0];
            A1231Lote_ContratadaCod = P00689_A1231Lote_ContratadaCod[0];
            A575Lote_Status = P00689_A575Lote_Status[0];
            A1057Lote_ValorGlosas = P00689_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00689_n1057Lote_ValorGlosas[0];
            A560Lote_PessoaCod = P00689_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00689_n560Lote_PessoaCod[0];
            A561Lote_UserNom = P00689_A561Lote_UserNom[0];
            n561Lote_UserNom = P00689_n561Lote_UserNom[0];
            A1748Lote_ContratadaNom = P00689_A1748Lote_ContratadaNom[0];
            n1748Lote_ContratadaNom = P00689_n1748Lote_ContratadaNom[0];
            A1057Lote_ValorGlosas = P00689_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00689_n1057Lote_ValorGlosas[0];
            A569Lote_QtdeDmn = P00689_A569Lote_QtdeDmn[0];
            A1231Lote_ContratadaCod = P00689_A1231Lote_ContratadaCod[0];
            A575Lote_Status = P00689_A575Lote_Status[0];
            A568Lote_DataFim = P00689_A568Lote_DataFim[0];
            A567Lote_DataIni = P00689_A567Lote_DataIni[0];
            if ( ( AV79GridState.gxTpr_Dynamicfilters.Count >= 1 ) || ( ( DateTimeUtil.Month( A564Lote_Data) == DateTimeUtil.Month( Gx_date) ) && ( DateTimeUtil.Year( A564Lote_Data) == DateTimeUtil.Year( Gx_date) ) ) )
            {
               GetLote_ValorOSs( A596Lote_Codigo) ;
               A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
               if ( ! ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_VALOR") == 0 ) && ( ! (Convert.ToDecimal(0)==AV135WWLoteDS_18_Lote_valor1) ) ) || ( ( A572Lote_Valor >= AV135WWLoteDS_18_Lote_valor1 ) ) )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_VALOR") == 0 ) && ( ! (Convert.ToDecimal(0)==AV136WWLoteDS_19_Lote_valor_to1) ) ) || ( ( A572Lote_Valor <= AV136WWLoteDS_19_Lote_valor_to1 ) ) )
                  {
                     if ( ! ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_VALOR") == 0 ) && ( ! (Convert.ToDecimal(0)==AV156WWLoteDS_39_Lote_valor2) ) ) || ( ( A572Lote_Valor >= AV156WWLoteDS_39_Lote_valor2 ) ) )
                     {
                        if ( ! ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_VALOR") == 0 ) && ( ! (Convert.ToDecimal(0)==AV157WWLoteDS_40_Lote_valor_to2) ) ) || ( ( A572Lote_Valor <= AV157WWLoteDS_40_Lote_valor_to2 ) ) )
                        {
                           if ( ! ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_VALOR") == 0 ) && ( ! (Convert.ToDecimal(0)==AV177WWLoteDS_60_Lote_valor3) ) ) || ( ( A572Lote_Valor >= AV177WWLoteDS_60_Lote_valor3 ) ) )
                           {
                              if ( ! ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_VALOR") == 0 ) && ( ! (Convert.ToDecimal(0)==AV178WWLoteDS_61_Lote_valor_to3) ) ) || ( ( A572Lote_Valor <= AV178WWLoteDS_61_Lote_valor_to3 ) ) )
                              {
                                 AV13CellRow = (int)(AV13CellRow+1);
                                 /* Execute user subroutine: 'BEFOREWRITELINE' */
                                 S172 ();
                                 if ( returnInSub )
                                 {
                                    pr_default.close(0);
                                    returnInSub = true;
                                    if (true) return;
                                 }
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = A562Lote_Numero;
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = A563Lote_Nome;
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = A1748Lote_ContratadaNom;
                                 AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = A564Lote_Data;
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Text = A561Lote_UserNom;
                                 GXt_dtime1 = DateTimeUtil.ResetTime( A567Lote_DataIni ) ;
                                 AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Date = GXt_dtime1;
                                 GXt_dtime1 = DateTimeUtil.ResetTime( A568Lote_DataFim ) ;
                                 AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Date = GXt_dtime1;
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,A575Lote_Status);
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Number = A569Lote_QtdeDmn;
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Number = (double)(A572Lote_Valor);
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Number = A673Lote_NFe;
                                 GXt_dtime1 = DateTimeUtil.ResetTime( A674Lote_DataNfe ) ;
                                 AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Date = GXt_dtime1;
                                 GXt_dtime1 = DateTimeUtil.ResetTime( A857Lote_PrevPagamento ) ;
                                 AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                                 AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Date = GXt_dtime1;
                                 /* Execute user subroutine: 'AFTERWRITELINE' */
                                 S182 ();
                                 if ( returnInSub )
                                 {
                                    pr_default.close(0);
                                    returnInSub = true;
                                    if (true) return;
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S191( )
      {
         /* 'CLOSEDOCUMENT' Routine */
         AV10ExcelDocument.Save();
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Close();
      }

      protected void S121( )
      {
         /* 'CHECKSTATUS' Routine */
         if ( AV10ExcelDocument.ErrCode != 0 )
         {
            AV11Filename = "";
            AV12ErrorMessage = AV10ExcelDocument.ErrDescription;
            AV10ExcelDocument.Close();
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'BEFOREWRITELINE' Routine */
      }

      protected void S182( )
      {
         /* 'AFTERWRITELINE' Routine */
      }

      protected void GetLote_ValorOSs( int A596Lote_Codigo )
      {
         /* Create private dbobjects */
         /* Navigation */
         A1058Lote_ValorOSs = 0;
         /* Using cursor P006810 */
         pr_default.execute(1, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(1) != 101) && ( P006810_A597ContagemResultado_LoteAceiteCod[0] == A596Lote_Codigo ) )
         {
            GXt_decimal2 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  P006810_A456ContagemResultado_Codigo[0], out  GXt_decimal2) ;
            A574ContagemResultado_PFFinal = GXt_decimal2;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*P006810_A512ContagemResultado_ValorPF[0]);
            A1058Lote_ValorOSs = (decimal)(A1058Lote_ValorOSs+A606ContagemResultado_ValorFinal);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10ExcelDocument = new ExcelDocumentI();
         AV79GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV80GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV19DynamicFiltersSelector1 = "";
         AV20Lote_Numero1 = "";
         AV21Lote_Nome1 = "";
         AV22Lote_Data1 = (DateTime)(DateTime.MinValue);
         AV23Lote_Data_To1 = (DateTime)(DateTime.MinValue);
         AV24Lote_UserNom1 = "";
         AV25Lote_DataIni1 = DateTime.MinValue;
         AV26Lote_DataIni_To1 = DateTime.MinValue;
         AV27Lote_DataFim1 = DateTime.MinValue;
         AV28Lote_DataFim_To1 = DateTime.MinValue;
         AV30Lote_DataNfe1 = DateTime.MinValue;
         AV31Lote_DataNfe_To1 = DateTime.MinValue;
         AV82Lote_ContratadaNom1 = "";
         AV39DynamicFiltersSelector2 = "";
         AV40Lote_Numero2 = "";
         AV41Lote_Nome2 = "";
         AV42Lote_Data2 = (DateTime)(DateTime.MinValue);
         AV43Lote_Data_To2 = (DateTime)(DateTime.MinValue);
         AV44Lote_UserNom2 = "";
         AV45Lote_DataIni2 = DateTime.MinValue;
         AV46Lote_DataIni_To2 = DateTime.MinValue;
         AV47Lote_DataFim2 = DateTime.MinValue;
         AV48Lote_DataFim_To2 = DateTime.MinValue;
         AV50Lote_DataNfe2 = DateTime.MinValue;
         AV51Lote_DataNfe_To2 = DateTime.MinValue;
         AV83Lote_ContratadaNom2 = "";
         AV59DynamicFiltersSelector3 = "";
         AV60Lote_Numero3 = "";
         AV61Lote_Nome3 = "";
         AV62Lote_Data3 = (DateTime)(DateTime.MinValue);
         AV63Lote_Data_To3 = (DateTime)(DateTime.MinValue);
         AV64Lote_UserNom3 = "";
         AV65Lote_DataIni3 = DateTime.MinValue;
         AV66Lote_DataIni_To3 = DateTime.MinValue;
         AV67Lote_DataFim3 = DateTime.MinValue;
         AV68Lote_DataFim_To3 = DateTime.MinValue;
         AV70Lote_DataNfe3 = DateTime.MinValue;
         AV71Lote_DataNfe_To3 = DateTime.MinValue;
         AV84Lote_ContratadaNom3 = "";
         AV120WWLoteDS_3_Dynamicfiltersselector1 = "";
         AV121WWLoteDS_4_Lote_numero1 = "";
         AV122WWLoteDS_5_Lote_nome1 = "";
         AV123WWLoteDS_6_Lote_data1 = (DateTime)(DateTime.MinValue);
         AV124WWLoteDS_7_Lote_data_to1 = (DateTime)(DateTime.MinValue);
         AV125WWLoteDS_8_Lote_usernom1 = "";
         AV126WWLoteDS_9_Lote_dataini1 = DateTime.MinValue;
         AV127WWLoteDS_10_Lote_dataini_to1 = DateTime.MinValue;
         AV128WWLoteDS_11_Lote_datafim1 = DateTime.MinValue;
         AV129WWLoteDS_12_Lote_datafim_to1 = DateTime.MinValue;
         AV131WWLoteDS_14_Lote_datanfe1 = DateTime.MinValue;
         AV132WWLoteDS_15_Lote_datanfe_to1 = DateTime.MinValue;
         AV139WWLoteDS_22_Lote_contratadanom1 = "";
         AV141WWLoteDS_24_Dynamicfiltersselector2 = "";
         AV142WWLoteDS_25_Lote_numero2 = "";
         AV143WWLoteDS_26_Lote_nome2 = "";
         AV144WWLoteDS_27_Lote_data2 = (DateTime)(DateTime.MinValue);
         AV145WWLoteDS_28_Lote_data_to2 = (DateTime)(DateTime.MinValue);
         AV146WWLoteDS_29_Lote_usernom2 = "";
         AV147WWLoteDS_30_Lote_dataini2 = DateTime.MinValue;
         AV148WWLoteDS_31_Lote_dataini_to2 = DateTime.MinValue;
         AV149WWLoteDS_32_Lote_datafim2 = DateTime.MinValue;
         AV150WWLoteDS_33_Lote_datafim_to2 = DateTime.MinValue;
         AV152WWLoteDS_35_Lote_datanfe2 = DateTime.MinValue;
         AV153WWLoteDS_36_Lote_datanfe_to2 = DateTime.MinValue;
         AV160WWLoteDS_43_Lote_contratadanom2 = "";
         AV162WWLoteDS_45_Dynamicfiltersselector3 = "";
         AV163WWLoteDS_46_Lote_numero3 = "";
         AV164WWLoteDS_47_Lote_nome3 = "";
         AV165WWLoteDS_48_Lote_data3 = (DateTime)(DateTime.MinValue);
         AV166WWLoteDS_49_Lote_data_to3 = (DateTime)(DateTime.MinValue);
         AV167WWLoteDS_50_Lote_usernom3 = "";
         AV168WWLoteDS_51_Lote_dataini3 = DateTime.MinValue;
         AV169WWLoteDS_52_Lote_dataini_to3 = DateTime.MinValue;
         AV170WWLoteDS_53_Lote_datafim3 = DateTime.MinValue;
         AV171WWLoteDS_54_Lote_datafim_to3 = DateTime.MinValue;
         AV173WWLoteDS_56_Lote_datanfe3 = DateTime.MinValue;
         AV174WWLoteDS_57_Lote_datanfe_to3 = DateTime.MinValue;
         AV181WWLoteDS_64_Lote_contratadanom3 = "";
         AV184WWLoteDS_67_Tflote_datanfe = DateTime.MinValue;
         AV185WWLoteDS_68_Tflote_datanfe_to = DateTime.MinValue;
         AV186WWLoteDS_69_Tflote_prevpagamento = DateTime.MinValue;
         AV187WWLoteDS_70_Tflote_prevpagamento_to = DateTime.MinValue;
         scmdbuf = "";
         lV139WWLoteDS_22_Lote_contratadanom1 = "";
         lV160WWLoteDS_43_Lote_contratadanom2 = "";
         lV181WWLoteDS_64_Lote_contratadanom3 = "";
         lV121WWLoteDS_4_Lote_numero1 = "";
         lV122WWLoteDS_5_Lote_nome1 = "";
         lV125WWLoteDS_8_Lote_usernom1 = "";
         lV142WWLoteDS_25_Lote_numero2 = "";
         lV143WWLoteDS_26_Lote_nome2 = "";
         lV146WWLoteDS_29_Lote_usernom2 = "";
         lV163WWLoteDS_46_Lote_numero3 = "";
         lV164WWLoteDS_47_Lote_nome3 = "";
         lV167WWLoteDS_50_Lote_usernom3 = "";
         A562Lote_Numero = "";
         A563Lote_Nome = "";
         A564Lote_Data = (DateTime)(DateTime.MinValue);
         A561Lote_UserNom = "";
         A674Lote_DataNfe = DateTime.MinValue;
         A857Lote_PrevPagamento = DateTime.MinValue;
         A567Lote_DataIni = DateTime.MinValue;
         A568Lote_DataFim = DateTime.MinValue;
         A1748Lote_ContratadaNom = "";
         Gx_date = DateTime.MinValue;
         P00689_A559Lote_UserCod = new int[1] ;
         P00689_A560Lote_PessoaCod = new int[1] ;
         P00689_n560Lote_PessoaCod = new bool[] {false} ;
         P00689_A857Lote_PrevPagamento = new DateTime[] {DateTime.MinValue} ;
         P00689_n857Lote_PrevPagamento = new bool[] {false} ;
         P00689_A565Lote_ValorPF = new decimal[1] ;
         P00689_A674Lote_DataNfe = new DateTime[] {DateTime.MinValue} ;
         P00689_n674Lote_DataNfe = new bool[] {false} ;
         P00689_A673Lote_NFe = new int[1] ;
         P00689_n673Lote_NFe = new bool[] {false} ;
         P00689_A561Lote_UserNom = new String[] {""} ;
         P00689_n561Lote_UserNom = new bool[] {false} ;
         P00689_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         P00689_A563Lote_Nome = new String[] {""} ;
         P00689_A562Lote_Numero = new String[] {""} ;
         P00689_A595Lote_AreaTrabalhoCod = new int[1] ;
         P00689_A596Lote_Codigo = new int[1] ;
         P00689_A1748Lote_ContratadaNom = new String[] {""} ;
         P00689_n1748Lote_ContratadaNom = new bool[] {false} ;
         P00689_A569Lote_QtdeDmn = new short[1] ;
         P00689_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         P00689_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         P00689_A1231Lote_ContratadaCod = new int[1] ;
         P00689_A575Lote_Status = new String[] {""} ;
         P00689_A1057Lote_ValorGlosas = new decimal[1] ;
         P00689_n1057Lote_ValorGlosas = new bool[] {false} ;
         A575Lote_Status = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         P006810_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006810_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006810_A512ContagemResultado_ValorPF = new decimal[1] ;
         P006810_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P006810_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.exportwwlote__default(),
            new Object[][] {
                new Object[] {
               P00689_A559Lote_UserCod, P00689_A560Lote_PessoaCod, P00689_n560Lote_PessoaCod, P00689_A857Lote_PrevPagamento, P00689_n857Lote_PrevPagamento, P00689_A565Lote_ValorPF, P00689_A674Lote_DataNfe, P00689_n674Lote_DataNfe, P00689_A673Lote_NFe, P00689_n673Lote_NFe,
               P00689_A561Lote_UserNom, P00689_n561Lote_UserNom, P00689_A564Lote_Data, P00689_A563Lote_Nome, P00689_A562Lote_Numero, P00689_A595Lote_AreaTrabalhoCod, P00689_A596Lote_Codigo, P00689_A1748Lote_ContratadaNom, P00689_n1748Lote_ContratadaNom, P00689_A569Lote_QtdeDmn,
               P00689_A568Lote_DataFim, P00689_A567Lote_DataIni, P00689_A1231Lote_ContratadaCod, P00689_A575Lote_Status, P00689_A1057Lote_ValorGlosas, P00689_n1057Lote_ValorGlosas
               }
               , new Object[] {
               P006810_A597ContagemResultado_LoteAceiteCod, P006810_n597ContagemResultado_LoteAceiteCod, P006810_A512ContagemResultado_ValorPF, P006810_n512ContagemResultado_ValorPF, P006810_A456ContagemResultado_Codigo
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short AV16OrderedBy ;
      private short AV32Lote_QtdeDmn1 ;
      private short AV33Lote_QtdeDmn_To1 ;
      private short AV52Lote_QtdeDmn2 ;
      private short AV53Lote_QtdeDmn_To2 ;
      private short AV72Lote_QtdeDmn3 ;
      private short AV73Lote_QtdeDmn_To3 ;
      private short AV133WWLoteDS_16_Lote_qtdedmn1 ;
      private short AV134WWLoteDS_17_Lote_qtdedmn_to1 ;
      private short AV154WWLoteDS_37_Lote_qtdedmn2 ;
      private short AV155WWLoteDS_38_Lote_qtdedmn_to2 ;
      private short AV175WWLoteDS_58_Lote_qtdedmn3 ;
      private short AV176WWLoteDS_59_Lote_qtdedmn_to3 ;
      private short A569Lote_QtdeDmn ;
      private int AV18Lote_AreaTrabalhoCod ;
      private int AV81Lote_ContratadaCod ;
      private int AV109TFLote_NFe ;
      private int AV110TFLote_NFe_To ;
      private int AV13CellRow ;
      private int AV14FirstColumn ;
      private int AV15Random ;
      private int AV29Lote_NFe1 ;
      private int AV49Lote_NFe2 ;
      private int AV69Lote_NFe3 ;
      private int AV118WWLoteDS_1_Lote_areatrabalhocod ;
      private int AV119WWLoteDS_2_Lote_contratadacod ;
      private int AV130WWLoteDS_13_Lote_nfe1 ;
      private int AV151WWLoteDS_34_Lote_nfe2 ;
      private int AV172WWLoteDS_55_Lote_nfe3 ;
      private int AV182WWLoteDS_65_Tflote_nfe ;
      private int AV183WWLoteDS_66_Tflote_nfe_to ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int AV79GridState_gxTpr_Dynamicfilters_Count ;
      private int A673Lote_NFe ;
      private int A1231Lote_ContratadaCod ;
      private int A595Lote_AreaTrabalhoCod ;
      private int A559Lote_UserCod ;
      private int A560Lote_PessoaCod ;
      private int A596Lote_Codigo ;
      private decimal AV34Lote_Valor1 ;
      private decimal AV35Lote_Valor_To1 ;
      private decimal AV36Lote_ValorPF1 ;
      private decimal AV37Lote_ValorPF_To1 ;
      private decimal AV54Lote_Valor2 ;
      private decimal AV55Lote_Valor_To2 ;
      private decimal AV56Lote_ValorPF2 ;
      private decimal AV57Lote_ValorPF_To2 ;
      private decimal AV74Lote_Valor3 ;
      private decimal AV75Lote_Valor_To3 ;
      private decimal AV76Lote_ValorPF3 ;
      private decimal AV77Lote_ValorPF_To3 ;
      private decimal AV135WWLoteDS_18_Lote_valor1 ;
      private decimal AV136WWLoteDS_19_Lote_valor_to1 ;
      private decimal AV137WWLoteDS_20_Lote_valorpf1 ;
      private decimal AV138WWLoteDS_21_Lote_valorpf_to1 ;
      private decimal AV156WWLoteDS_39_Lote_valor2 ;
      private decimal AV157WWLoteDS_40_Lote_valor_to2 ;
      private decimal AV158WWLoteDS_41_Lote_valorpf2 ;
      private decimal AV159WWLoteDS_42_Lote_valorpf_to2 ;
      private decimal AV177WWLoteDS_60_Lote_valor3 ;
      private decimal AV178WWLoteDS_61_Lote_valor_to3 ;
      private decimal AV179WWLoteDS_62_Lote_valorpf3 ;
      private decimal AV180WWLoteDS_63_Lote_valorpf_to3 ;
      private decimal A565Lote_ValorPF ;
      private decimal A572Lote_Valor ;
      private decimal A1057Lote_ValorGlosas ;
      private decimal A1058Lote_ValorOSs ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal2 ;
      private decimal A606ContagemResultado_ValorFinal ;
      private String AV20Lote_Numero1 ;
      private String AV21Lote_Nome1 ;
      private String AV24Lote_UserNom1 ;
      private String AV82Lote_ContratadaNom1 ;
      private String AV40Lote_Numero2 ;
      private String AV41Lote_Nome2 ;
      private String AV44Lote_UserNom2 ;
      private String AV83Lote_ContratadaNom2 ;
      private String AV60Lote_Numero3 ;
      private String AV61Lote_Nome3 ;
      private String AV64Lote_UserNom3 ;
      private String AV84Lote_ContratadaNom3 ;
      private String AV121WWLoteDS_4_Lote_numero1 ;
      private String AV122WWLoteDS_5_Lote_nome1 ;
      private String AV125WWLoteDS_8_Lote_usernom1 ;
      private String AV139WWLoteDS_22_Lote_contratadanom1 ;
      private String AV142WWLoteDS_25_Lote_numero2 ;
      private String AV143WWLoteDS_26_Lote_nome2 ;
      private String AV146WWLoteDS_29_Lote_usernom2 ;
      private String AV160WWLoteDS_43_Lote_contratadanom2 ;
      private String AV163WWLoteDS_46_Lote_numero3 ;
      private String AV164WWLoteDS_47_Lote_nome3 ;
      private String AV167WWLoteDS_50_Lote_usernom3 ;
      private String AV181WWLoteDS_64_Lote_contratadanom3 ;
      private String scmdbuf ;
      private String lV139WWLoteDS_22_Lote_contratadanom1 ;
      private String lV160WWLoteDS_43_Lote_contratadanom2 ;
      private String lV181WWLoteDS_64_Lote_contratadanom3 ;
      private String lV121WWLoteDS_4_Lote_numero1 ;
      private String lV122WWLoteDS_5_Lote_nome1 ;
      private String lV125WWLoteDS_8_Lote_usernom1 ;
      private String lV142WWLoteDS_25_Lote_numero2 ;
      private String lV143WWLoteDS_26_Lote_nome2 ;
      private String lV146WWLoteDS_29_Lote_usernom2 ;
      private String lV163WWLoteDS_46_Lote_numero3 ;
      private String lV164WWLoteDS_47_Lote_nome3 ;
      private String lV167WWLoteDS_50_Lote_usernom3 ;
      private String A562Lote_Numero ;
      private String A563Lote_Nome ;
      private String A561Lote_UserNom ;
      private String A1748Lote_ContratadaNom ;
      private String A575Lote_Status ;
      private DateTime AV22Lote_Data1 ;
      private DateTime AV23Lote_Data_To1 ;
      private DateTime AV42Lote_Data2 ;
      private DateTime AV43Lote_Data_To2 ;
      private DateTime AV62Lote_Data3 ;
      private DateTime AV63Lote_Data_To3 ;
      private DateTime AV123WWLoteDS_6_Lote_data1 ;
      private DateTime AV124WWLoteDS_7_Lote_data_to1 ;
      private DateTime AV144WWLoteDS_27_Lote_data2 ;
      private DateTime AV145WWLoteDS_28_Lote_data_to2 ;
      private DateTime AV165WWLoteDS_48_Lote_data3 ;
      private DateTime AV166WWLoteDS_49_Lote_data_to3 ;
      private DateTime A564Lote_Data ;
      private DateTime GXt_dtime1 ;
      private DateTime AV111TFLote_DataNfe ;
      private DateTime AV112TFLote_DataNfe_To ;
      private DateTime AV113TFLote_PrevPagamento ;
      private DateTime AV114TFLote_PrevPagamento_To ;
      private DateTime AV25Lote_DataIni1 ;
      private DateTime AV26Lote_DataIni_To1 ;
      private DateTime AV27Lote_DataFim1 ;
      private DateTime AV28Lote_DataFim_To1 ;
      private DateTime AV30Lote_DataNfe1 ;
      private DateTime AV31Lote_DataNfe_To1 ;
      private DateTime AV45Lote_DataIni2 ;
      private DateTime AV46Lote_DataIni_To2 ;
      private DateTime AV47Lote_DataFim2 ;
      private DateTime AV48Lote_DataFim_To2 ;
      private DateTime AV50Lote_DataNfe2 ;
      private DateTime AV51Lote_DataNfe_To2 ;
      private DateTime AV65Lote_DataIni3 ;
      private DateTime AV66Lote_DataIni_To3 ;
      private DateTime AV67Lote_DataFim3 ;
      private DateTime AV68Lote_DataFim_To3 ;
      private DateTime AV70Lote_DataNfe3 ;
      private DateTime AV71Lote_DataNfe_To3 ;
      private DateTime AV126WWLoteDS_9_Lote_dataini1 ;
      private DateTime AV127WWLoteDS_10_Lote_dataini_to1 ;
      private DateTime AV128WWLoteDS_11_Lote_datafim1 ;
      private DateTime AV129WWLoteDS_12_Lote_datafim_to1 ;
      private DateTime AV131WWLoteDS_14_Lote_datanfe1 ;
      private DateTime AV132WWLoteDS_15_Lote_datanfe_to1 ;
      private DateTime AV147WWLoteDS_30_Lote_dataini2 ;
      private DateTime AV148WWLoteDS_31_Lote_dataini_to2 ;
      private DateTime AV149WWLoteDS_32_Lote_datafim2 ;
      private DateTime AV150WWLoteDS_33_Lote_datafim_to2 ;
      private DateTime AV152WWLoteDS_35_Lote_datanfe2 ;
      private DateTime AV153WWLoteDS_36_Lote_datanfe_to2 ;
      private DateTime AV168WWLoteDS_51_Lote_dataini3 ;
      private DateTime AV169WWLoteDS_52_Lote_dataini_to3 ;
      private DateTime AV170WWLoteDS_53_Lote_datafim3 ;
      private DateTime AV171WWLoteDS_54_Lote_datafim_to3 ;
      private DateTime AV173WWLoteDS_56_Lote_datanfe3 ;
      private DateTime AV174WWLoteDS_57_Lote_datanfe_to3 ;
      private DateTime AV184WWLoteDS_67_Tflote_datanfe ;
      private DateTime AV185WWLoteDS_68_Tflote_datanfe_to ;
      private DateTime AV186WWLoteDS_69_Tflote_prevpagamento ;
      private DateTime AV187WWLoteDS_70_Tflote_prevpagamento_to ;
      private DateTime A674Lote_DataNfe ;
      private DateTime A857Lote_PrevPagamento ;
      private DateTime A567Lote_DataIni ;
      private DateTime A568Lote_DataFim ;
      private DateTime Gx_date ;
      private bool AV17OrderedDsc ;
      private bool returnInSub ;
      private bool AV38DynamicFiltersEnabled2 ;
      private bool AV58DynamicFiltersEnabled3 ;
      private bool AV140WWLoteDS_23_Dynamicfiltersenabled2 ;
      private bool AV161WWLoteDS_44_Dynamicfiltersenabled3 ;
      private bool AV9WWPContext_gxTpr_Userehcontratante ;
      private bool n560Lote_PessoaCod ;
      private bool n857Lote_PrevPagamento ;
      private bool n674Lote_DataNfe ;
      private bool n673Lote_NFe ;
      private bool n561Lote_UserNom ;
      private bool n1748Lote_ContratadaNom ;
      private bool n1057Lote_ValorGlosas ;
      private String AV78GridStateXML ;
      private String AV12ErrorMessage ;
      private String AV11Filename ;
      private String AV19DynamicFiltersSelector1 ;
      private String AV39DynamicFiltersSelector2 ;
      private String AV59DynamicFiltersSelector3 ;
      private String AV120WWLoteDS_3_Dynamicfiltersselector1 ;
      private String AV141WWLoteDS_24_Dynamicfiltersselector2 ;
      private String AV162WWLoteDS_45_Dynamicfiltersselector3 ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00689_A559Lote_UserCod ;
      private int[] P00689_A560Lote_PessoaCod ;
      private bool[] P00689_n560Lote_PessoaCod ;
      private DateTime[] P00689_A857Lote_PrevPagamento ;
      private bool[] P00689_n857Lote_PrevPagamento ;
      private decimal[] P00689_A565Lote_ValorPF ;
      private DateTime[] P00689_A674Lote_DataNfe ;
      private bool[] P00689_n674Lote_DataNfe ;
      private int[] P00689_A673Lote_NFe ;
      private bool[] P00689_n673Lote_NFe ;
      private String[] P00689_A561Lote_UserNom ;
      private bool[] P00689_n561Lote_UserNom ;
      private DateTime[] P00689_A564Lote_Data ;
      private String[] P00689_A563Lote_Nome ;
      private String[] P00689_A562Lote_Numero ;
      private int[] P00689_A595Lote_AreaTrabalhoCod ;
      private int[] P00689_A596Lote_Codigo ;
      private String[] P00689_A1748Lote_ContratadaNom ;
      private bool[] P00689_n1748Lote_ContratadaNom ;
      private short[] P00689_A569Lote_QtdeDmn ;
      private DateTime[] P00689_A568Lote_DataFim ;
      private DateTime[] P00689_A567Lote_DataIni ;
      private int[] P00689_A1231Lote_ContratadaCod ;
      private String[] P00689_A575Lote_Status ;
      private decimal[] P00689_A1057Lote_ValorGlosas ;
      private bool[] P00689_n1057Lote_ValorGlosas ;
      private int[] P006810_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006810_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] P006810_A512ContagemResultado_ValorPF ;
      private bool[] P006810_n512ContagemResultado_ValorPF ;
      private int[] P006810_A456ContagemResultado_Codigo ;
      private String aP11_Filename ;
      private String aP12_ErrorMessage ;
      private ExcelDocumentI AV10ExcelDocument ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV79GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV80GridStateDynamicFilter ;
   }

   public class exportwwlote__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00689( IGxContext context ,
                                             String AV120WWLoteDS_3_Dynamicfiltersselector1 ,
                                             String AV121WWLoteDS_4_Lote_numero1 ,
                                             String AV122WWLoteDS_5_Lote_nome1 ,
                                             DateTime AV123WWLoteDS_6_Lote_data1 ,
                                             DateTime AV124WWLoteDS_7_Lote_data_to1 ,
                                             String AV125WWLoteDS_8_Lote_usernom1 ,
                                             int AV130WWLoteDS_13_Lote_nfe1 ,
                                             DateTime AV131WWLoteDS_14_Lote_datanfe1 ,
                                             DateTime AV132WWLoteDS_15_Lote_datanfe_to1 ,
                                             short AV133WWLoteDS_16_Lote_qtdedmn1 ,
                                             short AV134WWLoteDS_17_Lote_qtdedmn_to1 ,
                                             decimal AV137WWLoteDS_20_Lote_valorpf1 ,
                                             decimal AV138WWLoteDS_21_Lote_valorpf_to1 ,
                                             bool AV140WWLoteDS_23_Dynamicfiltersenabled2 ,
                                             String AV141WWLoteDS_24_Dynamicfiltersselector2 ,
                                             String AV142WWLoteDS_25_Lote_numero2 ,
                                             String AV143WWLoteDS_26_Lote_nome2 ,
                                             DateTime AV144WWLoteDS_27_Lote_data2 ,
                                             DateTime AV145WWLoteDS_28_Lote_data_to2 ,
                                             String AV146WWLoteDS_29_Lote_usernom2 ,
                                             int AV151WWLoteDS_34_Lote_nfe2 ,
                                             DateTime AV152WWLoteDS_35_Lote_datanfe2 ,
                                             DateTime AV153WWLoteDS_36_Lote_datanfe_to2 ,
                                             short AV154WWLoteDS_37_Lote_qtdedmn2 ,
                                             short AV155WWLoteDS_38_Lote_qtdedmn_to2 ,
                                             decimal AV158WWLoteDS_41_Lote_valorpf2 ,
                                             decimal AV159WWLoteDS_42_Lote_valorpf_to2 ,
                                             bool AV161WWLoteDS_44_Dynamicfiltersenabled3 ,
                                             String AV162WWLoteDS_45_Dynamicfiltersselector3 ,
                                             String AV163WWLoteDS_46_Lote_numero3 ,
                                             String AV164WWLoteDS_47_Lote_nome3 ,
                                             DateTime AV165WWLoteDS_48_Lote_data3 ,
                                             DateTime AV166WWLoteDS_49_Lote_data_to3 ,
                                             String AV167WWLoteDS_50_Lote_usernom3 ,
                                             int AV172WWLoteDS_55_Lote_nfe3 ,
                                             DateTime AV173WWLoteDS_56_Lote_datanfe3 ,
                                             DateTime AV174WWLoteDS_57_Lote_datanfe_to3 ,
                                             short AV175WWLoteDS_58_Lote_qtdedmn3 ,
                                             short AV176WWLoteDS_59_Lote_qtdedmn_to3 ,
                                             decimal AV179WWLoteDS_62_Lote_valorpf3 ,
                                             decimal AV180WWLoteDS_63_Lote_valorpf_to3 ,
                                             int AV182WWLoteDS_65_Tflote_nfe ,
                                             int AV183WWLoteDS_66_Tflote_nfe_to ,
                                             DateTime AV184WWLoteDS_67_Tflote_datanfe ,
                                             DateTime AV185WWLoteDS_68_Tflote_datanfe_to ,
                                             DateTime AV186WWLoteDS_69_Tflote_prevpagamento ,
                                             DateTime AV187WWLoteDS_70_Tflote_prevpagamento_to ,
                                             String A562Lote_Numero ,
                                             String A563Lote_Nome ,
                                             DateTime A564Lote_Data ,
                                             String A561Lote_UserNom ,
                                             int A673Lote_NFe ,
                                             DateTime A674Lote_DataNfe ,
                                             short A569Lote_QtdeDmn ,
                                             decimal A565Lote_ValorPF ,
                                             DateTime A857Lote_PrevPagamento ,
                                             short AV16OrderedBy ,
                                             bool AV17OrderedDsc ,
                                             bool AV9WWPContext_gxTpr_Userehcontratante ,
                                             int A1231Lote_ContratadaCod ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             DateTime AV126WWLoteDS_9_Lote_dataini1 ,
                                             DateTime A567Lote_DataIni ,
                                             DateTime AV127WWLoteDS_10_Lote_dataini_to1 ,
                                             DateTime AV128WWLoteDS_11_Lote_datafim1 ,
                                             DateTime A568Lote_DataFim ,
                                             DateTime AV129WWLoteDS_12_Lote_datafim_to1 ,
                                             decimal AV135WWLoteDS_18_Lote_valor1 ,
                                             decimal A572Lote_Valor ,
                                             decimal AV136WWLoteDS_19_Lote_valor_to1 ,
                                             String AV139WWLoteDS_22_Lote_contratadanom1 ,
                                             String A1748Lote_ContratadaNom ,
                                             DateTime AV147WWLoteDS_30_Lote_dataini2 ,
                                             DateTime AV148WWLoteDS_31_Lote_dataini_to2 ,
                                             DateTime AV149WWLoteDS_32_Lote_datafim2 ,
                                             DateTime AV150WWLoteDS_33_Lote_datafim_to2 ,
                                             decimal AV156WWLoteDS_39_Lote_valor2 ,
                                             decimal AV157WWLoteDS_40_Lote_valor_to2 ,
                                             String AV160WWLoteDS_43_Lote_contratadanom2 ,
                                             DateTime AV168WWLoteDS_51_Lote_dataini3 ,
                                             DateTime AV169WWLoteDS_52_Lote_dataini_to3 ,
                                             DateTime AV170WWLoteDS_53_Lote_datafim3 ,
                                             DateTime AV171WWLoteDS_54_Lote_datafim_to3 ,
                                             decimal AV177WWLoteDS_60_Lote_valor3 ,
                                             decimal AV178WWLoteDS_61_Lote_valor_to3 ,
                                             String AV181WWLoteDS_64_Lote_contratadanom3 ,
                                             int AV79GridState_gxTpr_Dynamicfilters_Count ,
                                             DateTime Gx_date ,
                                             int A595Lote_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [100] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Lote_UserCod] AS Lote_UserCod, T2.[Usuario_PessoaCod] AS Lote_PessoaCod, T1.[Lote_PrevPagamento], T1.[Lote_ValorPF], T1.[Lote_DataNfe], T1.[Lote_NFe], T3.[Pessoa_Nome] AS Lote_UserNom, T1.[Lote_Data], T1.[Lote_Nome], T1.[Lote_Numero], T1.[Lote_AreaTrabalhoCod], T1.[Lote_Codigo], COALESCE( T4.[Lote_ContratadaNom], '') AS Lote_ContratadaNom, COALESCE( T6.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn, COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim, COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T6.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod, COALESCE( T6.[Lote_Status], '') AS Lote_Status, COALESCE( T5.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas FROM (((((([Lote] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Lote_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(T10.[Pessoa_Nome]) AS Lote_ContratadaNom, T8.[ContagemResultado_LoteAceiteCod] FROM (([ContagemResultado] T8 WITH (NOLOCK) LEFT JOIN [Contratada] T9 WITH (NOLOCK) ON T9.[Contratada_Codigo] = T8.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T10 WITH (NOLOCK) ON T10.[Pessoa_Codigo] = T9.[Contratada_PessoaCod]) GROUP BY T8.[ContagemResultado_LoteAceiteCod] ) T4 ON T4.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) INNER JOIN (SELECT COALESCE( T10.[GXC3], 0) + COALESCE( T9.[GXC4], 0) AS Lote_ValorGlosas, T8.[Lote_Codigo] FROM (([Lote] T8 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T11.[ContagemResultadoIndicadores_Valor]) AS GXC4, T12.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T11 WITH (NOLOCK),  [Lote] T12 WITH (NOLOCK) WHERE T11.[ContagemResultadoIndicadores_LoteCod] = T12.[Lote_Codigo] GROUP BY T12.[Lote_Codigo] ) T9 ON";
         scmdbuf = scmdbuf + " T9.[Lote_Codigo] = T8.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC3, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T10 ON T10.[ContagemResultado_LoteAceiteCod] = T8.[Lote_Codigo]) ) T5 ON T5.[Lote_Codigo] = T1.[Lote_Codigo]) LEFT JOIN (SELECT COUNT(*) AS Lote_QtdeDmn, [ContagemResultado_LoteAceiteCod], MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod, MIN([ContagemResultado_StatusDmn]) AS Lote_Status FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) LEFT JOIN (SELECT MAX(COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim, T8.[ContagemResultado_LoteAceiteCod], MIN(COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni FROM ([ContagemResultado] T8 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T9 ON T9.[ContagemResultado_Codigo] = T8.[ContagemResultado_Codigo]) GROUP BY T8.[ContagemResultado_LoteAceiteCod] ) T7 ON T7.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo])";
         scmdbuf = scmdbuf + " WHERE (@AV9WWPCo_2Userehcontratante = 1 or ( COALESCE( T6.[Lote_ContratadaCod], 0) = @AV9WWPCo_1Contratada_codigo))";
         scmdbuf = scmdbuf + " and (Not ( @AV120WWLoteDS_3_Dynamicfiltersselector1 = 'LOTE_DATAINI' and ( Not (@AV126WWLoteDS_9_Lote_dataini1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) >= @AV126WWLoteDS_9_Lote_dataini1))";
         scmdbuf = scmdbuf + " and (Not ( @AV120WWLoteDS_3_Dynamicfiltersselector1 = 'LOTE_DATAINI' and ( Not (@AV127WWLoteDS_10_Lote_dataini_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) <= @AV127WWLoteDS_10_Lote_dataini_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV120WWLoteDS_3_Dynamicfiltersselector1 = 'LOTE_DATAFIM' and ( Not (@AV128WWLoteDS_11_Lote_datafim1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV128WWLoteDS_11_Lote_datafim1))";
         scmdbuf = scmdbuf + " and (Not ( @AV120WWLoteDS_3_Dynamicfiltersselector1 = 'LOTE_DATAFIM' and ( Not (@AV129WWLoteDS_12_Lote_datafim_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) <= @AV129WWLoteDS_12_Lote_datafim_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV120WWLoteDS_3_Dynamicfiltersselector1 = 'LOTE_CONTRATADANOM' and ( Not (@AV139WWLoteDS_22_Lote_contratadanom1 = ''))) or ( COALESCE( T4.[Lote_ContratadaNom], '') like '%' + @lV139WWLoteDS_22_Lote_contratadanom1))";
         scmdbuf = scmdbuf + " and (Not ( @AV140WWLoteDS_23_Dynamicfiltersenabled2 = 1 and @AV141WWLoteDS_24_Dynamicfiltersselector2 = 'LOTE_DATAINI' and ( Not (@AV147WWLoteDS_30_Lote_dataini2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) >= @AV147WWLoteDS_30_Lote_dataini2))";
         scmdbuf = scmdbuf + " and (Not ( @AV140WWLoteDS_23_Dynamicfiltersenabled2 = 1 and @AV141WWLoteDS_24_Dynamicfiltersselector2 = 'LOTE_DATAINI' and ( Not (@AV148WWLoteDS_31_Lote_dataini_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) <= @AV148WWLoteDS_31_Lote_dataini_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV140WWLoteDS_23_Dynamicfiltersenabled2 = 1 and @AV141WWLoteDS_24_Dynamicfiltersselector2 = 'LOTE_DATAFIM' and ( Not (@AV149WWLoteDS_32_Lote_datafim2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV149WWLoteDS_32_Lote_datafim2))";
         scmdbuf = scmdbuf + " and (Not ( @AV140WWLoteDS_23_Dynamicfiltersenabled2 = 1 and @AV141WWLoteDS_24_Dynamicfiltersselector2 = 'LOTE_DATAFIM' and ( Not (@AV150WWLoteDS_33_Lote_datafim_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) <= @AV150WWLoteDS_33_Lote_datafim_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV140WWLoteDS_23_Dynamicfiltersenabled2 = 1 and @AV141WWLoteDS_24_Dynamicfiltersselector2 = 'LOTE_CONTRATADANOM' and ( Not (@AV160WWLoteDS_43_Lote_contratadanom2 = ''))) or ( COALESCE( T4.[Lote_ContratadaNom], '') like '%' + @lV160WWLoteDS_43_Lote_contratadanom2))";
         scmdbuf = scmdbuf + " and (Not ( @AV161WWLoteDS_44_Dynamicfiltersenabled3 = 1 and @AV162WWLoteDS_45_Dynamicfiltersselector3 = 'LOTE_DATAINI' and ( Not (@AV168WWLoteDS_51_Lote_dataini3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) >= @AV168WWLoteDS_51_Lote_dataini3))";
         scmdbuf = scmdbuf + " and (Not ( @AV161WWLoteDS_44_Dynamicfiltersenabled3 = 1 and @AV162WWLoteDS_45_Dynamicfiltersselector3 = 'LOTE_DATAINI' and ( Not (@AV169WWLoteDS_52_Lote_dataini_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) <= @AV169WWLoteDS_52_Lote_dataini_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV161WWLoteDS_44_Dynamicfiltersenabled3 = 1 and @AV162WWLoteDS_45_Dynamicfiltersselector3 = 'LOTE_DATAFIM' and ( Not (@AV170WWLoteDS_53_Lote_datafim3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV170WWLoteDS_53_Lote_datafim3))";
         scmdbuf = scmdbuf + " and (Not ( @AV161WWLoteDS_44_Dynamicfiltersenabled3 = 1 and @AV162WWLoteDS_45_Dynamicfiltersselector3 = 'LOTE_DATAFIM' and ( Not (@AV171WWLoteDS_54_Lote_datafim_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T7.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) <= @AV171WWLoteDS_54_Lote_datafim_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV161WWLoteDS_44_Dynamicfiltersenabled3 = 1 and @AV162WWLoteDS_45_Dynamicfiltersselector3 = 'LOTE_CONTRATADANOM' and ( Not (@AV181WWLoteDS_64_Lote_contratadanom3 = ''))) or ( COALESCE( T4.[Lote_ContratadaNom], '') like '%' + @lV181WWLoteDS_64_Lote_contratadanom3))";
         scmdbuf = scmdbuf + " and (T1.[Lote_AreaTrabalhoCod] = @AV9WWPCo_3Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWLoteDS_4_Lote_numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV121WWLoteDS_4_Lote_numero1 + '%')";
         }
         else
         {
            GXv_int3[58] = 1;
         }
         if ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122WWLoteDS_5_Lote_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV122WWLoteDS_5_Lote_nome1 + '%')";
         }
         else
         {
            GXv_int3[59] = 1;
         }
         if ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_DATA") == 0 ) && ( ! (DateTime.MinValue==AV123WWLoteDS_6_Lote_data1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] >= @AV123WWLoteDS_6_Lote_data1)";
         }
         else
         {
            GXv_int3[60] = 1;
         }
         if ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_DATA") == 0 ) && ( ! (DateTime.MinValue==AV124WWLoteDS_7_Lote_data_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] <= @AV124WWLoteDS_7_Lote_data_to1)";
         }
         else
         {
            GXv_int3[61] = 1;
         }
         if ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_USERNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125WWLoteDS_8_Lote_usernom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV125WWLoteDS_8_Lote_usernom1 + '%')";
         }
         else
         {
            GXv_int3[62] = 1;
         }
         if ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_NFE") == 0 ) && ( ! (0==AV130WWLoteDS_13_Lote_nfe1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV130WWLoteDS_13_Lote_nfe1)";
         }
         else
         {
            GXv_int3[63] = 1;
         }
         if ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV131WWLoteDS_14_Lote_datanfe1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV131WWLoteDS_14_Lote_datanfe1)";
         }
         else
         {
            GXv_int3[64] = 1;
         }
         if ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV132WWLoteDS_15_Lote_datanfe_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV132WWLoteDS_15_Lote_datanfe_to1)";
         }
         else
         {
            GXv_int3[65] = 1;
         }
         if ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_QTDEDMN") == 0 ) && ( ! (0==AV133WWLoteDS_16_Lote_qtdedmn1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T6.[Lote_QtdeDmn], 0) >= @AV133WWLoteDS_16_Lote_qtdedmn1)";
         }
         else
         {
            GXv_int3[66] = 1;
         }
         if ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_QTDEDMN") == 0 ) && ( ! (0==AV134WWLoteDS_17_Lote_qtdedmn_to1) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T6.[Lote_QtdeDmn], 0) <= @AV134WWLoteDS_17_Lote_qtdedmn_to1)";
         }
         else
         {
            GXv_int3[67] = 1;
         }
         if ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_VALORPF") == 0 ) && ( ! (Convert.ToDecimal(0)==AV137WWLoteDS_20_Lote_valorpf1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] >= @AV137WWLoteDS_20_Lote_valorpf1)";
         }
         else
         {
            GXv_int3[68] = 1;
         }
         if ( ( StringUtil.StrCmp(AV120WWLoteDS_3_Dynamicfiltersselector1, "LOTE_VALORPF") == 0 ) && ( ! (Convert.ToDecimal(0)==AV138WWLoteDS_21_Lote_valorpf_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] <= @AV138WWLoteDS_21_Lote_valorpf_to1)";
         }
         else
         {
            GXv_int3[69] = 1;
         }
         if ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV142WWLoteDS_25_Lote_numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV142WWLoteDS_25_Lote_numero2 + '%')";
         }
         else
         {
            GXv_int3[70] = 1;
         }
         if ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143WWLoteDS_26_Lote_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV143WWLoteDS_26_Lote_nome2 + '%')";
         }
         else
         {
            GXv_int3[71] = 1;
         }
         if ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_DATA") == 0 ) && ( ! (DateTime.MinValue==AV144WWLoteDS_27_Lote_data2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] >= @AV144WWLoteDS_27_Lote_data2)";
         }
         else
         {
            GXv_int3[72] = 1;
         }
         if ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_DATA") == 0 ) && ( ! (DateTime.MinValue==AV145WWLoteDS_28_Lote_data_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] <= @AV145WWLoteDS_28_Lote_data_to2)";
         }
         else
         {
            GXv_int3[73] = 1;
         }
         if ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_USERNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV146WWLoteDS_29_Lote_usernom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV146WWLoteDS_29_Lote_usernom2 + '%')";
         }
         else
         {
            GXv_int3[74] = 1;
         }
         if ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_NFE") == 0 ) && ( ! (0==AV151WWLoteDS_34_Lote_nfe2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV151WWLoteDS_34_Lote_nfe2)";
         }
         else
         {
            GXv_int3[75] = 1;
         }
         if ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV152WWLoteDS_35_Lote_datanfe2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV152WWLoteDS_35_Lote_datanfe2)";
         }
         else
         {
            GXv_int3[76] = 1;
         }
         if ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV153WWLoteDS_36_Lote_datanfe_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV153WWLoteDS_36_Lote_datanfe_to2)";
         }
         else
         {
            GXv_int3[77] = 1;
         }
         if ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_QTDEDMN") == 0 ) && ( ! (0==AV154WWLoteDS_37_Lote_qtdedmn2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T6.[Lote_QtdeDmn], 0) >= @AV154WWLoteDS_37_Lote_qtdedmn2)";
         }
         else
         {
            GXv_int3[78] = 1;
         }
         if ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_QTDEDMN") == 0 ) && ( ! (0==AV155WWLoteDS_38_Lote_qtdedmn_to2) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T6.[Lote_QtdeDmn], 0) <= @AV155WWLoteDS_38_Lote_qtdedmn_to2)";
         }
         else
         {
            GXv_int3[79] = 1;
         }
         if ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_VALORPF") == 0 ) && ( ! (Convert.ToDecimal(0)==AV158WWLoteDS_41_Lote_valorpf2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] >= @AV158WWLoteDS_41_Lote_valorpf2)";
         }
         else
         {
            GXv_int3[80] = 1;
         }
         if ( AV140WWLoteDS_23_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV141WWLoteDS_24_Dynamicfiltersselector2, "LOTE_VALORPF") == 0 ) && ( ! (Convert.ToDecimal(0)==AV159WWLoteDS_42_Lote_valorpf_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] <= @AV159WWLoteDS_42_Lote_valorpf_to2)";
         }
         else
         {
            GXv_int3[81] = 1;
         }
         if ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV163WWLoteDS_46_Lote_numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV163WWLoteDS_46_Lote_numero3 + '%')";
         }
         else
         {
            GXv_int3[82] = 1;
         }
         if ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV164WWLoteDS_47_Lote_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV164WWLoteDS_47_Lote_nome3 + '%')";
         }
         else
         {
            GXv_int3[83] = 1;
         }
         if ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_DATA") == 0 ) && ( ! (DateTime.MinValue==AV165WWLoteDS_48_Lote_data3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] >= @AV165WWLoteDS_48_Lote_data3)";
         }
         else
         {
            GXv_int3[84] = 1;
         }
         if ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_DATA") == 0 ) && ( ! (DateTime.MinValue==AV166WWLoteDS_49_Lote_data_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] <= @AV166WWLoteDS_49_Lote_data_to3)";
         }
         else
         {
            GXv_int3[85] = 1;
         }
         if ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_USERNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV167WWLoteDS_50_Lote_usernom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV167WWLoteDS_50_Lote_usernom3 + '%')";
         }
         else
         {
            GXv_int3[86] = 1;
         }
         if ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_NFE") == 0 ) && ( ! (0==AV172WWLoteDS_55_Lote_nfe3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV172WWLoteDS_55_Lote_nfe3)";
         }
         else
         {
            GXv_int3[87] = 1;
         }
         if ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV173WWLoteDS_56_Lote_datanfe3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV173WWLoteDS_56_Lote_datanfe3)";
         }
         else
         {
            GXv_int3[88] = 1;
         }
         if ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV174WWLoteDS_57_Lote_datanfe_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV174WWLoteDS_57_Lote_datanfe_to3)";
         }
         else
         {
            GXv_int3[89] = 1;
         }
         if ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_QTDEDMN") == 0 ) && ( ! (0==AV175WWLoteDS_58_Lote_qtdedmn3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T6.[Lote_QtdeDmn], 0) >= @AV175WWLoteDS_58_Lote_qtdedmn3)";
         }
         else
         {
            GXv_int3[90] = 1;
         }
         if ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_QTDEDMN") == 0 ) && ( ! (0==AV176WWLoteDS_59_Lote_qtdedmn_to3) ) )
         {
            sWhereString = sWhereString + " and (COALESCE( T6.[Lote_QtdeDmn], 0) <= @AV176WWLoteDS_59_Lote_qtdedmn_to3)";
         }
         else
         {
            GXv_int3[91] = 1;
         }
         if ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_VALORPF") == 0 ) && ( ! (Convert.ToDecimal(0)==AV179WWLoteDS_62_Lote_valorpf3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] >= @AV179WWLoteDS_62_Lote_valorpf3)";
         }
         else
         {
            GXv_int3[92] = 1;
         }
         if ( AV161WWLoteDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV162WWLoteDS_45_Dynamicfiltersselector3, "LOTE_VALORPF") == 0 ) && ( ! (Convert.ToDecimal(0)==AV180WWLoteDS_63_Lote_valorpf_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] <= @AV180WWLoteDS_63_Lote_valorpf_to3)";
         }
         else
         {
            GXv_int3[93] = 1;
         }
         if ( ! (0==AV182WWLoteDS_65_Tflote_nfe) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] >= @AV182WWLoteDS_65_Tflote_nfe)";
         }
         else
         {
            GXv_int3[94] = 1;
         }
         if ( ! (0==AV183WWLoteDS_66_Tflote_nfe_to) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] <= @AV183WWLoteDS_66_Tflote_nfe_to)";
         }
         else
         {
            GXv_int3[95] = 1;
         }
         if ( ! (DateTime.MinValue==AV184WWLoteDS_67_Tflote_datanfe) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV184WWLoteDS_67_Tflote_datanfe)";
         }
         else
         {
            GXv_int3[96] = 1;
         }
         if ( ! (DateTime.MinValue==AV185WWLoteDS_68_Tflote_datanfe_to) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV185WWLoteDS_68_Tflote_datanfe_to)";
         }
         else
         {
            GXv_int3[97] = 1;
         }
         if ( ! (DateTime.MinValue==AV186WWLoteDS_69_Tflote_prevpagamento) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV186WWLoteDS_69_Tflote_prevpagamento)";
         }
         else
         {
            GXv_int3[98] = 1;
         }
         if ( ! (DateTime.MinValue==AV187WWLoteDS_70_Tflote_prevpagamento_to) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV187WWLoteDS_70_Tflote_prevpagamento_to)";
         }
         else
         {
            GXv_int3[99] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV16OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Codigo] DESC";
         }
         else if ( ( AV16OrderedBy == 2 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Numero]";
         }
         else if ( ( AV16OrderedBy == 2 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Numero] DESC";
         }
         else if ( ( AV16OrderedBy == 3 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Nome]";
         }
         else if ( ( AV16OrderedBy == 3 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Nome] DESC";
         }
         else if ( ( AV16OrderedBy == 4 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Data]";
         }
         else if ( ( AV16OrderedBy == 4 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Data] DESC";
         }
         else if ( ( AV16OrderedBy == 5 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         }
         else if ( ( AV16OrderedBy == 5 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome] DESC";
         }
         else if ( ( AV16OrderedBy == 6 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_NFe]";
         }
         else if ( ( AV16OrderedBy == 6 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_NFe] DESC";
         }
         else if ( ( AV16OrderedBy == 7 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_DataNfe]";
         }
         else if ( ( AV16OrderedBy == 7 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_DataNfe] DESC";
         }
         else if ( ( AV16OrderedBy == 8 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_PrevPagamento]";
         }
         else if ( ( AV16OrderedBy == 8 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_PrevPagamento] DESC";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00689(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (short)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (bool)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (short)dynConstraints[37] , (short)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (DateTime)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (DateTime)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (String)dynConstraints[50] , (int)dynConstraints[51] , (DateTime)dynConstraints[52] , (short)dynConstraints[53] , (decimal)dynConstraints[54] , (DateTime)dynConstraints[55] , (short)dynConstraints[56] , (bool)dynConstraints[57] , (bool)dynConstraints[58] , (int)dynConstraints[59] , (int)dynConstraints[60] , (DateTime)dynConstraints[61] , (DateTime)dynConstraints[62] , (DateTime)dynConstraints[63] , (DateTime)dynConstraints[64] , (DateTime)dynConstraints[65] , (DateTime)dynConstraints[66] , (decimal)dynConstraints[67] , (decimal)dynConstraints[68] , (decimal)dynConstraints[69] , (String)dynConstraints[70] , (String)dynConstraints[71] , (DateTime)dynConstraints[72] , (DateTime)dynConstraints[73] , (DateTime)dynConstraints[74] , (DateTime)dynConstraints[75] , (decimal)dynConstraints[76] , (decimal)dynConstraints[77] , (String)dynConstraints[78] , (DateTime)dynConstraints[79] , (DateTime)dynConstraints[80] , (DateTime)dynConstraints[81] , (DateTime)dynConstraints[82] , (decimal)dynConstraints[83] , (decimal)dynConstraints[84] , (String)dynConstraints[85] , (int)dynConstraints[86] , (DateTime)dynConstraints[87] , (int)dynConstraints[88] , (int)dynConstraints[89] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006810 ;
          prmP006810 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00689 ;
          prmP00689 = new Object[] {
          new Object[] {"@AV9WWPCo_2Userehcontratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV9WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV120WWLoteDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV126WWLoteDS_9_Lote_dataini1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV126WWLoteDS_9_Lote_dataini1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV120WWLoteDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV127WWLoteDS_10_Lote_dataini_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV127WWLoteDS_10_Lote_dataini_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV120WWLoteDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV128WWLoteDS_11_Lote_datafim1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV128WWLoteDS_11_Lote_datafim1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV120WWLoteDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV129WWLoteDS_12_Lote_datafim_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV129WWLoteDS_12_Lote_datafim_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV120WWLoteDS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV139WWLoteDS_22_Lote_contratadanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV139WWLoteDS_22_Lote_contratadanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV140WWLoteDS_23_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV141WWLoteDS_24_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV147WWLoteDS_30_Lote_dataini2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV147WWLoteDS_30_Lote_dataini2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV140WWLoteDS_23_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV141WWLoteDS_24_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV148WWLoteDS_31_Lote_dataini_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148WWLoteDS_31_Lote_dataini_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV140WWLoteDS_23_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV141WWLoteDS_24_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV149WWLoteDS_32_Lote_datafim2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV149WWLoteDS_32_Lote_datafim2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV140WWLoteDS_23_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV141WWLoteDS_24_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV150WWLoteDS_33_Lote_datafim_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV150WWLoteDS_33_Lote_datafim_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV140WWLoteDS_23_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV141WWLoteDS_24_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV160WWLoteDS_43_Lote_contratadanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV160WWLoteDS_43_Lote_contratadanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV161WWLoteDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV162WWLoteDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV168WWLoteDS_51_Lote_dataini3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV168WWLoteDS_51_Lote_dataini3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV161WWLoteDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV162WWLoteDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV169WWLoteDS_52_Lote_dataini_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV169WWLoteDS_52_Lote_dataini_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV161WWLoteDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV162WWLoteDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV170WWLoteDS_53_Lote_datafim3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170WWLoteDS_53_Lote_datafim3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV161WWLoteDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV162WWLoteDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV171WWLoteDS_54_Lote_datafim_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV171WWLoteDS_54_Lote_datafim_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV161WWLoteDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV162WWLoteDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV181WWLoteDS_64_Lote_contratadanom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV181WWLoteDS_64_Lote_contratadanom3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV9WWPCo_3Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV121WWLoteDS_4_Lote_numero1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV122WWLoteDS_5_Lote_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV123WWLoteDS_6_Lote_data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV124WWLoteDS_7_Lote_data_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV125WWLoteDS_8_Lote_usernom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV130WWLoteDS_13_Lote_nfe1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV131WWLoteDS_14_Lote_datanfe1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV132WWLoteDS_15_Lote_datanfe_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV133WWLoteDS_16_Lote_qtdedmn1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV134WWLoteDS_17_Lote_qtdedmn_to1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV137WWLoteDS_20_Lote_valorpf1",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV138WWLoteDS_21_Lote_valorpf_to1",SqlDbType.Decimal,18,5} ,
          new Object[] {"@lV142WWLoteDS_25_Lote_numero2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV143WWLoteDS_26_Lote_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV144WWLoteDS_27_Lote_data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV145WWLoteDS_28_Lote_data_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV146WWLoteDS_29_Lote_usernom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV151WWLoteDS_34_Lote_nfe2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV152WWLoteDS_35_Lote_datanfe2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV153WWLoteDS_36_Lote_datanfe_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV154WWLoteDS_37_Lote_qtdedmn2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV155WWLoteDS_38_Lote_qtdedmn_to2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV158WWLoteDS_41_Lote_valorpf2",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV159WWLoteDS_42_Lote_valorpf_to2",SqlDbType.Decimal,18,5} ,
          new Object[] {"@lV163WWLoteDS_46_Lote_numero3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV164WWLoteDS_47_Lote_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV165WWLoteDS_48_Lote_data3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV166WWLoteDS_49_Lote_data_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV167WWLoteDS_50_Lote_usernom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV172WWLoteDS_55_Lote_nfe3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV173WWLoteDS_56_Lote_datanfe3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV174WWLoteDS_57_Lote_datanfe_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV175WWLoteDS_58_Lote_qtdedmn3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV176WWLoteDS_59_Lote_qtdedmn_to3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV179WWLoteDS_62_Lote_valorpf3",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV180WWLoteDS_63_Lote_valorpf_to3",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV182WWLoteDS_65_Tflote_nfe",SqlDbType.Int,6,0} ,
          new Object[] {"@AV183WWLoteDS_66_Tflote_nfe_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV184WWLoteDS_67_Tflote_datanfe",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV185WWLoteDS_68_Tflote_datanfe_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV186WWLoteDS_69_Tflote_prevpagamento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV187WWLoteDS_70_Tflote_prevpagamento_to",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00689", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00689,100,0,true,false )
             ,new CursorDef("P006810", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006810,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[12])[0] = rslt.getGXDateTime(8) ;
                ((String[]) buf[13])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[14])[0] = rslt.getString(10, 10) ;
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((int[]) buf[16])[0] = rslt.getInt(12) ;
                ((String[]) buf[17])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((short[]) buf[19])[0] = rslt.getShort(14) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(15) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(16) ;
                ((int[]) buf[22])[0] = rslt.getInt(17) ;
                ((String[]) buf[23])[0] = rslt.getString(18, 1) ;
                ((decimal[]) buf[24])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(19);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[100]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[101]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[103]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[104]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[105]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[106]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[107]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[108]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[109]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[110]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[111]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[112]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[113]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[114]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[115]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[116]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[117]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[118]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[119]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[120]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[121]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[122]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[123]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[124]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[125]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[126]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[127]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[128]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[129]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[131]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[132]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[133]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[134]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[135]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[136]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[137]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[138]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[139]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[141]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[142]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[143]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[144]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[145]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[146]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[147]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[148]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[149]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[150]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[151]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[152]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[153]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[154]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[155]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[156]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[157]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[158]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[159]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[160]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[161]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[162]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[163]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[164]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[165]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[166]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[167]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[168]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[169]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[170]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[171]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[172]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[173]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[174]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[175]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[176]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[177]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[178]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[179]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[180]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[181]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[182]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[183]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[184]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[185]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[186]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[187]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[188]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[189]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[190]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[191]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[192]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[193]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[194]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[195]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[196]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[197]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[198]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[199]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
