/*
               File: WP_ClonarPerfil
        Description: Clonar perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:7:33.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_clonarperfil : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_clonarperfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_clonarperfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ParaCod ,
                           int aP1_Contratada_Codigo ,
                           int aP2_Contratante_Codigo )
      {
         this.AV16ParaCod = aP0_ParaCod;
         this.AV18Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV17Contratante_Codigo = aP2_Contratante_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavDousuario = new GXCombobox();
         chkavClonarperfis = new GXCheckbox();
         chkavClonarservicos = new GXCheckbox();
         chkavClonargestao = new GXCheckbox();
         chkavClonarsistemas = new GXCheckbox();
         chkavClonarfinanceiro = new GXCheckbox();
         chkavClonarcusto = new GXCheckbox();
         chkavApagarperfis = new GXCheckbox();
         chkavApagarservicos = new GXCheckbox();
         chkavApagargestao = new GXCheckbox();
         chkavApagarsistemas = new GXCheckbox();
         chkavApagarfinanceiro = new GXCheckbox();
         chkavApagarcusto = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV16ParaCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ParaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16ParaCod), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV18Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Contratada_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Contratada_Codigo), "ZZZZZ9")));
                  AV17Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratante_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV17Contratante_Codigo), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAQO2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTQO2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311973340");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_clonarperfil.aspx") + "?" + UrlEncode("" +AV16ParaCod) + "," + UrlEncode("" +AV18Contratada_Codigo) + "," + UrlEncode("" +AV17Contratante_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCLONAR", AV19Clonar);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCLONAR", AV19Clonar);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAPAGAR", AV27Apagar);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAPAGAR", AV27Apagar);
         }
         GxWebStd.gx_hidden_field( context, "vPARACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16ParaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "PERFIL_ATIVO", A276Perfil_Ativo);
         GxWebStd.gx_hidden_field( context, "USUARIOSERVICOS_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOAUXILIAR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1825ContratoAuxiliar_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_EHFINANCEIRO", A293Usuario_EhFinanceiro);
         GxWebStd.gx_hidden_field( context, "gxhash_vPARACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16ParaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV17Contratante_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPARACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16ParaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV17Contratante_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEQO2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTQO2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_clonarperfil.aspx") + "?" + UrlEncode("" +AV16ParaCod) + "," + UrlEncode("" +AV18Contratada_Codigo) + "," + UrlEncode("" +AV17Contratante_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_ClonarPerfil" ;
      }

      public override String GetPgmdesc( )
      {
         return "Clonar perfil" ;
      }

      protected void WBQO0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_QO2( true) ;
         }
         else
         {
            wb_table1_2_QO2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_QO2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTQO2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Clonar perfil", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPQO0( ) ;
      }

      protected void WSQO2( )
      {
         STARTQO2( ) ;
         EVTQO2( ) ;
      }

      protected void EVTQO2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11QO2 */
                              E11QO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12QO2 */
                                    E12QO2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDOUSUARIO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13QO2 */
                              E13QO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCLONARFINANCEIRO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14QO2 */
                              E14QO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15QO2 */
                              E15QO2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEQO2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAQO2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavDousuario.Name = "vDOUSUARIO";
            cmbavDousuario.WebTags = "";
            cmbavDousuario.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
            if ( cmbavDousuario.ItemCount > 0 )
            {
               AV8DoUsuario = (short)(NumberUtil.Val( cmbavDousuario.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8DoUsuario), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8DoUsuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8DoUsuario), 4, 0)));
            }
            chkavClonarperfis.Name = "vCLONARPERFIS";
            chkavClonarperfis.WebTags = "";
            chkavClonarperfis.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClonarperfis_Internalname, "TitleCaption", chkavClonarperfis.Caption);
            chkavClonarperfis.CheckedValue = "false";
            chkavClonarservicos.Name = "vCLONARSERVICOS";
            chkavClonarservicos.WebTags = "";
            chkavClonarservicos.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClonarservicos_Internalname, "TitleCaption", chkavClonarservicos.Caption);
            chkavClonarservicos.CheckedValue = "false";
            chkavClonargestao.Name = "vCLONARGESTAO";
            chkavClonargestao.WebTags = "";
            chkavClonargestao.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClonargestao_Internalname, "TitleCaption", chkavClonargestao.Caption);
            chkavClonargestao.CheckedValue = "false";
            chkavClonarsistemas.Name = "vCLONARSISTEMAS";
            chkavClonarsistemas.WebTags = "";
            chkavClonarsistemas.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClonarsistemas_Internalname, "TitleCaption", chkavClonarsistemas.Caption);
            chkavClonarsistemas.CheckedValue = "false";
            chkavClonarfinanceiro.Name = "vCLONARFINANCEIRO";
            chkavClonarfinanceiro.WebTags = "";
            chkavClonarfinanceiro.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClonarfinanceiro_Internalname, "TitleCaption", chkavClonarfinanceiro.Caption);
            chkavClonarfinanceiro.CheckedValue = "false";
            chkavClonarcusto.Name = "vCLONARCUSTO";
            chkavClonarcusto.WebTags = "";
            chkavClonarcusto.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClonarcusto_Internalname, "TitleCaption", chkavClonarcusto.Caption);
            chkavClonarcusto.CheckedValue = "false";
            chkavApagarperfis.Name = "vAPAGARPERFIS";
            chkavApagarperfis.WebTags = "";
            chkavApagarperfis.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarperfis_Internalname, "TitleCaption", chkavApagarperfis.Caption);
            chkavApagarperfis.CheckedValue = "false";
            chkavApagarservicos.Name = "vAPAGARSERVICOS";
            chkavApagarservicos.WebTags = "";
            chkavApagarservicos.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarservicos_Internalname, "TitleCaption", chkavApagarservicos.Caption);
            chkavApagarservicos.CheckedValue = "false";
            chkavApagargestao.Name = "vAPAGARGESTAO";
            chkavApagargestao.WebTags = "";
            chkavApagargestao.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagargestao_Internalname, "TitleCaption", chkavApagargestao.Caption);
            chkavApagargestao.CheckedValue = "false";
            chkavApagarsistemas.Name = "vAPAGARSISTEMAS";
            chkavApagarsistemas.WebTags = "";
            chkavApagarsistemas.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarsistemas_Internalname, "TitleCaption", chkavApagarsistemas.Caption);
            chkavApagarsistemas.CheckedValue = "false";
            chkavApagarfinanceiro.Name = "vAPAGARFINANCEIRO";
            chkavApagarfinanceiro.WebTags = "";
            chkavApagarfinanceiro.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarfinanceiro_Internalname, "TitleCaption", chkavApagarfinanceiro.Caption);
            chkavApagarfinanceiro.CheckedValue = "false";
            chkavApagarcusto.Name = "vAPAGARCUSTO";
            chkavApagarcusto.WebTags = "";
            chkavApagarcusto.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarcusto_Internalname, "TitleCaption", chkavApagarcusto.Caption);
            chkavApagarcusto.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavDousuario_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavDousuario.ItemCount > 0 )
         {
            AV8DoUsuario = (short)(NumberUtil.Val( cmbavDousuario.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8DoUsuario), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8DoUsuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8DoUsuario), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFQO2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFQO2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E15QO2 */
            E15QO2 ();
            WBQO0( ) ;
         }
      }

      protected void STRUPQO0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11QO2 */
         E11QO2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavDousuario.CurrentValue = cgiGet( cmbavDousuario_Internalname);
            AV8DoUsuario = (short)(NumberUtil.Val( cgiGet( cmbavDousuario_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8DoUsuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8DoUsuario), 4, 0)));
            AV9ClonarPerfis = StringUtil.StrToBool( cgiGet( chkavClonarperfis_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ClonarPerfis", AV9ClonarPerfis);
            AV10ClonarServicos = StringUtil.StrToBool( cgiGet( chkavClonarservicos_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ClonarServicos", AV10ClonarServicos);
            AV11ClonarGestao = StringUtil.StrToBool( cgiGet( chkavClonargestao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ClonarGestao", AV11ClonarGestao);
            AV12ClonarSistemas = StringUtil.StrToBool( cgiGet( chkavClonarsistemas_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ClonarSistemas", AV12ClonarSistemas);
            AV13ClonarFinanceiro = StringUtil.StrToBool( cgiGet( chkavClonarfinanceiro_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ClonarFinanceiro", AV13ClonarFinanceiro);
            AV14ClonarCusto = StringUtil.StrToBool( cgiGet( chkavClonarcusto_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ClonarCusto", AV14ClonarCusto);
            AV21ApagarPerfis = StringUtil.StrToBool( cgiGet( chkavApagarperfis_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ApagarPerfis", AV21ApagarPerfis);
            AV22ApagarServicos = StringUtil.StrToBool( cgiGet( chkavApagarservicos_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ApagarServicos", AV22ApagarServicos);
            AV23ApagarGestao = StringUtil.StrToBool( cgiGet( chkavApagargestao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ApagarGestao", AV23ApagarGestao);
            AV24ApagarSistemas = StringUtil.StrToBool( cgiGet( chkavApagarsistemas_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ApagarSistemas", AV24ApagarSistemas);
            AV25ApagarFinanceiro = StringUtil.StrToBool( cgiGet( chkavApagarfinanceiro_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ApagarFinanceiro", AV25ApagarFinanceiro);
            AV26ApagarCusto = StringUtil.StrToBool( cgiGet( chkavApagarcusto_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ApagarCusto", AV26ApagarCusto);
            /* Read saved values. */
            Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
            Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
            Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
            Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
            Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
            Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
            Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
            Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
            Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
            Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11QO2 */
         E11QO2 ();
         if (returnInSub) return;
      }

      protected void E11QO2( )
      {
         /* Start Routine */
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         AV9ClonarPerfis = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ClonarPerfis", AV9ClonarPerfis);
         AV10ClonarServicos = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ClonarServicos", AV10ClonarServicos);
         AV11ClonarGestao = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ClonarGestao", AV11ClonarGestao);
         AV13ClonarFinanceiro = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ClonarFinanceiro", AV13ClonarFinanceiro);
         AV12ClonarSistemas = (bool)(((AV17Contratante_Codigo>0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ClonarSistemas", AV12ClonarSistemas);
         chkavClonarsistemas.Visible = (((AV17Contratante_Codigo>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClonarsistemas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavClonarsistemas.Visible), 5, 0)));
         AV24ApagarSistemas = (bool)(((AV17Contratante_Codigo>0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ApagarSistemas", AV24ApagarSistemas);
         chkavApagarsistemas.Visible = (((AV17Contratante_Codigo>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarsistemas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagarsistemas.Visible), 5, 0)));
         AV14ClonarCusto = (bool)(((AV18Contratada_Codigo>0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ClonarCusto", AV14ClonarCusto);
         chkavClonarcusto.Visible = (((AV18Contratada_Codigo>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClonarcusto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavClonarcusto.Visible), 5, 0)));
         AV26ApagarCusto = (bool)(((AV18Contratada_Codigo>0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ApagarCusto", AV26ApagarCusto);
         chkavApagarcusto.Visible = (((AV18Contratada_Codigo>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarcusto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagarcusto.Visible), 5, 0)));
         chkavApagarfinanceiro.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarfinanceiro_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagarfinanceiro.Visible), 5, 0)));
         lblTextblockclonarcusto_Visible = (((AV18Contratada_Codigo>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockclonarcusto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockclonarcusto_Visible), 5, 0)));
         lblTextblockapagarcusto_Visible = (((AV18Contratada_Codigo>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagarcusto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockapagarcusto_Visible), 5, 0)));
         lblTextblockclonarsistemas_Visible = (((AV17Contratante_Codigo>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockclonarsistemas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockclonarsistemas_Visible), 5, 0)));
         lblTextblockapagarsistemas_Visible = (((AV17Contratante_Codigo>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagarsistemas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockapagarsistemas_Visible), 5, 0)));
         lblTextblockapagarfinanceiro_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagarfinanceiro_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockapagarfinanceiro_Visible), 5, 0)));
         /* Execute user subroutine: 'DADOSDOPARA' */
         S112 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'CARREGADE' */
         S122 ();
         if (returnInSub) return;
      }

      public void GXEnter( )
      {
         /* Execute user event: E12QO2 */
         E12QO2 ();
         if (returnInSub) return;
      }

      protected void E12QO2( )
      {
         /* Enter Routine */
         AV19Clonar.Add(AV9ClonarPerfis, 0);
         AV19Clonar.Add(AV10ClonarServicos, 0);
         AV19Clonar.Add(AV11ClonarGestao, 0);
         AV19Clonar.Add(AV12ClonarSistemas, 0);
         AV19Clonar.Add(AV13ClonarFinanceiro, 0);
         AV19Clonar.Add(AV14ClonarCusto, 0);
         AV27Apagar.Add(AV21ApagarPerfis, 0);
         AV27Apagar.Add(AV22ApagarServicos, 0);
         AV27Apagar.Add(AV23ApagarGestao, 0);
         AV27Apagar.Add(AV24ApagarSistemas, 0);
         AV27Apagar.Add(AV25ApagarFinanceiro, 0);
         AV27Apagar.Add(AV26ApagarCusto, 0);
         new prc_clonarperfil(context ).execute(  AV16ParaCod,  AV8DoUsuario,  AV18Contratada_Codigo,  AV17Contratante_Codigo,  AV19Clonar,  AV27Apagar) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ParaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16ParaCod), "ZZZZZ9")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8DoUsuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8DoUsuario), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Contratada_Codigo), "ZZZZZ9")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratante_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV17Contratante_Codigo), "ZZZZZ9")));
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19Clonar", AV19Clonar);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV27Apagar", AV27Apagar);
      }

      protected void E13QO2( )
      {
         /* Dousuario_Click Routine */
         AV20Count = 0;
         /* Optimized group. */
         /* Using cursor H00QO2 */
         pr_default.execute(0, new Object[] {AV8DoUsuario});
         cV20Count = H00QO2_AV20Count[0];
         pr_default.close(0);
         AV20Count = (short)(AV20Count+cV20Count*1);
         /* End optimized group. */
         lblTextblockclonarperfis_Caption = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+" Perfis";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockclonarperfis_Internalname, "Caption", lblTextblockclonarperfis_Caption);
         AV20Count = 0;
         /* Optimized group. */
         /* Using cursor H00QO3 */
         pr_default.execute(1, new Object[] {AV8DoUsuario});
         cV20Count = H00QO3_AV20Count[0];
         pr_default.close(1);
         AV20Count = (short)(AV20Count+cV20Count*1);
         /* End optimized group. */
         lblTextblockclonarservicos_Caption = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+" Servi�os";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockclonarservicos_Internalname, "Caption", lblTextblockclonarservicos_Caption);
         AV20Count = 0;
         /* Optimized group. */
         /* Using cursor H00QO4 */
         pr_default.execute(2, new Object[] {AV8DoUsuario});
         cV20Count = H00QO4_AV20Count[0];
         pr_default.close(2);
         AV20Count = (short)(AV20Count+cV20Count*1);
         /* End optimized group. */
         /* Optimized group. */
         /* Using cursor H00QO5 */
         pr_default.execute(3, new Object[] {AV8DoUsuario});
         cV20Count = H00QO5_AV20Count[0];
         pr_default.close(3);
         AV20Count = (short)(AV20Count+cV20Count*1);
         /* End optimized group. */
         lblTextblockclonargestao_Caption = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+" Gest�o";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockclonargestao_Internalname, "Caption", lblTextblockclonargestao_Caption);
         if ( AV17Contratante_Codigo > 0 )
         {
            AV20Count = 0;
            /* Optimized group. */
            /* Using cursor H00QO6 */
            pr_default.execute(4, new Object[] {AV8DoUsuario});
            cV20Count = H00QO6_AV20Count[0];
            pr_default.close(4);
            AV20Count = (short)(AV20Count+cV20Count*1);
            /* End optimized group. */
            lblTextblockclonarsistemas_Caption = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+" Sistemas";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockclonarsistemas_Internalname, "Caption", lblTextblockclonarsistemas_Caption);
         }
         /* Execute user subroutine: 'APAGAFINANCEIRO' */
         S132 ();
         if (returnInSub) return;
      }

      protected void E14QO2( )
      {
         /* Clonarfinanceiro_Click Routine */
         /* Execute user subroutine: 'APAGAFINANCEIRO' */
         S132 ();
         if (returnInSub) return;
      }

      protected void S122( )
      {
         /* 'CARREGADE' Routine */
         if ( AV18Contratada_Codigo > 0 )
         {
            /* Using cursor H00QO7 */
            pr_default.execute(5, new Object[] {AV16ParaCod, AV18Contratada_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A70ContratadaUsuario_UsuarioPessoaCod = H00QO7_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00QO7_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A66ContratadaUsuario_ContratadaCod = H00QO7_A66ContratadaUsuario_ContratadaCod[0];
               A1394ContratadaUsuario_UsuarioAtivo = H00QO7_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H00QO7_n1394ContratadaUsuario_UsuarioAtivo[0];
               A69ContratadaUsuario_UsuarioCod = H00QO7_A69ContratadaUsuario_UsuarioCod[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00QO7_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00QO7_n71ContratadaUsuario_UsuarioPessoaNom[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00QO7_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00QO7_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A1394ContratadaUsuario_UsuarioAtivo = H00QO7_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H00QO7_n1394ContratadaUsuario_UsuarioAtivo[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00QO7_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00QO7_n71ContratadaUsuario_UsuarioPessoaNom[0];
               cmbavDousuario.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 4, 0)), A71ContratadaUsuario_UsuarioPessoaNom, 0);
               pr_default.readNext(5);
            }
            pr_default.close(5);
         }
         else if ( AV17Contratante_Codigo > 0 )
         {
            /* Using cursor H00QO8 */
            pr_default.execute(6, new Object[] {AV16ParaCod, AV17Contratante_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A61ContratanteUsuario_UsuarioPessoaCod = H00QO8_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00QO8_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A63ContratanteUsuario_ContratanteCod = H00QO8_A63ContratanteUsuario_ContratanteCod[0];
               A54Usuario_Ativo = H00QO8_A54Usuario_Ativo[0];
               n54Usuario_Ativo = H00QO8_n54Usuario_Ativo[0];
               A60ContratanteUsuario_UsuarioCod = H00QO8_A60ContratanteUsuario_UsuarioCod[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H00QO8_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H00QO8_n62ContratanteUsuario_UsuarioPessoaNom[0];
               A61ContratanteUsuario_UsuarioPessoaCod = H00QO8_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00QO8_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A54Usuario_Ativo = H00QO8_A54Usuario_Ativo[0];
               n54Usuario_Ativo = H00QO8_n54Usuario_Ativo[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H00QO8_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H00QO8_n62ContratanteUsuario_UsuarioPessoaNom[0];
               cmbavDousuario.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 4, 0)), A62ContratanteUsuario_UsuarioPessoaNom, 0);
               pr_default.readNext(6);
            }
            pr_default.close(6);
         }
      }

      protected void S112( )
      {
         /* 'DADOSDOPARA' Routine */
         /* Using cursor H00QO9 */
         pr_default.execute(7, new Object[] {AV16ParaCod});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A57Usuario_PessoaCod = H00QO9_A57Usuario_PessoaCod[0];
            A1Usuario_Codigo = H00QO9_A1Usuario_Codigo[0];
            A58Usuario_PessoaNom = H00QO9_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00QO9_n58Usuario_PessoaNom[0];
            A58Usuario_PessoaNom = H00QO9_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00QO9_n58Usuario_PessoaNom[0];
            Dvpanel_tableattributes_Title = "Para: "+StringUtil.Trim( A58Usuario_PessoaNom);
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(7);
         AV20Count = 0;
         /* Optimized group. */
         /* Using cursor H00QO10 */
         pr_default.execute(8, new Object[] {AV16ParaCod});
         cV20Count = H00QO10_AV20Count[0];
         pr_default.close(8);
         AV20Count = (short)(AV20Count+cV20Count*1);
         /* End optimized group. */
         if ( AV20Count > 0 )
         {
            AV21ApagarPerfis = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ApagarPerfis", AV21ApagarPerfis);
            chkavApagarperfis.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarperfis_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagarperfis.Visible), 5, 0)));
            lblTextblockapagarperfis_Caption = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+" Perfis";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagarperfis_Internalname, "Caption", lblTextblockapagarperfis_Caption);
         }
         else
         {
            AV21ApagarPerfis = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ApagarPerfis", AV21ApagarPerfis);
            chkavApagarperfis.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarperfis_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagarperfis.Visible), 5, 0)));
            lblTextblockapagarperfis_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagarperfis_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockapagarperfis_Visible), 5, 0)));
         }
         AV20Count = 0;
         /* Optimized group. */
         /* Using cursor H00QO11 */
         pr_default.execute(9, new Object[] {AV16ParaCod});
         cV20Count = H00QO11_AV20Count[0];
         pr_default.close(9);
         AV20Count = (short)(AV20Count+cV20Count*1);
         /* End optimized group. */
         if ( AV20Count > 0 )
         {
            AV22ApagarServicos = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ApagarServicos", AV22ApagarServicos);
            chkavApagarservicos.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarservicos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagarservicos.Visible), 5, 0)));
            lblTextblockapagarservicos_Caption = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+" Servi�os";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagarservicos_Internalname, "Caption", lblTextblockapagarservicos_Caption);
         }
         else
         {
            AV22ApagarServicos = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ApagarServicos", AV22ApagarServicos);
            chkavApagarservicos.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarservicos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagarservicos.Visible), 5, 0)));
            lblTextblockapagarservicos_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagarservicos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockapagarservicos_Visible), 5, 0)));
         }
         AV20Count = 0;
         /* Optimized group. */
         /* Using cursor H00QO12 */
         pr_default.execute(10, new Object[] {AV16ParaCod});
         cV20Count = H00QO12_AV20Count[0];
         pr_default.close(10);
         AV20Count = (short)(AV20Count+cV20Count*1);
         /* End optimized group. */
         /* Optimized group. */
         /* Using cursor H00QO13 */
         pr_default.execute(11, new Object[] {AV16ParaCod});
         cV20Count = H00QO13_AV20Count[0];
         pr_default.close(11);
         AV20Count = (short)(AV20Count+cV20Count*1);
         /* End optimized group. */
         if ( AV20Count > 0 )
         {
            AV23ApagarGestao = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ApagarGestao", AV23ApagarGestao);
            chkavApagargestao.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagargestao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagargestao.Visible), 5, 0)));
            lblTextblockapagargestao_Caption = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+" Gest�o";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagargestao_Internalname, "Caption", lblTextblockapagargestao_Caption);
         }
         else
         {
            AV23ApagarGestao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ApagarGestao", AV23ApagarGestao);
            chkavApagargestao.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagargestao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagargestao.Visible), 5, 0)));
            lblTextblockapagargestao_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagargestao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockapagargestao_Visible), 5, 0)));
         }
         if ( AV17Contratante_Codigo > 0 )
         {
            AV20Count = 0;
            /* Optimized group. */
            /* Using cursor H00QO14 */
            pr_default.execute(12, new Object[] {AV16ParaCod});
            cV20Count = H00QO14_AV20Count[0];
            pr_default.close(12);
            AV20Count = (short)(AV20Count+cV20Count*1);
            /* End optimized group. */
            if ( AV20Count > 0 )
            {
               AV24ApagarSistemas = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ApagarSistemas", AV24ApagarSistemas);
               chkavApagarsistemas.Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarsistemas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagarsistemas.Visible), 5, 0)));
               lblTextblockapagarsistemas_Caption = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+" Sistemas";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagarsistemas_Internalname, "Caption", lblTextblockapagarsistemas_Caption);
            }
            else
            {
               AV24ApagarSistemas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ApagarSistemas", AV24ApagarSistemas);
               chkavApagarsistemas.Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarsistemas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagarsistemas.Visible), 5, 0)));
               lblTextblockapagarsistemas_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagarsistemas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockapagarsistemas_Visible), 5, 0)));
            }
         }
         if ( AV18Contratada_Codigo > 0 )
         {
            /* Using cursor H00QO15 */
            pr_default.execute(13, new Object[] {AV16ParaCod});
            while ( (pr_default.getStatus(13) != 101) )
            {
               A69ContratadaUsuario_UsuarioCod = H00QO15_A69ContratadaUsuario_UsuarioCod[0];
               A577ContratadaUsuario_CstUntPrdExt = H00QO15_A577ContratadaUsuario_CstUntPrdExt[0];
               n577ContratadaUsuario_CstUntPrdExt = H00QO15_n577ContratadaUsuario_CstUntPrdExt[0];
               A576ContratadaUsuario_CstUntPrdNrm = H00QO15_A576ContratadaUsuario_CstUntPrdNrm[0];
               n576ContratadaUsuario_CstUntPrdNrm = H00QO15_n576ContratadaUsuario_CstUntPrdNrm[0];
               AV26ApagarCusto = (bool)(((A576ContratadaUsuario_CstUntPrdNrm>Convert.ToDecimal(0))||(A577ContratadaUsuario_CstUntPrdExt>Convert.ToDecimal(0))));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ApagarCusto", AV26ApagarCusto);
               chkavApagarcusto.Visible = (AV26ApagarCusto ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarcusto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagarcusto.Visible), 5, 0)));
               lblTextblockapagarcusto_Visible = (AV26ApagarCusto ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagarcusto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockapagarcusto_Visible), 5, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(13);
            }
            pr_default.close(13);
         }
      }

      protected void S132( )
      {
         /* 'APAGAFINANCEIRO' Routine */
         if ( AV13ClonarFinanceiro )
         {
            /* Using cursor H00QO16 */
            pr_default.execute(14, new Object[] {AV8DoUsuario});
            while ( (pr_default.getStatus(14) != 101) )
            {
               A1Usuario_Codigo = H00QO16_A1Usuario_Codigo[0];
               A293Usuario_EhFinanceiro = H00QO16_A293Usuario_EhFinanceiro[0];
               AV28EhFinanceiro = A293Usuario_EhFinanceiro;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(14);
            /* Using cursor H00QO17 */
            pr_default.execute(15, new Object[] {AV16ParaCod});
            while ( (pr_default.getStatus(15) != 101) )
            {
               A1Usuario_Codigo = H00QO17_A1Usuario_Codigo[0];
               A293Usuario_EhFinanceiro = H00QO17_A293Usuario_EhFinanceiro[0];
               AV29ParaEhFinanceiro = A293Usuario_EhFinanceiro;
               AV25ApagarFinanceiro = (bool)(A293Usuario_EhFinanceiro&&!AV28EhFinanceiro);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ApagarFinanceiro", AV25ApagarFinanceiro);
               chkavApagarfinanceiro.Visible = (AV25ApagarFinanceiro ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarfinanceiro_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagarfinanceiro.Visible), 5, 0)));
               lblTextblockapagarfinanceiro_Visible = (AV25ApagarFinanceiro ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagarfinanceiro_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockapagarfinanceiro_Visible), 5, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(15);
         }
         else
         {
            AV25ApagarFinanceiro = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ApagarFinanceiro", AV25ApagarFinanceiro);
            chkavApagarfinanceiro.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavApagarfinanceiro_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavApagarfinanceiro.Visible), 5, 0)));
            lblTextblockapagarfinanceiro_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockapagarfinanceiro_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockapagarfinanceiro_Visible), 5, 0)));
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E15QO2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_QO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_QO2( true) ;
         }
         else
         {
            wb_table2_5_QO2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_QO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table3_91_QO2( true) ;
         }
         else
         {
            wb_table3_91_QO2( false) ;
         }
         return  ;
      }

      protected void wb_table3_91_QO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_QO2e( true) ;
         }
         else
         {
            wb_table1_2_QO2e( false) ;
         }
      }

      protected void wb_table3_91_QO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Clonar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_91_QO2e( true) ;
         }
         else
         {
            wb_table3_91_QO2e( false) ;
         }
      }

      protected void wb_table2_5_QO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_QO2( true) ;
         }
         else
         {
            wb_table4_13_QO2( false) ;
         }
         return  ;
      }

      protected void wb_table4_13_QO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_QO2e( true) ;
         }
         else
         {
            wb_table2_5_QO2e( false) ;
         }
      }

      protected void wb_table4_13_QO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table5_16_QO2( true) ;
         }
         else
         {
            wb_table5_16_QO2( false) ;
         }
         return  ;
      }

      protected void wb_table5_16_QO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup3_Internalname, "Clonar:", 1, 0, "px", 0, "px", "Group", "", "HLP_WP_ClonarPerfil.htm");
            wb_table6_25_QO2( true) ;
         }
         else
         {
            wb_table6_25_QO2( false) ;
         }
         return  ;
      }

      protected void wb_table6_25_QO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup5_Internalname, "Apagando:", 1, 0, "px", 0, "px", "Group", "", "HLP_WP_ClonarPerfil.htm");
            wb_table7_58_QO2( true) ;
         }
         else
         {
            wb_table7_58_QO2( false) ;
         }
         return  ;
      }

      protected void wb_table7_58_QO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_QO2e( true) ;
         }
         else
         {
            wb_table4_13_QO2e( false) ;
         }
      }

      protected void wb_table7_58_QO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable4_Internalname, tblUnnamedtable4_Internalname, "", "TableWrap", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockapagarperfis_Internalname, lblTextblockapagarperfis_Caption, "", "", lblTextblockapagarperfis_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockapagarperfis_Visible, 1, 0, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavApagarperfis_Internalname, StringUtil.BoolToStr( AV21ApagarPerfis), "", "", chkavApagarperfis.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(63, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockapagarservicos_Internalname, lblTextblockapagarservicos_Caption, "", "", lblTextblockapagarservicos_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockapagarservicos_Visible, 1, 0, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavApagarservicos_Internalname, StringUtil.BoolToStr( AV22ApagarServicos), "", "", chkavApagarservicos.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(68, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockapagargestao_Internalname, lblTextblockapagargestao_Caption, "", "", lblTextblockapagargestao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockapagargestao_Visible, 1, 0, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavApagargestao_Internalname, StringUtil.BoolToStr( AV23ApagarGestao), "", "", chkavApagargestao.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(73, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockapagarsistemas_Internalname, lblTextblockapagarsistemas_Caption, "", "", lblTextblockapagarsistemas_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockapagarsistemas_Visible, 1, 0, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavApagarsistemas_Internalname, StringUtil.BoolToStr( AV24ApagarSistemas), "", "", chkavApagarsistemas.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(78, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockapagarfinanceiro_Internalname, "Financeiro", "", "", lblTextblockapagarfinanceiro_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockapagarfinanceiro_Visible, 1, 0, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavApagarfinanceiro_Internalname, StringUtil.BoolToStr( AV25ApagarFinanceiro), "", "", chkavApagarfinanceiro.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(83, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockapagarcusto_Internalname, "Custo atual", "", "", lblTextblockapagarcusto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockapagarcusto_Visible, 1, 0, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavApagarcusto_Internalname, StringUtil.BoolToStr( AV26ApagarCusto), "", "", chkavApagarcusto.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(88, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_58_QO2e( true) ;
         }
         else
         {
            wb_table7_58_QO2e( false) ;
         }
      }

      protected void wb_table6_25_QO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "TableWrap", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockclonarperfis_Internalname, lblTextblockclonarperfis_Caption, "", "", lblTextblockclonarperfis_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClonarperfis_Internalname, StringUtil.BoolToStr( AV9ClonarPerfis), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(30, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockclonarservicos_Internalname, lblTextblockclonarservicos_Caption, "", "", lblTextblockclonarservicos_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClonarservicos_Internalname, StringUtil.BoolToStr( AV10ClonarServicos), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(35, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockclonargestao_Internalname, lblTextblockclonargestao_Caption, "", "", lblTextblockclonargestao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClonargestao_Internalname, StringUtil.BoolToStr( AV11ClonarGestao), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(40, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockclonarsistemas_Internalname, lblTextblockclonarsistemas_Caption, "", "", lblTextblockclonarsistemas_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockclonarsistemas_Visible, 1, 0, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClonarsistemas_Internalname, StringUtil.BoolToStr( AV12ClonarSistemas), "", "", chkavClonarsistemas.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(45, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockclonarfinanceiro_Internalname, "Financeiro", "", "", lblTextblockclonarfinanceiro_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClonarfinanceiro_Internalname, StringUtil.BoolToStr( AV13ClonarFinanceiro), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(50, this, 'true', 'false');gx.ajax.executeCliEvent('e14qo2_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockclonarcusto_Internalname, "Custo Op.", "", "", lblTextblockclonarcusto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockclonarcusto_Visible, 1, 0, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClonarcusto_Internalname, StringUtil.BoolToStr( AV14ClonarCusto), "", "", chkavClonarcusto.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(55, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_25_QO2e( true) ;
         }
         else
         {
            wb_table6_25_QO2e( false) ;
         }
      }

      protected void wb_table5_16_QO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockdousuario_Internalname, "Do usu�rio", "", "", lblTextblockdousuario_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_ClonarPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDousuario, cmbavDousuario_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV8DoUsuario), 4, 0)), 1, cmbavDousuario_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDOUSUARIO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "", true, "HLP_WP_ClonarPerfil.htm");
            cmbavDousuario.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8DoUsuario), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDousuario_Internalname, "Values", (String)(cmbavDousuario.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_16_QO2e( true) ;
         }
         else
         {
            wb_table5_16_QO2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV16ParaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ParaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ParaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16ParaCod), "ZZZZZ9")));
         AV18Contratada_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Contratada_Codigo), "ZZZZZ9")));
         AV17Contratante_Codigo = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contratante_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV17Contratante_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAQO2( ) ;
         WSQO2( ) ;
         WEQO2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311973454");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_clonarperfil.js", "?2020311973455");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockdousuario_Internalname = "TEXTBLOCKDOUSUARIO";
         cmbavDousuario_Internalname = "vDOUSUARIO";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         lblTextblockclonarperfis_Internalname = "TEXTBLOCKCLONARPERFIS";
         chkavClonarperfis_Internalname = "vCLONARPERFIS";
         lblTextblockclonarservicos_Internalname = "TEXTBLOCKCLONARSERVICOS";
         chkavClonarservicos_Internalname = "vCLONARSERVICOS";
         lblTextblockclonargestao_Internalname = "TEXTBLOCKCLONARGESTAO";
         chkavClonargestao_Internalname = "vCLONARGESTAO";
         lblTextblockclonarsistemas_Internalname = "TEXTBLOCKCLONARSISTEMAS";
         chkavClonarsistemas_Internalname = "vCLONARSISTEMAS";
         lblTextblockclonarfinanceiro_Internalname = "TEXTBLOCKCLONARFINANCEIRO";
         chkavClonarfinanceiro_Internalname = "vCLONARFINANCEIRO";
         lblTextblockclonarcusto_Internalname = "TEXTBLOCKCLONARCUSTO";
         chkavClonarcusto_Internalname = "vCLONARCUSTO";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         grpUnnamedgroup3_Internalname = "UNNAMEDGROUP3";
         lblTextblockapagarperfis_Internalname = "TEXTBLOCKAPAGARPERFIS";
         chkavApagarperfis_Internalname = "vAPAGARPERFIS";
         lblTextblockapagarservicos_Internalname = "TEXTBLOCKAPAGARSERVICOS";
         chkavApagarservicos_Internalname = "vAPAGARSERVICOS";
         lblTextblockapagargestao_Internalname = "TEXTBLOCKAPAGARGESTAO";
         chkavApagargestao_Internalname = "vAPAGARGESTAO";
         lblTextblockapagarsistemas_Internalname = "TEXTBLOCKAPAGARSISTEMAS";
         chkavApagarsistemas_Internalname = "vAPAGARSISTEMAS";
         lblTextblockapagarfinanceiro_Internalname = "TEXTBLOCKAPAGARFINANCEIRO";
         chkavApagarfinanceiro_Internalname = "vAPAGARFINANCEIRO";
         lblTextblockapagarcusto_Internalname = "TEXTBLOCKAPAGARCUSTO";
         chkavApagarcusto_Internalname = "vAPAGARCUSTO";
         tblUnnamedtable4_Internalname = "UNNAMEDTABLE4";
         grpUnnamedgroup5_Internalname = "UNNAMEDGROUP5";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblTbjava_Internalname = "TBJAVA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavDousuario_Jsonclick = "";
         lblTextblockclonarcusto_Visible = 1;
         lblTextblockclonarsistemas_Visible = 1;
         lblTextblockapagarcusto_Visible = 1;
         lblTextblockapagarfinanceiro_Visible = 1;
         lblTextblockapagarsistemas_Visible = 1;
         lblTextblockapagargestao_Visible = 1;
         lblTextblockapagarservicos_Visible = 1;
         lblTextblockapagarperfis_Visible = 1;
         lblTbjava_Visible = 1;
         lblTextblockapagarsistemas_Caption = "Sistemas";
         lblTextblockapagargestao_Caption = "Gest�o";
         chkavApagargestao.Visible = 1;
         lblTextblockapagarservicos_Caption = "Servi�os";
         chkavApagarservicos.Visible = 1;
         lblTextblockapagarperfis_Caption = "Perfis";
         chkavApagarperfis.Visible = 1;
         lblTextblockclonarsistemas_Caption = "Sistemas";
         lblTextblockclonargestao_Caption = "Gest�o";
         lblTextblockclonarservicos_Caption = "Servi�os";
         lblTextblockclonarperfis_Caption = "Perfis";
         chkavApagarfinanceiro.Visible = 1;
         chkavApagarcusto.Visible = 1;
         chkavClonarcusto.Visible = 1;
         chkavApagarsistemas.Visible = 1;
         chkavClonarsistemas.Visible = 1;
         lblTbjava_Caption = "Script";
         chkavApagarcusto.Caption = "";
         chkavApagarfinanceiro.Caption = "";
         chkavApagarsistemas.Caption = "";
         chkavApagargestao.Caption = "";
         chkavApagarservicos.Caption = "";
         chkavApagarperfis.Caption = "";
         chkavClonarcusto.Caption = "";
         chkavClonarfinanceiro.Caption = "";
         chkavClonarsistemas.Caption = "";
         chkavClonargestao.Caption = "";
         chkavClonarservicos.Caption = "";
         chkavClonarperfis.Caption = "";
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Para:";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Clonar perfil";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12QO2',iparms:[{av:'AV9ClonarPerfis',fld:'vCLONARPERFIS',pic:'',nv:false},{av:'AV19Clonar',fld:'vCLONAR',pic:'',nv:null},{av:'AV10ClonarServicos',fld:'vCLONARSERVICOS',pic:'',nv:false},{av:'AV11ClonarGestao',fld:'vCLONARGESTAO',pic:'',nv:false},{av:'AV12ClonarSistemas',fld:'vCLONARSISTEMAS',pic:'',nv:false},{av:'AV13ClonarFinanceiro',fld:'vCLONARFINANCEIRO',pic:'',nv:false},{av:'AV14ClonarCusto',fld:'vCLONARCUSTO',pic:'',nv:false},{av:'AV21ApagarPerfis',fld:'vAPAGARPERFIS',pic:'',nv:false},{av:'AV27Apagar',fld:'vAPAGAR',pic:'',nv:null},{av:'AV22ApagarServicos',fld:'vAPAGARSERVICOS',pic:'',nv:false},{av:'AV23ApagarGestao',fld:'vAPAGARGESTAO',pic:'',nv:false},{av:'AV24ApagarSistemas',fld:'vAPAGARSISTEMAS',pic:'',nv:false},{av:'AV25ApagarFinanceiro',fld:'vAPAGARFINANCEIRO',pic:'',nv:false},{av:'AV26ApagarCusto',fld:'vAPAGARCUSTO',pic:'',nv:false},{av:'AV16ParaCod',fld:'vPARACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8DoUsuario',fld:'vDOUSUARIO',pic:'ZZZ9',nv:0},{av:'AV18Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV19Clonar',fld:'vCLONAR',pic:'',nv:null},{av:'AV27Apagar',fld:'vAPAGAR',pic:'',nv:null}]}");
         setEventMetadata("VDOUSUARIO.CLICK","{handler:'E13QO2',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8DoUsuario',fld:'vDOUSUARIO',pic:'ZZZ9',nv:0},{av:'A276Perfil_Ativo',fld:'PERFIL_ATIVO',pic:'',nv:false},{av:'A828UsuarioServicos_UsuarioCod',fld:'USUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV13ClonarFinanceiro',fld:'vCLONARFINANCEIRO',pic:'',nv:false},{av:'A293Usuario_EhFinanceiro',fld:'USUARIO_EHFINANCEIRO',pic:'',nv:false},{av:'AV16ParaCod',fld:'vPARACOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'lblTextblockclonarperfis_Caption',ctrl:'TEXTBLOCKCLONARPERFIS',prop:'Caption'},{av:'lblTextblockclonarservicos_Caption',ctrl:'TEXTBLOCKCLONARSERVICOS',prop:'Caption'},{av:'lblTextblockclonargestao_Caption',ctrl:'TEXTBLOCKCLONARGESTAO',prop:'Caption'},{av:'lblTextblockclonarsistemas_Caption',ctrl:'TEXTBLOCKCLONARSISTEMAS',prop:'Caption'},{av:'AV25ApagarFinanceiro',fld:'vAPAGARFINANCEIRO',pic:'',nv:false},{av:'chkavApagarfinanceiro.Visible',ctrl:'vAPAGARFINANCEIRO',prop:'Visible'},{av:'lblTextblockapagarfinanceiro_Visible',ctrl:'TEXTBLOCKAPAGARFINANCEIRO',prop:'Visible'}]}");
         setEventMetadata("VCLONARFINANCEIRO.CLICK","{handler:'E14QO2',iparms:[{av:'AV13ClonarFinanceiro',fld:'vCLONARFINANCEIRO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8DoUsuario',fld:'vDOUSUARIO',pic:'ZZZ9',nv:0},{av:'A293Usuario_EhFinanceiro',fld:'USUARIO_EHFINANCEIRO',pic:'',nv:false},{av:'AV16ParaCod',fld:'vPARACOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV25ApagarFinanceiro',fld:'vAPAGARFINANCEIRO',pic:'',nv:false},{av:'chkavApagarfinanceiro.Visible',ctrl:'vAPAGARFINANCEIRO',prop:'Visible'},{av:'lblTextblockapagarfinanceiro_Visible',ctrl:'TEXTBLOCKAPAGARFINANCEIRO',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV19Clonar = new GxSimpleCollection();
         AV27Apagar = new GxSimpleCollection();
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00QO2_AV20Count = new short[1] ;
         H00QO3_AV20Count = new short[1] ;
         H00QO4_AV20Count = new short[1] ;
         H00QO5_AV20Count = new short[1] ;
         H00QO6_AV20Count = new short[1] ;
         H00QO7_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00QO7_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00QO7_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00QO7_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00QO7_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00QO7_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00QO7_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00QO7_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         H00QO8_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H00QO8_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00QO8_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00QO8_A54Usuario_Ativo = new bool[] {false} ;
         H00QO8_n54Usuario_Ativo = new bool[] {false} ;
         H00QO8_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00QO8_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00QO8_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         H00QO9_A57Usuario_PessoaCod = new int[1] ;
         H00QO9_A1Usuario_Codigo = new int[1] ;
         H00QO9_A58Usuario_PessoaNom = new String[] {""} ;
         H00QO9_n58Usuario_PessoaNom = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         H00QO10_AV20Count = new short[1] ;
         H00QO11_AV20Count = new short[1] ;
         H00QO12_AV20Count = new short[1] ;
         H00QO13_AV20Count = new short[1] ;
         H00QO14_AV20Count = new short[1] ;
         H00QO15_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00QO15_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00QO15_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         H00QO15_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         H00QO15_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         H00QO15_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         H00QO16_A1Usuario_Codigo = new int[1] ;
         H00QO16_A293Usuario_EhFinanceiro = new bool[] {false} ;
         H00QO17_A1Usuario_Codigo = new int[1] ;
         H00QO17_A293Usuario_EhFinanceiro = new bool[] {false} ;
         sStyleString = "";
         lblTbjava_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnenter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblTextblockapagarperfis_Jsonclick = "";
         lblTextblockapagarservicos_Jsonclick = "";
         lblTextblockapagargestao_Jsonclick = "";
         lblTextblockapagarsistemas_Jsonclick = "";
         lblTextblockapagarfinanceiro_Jsonclick = "";
         lblTextblockapagarcusto_Jsonclick = "";
         lblTextblockclonarperfis_Jsonclick = "";
         lblTextblockclonarservicos_Jsonclick = "";
         lblTextblockclonargestao_Jsonclick = "";
         lblTextblockclonarsistemas_Jsonclick = "";
         lblTextblockclonarfinanceiro_Jsonclick = "";
         lblTextblockclonarcusto_Jsonclick = "";
         lblTextblockdousuario_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_clonarperfil__default(),
            new Object[][] {
                new Object[] {
               H00QO2_AV20Count
               }
               , new Object[] {
               H00QO3_AV20Count
               }
               , new Object[] {
               H00QO4_AV20Count
               }
               , new Object[] {
               H00QO5_AV20Count
               }
               , new Object[] {
               H00QO6_AV20Count
               }
               , new Object[] {
               H00QO7_A70ContratadaUsuario_UsuarioPessoaCod, H00QO7_n70ContratadaUsuario_UsuarioPessoaCod, H00QO7_A66ContratadaUsuario_ContratadaCod, H00QO7_A1394ContratadaUsuario_UsuarioAtivo, H00QO7_n1394ContratadaUsuario_UsuarioAtivo, H00QO7_A69ContratadaUsuario_UsuarioCod, H00QO7_A71ContratadaUsuario_UsuarioPessoaNom, H00QO7_n71ContratadaUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00QO8_A61ContratanteUsuario_UsuarioPessoaCod, H00QO8_n61ContratanteUsuario_UsuarioPessoaCod, H00QO8_A63ContratanteUsuario_ContratanteCod, H00QO8_A54Usuario_Ativo, H00QO8_n54Usuario_Ativo, H00QO8_A60ContratanteUsuario_UsuarioCod, H00QO8_A62ContratanteUsuario_UsuarioPessoaNom, H00QO8_n62ContratanteUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00QO9_A57Usuario_PessoaCod, H00QO9_A1Usuario_Codigo, H00QO9_A58Usuario_PessoaNom, H00QO9_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00QO10_AV20Count
               }
               , new Object[] {
               H00QO11_AV20Count
               }
               , new Object[] {
               H00QO12_AV20Count
               }
               , new Object[] {
               H00QO13_AV20Count
               }
               , new Object[] {
               H00QO14_AV20Count
               }
               , new Object[] {
               H00QO15_A66ContratadaUsuario_ContratadaCod, H00QO15_A69ContratadaUsuario_UsuarioCod, H00QO15_A577ContratadaUsuario_CstUntPrdExt, H00QO15_n577ContratadaUsuario_CstUntPrdExt, H00QO15_A576ContratadaUsuario_CstUntPrdNrm, H00QO15_n576ContratadaUsuario_CstUntPrdNrm
               }
               , new Object[] {
               H00QO16_A1Usuario_Codigo, H00QO16_A293Usuario_EhFinanceiro
               }
               , new Object[] {
               H00QO17_A1Usuario_Codigo, H00QO17_A293Usuario_EhFinanceiro
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV8DoUsuario ;
      private short AV20Count ;
      private short cV20Count ;
      private short nGXWrapped ;
      private int AV16ParaCod ;
      private int AV18Contratada_Codigo ;
      private int AV17Contratante_Codigo ;
      private int wcpOAV16ParaCod ;
      private int wcpOAV18Contratada_Codigo ;
      private int wcpOAV17Contratante_Codigo ;
      private int A1Usuario_Codigo ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1825ContratoAuxiliar_UsuarioCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int lblTbjava_Visible ;
      private int lblTextblockclonarcusto_Visible ;
      private int lblTextblockapagarcusto_Visible ;
      private int lblTextblockclonarsistemas_Visible ;
      private int lblTextblockapagarsistemas_Visible ;
      private int lblTextblockapagarfinanceiro_Visible ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A57Usuario_PessoaCod ;
      private int lblTextblockapagarperfis_Visible ;
      private int lblTextblockapagarservicos_Visible ;
      private int lblTextblockapagargestao_Visible ;
      private int idxLst ;
      private decimal A577ContratadaUsuario_CstUntPrdExt ;
      private decimal A576ContratadaUsuario_CstUntPrdNrm ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavClonarperfis_Internalname ;
      private String chkavClonarservicos_Internalname ;
      private String chkavClonargestao_Internalname ;
      private String chkavClonarsistemas_Internalname ;
      private String chkavClonarfinanceiro_Internalname ;
      private String chkavClonarcusto_Internalname ;
      private String chkavApagarperfis_Internalname ;
      private String chkavApagarservicos_Internalname ;
      private String chkavApagargestao_Internalname ;
      private String chkavApagarsistemas_Internalname ;
      private String chkavApagarfinanceiro_Internalname ;
      private String chkavApagarcusto_Internalname ;
      private String cmbavDousuario_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTextblockclonarcusto_Internalname ;
      private String lblTextblockapagarcusto_Internalname ;
      private String lblTextblockclonarsistemas_Internalname ;
      private String lblTextblockapagarsistemas_Internalname ;
      private String lblTextblockapagarfinanceiro_Internalname ;
      private String scmdbuf ;
      private String lblTextblockclonarperfis_Caption ;
      private String lblTextblockclonarperfis_Internalname ;
      private String lblTextblockclonarservicos_Caption ;
      private String lblTextblockclonarservicos_Internalname ;
      private String lblTextblockclonargestao_Caption ;
      private String lblTextblockclonargestao_Internalname ;
      private String lblTextblockclonarsistemas_Caption ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String A58Usuario_PessoaNom ;
      private String Dvpanel_tableattributes_Internalname ;
      private String lblTextblockapagarperfis_Caption ;
      private String lblTextblockapagarperfis_Internalname ;
      private String lblTextblockapagarservicos_Caption ;
      private String lblTextblockapagarservicos_Internalname ;
      private String lblTextblockapagargestao_Caption ;
      private String lblTextblockapagargestao_Internalname ;
      private String lblTextblockapagarsistemas_Caption ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTbjava_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String grpUnnamedgroup3_Internalname ;
      private String grpUnnamedgroup5_Internalname ;
      private String tblUnnamedtable4_Internalname ;
      private String lblTextblockapagarperfis_Jsonclick ;
      private String lblTextblockapagarservicos_Jsonclick ;
      private String lblTextblockapagargestao_Jsonclick ;
      private String lblTextblockapagarsistemas_Jsonclick ;
      private String lblTextblockapagarfinanceiro_Jsonclick ;
      private String lblTextblockapagarcusto_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTextblockclonarperfis_Jsonclick ;
      private String lblTextblockclonarservicos_Jsonclick ;
      private String lblTextblockclonargestao_Jsonclick ;
      private String lblTextblockclonarsistemas_Jsonclick ;
      private String lblTextblockclonarfinanceiro_Internalname ;
      private String lblTextblockclonarfinanceiro_Jsonclick ;
      private String lblTextblockclonarcusto_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockdousuario_Internalname ;
      private String lblTextblockdousuario_Jsonclick ;
      private String cmbavDousuario_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A276Perfil_Ativo ;
      private bool A293Usuario_EhFinanceiro ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV9ClonarPerfis ;
      private bool AV10ClonarServicos ;
      private bool AV11ClonarGestao ;
      private bool AV12ClonarSistemas ;
      private bool AV13ClonarFinanceiro ;
      private bool AV14ClonarCusto ;
      private bool AV21ApagarPerfis ;
      private bool AV22ApagarServicos ;
      private bool AV23ApagarGestao ;
      private bool AV24ApagarSistemas ;
      private bool AV25ApagarFinanceiro ;
      private bool AV26ApagarCusto ;
      private bool returnInSub ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool A54Usuario_Ativo ;
      private bool n54Usuario_Ativo ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool n58Usuario_PessoaNom ;
      private bool n577ContratadaUsuario_CstUntPrdExt ;
      private bool n576ContratadaUsuario_CstUntPrdNrm ;
      private bool AV28EhFinanceiro ;
      private bool AV29ParaEhFinanceiro ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavDousuario ;
      private GXCheckbox chkavClonarperfis ;
      private GXCheckbox chkavClonarservicos ;
      private GXCheckbox chkavClonargestao ;
      private GXCheckbox chkavClonarsistemas ;
      private GXCheckbox chkavClonarfinanceiro ;
      private GXCheckbox chkavClonarcusto ;
      private GXCheckbox chkavApagarperfis ;
      private GXCheckbox chkavApagarservicos ;
      private GXCheckbox chkavApagargestao ;
      private GXCheckbox chkavApagarsistemas ;
      private GXCheckbox chkavApagarfinanceiro ;
      private GXCheckbox chkavApagarcusto ;
      private IDataStoreProvider pr_default ;
      private short[] H00QO2_AV20Count ;
      private short[] H00QO3_AV20Count ;
      private short[] H00QO4_AV20Count ;
      private short[] H00QO5_AV20Count ;
      private short[] H00QO6_AV20Count ;
      private int[] H00QO7_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00QO7_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00QO7_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H00QO7_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00QO7_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] H00QO7_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00QO7_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00QO7_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00QO8_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00QO8_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H00QO8_A63ContratanteUsuario_ContratanteCod ;
      private bool[] H00QO8_A54Usuario_Ativo ;
      private bool[] H00QO8_n54Usuario_Ativo ;
      private int[] H00QO8_A60ContratanteUsuario_UsuarioCod ;
      private String[] H00QO8_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H00QO8_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] H00QO9_A57Usuario_PessoaCod ;
      private int[] H00QO9_A1Usuario_Codigo ;
      private String[] H00QO9_A58Usuario_PessoaNom ;
      private bool[] H00QO9_n58Usuario_PessoaNom ;
      private short[] H00QO10_AV20Count ;
      private short[] H00QO11_AV20Count ;
      private short[] H00QO12_AV20Count ;
      private short[] H00QO13_AV20Count ;
      private short[] H00QO14_AV20Count ;
      private int[] H00QO15_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00QO15_A69ContratadaUsuario_UsuarioCod ;
      private decimal[] H00QO15_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] H00QO15_n577ContratadaUsuario_CstUntPrdExt ;
      private decimal[] H00QO15_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] H00QO15_n576ContratadaUsuario_CstUntPrdNrm ;
      private int[] H00QO16_A1Usuario_Codigo ;
      private bool[] H00QO16_A293Usuario_EhFinanceiro ;
      private int[] H00QO17_A1Usuario_Codigo ;
      private bool[] H00QO17_A293Usuario_EhFinanceiro ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( bool ))]
      private IGxCollection AV19Clonar ;
      [ObjectCollection(ItemType=typeof( bool ))]
      private IGxCollection AV27Apagar ;
      private GXWebForm Form ;
   }

   public class wp_clonarperfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00QO2 ;
          prmH00QO2 = new Object[] {
          new Object[] {"@AV8DoUsuario",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00QO3 ;
          prmH00QO3 = new Object[] {
          new Object[] {"@AV8DoUsuario",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00QO4 ;
          prmH00QO4 = new Object[] {
          new Object[] {"@AV8DoUsuario",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00QO5 ;
          prmH00QO5 = new Object[] {
          new Object[] {"@AV8DoUsuario",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00QO6 ;
          prmH00QO6 = new Object[] {
          new Object[] {"@AV8DoUsuario",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00QO7 ;
          prmH00QO7 = new Object[] {
          new Object[] {"@AV16ParaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QO8 ;
          prmH00QO8 = new Object[] {
          new Object[] {"@AV16ParaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QO9 ;
          prmH00QO9 = new Object[] {
          new Object[] {"@AV16ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QO10 ;
          prmH00QO10 = new Object[] {
          new Object[] {"@AV16ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QO11 ;
          prmH00QO11 = new Object[] {
          new Object[] {"@AV16ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QO12 ;
          prmH00QO12 = new Object[] {
          new Object[] {"@AV16ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QO13 ;
          prmH00QO13 = new Object[] {
          new Object[] {"@AV16ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QO14 ;
          prmH00QO14 = new Object[] {
          new Object[] {"@AV16ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QO15 ;
          prmH00QO15 = new Object[] {
          new Object[] {"@AV16ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QO16 ;
          prmH00QO16 = new Object[] {
          new Object[] {"@AV8DoUsuario",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00QO17 ;
          prmH00QO17 = new Object[] {
          new Object[] {"@AV16ParaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00QO2", "SELECT COUNT(*) FROM ([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo]) WHERE (T1.[Usuario_Codigo] = @AV8DoUsuario) AND (T2.[Perfil_Ativo] = 1) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO2,1,0,true,false )
             ,new CursorDef("H00QO3", "SELECT COUNT(*) FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @AV8DoUsuario ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO3,1,0,true,false )
             ,new CursorDef("H00QO4", "SELECT COUNT(*) FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_UsuarioCod] = @AV8DoUsuario ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO4,1,0,true,false )
             ,new CursorDef("H00QO5", "SELECT COUNT(*) FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_UsuarioCod] = @AV8DoUsuario ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO5,1,0,true,false )
             ,new CursorDef("H00QO6", "SELECT COUNT(*) FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [ContratanteUsuario_UsuarioCod] = @AV8DoUsuario ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO6,1,0,true,false )
             ,new CursorDef("H00QO7", "SELECT T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_ContratadaCod], T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] <> @AV16ParaCod) AND (T2.[Usuario_Ativo] = 1) AND (T1.[ContratadaUsuario_ContratadaCod] = @AV18Contratada_Codigo) ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO7,100,0,false,false )
             ,new CursorDef("H00QO8", "SELECT T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPess, T1.[ContratanteUsuario_ContratanteCod], T2.[Usuario_Ativo], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE (T1.[ContratanteUsuario_UsuarioCod] <> @AV16ParaCod) AND (T2.[Usuario_Ativo] = 1) AND (T1.[ContratanteUsuario_ContratanteCod] = @AV17Contratante_Codigo) ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO8,100,0,false,false )
             ,new CursorDef("H00QO9", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV16ParaCod ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO9,1,0,false,true )
             ,new CursorDef("H00QO10", "SELECT COUNT(*) FROM ([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo]) WHERE (T1.[Usuario_Codigo] = @AV16ParaCod) AND (T2.[Perfil_Ativo] = 1) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO10,1,0,true,false )
             ,new CursorDef("H00QO11", "SELECT COUNT(*) FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @AV16ParaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO11,1,0,true,false )
             ,new CursorDef("H00QO12", "SELECT COUNT(*) FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_UsuarioCod] = @AV16ParaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO12,1,0,true,false )
             ,new CursorDef("H00QO13", "SELECT COUNT(*) FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_UsuarioCod] = @AV16ParaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO13,1,0,true,false )
             ,new CursorDef("H00QO14", "SELECT COUNT(*) FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [ContratanteUsuario_UsuarioCod] = @AV16ParaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO14,1,0,true,false )
             ,new CursorDef("H00QO15", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, [ContratadaUsuario_CstUntPrdExt], [ContratadaUsuario_CstUntPrdNrm] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_UsuarioCod] = @AV16ParaCod ORDER BY [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO15,1,0,false,true )
             ,new CursorDef("H00QO16", "SELECT TOP 1 [Usuario_Codigo], [Usuario_EhFinanceiro] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV8DoUsuario ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO16,1,0,false,true )
             ,new CursorDef("H00QO17", "SELECT TOP 1 [Usuario_Codigo], [Usuario_EhFinanceiro] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV16ParaCod ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QO17,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 2 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 8 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 9 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 10 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 11 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 12 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
